const http = require("http");
const { exec } = require("child_process");
const { join } = require("path");
const { existsSync, writeFileSync, unlinkSync } = require("fs");

const PERMISSIONS_TABLE_PATH = join(
  __dirname,
  "src",
  "constants",
  "permissions.js"
);

if (existsSync(PERMISSIONS_TABLE_PATH)) {
  // unlinkSync(PERMISSIONS_TABLE_PATH);
}

http
  .get(`${process.env.PROXY}/api/menu/functionId`, res => {
    let data = "";
    res.on("data", chunk => {
      data += chunk;
    });
    res.on("end", () => {
      data = JSON.parse(data).Data;
      data = Object.keys(data).map(
        key => `export const ${key} = ${data[key]};`
      );
      writeFileSync(join(PERMISSIONS_TABLE_PATH), data.join("\n"), "utf8");
    });
    res.resume();
    exec("ls -al ./src/constants", (error, stdout, stderr) => {
      console.log("error \n", error);
      console.log("stdout \n", stdout);
    });
  })
  .on("error", e => {
    console.log(`Got error: ${e.message}`);
  });
