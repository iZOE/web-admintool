import React, { useState, useMemo, useEffect, createContext } from 'react'
import PropTypes from 'prop-types'
import { IntlProvider } from 'react-intl'
import moment from 'moment'
import * as zhCN from '../locale/zh-CN.json'
import * as enUS from '../locale/en-US.json'
import * as viVN from '../locale/vi-VN.json'

/**
 * 把{a: {b: c: 'd'}變成a.b.c
 * a.b.c === 'd'
 */
const flattenMessages = (nestedMessages, prefix = '') =>
    Object.keys(nestedMessages).reduce((messages, key) => {
        const value = nestedMessages[key]
        const prefixedKey = prefix ? `${prefix}.${key}` : key
        if (typeof value === 'string') {
            Object.assign(messages, {
                [prefixedKey]: value,
            })
        } else {
            Object.assign(messages, flattenMessages(value, prefixedKey))
        }
        return messages
    }, {})

const LANG_CONFIG = {
    'zh-CN': zhCN,
    'vi-VN': viVN,
    'en-US': enUS,
}

export const LANG_OPTIONS = {
    'zh-CN': {
        displayName: '简体中文',
    },
    'vi-VN': {
        displayName: '越南文',
    },
    /*
  "en-US": {
    displayName: "English"
  }
  */
}

export const LOCALE_TIMEZONE = {
    'zh-CN': 'Asia/Shanghai',
    'vi-VN': 'Asia/Ho_Chi_Minh',
    /*
  "en-US": {
    displayName: "English"
  }
  */
}

export const LanguageContext = createContext()

export function LanguageProvider({ children }) {
    const [locale, setLocale] = useState(
        localStorage.getItem('USER_LANG') || 'zh-CN'
    )

    function handleUpdateLocale(newLocale) {
        setLocale(newLocale)
        localStorage.setItem('USER_LANG', newLocale)
        moment.locale(newLocale)
    }

    function onError(e) {
        if ((e.code = 'MISSING_TRANSLATION')) {
            return
        }
        // console.error(e)
    }

    return (
        <LanguageContext.Provider
            value={{
                locale,
                localTimeZone: LOCALE_TIMEZONE[locale],
                onUpdateLocale: handleUpdateLocale,
            }}
            onError={onError}
        >
            <IntlProvider
                locale={locale}
                messages={flattenMessages(LANG_CONFIG[locale].default)}
                defaultLocale="en"
            >
                {React.Children.only(children)}
            </IntlProvider>
        </LanguageContext.Provider>
    )
}

LanguageProvider.propTypes = {
    children: PropTypes.element.isRequired,
}
