import { fromJS } from "immutable";

const UPDATE_USER_DETAILS = "UPDATE_USER_DETAILS";

const initState = fromJS({
  userDetails: null
});

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case UPDATE_USER_DETAILS: {
      return {
        userDetails: action.payload.userDetails
      };
    }
    default:
      return state;
  }
};

export default userReducer;
