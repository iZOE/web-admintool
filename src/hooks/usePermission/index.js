import { useRecoilState } from 'recoil'
import { permissionState } from 'recoilsystem/atoms'

/**
 * 傳入「模組」或「模組功能」權限代碼，會回傳個別的權限結果
 * * 權限表可從 ```constants/permissions``` import
 *
 * @example
 * import usePermission from 'hooks/usePermission';
 * import { MEMBERS_MEMBERS_VIEW, MEMBERS_MEMBERS_EDIT } from 'constants/permissions'
 *
 * const [memberView, memberEdit] = usePermission(MEMBERS_MEMBERS_VIEW, MEMBERS_MEMBERS_EDIT)
 * memberView // true or false
 * memberEdit // true or false
 * @returns {(boolean|Map<string, boolean>)[]}
 */

function usePermission() {
    /**
     * @type {boolean[]}
     */
    const permissions = []
    const [permission] = useRecoilState(permissionState);
    // console.log("arguments", arguments, permission);
    if (arguments.length) {
        for (let a = 0; a < arguments.length; a++) {
            permissions.push(
                permission ? !!permission.get(arguments[a]) : false
            )
        }
        return permissions
    }
    return []
}

export default usePermission
