import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import useSWR from 'swr';
import axios from 'axios';

function useGetThirdpartyTabs() {
  const { pathname } = useLocation();
  const [result, setResult] = useState([]);
  const gameProviderTypeGroup = pathname.split('/')[2];

  const { data: tabs } = useSWR(`/api/ThirdPartyBetDetail/${gameProviderTypeGroup}/Query`, url =>
    axios(url).then(res => res.data.Data)
  );

  useEffect(() => {
    if (tabs) {
      setResult(tabs);
    }
  }, [tabs]);

  return result;
}

export default useGetThirdpartyTabs;
