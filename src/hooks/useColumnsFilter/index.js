import React, { useState, useEffect } from "react";
import { Checkbox } from "antd";
import styled from "styled-components";
import invariant from "invariant";

const VerticalFlexWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const NO_ROW_CHECKED = "NO_ROW_CHECKED";
const ALL_ROW_CHECKED = "ALL_ROW_CHECKED";
const SOME_ROW_CHECKED = "SOME_ROW_CHECKED";

const calculateRowShowCheckBoxCondition = columns => {
  const allRowCount = columns.length;
  const checkedColumnCount = columns.filter(c => c.isShow).length;
  if (checkedColumnCount === 0) {
    return NO_ROW_CHECKED;
  } else if (checkedColumnCount >= 1 && checkedColumnCount < allRowCount) {
    return SOME_ROW_CHECKED;
  }
  return ALL_ROW_CHECKED;
};

export const useColumnsFilter = initWholeColumns => {
  const [choosedColumns, setChoosedColumns] = useState(initWholeColumns);
  const [isAllRowShow, setIsAllRowShow] = useState(true);
  const [isAllRowShowCBIntermediate, setIsAllRowShowCBIntermediate] = useState(
    false
  );

  const toggleAllRowShow = evt => {
    setChoosedColumns(prev =>
      prev.map(c => {
        if (isAllRowShow) {
          return { ...c, isShow: false };
        } else {
          return { ...c, isShow: true };
        }
      })
    );

    setIsAllRowShow(prev => !prev);
    setIsAllRowShowCBIntermediate(prev => false);
  };

  const toggleSpecifiedColumnIsShow = dataIndex => evt => {
    const { checked } = evt.target;
    setChoosedColumns(prev =>
      prev.map(c => {
        if (c.dataIndex === dataIndex) {
          return { ...c, isShow: checked };
        }
        return { ...c };
      })
    );
  };

  // update 全選的 checkbox
  useEffect(() => {
    const rowShowCBCheckedCondition = calculateRowShowCheckBoxCondition(
      choosedColumns
    );
    switch (rowShowCBCheckedCondition) {
      case NO_ROW_CHECKED:
        setIsAllRowShow(prev => false);
        setIsAllRowShowCBIntermediate(prev => false);
        break;
      case ALL_ROW_CHECKED:
        setIsAllRowShow(prev => true);
        setIsAllRowShowCBIntermediate(prev => false);
        break;
      case SOME_ROW_CHECKED:
        setIsAllRowShowCBIntermediate(prev => true);
        break;
      default:
        invariant(false, "something wrong");
    }
  }, [choosedColumns]);

  return [
    <VerticalFlexWrapper>
      <Checkbox
        onChange={toggleAllRowShow}
        checked={isAllRowShow}
        indeterminate={isAllRowShowCBIntermediate}
      >
        列展示
      </Checkbox>
      {choosedColumns.map(c => (
        <Checkbox
          key={c.title}
          onChange={toggleSpecifiedColumnIsShow(c.dataIndex)}
          checked={c.isShow}
        >
          {c.title}
        </Checkbox>
      ))}
    </VerticalFlexWrapper>,
    choosedColumns
  ];
};
