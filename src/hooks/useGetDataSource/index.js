import { useState, useEffect, useCallback } from "react";
import caller from "utils/fetcher";

export default function useGetDataSource(url, conditionInitialValue) {
  const [fetching, setFetching] = useState(false);
  const [dataSource, setDataSource] = useState(null);
  // 查詢參數 有三種：form表單, 分頁區, table排序區
  const [condition, setCondition] = useState(conditionInitialValue);

  function getDataResource() {
    setFetching(true);
    caller({
      method: "post",
      endpoint: url,
      body: condition
    })
      .then(res => {
        const data = res.Data.map((item, key) => {
          return {
            ...item,
            key
          };
        });
        setDataSource(data);
      })
      .catch(err => {
        console.error("Get Data Source Error: ", err);
      })
      .finally(() => {
        setFetching(false);
      });
  }
  useEffect(() => {
    // 打API 取資料
    getDataResource();
  }, [condition]);

  // 更新condition
  const onUpdateCondition = useCallback(newCondition => {
    setCondition(prevCondition => ({
      ...prevCondition,
      ...newCondition
    }));
  }, []);

  return {
    fetching,
    dataSource,
    condition,
    onUpdateCondition
  };
}
