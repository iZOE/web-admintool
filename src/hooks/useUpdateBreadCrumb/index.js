import React, { useContext, useEffect } from 'react'
import { BreadCrumbsContext } from 'contexts/BreadcrumbsContext'

export default function useUpdateBreadCrumb(obj, effectArgs) {
    const { onUpdateBreadCrumb } = useContext(BreadCrumbsContext)

    useEffect(
        () => {
            onUpdateBreadCrumb(obj)
        },
        effectArgs ? [...effectArgs] : []
    )
}
