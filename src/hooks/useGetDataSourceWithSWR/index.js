import { useState, useCallback, useEffect } from 'react';
import useSWR from 'swr';
import { PARAMS_METHOD } from 'constants/callerConfig';
import { SORT_DIRECTION } from 'constants/sortDirection';
import caller from 'utils/fetcher';
import { PAGINATION_CONFIG } from 'constants/paginationConfig';

const { defaultCurrent, defaultPageSize } = PAGINATION_CONFIG;
const DEFAULT_CONDITION_INFO = {
  paginationInfo: {
    pageNumber: defaultCurrent,
    pageSize: defaultPageSize,
  },
  sortInfo: { SortColumn: '', SortBy: SORT_DIRECTION.Desc },
};

export default function useGetDataSourceWithSWR({
  url,
  method = 'post',
  needPagenationInfo = false,
  needSortInfo = false,
  initialCondition,
  defaultSortKey = '',
  defaultSortDirection = SORT_DIRECTION.Desc,
  dataRefactor,
  swrOption,
  autoFetch = false,
  noCondition,
}) {
  const { paginationInfo, sortInfo } = DEFAULT_CONDITION_INFO;
  const [dataSource, setDataSource] = useState();
  const [isReady, setIsReady] = useState(1 << Number(!!autoFetch));
  const isGet = PARAMS_METHOD.has(method.toLowerCase());
  const [condition, setCondition] = useState(
    initialCondition || {
      paginationInfo: !isGet || needPagenationInfo ? paginationInfo : null,
      sortInfo:
        !isGet || needSortInfo
          ? {
              ...sortInfo,
              SortColumn: defaultSortKey,
              SortBy: defaultSortDirection,
            }
          : null,
    }
  );

  const { data, isValidating: fetching, mutate: boundedMutate, error } = useSWR(
    isReady === 4 ? [url, condition] : null,
    (url, body) => {
      const payload = {
        [isGet ? 'params' : 'body']: body,
      };
      return caller({
        method,
        endpoint: url,
        ...payload,
      }).then(data => {
        if (data) {
          if (dataRefactor) {
            return dataRefactor(data);
          }
          return data.Data;
        }
      });
    },
    {
      ...swrOption,
      revalidateOnFocus: false,
      onError: () => console.error('onError~~~~'),
    }
  );

  useEffect(() => {
    if (data) {
      setDataSource(data);
    }
  }, [data]);

  const onReady = useCallback(value => {
    setIsReady(prev => prev << Number(value));
  });

  useEffect(() => {
    if (noCondition) {
      onReady(true);
    }
  }, [noCondition]);

  // 更新condition
  const onUpdateCondition = useCallback(newCondition => {
    const { sortInfo } = DEFAULT_CONDITION_INFO;
    setCondition(prevCondition => ({
      sortInfo:
        !isGet || needSortInfo
          ? {
              ...sortInfo,
              SortColumn: defaultSortKey,
              SortBy: defaultSortDirection,
            }
          : null,
      ...prevCondition,
      ...newCondition,
    }));
  });

  onUpdateCondition.bySearch = newCondition => {
    const { paginationInfo } = condition;
    onUpdateCondition({
      ...newCondition,
      paginationInfo: !isGet || needPagenationInfo ? { ...paginationInfo, pageNumber: 1 } : null,
    });
    isReady !== 4 && setIsReady(4);
  };

  return {
    fetching,
    dataSource,
    boundedMutate,
    condition,
    onReady,
    onUpdateCondition,
    error,
  };
}
