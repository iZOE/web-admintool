import { useEffect, useContext, useRef } from 'react';
import axios from 'axios';
import moment from 'moment-timezone';
import { useHistory } from 'react-router-dom';
import { message } from 'antd';
import { LanguageContext } from 'i18n/LanguageProvider';
import { PARAMS_METHOD } from 'constants/callerConfig';
import { PATTERN } from 'constants/dateConfig';
import { UserContext } from 'contexts/UserContext';

export default function useAxiosMiddleware() {
  const { locale } = useContext(LanguageContext);
  const { data: userCtxData } = useContext(UserContext);
  const ref = useRef();
  const history = useHistory();

  useEffect(() => {
    axios.interceptors.request.use(config => {
      const payload = PARAMS_METHOD.has(config.method.toLowerCase())
        ? 'params'
        : 'data';
      const data = Array.isArray(config[payload])
        ? [...config[payload]]
        : { ...config[payload] };

      if (userCtxData && !Array.isArray(data)) {
        const siteTimeZone = moment.tz(userCtxData.Locale.TimeZone).format('Z');

        Object.keys(data).forEach(key => {
          let isDateTimeField = PATTERN.INCLUDE_DATE.test(key);

          if (isDateTimeField && data[key] !== null) {
            let isISOFormat = PATTERN.TIME_FORMAT.test(data[key]);

            if (isISOFormat) {
              data[key] = data[key] + siteTimeZone;
            }

            return;
          }

          if (data[key] === '') {
            return (data[key] = null);
          }
        });
      }
      return { ...config, [payload]: data };
    });
  }, [userCtxData]);

  useEffect(() => {
    /**
     * AXIOS middleware
     */
    axios.interceptors.response.use(
      config => {
        if (
          config.config.responseType !== 'arraybuffer' &&
          config.status === 200 &&
          !config.data.Status
        ) {
          message.error(config.data.ResponseMessage, 5);
          return Promise.reject({
            type: 'STATUS_FALSE',
            data: config
          });
        }
        return config;
      },
      error => {
        if (error.response.status === 403) {
          history.go(0);
        }
        if (!error.response.config.closeMessage) {
          message.error(error.response.data.ResponseMessage, 5);
        }
        return Promise.reject({
          type: 'STATUS_FALSE',
          data: error.response
        });
      }
    );
  }, []);

  useEffect(() => {
    axios.defaults.headers.common['Language'] = locale;
    if (ref.current && ref.current !== locale) {
      window.location.reload();
    }
    ref.current = locale;
  }, [locale]);
}
