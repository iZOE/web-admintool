import { combineReducers } from "redux-immutable";
import user from "../reducers/user/loginReducers";

const rootReducer = () =>
  combineReducers({
    user
  });

export default rootReducer;
