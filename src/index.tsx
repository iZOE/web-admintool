import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { SWRConfig } from "swr";
import { ConfigProvider } from "antd";
import { BrowserRouter as Router } from "react-router-dom";
import { RecoilRoot } from 'recoil';
import { LanguageProvider } from "i18n/LanguageProvider";
import axios from "axios";
import UserContextProvider from "contexts/UserContext";
import store from "./store";
import "./index.css";
import App from "./App";
import reportWebVitals from './reportWebVitals';

const validateMessages = {
  required: "是必选字段",
};

axios.defaults.headers.common["Language"] =
  localStorage.getItem("USER_LANG") || "zh-CN";

ReactDOM.render(
  <React.StrictMode>
    <RecoilRoot>
      <LanguageProvider>
        <ConfigProvider form={{ validateMessages }}>
          <Provider store={store}>
            <SWRConfig
              value={{
                refreshInterval: 0,
              }}
            >
              <UserContextProvider>
                <Router>
                  <App />
                </Router>
              </UserContextProvider>
            </SWRConfig>
          </Provider>
        </ConfigProvider>
      </LanguageProvider>
    </RecoilRoot>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
reportWebVitals();