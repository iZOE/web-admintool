export const PAGESIZE_OPTIONS = ["10", "25", "50", "100"];
export const ACTIVITY_PAGESIZE_OPTIONS = ["10", "20", "30", "50"];

export const FULL_SCREENABLE_AREA_WRAPPER_ID =
  "FULL_SCREENABLE_AREA_WRAPPER_ID";

export const getPopupContainer = () =>
  document.getElementById(FULL_SCREENABLE_AREA_WRAPPER_ID);
