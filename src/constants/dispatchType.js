export const DISPATCH_TYPE = {
    REJECT: 0,
    DISPATCH: 1,
}

export const DISPATCH_COMMISSION_MODE = {
    DIVIDEND: 1, // 分紅
    LOTTERY: 2, // 彩票虧損佣金
    AGENCY: 4, // 全民代
    FIRST_LEVEL_DIVIDEND: 6, // 一級分紅
}

export const DISPATCH_REPORT_DETAIL_STATUS = {
    NOTDISPATCH: 1, // 未派發
    ISDISPATCH: 2, // 已派發
    REJECT: 3, // 已拒絕
    ACHIEVED_GOAL: 4, // 已達領取上限
}
