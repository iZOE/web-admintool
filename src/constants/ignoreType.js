export const IGNORE_TYPE = {
  DIVIDEND: 1, // 分紅
  DAILY_WAGES: 2, // 日工資
  AGENCY: 4, // 全民代
  FIRST_LEVEL_DIVIDEND: 6 // 一級分紅
};
