import * as dayjs from 'dayjs'
import moment from 'moment'
const now = dayjs()

const formatted = v => {
    return moment(v.format(FORMAT.DEFAULT.FULL))
}

export const FORMAT = {
    DEFAULT: {
        FULL: 'YYYY/MM/DD HH:mm:ss',
        DAILY: 'YYYY/MM/DD',
    },
    ISO:
        localStorage.getItem('USER_LANG') === 'vi-VN'
            ? 'DD/MM/YYYY HH:mm:ss'
            : 'YYYY/MM/DD HH:mm:ss',
    DISPLAY: {
        DEFAULT:
            localStorage.getItem('USER_LANG') === 'vi-VN'
                ? 'DD-MM-YYYY HH:mm:ss'
                : 'YYYY-MM-DD HH:mm:ss',
        DAILY:
            localStorage.getItem('USER_LANG') === 'vi-VN'
                ? 'DD-MM-YYYY'
                : 'YYYY-MM-DD',
    },
}

export const PATTERN = {
    INCLUDE_DATE: new RegExp(/Time|Date/),
    TIME_FORMAT: new RegExp(
        /[0-9]{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]/
    ),
}

export const VALUES = {
    CURRENT: {
        DAY: {
            START: now.startOf('day'),
            DEFAULT: now,
            END: now.endOf('day'),
        },
    },
    NEXT: {
        DAY: now.startOf('day').add(1, 'day'),
        MONTH: now.startOf('day').add(1, 'month'),
    },
    LAST: {
        DAY: now.subtract(1, 'day').startOf('day'),
        MONTH: now.subtract(1, 'month').startOf('day'),
        QUARTER: now.subtract(3, 'month').startOf('day'),
    },
    INITIAL: dayjs('2017-01-01 00:00:00'),
}

export const DEFAULT = {
    RANGE: {
        FROM_TOMORROW_TO_A_MONTH: [
            formatted(VALUES.NEXT.DAY),
            formatted(VALUES.NEXT.MONTH),
        ],
        FROM_TODAY_TO_TODAY: [
            formatted(VALUES.CURRENT.DAY.START),
            formatted(VALUES.CURRENT.DAY.END),
        ],
        FROM_YESTERDAY_TO_TODAY: [
            formatted(VALUES.LAST.DAY),
            formatted(VALUES.CURRENT.DAY.END),
        ],
        FROM_A_MONTH_TO_TODAY: [
            formatted(VALUES.LAST.MONTH),
            formatted(VALUES.CURRENT.DAY.END),
        ],
        FROM_A_QUARTER_TO_TODAY: [
            formatted(VALUES.LAST.QUARTER),
            formatted(VALUES.CURRENT.DAY.END),
        ],
        FROM_INITIAL_TO_TODAY: [
            formatted(VALUES.INITIAL),
            formatted(VALUES.CURRENT.DAY.END),
        ],
    },
}
