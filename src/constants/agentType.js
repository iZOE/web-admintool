export const AGENT_TYPE = {
  CAILIFUN_I: 1, // 彩立方 1
  XING_HUI: 5, // 星輝
  CAILIFUN_II: 6, // 彩立方 2
  SHOU_MI: 7, // 收米
  JIN_SHA: 8, // 金莎
  VT_999: 9, // 越南站
  CN_999: 10, // 越南站(簡中版)
  HL_101: 11 // 皇樂體育
};
