import React from 'react';
import { FormattedMessage } from 'react-intl';
import { PAGESIZE_OPTIONS } from 'constants/forList';

export const PAGINATION_CONFIG = {
  size: 'small',
  defaultCurrent: 1,
  defaultPageSize: 25,
  showTotal: (total, range) => (
    <FormattedMessage
      id="Share.Table.Footer"
      values={{
        first: range[0],
        last: range[1],
        total: total,
      }}
    />
  ),
  pageSizeOptions: PAGESIZE_OPTIONS,
  position: ['bottomCenter'],
  showQuickJumper: true,
  showSizeChanger: true,
};
