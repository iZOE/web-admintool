export const TAB_INFO = {
  BASIC_DATA: "BasicData", // 基本資料
  BANK_DATA: "BankData", // 銀行資料
  ACCOUNT_SETTING: "AccountSetting", // 帳號設定
  ACCOUNT_SECURITY: "AccountSecurity", // 安全設定
  TURNOVER_LOG: "TurnoverLog", // 流水審核
  SUMMARIZED_LOG: "SummarizedLog", // 綜合紀錄
  CHANGE_LOG: "ChangeLog" // 異動紀錄
};
