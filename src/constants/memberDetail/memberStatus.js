export const MEMBER_STATUS = {
  NORMAL: 0, // 正常狀態
  LOCK_BY_MEMBER: 1, // 會員自己登入造成的鎖定
  LOCK_BY_ADMINTOOL: 2, // 後台鎖定(凍結)
  LOCK_BY_UNUSUAL_LOGIN: 3, // 登入異常造成的鎖定
  LOCK_BY_RISK_CONTROL: 4 // 風險控制鎖定
};

export const ONLINE_STATUS = {
  ACTIVE: 1, // 線上
  DEACTIVE: 2, // 離線
  LOCK: 3, // 鎖定
  FREEZE: 4 // 凍結
};
