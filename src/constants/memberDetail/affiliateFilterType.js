export const FILTER_TYPE = {
  NONE: 0,
  ONLY_MEMBER: 1,
  ONLY_AGENCY: 2,
  ALL_AGENCY: 3
};
