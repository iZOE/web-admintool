export const NICKNAME = {
  MAX: 14,
  MIN: 1
};

export const MEMBER_ACCOUNT = {
  MAX: 16,
  MIN: 6,
  PATTERN: new RegExp(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/)
};

export const LOGIN_PASSWORD = {
  MAX: 16,
  MIN: 6,
  PATTERN: new RegExp(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/)
};

export const RETURN_POINT = {
  MAX: 1980,
  MIN: 1800
};

export const RETURN_POINT_FIELD = {
  MAX: 0,
  MIN: 1
};
