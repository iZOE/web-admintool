export const MEMBER_TYPE = {
  DEFAULT: 1, // 非內部會員
  INTERNAL: 2 // 內部會員
};

export const AFFILIATE_LEVEL_TYPE = {
  SHAREHOLDER: 1, // 股東號
  VIP: 2, // VIP 直屬
  AGENCY: 3, // 代理
  MEMBER: 99 // 一般會員
};

export const MEMBER_STYLE = {
  MEMBER: 1, // 一般會員
  AGENT: 2, // 代理商: 含股東號、VIP 直屬、代理
  BANK: 3 // 銀商
};
