export const TAB_INFO = {
  PENDING: "Pending", // 待審核
  FINISHED: "Finished", // 已審核
  HISTORY: "History" // 歷史紀錄
};

export const AUDIT_TYPE = {
  ALL: 1, // 全部, api = null
  PENDING: 2, // 待審核, api = false
  FINISHED: 3 // 已審核, api = true
};

export const RECORD_TYPE = {
  CURRENT: "CurrentRecord", // 當前紀錄
  TURNOVER: "TurnoverList" // 流水審核列表
};
