export const FORM_LAYOUT = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};
export const FORM_TAIL_LAYOUT = {
  wrapperCol: { offset: 6, span: 18 }
};
