export default {
  CREATE: 0,
  0: "CREATE",
  READ: 1,
  1: "READ",
  UPDATE: 2,
  2: "UPDATE",
  DELETE: 3,
  3: "DELETE"
};
