export const LAYOUT = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
  labelAlign: "left"
};

export const GUTTER = [16, 16];

export const SINGLE_ITEM = {
  xs: { span: 24 },
  sm: { span: 24 },
  md: { span: 8 },
  xl: { span: 6 }
};

export const SINGLE_ITEM_OFFSET = {
  xs: { span: 24 },
  sm: { span: 24 },
  md: { span: 8, offset: 8 },
  xl: { span: 6, offset: 12 }
};
