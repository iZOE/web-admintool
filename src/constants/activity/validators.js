import React from 'react'
import { FormattedMessage } from 'react-intl'
import { WITHDRAWAL_TIMES_MAXIMUM, TURNOVER_MULTIPLE } from 'constants/activity'

export const getMaximum = max => {
    return [
        {
            max,
            message: <FormattedMessage id="Share.FormValidate.Value.Maximum" />,
            type: 'number',
        },
    ]
}

export const getMinimum = min => {
    return [
        {
            min,
            message: <FormattedMessage id="Share.FormValidate.Value.Minimum" />,
            type: 'number',
        },
    ]
}

export const getLengthMaximum = max => {
    return [
        {
            max,
            message: (
                <FormattedMessage
                    id="Share.FormValidate.Length.Maximum"
                    values={{ max }}
                />
            ),
        },
    ]
}

const DEFAULT_VALUE = {
    MAX: 99999999.99,
    MIN: 0,
}

const REQUIRED = [
    {
        required: true,
        message: <FormattedMessage id="Share.FormValidate.Required.Input" />,
    },
]

const NOT_BE_ZERO = [
    {
        validator: (_, value) => {
            return value !== null && value === 0
                ? Promise.reject(
                      <FormattedMessage id="Share.FormValidate.Value.NotBeNull" />
                  )
                : Promise.resolve()
        },
    },
]

export const VALUE_MINIMUM = getMinimum(DEFAULT_VALUE.MIN)
export const VALUE_MAXIMUM = getMaximum(DEFAULT_VALUE.MAX)
const withdrawalTimesMaximum = getMaximum(WITHDRAWAL_TIMES_MAXIMUM.AMOUNT.MAX)
const turnoverMultipleMaximum = getMaximum(TURNOVER_MULTIPLE.MAX)
const subjectLengthMaximum = getLengthMaximum(20)
const summaryLengthMaximum = getLengthMaximum(100)

export const RULES = {
    REQUIRED: [...REQUIRED],
    SUBJECT: [...REQUIRED, ...subjectLengthMaximum],
    SUMMARY: [...summaryLengthMaximum],
    REWARD_RULE: {
        MODE: [...REQUIRED],
        AMOUNT: [...REQUIRED, ...VALUE_MINIMUM],
    },
    THRESHOLDS: [...VALUE_MAXIMUM],
    WITHDRAWAL_TIMES_MAXIMUM: [...withdrawalTimesMaximum, ...VALUE_MINIMUM],
    WITHDRAWAL_AMOUNT_MAXIMUM: [
        ...REQUIRED,
        ...VALUE_MINIMUM,
        ...VALUE_MAXIMUM,
    ],
    TURNOVER_MULTIPLE: [
        ...turnoverMultipleMaximum,
        ...REQUIRED,
        ...VALUE_MINIMUM,
    ],
    NOT_BE_ZERO: [...NOT_BE_ZERO],
}
