export const TAB_INFO = {
    BASIC: 'Basic', // 基本設定
    ADVANCED: 'Advanced', // 進階設定
    LOG: 'Log', // 異動紀錄
}

// 活動狀態
// DB的註解：活動狀態(1:只有基本設定,不需結算派發/2.有規則設定,需結算派發)
export const STATUS = {
    BASIC: 1,
    ADVANCED: 2,
}

// 結算時間
export const SETTLEMENT = {
    DAILY: 1,
    WEEKLY: 2,
    MONTHLY: 3,
}

// 門檻
export const THRESHOLD = {
    TYPE: {
        // 門檻類別
        TURNOVER: 'ValidBetAmount', // 流水門檻
        DEPOSIT: 'DepositAmount', // 充值門檻
        PROFIT_LOSS: 'WinLossAmount', // 盈虧門檻
    },
    MINIMUM: {
        IS_PROFIT_LOSS: -99999999.99, // 盈虧門檻最小值允許負值
        NOT_PROFIT_LOSS: 0.01, // 流水&充值門檻最小值
    },
    PERIOD: {
        LAST_WEEK: 3, // 前一週
        LAST_MONTH: 4, // 前一月
    },
}

// 獎金模式 (規則)
export const REWARD_RULE = {
    MODE: {
        ALL: 1, // 全部
        BY_MEMBER_LEVEL: 2, // 根據會員等級
    },
    TYPE: {
        FIXED_AMOUNT: {
            // 固定金額
            ID: 1,
            MAX: 99999999.99,
        },
        BY_TURNOVER: {
            // 依流水門檻比例
            ID: 2,
        },
        BY_DEPOSIT: {
            // 依充值門檻比例
            ID: 3,
        },
        BY_PROFIT_LOSS: {
            // 依盈虧門檻比例
            ID: 4,
        },
        BY_PERCENT: {
            // 非固定金額
            MAX: 1000,
        },
    },
}

// 領取次數上限
export const WITHDRAWAL_TIMES_MAXIMUM = {
    AMOUNT: {
        MAX: 1000,
    },
}

// 獎金上限
export const WITHDRAWAL_AMOUNT_MAXIMUM = {
    AMOUNT: {
        MAX: 99999999.99,
    },
    MODE: {
        INFINITE: 0, // 無上限
        LIMITED: 1, // 有上限
    },
}

// 流水審核倍數
export const TURNOVER_MULTIPLE = {
    MAX: 1000,
}

// 結算時間提醒模式
export const ALERT = {
    FOR_MONTHLY: 'Monthly', // 需提醒改為「月結」
    FOR_WEEKLY: 'Weekly', //需提醒改為「週結」
}
