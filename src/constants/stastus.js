export const ENABLE_STATUS = {
  0: "Inactive",
  Inactive: 0,
  1: "Active",
  Active: 1,
  2: "Pause",
  Pause: 2
};
