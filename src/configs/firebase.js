import firebase from "firebase/app";
import "firebase/storage";
import "firebase/database";

export const firebaseConfig = {
  apiKey: "AIzaSyAQoP_NQ0NvVs4KfPRsxoZyicCs_CTLKxY",
  authDomain: "admintool-example.firebaseapp.com",
  databaseURL: "https://admintool-example.firebaseio.com",
  projectId: "admintool-example",
  storageBucket: "admintool-example.appspot.com",
  messagingSenderId: "541966205276",
  appId: "1:541966205276:web:b050b64c4253adf5128199"
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();
const db = firebase.database();

export { firebase, storage, db };
