import React, { createContext, useState } from 'react';
import useSWR from 'swr';
import axios from 'axios';
import MemberDetail from 'components/MemberDetail';
import { MODE } from 'constants/mode';

export const MemberDetailContext = createContext();

export default function MemberDetailContextProvider({ children }) {
  const [tabToBeOpened, setTabToBeOpened] = useState(null);
  const [currentMemberId, setCurrentMemberId] = useState(null);
  const [isShowMemberDetail, setIsShownMemberDetail] = useState(false);
  const [selectedMode, setSelectedMode] = useState(MODE.VIEW);

  let { data: memberBasicDetail } = useSWR(
    currentMemberId ? `/api/Member/${currentMemberId}` : null,
    url => axios(url).then(res => res && res.data.Data)
  );

  function onDisplayMemberDetail(id, mode, tab) {
    setCurrentMemberId(id);
    setIsShownMemberDetail(true);
    setSelectedMode(mode);
    setTabToBeOpened(tab);
  }

  return (
    <MemberDetailContext.Provider
      value={{
        memberIdentity: memberBasicDetail && {
          agentType: {
            id: memberBasicDetail.AgentId,
            text: memberBasicDetail.AgentName
          },
          affiliateType: {
            id: memberBasicDetail.AffiliateLevelId,
            text: memberBasicDetail.AffiliateLevelName
          },
          memberData: {
            id: memberBasicDetail.MemberId,
            name: memberBasicDetail.MemberName
          },
          memberType: {
            id: memberBasicDetail.MemberTypeId,
            text: memberBasicDetail.MemberLevelName
          }
        },
        isEditMode: selectedMode === MODE.EDIT,
        onDisplayMemberDetail
      }}
    >
      {children}
      {isShowMemberDetail && (
        <MemberDetail
          memberBasicDetail={memberBasicDetail}
          showMemberDetail={isShowMemberDetail}
          tab={tabToBeOpened}
          onClose={() => setIsShownMemberDetail(false)}
        />
      )}
    </MemberDetailContext.Provider>
  );
}
