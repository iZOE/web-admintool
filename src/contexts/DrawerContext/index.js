import React, { createContext, useState } from "react";
import update from "immutability-helper";
import { Drawer, Skeleton } from "antd";

const Default_Config = {
  title: null,
  placement: "right",
  width: 720,
  closable: false,
  component: <Skeleton active />,
  onClose: null
};

export const DrawerContext = createContext();

export default function DrawerContextProvider({ children }) {
  const [config, setConfig] = useState(Default_Config);
  const [isEditing, setIsEditing] = useState(false);
  const { onClose: onDrawerClose, component, ...restConfig } = config;

  function setDrawerConfig(c = Default_Config) {
    setConfig(update(config, { $merge: c }));
  }

  function onEditing(value) {
    setIsEditing(value);
  }

  function onOpenDrawer(obj) {
    setConfig({
      ...obj,
      visible: true
    })
  }

  function onClose(e) {
    if (!isEditing) {
      setConfig({visible: false})
      onDrawerClose && onDrawerClose(e);
    }
  }

  return (
    <DrawerContext.Provider
      value={{ onOpenDrawer, setDrawerConfig, onEditing }}
    >
      {children}
      <Drawer visible={config.visible} onClose={e => onClose(e)} {...restConfig} destroyOnClose>
        {component}
      </Drawer>
    </DrawerContext.Provider>
  );
}
