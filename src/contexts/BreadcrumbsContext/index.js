import React, { useState, useCallback } from "react";

export const BreadCrumbsContext = React.createContext([]);

export default function BreadCrumbsContextProvider({ children }) {
  const [breadCrumbs, setBreadCrumbs] = useState({
    routes: [],
    title: null,
    subTitle: null,
    suffixBreadcrumbs: null
  });

  /**
   * 可更新 ```routes```, ```title```, ```subTitle```, ```content```
   */
  const onUpdateBreadCrumb = useCallback(value => {
    const { ...restValue } = value;
    setBreadCrumbs(prev => {
      return {
        ...prev,
        ...restValue
      };
    });
  }, []);

  const onSetsuffixBreadcrumbs = useCallback(items => {
    setBreadCrumbs(prev => {
      return {
        ...prev,
        suffixBreadcrumbs: [...breadCrumbs.routes].concat(items)
      };
    });
  }, []);

  return (
    <BreadCrumbsContext.Provider
      value={{
        ...breadCrumbs,
        onUpdateBreadCrumb,
        onSetsuffixBreadcrumbs
      }}
    >
      {children}
    </BreadCrumbsContext.Provider>
  );
}
