import React, { createContext, useState } from 'react';
import GroupDetail from 'components/GroupDetail';
import { MODE } from 'constants/mode';

export const GroupDetailContext = createContext();

export default function GroupDetailContextProvider({ children }) {
  const [currentGroupId, setCurrentGroupId] = useState(null);
  const [isShowGroupDetail, setIsShownGroupDetail] = useState(false);

  function onDisplayGroupDetail(id) {
    setCurrentGroupId(id);
    setIsShownGroupDetail(true);
  }

  return (
    <GroupDetailContext.Provider
      value={{
        memberId: currentGroupId,
        onDisplayGroupDetail
      }}
    >
      {children}
      {isShowGroupDetail && (
        <GroupDetail
          groupId={currentGroupId}
          showGroupDetail={isShowGroupDetail}
          onClose={() => setIsShownGroupDetail(false)}
        />
      )}
    </GroupDetailContext.Provider>
  );
}
