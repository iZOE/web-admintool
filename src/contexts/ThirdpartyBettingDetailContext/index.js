import { useState, useMemo, createContext } from 'react';
import DateWithFormat from 'components/DateWithFormat';
import { useParams } from 'react-router-dom';
import useSWR from 'swr';
import axios from 'axios';
import { Modal, Descriptions, Result, Skeleton } from 'antd';

export const ThirdpartyBettingDetailContext = createContext();

export default function ThirdpartyBettingDetailProvider({ defaultKey, children }) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [currentBillNo, setCurrentBillNo] = useState(null);
  const { key = defaultKey } = useParams();

  const { data, error } = useSWR(
    isModalVisible ? `/api/ThirdPartyBetDetail/${key}/${currentBillNo}` : null,
    url => axios(url).then(res => res && res.data.Data),
    {
      revalidateOnFocus: false,
      shouldRetryOnError: false,
    }
  );

  const thirdpartyBettingDetailData = useMemo(() => {
    if (!data) {
      return null;
    }
    return Object.keys(data.Data).reduce(
      (result, key) => [...result, { Name: data.Fields[key], Value: data.Data[key], key }],
      []
    );
  }, [data]);

  function onDisplayBettingDetail(billNo) {
    setIsModalVisible(true);
    setCurrentBillNo(billNo);
  }
  function closeModal() {
    setIsModalVisible(false);
  }
  return (
    <ThirdpartyBettingDetailContext.Provider value={{ onDisplayBettingDetail }}>
      <>
        {children}
        <Modal footer={false} visible={isModalVisible} onCancel={closeModal}>
          {thirdpartyBettingDetailData ? (
            <Descriptions layout="vertical" labelStyle={{ fontWeight: 'bold' }}>
              {thirdpartyBettingDetailData.map(({ key, Name, Value }) => (
                <Descriptions.Item key={key} label={Name}>
                  {/Time/.test(key) && Value !== '-' ? <DateWithFormat time={Value} /> : Value}
                </Descriptions.Item>
              ))}
            </Descriptions>
          ) : error ? (
            <Result status={error.data.status} title={error.data.status} subTitle={error.data.statusText} />
          ) : (
            <Skeleton active={true} />
          )}
        </Modal>
      </>
    </ThirdpartyBettingDetailContext.Provider>
  );
}
