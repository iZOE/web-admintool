import React, { createContext, useCallback, useEffect, useState } from 'react';

export const ConditionContext = createContext();

export function ConditionContextProvider({ children }) {
  const [formItems, setFormItems] = useState();
  const [isReady, setReady] = useState(false);
  const updateFormItems = useCallback(formItem => {
    setFormItems(prev => {
      return {
        ...prev,
        ...formItem
      };
    });
  }, []);

  useEffect(() => {
    console.log('ConditionContextProvider -> formItems', formItems);
    if (formItems) {
      const ALL_READY = new Set(Object.values(formItems));
      setReady(!ALL_READY.size || !ALL_READY.has(false));
    }
  }, [formItems]);

  return (
    <ConditionContext.Provider
      value={{
        formItems,
        updateFormItems,
        isReady
      }}
    >
      {children}
    </ConditionContext.Provider>
  );
}
