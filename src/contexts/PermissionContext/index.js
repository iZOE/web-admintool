import React, { useMemo } from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { configConsumerProps } from 'antd/lib/config-provider'

/**
 * A number, or a string containing a number.
 * @typedef {Map<number|string, boolean>} IPermission
 * @typedef {{[key: string]: IPermission}} Permission
 */

/**
 * @type {React.Context<Permission>}
 */
export const PermissionContext = React.createContext()

/**
 *
 *
 * @param {Object} props - Permission Props.
 * @param {React.Component} props.children - Permission Props.
 */
export default function PermissionContextProvider({ children }) {
    const { data: permissionData } = useSWR(
        ['/api/User/FunctionIds', '/api/menu/functionId'],
        (userFuIds, allFuIds) =>
            axios
                .all([axios.get(userFuIds), axios.get(allFuIds)])
                .then(res => ({
                    userFuIds: res[0].data.Data,
                    allFuIds: res[1].data.Data,
                }))
    )

    /**
     * @type {Permission}
     */
    const allFnIdsData = useMemo(() => {
        if (permissionData) {
            const { userFuIds, allFuIds } = permissionData
            const userFnIdsSet = new Set(userFuIds)
            return new Map(
                Object.keys(allFuIds).map(key => [
                    allFuIds[key],
                    userFnIdsSet.has(allFuIds[key]),
                ])
            )
        }
        return new Map()
    }, [permissionData])

    // console.log("allFnIdsData", allFnIdsData);

    return (
        <PermissionContext.Provider value={allFnIdsData}>
            {children}
        </PermissionContext.Provider>
    )
}
