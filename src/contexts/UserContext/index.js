import React, { useState, useEffect } from "react";
import useSWR, { mutate } from "swr";
import axios from "axios";
import cookies from "js-cookie";
import { cookieFetcher } from "utils/fetcher";

export const API_PROCESS = {
  LOADING: "LOADING",
  SUCCESS: "SUCCESS",
  ERROR: "ERROR",
};

function deleteAllCookies() {
  var cookies = document.cookie.split(";");

  if (cookies) {
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  }
}

export const UserContext = React.createContext();

export default function UserContextProvider({ children }) {
  const nowDate = new Date();
  const [userFetchStatus, setUserFetchStatus] = useState(API_PROCESS.LOADING);

  const {
    data: userCookie,
    error: fetchUserDataError,
    mutate,
  } = useSWR("local/user", () => cookieFetcher("gameadmin_auth"));

  const { data: isUserSessionOK, error } = useSWR(
    userCookie ? ["/api/User/Login", userCookie] : null,
    (url) =>
      axios(url)
        .then((res) => res.data.Status)
        .catch((err) => {
          return Promise.reject({ error: err });
        })
  );

  useEffect(() => {
    if (!userCookie) {
      setUserFetchStatus(API_PROCESS.SUCCESS);
    } else if (userCookie && fetchUserDataError) {
      setUserFetchStatus(API_PROCESS.ERROR);
    }
  }, [isUserSessionOK, userCookie, fetchUserDataError]);

  useEffect(() => {
    if (error) {
      cookies.remove("gameadmin_auth");
      mutate("local/user");
    }
  }, [error]);

  function onRefreshData() {
    // 更新使用者狀態
    /*
    const nowDate = new Date();
    setUpdateTime(nowDate.getTime());
    */
    mutate("/api/User/Login");
  }

  return (
    <UserContext.Provider
      value={{
        userFetchStatus,
        data: userCookie,
        onRefreshData,
      }}
    >
      {children}
    </UserContext.Provider>
  );
}
