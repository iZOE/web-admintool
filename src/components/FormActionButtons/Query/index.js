import React from 'react'
import { Button } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'

export default function QueryButton({ loading = false }) {
    return (
        <Button
            type="primary"
            htmlType="submit"
            icon={<SearchOutlined style={{ marginRight: 6 }} />}
            loading={loading}
        >
            <FormattedMessage
                id="Share.ActionButton.Query"
                description="查詢"
            />
        </Button>
    )
}
