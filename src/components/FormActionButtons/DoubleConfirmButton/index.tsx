import React from 'react'
import { Popconfirm, Button } from 'antd'
import { AlertFilled } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'

export interface Props {
    confirmMsg: any
    onCancel?: any
    onOk: any
}

export default function DoubleConfirmButton({
    onOk,
    onCancel = () => {},
    confirmMsg,
}: Props) {
    return (
        <Popconfirm
            onConfirm={onOk}
            onCancel={onCancel}
            placement="top"
            icon={<AlertFilled style={{ color: 'red' }} />}
            title={confirmMsg}
            okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
            cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
        >
            <Button type="primary">
                <FormattedMessage id="Share.ActionButton.Submit" />
            </Button>
        </Popconfirm>
    )
}
