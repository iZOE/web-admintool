import React from 'react';
import styled from 'styled-components';

export const Wrapper = styled.div`
  .picture-uploader .ant-upload {
    width: 100%;
    height: auto;
  }
`;
