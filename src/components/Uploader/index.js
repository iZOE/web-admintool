import React, { useState, useEffect } from 'react'
import { message, Typography, Upload } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import { Wrapper } from './Styled'

const { Text } = Typography

const API_PROCESS = {
  INITIAL: 'INITIAL',
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
}

export default function Uploader({
  action,
  disabled,
  text,
  onSuccess,
  defaultUrl,
}) {
  const [state, setState] = useState({
    status: API_PROCESS.INITIAL,
  })

  useEffect(() => {
    setState({
      ...state,
      url: defaultUrl,
    })
  }, [defaultUrl])

  function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!')
    }
    const isLt2M = file.size / 1024 / 1024 < 3
    if (!isLt2M) {
      message.error('Image must smaller than 3MB!')
    }
    return isJpgOrPng && isLt2M
  }

  return (
    <Wrapper>
      <Upload
        name="image"
        disabled={disabled}
        listType="picture-card"
        className="picture-uploader"
        showUploadList={false}
        action={action}
        beforeUpload={beforeUpload}
        onChange={info => {
          if (info.file.status === 'loading') {
            setState({
              status: API_PROCESS.LOADING,
            })
          } else if (info.file.status === 'done') {
            setState({
              status: API_PROCESS.SUCCESS,
              url: info.file.response.Data,
            })
            onSuccess(JSON.parse(info.file.xhr.response))
          }
        }}
      >
        {state.url ? (
          <img src={state.url} style={{ width: 'inherit' }} />
        ) : (
          <div>
            <PlusOutlined />
            <div className="ant-upload-text">
              <Text type="secondary">{text}</Text>
            </div>
          </div>
        )}
      </Upload>
    </Wrapper>
  )
}
