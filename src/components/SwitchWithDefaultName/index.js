import React from "react";
import { Switch } from "antd";
import { FormattedMessage } from "react-intl";

export default function SwitchWithDefaultName({
  label = "IsEnable",
  isDisabled,
  ...props
}) {
  const isTypeValidated = typeof props.checked === "boolean";

  return (
    <Switch
      {...props}
      disabled={isDisabled}
      checked={isTypeValidated ? props.checked : true}
      checkedChildren={
        <FormattedMessage id={"Share.SwitchButton." + label + ".Yes"} />
      }
      unCheckedChildren={
        <FormattedMessage id={"Share.SwitchButton." + label + ".No"} />
      }
    />
  );
}
