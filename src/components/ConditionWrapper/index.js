import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 16px;
  background: #ffffff;
`;

export default function ConditionWrapper({ children, ...restProps }) {
  return <Wrapper {...restProps}>{children}</Wrapper>;
}
