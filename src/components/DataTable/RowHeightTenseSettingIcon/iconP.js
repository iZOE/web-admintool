import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const CustomP = styled.p`
  &:hover {
    background-color: #1790ff;
    cursor: pointer;
  }
`;

function IconP({ value, text, onUpdateSetting }) {
  const onIconClick = () => {
    onUpdateSetting(value);
  };

  return <CustomP onClick={onIconClick}>{text}</CustomP>;
}

IconP.propTypes = {
  value: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onUpdateSetting: PropTypes.func
};

export default IconP;
