import React from "react";
import { ColumnHeightOutlined } from "@ant-design/icons";
import { Popover, Tooltip, Button } from "antd";
import PropTypes from "prop-types";
import IconP from "./iconP";

function RowHeightTenseIcon({ onUpdateColumnHeightChoice, getPopupContainer }) {
  const renderColumnRowHeightChoiceJSXLiteral = () => (
    <>
      <IconP
        value="default"
        text="正常"
        onUpdateSetting={onUpdateColumnHeightChoice}
      />
      <IconP
        value="middle"
        text="中等"
        onUpdateSetting={onUpdateColumnHeightChoice}
      />
      <IconP
        value="small"
        text="緊湊"
        onUpdateSetting={onUpdateColumnHeightChoice}
      />
    </>
  );

  return (
    <Popover
      placement="bottomRight"
      content={renderColumnRowHeightChoiceJSXLiteral()}
      trigger="click"
      getPopupContainer={getPopupContainer}
    >
      <Tooltip
        placement="top"
        title="密度"
        getPopupContainer={getPopupContainer}
      >
        <Button type="link" size="large">
          <ColumnHeightOutlined />
        </Button>
      </Tooltip>
    </Popover>
  );
}

RowHeightTenseIcon.propTypes = {
  onUpdateColumnHeightChoice: PropTypes.func,
};

export default RowHeightTenseIcon;
