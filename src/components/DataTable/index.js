import React, { useMemo } from 'react';
import { useIntl } from 'react-intl';
import { SORT_DIRECTION } from 'constants/sortDirection';
import { PAGINATION_CONFIG } from 'constants/paginationConfig';
import { Table, Row, Col, Descriptions } from 'antd';
import { FormattedMessage } from 'react-intl';
import { Wrapper } from './Styled';

const { Desc, Asc } = SORT_DIRECTION;
const rowKeyMapper = d => d.Id;

export default function DataTable({
  displayResult,
  condition,
  title = <FormattedMessage id="Share.Table.SearchResult" />,
  loading,
  dataSource,
  config,
  extendArea,
  components,
  multipleSort,
  noHeader,
  rowSelection,
  rowKey,
  onRow,
  onUpdate,
  summary,
  total = 0,
  pagination,
  bordered = false,
}) {
  const intl = useIntl();

  const onTableChange = (pagination, _2, sorter, { action }) => {
    switch (action) {
      case 'sort':
        onUpdate({
          sortInfo: {
            SortColumn: sorter.columnKey,
            SortBy: sorter.order === 'descend' ? Desc : Asc,
          },
        });
        /**
         * 多重排序由後端處理 
        if (multipleSort) {
          // 一個排序是吐object, 兩個以上排序是吐array[object]
          let sortColumn = '';
          if (sorter.length) {
            sorter.map(sort => {
              sortColumn += ' ' + sort.columnKey;
              if (sort.order === 'descend') {
                sortColumn += ' desc,';
              } else {
                sortColumn += ' asc,';
              }
            });
            sortColumn = sortColumn.slice(1, sortColumn.length - 1);
            onUpdate({
              sortColumn
            });
          } else {
            if (sorter.column) {
              sortColumn += ' ' + sorter.columnKey;
              if (sorter.order === 'descend') {
                sortColumn += ' desc,';
              } else {
                sortColumn += ' asc,';
              }
              sortColumn = sortColumn.slice(1, sortColumn.length - 1);
              onUpdate({
                sortColumn
              });
            } else {
              onUpdate({
                sortColumn: null
              });
            }
          }
        } else {
          const sortInfo = {
            SortColumn: sorter.columnKey,
            SortBy: sorter.order === 'descend' ? Desc : Asc
          };
          onUpdate({
            sortInfo
          });
        }
        */
        break;
      case 'paginate':
        onUpdate({
          paginationInfo: {
            pageSize: pagination.pageSize,
            pageNumber: pagination.current,
          },
        });
        break;
      default:
        break;
    }
  };

  const paginationConfig = useMemo(() => {
    if (pagination === false) {
      return false;
    }
    const { paginationInfo } = condition || {};
    const { pageNumber } = paginationInfo || {};
    const result = {
      ...PAGINATION_CONFIG,
      total,
    };
    return pageNumber
      ? {
          ...result,
          current: pageNumber,
          ...pagination,
        }
      : { ...result, ...pagination };
  }, [condition, total, pagination]);

  return (
    <Wrapper>
      {!noHeader && (
        <div id="dt-header">
          <Row justify="space-around" align="middle">
            <Col span={12}>
              <Descriptions title={title} column={1} size="small" />
            </Col>
            <Col span={12}>
              <div id="dt-header-right">{extendArea && <div id="ex-area">{extendArea}</div>}</div>
            </Col>
          </Row>
        </div>
      )}
      <Table
        size="small"
        showSorterTooltip={false}
        bordered={bordered}
        columns={config}
        loading={loading}
        dataSource={dataSource}
        rowKey={rowKey || rowKeyMapper}
        pagination={paginationConfig}
        onChange={onTableChange}
        components={components}
        onRow={onRow}
        rowSelection={
          rowSelection && {
            columnWidth: 45,
            ...rowSelection,
          }
        }
        summary={summary}
        locale={{
          emptyText: displayResult
            ? intl.formatMessage({
                id: 'DataTable.NoData',
                defaultMessage: 'No data',
              })
            : intl.formatMessage({
                id: 'DataTable.PleaseSearch',
                defaultMessage: 'Please enter search',
              }),
        }}
      />
    </Wrapper>
  );
}
