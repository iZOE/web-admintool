import React, { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { SettingOutlined } from "@ant-design/icons";
import { Popover, Tooltip, Checkbox, Button } from "antd";
// import { useColumnsFilter } from "../../hooks/useColumnsFilter";
import PropTypes from "prop-types";
import styled from "styled-components";
import invariant from "invariant";

const VerticalFlexWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const NO_ROW_CHECKED = "NO_ROW_CHECKED";
const ALL_ROW_CHECKED = "ALL_ROW_CHECKED";
const SOME_ROW_CHECKED = "SOME_ROW_CHECKED";

const calculateRowShowCheckBoxCondition = columns => {
  const allRowCount = columns.length;
  const checkedColumnCount = columns.filter(c => c.isShow).length;
  if (checkedColumnCount === 0) {
    return NO_ROW_CHECKED;
  } else if (checkedColumnCount >= 1 && checkedColumnCount < allRowCount) {
    return SOME_ROW_CHECKED;
  }
  return ALL_ROW_CHECKED;
};

function ColumnSettingGearIcon({
  columnsMetaData,
  onUpdateSelectedColumns,
  getPopupContainer
}) {
  // const [columnFilterJSXLiteral, selectedColumns] = useColumnsFilter(
  //   columnsMetaData
  // );

  const [choosedColumns, setChoosedColumns] = useState(columnsMetaData);
  const [isAllRowShow, setIsAllRowShow] = useState(true);
  const [isAllRowShowCBIntermediate, setIsAllRowShowCBIntermediate] = useState(
    false
  );

  const toggleAllRowShow = evt => {
    setChoosedColumns(prev =>
      prev.map(c => {
        if (isAllRowShow) {
          return { ...c, isShow: false };
        } else {
          return { ...c, isShow: true };
        }
      })
    );

    setIsAllRowShow(prev => !prev);
    setIsAllRowShowCBIntermediate(prev => false);
  };

  const toggleSpecifiedColumnIsShow = dataIndex => evt => {
    const { checked } = evt.target;
    setChoosedColumns(prev =>
      prev.map(c => {
        if (c.dataIndex === dataIndex) {
          return { ...c, isShow: checked };
        }
        return { ...c };
      })
    );
  };

  useEffect(() => {
    onUpdateSelectedColumns(choosedColumns);
  }, [choosedColumns]);

  // update 全選的 checkbox
  useEffect(() => {
    const rowShowCBCheckedCondition = calculateRowShowCheckBoxCondition(
      choosedColumns
    );
    switch (rowShowCBCheckedCondition) {
      case NO_ROW_CHECKED:
        setIsAllRowShow(prev => false);
        setIsAllRowShowCBIntermediate(prev => false);
        break;
      case ALL_ROW_CHECKED:
        setIsAllRowShow(prev => true);
        setIsAllRowShowCBIntermediate(prev => false);
        break;
      case SOME_ROW_CHECKED:
        setIsAllRowShowCBIntermediate(prev => true);
        break;
      default:
        invariant(false, "something wrong");
    }
  }, [choosedColumns]);

  const renderCheckboxJSXLiteral = () => (
    <VerticalFlexWrapper>
      <Checkbox
        onChange={toggleAllRowShow}
        checked={isAllRowShow}
        indeterminate={isAllRowShowCBIntermediate}
      >
        <FormattedMessage
          id="Share.DataTable.Control.ColumnsList"
          defaultMessage="列展示"
        />
      </Checkbox>
      {choosedColumns
        .filter(c => !c.isManage)
        .map((c, idx) => (
          <Checkbox
            key={idx}
            onChange={toggleSpecifiedColumnIsShow(c.dataIndex)}
            checked={c.isShow}
          >
            {c.title}
          </Checkbox>
        ))}
    </VerticalFlexWrapper>
  );

  return (
    <Popover
      placement="bottomRight"
      content={renderCheckboxJSXLiteral()}
      trigger="click"
      getPopupContainer={getPopupContainer}
    >
      <Tooltip
        placement="top"
        title="栏设置"
        getPopupContainer={getPopupContainer}
      >
        <Button type="link" size="large">
          <SettingOutlined />
        </Button>
      </Tooltip>
    </Popover>
  );
}

ColumnSettingGearIcon.propTypes = {
  columnsMetaData: PropTypes.arrayOf(
    PropTypes.shape({
      // title: PropTypes.string.isRequired,
      dataIndex: PropTypes.string.isRequired,
      key: PropTypes.string.isRequired,
      isShow: PropTypes.bool.isRequired
    })
  ),
  onUpdateSelectedColumns: PropTypes.func
};

export default ColumnSettingGearIcon;
