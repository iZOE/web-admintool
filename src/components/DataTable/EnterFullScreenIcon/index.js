import React, { useState } from "react";
import { FullscreenOutlined, FullscreenExitOutlined } from "@ant-design/icons";
import { Tooltip, Button } from "antd";
import PropTypes from "prop-types";

function EnterFullScreenIcon({ getPopupContainer }) {
  const [isFullScreen, setIsFullScreen] = useState(false);

  function onEnterFullscreen() {
    const domNode = document.getElementById("FULL_SCREENABLE_AREA_WRAPPER_ID");
    if (domNode.requestFullscreen) {
      domNode.requestFullscreen();
    } else if (domNode.msRequestFullscreen) {
      domNode.msRequestFullscreen();
    } else if (domNode.mozRequestFullScreen) {
      domNode.mozRequestFullScreen();
    } else if (domNode.webkitRequestFullscreen) {
      domNode.webkitRequestFullscreen();
    }
  }

  return (
    <Tooltip placement="top" title="全屏" getPopupContainer={getPopupContainer}>
      <Button
        type="link"
        size="large"
        onClick={() => {
          if (document.fullscreenElement) {
            setIsFullScreen(false);
            document.exitFullscreen();
          } else {
            setIsFullScreen(true);
            onEnterFullscreen();
          }
        }}
      >
        {isFullScreen ? <FullscreenOutlined /> : <FullscreenExitOutlined />}
      </Button>
    </Tooltip>
  );
}

EnterFullScreenIcon.propTypes = {
  onEnterFullscreen: PropTypes.func
};

export default EnterFullScreenIcon;
