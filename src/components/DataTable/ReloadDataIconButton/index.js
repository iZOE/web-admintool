import React from "react";
import { Tooltip, Button } from "antd";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { ReloadOutlined } from "@ant-design/icons";

function ReloadDataIconButton({ onReload }) {
  if (!onReload) return null;
  return (
    <Tooltip
      placement="top"
      title={<FormattedMessage id="Share.ActionButton.Reload" />}
    >
      <Button type="link" size="large" onClick={onReload}>
        <ReloadOutlined />
      </Button>
    </Tooltip>
  );
}

ReloadDataIconButton.propTypes = {
  onReload: PropTypes.func
};

export default ReloadDataIconButton;
