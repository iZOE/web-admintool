import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #fff;
  #dt-header {
    padding: 8px;
    span.title {
      color: rgba(0, 0, 0, 0.85);
      font-size: 16px;
      line-height: 24px;
    }
    #dt-header-right {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      #dt-config {
        text-align: right;
        button {
          padding: 0 0 0 16px;
        }
      }
    }
  }
  .ant-descriptions-header {
    margin-bottom: 0;
  }
  .ant-table-pagination {
    &:first-child {
      margin-top: 0;
    }
  }
  .ant-table-content {
    tr .ant-table-cell:first-child {
      padding-left: 24px !important;
    }
  }
  #table-skeleton {
    padding: 16px;
  }
`;
