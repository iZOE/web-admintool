import React, { useMemo } from 'react'
import { FormattedMessage } from 'react-intl'
import { Skeleton, Tag, Table } from 'antd'
import DateWithFormat from 'components/DateWithFormat'

const COLUMN_CONFIG = [
    {
        title: (
            <FormattedMessage id="GroupDetail.Tabs.UserList.Areas.Column.Account" />
        ),
        dataIndex: 'Account',
        key: 'Account',
    },
    {
        title: (
            <FormattedMessage id="GroupDetail.Tabs.UserList.Areas.Column.CreatedOn" />
        ),
        dataIndex: 'CreatedOn',
        key: 'CreatedOn',
        render: (_1, { CreatedOn: time }, _2) => <DateWithFormat time={time} />,
    },
    {
        title: (
            <FormattedMessage id="GroupDetail.Tabs.UserList.Areas.Column.IsEnable" />
        ),
        dataIndex: 'IsEnable',
        key: 'IsEnable',
        render: IsEnable => (
            <Tag color={IsEnable ? 'processing' : 'warning'}>
                <FormattedMessage
                    id={`Share.Status.${IsEnable ? 'Enable' : 'Disable'}`}
                    defaultMessage={IsEnable ? '啟用' : '停用'}
                />
            </Tag>
        ),
    },
]

export default function UserList({ data }) {
    const isOnline = false
    const { Data, TotalCount } = data
    const dataSource = useMemo(
        () =>
            Data.map(item => ({
                ...item,
                key: item.Id,
            })),
        [Data]
    )

    const columns = useMemo(
        () => [
            ...COLUMN_CONFIG,
            {
                title: (
                    <FormattedMessage id="GroupDetail.Tabs.UserList.Areas.Column.IsLocked" />
                ),
                dataIndex: 'IsLocked',
                key: 'IsLocked',
                render: IsLocked => (
                    <Tag
                        color={
                            IsLocked
                                ? 'error'
                                : isOnline
                                ? 'success'
                                : 'default'
                        }
                    >
                        <FormattedMessage
                            id={`GroupDetail.Tabs.UserList.Areas.${
                                IsLocked
                                    ? 'UserLocked'
                                    : isOnline
                                    ? 'UserOnline'
                                    : 'UserOffline'
                            }`}
                        />
                    </Tag>
                ),
            },
        ],
        []
    )

    return dataSource ? (
        <Table
            dataSource={dataSource}
            columns={columns}
            pagination={{
                showTotal: () => (
                    <FormattedMessage
                        id="GroupDetail.Tabs.UserList.Areas.TotalCount"
                        values={{ count: TotalCount }}
                    />
                ),
                position: ['topRight', 'bottomRight'],
            }}
        />
    ) : (
        <Skeleton active />
    )
}
