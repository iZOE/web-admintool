import React from 'react';
import { Skeleton } from 'antd';
import GroupFunctionCascader from 'components/GroupFunctionCascader';
import { Wrapper } from './Styled';

export default function Permissions({ functionIds }) {
  return functionIds ? (
    <Wrapper>
      <GroupFunctionCascader functionPermissionIds={functionIds} />
    </Wrapper>
  ) : (
    <Skeleton active />
  );
}
