import styled from 'styled-components';

export const Wrapper = styled.div`
  .ant-list-item {
    &.selected {
      .ant-tree-checkbox-disabled + span {
        color: #1890ff;
      }
      .ant-checkbox-disabled + span {
        color: #1890ff;
      }
    }
    .ant-tree-checkbox-disabled + span {
      color: rgba(0, 0, 0, 0.65);
    }
    .ant-checkbox-disabled + span {
      color: rgba(0, 0, 0, 0.65);
    }
  }
`;
