import React, { useEffect, useMemo, Suspense, lazy, useState } from 'react';
import useSWR from 'swr';
import axios from 'axios';
import { Drawer, Tabs, Spin, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';
import { SORT_DIRECTION } from 'constants/sortDirection';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import optionsServices from 'services/optionsServices';

const {
  SETTINGS_GROUPS_VIEW,
  SETTINGS_GROUPS_CREATE,
  SETTINGS_GROUPS_EDIT_INFO,
  SETTINGS_GROUPS_CHANGE_STATUS,
  SETTINGS_GROUPS_FUNCTIONS
} = PERMISSION;

const { getTopGroupOptions } = optionsServices();

const BasicDataAsync = lazy(() => import('./BasicData'));
const PermissionsAsync = lazy(() => import('./Permissions'));
const ChecksAsync = lazy(() => import('./Checks'));
const UserListAsync = lazy(() => import('./UserList'));
const ChangeLogAsync = lazy(() => import('./ChangeLog'));
const { TabPane } = Tabs;

export default function GroupDetail({ groupId, showGroupDetail, onClose }) {
  const DEFAULT_TAB = 'basicData';
  const [currentTab, setCurrentTab] = useState(DEFAULT_TAB);
  const [topGroupOptions, setTopGroupOptions] = useState(null);

  let { data: detail } = useSWR(groupId ? `/api/Group/${groupId}` : null, url =>
    axios(url).then(res => res && res.data.Data)
  );

  let { data: userList } = useSWR(
    groupId ? '/api/User/UsersInGroup' : null,
    url =>
      axios({
        url,
        method: 'post',
        data: {
          GroupId: groupId,
          paginationInfo: { pageNumber: 1, pageSize: 1000000 },
          sortInfo: { SortBy: SORT_DIRECTION.Asc, SortColumn: '' }
        }
      }).then(res => res && res.data.Data)
  );

  let { data: functionIds } = useSWR(
    groupId ? `/api/Group/FunctionPermission/${groupId}` : null,
    url => axios(url).then(res => res && res.data.Data)
  );

  let { data: approvedTabData } = useSWR(
    groupId ? `/api/Group/FunctionPermissionApproved/${groupId}` : null,
    url => axios(url).then(res => res && res.data.Data)
  );

  useEffect(() => {
    if (detail) {
      getTopGroupOptions({
        selfGroupId: groupId || -1,
        oldGroupId: -1,
        agentId: detail.AgentId
      }).then(res =>
        setTopGroupOptions(res.reduce((p, c) => ({ ...p, [c.Id]: c.Name }), {}))
      );
    }
  }, [detail]);

  const checksProps = useMemo(() => {
    if (approvedTabData) {
      return {
        groupId,
        topGroupsName: topGroupOptions,
        parentGroupId: approvedTabData.ParentGroupId,
        approveds: approvedTabData.ApprovedList.map(item => ({
          ...item,
          key: item.FunctionId
        }))
      };
    }
  }, [approvedTabData, topGroupOptions]);

  function onCloseTab() {
    detail = null;
    userList = null;
    approvedTabData = null;
    functionIds = null;
    onClose();
  }

  return (
    <Drawer
      title={
        <Tabs
          defaultActiveKey={DEFAULT_TAB}
          onChange={key => {
            setCurrentTab(key);
          }}
        >
          <TabPane
            tab={<FormattedMessage id="GroupDetail.Tabs.BasicData.Title" />}
            key="basicData"
          />
          <TabPane
            tab={<FormattedMessage id="GroupDetail.Tabs.Permissions.Title" />}
            key="permissions"
          />
          <TabPane
            tab={<FormattedMessage id="GroupDetail.Tabs.Checks.Title" />}
            key="checks"
          />
          <TabPane
            tab={<FormattedMessage id="GroupDetail.Tabs.UserList.Title" />}
            key="userList"
          />
          <TabPane
            tab={<FormattedMessage id="GroupDetail.Tabs.ChangeLog.Title" />}
            key="changeLog"
          />
        </Tabs>
      }
      placement="right"
      headerStyle={{ paddingBottom: 0, border: 'none' }}
      width={960}
      closable={false}
      onClose={onCloseTab}
      visible={showGroupDetail}
    >
      <Permission isPage functionIds={[SETTINGS_GROUPS_VIEW]}>
        <Suspense fallback={<Spin />}>
          {!!(currentTab === 'basicData') && <BasicDataAsync detail={detail} />}

          {!!(currentTab === 'userList') && <UserListAsync data={userList} />}

          {!!(currentTab === 'checks') && <ChecksAsync {...checksProps} />}

          {!!(currentTab === 'permissions') && (
            <PermissionsAsync functionIds={functionIds} />
          )}
          {!!(currentTab === 'changeLog') && (
            <ChangeLogAsync groupId={groupId} />
          )}
        </Suspense>
      </Permission>
    </Drawer>
  );
}
