import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Skeleton, Tag, Table, Typography } from 'antd';

const { Text, Link } = Typography;

export default function Checks({
  groupId,
  topGroupsName,
  parentGroupId,
  approveds
}) {
  const COLUMN_CONFIG = [
    {
      title: (
        <FormattedMessage id="GroupDetail.Tabs.Checks.Areas.Column.ApprovedName" />
      ),
      dataIndex: 'FunctionName',
      key: 'FunctionName',
      width: 200
    },
    {
      title: (
        <FormattedMessage id="GroupDetail.Tabs.Checks.Areas.Column.ApprovedType" />
      ),
      dataIndex: 'ApprovedType',
      key: 'ApprovedType',
      render: (ApprovedType, record) => {
        switch (ApprovedType) {
          case 0:
            return (
              <Tag color="blue">
                <FormattedMessage
                  id={`PageSystemGroups.Form.ApprovedOptions.${ApprovedType}`}
                />
              </Tag>
            );
          case 1:
            return (
              <Tag color="cyan">
                <FormattedMessage
                  id={`PageSystemGroups.Form.ApprovedOptions.${ApprovedType}`}
                />{' '}
                - {topGroupsName[record.CountersignGroupId]}
              </Tag>
            );
          case 2:
            return (
              <Tag color="warning">
                <FormattedMessage
                  id={`PageSystemGroups.Form.ApprovedOptions.${ApprovedType}`}
                />
              </Tag>
            );
          default:
            return (
              <>
                <Text type="danger">
                  <FormattedMessage id="PageSystemGroups.Form.NoApprovedOptions" />
                </Text>{' '}
                <Link href={`/settings/groups/detail/${groupId}?tab=checks`}>
                  (<FormattedMessage id="Share.CommonKeys.GoSetting" />)
                </Link>
              </>
            );
        }
      }
    },
    {
      title: (
        <FormattedMessage id="GroupDetail.Tabs.Checks.Areas.Column.Restrict" />
      ),
      dataIndex: 'Constraint',
      key: 'Constraint',
      width: 180
    }
  ];

  return approveds ? (
    <Table
      pagination={false}
      columns={COLUMN_CONFIG}
      dataSource={approveds}
      title={() =>
        parentGroupId ? (
          <FormattedMessage
            id="GroupDetail.Tabs.Checks.Areas.Column.TopGroupsName"
            values={{ name: topGroupsName[parentGroupId] }}
          />
        ) : null
      }
    />
  ) : (
    <Skeleton active />
  );
}
