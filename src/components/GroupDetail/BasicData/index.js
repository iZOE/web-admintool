import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Descriptions, Divider, PageHeader, Skeleton, Tag } from 'antd'
import DateWithFormat from 'components/DateWithFormat'

export default function BasicData({ detail }) {
    return detail ? (
        <PageHeader>
            <Descriptions column={2}>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.Name" />
                    }
                >
                    {detail.Name}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.Id" />
                    }
                >
                    {detail.Id}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.AgentName" />
                    }
                >
                    {detail.AgentName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.AgentId" />
                    }
                >
                    {detail.AgentId}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.ParentGroupName" />
                    }
                >
                    {detail.ParentGroupName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.Status" />
                    }
                >
                    <Tag color={detail.IsEnable ? 'processing' : 'warning'}>
                        <FormattedMessage
                            id={`Share.Status.${
                                detail.IsEnable ? 'Enable' : 'Disable'
                            }`}
                        />
                    </Tag>
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.CreatedOn" />
                    }
                >
                    <DateWithFormat time={detail.CreatedOn} />
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.CreatedBy" />
                    }
                >
                    {detail.CreatedByName}
                </Descriptions.Item>
                <Descriptions.Item
                    span={2}
                    label={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Areas.Memo" />
                    }
                >
                    {detail.Memo}
                </Descriptions.Item>
            </Descriptions>
            <Divider dashed />{' '}
        </PageHeader>
    ) : (
        <Skeleton active />
    )
}
