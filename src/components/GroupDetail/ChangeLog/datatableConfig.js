import React from 'react'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import { NO_DATA } from 'constants/noData'

const getFormattedDetail = detail => {
    return detail.replace(/\n/g, '<br/>')
}

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'ModifiedOn',
        key: 'ModifiedOn',
        width: 200,
        render: (_1, { ModifiedOn: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.Modifier"
                description="異動人員"
            />
        ),
        dataIndex: 'ModifiedByName',
        key: 'ModifiedByName',
        width: 100,
        render: name => (name ? name : NO_DATA),
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ChangeItem"
                description="異動項目"
            />
        ),
        dataIndex: 'Column',
        key: 'Column',
        width: 100,
    },
    {
        title: <FormattedMessage id="Share.Fields.Detail" description="明細" />,
        dataIndex: 'Value',
        key: 'Value',
        render: detail => {
            return detail ? (
                <span
                    dangerouslySetInnerHTML={{
                        __html: getFormattedDetail(detail),
                    }}
                ></span>
            ) : (
                NO_DATA
            )
        },
    },
]
