import React from 'react';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import Condition from './Condition';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import { COLUMNS_CONFIG } from './datatableConfig';

export default function ChangeLog({ groupId }) {
  const { dataSource, fetching, onUpdateCondition, condition } = useGetDataSourceWithSWR({
    url: `/api/Group/Log/${groupId}`,
    defaultSortKey: 'ModifiedOn',
  });

  return (
    <ReportScaffold
      displayResult={condition}
      conditionComponent={<Condition onUpdate={onUpdateCondition} />}
      conditionHasCollapseWrapper={false}
      datatableComponent={
        <DataTable
          displayResult={condition}
          dataSource={dataSource && dataSource}
          loading={fetching}
          config={COLUMNS_CONFIG}
          pagination={false}
        />
      }
    />
  );
}
