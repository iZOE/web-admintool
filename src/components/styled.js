import styled from "styled-components";

export const GenericHorizontalFlexWrapper = styled.div`
  display: flex;
  align-items: center;
  align-content: space-around;
  margin-right: 1em;
  margin-bottom: 1.2em;
`;

export const GenericTextWrapper = styled.div`
  color: #000000;
  font-size: 14px;
  margin-right: 0.2em;
`;
