import React from "react";
import { Select } from "antd";
import invariant from "invariant";
import { GenericHorizontalFlexWrapper, GenericTextWrapper } from "../styled";

const { Option } = Select;

const transToOptions = (data, transformer) => data.map(d => transformer(d));

const SelectorWithAttributeName = props => {
  const {
    data,
    name,
    title,
    transformer,
    defaultValue,
    setFieldValue,
    handleBlur,
    ...rest
  } = props;

  invariant(
    handleBlur,
    "請傳入formik的handleBlur, 否則antd的下拉選單在onBlur時formilk會拋錯"
  );

  const optionsData = transToOptions(data, transformer);
  const selectorInitValue =
    defaultValue || (optionsData[0] && optionsData[0].displayText) || "無資料";
  const handleChange = v => {
    setFieldValue(`${name}`, v);
  };
  const patchBlurChange = v => {
    handleBlur(`${name}`, { currentTarget: { value: v } });
  };

  return (
    <GenericHorizontalFlexWrapper>
      <GenericTextWrapper style={{ width: "40px" }}>
        {" "}
        {title}{" "}
      </GenericTextWrapper>
      <Select
        defaultValue={selectorInitValue}
        onChange={handleChange}
        onBlur={patchBlurChange}
        // style={{ width: "120px" }}
        {...rest}
      >
        {optionsData.map(({ disabled, value, displayText }) => {
          if (disabled) {
            return (
              <Option disabled key={value} value={value}>
                {displayText}
              </Option>
            );
          }
          return (
            <Option key={value} value={value}>
              {displayText}
            </Option>
          );
        })}
      </Select>
    </GenericHorizontalFlexWrapper>
  );
};

export default SelectorWithAttributeName;
