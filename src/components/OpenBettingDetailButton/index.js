import React, { useContext } from 'react';
import { Typography } from 'antd';
import { ThirdpartyBettingDetailContext } from 'contexts/ThirdpartyBettingDetailContext';

const { Link } = Typography;

export default function OpenBettingDetailButton({ children, betOrderNo }) {
  const { onDisplayBettingDetail } = useContext(ThirdpartyBettingDetailContext);
  function onOpenBettingDetail() {
    onDisplayBettingDetail(betOrderNo);
  }
  return <Link onClick={onOpenBettingDetail}>{children}</Link>;
}
