import React from "react";
import { Select, Spin } from "antd";
import { FormattedMessage } from "react-intl";

export default function ListSelect({ list, ...props }) {
  return list ? (
    <Select {...props}>
      {list.map(({ Id, Name }) => (
        <Select.Option key={Id} value={Id}>
          {Name}
        </Select.Option>
      ))}
    </Select>
  ) : (
    <Spin />
  );
}
