import React from 'react';
import ReportPageWrapper from 'layouts/ReportPage';
import ConditionWrapper from 'layouts/ReportPage/Condition';
import DrawerConditionWrapper from 'layouts/Drawer/Condition';
import SummaryWrapper from 'layouts/ReportPage/Summary';

export default function ReportScaffold({
  displayResult,
  conditionComponent: c,
  conditionHasCollapseWrapper = true,
  summaryComponent,
  datatableComponent
}) {
  return (
    <ReportPageWrapper>
      {c &&
        (conditionHasCollapseWrapper ? (
          <ConditionWrapper>{c}</ConditionWrapper>
        ) : (
          <DrawerConditionWrapper>{c}</DrawerConditionWrapper>
        ))}
      {summaryComponent && (
        <SummaryWrapper summaryResult={displayResult}>
          {summaryComponent}
        </SummaryWrapper>
      )}
      {datatableComponent}
    </ReportPageWrapper>
  );
}
