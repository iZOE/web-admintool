import styled from 'styled-components';

export const Wrapper = styled.div`
  .ant-card {
    margin-bottom: 24px;
    .ant-card-body {
      display: flex;
      flex-direction: row;
      padding: 0;
      .ant-card-grid {
        padding: 0;
        float: unset;
        width: initial;
        flex: 1;
        box-shadow: 1px 0 0 0 #f0f0f0;
        &:last-child {
          box-shadow: unset;
        }
        .ant-list {
          .ant-list-item {
            cursor: pointer;
            &.selected {
              background-color: #e6f7ff;
              color: #1890ff;
            }
            .ant-form-item {
              margin: 0;
            }
          }
          &:not(.list-functions) {
            .ant-list-item {
              &:hover {
                color: #1890ff;
                box-shadow: 0 6px 16px -8px rgba(0, 0, 0, 0.08),
                  0 9px 28px 0 rgba(0, 0, 0, 0.05),
                  0 12px 48px 16px rgba(0, 0, 0, 0.03);
              }
            }
          }
        }
      }
    }
  }
`;
