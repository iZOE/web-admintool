import React, { useEffect, useMemo, useState } from 'react';
import useSWR from 'swr';
import axios from 'axios';
import { Card, Checkbox, List } from 'antd';
import { CaretRightOutlined } from '@ant-design/icons';
import TreeCheckBox from 'components/TreeCheckBox';
import { Wrapper } from './Styled';

export default function GroupFunctionCascader({
  value,
  functionPermissionIds,
  onChange
}) {
  const [functionIds, setFunctionIds] = useState(new Set([]));
  const [categoryId, setCategoryId] = useState(null);
  const [moduleId, setModuleId] = useState(null);
  const [isViewMode] = useState(!onChange);
  const [selectedItem, setSelectedItem] = useState({
    category: null,
    module: null
  });

  const { data: functionData } = useSWR('/api/Menu/Function', url =>
    axios(url).then(res => res && res.data.Data)
  );

  useEffect(() => {
    setFunctionIds(new Set(value || functionPermissionIds));
  }, [functionPermissionIds, value]);

  /**
   * @typedef {{Id: number, Name: string, Check: 0 | 1 | 2}[]} Category
   * @typedef {{[key: number]: {Id: number, Name: string, Check: 0 | 1 | 2}}} Module
   * @typedef {{[key: number]: {Id: number, Name: string, NeedApproved: boolean, ApprovedRemark: string}}} Functions
   * @type {{category: Category, module: Module, functions: Functions}}
   *
   * 將 functionData 轉換成 dataSource
   * 切分成三層
   */

  const { category, module, functions } = useMemo(() => {
    if (functionData) {
      return functionData.reduce(
        ({ category, module, functions }, current) => {
          let checkLength = [0, 0];
          module[current.Id] = current.SubMenus.map(menu => {
            const { Id, Name, Functions } = menu;
            let functionCheckLength = 0;
            functions[Id] = Functions.map(fun => {
              functionCheckLength += functionIds.has(fun.FunctionId) ? 1 : 0;
              return {
                Check: functionIds.has(fun.FunctionId),
                ...fun
              };
            });
            checkLength[0] += functionCheckLength;
            checkLength[1] += Functions.length;
            return {
              Id,
              Name,
              Check: functionCheckLength
                ? functionCheckLength < Functions.length
                  ? 1
                  : 2
                : 0
            };
          });
          category.push({
            Id: current.Id,
            Name: current.Name,
            Check: checkLength[0]
              ? checkLength[0] < checkLength[1]
                ? 1
                : 2
              : 0
          });
          return { category, module, functions };
        },
        {
          category: [],
          module: {},
          functions: {}
        }
      );
    }
    return {
      category: [],
      module: {},
      functions: {}
    };
  }, [functionIds, functionData]);

  const onCheckAllModule = (id, status) => {
    module[id].forEach(m => {
      functions[m.Id].forEach(item => {
        status === 2
          ? functionIds.delete(item.FunctionId)
          : functionIds.add(item.FunctionId);
      });
    });
    onChange && onChange(Array.from(functionIds));
  };

  const onCheckAllFunction = (id, status) => {
    functions[id].forEach(item => {
      status === 2
        ? functionIds.delete(item.FunctionId)
        : functionIds.add(item.FunctionId);
    });
    onChange && onChange(Array.from(functionIds));
  };

  const onFunctionCheckChange = (id, { target }) => {
    target.checked ? functionIds.add(id) : functionIds.delete(id);
    onChange && onChange(Array.from(functionIds));
  };

  return (
    <Wrapper>
      <Card>
        {category.length && (
          <Card.Grid hoverable={false}>
            <List
              className="list-category"
              size="small"
              dataSource={category}
              renderItem={item => (
                <List.Item
                  className={
                    item.Id === selectedItem.category ? 'selected' : ''
                  }
                  key={item.Id}
                  extra={<CaretRightOutlined />}
                  onClick={() => {
                    if (categoryId !== item.Id) {
                      setSelectedItem(prev => {
                        prev.category = item.Id;
                        return prev;
                      });
                      setCategoryId(item.Id);
                      setModuleId(null);
                    }
                  }}
                >
                  <TreeCheckBox
                    disabled={isViewMode}
                    label={item.Name}
                    checkStatus={item.Check}
                    onClick={() => onCheckAllModule(item.Id, item.Check)}
                  />
                </List.Item>
              )}
            />
          </Card.Grid>
        )}
        {(categoryId || categoryId === 0) && (
          <Card.Grid hoverable={false}>
            <List
              className="list-module"
              size="small"
              dataSource={module[categoryId]}
              renderItem={item => (
                <List.Item
                  className={item.Id === selectedItem.module ? 'selected' : ''}
                  key={item.Id}
                  extra={<CaretRightOutlined />}
                  onClick={() => {
                    if (moduleId !== item.Id) {
                      setSelectedItem(prev => {
                        prev.module = item.Id;
                        return prev;
                      });
                      setModuleId(item.Id);
                    }
                  }}
                >
                  <TreeCheckBox
                    disabled={isViewMode}
                    label={item.Name}
                    checkStatus={item.Check}
                    onClick={() => onCheckAllFunction(item.Id, item.Check)}
                  />
                </List.Item>
              )}
            />
          </Card.Grid>
        )}
        {(moduleId || moduleId === 0) && (
          <Card.Grid hoverable={false}>
            <List
              className="list-functions"
              size="small"
              dataSource={functions[moduleId]}
              renderItem={item => (
                <List.Item key={item.Id}>
                  <Checkbox
                    disabled={isViewMode}
                    checked={item.Check}
                    onChange={event =>
                      onFunctionCheckChange(item.FunctionId, event)
                    }
                  >
                    {item.Name}
                  </Checkbox>
                </List.Item>
              )}
            />
          </Card.Grid>
        )}
      </Card>
    </Wrapper>
  );
}
