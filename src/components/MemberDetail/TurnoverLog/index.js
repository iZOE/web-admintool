import React, { useContext } from "react";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import TurnoverDetail from "components/TurnoverDetail";

export default function TurnoverLog() {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberName = m && m.memberData.name;

  return <TurnoverDetail memberName={memberName} />;
}
