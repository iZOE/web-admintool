import React, { useContext } from 'react';
import { MemberDetailContext } from 'contexts/MemberDetailContext';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import Condition from './Condition';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import { COLUMNS_CONFIG } from './datatableConfig';

export default function ChangeLog() {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;

  const { dataSource, fetching, onUpdateCondition, condition } = useGetDataSourceWithSWR({
    url: `/api/Member/Log/${memberId}`,
    defaultSortKey: 'ModifiedOn',
  });

  return (
    <ReportScaffold
      displayResult={condition}
      conditionComponent={<Condition onUpdate={onUpdateCondition} memberName={memberId} />}
      conditionHasCollapseWrapper={false}
      datatableComponent={
        <DataTable
          displayResult={condition}
          dataSource={dataSource && dataSource}
          loading={fetching}
          config={COLUMNS_CONFIG}
          pagination={false}
        />
      }
    />
  );
}
