import React, { useState, useEffect, useContext } from 'react'
import {
    DatePicker,
    Descriptions,
    Divider,
    Form,
    Table,
    Skeleton,
    Row,
    Col,
} from 'antd'
import { FormattedMessage } from 'react-intl'
import { DEFAULT, FORMAT } from 'constants/dateConfig'
import { getISODateTimeString } from 'mixins/dateTime'
import caller from 'utils/fetcher'
import { Wrapper } from './Styled'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import QueryButton from 'components/FormActionButtons/Query'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import { NO_DATA } from 'constants/noData'

const { RangePicker } = DatePicker

export default function SummarizedLog() {
    const { memberIdentity: m } = useContext(MemberDetailContext)
    const memberId = m && m.memberData.id
    const [condition, setCondition] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [detail, setDetail] = useState(null)
    const [gameLog, setGameLog] = useState(null)

    const COLUMN_CONFIG = [
        {
            title: (
                <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.GameProvider" />
            ),
            dataIndex: 'GameProviderText',
            key: 'GameProviderText',
            render: GameProviderText =>
                GameProviderText ? GameProviderText : NO_DATA,
        },
        {
            title: (
                <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.TotalBetAmount" />
            ),
            dataIndex: 'TotalBetAmount',
            key: 'TotalBetAmount',
            render: TotalBetAmount => (
                <ReactIntlCurrencyWithFixedDecimal value={TotalBetAmount} />
            ),
        },
        {
            title: (
                <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.TotalCount" />
            ),
            dataIndex: 'TotalCount',
            key: 'TotalCount',
            render: TotalCount => (
                <ReactIntlCurrencyWithFixedDecimal value={TotalCount} />
            ),
        },
        {
            title: (
                <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.TotalWinLossAmount" />
            ),
            dataIndex: 'TotalWinLossAmount',
            key: 'TotalWinLossAmount',
            render: TotalWinLossAmount => (
                <ReactIntlCurrencyWithFixedDecimal value={TotalWinLossAmount} />
            ),
        },
    ]

    function onFinish(values) {
        setIsLoading(true)
        const [startDate, endDate] = values.dateRange
        setCondition({
            memberId,
            startDate: getISODateTimeString(startDate),
            endDate: getISODateTimeString(endDate),
            isSearchTeamMember: false,
        })
    }

    useEffect(() => {
        if (!!condition) {
            caller({
                method: 'post',
                endpoint: '/api/Member/IntegratedRecord',
                body: condition,
            })
                .then(res => {
                    if (res) {
                        setDetail(res.Data)
                        setIsLoading(false)
                    }
                })
                .catch(err => {
                    setIsLoading(false)
                })
        }
    }, [condition])

    useEffect(() => {
        if (!!detail) {
            let data = detail.GameAmounts.map((item, key) => {
                return {
                    key,
                    GameProviderText: item.Remark
                        ? item.Remark
                        : item.GameProviderName,
                    ...item,
                }
            })
            setGameLog(data)
        }
    }, [detail])

    return (
        <Wrapper>
            {!!memberId ? (
                <>
                    <Descriptions
                        title={
                            <FormattedMessage id="Share.QueryCondition.Title" />
                        }
                        column={1}
                    >
                        <Descriptions.Item>
                            <Form onFinish={onFinish}>
                                <Row gutter={24}>
                                    <Col span={20}>
                                        <Form.Item
                                            label={
                                                <FormattedMessage id="Share.QueryCondition.DateRange" />
                                            }
                                            name="dateRange"
                                            initialValue={
                                                DEFAULT.RANGE
                                                    .FROM_A_QUARTER_TO_TODAY
                                            }
                                        >
                                            <RangePicker
                                                bordered={false}
                                                format={FORMAT.DISPLAY.DAILY}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col span={2}>
                                        <QueryButton loading={isLoading} />
                                    </Col>
                                </Row>
                            </Form>
                        </Descriptions.Item>
                    </Descriptions>
                    <Divider orientation="left">
                        {!detail && (
                            <FormattedMessage id="Share.QueryCondition.Divider" />
                        )}
                    </Divider>
                    {detail && (
                        <>
                            <Descriptions
                                title={
                                    <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Areas.AccountLog" />
                                }
                                column={2}
                            >
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.MemberLoginCount" />
                                    }
                                >
                                    {detail.MemberLoginCount}
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.MemberLoginFaliCount" />
                                    }
                                >
                                    {detail.MemberLoginFaliCount}
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.MemberChangePwdCount" />
                                    }
                                >
                                    {detail.MemberChangePwdCount}
                                </Descriptions.Item>
                            </Descriptions>
                            <Divider dashed />
                            <Descriptions
                                title={
                                    <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Areas.CapitalLog" />
                                }
                                column={2}
                            >
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.DepositRequestAmountTotal" />
                                    }
                                >
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={detail.DepositRequestAmountTotal}
                                    />
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.DepositCount" />
                                    }
                                >
                                    {detail.DepositCount}
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.WithdrawalRequestAmountTotal" />
                                    }
                                >
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={
                                            detail.WithdrawalRequestAmountTotal
                                        }
                                    />
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.WithdrawalCount" />
                                    }
                                >
                                    {detail.WithdrawalCount}
                                </Descriptions.Item>
                            </Descriptions>
                            <Divider dashed />
                            <Descriptions
                                title={
                                    <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Areas.Promotion" />
                                }
                                column={2}
                            >
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.PreferentialTotal" />
                                    }
                                >
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={detail.PreferentialTotal}
                                    />
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.PersonalReturnPoint" />
                                    }
                                >
                                    {detail.PersonalReturnPoint}
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.AffilateReturnPoint" />
                                    }
                                >
                                    {detail.AffilateReturnPoint}
                                </Descriptions.Item>
                            </Descriptions>
                            <Divider dashed />
                            <Descriptions
                                title={
                                    <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Areas.AffiliateReward" />
                                }
                                column={2}
                            >
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.RealsommisionAmount" />
                                    }
                                >
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={detail.RealsommisionAmount}
                                    />
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.DailyAmount" />
                                    }
                                >
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={detail.DailyAmount}
                                    />
                                </Descriptions.Item>
                                <Descriptions.Item
                                    label={
                                        <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Fields.OtherAmount" />
                                    }
                                >
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={detail.OtherAmount}
                                    />
                                </Descriptions.Item>
                            </Descriptions>
                            <Divider dashed />
                            <Descriptions
                                title={
                                    <FormattedMessage id="MemberDetail.Tabs.SummarizedLog.Areas.GameLog" />
                                }
                            >
                                <Descriptions.Item className="merged-column">
                                    {!!gameLog ? (
                                        <Table
                                            columns={COLUMN_CONFIG}
                                            dataSource={gameLog}
                                            pagination={false}
                                            size="middle"
                                            style={{ width: '100%' }}
                                        />
                                    ) : (
                                        <Skeleton />
                                    )}
                                </Descriptions.Item>
                            </Descriptions>
                        </>
                    )}
                </>
            ) : (
                <Skeleton />
            )}
        </Wrapper>
    )
}
