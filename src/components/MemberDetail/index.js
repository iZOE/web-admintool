import React, { Suspense, lazy, useState } from "react";
import { Drawer, Spin } from "antd";
import { TAB_INFO } from "constants/memberDetail/tabInfo";
import { Wrapper } from "./Styled";
import TabHeader from "./TabHeader";
import MemberIdentity from "./MemberIdentity";

const BasicDataAsync = lazy(() => import("./BasicData"));
const BankDataAsync = lazy(() => import("./BankData"));
const AccountSettingAsync = lazy(() => import("./AccountSetting"));
const AccountSecurityAsync = lazy(() => import("./AccountSecurity"));
const TurnoverLogAsync = lazy(() => import("./TurnoverLog"));
const SummarizedLogAsync = lazy(() => import("./SummarizedLog"));
const ChangeLogAsync = lazy(() => import("./ChangeLog"));

export default function MemberDetail({
  memberBasicDetail,
  showMemberDetail,
  tab,
  onClose
}) {
  const DEFAULT_TAB = TAB_INFO.BASIC_DATA;
  const [currentTab, setCurrentTab] = useState(tab || DEFAULT_TAB);

  const CURRENT_TAB_COMPONENT = {
    BasicData: memberBasicDetail && (
      <BasicDataAsync detail={memberBasicDetail} />
    ),
    BankData: <BankDataAsync />,
    AccountSetting: memberBasicDetail && (
      <AccountSettingAsync detail={memberBasicDetail} />
    ),
    AccountSecurity: <AccountSecurityAsync />,
    TurnoverLog: <TurnoverLogAsync />,
    SummarizedLog: <SummarizedLogAsync />,
    ChangeLog: <ChangeLogAsync />
  };

  return (
    <Drawer
      title={
        <>
          <MemberIdentity />
          <TabHeader
            currentTab={currentTab}
            onSelectTab={key => setCurrentTab(key)}
          />
        </>
      }
      placement="right"
      headerStyle={{ paddingBottom: 0, border: "none" }}
      footer
      width={720}
      closable={false}
      onClose={onClose}
      visible={showMemberDetail}
    >
      <Wrapper>
        <Suspense fallback={<Spin />}>
          {memberBasicDetail && CURRENT_TAB_COMPONENT[currentTab]}
        </Suspense>
      </Wrapper>
    </Drawer>
  );
}
