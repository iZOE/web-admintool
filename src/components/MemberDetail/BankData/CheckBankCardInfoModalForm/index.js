import React, { useState, useContext } from 'react'
import { Modal, Form, Row, Col, Input, Button, message } from 'antd'
import { VerifiedOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'
import { MemberDetailContext } from 'contexts/MemberDetailContext'

export default function CheckBankCardInfoModalForm() {
    const { memberIdentity: m } = useContext(MemberDetailContext)
    const memberId = m && m.memberData.id
    const [isVisible, setIsVisible] = useState(false)
    const [form] = Form.useForm()
    const intl = useIntl()

    const VERIFY_FIELD = {
        ACCOUNT_NAME: 1,
        CARD_NUMBER: 2,
        WALLET_ADDRESS: 3,
    }

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.DataCorrect',
    })
    const errorMsg = intl.formatMessage({
        id: 'Share.ErrorMessage.DataIncorrect',
    })

    const onFinish = value => {
        const payload = {
            ...value,
            MemberId: memberId,
        }
        caller({
            method: 'post',
            endpoint: `/api/Member/MemberBank/VerifyCard`,
            body: payload,
        }).then(res => {
            handleFeedback(res)
            form.resetFields()
        })
    }

    const handleFeedback = response => {
        if (response.Status) {
            return response.Data
                ? message.success(successMsg)
                : message.error(errorMsg)
        } else {
            return message.warning(response.Message)
        }
    }

    return (
        <>
            <Button
                type="primary"
                icon={<VerifiedOutlined style={{ marginRight: 8 }} />}
                onClick={() => {
                    setIsVisible(true)
                }}
            >
                <FormattedMessage id="MemberDetail.Tabs.BankData.Actions.Check" />
            </Button>

            <Modal
                title={
                    <FormattedMessage id="MemberDetail.Tabs.BankData.Areas.BankCardCheck" />
                }
                visible={isVisible}
                onCancel={() => setIsVisible(false)}
                footer={null}
            >
                <Form form={form}>
                    <Row gutter={24}>
                        <Col span={16}>
                            <Form.Item
                                labelCol={{ span: 8 }}
                                label={
                                    <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.CardNumber" />
                                }
                                name="CardNumber"
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item>
                                <Button
                                    type="primary"
                                    onClick={() =>
                                        onFinish({
                                            CardNumber: form.getFieldValue(
                                                'CardNumber'
                                            ),
                                            VerifyWith:
                                                VERIFY_FIELD.CARD_NUMBER,
                                        })
                                    }
                                >
                                    <FormattedMessage id="MemberDetail.Tabs.BankData.Actions.Check" />
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={24}>
                        <Col span={16}>
                            <Form.Item
                                labelCol={{ span: 8 }}
                                label={
                                    <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.AccountName" />
                                }
                                name="AccountName"
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item>
                                <Button
                                    type="primary"
                                    onClick={() =>
                                        onFinish({
                                            AccountName: form.getFieldValue(
                                                'AccountName'
                                            ),
                                            VerifyWith:
                                                VERIFY_FIELD.ACCOUNT_NAME,
                                        })
                                    }
                                >
                                    <FormattedMessage id="MemberDetail.Tabs.BankData.Actions.Check" />
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={16}>
                            <Form.Item
                                labelCol={{ span: 8 }}
                                label={
                                    <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.WalletAddress" />
                                }
                                name="WalletAddress"
                            >
                                <Input />
                            </Form.Item>
                        </Col>

                        <Col span={8}>
                            <Form.Item>
                                <Button
                                    type="primary"
                                    onClick={() =>
                                        onFinish({
                                            WalletAddress: form.getFieldValue(
                                                'WalletAddress'
                                            ),
                                            VerifyWith:
                                                VERIFY_FIELD.WALLET_ADDRESS,
                                        })
                                    }
                                >
                                    <FormattedMessage id="MemberDetail.Tabs.BankData.Actions.Check" />
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    )
}
