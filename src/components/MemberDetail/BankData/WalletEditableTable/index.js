import React, { useState, useMemo, useContext } from 'react';
import { trigger } from 'swr';
import caller from 'utils/fetcher';
import { Form, Table, Space, Button, Popconfirm, Input, Tag, message } from 'antd';
import { MemberDetailContext } from 'contexts/MemberDetailContext';
import { useIntl, FormattedMessage } from 'react-intl';
import BadgeWithDefaultStatusName from 'components/BadgeWithDefaultStatusName';
import SwitchWithDefaultName from 'components/SwitchWithDefaultName';
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect';
import FormItemsWalletAddress from 'components/FormItems/WalletAddress';
import usePermission from 'hooks/usePermission';
import {
  MEMBERS_MEMBERS_EDIT_MEMBER_BANKCARD,
  MEMBERS_AFFILIATES_EDIT_AFFILIATE_BANKCARD,
} from 'constants/permissions';
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType';

const ACCOUNT_INFO = {
  NAME: 0,
  TAG: 1,
};

const EditableCell = ({ editing, editInput, dataIndex, children, ...restProps }) => {
  return <td {...restProps}>{editing ? editInput : children}</td>;
};

export default function EditableTable({ dataSource }) {
  const intl = useIntl();
  const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext);
  const currentAffiliateTypeId = m && m.affiliateType.id;
  const isAffiliateMember = currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER;

  const [editBankcardPermission] = usePermission(
    isAffiliateMember ? MEMBERS_AFFILIATES_EDIT_AFFILIATE_BANKCARD : MEMBERS_MEMBERS_EDIT_MEMBER_BANKCARD
  );

  const hasBankDataEditPermission = isEditMode && editBankcardPermission;

  const successMsg = intl.formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess',
  });
  const errorMsg = intl.formatMessage({
    id: 'Share.ErrorMessage.UnknownError',
  });

  const [form] = Form.useForm();
  const data = useMemo(() => dataSource, [dataSource]);
  const [editingKey, setEditingKey] = useState('');
  const isEditing = record => record.key === editingKey;

  const edit = record => {
    form.setFieldsValue({
      AccountName: '',
      Enabled: '',
      ...record,
    });
    setEditingKey(record.key);
  };

  const save = async key => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex(item => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
      } else {
        newData.push(row);
      }

      handleUpdate(newData[index]);
      setEditingKey('');
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const handleUpdate = payload => {
    delete payload.createInfo;
    delete payload.accountInfo;
    delete payload.branchName;
    delete payload.key;
    delete payload.CreateOn;
    delete payload.BindRemoteIp;
    delete payload.ApproveState;
    delete payload.CryptoProtocolName;

    const endpoint = isAffiliateMember ? '/api/Affiliate/CryptoCurrencyWallet' : '/api/Member/CryptoCurrencyWallet';

    caller({
      method: 'patch',
      endpoint,
      body: payload,
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        trigger(`${endpoint}/${payload.MemberId}`);
      })
      .catch(err => {
        console.error('Update Bank Data Error', err);
        message.error(err.ResponseMessage ? err.ResponseMessage : errorMsg, 5);
      });
  };

  const cancel = () => {
    setEditingKey('');
  };

  const COLUMN_CONFIG = [
    {
      title: <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.AccountName" />,
      dataIndex: 'accountInfo',
      editable: hasBankDataEditPermission && isEditMode,
      editInput: (
        <Form.Item name="AccountName">
          <Input maxLength={50} />
        </Form.Item>
      ),
      width: 260,
      fixed: 'left',
      render: accountInfo => {
        return (
          <Space>
            <span style={{ wordBreak: 'break-all' }}>{accountInfo[ACCOUNT_INFO.NAME]}</span>
            {!!accountInfo[ACCOUNT_INFO.TAG] && (
              <Tag color="geekblue">
                <FormattedMessage id={'MemberDetail.Tabs.BankData.Tages.Default'} />
              </Tag>
            )}
          </Space>
        );
      },
    },
    {
      title: <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.WalletNickName" />,
      dataIndex: 'WalletNickName',
      editable: hasBankDataEditPermission && isEditMode,
      editInput: (
        <Form.Item name="WalletNickName">
          <Input />
        </Form.Item>
      ),
      width: 160,
    },
    {
      title: <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.CryptoProtocol" />,
      dataIndex: 'CryptoProtocolName',
      editable: hasBankDataEditPermission && isEditMode,
      editInput: <FormItemsSimpleSelect url="/api/Option/CryptoCurrency/CryptoProtocol" name="CryptoProtocolId" />,
      width: 160,
    },
    {
      title: <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.WalletAddress" />,
      dataIndex: 'WalletAddress',
      editable: hasBankDataEditPermission && isEditMode,
      editInput: <FormItemsWalletAddress noLabel={true} />,
      width: 160,
    },
    {
      title: <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.CreateInfo" />,
      dataIndex: 'createInfo',
      width: 280,
    },
    {
      title: <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.Status" />,
      dataIndex: 'Enabled',
      editable: isEditMode,
      editInput: (
        <Form.Item name="Enabled" valuePropName="checked">
          <SwitchWithDefaultName />
        </Form.Item>
      ),
      width: 120,
      render: (_, record) => {
        return <BadgeWithDefaultStatusName status={record.Enabled} />;
      },
    },
  ];

  const ACTION_COLUMN_CONFIG = [
    {
      title: <FormattedMessage id="Share.CommonKeys.Actions" />,
      dataIndex: 'actions',
      width: 160,
      fixed: 'right',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <Space>
            <Button type="link" onClick={() => save(record.key)}>
              <FormattedMessage id="Share.ActionButton.Save" />
            </Button>
            <Popconfirm
              placement="top"
              title={<FormattedMessage id="Share.ActionButton.SureToCancel" />}
              onConfirm={cancel}
              okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
              cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
            >
              <Button type="link">
                <FormattedMessage id="Share.ActionButton.Cancel" />
              </Button>
            </Popconfirm>
          </Space>
        ) : (
          <Button type="link" disabled={editingKey !== ''} onClick={() => edit(record)}>
            <FormattedMessage id="Share.ActionButton.Edit" />
          </Button>
        );
      },
    },
  ];

  const columnConfig = useMemo(() => {
    if (isEditMode) {
      return COLUMN_CONFIG.concat(ACTION_COLUMN_CONFIG);
    } else {
      return COLUMN_CONFIG;
    }
  }, [isEditMode, isEditing]);

  const mergedColumns = columnConfig.map(col => {
    if (!col.editable) {
      return {
        ...col,
      };
    }

    return {
      ...col,
      onCell: record => ({
        record,
        editInput: col.editInput,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        size="middle"
        scroll={{ x: isEditMode ? 1040 : 840 }}
        dataSource={data}
        columns={mergedColumns}
        pagination={false}
      />
    </Form>
  );
}
