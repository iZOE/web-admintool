import React, { useState, useContext } from 'react'
import caller from 'utils/fetcher'
import { trigger } from 'swr'
import { Modal, Form, Input, Button, message } from 'antd'
import { CreditCardOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import SwitchWithDefaultName from 'components/SwitchWithDefaultName'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import FormItemsWalletAddress from 'components/FormItems/WalletAddress'

const checkSpcialCharapters = value =>
  new RegExp(
    /[~`!@#$%^&*(){}「～·！＠＃＄％＾＆＊（）＿＋『』｜“：，。？」\[\];:"'<,.>?\/\\|_+=-]/g,
  ).test(value)

const RULES = {
  REQUIRED: {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />,
  },
}

export default function AddCryptoCurrencyModalForm({ isAffiliateMember }) {
  const intl = useIntl()
  const { memberIdentity: m } = useContext(MemberDetailContext)
  const memberId = m && m.memberData.id
  const [isVisible, setIsVisible] = useState(false)
  const [form] = Form.useForm()
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  }

  const successMsg = intl.formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess',
  })
  const warningMsg = intl.formatMessage({
    id:
      'Share.ErrorMessage.ErrorKey.GroupFunctionPermissionParentGroupNotExist',
  })

  const onModalSave = values => {
    const isTypeValidated = typeof values.Enabled === 'boolean'

    const endpoint = isAffiliateMember
      ? '/api/Affiliate/CryptoCurrencyWallet'
      : '/api/Member/CryptoCurrencyWallet'

    const payload = {
      ...values,
      IsDefault: false,
      MemberId: memberId,
      Enabled: isTypeValidated ? values.Enabled : true,
    }

    caller({
      method: 'post',
      endpoint,
      body: payload,
    })
      .then(res => {
        if (res.ErrorKey === 'GroupFunctionPermission_ParentGroupNotExist') {
          message.warning(warningMsg, 5)
        } else {
          message.success(successMsg, 5)
        }
      })
      .then(() => {
        trigger(`${endpoint}/${payload.MemberId}`)
      })
      .catch(err => {
        console.error(err)
        message.error(err.Message, 5)
      })

    form.resetFields()
    setIsVisible(false)
  }

  const onOk = () => {
    form.submit()
  }

  const onCancel = () => {
    form.resetFields()
    setIsVisible(false)
  }

  return (
    <>
      <Button
        type="primary"
        icon={<CreditCardOutlined style={{ marginRight: 8 }} />}
        onClick={() => {
          setIsVisible(true)
        }}
      >
        <FormattedMessage id="MemberDetail.Tabs.BankData.Actions.AddCryptoCurrency" />
      </Button>

      <Modal
        title={
          <FormattedMessage id="MemberDetail.Tabs.BankData.Actions.AddCryptoCurrency" />
        }
        visible={isVisible}
        onOk={onOk}
        onCancel={onCancel}
        okText={<FormattedMessage id="Share.ActionButton.Submit" />}
        cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
      >
        <Form {...layout} form={form} onFinish={onModalSave}>
          <Form.Item
            label={
              <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.AccountName" />
            }
            name="AccountName"
          >
            <Input maxLength={50} />
          </Form.Item>

          <FormItemsSimpleSelect
            url="/api/Option/CryptoCurrency/CryptoProtocol"
            name="CryptoProtocolId"
            label={
              <FormattedMessage id="PageCryptoCurrency.DataTable.Title.CryptoProtocol" />
            }
            placeholder={
              <FormattedMessage
                id="Share.PlaceHolder.Switch.Default"
                description="请选择"
              />
            }
            rules={[RULES.REQUIRED]}
          />

          <Form.Item
            label={
              <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.WalletNickName" />
            }
            name="WalletNickName"
            rules={[
              RULES.REQUIRED,
              {
                validator: (_, value) =>
                  checkSpcialCharapters(value)
                    ? Promise.reject(
                        <FormattedMessage
                          id="Share.FormValidate.Pattern.NotAllowSpecialCharacters"
                          defaultMessage="输入格式为30个字，包含中文、英文、数字，不包含特殊符号"
                        />,
                      )
                    : Promise.resolve(true),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <FormItemsWalletAddress />
          <Form.Item
            label={
              <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.Status" />
            }
            name="Enabled"
            valuePropName="checked"
          >
            <SwitchWithDefaultName />
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}
