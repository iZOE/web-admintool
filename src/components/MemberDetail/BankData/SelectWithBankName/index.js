import React from "react";
import useSWR from "swr";
import axios from "axios";
import { Spin } from "antd";
import ListSelect from "components/ListSelect";

export default function SelectWithBankName({ ...props }) {
  let { data: bankList } = useSWR(`/api/Option/MemberBank`, url =>
    axios(url).then(res => res.data.Data)
  );

  return bankList ? <ListSelect list={bankList} {...props} /> : <Spin />;
}
