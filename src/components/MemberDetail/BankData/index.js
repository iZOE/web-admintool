import React, { useMemo, useContext, lazy } from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Descriptions, Divider, Space, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import BankEditableTable from './BankEditableTable'
import DateWithFormat from 'components/DateWithFormat'
import WalletEditableTable from './WalletEditableTable'
import CheckBankCardInfoModalForm from './CheckBankCardInfoModalForm'
import usePermission from 'hooks/usePermission'
import * as PERMISSION from 'constants/permissions'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import { NO_DATA } from 'constants/noData'

const {
    MEMBERS_MEMBERS_CREATE_MEMBER_BANKCARD,
    MEMBERS_AFFILIATES_CREATE_AFFILIATE_BANKCARD,
} = PERMISSION
const AddBankCardModalFormAsync = lazy(() => import('./AddBankCardModalForm'))
const AddCryptoCurrencyModalFormAsync = lazy(() =>
    import('./AddCryptoCurrencyModalForm'),
)

export default function BankData() {
    const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext)
    const currentAffiliateTypeId = m && m.affiliateType.id
    const memberId = m && m.memberData.id
    const isAffiliateMember =
        currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER

    const [createBankcardPermission] = usePermission(
        isAffiliateMember
            ? MEMBERS_AFFILIATES_CREATE_AFFILIATE_BANKCARD
            : MEMBERS_MEMBERS_CREATE_MEMBER_BANKCARD,
    )

    const hasBankDataCreatePermission = isEditMode && createBankcardPermission

    const bankApiUrl = isAffiliateMember
        ? `/api/Affiliate/MemberBank/${memberId}`
        : `/api/Member/MemberBank/${memberId}`

    const walletApiUrl = isAffiliateMember
        ? `/api/Affiliate/CryptoCurrencyWallet/${memberId}`
        : `/api/Member/CryptoCurrencyWallet/${memberId}`

    const { data: bankData } = useSWR(memberId ? bankApiUrl : null, url =>
        axios(url).then(res => res && res.data.Data),
    )

    const { data: walletData } = useSWR(memberId ? walletApiUrl : null, url =>
        axios(url).then(res => res && res.data.Data),
    )

    const bankDataSource = useMemo(() => {
        if (!!bankData) {
            const data = bankData.map((item, key) => {
                return {
                    key,
                    accountInfo: [item.AccountName, item.IsDefault],
                    branchName: !!item.BankBranchName
                        ? item.BankBranchName
                        : NO_DATA,
                    createInfo: (
                        <>
                            <DateWithFormat time={item.CreateOn} />
                            {` (${
                                !!item.BindRemoteIp
                                    ? item.BindRemoteIp
                                    : NO_DATA
                            })`}
                        </>
                    ),
                    ...item,
                }
            })

            return data
        }
    }, [bankData])

    const walletDataSource = useMemo(() => {
        if (!!walletData) {
            const data = walletData.map((item, key) => {
                return {
                    key,
                    accountInfo: [item.AccountName, item.IsDefault],
                    createInfo: (
                        <>
                            <DateWithFormat time={item.CreateOn} />
                            {` (${
                                !!item.BindRemoteIp
                                    ? item.BindRemoteIp
                                    : NO_DATA
                            })`}
                        </>
                    ),
                    ...item,
                }
            })

            return data
        }
    }, [walletData])

    return (
        <>
            <Descriptions
                title={
                    <FormattedMessage id="MemberDetail.Tabs.BankData.Areas.BankCards" />
                }
            />
            {bankDataSource ? (
                <BankEditableTable dataSource={bankDataSource} />
            ) : (
                <Skeleton active />
            )}
            <Divider />
            <Descriptions
                title={
                    <FormattedMessage id="MemberDetail.Tabs.BankData.Areas.CryptoCurrencyWallet" />
                }
            />
            {walletDataSource ? (
                <WalletEditableTable dataSource={walletDataSource} />
            ) : (
                <Skeleton active />
            )}
            <Divider />

            <Space>
                <CheckBankCardInfoModalForm />
                {hasBankDataCreatePermission && (
                    <AddBankCardModalFormAsync
                        isAffiliateMember={isAffiliateMember}
                    />
                )}
                {hasBankDataCreatePermission && (
                    <AddCryptoCurrencyModalFormAsync
                        isAffiliateMember={isAffiliateMember}
                    />
                )}
            </Space>
        </>
    )
}
