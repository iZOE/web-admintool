import styled from "styled-components";

export const Wrapper = styled.div`
  .ant-form-item {
    margin-bottom: 0px;
  }
  .transfer {
    display: flex;
    margin: 10px 0;
    .account {
      width: 33%;
      text-align: center;
      border: 2px solid #bce6fa;
      padding: 6px 0px;
      border-radius: 3px;
      background: #f6fcff;
    }
    .icon {
      border: 2px solid #8ad4f8;
      border-radius: 50%;
      font-size: 20px;
      width: 36px;
      background: #f6fcff;
      padding: 6px 0;
      margin: 0 10px;
      color: #0088c9;
    }
  }
  .amount {
    position: relative;

    button {
      position: absolute;
      right: 6px;
      top: 4px;
      padding: 0 10px;
      background: #ccc;
      color: #000;
    }
  }
`;
