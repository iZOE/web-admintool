import React, { useContext, useState, useMemo } from 'react'
import { Form, Input, message, Modal, Button } from 'antd'
import { ArrowRightOutlined, ArrowLeftOutlined } from '@ant-design/icons'
import caller from 'utils/fetcher'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import { FormattedMessage, useIntl } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import { Wrapper } from './Styled'
import { PageContext } from '../index'

const { useForm } = Form

//5:后台转出, 6:后台转入
const TRANSFER_TYPE = {
    OUT: 5,
    IN: 6,
}

export default function TransferModal({ modal, detail }) {
    const { visible, data } = modal
    const intl = useIntl()
    const [form] = useForm()
    const { getBalance, setModal } = useContext(PageContext)
    const { memberIdentity: m } = useContext(MemberDetailContext)
    const currentAffiliateTypeId = m && m.affiliateType.id
    const isAffiliateMember =
        currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER
    const { MemberBalanceAmount: memberBalanceAmount } = detail
    const [transferOut, setTransferOut] = useState(true)
    const gameProviderBalance = data ? data.currentBalance : null

    const successMsg = useIntl().formatMessage({
        id: 'MemberDetail.Tabs.AccountSetting.Modal.Messages.UpdateSuccess',
    })

    const gameProvider = useMemo(() => {
        if (data) {
            return detail.GameTransferSetting.find(
                d => d.GameProviderId === data.gameProviderId
            )
        }
    }, [data, detail.GameTransferSetting])

    const handleCancel = () => {
        setModal({ visible: false, data: null })
        form.resetFields()
    }

    const onFinish = value => {
        const url = isAffiliateMember
            ? '/api/Affiliate/BackendTransfer'
            : '/api/Member/BackendTransfer'
        caller({
            method: 'post',
            endpoint: url,
            body: {
                MemberId: m.memberData.id,
                GameProviderId: data.gameProviderId,
                Amount: Number(value.amount),
                TransferMode: transferOut
                    ? TRANSFER_TYPE.OUT
                    : TRANSFER_TYPE.IN,
            },
        })
            .then(() => {
                message.success(successMsg)
                handleCancel()
                getBalance(data.gameProviderId)
            })
            .catch(err => {
                console.error('[ERR]', err)
            })
    }

    if (!data) return null

    return (
        <Form form={form} layout="inline" onFinish={onFinish}>
            <Modal
                visible={visible}
                title={
                    <FormattedMessage
                        id="MemberDetail.Tabs.AccountSetting.Modal.Title"
                        description="後台轉帳"
                    />
                }
                onCancel={handleCancel}
                footer={[
                    <Button type="primary" onClick={handleCancel} key={1}>
                        <FormattedMessage id="Share.ActionButton.Cancel" />
                    </Button>,
                    <Form.Item
                        shouldUpdate
                        style={{ display: 'inline-block', marginLeft: '10px' }}
                        key={2}
                    >
                        {() => (
                            <Button
                                type="primary"
                                htmlType="submit"
                                onClick={() => form.submit()}
                                disabled={
                                    !form.isFieldsTouched(true) ||
                                    form
                                        .getFieldsError()
                                        .filter(({ errors }) => errors.length)
                                        .length
                                }
                            >
                                <FormattedMessage
                                    id="MemberDetail.Tabs.AccountSetting.Action.TransferNow"
                                    description="立即轉帳"
                                />
                            </Button>
                        )}
                    </Form.Item>,
                ]}
            >
                <Wrapper>
                    <Form.Item
                        label={
                            <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Fields.MainAccountBalance" />
                        }
                    >
                        <ReactIntlCurrencyWithFixedDecimal
                            value={memberBalanceAmount}
                        />
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="MemberDetail.Tabs.AccountSetting.Fields.GameProviderBalance"
                                values={{ gameProvider: gameProvider.Remark }}
                            />
                        }
                    >
                        <ReactIntlCurrencyWithFixedDecimal
                            value={gameProviderBalance}
                        />
                    </Form.Item>
                    <div className="transfer">
                        <div className="account">
                            <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Fields.MainAccountBalance" />
                        </div>
                        <div onClick={() => setTransferOut(!transferOut)}>
                            {transferOut ? (
                                <ArrowRightOutlined className="icon" />
                            ) : (
                                <ArrowLeftOutlined className="icon" />
                            )}
                        </div>
                        <div className="account">{gameProvider.Remark}</div>
                    </div>
                    <div className="amount">
                        <Form.Item
                            name="amount"
                            rules={[
                                {
                                    required: true,
                                    message: (
                                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                                    ),
                                },
                                {
                                    pattern: RegExp('^[0-9]*$', 'g'),
                                    message: (
                                        <FormattedMessage id="Share.FormValidate.Pattern.InputNumber" />
                                    ),
                                },
                            ]}
                        >
                            <Input
                                placeholder={intl.formatMessage({
                                    id:
                                        'MemberDetail.Tabs.AccountSetting.Modal.PlaceHolder.CustomAmount',
                                    description: '自订金额，仅限整数',
                                })}
                            />
                        </Form.Item>
                        <Button
                            size="small"
                            onClick={() =>
                                form.setFieldsValue({
                                    amount: Math.floor(
                                        transferOut
                                            ? memberBalanceAmount
                                            : gameProviderBalance
                                    ),
                                })
                            }
                        >
                            <FormattedMessage
                                id="MemberDetail.Tabs.AccountSetting.Action.MaximumAmount"
                                description="最大金額"
                            />
                        </Button>
                    </div>
                </Wrapper>
            </Modal>
        </Form>
    )
}
