import React, { lazy, useContext, useState, createContext } from 'react'
import axios from 'axios'
import { Descriptions, Divider, Typography, Skeleton, Space } from 'antd'
import { FormattedMessage } from 'react-intl'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import GameTransferSetting from './GameTransferSetting'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import TransferModal from './TransferModal'

const TransferAllThirdPartyBalanceBackButtonAsync = lazy(() =>
    import('./TransferAllThirdPartyBalanceBackButton')
)

const { Text } = Typography
export const PageContext = createContext()

export const BALANCE_STATUS = {
    DEFAULT: null,
    NOT_FOUND: -1,
}

const getGameProviderList = list => {
    const data = {}
    list.map(item => {
        data[item.GameProviderId] = BALANCE_STATUS.DEFAULT
    })
    return data
}

export default function AccountSetting({ detail }) {
    const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext)
    const memberId = m && m.memberData.id
    const [modal, setModal] = useState({
        visible: false,
        data: null,
    })
    const [gamesBalance, setGamesBalance] = useState(
        getGameProviderList(detail.GameTransferSetting)
    )

    const updateBalance = (gameProviderId, balance) => {
        setGamesBalance(prevState => ({
            ...prevState,
            [gameProviderId]: balance,
        }))
    }

    function getBalance(gameProviderId) {
        axios(`/api/Member/Balance/${gameProviderId}/${memberId}`).then(res => {
            updateBalance(gameProviderId, res.data.Data)
        })
    }

    return (
        <PageContext.Provider
            value={{
                setModal,
                getBalance,
                gamesBalance,
            }}
        >
            {!!detail ? (
                <>
                    <Descriptions
                        title={
                            <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Areas.MainAccountInfo" />
                        }
                        column={2}
                    >
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Fields.MainAccountBalance" />
                            }
                        >
                            <Space>
                                <Text>
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={detail.MemberBalanceAmount}
                                    />
                                </Text>
                                {isEditMode && (
                                    <TransferAllThirdPartyBalanceBackButtonAsync
                                        memberId={detail.MemberId}
                                    />
                                )}
                            </Space>
                        </Descriptions.Item>
                    </Descriptions>
                    <Divider dashed />
                    <Descriptions
                        title={
                            <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Areas.ThirdPartyAccountSetting" />
                        }
                        column={2}
                    />
                    <GameTransferSetting detail={detail} />
                    <TransferModal modal={modal} detail={detail} />
                </>
            ) : (
                <Skeleton />
            )}
        </PageContext.Provider>
    )
}
