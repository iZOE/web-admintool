import React, { useContext } from 'react'
import { trigger } from 'swr'
import caller from 'utils/fetcher'
import { Form, Row, Col, Button, Tooltip, message } from 'antd'
import { SaveOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import {
    MEMBERS_MEMBERS_GAME_PROVIDER_TRANSACTION,
    MEMBERS_AFFILIATES_GAME_PROVIDER_TRANSACTION,
} from 'constants/permissions'
import { FORM_COLUMN } from 'constants/memberDetail/layoutSetting'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import usePermission from 'hooks/usePermission'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import GetLatestBalanceButton from './GetLatestBalanceButton'
import ThirdPartyTransferFormList from 'components/Member/ThirdPartyTransferFormList'
import BadgeWithDefaultStatusName from 'components/BadgeWithDefaultStatusName'

const { useForm } = Form
const getProcessedSetting = list => {
    const data = list.map(item => {
        return {
            ...item,
            GameProviderText: item.Remark ? item.Remark : item.GameProviderName,
            TransferLock: !item.TransferLock,
        }
    })
    return data
}

export default function GameTransferSetting({ detail }) {
    const intl = useIntl()
    const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext)
    const memberId = m && m.memberData.id
    const currentAffiliateTypeId = m && m.affiliateType.id
    const isAffiliateMember =
        currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER
    const [form] = useForm()
    const [
        hasMember3rdPartyDataPermission,
        hasAfiliate3rdPartyDataPermission,
    ] = usePermission(
        MEMBERS_MEMBERS_GAME_PROVIDER_TRANSACTION,
        MEMBERS_AFFILIATES_GAME_PROVIDER_TRANSACTION
    )

    const has3rdPartyTransferSetting = isAffiliateMember
        ? hasAfiliate3rdPartyDataPermission
        : hasMember3rdPartyDataPermission

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })
    const unknownErrorMsg = intl.formatMessage({
        id: 'Share.ErrorMessage.UnknownError',
    })
    const isEditable = isEditMode
    const initialValues = detail && {
        ...detail,
        GameTransferSetting: getProcessedSetting(detail.GameTransferSetting),
    }

    function onFinish(values) {
        const processedSettings = values.GameTransferSetting.map(
            ({ GameProviderId, TransferLock }) => {
                return {
                    GameProviderId,
                    TransferLock: !TransferLock,
                }
            }
        )

        const payload = {
            MemberId: memberId,
            GameTransferSetting: processedSettings,
        }

        const endpoint = isAffiliateMember
            ? '/api/Affiliate/Update/GameProviderLock'
            : '/api/Member/Update/GameProviderLock'

        if (!!payload && form.isFieldsTouched()) {
            caller({
                method: 'put',
                endpoint,
                body: payload,
            })
                .then(() => {
                    message.success(successMsg)
                })
                .then(() => {
                    trigger(`/api/Member/${memberId}`)
                })
                .catch(err => {
                    const errMsg = err.Message ? err.Message : unknownErrorMsg
                    message.error(errMsg, 5)
                    console.error(
                        "Error on updating member's 3rd party transfer settings:",
                        err
                    )
                })
        }
    }

    return (
        detail && (
            <Form
                size="middle"
                form={form}
                layout="vertical"
                onFinish={onFinish}
                hideRequiredMark={true}
                initialValues={initialValues}
            >
                <Row gutter={FORM_COLUMN.FULL}>
                    <Col span={FORM_COLUMN.HALF}>
                        {initialValues.GameTransferSetting.map(
                            ({ GameProviderId, GameProviderText }, key) => (
                                <Form.Item
                                    labelCol={{ span: FORM_COLUMN.FULL }}
                                    key={key}
                                    label={
                                        <>
                                            {GameProviderText}
                                            <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Fields.AccountBalance" />
                                        </>
                                    }
                                >
                                    <GetLatestBalanceButton
                                        gameProviderId={GameProviderId}
                                    />
                                </Form.Item>
                            )
                        )}
                    </Col>
                    {has3rdPartyTransferSetting && (
                        <>
                            <Col span={FORM_COLUMN.HALF}>
                                {isEditable ? (
                                    <ThirdPartyTransferFormList
                                        list={initialValues.GameTransferSetting}
                                    />
                                ) : (
                                    initialValues.GameTransferSetting.map(
                                        (
                                            {
                                                TransferLock: isLock,
                                                GameProviderText,
                                            },
                                            key
                                        ) => (
                                            <Form.Item
                                                labelCol={{
                                                    span: FORM_COLUMN.FULL,
                                                }}
                                                key={key}
                                                label={
                                                    <>
                                                        {GameProviderText}
                                                        <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Fields.AccountSetting" />
                                                    </>
                                                }
                                            >
                                                <BadgeWithDefaultStatusName
                                                    status={isLock}
                                                />
                                            </Form.Item>
                                        )
                                    )
                                )}
                            </Col>
                            {isEditMode && (
                                <Form.Item>
                                    <Tooltip
                                        title={
                                            <FormattedMessage id="Share.ActionButton.Save" />
                                        }
                                    >
                                        <Button
                                            className="fixedButton"
                                            type="primary"
                                            htmlType="submit"
                                            shape="circle"
                                            size="large"
                                            icon={<SaveOutlined />}
                                        />
                                    </Tooltip>
                                </Form.Item>
                            )}
                        </>
                    )}
                </Row>
            </Form>
        )
    )
}
