import React, { useContext } from "react";
import { FormattedMessage } from "react-intl";
import { Button } from "antd";
import { PageContext } from "../../../index";

export default function TransferModalButton({
  gameProviderId,
  currentBalance
}) {
  const { setModal } = useContext(PageContext);
  return (
    <Button
      type="primary"
      size="small"
      onClick={() =>
        setModal({
          visible: true,
          data: {
            gameProviderId,
            currentBalance
          }
        })
      }
    >
      <FormattedMessage
        id="MemberDetail.Tabs.AccountSetting.Action.Transfer"
        description="轉帳"
      />
    </Button>
  );
}
