import React, { useMemo, useContext } from 'react'
import { FormattedMessage } from 'react-intl'
import { Button, Typography, Space } from 'antd'
import { AccountBookOutlined } from '@ant-design/icons'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import TransferModalButton from './TransferModalButton'
import { PageContext } from '../../index'
import { BALANCE_STATUS } from '../../index'

const { Text } = Typography
const { MEMBERS_MEMBERS_TRANSFER, MEMBERS_AFFILIATES_TRANSFER } = PERMISSION

export default function GetLatestBalanceButton({ gameProviderId }) {
    const { memberIdentity: m } = useContext(MemberDetailContext)
    const { getBalance, gamesBalance } = useContext(PageContext)
    const currentAffiliateTypeId = m && m.affiliateType.id
    const isAffiliateMember =
        currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER

    const currentBalance = useMemo(() => gamesBalance[gameProviderId], [
        gameProviderId,
        gamesBalance,
    ])

    return !!(currentBalance === BALANCE_STATUS.DEFAULT) ? (
        <Button
            type="primary"
            size="small"
            onClick={() => getBalance(gameProviderId)}
        >
            <Space size="small">
                <AccountBookOutlined />
                <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Action.UpdateBalace" />
            </Space>
        </Button>
    ) : (
        <Text>
            {!!(currentBalance === BALANCE_STATUS.NOT_FOUND) ? (
                <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Status.BalanceNotFound" />
            ) : (
                <Space size="small">
                    <ReactIntlCurrencyWithFixedDecimal value={currentBalance} />
                    <Permission
                        functionIds={
                            isAffiliateMember
                                ? [MEMBERS_AFFILIATES_TRANSFER]
                                : [MEMBERS_MEMBERS_TRANSFER]
                        }
                    >
                        <TransferModalButton
                            gameProviderId={gameProviderId}
                            currentBalance={currentBalance}
                        />
                    </Permission>
                </Space>
            )}
        </Text>
    )
}
