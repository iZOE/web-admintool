import React from "react";
import { Popconfirm, Button, message } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import { TransactionOutlined } from "@ant-design/icons";
import caller from "utils/fetcher";

export default function TransferAllThirdPartyBalanceBackButton({ memberId }) {
  const intl = useIntl();
  const successMsg = intl.formatMessage({
    id: "MemberDetail.Tabs.AccountSetting.Messages.TransferSuccess"
  });

  const onConfirm = () => {
    caller({
      method: "get",
      endpoint: `/api/Withdrawal/aft?memberId=${memberId}`
    }).then(() => {
      message.success(successMsg, 5);
    });
  };

  return (
    <Popconfirm
      placement="right"
      title={
        <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Messages.ConfirmToTransfer" />
      }
      onConfirm={onConfirm}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Button
        type="danger"
        size="small"
        style={{ marginLeft: 8 }}
        icon={<TransactionOutlined style={{ marginRight: 6 }} />}
        onClick={() => {}}
      >
        <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Action.GetAllBack" />
      </Button>
    </Popconfirm>
  );
}
