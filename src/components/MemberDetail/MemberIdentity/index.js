import React, { useContext } from 'react';
import { PageHeader, Space, Tag, Skeleton } from 'antd';
import { MemberDetailContext } from 'contexts/MemberDetailContext';

export default function MemberIdentity() {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberName = m && m.memberData.name;

  return m ? (
    <PageHeader
      style={{ paddingRight: 0, paddingLeft: 0 }}
      title={memberName.toUpperCase()}
      subTitle={
        <Space size="small">
          {m.agentType.text && <Tag>{m.agentType.text}</Tag>}
          {m.memberType.text && <Tag>{m.memberType.text}</Tag>}
          {m.affiliateType.text && <Tag>{m.affiliateType.text}</Tag>}
        </Space>
      }
    />
  ) : (
    <Skeleton active />
  );
}
