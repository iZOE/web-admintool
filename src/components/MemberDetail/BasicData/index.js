import React, { useContext, lazy } from 'react'
import { trigger } from 'swr'
import caller from 'utils/fetcher'
import { Divider, Skeleton, Form, Tooltip, Button, message } from 'antd'
import { SaveOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import usePermission from 'hooks/usePermission'
import { MEMBERS_MEMBERS_LOCK_FUNCTION } from 'constants/permissions'
import AccountInfoSection from './AccountInfoSection'
import IdentitySettingSection from './IdentitySettingSection'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'

const { useForm } = Form
const QuotaInfoSectionAsync = lazy(() => import('./QuotaInfoSection'))
const FinancialPermissionSettingSectionAsync = lazy(() =>
    import('./FinancialPermissionSettingSection')
)

export default function BasicData({ detail }) {
    let initialValues = {
        NickName: detail.NickName,
        MemberLevelId: detail.MemberLevelId,
        AffiliateLevelId: detail.AffiliateLevelId,
        Status: detail.Status,
        Remarks: detail.Remarks,
        ReturnPoint: detail.ReturnPoint,
        IsBetPlaceable: detail.IsBetPlaceable,
        IsDepositable: detail.IsDepositable,
        IsWithdrawable: detail.IsWithdrawable,
        IsFundTransferable: detail.IsFundTransferable,
        IsSameLevel: detail.IsSameLevel,
        IsDeleteAvatar: false,
    }

    const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext)
    const memberId = m && m.memberData.id
    const isAffiliateMember = m.affiliateType.id !== AFFILIATE_LEVEL_TYPE.MEMBER
    const [hasViewMemberFinancialSettingPermission] = usePermission(
        MEMBERS_MEMBERS_LOCK_FUNCTION
    )
    const intl = useIntl()
    const [form] = useForm()

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })
    const errorMsg = intl.formatMessage({
        id: 'Share.ErrorMessage.UnknownError',
    })

    const onFinish = values => {
        const payload = {
            ...values,
            MemberId: memberId,
        }

        const endpoint =
            detail.AffiliateLevelId === AFFILIATE_LEVEL_TYPE.MEMBER
                ? '/api/Member/Update/BasicSetting'
                : '/api/Affiliate/Update/BasicSetting'

        caller({
            method: 'put',
            endpoint,
            body: payload,
        })
            .then(() => {
                message.success(successMsg, 5)
            })
            .then(() => {
                trigger(`/api/Member/${memberId}`)
            })
            .catch(err => {
                console.error('Update Member Basic Data Error: ', err)
                message.error(errorMsg, 5)
            })
    }

    return (
        <>
            {!!detail ? (
                <Form
                    size="middle"
                    hideRequiredMark={true}
                    form={form}
                    onFinish={onFinish}
                    initialValues={initialValues}
                >
                    <AccountInfoSection detail={detail} />
                    <Divider dashed />
                    <IdentitySettingSection detail={detail} />
                    {isAffiliateMember && (
                        <>
                            <Divider dashed />
                            <QuotaInfoSectionAsync />
                        </>
                    )}
                    {hasViewMemberFinancialSettingPermission && (
                        <>
                            <Divider dashed />
                            <FinancialPermissionSettingSectionAsync
                                detail={detail}
                            />
                        </>
                    )}
                    {isEditMode && (
                        <Form.Item>
                            <Tooltip
                                title={
                                    <FormattedMessage id="Share.ActionButton.Save" />
                                }
                            >
                                <Button
                                    className="fixedButton"
                                    type="primary"
                                    htmlType="submit"
                                    shape="circle"
                                    size="large"
                                    icon={<SaveOutlined />}
                                />
                            </Tooltip>
                        </Form.Item>
                    )}
                </Form>
            ) : (
                <Skeleton />
            )}
        </>
    )
}
