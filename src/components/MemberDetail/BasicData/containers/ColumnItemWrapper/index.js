import React from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import { COLUMN } from "../../constants/layoutSetting";

export default function ColumnItemWrapper({
  label,
  name,
  span = COLUMN.HALF,
  children
}) {
  return (
    <Col span={span}>
      <Form.Item
        label={
          <FormattedMessage
            id={"MemberDetail.Tabs.BasicData.Fields." + label}
          />
        }
        name={name}
      >
        {children}
      </Form.Item>
    </Col>
  );
}
