import React from "react";
import { Descriptions, Row } from "antd";
import { FormattedMessage } from "react-intl";
import { COLUMN } from "components/MemberDetail/BasicData/constants/layoutSetting";

export default function AreaRowWrapper({ title, children }) {
  return (
    <>
      <Descriptions
        title={
          <FormattedMessage id={"MemberDetail.Tabs.BasicData.Areas." + title} />
        }
      />
      <Row gutter={COLUMN.FULL}>{children}</Row>
    </>
  );
}
