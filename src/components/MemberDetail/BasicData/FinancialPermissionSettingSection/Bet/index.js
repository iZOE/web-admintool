import React from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import BadgeWithDefaultStatusName from "components/BadgeWithDefaultStatusName";
import BetStatusFormItem from "components/Member/BetStatusFormItem";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function Bet({ status, isEditMode }) {
  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditMode ? (
        <BetStatusFormItem />
      ) : (
        <Form.Item
          label={<FormattedMessage id="MemberDetail.Fields.Bet" />}
          valuePropName="checked"
        >
          <BadgeWithDefaultStatusName status={status} />
        </Form.Item>
      )}
    </Col>
  );
}
