import React from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import BadgeWithDefaultStatusName from "components/BadgeWithDefaultStatusName";
import DepositStatusFormItem from "components/Member/DepositStatusFormItem";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function Deposit({ status, isEditMode }) {
  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditMode ? (
        <DepositStatusFormItem />
      ) : (
        <Form.Item
          label={<FormattedMessage id="MemberDetail.Fields.Deposit" />}
          valuePropName="checked"
        >
          <BadgeWithDefaultStatusName status={status} />
        </Form.Item>
      )}
    </Col>
  );
}
