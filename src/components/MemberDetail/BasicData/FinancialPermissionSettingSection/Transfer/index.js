import React from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import BadgeWithDefaultStatusName from "components/BadgeWithDefaultStatusName";
import TransferStatusFormItem from "components/Member/TransferStatusFormItem";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function Transfer({ status, isEditMode }) {
  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditMode ? (
        <TransferStatusFormItem />
      ) : (
        <Form.Item
          label={<FormattedMessage id="MemberDetail.Fields.Transfer" />}
          valuePropName="checked"
        >
          <BadgeWithDefaultStatusName status={status} />
        </Form.Item>
      )}
    </Col>
  );
}
