import React, { useContext } from "react";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import AreaRowWrapper from "../containers/AreaRowWrapper";
import Bet from "./Bet";
import Deposit from "./Deposit";
import Withdrawal from "./Withdrawal";
import Transfer from "./Transfer";

export default function FinancialPermissionSettingSection({ detail: d }) {
  const { isEditMode } = useContext(MemberDetailContext);

  return (
    <AreaRowWrapper title="PermissionSetting">
      {d && (
        <>
          <Bet status={d.IsBetPlaceable} isEditMode={isEditMode} />
          <Deposit status={d.IsDepositable} isEditMode={isEditMode} />
          <Withdrawal status={d.IsWithdrawable} isEditMode={isEditMode} />
          <Transfer status={d.IsFundTransferable} isEditMode={isEditMode} />
        </>
      )}
    </AreaRowWrapper>
  );
}
