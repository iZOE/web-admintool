import React from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import BadgeWithDefaultStatusName from "components/BadgeWithDefaultStatusName";
import WithdrawalStatusFormItem from "components/Member/WithdrawalStatusFormItem";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function Withdrawal({ status, isEditMode }) {
  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditMode ? (
        <WithdrawalStatusFormItem />
      ) : (
        <Form.Item
          label={<FormattedMessage id="MemberDetail.Fields.Withdrawal" />}
          valuePropName="checked"
        >
          <BadgeWithDefaultStatusName status={status} />
        </Form.Item>
      )}
    </Col>
  );
}
