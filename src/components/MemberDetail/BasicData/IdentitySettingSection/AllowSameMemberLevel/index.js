import React, { useContext } from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import AllowSameMemberLevelFormItem from "components/Member/AllowSameMemberLevelFormItem";
import BadgeWithDefaultStatusName from "components/BadgeWithDefaultStatusName";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function AllowSameMemberLevel({ data: status }) {
  const { isEditMode } = useContext(MemberDetailContext);
  const isEditable = isEditMode;

  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditable ? (
        <AllowSameMemberLevelFormItem />
      ) : (
        <Form.Item
          label={
            <FormattedMessage id="MemberDetail.Fields.AllowSameMemberLevel" />
          }
          valuePropName="checked"
        >
          <BadgeWithDefaultStatusName label="IsAllow" status={status} />
        </Form.Item>
      )}
    </Col>
  );
}
