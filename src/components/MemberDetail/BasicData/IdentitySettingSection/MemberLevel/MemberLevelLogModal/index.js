import React, { useState, useContext, useEffect } from 'react'
import { Modal, Table, Button, Descriptions, Typography, Divider } from 'antd'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import caller from 'utils/fetcher'
import { MemberDetailContext } from 'contexts/MemberDetailContext'

export default function MemberLevelLogModal() {
    const { Title } = Typography
    const { memberIdentity: m } = useContext(MemberDetailContext)
    const memberId = m && m.memberData.id
    const [isVisible, setIsVisible] = useState(false)
    const [logData, setLogData] = useState()
    const [logLevelData, setLogLevelData] = useState()

    const COLUMN_CONFIG = [
        {
            title: (
                <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Fields.ChangeTime" />
            ),
            dataIndex: 'ChangeTime',
            key: 'ChangeTime',
            width: 200,
            render: (_1, { ChangeTime: time }, _2) => (
                <DateWithFormat time={time} />
            ),
        },
        {
            title: (
                <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Fields.MemberLevelName" />
            ),
            dataIndex: 'MemberLevelName',
            key: 'MemberLevelName',
        },
        {
            title: (
                <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Fields.Account" />
            ),
            dataIndex: 'Account',
            key: 'Account',
        },
    ]

    useEffect(() => {
        caller({
            method: 'post',
            endpoint: '/api/Member/Log/MemberLevel',
            body: {
                MemberId: memberId,
            },
            exportMode: true,
        }).then(({ data }) => {
            setLogData(data.Data.LevelLog)
            setLogLevelData(data.Data)
        })
    }, [memberId])

    return (
        <>
            <Button
                type="primary"
                size="small"
                onClick={() => {
                    setIsVisible(true)
                }}
            >
                <FormattedMessage id="MemberDetail.Tabs.BasicData.Actions.ViewMemberLevelLog" />
            </Button>

            <Modal
                title={
                    <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Title1" />
                }
                visible={isVisible}
                onCancel={() => setIsVisible(false)}
                footer={null}
                bodyStyle={{ height: '500px', overflow: 'auto' }}
            >
                {/* 
                恢复等级需达流水 RestoreTurnoverBetAmount might be null
                晋级已达充值 CumulativeDepositAmount / 晋级需达充值 ThresholdOfDepositAmount might be null
                晋级已达流水 CumulativeTurnoverBetAmount / 晋级需达流水 ThresholdOfTurnoverBetAmount might be null
                */}
                {logLevelData && (
                    <Descriptions column={1}>
                        <Descriptions.Item>
                            <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Detail.LastUpdateTime" />
                            <DateWithFormat
                                time={logLevelData.LastUpdateTime}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item>
                            <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Detail.RestoreTurnoverBetAmount" />
                            {logLevelData.RestoreTurnoverBetAmount === null ? (
                                '--'
                            ) : (
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={
                                        logLevelData.RestoreTurnoverBetAmount
                                    }
                                />
                            )}
                        </Descriptions.Item>
                        <Descriptions.Item>
                            <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Detail.CumulativeDepositAmount" />
                            <ReactIntlCurrencyWithFixedDecimal
                                value={logLevelData.CumulativeDepositAmount}
                            />
                            {' / '}
                            <ReactIntlCurrencyWithFixedDecimal
                                value={
                                    logLevelData.ThresholdOfDepositAmount ===
                                    null
                                        ? '--'
                                        : logLevelData.ThresholdOfDepositAmount
                                }
                            />
                        </Descriptions.Item>
                        <Descriptions.Item>
                            <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Detail.CumulativeTurnoverBetAmount" />
                            <ReactIntlCurrencyWithFixedDecimal
                                value={logLevelData.CumulativeTurnoverBetAmount}
                            />
                            {' / '}
                            <ReactIntlCurrencyWithFixedDecimal
                                value={
                                    logLevelData.ThresholdOfTurnoverBetAmount ===
                                    null
                                        ? '--'
                                        : logLevelData.ThresholdOfTurnoverBetAmount
                                }
                            />
                        </Descriptions.Item>
                    </Descriptions>
                )}
                <Title
                    level={5}
                    style={{ marginTop: '36px', marginBottom: '16px' }}
                >
                    <FormattedMessage id="MemberDetail.Tabs.BasicData.MemberLevelLog.Title2" />
                </Title>
                <Divider
                    style={{
                        margin: '0 -24px 16px',
                        width: 'calc(100% + 48px)',
                    }}
                />
                <Table
                    columns={COLUMN_CONFIG}
                    dataSource={logData}
                    pagination={false}
                    size="middle"
                    style={{ width: '100%' }}
                />
            </Modal>
        </>
    )
}
