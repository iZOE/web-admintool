import React from "react";
import useSWR from "swr";
import axios from "axios";
import { Spin } from "antd";
import ListSelect from "components/ListSelect";

export default function SelectWithMemberLevelList({ ...props }) {
  let { data: memberLevelList } = useSWR(`/api/Option/MemberLevel`, url =>
    axios(url).then(res => res.data.Data)
  );

  return memberLevelList ? (
    <ListSelect list={memberLevelList} {...props} />
  ) : (
    <Spin />
  );
}
