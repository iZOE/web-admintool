import React, { useContext } from 'react'
import usePermission from 'hooks/usePermission'
import * as PERMISSION from 'constants/permissions'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import { FormattedMessage } from 'react-intl'
import { Col, Form, Button } from 'antd'
import { FORM_COLUMN } from 'constants/memberDetail/layoutSetting'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import SelectWithMemberLevelList from './SelectWithMemberLevelList'
import MemberLevelLogModal from './MemberLevelLogModal'

const {
    MEMBERS_MEMBERS_MEMBER_LEVEL,
    MEMBERS_AFFILIATES_MEMBER_LEVEL,
} = PERMISSION
export default function MemberLevel({ data }) {
    const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext)
    const [
        hasEditMemberLevelPermission,
        hasEditAffiliateMemberLevelPermission,
    ] = usePermission(
        MEMBERS_MEMBERS_MEMBER_LEVEL,
        MEMBERS_AFFILIATES_MEMBER_LEVEL
    )
    const currentAffiliateTypeId = m && m.affiliateType.id
    const isAffiliateMember =
        currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER
    const isEditable =
        isEditMode &&
        (isAffiliateMember
            ? hasEditAffiliateMemberLevelPermission
            : hasEditMemberLevelPermission)

    return (
        <Col span={FORM_COLUMN.HALF}>
            {isEditable ? (
                <Form.Item
                    label={
                        <FormattedMessage id="MemberDetail.Fields.MemberLevel" />
                    }
                    name="MemberLevelId"
                >
                    <SelectWithMemberLevelList />
                </Form.Item>
            ) : (
                <Form.Item
                    label={
                        <FormattedMessage id="MemberDetail.Fields.MemberLevel" />
                    }
                >
                    <span className="ant-form-text">{data}</span>
                    <MemberLevelLogModal />
                </Form.Item>
            )}
        </Col>
    )
}
