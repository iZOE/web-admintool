import React, { useContext } from "react";
import useSWR from "swr";
import axios from "axios";
import { Col } from "antd";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import { AGENT_TYPE } from "constants/agentType";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";
import ReturnPointFormItem from "components/Member/ReturnPointFormItem";
import StaticFieldItem from "components/Member/StaticFieldItem";
import { RETURN_POINT_FIELD } from "constants/memberDetail/validators";

const validatedAgentList = [
  AGENT_TYPE.CAILIFUN_II,
  AGENT_TYPE.XING_HUI,
  AGENT_TYPE.JIN_SHA,
  AGENT_TYPE.VT_999,
  AGENT_TYPE.CN_999,
  AGENT_TYPE.HL_101
];

export default function ReturnPoint({ data: d }) {
  const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext);
  const isValidatedAgent = validatedAgentList.includes(m.agentType.id);
  const isEditable = isEditMode;
  const isDisabled = !isValidatedAgent;

  const payload = {
    AffiliateLevelId: d.affiliateLevelId,
    ParentMemberName: d.parentMemberName
  };

  const { data: returnPointRange } = useSWR(
    !!payload && isEditMode
      ? [`/api/Member/ReturnPointRange`, d.affiliateLevelId, d.parentMemberName]
      : null,
    url =>
      axios({
        url,
        method: "post",
        data: payload
      }).then(res => res && res.data.Data)
  );

  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditable && returnPointRange ? (
        <ReturnPointFormItem
          max={returnPointRange[RETURN_POINT_FIELD.MAX]}
          min={returnPointRange[RETURN_POINT_FIELD.MIN]}
          isDisabled={isDisabled}
        />
      ) : (
        <StaticFieldItem fieldName="ReturnPoint" value={d.returnPoint} />
      )}
    </Col>
  );
}
