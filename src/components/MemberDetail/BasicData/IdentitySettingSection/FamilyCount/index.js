import React from "react";
import ColumnItemWrapper from "../../containers/ColumnItemWrapper";
import { NO_DATA } from "constants/noData";

export default function FamilyCount({ data }) {
  return (
    <ColumnItemWrapper label="FamilyCount">
      {data ? data : NO_DATA}
    </ColumnItemWrapper>
  );
}
