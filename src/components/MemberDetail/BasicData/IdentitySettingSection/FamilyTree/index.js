import React, { useContext, lazy } from "react";
import useSWR from "swr";
import axios from "axios";
import { Breadcrumb, Button, Space } from "antd";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import ColumnItemWrapper from "../../containers/ColumnItemWrapper";
import { COLUMN } from "../../constants/layoutSetting";
import { AFFILIATE_LEVEL_TYPE } from "constants/memberDetail/memberType";
import { NO_DATA } from "constants/noData";

const ShowChangeParentModalFormButtonAsync = lazy(() =>
  import("./ShowChangeParentModalFormButton")
);

export default function FamilyTree({ data: parentMemberId }) {
  const { onDisplayMemberDetail, isEditMode, memberIdentity: m } = useContext(
    MemberDetailContext
  );
  const isGerneralMember =
    m && m.affiliateType.id === AFFILIATE_LEVEL_TYPE.MEMBER;
  const isParentTransferable = isEditMode && isGerneralMember;
  const memberId = m && m.memberData.id;

  const { data: detail } = useSWR(
    memberId && parentMemberId ? `/api/Member/AllParents/${memberId}` : null,
    url => axios(url).then(res => res.data.Data.sort((a, b) => b.Num - a.Num))
  );

  const onClick = selectedMemberId => {
    if (selectedMemberId !== memberId) {
      onDisplayMemberDetail(selectedMemberId);
    }
  };

  return (
    <ColumnItemWrapper
      label="FamilyTree"
      span={parentMemberId ? COLUMN.FULL : COLUMN.HALF}
    >
      {parentMemberId ? (
        <Space>
          <Breadcrumb separator=">">
            {detail &&
              detail.map(item => (
                <Breadcrumb.Item key={item.Num}>
                  {item.MemberId === memberId ? (
                    <span>{item.MemberName}</span>
                  ) : (
                    <Button
                      style={{ padding: 0 }}
                      onClick={() => onClick(item.MemberId)}
                      type="link"
                    >
                      {item.MemberName}
                    </Button>
                  )}
                </Breadcrumb.Item>
              ))}
          </Breadcrumb>
          {isParentTransferable && <ShowChangeParentModalFormButtonAsync />}
        </Space>
      ) : (
        NO_DATA
      )}
    </ColumnItemWrapper>
  );
}
