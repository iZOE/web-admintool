import React, { useState, lazy } from "react";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";

const ChangeParentModalFormAsync = lazy(() =>
  import("../ChangeParentModalForm")
);

export default function ShowChangeParentModalFormButton() {
  const [isShownModal, setIsShownModal] = useState(false);

  return (
    <>
      <Button
        type="primary"
        onClick={() => {
          setIsShownModal(true);
        }}
      >
        <FormattedMessage
          id={"MemberDetail.Tabs.BasicData.Actions.TransferParent"}
        />
      </Button>

      {isShownModal && (
        <ChangeParentModalFormAsync
          isVisible={isShownModal}
          onCloseModal={() => {
            setIsShownModal(false);
          }}
        />
      )}
    </>
  );
}
