import React, { useContext } from "react";
import { Modal, Form, Input, message } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import caller from "utils/fetcher";
import { trigger } from "swr";
import { MemberDetailContext } from "contexts/MemberDetailContext";

export default function ChangeParentModalForm({ isVisible, onCloseModal }) {
  const intl = useIntl();
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;

  const [form] = Form.useForm();
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 14 }
  };

  const successMsg = intl.formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });

  const onCancel = () => {
    form.resetFields();
    onCloseModal();
  };

  const onModalSave = values => {
    const payload = {
      ...values,
      SourceMemberName: m.memberData.name
    };

    caller({
      method: "put",
      endpoint: "/api/Member/Transfer",
      body: payload
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        trigger(`/api/Member/${memberId}`);
        trigger(`/api/Member/AllParents/${memberId}`);
      })
      .catch(err => {
        console.error("Change Member's Parent Error:", err);
      });
  };

  return (
    <Modal
      centered
      destroyOnClose
      visible={isVisible}
      onOk={() => {
        form.submit();
      }}
      onCancel={onCancel}
      title={
        <FormattedMessage id="MemberDetail.Tabs.BasicData.Fields.ChangeParent" />
      }
      okText={<FormattedMessage id="Share.ActionButton.Submit" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Form {...layout} form={form} onFinish={onModalSave}>
        <Form.Item
          name="TargetMemberName"
          label={
            <FormattedMessage id="MemberDetail.Tabs.BasicData.Fields.TargetParentName" />
          }
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="Share.FormValidate.Required.Input" />
              )
            }
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
}
