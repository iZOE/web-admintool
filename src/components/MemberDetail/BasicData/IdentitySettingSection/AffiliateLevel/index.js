import React, { useContext } from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import SelectWithAffiliateLevelList from "components/Member/SelectWithAffiliateLevelList";
import { AGENT_TYPE } from "constants/agentType";
import { AFFILIATE_LEVEL_TYPE } from "constants/memberDetail/memberType";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";
import StaticFieldItem from "components/Member/StaticFieldItem";

const validatedAgentList = [
  AGENT_TYPE.CAILIFUN_II,
  AGENT_TYPE.XING_HUI,
  AGENT_TYPE.JIN_SHA,
  AGENT_TYPE.HL_101
];

export default function AffiliateLevel({ data }) {
  const { isEditMode, memberIdentity } = useContext(MemberDetailContext);
  const { agentType, affiliateType } = memberIdentity;
  const isValidatedAgent = validatedAgentList.includes(agentType.id);
  const isValidatedAffiliate = affiliateType.id !== AFFILIATE_LEVEL_TYPE.MEMBER;

  const isEditable = isEditMode && isValidatedAgent && isValidatedAffiliate;

  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditable ? (
        <Form.Item
          label={<FormattedMessage id="MemberDetail.Fields.AffiliateLevel" />}
          name="AffiliateLevelId"
        >
          <SelectWithAffiliateLevelList />
        </Form.Item>
      ) : (
        <StaticFieldItem fieldName="AffiliateLevel" value={data} />
      )}
    </Col>
  );
}
