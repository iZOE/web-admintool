import React, { useContext, lazy } from "react";
import { Form } from "antd";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import AreaRowWrapper from "../containers/AreaRowWrapper";
import MemberLevel from "./MemberLevel";
import AffiliateLevel from "./AffiliateLevel";
import ReturnPoint from "./ReturnPoint";
import FamilyTree from "./FamilyTree";
import { AFFILIATE_LEVEL_TYPE } from "constants/memberDetail/memberType";

const AllowSameMemberLevelAsync = lazy(() => import("./AllowSameMemberLevel"));
const FamilyCountAsync = lazy(() => import("./FamilyCount"));

export default function IdentitySettingSection({ detail: d }) {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const isAffiliateMember = m.affiliateType.id !== AFFILIATE_LEVEL_TYPE.MEMBER;

  return (
    <AreaRowWrapper title="IdentitySetting">
      <MemberLevel data={d.MemberLevelName} />
      {isAffiliateMember && <AllowSameMemberLevelAsync data={d.IsSameLevel} />}
      <AffiliateLevel data={d.AffiliateLevelName} />
      <Form.Item noStyle shouldUpdate>
        {({ getFieldValue }) => (
          <ReturnPoint
            data={{
              returnPoint: d.ReturnPoint,
              affiliateLevelId: getFieldValue("AffiliateLevelId"),
              parentMemberName: getFieldValue("ParentMemberName")
            }}
          />
        )}
      </Form.Item>
      <FamilyTree data={d.ParentMemberId} />
      {isAffiliateMember && <FamilyCountAsync data={d.FollowingCount} />}
    </AreaRowWrapper>
  );
}
