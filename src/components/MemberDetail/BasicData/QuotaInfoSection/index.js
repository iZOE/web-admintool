import React, { useContext } from "react";
import useSWR from "swr";
import axios from "axios";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import AreaRowWrapper from "../containers/AreaRowWrapper";
import { Table } from "antd";
import { FormattedMessage, FormattedNumber } from "react-intl";

export default function QuotaInfoSection() {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;

  const { data: quotaDetail } = useSWR(
    memberId ? `/api/Affiliate/RangeUsage/${memberId}` : null,
    url =>
      axios(url).then(res => {
        let data = res.data.Data.AffilateRangeUsages.map((item, key) => {
          return {
            key,
            ...item
          };
        });
        return data;
      })
  );

  const COLUMN_CONFIG = [
    {
      title: <FormattedMessage id="MemberDetail.Fields.ReturnPoint" />,
      dataIndex: "ReturnPoint",
      key: "ReturnPoint"
    },
    {
      title: <FormattedMessage id="MemberDetail.Tabs.BasicData.Fields.Quota" />,
      dataIndex: "AffiliateRangeLimit",
      key: "AffiliateRangeLimit",
      render: value => <FormattedNumber value={value} />
    },
    {
      title: (
        <FormattedMessage id="MemberDetail.Tabs.BasicData.Fields.CurrentUser" />
      ),
      dataIndex: "UseCount",
      key: "UseCount",
      render: value => <FormattedNumber value={value} />
    }
  ];

  return (
    <AreaRowWrapper title="QuotaInfo">
      <Table
        dataSource={quotaDetail}
        columns={COLUMN_CONFIG}
        bordered
        size="small"
        pagination={false}
        style={{ width: "100%", padding: "0 12px" }}
        scroll={{ y: 160 }}
      />
    </AreaRowWrapper>
  );
}
