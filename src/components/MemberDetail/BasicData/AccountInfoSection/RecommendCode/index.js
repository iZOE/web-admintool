import React from "react";
import { Typography } from "antd";
import ColumnItemWrapper from "../../containers/ColumnItemWrapper";
import { NO_DATA } from "constants/noData";

export default function RecommendCode({ data }) {
  return (
    <ColumnItemWrapper label="RecommendCode">
      {data ? (
        <Typography.Text copyable>{data}</Typography.Text>
      ) : (
        <span className="ant-form-text">{NO_DATA}</span>
      )}
    </ColumnItemWrapper>
  );
}
