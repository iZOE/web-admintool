import React, { useContext } from "react";
import { Col } from "antd";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import NickNameFormItem from "components/Member/NickNameFormItem";
import StaticFieldItem from "components/Member/StaticFieldItem";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function NickName({ data }) {
  const { isEditMode } = useContext(MemberDetailContext);
  const isEditable = isEditMode;

  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditable ? (
        <NickNameFormItem />
      ) : (
        <StaticFieldItem fieldName="NickName" value={data} />
      )}
    </Col>
  );
}
