import React from "react";
import ColumnItemWrapper from "../../containers/ColumnItemWrapper";

export default function MemberId({ data }) {
  return (
    <ColumnItemWrapper label="Id">
      <span className="ant-form-text">{data}</span>
    </ColumnItemWrapper>
  );
}
