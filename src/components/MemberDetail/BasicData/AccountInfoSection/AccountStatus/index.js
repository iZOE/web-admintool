import React, { useContext } from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import StatusBadge from "components/Member/StatusBadge";
import AccountStatusFormItem from "components/Member/AccountStatusFormItem";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function AccountStatus({ status, text }) {
  const { isEditMode } = useContext(MemberDetailContext);

  return (
    <Col span={FORM_COLUMN.HALF}>
      {isEditMode ? (
        <AccountStatusFormItem />
      ) : (
        <Form.Item
          label={<FormattedMessage id="MemberDetail.Fields.AccountStatus" />}
        >
          <StatusBadge status={status} text={text} />
        </Form.Item>
      )}
    </Col>
  );
}
