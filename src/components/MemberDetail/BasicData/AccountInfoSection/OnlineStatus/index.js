import React from "react";
import { Col, Form } from "antd";
import { FormattedMessage } from "react-intl";
import OnlineStatusBadge from "components/Member/OnlineStatusBadge";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";

export default function OnlineStatus({ status, text }) {
  return (
    <Col span={FORM_COLUMN.HALF}>
      <Form.Item
        label={<FormattedMessage id="MemberDetail.Fields.OnlineStatus" />}
        valuePropName="checked"
      >
        <OnlineStatusBadge status={status} text={text} />
      </Form.Item>
    </Col>
  );
}
