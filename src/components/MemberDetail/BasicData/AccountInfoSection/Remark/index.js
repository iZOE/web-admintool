import React, { useContext } from "react";
import { Col } from "antd";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";
import RemarkFormItem from "components/Member/RemarkFormItem";
import StaticFieldItem from "components/Member/StaticFieldItem";

export default function Remark({ data }) {
  const { isEditMode } = useContext(MemberDetailContext);
  const isEditable = isEditMode;

  return (
    <Col span={FORM_COLUMN.FULL}>
      {isEditable ? (
        <RemarkFormItem />
      ) : (
        <StaticFieldItem fieldName="Remark" value={data} />
      )}
    </Col>
  );
}
