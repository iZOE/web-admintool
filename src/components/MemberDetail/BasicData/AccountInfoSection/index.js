import React, { useContext, lazy } from 'react'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import { AGENT_TYPE } from 'constants/agentType'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import AreaRowWrapper from '../containers/AreaRowWrapper'
import MemberId from './MemberId'
import MemberAccount from './MemberAccount'
import MemberAvatar from './MemberAvatar'
import MemberType from './MemberType'
import NickName from './NickName'
import AccountStatus from './AccountStatus'
import OnlineStatus from './OnlineStatus'
import CreateInfo from './CreateInfo'
import LastLoginInfo from './LastLoginInfo'
import Remark from './Remark'

const RecommendCodeAsync = lazy(() => import('./RecommendCode'))
const validatedAgentList = [
    AGENT_TYPE.SHOU_MI,
    AGENT_TYPE.VT_999,
    AGENT_TYPE.CN_999,
]

export default function AccountInfoSection({ detail }) {
    const { memberIdentity: m } = useContext(MemberDetailContext)
    const isValidatedAgent = validatedAgentList.includes(m.agentType.id)
    const isValidatedAffiliateType =
        m.affiliateType.id !== AFFILIATE_LEVEL_TYPE.MEMBER
    const hasRecommendCode = isValidatedAgent && isValidatedAffiliateType

    return (
        m && (
            <AreaRowWrapper title="AccountInfo">
                <MemberId data={detail.Id} />
                <MemberAvatar data={detail.MemberPicPath} />
                <MemberAccount data={detail.MemberName} />
                <NickName data={detail.NickName} />
                <AccountStatus
                    status={detail.Status}
                    text={detail.StatusText}
                />
                <OnlineStatus
                    status={detail.OnlineStatus}
                    text={detail.OnlineStatusText}
                />
                <MemberType
                    data={{
                        id: detail.MemberTypeId,
                        text: detail.MemberTypeName,
                    }}
                />
                {hasRecommendCode && (
                    <RecommendCodeAsync data={detail.RecommendCode} />
                )}
                <CreateInfo
                    data={{
                        time: detail.CreateTime,
                        ip: detail.RegisterIp,
                        info: detail.RegisterIPInfo,
                    }}
                />
                <LastLoginInfo
                    data={{
                        time: detail.PreLoginTime,
                        ip: detail.CurrLoginIp,
                        info: detail.CurrLoginIPInfo,
                    }}
                />
                <Remark data={detail.Remarks} />
            </AreaRowWrapper>
        )
    )
}
