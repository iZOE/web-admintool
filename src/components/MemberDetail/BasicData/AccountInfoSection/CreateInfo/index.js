import React from 'react'
import { Space, Spin } from 'antd'
import ColumnItemWrapper from '../../containers/ColumnItemWrapper'
import DateWithFormat from 'components/DateWithFormat'
import { NO_DATA } from 'constants/noData'
import { COLUMN } from '../../constants/layoutSetting'

export default function CreateInfo({ data }) {
    return data ? (
        <ColumnItemWrapper label="CreateInfo" span={COLUMN.FULL}>
            <Space>
                <span className="ant-form-text">
                    <DateWithFormat time={data.time} />
                </span>
                <span className="ant-form-text">
                    IP: {data.ip ? data.ip : NO_DATA} (
                    {!data.info?.Country || data.info?.Country === ''
                        ? NO_DATA
                        : data.info.Country}
                    |
                    {!data.info?.RegionName || data.info?.RegionName === ''
                        ? NO_DATA
                        : data.info.RegionName}
                    |
                    {!data.info?.City || data.info?.City === ''
                        ? NO_DATA
                        : data.info.City}
                    |
                    {!data.info?.DeviceTypeName ||
                    data.info?.DeviceTypeName === ''
                        ? NO_DATA
                        : data.info.DeviceTypeName}
                    )
                </span>
            </Space>
        </ColumnItemWrapper>
    ) : (
        <Spin />
    )
}
