import React from "react";
import { Tag } from "antd";
import ColumnItemWrapper from "../../containers/ColumnItemWrapper";
import { MEMBER_TYPE } from "constants/memberDetail/memberType";
import { NO_DATA } from "constants/noData";

const getColor = type => {
  return type === MEMBER_TYPE.INTERNAL ? "volcano" : "cyan";
};

export default function MemberType({ data }) {
  const color = getColor(data.id);

  return (
    <ColumnItemWrapper label="MemberType">
      {data.text ? (
        <Tag color={color}>{data.text}</Tag>
      ) : (
        <span className="ant-form-text">{NO_DATA}</span>
      )}
    </ColumnItemWrapper>
  );
}
