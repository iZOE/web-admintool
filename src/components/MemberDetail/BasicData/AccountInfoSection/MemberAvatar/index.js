import React, { useContext } from "react";
import { Avatar, Space, Checkbox, Form } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import ColumnItemWrapper from "../../containers/ColumnItemWrapper";
import { FormattedMessage } from "react-intl";

export default function MemberAvatar({ data: imgUrl }) {
  const { isEditMode } = useContext(MemberDetailContext);
  const isEditable = isEditMode;

  return (
    <ColumnItemWrapper label="Avatar">
      <Space>
        {imgUrl ? <Avatar src={imgUrl} /> : <Avatar icon={<UserOutlined />} />}
        {isEditable && (
          <Form.Item noStyle name="IsDeleteAvatar" valuePropName="checked">
            <Checkbox>
              <FormattedMessage id="MemberDetail.Tabs.BasicData.Fields.ResetAvatar" />
            </Checkbox>
          </Form.Item>
        )}
      </Space>
    </ColumnItemWrapper>
  );
}
