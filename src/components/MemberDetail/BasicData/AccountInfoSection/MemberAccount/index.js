import React from "react";
import { Col } from "antd";
import { FORM_COLUMN } from "constants/memberDetail/layoutSetting";
import StaticFieldItem from "components/Member/StaticFieldItem";

export default function MemberAccount({ data }) {
  return (
    <Col span={FORM_COLUMN.HALF}>
      <StaticFieldItem fieldName="MemberAccount" value={data} />
    </Col>
  );
}
