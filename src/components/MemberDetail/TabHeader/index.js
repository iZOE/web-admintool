import React, { useContext } from 'react';
import { Tabs } from 'antd';
import { FormattedMessage } from 'react-intl';
import { TAB_INFO } from 'constants/memberDetail/tabInfo';
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType';
import * as PERMISSION from 'constants/permissions';
import usePermission from 'hooks/usePermission';
import { MemberDetailContext } from 'contexts/MemberDetailContext';

const {
  MEMBERS_MEMBERS_VIEW_PARTIAL_MEMBER_BANKCARD,
  MEMBERS_AFFILIATES_VIEW_PARTIAL_AFFILIATE_BANKCARD,
  MEMBERS_MEMBERS_VIEW_ENTIRE_MEMBER_BANKCARD,
  MEMBERS_AFFILIATES_VIEW_ENTIRE_AFFILIATE_BANKCARD,
} = PERMISSION;

export default function TabHeader({ onSelectTab, currentTab }) {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const currentAffiliateTypeId = m && m.affiliateType.id;

  const [
    hasViewMemberBankDataPermission,
    hasViewAffiliateBankDataPermission,
    hasViewMemberEntireBankDataPermission,
    hasViewAffiliateEntireBankDataPermission,
  ] = usePermission(
    MEMBERS_MEMBERS_VIEW_PARTIAL_MEMBER_BANKCARD,
    MEMBERS_AFFILIATES_VIEW_PARTIAL_AFFILIATE_BANKCARD,
    MEMBERS_MEMBERS_VIEW_ENTIRE_MEMBER_BANKCARD,
    MEMBERS_AFFILIATES_VIEW_ENTIRE_AFFILIATE_BANKCARD
  );

  const hasMemberBankDataTab =
    currentAffiliateTypeId === AFFILIATE_LEVEL_TYPE.MEMBER &&
    (hasViewMemberBankDataPermission || hasViewMemberEntireBankDataPermission);
  const hasAffiliateBankDataTab =
    currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER &&
    (hasViewAffiliateBankDataPermission || hasViewAffiliateEntireBankDataPermission);

  return (
    <Tabs
      defaultActiveKey={currentTab}
      onChange={key => {
        onSelectTab(key);
      }}
    >
      <Tabs.TabPane
        tab={<FormattedMessage id={'MemberDetail.Tabs.' + TAB_INFO.BASIC_DATA + '.Title'} />}
        key={TAB_INFO.BASIC_DATA}
      />
      {(hasMemberBankDataTab || hasAffiliateBankDataTab) && (
        <Tabs.TabPane
          tab={<FormattedMessage id={'MemberDetail.Tabs.' + TAB_INFO.BANK_DATA + '.Title'} />}
          key={TAB_INFO.BANK_DATA}
        />
      )}
      <Tabs.TabPane
        tab={<FormattedMessage id={'MemberDetail.Tabs.' + TAB_INFO.ACCOUNT_SETTING + '.Title'} />}
        key={TAB_INFO.ACCOUNT_SETTING}
      />
      <Tabs.TabPane
        tab={<FormattedMessage id={'MemberDetail.Tabs.' + TAB_INFO.ACCOUNT_SECURITY + '.Title'} />}
        key={TAB_INFO.ACCOUNT_SECURITY}
      />
      <Tabs.TabPane tab={<FormattedMessage id={'PageTurnover.Title'} />} key={TAB_INFO.TURNOVER_LOG} />
      <Tabs.TabPane
        tab={<FormattedMessage id={'MemberDetail.Tabs.' + TAB_INFO.SUMMARIZED_LOG + '.Title'} />}
        key={TAB_INFO.SUMMARIZED_LOG}
      />
      <Tabs.TabPane
        tab={<FormattedMessage id={'MemberDetail.Tabs.' + TAB_INFO.CHANGE_LOG + '.Title'} />}
        key={TAB_INFO.CHANGE_LOG}
      />
    </Tabs>
  );
}
