import React, { useContext } from "react";
import {
  Empty,
  Descriptions,
  Typography,
  Button,
  Popconfirm,
  message
} from "antd";
import { DisconnectOutlined, DeleteFilled } from "@ant-design/icons";
import { useIntl, FormattedMessage } from "react-intl";
import { trigger } from "swr";
import caller from "utils/fetcher";
import { MemberDetailContext } from "contexts/MemberDetailContext";

export default function SecurityQuestionSection({ securityDetail }) {
  const intl = useIntl();
  const { memberIdentity: m, isEditMode } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;

  const successMsg = intl.formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });

  const confirm = () => {
    caller({
      method: "delete",
      endpoint: `/api/Member/Security/SecQue/${memberId}`
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        trigger(`/api/Member/Security/${memberId}`);
      })
      .catch(err => {
        console.error(err);
      });
  };

  return (
    <Descriptions
      title={
        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Areas.SecurityQuestion" />
      }
      column={2}
    >
      {!!securityDetail.Quests.length ? (
        <>
          {securityDetail.Quests.map((item, key) => (
            <React.Fragment key={key}>
              <Descriptions.Item
                label={
                  <FormattedMessage
                    id="MemberDetail.Tabs.AccountSecurity.Fields.Question"
                    values={{ id: key + 1 }}
                  />
                }
              >
                <Typography.Text>{item.SecQueName}</Typography.Text>
              </Descriptions.Item>
              <Descriptions.Item
                label={
                  <FormattedMessage
                    id="MemberDetail.Tabs.AccountSecurity.Fields.Answer"
                    values={{ id: key + 1 }}
                  />
                }
              >
                <Typography.Text>{item.SecurityAnswer}</Typography.Text>
              </Descriptions.Item>
            </React.Fragment>
          ))}
          {isEditMode && (
            <Descriptions.Item>
              <Popconfirm
                placement="top"
                title={
                  <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Messages.ConfirmToRemove" />
                }
                icon={<DeleteFilled style={{ color: "red" }} />}
                onConfirm={confirm}
                okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
                cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
              >
                <Button
                  type="primary"
                  danger
                  icon={<DisconnectOutlined style={{ marginRight: 8 }} />}
                >
                  <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Actions.RemoveSecurityQuestionSetting" />
                </Button>
              </Popconfirm>
            </Descriptions.Item>
          )}
        </>
      ) : (
        <Descriptions.Item className="merged-column" span={2}>
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={
              <FormattedMessage id="Share.EmptyMessage.WithoutSetting" />
            }
          />
        </Descriptions.Item>
      )}
    </Descriptions>
  );
}
