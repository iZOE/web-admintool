import React, { useContext } from 'react'
import { Descriptions, Card, List, Empty } from 'antd'
import { FormattedMessage } from 'react-intl'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import DateWithFormat from 'components/DateWithFormat'
import RemoveButton from './RemoveButton'

export default function DeviceDetailSection({ deviceDetail }) {
    const { isEditMode } = useContext(MemberDetailContext)

    return (
        <Descriptions
            title={
                <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Areas.DeviceBinding" />
            }
            column={2}
        >
            {!!deviceDetail.length ? (
                deviceDetail.map(device => (
                    <React.Fragment key={device.MemberDeviceId}>
                        <Descriptions.Item>
                            <Card
                                actions={
                                    isEditMode && [
                                        <RemoveButton
                                            deviceId={device.MemberDeviceId}
                                        />,
                                    ]
                                }
                            >
                                <Card.Meta
                                    key={device.MemberDeviceId}
                                    title={device.DeviceName}
                                    description={
                                        <List>
                                            <List.Item>
                                                <List.Item.Meta
                                                    title={
                                                        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Fields.CreateInfo" />
                                                    }
                                                />
                                                <DateWithFormat
                                                    time={device.CreateTime}
                                                />{' '}
                                                ({device.RegisterDeviceIp})
                                            </List.Item>
                                            <List.Item>
                                                <List.Item.Meta
                                                    title={
                                                        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Fields.LastLoginInfo" />
                                                    }
                                                />
                                                <DateWithFormat
                                                    time={device.LastLoginTime}
                                                />{' '}
                                                ({device.LastLoginIp})
                                            </List.Item>
                                        </List>
                                    }
                                />
                            </Card>
                        </Descriptions.Item>
                    </React.Fragment>
                ))
            ) : (
                <Descriptions.Item className="merged-column" span={2}>
                    <Empty
                        image={Empty.PRESENTED_IMAGE_SIMPLE}
                        description={
                            <FormattedMessage id="Share.EmptyMessage.WithoutSetting" />
                        }
                    />
                </Descriptions.Item>
            )}
        </Descriptions>
    )
}
