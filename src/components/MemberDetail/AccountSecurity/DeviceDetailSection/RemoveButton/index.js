import React, { useContext } from "react";
import { Space, Popconfirm, message } from "antd";
import { trigger } from "swr";
import caller from "utils/fetcher";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import { useIntl, FormattedMessage } from "react-intl";
import { DisconnectOutlined, DeleteFilled } from "@ant-design/icons";

export default function RemoveButton({ deviceId }) {
  const intl = useIntl();
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;

  const successMsg = intl.formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });

  const confirm = () => {
    caller({
      method: "delete",
      endpoint: `/api/Member/Security/Device/${memberId}/${deviceId}`
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        trigger(`/api/Member/Security/${memberId}`);
      })
      .catch(err => {
        console.error(err);
      });
  };

  return (
    <Popconfirm
      placement="top"
      title={
        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Messages.ConfirmToRemove" />
      }
      icon={<DeleteFilled style={{ color: "red" }} />}
      onConfirm={confirm}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Space key="remove">
        <DisconnectOutlined />
        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Actions.Remove" />
      </Space>
    </Popconfirm>
  );
}
