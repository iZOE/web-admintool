import React, { useContext } from "react";
import useSWR from "swr";
import axios from "axios";
import { Divider, Skeleton } from "antd";
import SecuritySettingSection from "./SecuritySettingSection";
import DeviceDetailSection from "./DeviceDetailSection";
import SecurityQuestionSection from "./SecurityQuestionSection";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import { Wrapper } from "./Styled";

export default function AccountSecurity() {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;

  const { data: securityDetail } = useSWR(
    memberId ? `/api/Member/Security/${memberId}` : null,
    url => axios(url).then(res => res && res.data.Data)
  );

  return (
    <Wrapper>
      {!!securityDetail ? (
        <>
          <SecuritySettingSection settingDetail={securityDetail.Settings} />
          <Divider dashed />
          <DeviceDetailSection deviceDetail={securityDetail.Devices} />
          <Divider dashed />
          <SecurityQuestionSection securityDetail={securityDetail} />
        </>
      ) : (
        <Skeleton active />
      )}
    </Wrapper>
  );
}
