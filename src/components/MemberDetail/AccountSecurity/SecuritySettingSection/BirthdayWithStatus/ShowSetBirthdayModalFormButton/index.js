import React, { useState, lazy } from "react";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";

const SetBirthdayModalFormAsync = lazy(() => import("../SetBirthdayModalForm"));

export default function ShowSetBirthdayModalFormButton() {
  const [isShownModal, setIsShownModal] = useState(false);

  return (
    <>
      <Button
        type="primary"
        onClick={() => {
          setIsShownModal(true);
        }}
      >
        <FormattedMessage
          id={"MemberDetail.Tabs.AccountSecurity.Actions.Set"}
        />
      </Button>

      {isShownModal && (
        <SetBirthdayModalFormAsync
          isVisible={isShownModal}
          onCloseModal={() => {
            setIsShownModal(false);
          }}
        />
      )}
    </>
  );
}
