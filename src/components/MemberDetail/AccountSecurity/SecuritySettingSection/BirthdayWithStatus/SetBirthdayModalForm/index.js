import React, { useContext } from 'react'
import { Modal, Form, DatePicker, message } from 'antd'
import { useIntl, FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'
import { trigger } from 'swr'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import { FORMAT } from 'constants/dateConfig'
import { getISODateTimeString } from 'mixins/dateTime'
import DoubleConfirmButton from 'components/FormActionButtons/DoubleConfirmButton'

export default function SetBirthdayModalForm({ isVisible, onCloseModal }) {
    const intl = useIntl()
    const { memberIdentity: m } = useContext(MemberDetailContext)
    const memberId = m && m.memberData.id

    const [form] = Form.useForm()
    const layout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })

    const onModalSave = ({ Birthday }) => {
        const birthday = getISODateTimeString(Birthday, FORMAT.DEFAULT.DAILY)

        const payload = {
            birthday,
        }

        caller({
            method: 'post',
            endpoint: `/api/Member/Security/SetBirthday/${memberId}`,
            body: payload,
        })
            .then(() => {
                message.success(successMsg, 5)
            })
            .then(() => {
                trigger(`/api/Member/Security/${memberId}`)
            })
            .catch(err => {
                console.error('Set Member Birthday Error: ', err?.data)
            })
            .finally(() => {
                onCloseModal()
            })
    }

    const onOk = () => {
        form.submit()
    }

    const onCancel = () => {
        form.resetFields()
        onCloseModal()
    }

    return (
        <Modal
            centered
            destroyOnClose
            visible={isVisible}
            footer={[
                <DoubleConfirmButton
                    key="ok"
                    onOk={onOk}
                    onCancel={onCancel}
                    confirmMsg={
                        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Messages.ConfirmToSetBirthday" />
                    }
                />,
            ]}
            onCancel={onCancel}
            title={
                <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Fields.SetBirthday" />
            }
            cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
        >
            <Form {...layout} form={form} onFinish={onModalSave}>
                <Form.Item
                    name="Birthday"
                    label={
                        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Fields.Birthday" />
                    }
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: (
                                <FormattedMessage id="Share.FormValidate.Required.Input" />
                            ),
                        },
                    ]}
                >
                    <DatePicker
                        style={{ width: '100%' }}
                        format={FORMAT.DISPLAY.DAILY}
                    />
                </Form.Item>
            </Form>
        </Modal>
    )
}
