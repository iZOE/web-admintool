import React, { lazy, useContext } from 'react'
import moment from 'moment'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import SettingRowWrapper from '../components/SettingRowWrapper'
import { SETTING_STATUS } from '../constants/settingStatus'
import { FORMAT } from 'constants/dateConfig'

const ShowSetBirthdayModalFormButtonAsync = lazy(() =>
    import('./ShowSetBirthdayModalFormButton')
)

export default function BirthdayWithStatus({ data }) {
    const { isEditMode } = useContext(MemberDetailContext)
    const isEditablt = isEditMode && !data

    return (
        <SettingRowWrapper
            fieldName="Birthday"
            data={data && moment(data).format(FORMAT.DISPLAY.DAILY)}
            status={data ? SETTING_STATUS.Set : SETTING_STATUS.NotSet}
        >
            {isEditablt && <ShowSetBirthdayModalFormButtonAsync />}
        </SettingRowWrapper>
    )
}
