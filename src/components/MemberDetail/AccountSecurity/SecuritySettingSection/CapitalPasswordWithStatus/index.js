import React, { useContext, lazy } from 'react'
import { SETTING_STATUS } from '../constants/settingStatus'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import usePermission from 'hooks/usePermission'
import * as PERMISSION from 'constants/permissions'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import SettingRowWrapper from '../components/SettingRowWrapper'

const {
    MEMBERS_MEMBERS_FUNDS_PASSWORD,
    MEMBERS_AFFILIATES_FUNDS_PASSWORD,
} = PERMISSION
const SendResetCapitalPasswordMailButtonAsync = lazy(() =>
    import('./SendResetCapitalPasswordMailButton')
)

export default function CapitalPasswordWithStatus({ data, isEmailValified }) {
    const [
        editMemberCapitalPasswordPermission,
        editAffiliateCapitalPasswordPermission,
    ] = usePermission(
        MEMBERS_MEMBERS_FUNDS_PASSWORD,
        MEMBERS_AFFILIATES_FUNDS_PASSWORD
    )
    const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext)
    const currentAffiliateTypeId = m && m.affiliateType.id
    const isAffiliateMember =
        currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER
    const isEditable =
        data &&
        isEditMode &&
        (isAffiliateMember
            ? editAffiliateCapitalPasswordPermission
            : editMemberCapitalPasswordPermission)

    return (
        <SettingRowWrapper
            fieldName="CapitalPassword"
            data={data}
            status={data ? SETTING_STATUS.Set : SETTING_STATUS.NotSet}
        >
            {isEditable && (
                <SendResetCapitalPasswordMailButtonAsync
                    isEmailValified={isEmailValified}
                />
            )}
        </SettingRowWrapper>
    )
}
