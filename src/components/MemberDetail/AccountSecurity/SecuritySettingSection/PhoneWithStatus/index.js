import React, { useContext, lazy } from "react";
import { SETTING_STATUS } from "../constants/settingStatus";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import SettingRowWrapper from "../components/SettingRowWrapper";

const ResetPhoneButtonAsync = lazy(() => import("./ResetPhoneButton"));

const getStatus = (data, isPhoneVerified) => {
  if (!data) {
    return SETTING_STATUS.NotSet;
  } else {
    return isPhoneVerified
      ? SETTING_STATUS.Verified
      : SETTING_STATUS.NotVerified;
  }
};

export default function PhoneWithStatus({ data, isPhoneVerified }) {
  const { isEditMode } = useContext(MemberDetailContext);
  const status = getStatus(data, isPhoneVerified);
  const isEditable = isEditMode && status === SETTING_STATUS.Verified;

  return (
    <SettingRowWrapper fieldName="Phone" data={data} status={status}>
      {isEditable && <ResetPhoneButtonAsync />}
    </SettingRowWrapper>
  );
}
