import React, { useContext } from "react";
import { Button, Popconfirm, message } from "antd";
import { AlertFilled } from "@ant-design/icons";
import { useIntl, FormattedMessage } from "react-intl";
import caller from "utils/fetcher";
import { trigger } from "swr";
import { MemberDetailContext } from "contexts/MemberDetailContext";

export default function ResetMailButton() {
  const intl = useIntl();
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;

  const successMsg = intl.formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });
  const errorMsg = intl.formatMessage({
    id: "Share.ErrorMessage.UnknownError"
  });

  const confirm = () => {
    caller({
      method: "delete",
      endpoint: `/api/Member/Security/Email/${memberId}`
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        trigger(`/api/Member/Security/${memberId}`);
      })
      .catch(err => {
        console.error("Reset Member Email Error: ", err);
        message.error(errorMsg, 5);
      });
  };

  return (
    <Popconfirm
      placement="top"
      title={
        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Messages.ConfirmToResetMail" />
      }
      icon={<AlertFilled />}
      onConfirm={confirm}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Button type="primary">
        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Actions.Remove" />
      </Button>
    </Popconfirm>
  );
}
