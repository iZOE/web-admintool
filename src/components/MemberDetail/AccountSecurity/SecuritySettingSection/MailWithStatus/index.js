import React, { useContext, lazy } from "react";
import { SETTING_STATUS } from "../constants/settingStatus";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import SettingRowWrapper from "../components/SettingRowWrapper";

const ResetMailButtonAsync = lazy(() => import("./ResetMailButton"));

export default function MailWithStatus({ data, status }) {
  const { isEditMode } = useContext(MemberDetailContext);
  const isEditable = isEditMode && status === SETTING_STATUS.Verified;

  return (
    <SettingRowWrapper fieldName="Email" data={data} status={status}>
      {isEditable && <ResetMailButtonAsync />}
    </SettingRowWrapper>
  );
}
