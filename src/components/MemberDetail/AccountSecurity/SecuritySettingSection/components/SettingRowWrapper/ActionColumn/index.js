import React from "react";
import { Col } from "antd";

export default function ActionColumn({ children }) {
  return <Col span={6}>{children}</Col>;
}
