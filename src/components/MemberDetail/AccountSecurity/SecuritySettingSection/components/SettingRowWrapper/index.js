import React from "react";
import { Form, Row } from "antd";
import InfoColumn from "./InfoColumn";
import StatusColumn from "./StatusColumn";
import ActionColumn from "./ActionColumn";

export default function SettingRowWrapper({
  fieldName,
  data,
  status,
  children
}) {
  return (
    <Form size="middle">
      <Row>
        <InfoColumn fieldName={fieldName} data={data} />
        <StatusColumn status={status} />
        <ActionColumn>{children}</ActionColumn>
      </Row>
    </Form>
  );
}
