import React from "react";
import { Col, Form, Typography } from "antd";
import { FormattedMessage } from "react-intl";
import { NO_DATA } from "constants/noData";

export default function InfoColumn({ fieldName, data }) {
  return (
    <Col span={15}>
      <Form.Item
        labelCol={{ span: 6 }}
        label={
          <FormattedMessage
            id={"MemberDetail.Tabs.AccountSecurity.Fields." + fieldName}
          />
        }
      >
        <Typography.Text className="ant-form-text">
          {data ? data : NO_DATA}
        </Typography.Text>
      </Form.Item>
    </Col>
  );
}
