import React from "react";
import { Col, Tag, Form } from "antd";
import { FormattedMessage } from "react-intl";
import { SETTING_STATUS } from "../../../constants/settingStatus";

const getTagColor = status => {
  switch (status) {
    case SETTING_STATUS.Set:
      return "blue";
    case SETTING_STATUS.NotSet:
      return "default";
    case SETTING_STATUS.Verified:
      return "green";
    case SETTING_STATUS.NotVerified:
      return "orange";
    default:
      return "geekblue";
  }
};

export default function StatusColumn({ status }) {
  const color = getTagColor(status);

  return (
    <Col span={3}>
      <Form.Item>
        <Tag color={color}>
          <FormattedMessage
            id={
              "MemberDetail.Tabs.AccountSecurity.Tages." +
              Object.keys(SETTING_STATUS)[status]
            }
          />
        </Tag>
      </Form.Item>
    </Col>
  );
}
