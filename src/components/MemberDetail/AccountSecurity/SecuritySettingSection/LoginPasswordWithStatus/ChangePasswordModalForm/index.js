import React, { useContext } from "react";
import { Modal, Form, message } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import caller from "utils/fetcher";
import { trigger } from "swr";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import ConfirmPasswordFormItem from "components/Member/ConfirmPasswordFormItem";
import { AFFILIATE_LEVEL_TYPE } from "constants/memberDetail/memberType";

const MEMBER_PASSWORD_TYPE = {
  LOGIN: 1,
  CAPITAL: 2
};

export default function ChangePasswordModalForm({ isVisible, onCloseModal }) {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;
  const currentAffiliateTypeId = m && m.affiliateType.id;
  const isAffiliateMember =
    currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER;

  const intl = useIntl();
  const [form] = Form.useForm();
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 }
  };

  const successMsg = intl.formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });
  const errorMsg = intl.formatMessage({
    id: "Share.ErrorMessage.UnknownError"
  });

  const onModalSave = values => {
    const payload = {
      MemberId: memberId,
      MemberPassTypeId: MEMBER_PASSWORD_TYPE.LOGIN,
      Password: values.Password
    };

    const endpoint = isAffiliateMember
      ? "/api/Affiliate/Security/Pass"
      : "/api/Member/Security/Pass";

    caller({
      method: "put",
      endpoint,
      body: payload
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        trigger(`/api/Member/Security/${memberId}`);
        onCloseModal();
      })
      .catch(err => {
        console.error("Update Member Password Error: ", err);
        message.error(errorMsg, 5);
        onCloseModal();
      });
  };

  const onCancel = () => {
    form.resetFields();
    onCloseModal();
  };

  return (
    <Modal
      centered
      destroyOnClose
      visible={isVisible}
      onOk={() => {
        form.submit();
      }}
      onCancel={onCancel}
      title={
        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Fields.ChangePassword" />
      }
      okText={<FormattedMessage id="Share.ActionButton.Submit" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Form {...layout} form={form} onFinish={onModalSave}>
        <ConfirmPasswordFormItem />
      </Form>
    </Modal>
  );
}
