import React, { useState, lazy } from "react";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";

const ChangePasswordModalFormAsync = lazy(() =>
  import("../ChangePasswordModalForm")
);

export default function ShowChangePasswordModalFormButton() {
  const [isShownModal, setIsShownModal] = useState(false);

  return (
    <>
      <Button
        type="primary"
        onClick={() => {
          setIsShownModal(true);
        }}
      >
        <FormattedMessage
          id={"MemberDetail.Tabs.AccountSecurity.Actions.Reset"}
        />
      </Button>

      {isShownModal && (
        <ChangePasswordModalFormAsync
          isVisible={isShownModal}
          onCloseModal={() => {
            setIsShownModal(false);
          }}
        />
      )}
    </>
  );
}
