import React, { useContext, lazy } from 'react'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import usePermission from 'hooks/usePermission'
import * as PERMISSION from 'constants/permissions'
import { AFFILIATE_LEVEL_TYPE } from 'constants/memberDetail/memberType'
import { SETTING_STATUS } from '../constants/settingStatus'
import SettingRowWrapper from '../components/SettingRowWrapper'

const {
    MEMBERS_MEMBERS_LOGIN_PASSWORD,
    MEMBERS_AFFILIATES_LOGIN_PASSWORD,
} = PERMISSION
const ShowChangePasswordModalFormButtonAsync = lazy(() =>
    import('./ShowChangePasswordModalFormButton')
)

export default function LoginPasswordWithStatus({ data }) {
    const [
        editMemberPasswordPermission,
        editAffiliatePasswordPermission,
    ] = usePermission(
        MEMBERS_MEMBERS_LOGIN_PASSWORD,
        MEMBERS_AFFILIATES_LOGIN_PASSWORD
    )
    const { isEditMode, memberIdentity: m } = useContext(MemberDetailContext)
    const currentAffiliateTypeId = m && m.affiliateType.id
    const isAffiliateMember =
        currentAffiliateTypeId !== AFFILIATE_LEVEL_TYPE.MEMBER
    const isEditable =
        isEditMode &&
        (isAffiliateMember
            ? editAffiliatePasswordPermission
            : editMemberPasswordPermission)

    return (
        <>
            <SettingRowWrapper
                fieldName="LoginPassword"
                data={data}
                status={data ? SETTING_STATUS.Set : SETTING_STATUS.NotSet}
            >
                {isEditable && <ShowChangePasswordModalFormButtonAsync />}
            </SettingRowWrapper>
        </>
    )
}
