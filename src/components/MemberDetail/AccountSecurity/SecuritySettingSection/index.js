import React from "react";
import { Descriptions, Skeleton } from "antd";
import { FormattedMessage } from "react-intl";
import LoginPasswordWithStatus from "./LoginPasswordWithStatus";
import CapitalPasswordWithStatus from "./CapitalPasswordWithStatus";
import MailWithStatus from "./MailWithStatus";
import PhoneWithStatus from "./PhoneWithStatus";
import QqWithStatus from "./QqWithStatus";
import WechatWithStatus from "./WechatWithStatus";
import BirthdayWithStatus from "./BirthdayWithStatus";
import { SETTING_STATUS } from "./constants/settingStatus";

export default function SecuritySettingSection({ settingDetail }) {
  return settingDetail ? (
    <>
      <Descriptions
        title={
          <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Areas.SecuritySetting" />
        }
      />
      <LoginPasswordWithStatus data={settingDetail.LoginPassword} />
      <CapitalPasswordWithStatus
        data={settingDetail.MoneyPassword}
        isEmailValified={
          settingDetail.EmailVerified === SETTING_STATUS.Verified
        }
      />
      <MailWithStatus
        data={settingDetail.Email}
        status={settingDetail.EmailVerified}
      />
      <PhoneWithStatus
        data={settingDetail.ContactNumber}
        isPhoneVerified={!!settingDetail.ContactVerifiedTime}
      />
      <WechatWithStatus
        data={settingDetail.Wechat}
        isWechatVerified={settingDetail.WechatIsVerified}
      />
      <QqWithStatus
        data={settingDetail.QQ}
        isQqVerified={settingDetail.QQIsVerified}
      />
      <BirthdayWithStatus data={settingDetail.Birthday} />
    </>
  ) : (
    <Skeleton active />
  );
}
