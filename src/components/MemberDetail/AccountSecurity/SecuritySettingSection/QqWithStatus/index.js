import React, { useContext, lazy } from "react";
import { SETTING_STATUS } from "../constants/settingStatus";
import { CONTACT_TYPE } from "../constants/contactType";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import SettingRowWrapper from "../components/SettingRowWrapper";

const UpdateContactStatusButtonAsync = lazy(() =>
  import("../components/UpdateContactStatusButton")
);

const getStatus = (data, isVerified) => {
  if (!data) {
    return SETTING_STATUS.NotSet;
  } else {
    return isVerified ? SETTING_STATUS.Verified : SETTING_STATUS.NotVerified;
  }
};

export default function QqWithStatus({ data, isQqVerified }) {
  const { isEditMode } = useContext(MemberDetailContext);
  const status = getStatus(data, isQqVerified);
  const isEditable = isEditMode && data;

  return (
    <SettingRowWrapper fieldName="Qq" data={data} status={status}>
      {isEditable && (
        <UpdateContactStatusButtonAsync
          contactType={CONTACT_TYPE.Qq}
          account={data}
          isVerified={isQqVerified}
        />
      )}
    </SettingRowWrapper>
  );
}
