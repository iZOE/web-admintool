import React, { useContext } from "react";
import { Button, Popconfirm, message } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import caller from "utils/fetcher";
import { trigger } from "swr";
import { MemberDetailContext } from "contexts/MemberDetailContext";

const ACTION_TYPE = {
  Bind: 0,
  Unbind: 1
};

export default function UpdateQqStatusButton({ account, isVerified }) {
  const intl = useIntl();
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const memberId = m && m.memberData.id;
  const currentActionType = isVerified ? ACTION_TYPE.Unbind : ACTION_TYPE.Bind;

  const successMsg = intl.formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });
  const errorMsg = intl.formatMessage({
    id: "Share.ErrorMessage.UnknownError"
  });

  const onConfirm = () => {
    const payload = {
      MemberId: memberId,
      IsQQ: true,
      IsBinding: !isVerified,
      AccountNumber: account
    };

    caller({
      method: "post",
      endpoint: "/api/Member/BindQqWeChat",
      body: payload
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        trigger(`/api/Member/Security/${memberId}`);
      })
      .catch(err => {
        console.error("Bind QQ Error: ", err);
        message.error(errorMsg, 5);
      });
  };

  return (
    <Popconfirm
      placement="top"
      title={
        <FormattedMessage id="MemberDetail.Tabs.AccountSecurity.Messages.ConfirmToUpdateQQStatus" />
      }
      onConfirm={onConfirm}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Button type="primary">
        <FormattedMessage
          id={
            "MemberDetail.Tabs.AccountSecurity.Actions." +
            Object.keys(ACTION_TYPE)[currentActionType]
          }
        />
      </Button>
    </Popconfirm>
  );
}
