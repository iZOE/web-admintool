export const SETTING_STATUS = {
  NotSet: 0,
  NotVerified: 1,
  Verified: 2,
  Set: 3
};
