import React, { useState } from "react";
import { Pagination, Skeleton } from "antd";
import { FormattedMessage } from "react-intl";

// components中的Pagination和PaginationByGet兩個的差異在於get是新版api分頁格式
// 日後如果有遇到搜尋條件的接口為get的請調用PaginationByGet
export default function PaginationComponent({
  totalCount,
  pageSizeOptions,
  onUpdateCondition,
}) {
  const [currentPage, setCurrentPage] = useState(1);
  if (!totalCount && totalCount !== 0) {
    return (
      <Skeleton
        active
        title={false}
        paragraph={false}
        paragraph={{ rows: 1 }}
      />
    );
  }
  return (
    <Pagination
      showSizeChanger={true}
      onChange={(page, pageSize) => {
        setCurrentPage(page);
        onUpdateCondition({
          "paginationInfo.pageNumber": page,
          "paginationInfo.pageSize": pageSize,
        });
      }}
      onShowSizeChange={(current, size) => {
        onUpdateCondition({
          "paginationInfo.pageNumber": current,
          "paginationInfo.pageSize": size,
        });
      }}
      defaultPageSize={Number(pageSizeOptions[1])}
      current={currentPage}
      pageSizeOptions={pageSizeOptions}
      total={totalCount}
      showTotal={(total, range) => (
        <FormattedMessage
          id="Share.Table.Footer"
          values={{
            first: range[0],
            last: range[1],
            total: total,
          }}
        />
      )}
      showQuickJumper
    />
  );
}
