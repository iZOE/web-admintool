import React, { useMemo, useState, useEffect } from 'react'
import { Form, TreeSelect } from 'antd'
import { FormattedMessage } from 'react-intl'

const { SHOW_CHILD } = TreeSelect

const defaultRenderOne = d => ({
    title: d.Name,
    value: d.Id,
    key: d.Id,
})

export default function WithSelectAllCheckBoxs({
    data,
    getPopupContainer,
    whenSelectEmptyPlaceHolder,
    howToRenderOne, // 指定 title, value, key 各要是 一筆資料裡的哪個欄位,
    ...restProps
}) {
    const treeData = [
        {
            title: (
                <FormattedMessage id="Share.Dropdown.All" description="全部" />
            ),
            value: 'all',
            key: 'all',
            children: data
                ? (howToRenderOne && data.map(howToRenderOne)) ||
                  data.map(defaultRenderOne)
                : [],
        },
    ]

    return (
        <Form.Item {...restProps}>
            {data && (
                <TreeSelect
                    getPopupContainer={getPopupContainer}
                    treeData={treeData}
                    treeCheckable
                    showCheckedStrategy={SHOW_CHILD}
                    placeholder={
                        <FormattedMessage
                            id="Share.PlaceHolder.Input.PleaseSelectSomething"
                            description="請選擇"
                            values={{
                                field: whenSelectEmptyPlaceHolder,
                            }}
                        />
                    }
                    style={{
                        width: '100%',
                    }}
                    maxTagCount={1}
                    maxTagPlaceholder="..."
                />
            )}
        </Form.Item>
    )
}
