import React from 'react'
import { Descriptions, Space, Typography } from 'antd'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import RefreshButton from './RefreshButton'

const { Text } = Typography

export default function RefreshArea({ type, value, memberName }) {
    return (
        <Descriptions>
            <Descriptions.Item
                label={
                    <Text strong>
                        <FormattedMessage id={'PageTurnover.Fields.' + type} />
                    </Text>
                }
            >
                <Space>
                    <Text type="secondary">
                        <FormattedMessage
                            id="PageTurnover.Notes.CurrentRecord"
                            description="資料更新時間："
                        />
                        <DateWithFormat time={value} />
                    </Text>
                    <RefreshButton memberName={memberName} type={type} />
                </Space>
            </Descriptions.Item>
        </Descriptions>
    )
}
