import React, { useCallback } from "react";
import { trigger } from "swr";
import { Button } from "antd";
import { TransactionOutlined } from "@ant-design/icons";
import { FormattedMessage } from "react-intl";
import { RECORD_TYPE } from "constants/turnover";

export default function RefreshButton({ memberName, type }) {
  const onClick = useCallback(() => {
    let url =
      type === RECORD_TYPE.CURRENT
        ? `/api/v2/Turnover/${memberName}/Current`
        : "/api/v2/Turnover/Search";
    trigger(url);
  }, [memberName, type]);

  return (
    <Button
      type="primary"
      icon={<TransactionOutlined style={{ marginRight: 6 }} />}
      onClick={onClick}
    >
      <FormattedMessage id="PageTurnover.Actions.Refresh" description="刷新" />
    </Button>
  );
}
