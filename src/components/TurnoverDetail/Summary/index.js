import React from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Descriptions, Space, Typography, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import RefreshArea from '../RefreshArea'
import DetailTable from './DetailTable'
import { RECORD_TYPE } from 'constants/turnover'

const { Text } = Typography

export default function TurnoverSummary({ memberName }) {
    const { data: d, isValidating: fetching } = useSWR(
        memberName ? `/api/v2/Turnover/${memberName}/Current` : null,
        url => axios(url).then(res => res.data.Data)
    )

    return memberName && d ? (
        <>
            <Descriptions column={1}>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageTurnover.Fields.CurrentTimeRange"
                            description="本次結算起始時間"
                        />
                    }
                >
                    <Space>
                        <Text>
                            <DateWithFormat time={d.LastSettlementTime} />
                        </Text>
                        <Text type="secondary">
                            <FormattedMessage
                                id="PageTurnover.Notes.CurrentTimeRange"
                                description="上次提現或轉賬時間"
                            />
                        </Text>
                    </Space>
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageTurnover.Fields.PeriodAvailableBetAmount"
                            description="期間有效投注額（有效流水）"
                        />
                    }
                >
                    <Text>
                        <ReactIntlCurrencyWithFixedDecimal
                            value={d.TotalValidBetAmount}
                        />
                    </Text>
                </Descriptions.Item>
                {/* <Descriptions.Item
                    label={
                        <FormattedMessage
                        id="PageTurnover.Fields.SurplusUnlockedAmount"
                        description="剩餘已解鎖金額"
                        />
                    }
                    >
                    <Text>
                        <ReactIntlCurrencyWithFixedDecimal
                        value={d.LeftUnLockBalanceAmount}
                        />
                    </Text>
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                        id="PageTurnover.Fields.SurplusAvailableBetAmount"
                        description="剩餘有效投注額（有效流水）"
                        />
                    }
                    >
                    <Text>
                        <ReactIntlCurrencyWithFixedDecimal value={d.LeftValidBetAmount} />
                    </Text>
                </Descriptions.Item> */}
            </Descriptions>
            {d.SettlementTime && (
                <RefreshArea
                    type={RECORD_TYPE.CURRENT}
                    value={d.SettlementTime}
                    memberName={memberName}
                />
            )}
            <DetailTable data={d} fetching={fetching} />
        </>
    ) : (
        <Skeleton active />
    )
}
