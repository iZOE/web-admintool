import React, { useMemo } from 'react'
import { Table } from 'antd'
import { COLUMN_CONFIG_BASIC } from './columnConfig'
import { FormattedMessage } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

const getExtraColumns = (list, subtotal) => {
    return [
        {
            title: (
                <FormattedMessage
                    id="PageTurnover.Fields.TurnoverAmount"
                    description="累計有效流水"
                />
            ),
            children: [
                ...list.map((item, key) => ({
                    key,
                    title: item.GameProviderTypeGroupName,
                    dataIndex: `GameProviderTypeGroup_${item.GameProviderTypeGroup}`,
                    width: 120,
                    render: value => (
                        <ReactIntlCurrencyWithFixedDecimal value={value} />
                    ),
                })),
                {
                    key: 'subtotal',
                    title: (
                        <FormattedMessage
                            id="PageTurnover.Fields.Subtotal"
                            description="累計有效流水小計"
                        />
                    ),
                    width: 120,
                    render: () => (
                        <ReactIntlCurrencyWithFixedDecimal value={subtotal} />
                    ),
                },
            ],
        },
    ]
}

const getProcessedList = list => {
    let processedList = {}
    list.forEach(({ GameProviderTypeGroup: key, ValidBetAmount: value }) => {
        let item = Object.assign(
            {},
            { [`GameProviderTypeGroup_${key}`]: value }
        )
        processedList = {
            ...processedList,
            ...item,
        }
    })

    return processedList
}

export default function DetailTable({ data: rawData, fetching }) {
    const { columns, dataSource } = useMemo(() => {
        if (rawData) {
            const extraColumns = getExtraColumns(
                rawData.GameProviderBetAmountList || [],
                rawData.TotalValidBetAmount
            )
            const processList = getProcessedList(
                rawData.GameProviderBetAmountList || []
            )

            return {
                columns: [...COLUMN_CONFIG_BASIC, ...extraColumns],
                dataSource: { ...rawData, ...processList },
            }
        }
    }, [rawData])

    return (
        <Table
            bordered
            loading={fetching}
            size="small"
            columns={columns}
            dataSource={[dataSource]}
            pagination={false}
            scroll={{ x: 1200 }}
            style={{ marginBottom: 16 }}
        />
    )
}
