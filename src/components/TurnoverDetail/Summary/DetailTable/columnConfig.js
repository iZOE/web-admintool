import React from 'react'
import { FormattedMessage } from 'react-intl'
import DateRangeWithIcon from 'components/DateRangeWithIcon'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

export const COLUMN_CONFIG_BASIC = [
    {
        title: (
            <FormattedMessage
                id="PageTurnover.Fields.ReviewTimeRange"
                description="審核時間"
            />
        ),
        width: 400,
        fixed: 'left',
        render: (_1, { LastSettlementTime: s, SettlementTime: e }, _2) => (
            <DateRangeWithIcon startTime={s} endTime={e} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageTurnover.Fields.MainBalance"
                description="中心錢包餘額"
            />
        ),
        width: 120,
        dataIndex: 'BalanceAmount',
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage
                id="PageTurnover.Fields.TurnoverSubtotal"
                description="所需流水小計"
            />
        ),
        width: 120,
        dataIndex: 'TotalDiscountValidBetAmount',
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: <FormattedMessage id="Share.Fields.Type" description="類型" />,
        children: [
            {
                title: (
                    <FormattedMessage
                        id="PageTurnover.Fields.DepositAmount"
                        description="累計充值"
                    />
                ),
                width: 120,
                dataIndex: 'TotalDepositAmount',
                render: value => (
                    <ReactIntlCurrencyWithFixedDecimal value={value} />
                ),
            },
            {
                title: (
                    <FormattedMessage
                        id="PageTurnover.Fields.DepositDiscount"
                        description="累計優惠"
                    />
                ),
                width: 120,
                dataIndex: 'TotalPreferentialAmount',
                render: value => (
                    <ReactIntlCurrencyWithFixedDecimal value={value} />
                ),
            },
        ],
    },
]
