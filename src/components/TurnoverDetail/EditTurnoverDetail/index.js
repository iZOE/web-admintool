import React, { useState, useCallback } from "react";
import { FormattedMessage } from "react-intl";
import { Button } from "antd";

import TurnoverModalForm from "./TurnoverModalForm";

export default function EditTurnoverDetail({
  mutateDatasource,
  auditId,
  originalTurnover,
  turnoverMaximum
}) {
  const [isModalShown, setIsModalShown] = useState(false);
  const handleToggleModal = useCallback(
    () => setIsModalShown(prevIsShown => !prevIsShown),
    []
  );

  return (
    <>
      <Button type="link" onClick={handleToggleModal}>
        <FormattedMessage id="Share.ActionButton.Edit" description="編輯" />
      </Button>
      {isModalShown && (
        <TurnoverModalForm
          mutateDatasource={mutateDatasource}
          visible={isModalShown}
          handleToggleModal={handleToggleModal}
          auditId={auditId}
          originalTurnover={originalTurnover}
          turnoverMaximum={turnoverMaximum}
        />
      )}
    </>
  );
}
