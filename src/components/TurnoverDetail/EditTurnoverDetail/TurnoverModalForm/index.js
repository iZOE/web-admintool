import React, { useMemo } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { Modal, Form, InputNumber, Button, message } from 'antd'
import caller from 'utils/fetcher'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

const min = 0

const BASIC_RULES = [
    {
        required: true,
        message: <FormattedMessage id="Share.FormValidate.Required.Input" />,
    },
    {
        min,
        message: (
            <FormattedMessage
                id="Share.FormValidate.Value.Minimum"
                values={{ min }}
            />
        ),
        type: 'number',
    },
]

const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}

function ConfirmButton({ onClick }) {
    return (
        <Button type="primary" onClick={onClick}>
            <FormattedMessage
                id="Share.ActionButton.Submit"
                description="提交"
            />
        </Button>
    )
}

export default function TurnoverModalForm({
    visible,
    auditId,
    originalTurnover,
    turnoverMaximum,
    handleToggleModal,
    mutateDatasource,
}) {
    const [form] = Form.useForm()
    const intl = useIntl()

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })

    const rules = useMemo(() => {
        if (turnoverMaximum) {
            const max = Math.floor(turnoverMaximum * 100) / 100
            return [
                ...BASIC_RULES,
                {
                    max,
                    message: (
                        <FormattedMessage
                            id="Share.FormValidate.Value.MaxAmount"
                            values={{ amount: max }}
                        />
                    ),
                    type: 'number',
                },
            ]
        }
    }, [turnoverMaximum])

    const onModalSave = ({ DiscountValidBetAmount }) => {
        const payload = {
            ValidBetAuditID: auditId,
            DiscountValidBetAmount,
        }

        caller({
            method: 'put',
            endpoint: '/api/v2/Turnover/Update',
            body: payload,
        })
            .then(() => {
                message.success(successMsg, 5)
                mutateDatasource()
            })
            .catch(err => {
                console.error('Update Turnover Error: ', err)
            })
            .finally(() => {
                handleToggleModal()
            })
    }

    return (
        <Modal
            title={
                <FormattedMessage
                    id="Share.ActionButton.Edit"
                    description="編輯"
                />
            }
            centered
            destroyOnClose
            onCancel={() => handleToggleModal()}
            visible={visible}
            footer={[<ConfirmButton onClick={() => form.submit()} />]}
        >
            <Form
                hideRequiredMark
                {...layout}
                form={form}
                onFinish={onModalSave}
            >
                <Form.Item
                    labelCol={{ span: 8 }}
                    label={
                        <FormattedMessage
                            id="PageTurnover.Fields.OriginalTurnover"
                            description="原始所需流水"
                        />
                    }
                >
                    <span className="ant-form-text">
                        {originalTurnover && (
                            <ReactIntlCurrencyWithFixedDecimal
                                value={originalTurnover}
                            />
                        )}
                    </span>
                </Form.Item>
                <Form.Item
                    labelCol={{ span: 8 }}
                    label={
                        <FormattedMessage
                            id="PageTurnover.Fields.AdjustTurnover"
                            description="調整所需流水"
                        />
                    }
                    name="DiscountValidBetAmount"
                    rules={rules}
                    hasFeedback
                >
                    <InputNumber style={{ width: '100%' }} />
                </Form.Item>
            </Form>
        </Modal>
    )
}
