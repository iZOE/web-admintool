import React from "react";
import { Link } from "react-router-dom";
import { Divider, Skeleton, Typography } from "antd";
import { FormattedMessage } from "react-intl";
import TurnoverSummary from "./Summary";
import TurnoverResult from "./Result";
import { AUDIT_TYPE } from "constants/turnover";

export default function TurnoverDetail({ memberName }) {
  return memberName ? (
    <>
      <TurnoverSummary memberName={memberName} />
      <Typography.Paragraph>
        <FormattedMessage id="PageTurnover.Notes.ForSummary" />
        <Link to="/deposit-withdraw/turnover" component={Typography.Link}>
          <FormattedMessage id="PageTurnover.Title" />
        </Link>
      </Typography.Paragraph>
      <Divider dashed />
      <TurnoverResult memberName={memberName} auditType={AUDIT_TYPE.ALL} />
      <Typography.Paragraph style={{ margin: "12px 0 24px" }}>
        <FormattedMessage id="PageTurnover.Notes.ForResult" />
        <Link to="/deposit-withdraw/turnover" component={Typography.Link}>
          <FormattedMessage id="PageTurnover.Title" />
        </Link>
      </Typography.Paragraph>
    </>
  ) : (
    <Skeleton active />
  );
}
