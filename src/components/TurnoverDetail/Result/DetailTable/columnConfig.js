import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import EditTurnoverDetail from '../../EditTurnoverDetail';
import { NO_DATA } from 'constants/noData';

export const COLUMN_CONFIG = [
  {
    title: <FormattedMessage id="PageTurnover.Fields.ItemId" description="項目 ID" />,
    fixed: 'left',
    dataIndex: 'ValidBetAuditID',
    key: 'ValidBetAuditID',
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.Time" description="時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.Type" description="類型" />,
    dataIndex: 'AuditTypeName',
    key: 'AuditTypeName',
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.Item" description="項目" />,
    dataIndex: 'FinancialTypeListName',
    key: 'FinancialTypeListName',
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.Amount" description="金額" />,
    dataIndex: 'Amount',
    key: 'Amount',
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.Multiple" description="稽核倍數" />,
    dataIndex: 'ValidBetMultiple',
    key: 'ValidBetMultiple',
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.RequiredTurnover" description="所需流水" />,
    dataIndex: 'DiscountValidBetAmount',
    key: 'DiscountValidBetAmount',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { DiscountValidBetAmount, ModifyTime }, _2) => (
      <span style={ModifyTime && { color: '#f50' }}>
        <ReactIntlCurrencyWithFixedDecimal value={DiscountValidBetAmount} />
      </span>
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.Status" description="狀態" />,
    dataIndex: 'AuditStatusName',
    key: 'AuditStatusName',
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.Modifier" description="異動人員" />,
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    sortDirections: ['descend', 'ascend'],
    render: author => (author ? author : NO_DATA),
  },
];

export const ACTION_COLUMN_CONFIG = ({ mutateDatasource }) => {
  return [
    {
      title: <FormattedMessage id="Share.Fields.Action" description="管理" />,
      fixed: 'right',
      render: (_1, { ValidBetAuditID, DiscountValidBetAmount, Amount, ValidBetMultiple }, _2) => (
        <EditTurnoverDetail
          mutateDatasource={mutateDatasource}
          auditId={ValidBetAuditID}
          originalTurnover={DiscountValidBetAmount}
          turnoverMaximum={Amount * ValidBetMultiple}
        />
      ),
    },
  ];
};
