import React, { useMemo } from "react";

import DataTable from "components/DataTable";
import usePermission from "hooks/usePermission";

import { COLUMN_CONFIG, ACTION_COLUMN_CONFIG } from "./columnConfig";
import * as PERMISSION from "constants/permissions";

const { DEPOSIT_WITHDRAW_TURNOVER_EDIT } = PERMISSION;

const getSortedColumnConfig = data => {
  const d = data.map(item => ({
    ...item,
    sorter: (a, b) => {
      return;
    }
  }));
  return d;
};

export default function DetailTable({ ...restProps }) {
  const [hasEditPermission] = usePermission(DEPOSIT_WITHDRAW_TURNOVER_EDIT);
  const { hasActionColumn, isDisplayedInTab, mutateDatasource } = restProps;

  const columnConfig = useMemo(() => {
    const basicColumnConfig = isDisplayedInTab
      ? [...COLUMN_CONFIG]
      : getSortedColumnConfig(COLUMN_CONFIG);

    if (hasEditPermission && hasActionColumn) {
      const MERGE_MUTATE_TO_ACTION_COLUMN_CONFIG = ACTION_COLUMN_CONFIG({mutateDatasource})
      return [...basicColumnConfig, ...MERGE_MUTATE_TO_ACTION_COLUMN_CONFIG];
    }

    return basicColumnConfig;
  }, [hasEditPermission, hasActionColumn, isDisplayedInTab]);

  return <DataTable {...restProps} config={columnConfig} />;
}
