import React, { useMemo, useState } from 'react';
import { Space } from 'antd';
import useSWR from 'swr';
import axios from 'axios';
import usePermission from 'hooks/usePermission';
import ExportReportButton from 'components/ExportReportButton';
import DetailTable from './DetailTable';
import BatchWidenButton from './BatchWidenButton';
import RefreshArea from '../RefreshArea';
import { getISODateTimeString } from 'mixins/dateTime';

import { AUDIT_TYPE, RECORD_TYPE } from 'constants/turnover';
import * as PERMISSION from 'constants/permissions';

const { DEPOSIT_WITHDRAW_TURNOVER_EXPORT, DEPOSIT_WITHDRAW_TURNOVER_BATCH_WIDEN } = PERMISSION;

const getWeirdApiParams = v => {
  // DB Request:
  // 待審核: 需傳入 false
  // 已審核: 需傳入 true
  // 結算紀錄: 需全空

  switch (v) {
    case AUDIT_TYPE.FINISHED:
      return true;
    case AUDIT_TYPE.PENDING:
      return false;
    default:
      return null;
  }
};

export default function TurnoverResult({ memberName, condition, auditType, onUpdate }) {
  const [exportPermission, batchWidenPermission] = usePermission(
    DEPOSIT_WITHDRAW_TURNOVER_EXPORT,
    DEPOSIT_WITHDRAW_TURNOVER_BATCH_WIDEN
  );
  const isAudit = getWeirdApiParams(auditType);
  const isPendingTab = auditType === AUDIT_TYPE.PENDING;
  const isShownBatchButton = isPendingTab && batchWidenPermission;
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const onSelectChange = selectedRowKeys => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const { INITIAL_CONDITION, isDisplayedInTab } = useMemo(() => {
    return {
      INITIAL_CONDITION: {
        MemberName: memberName,
        IsAudited: isAudit,
        paginationInfo: {
          pageNumber: 1,
          pageSize: auditType === AUDIT_TYPE.ALL ? 5 : 25,
        },
        sortInfo: { SortBy: 'Desc', SortColumn: 'ValidBetAuditID' },
      },
      isDisplayedInTab: auditType === AUDIT_TYPE.ALL,
    };
  }, [isAudit, auditType, memberName]);

  const { data: result, isValidating: fetching, mutate } = useSWR(
    memberName ? ['/api/v2/Turnover/Search', memberName, auditType, condition] : null,
    url =>
      axios({
        url,
        method: 'post',
        data: {
          ...INITIAL_CONDITION,
          ...condition,
        },
      }).then(res => {
        const updatedTime = getISODateTimeString(res.headers.date);

        return {
          ...res.data.Data,
          _updatedTime: updatedTime,
        };
      })
  );

  function mutateDatasource() {
    mutate();
  }

  return (
    <>
      {isDisplayedInTab && (
        <RefreshArea type={RECORD_TYPE.TURNOVER} value={result && result._updatedTime} memberName={memberName} />
      )}
      <DetailTable
        mutateDatasource={mutateDatasource}
        isDisplayedInTab={isDisplayedInTab}
        hasActionColumn={!isDisplayedInTab && auditType === AUDIT_TYPE.PENDING}
        noHeader={isDisplayedInTab}
        dataSource={result && result.Container}
        total={result && result.TotalCount}
        rowKey={record => record.ValidBetAuditID}
        rowSelection={!isDisplayedInTab && isPendingTab && rowSelection}
        displayResult={result}
        loading={fetching}
        pagination={!isDisplayedInTab}
        bordered={isDisplayedInTab}
        onUpdate={onUpdate}
        extendArea={
          !isDisplayedInTab && (
            <Space>
              {isShownBatchButton && (
                <BatchWidenButton
                  disabled={selectedRowKeys.length === 0}
                  list={selectedRowKeys}
                  mutateDatasource={mutateDatasource}
                  onSelectChange={onSelectChange}
                />
              )}
              {exportPermission && (
                <ExportReportButton
                  disabled={!memberName}
                  condition={{
                    ...condition,
                    IsAudited: isAudit,
                  }}
                  actionUrl="/api/v2/Turnover/Export"
                />
              )}
            </Space>
          )
        }
      />
    </>
  );
}
