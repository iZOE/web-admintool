import React, { useMemo } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import caller from 'utils/fetcher'
import { Button, Modal, message } from 'antd'

const { confirm } = Modal

export default function BatchWidenButton({
    onSelectChange,
    list,
    mutateDatasource,
    ...restProps
}) {
    const intl = useIntl()
    const { disabled } = restProps

    const CONFIRM = useMemo(() => {
        return {
            CancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
            }),
            Content: intl.formatMessage({
                id: 'PageTurnover.Modal.BatchWidenConfirm.Content',
            }),
            OkText: intl.formatMessage({
                id: 'Share.ActionButton.Confirm',
            }),
            Success: intl.formatMessage({
                id: 'Share.SuccessMessage.UpdateSuccess',
            }),
            Title: intl.formatMessage({
                id: 'PageTurnover.Modal.BatchWidenConfirm.Title',
            }),
        }
    }, [])

    const onSubmit = () => {
        caller({
            method: 'post',
            endpoint: '/api/v2/Turnover/BatchUpdate',
            body: {
                ValidBetAuditIdList: list,
            },
        })
            .then(() => {
                message.success(CONFIRM.Success, 5)
            })
            .then(() => {
                onSelectChange([])
            })
            .then(() => {
                mutateDatasource()
            })
    }

    const onClick = () => {
        confirm({
            cancelText: CONFIRM.CancelText,
            content: (
                <>
                    <span>{CONFIRM.Content}</span>
                    <b style={{ color: '#f50' }}>0</b>
                </>
            ),
            onOk: () => {
                onSubmit()
            },
            okText: CONFIRM.OkText,
            title: CONFIRM.Title,
        })
    }

    return (
        <Button
            {...restProps}
            onClick={onClick}
            style={
                disabled
                    ? null
                    : { background: '#87d068', borderColor: '#87d068' }
            }
            type="primary"
        >
            <FormattedMessage id="PageTurnover.Actions.BatchWiden" />
        </Button>
    )
}
