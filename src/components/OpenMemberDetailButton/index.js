import React, { useContext, useMemo } from 'react'
import { message, Menu, Dropdown, Typography } from 'antd'
import { DownOutlined, CopyOutlined, UserOutlined } from '@ant-design/icons'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { FormattedMessage } from 'react-intl'
import { MemberDetailContext } from 'contexts/MemberDetailContext'
import * as PERMISSION from 'constants/permissions'
import { MODE } from 'constants/mode'
import usePermission from 'hooks/usePermission'

const { MEMBERS_MEMBERS_VIEW, MEMBERS_AFFILIATES_VIEW } = PERMISSION

export default function OpenMemberDetailButton({
    memberId,
    memberName,
    displayText = memberName,
    mode = MODE.VIEW,
    noDropdown,
}) {
    const { onDisplayMemberDetail } = useContext(MemberDetailContext)
    const [hasViewMemberPermission, hasViewAffiliatePermission] = usePermission(
        MEMBERS_MEMBERS_VIEW,
        MEMBERS_AFFILIATES_VIEW
    )

    const hasViewPermission =
        hasViewMemberPermission || hasViewAffiliatePermission

    const menu = useMemo(() => (
        <Menu>
            <Menu.Item>
                <Typography.Link onClick={onOpenMemberDetail}>
                    <UserOutlined />{' '}
                    <FormattedMessage id="Share.CommonKeys.MemberDetail" />
                </Typography.Link>
            </Menu.Item>
            <Menu.Item>
                <CopyToClipboard
                    text={displayText}
                    onCopy={(text, result) => {
                        message[result ? 'success' : 'warning'](
                            <FormattedMessage
                                id={`PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Copy${
                                    result ? 'Success' : 'Fail'
                                }`}
                                defaultMessage={`複製${
                                    result ? '成功' : '失敗'
                                }`}
                            />,
                            5
                        )
                    }}
                >
                    <Typography.Link>
                        <CopyOutlined />{' '}
                        <FormattedMessage id="Share.ActionButton.Copy" />
                    </Typography.Link>
                </CopyToClipboard>
            </Menu.Item>
        </Menu>
    ))

    function onOpenMemberDetail() {
        onDisplayMemberDetail(memberId || memberName, mode)
    }

    return hasViewPermission ? (
        noDropdown ? (
            <Typography.Link onClick={onOpenMemberDetail}>
                {displayText}
            </Typography.Link>
        ) : (
            <Dropdown overlay={menu}>
                <Typography.Link onClick={onOpenMemberDetail}>
                    {displayText} <DownOutlined />
                </Typography.Link>
            </Dropdown>
        )
    ) : (
        <Typography.Text copyable>{displayText}</Typography.Text>
    )
}
