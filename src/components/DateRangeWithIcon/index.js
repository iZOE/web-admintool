import React from 'react'
import { Space } from 'antd'
import { SwapRightOutlined } from '@ant-design/icons'
import { displayDateTime } from 'mixins/dateTime'
import { NO_DATA } from 'constants/noData'

export default function DateRangeWithIcon({ startTime, endTime }) {
    return !startTime && !endTime ? (
        NO_DATA
    ) : (
        <Space>
            {startTime ? displayDateTime(startTime) : NO_DATA}
            <SwapRightOutlined style={{ color: '#108ee9' }} />
            {endTime ? displayDateTime(endTime) : NO_DATA}
        </Space>
    )
}
