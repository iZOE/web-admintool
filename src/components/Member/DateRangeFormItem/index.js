import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Form, DatePicker } from 'antd'
import { FORMAT } from 'constants/dateConfig'

const { RangePicker } = DatePicker

export default function DateRangeFormItem() {
    return (
        <Form.Item
            label={<FormattedMessage id="Share.QueryCondition.DateRange" />}
            name="UpdateDate"
        >
            <RangePicker
                showTime
                bordered={false}
                format={FORMAT.DISPLAY.DAILY}
                style={{ width: '100%' }}
            />
        </Form.Item>
    )
}
