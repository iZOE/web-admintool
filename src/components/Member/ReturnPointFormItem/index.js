import React from "react";
import { Form, InputNumber } from "antd";
import { FormattedMessage } from "react-intl";
import { RETURN_POINT } from "constants/memberDetail/validators";

const basicRules = [
  {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  }
];

export default function ReturnPointFormItem({
  max = RETURN_POINT.MAX,
  min = RETURN_POINT.MIN,
  isDisabled
}) {
  const hasRange = max !== min;
  const rangeRules = [
    {
      max,
      message: (
        <FormattedMessage
          id="Share.FormValidate.Value.Maximum"
          values={{ max }}
        />
      ),
      type: "number"
    },
    {
      min,
      message: (
        <FormattedMessage
          id="Share.FormValidate.Value.Minimum"
          values={{ min }}
        />
      ),
      type: "number"
    }
  ];

  const fixedRangeRules = [
    {
      validator: (_, value) => {
        return value === max
          ? Promise.resolve()
          : Promise.reject(
              <FormattedMessage
                id="Share.FormValidate.Value.Fixed"
                values={{ value: max }}
              />
            );
      }
    }
  ];

  const rules = [...basicRules].concat(hasRange ? rangeRules : fixedRangeRules);

  return (
    <Form.Item
      label={<FormattedMessage id="MemberDetail.Fields.ReturnPoint" />}
      name="ReturnPoint"
      rules={!isDisabled && rules}
      hasFeedback
    >
      <InputNumber disabled={isDisabled} style={{ width: "100%" }} />
    </Form.Item>
  );
}
