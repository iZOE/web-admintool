import React from "react";
import { Form, Input } from "antd";
import { FormattedMessage } from "react-intl";
import { LOGIN_PASSWORD } from "constants/memberDetail/validators";

const passwordRules = [
  {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  },
  {
    min: LOGIN_PASSWORD.MIN,
    message: (
      <FormattedMessage
        id="Share.FormValidate.Length.Minimum"
        values={{ min: LOGIN_PASSWORD.MIN }}
      />
    )
  },
  {
    max: LOGIN_PASSWORD.MAX,
    message: (
      <FormattedMessage
        id="Share.FormValidate.Length.Maximum"
        values={{ max: LOGIN_PASSWORD.MAX }}
      />
    )
  },
  {
    validator: (_, value) => {
      const isPasswordValidated = LOGIN_PASSWORD.PATTERN.test(value);
      return !!isPasswordValidated
        ? Promise.resolve()
        : Promise.reject(
            <FormattedMessage id="Share.FormValidate.Pattern.WordNumber" />
          );
    }
  }
];

const confirmPasswordRules = [
  {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  },
  ({ getFieldValue }) => ({
    validator(_, value) {
      if (!value || getFieldValue("Password") === value) {
        return Promise.resolve();
      }
      return Promise.reject(
        <FormattedMessage id="Share.FormValidate.Pattern.PasswordShouldMatch" />
      );
    }
  })
];

export default function ConfirmPasswordFormItem({
  name = "Password",
  label = <FormattedMessage id="MemberDetail.Fields.LoginPassword" />
}) {
  return (
    <>
      <Form.Item name={name} label={label} hasFeedback rules={passwordRules}>
        <Input.Password />
      </Form.Item>
      <Form.Item
        name="ConfirmPassword"
        label={<FormattedMessage id="MemberDetail.Fields.ConfirmPassword" />}
        dependencies={[name]}
        hasFeedback
        rules={confirmPasswordRules}
      >
        <Input.Password />
      </Form.Item>
    </>
  );
}
