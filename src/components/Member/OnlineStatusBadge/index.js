import React from "react";
import { Badge } from "antd";
import { ONLINE_STATUS } from "constants/memberDetail/memberStatus";

const getStatusColor = status => {
  switch (status) {
    case ONLINE_STATUS.ACTIVE:
      return "success";
    case ONLINE_STATUS.DEACTIVE:
      return "default";
    case ONLINE_STATUS.FREEZE:
      return "error";
    case ONLINE_STATUS.LOCK:
      return "error";
    default:
      return "processing";
  }
};

export default function OnlineStatusBadge({ status, text }) {
  return <Badge status={getStatusColor(status)} text={text} />;
}
