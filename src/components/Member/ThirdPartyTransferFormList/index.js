import React from "react";
import { Form } from "antd";
import { FormattedMessage } from "react-intl";
import SwitchWithDefaultName from "components/SwitchWithDefaultName";

const rules = [
  {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  }
];

export default function ThirdPartyTransferFormList({ list }) {
  return (
    list && (
      <Form.List name="GameTransferSetting">
        {fields => {
          return (
            <div>
              {fields.map((field, key) => (
                <Form.Item
                  key={key}
                  label={
                    <>
                      {list[field.name].GameProviderText}
                      <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Fields.AccountSetting" />
                    </>
                  }
                  name={[field.name, "TransferLock"]}
                  hasFeedback
                  rules={rules}
                  valuePropName="checked"
                >
                  <SwitchWithDefaultName />
                </Form.Item>
              ))}
            </div>
          );
        }}
      </Form.List>
    )
  );
}
