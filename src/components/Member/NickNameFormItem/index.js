import React from "react";
import { Form, Input } from "antd";
import { FormattedMessage } from "react-intl";
import { NICKNAME } from "constants/memberDetail/validators";

const rules = [
  {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  },
  {
    min: NICKNAME.MIN,
    message: (
      <FormattedMessage
        id="Share.FormValidate.Length.Minimum"
        values={{ min: NICKNAME.MIN }}
      />
    )
  },
  {
    max: NICKNAME.MAX,
    message: (
      <FormattedMessage
        id="Share.FormValidate.Length.Maximum"
        values={{ max: NICKNAME.MAX }}
      />
    )
  }
];

export default function NickNameFormItem({
  name = "NickName",
  label = <FormattedMessage id="MemberDetail.Fields.NickName" />
}) {
  return (
    <Form.Item name={name} label={label} hasFeedback rules={rules}>
      <Input />
    </Form.Item>
  );
}
