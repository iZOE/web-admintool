import React from "react";
import useSWR from "swr";
import axios from "axios";
import { Spin } from "antd";
import ListSelect from "components/ListSelect";
import { AFFILIATE_LEVEL_TYPE } from "constants/memberDetail/memberType";
import { FILTER_TYPE } from "constants/memberDetail/affiliateFilterType";

export default function SelectWithAffiliateLevelList({
  isShownAll = false,
  filterType,
  ...props
}) {
  let {
    data: affiliateLevelList
  } = useSWR(`/api/Option/AffiliateLevel?showAll=${isShownAll}`, url =>
    axios(url).then(res => res.data.Data)
  );

  const getFilteredList = list => {
    switch (filterType) {
      case FILTER_TYPE.ONLY_MEMBER:
        const memberData = list.filter(
          ({ Id }) => Id === AFFILIATE_LEVEL_TYPE.MEMBER
        );
        return memberData;
      case FILTER_TYPE.ONLY_AGENCY:
        const agencyData = list.filter(
          ({ Id }) => Id === AFFILIATE_LEVEL_TYPE.AGENCY
        );
        return agencyData;
      case FILTER_TYPE.ALL_AGENCY:
        const allAgencyData = list.filter(
          ({ Id }) => Id !== AFFILIATE_LEVEL_TYPE.MEMBER
        );
        return allAgencyData;
      default:
        return list;
    }
  };

  return affiliateLevelList ? (
    <ListSelect list={getFilteredList(affiliateLevelList)} {...props} />
  ) : (
    <Spin />
  );
}
