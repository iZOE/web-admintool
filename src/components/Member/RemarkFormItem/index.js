import React from "react";
import { Form, Input } from "antd";
import { FormattedMessage } from "react-intl";

export default function RemarkFormItem({
  name = "Remarks",
  label = <FormattedMessage id="MemberDetail.Fields.Remark" />
}) {
  return (
    <Form.Item name={name} label={label}>
      <Input.TextArea />
    </Form.Item>
  );
}
