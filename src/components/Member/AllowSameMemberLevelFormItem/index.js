import React from "react";
import StatusFormItem from "components/Member/StatusFormItem";

export default function AllowSameMemberLevelFormItem({ isDisabled = false }) {
  return (
    <StatusFormItem
      name="IsSameLevel"
      fieldName="AllowSameMemberLevel"
      statusLabel="IsAllow"
      isDisabled={isDisabled}
    />
  );
}
