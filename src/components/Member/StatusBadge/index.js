import React from "react";
import { Badge } from "antd";

export default function StatusBadge({ status, text }) {
  return <Badge status={!!status ? "success" : "default"} text={text} />;
}
