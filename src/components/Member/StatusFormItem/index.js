import React from "react";
import { Form } from "antd";
import { FormattedMessage } from "react-intl";
import SwitchWithDefaultName from "components/SwitchWithDefaultName";

const basicRules = [
  {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  }
];

export default function StatusFormItem({
  name,
  fieldName,
  moreRules = [],
  isDisabled = false,
  statusLabel
}) {
  const rules = [...basicRules, ...moreRules];

  return (
    <Form.Item
      name={name}
      label={<FormattedMessage id={"MemberDetail.Fields." + fieldName} />}
      rules={rules}
      hasFeedback
      valuePropName="checked"
    >
      <SwitchWithDefaultName isDisabled={isDisabled} label={statusLabel} />
    </Form.Item>
  );
}
