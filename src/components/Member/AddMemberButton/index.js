import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'antd'
import { UserAddOutlined, UsergroupAddOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import { MEMBER_STYLE } from 'constants/memberDetail/memberType'

export default function AddMemberButton({ memberStyle = MEMBER_STYLE.MEMBER }) {
    const path =
        memberStyle === MEMBER_STYLE.MEMBER
            ? '/members/members'
            : '/members/affiliates'
    return (
        <Link to={`${path}/create`}>
            <Button
                type="primary"
                icon={
                    memberStyle === MEMBER_STYLE.MEMBER ? (
                        <UserAddOutlined style={{ marginRight: 6 }} />
                    ) : (
                        <UsergroupAddOutlined style={{ marginRight: 6 }} />
                    )
                }
            >
                <FormattedMessage id="Share.ActionButton.Create" />
                <FormattedMessage id={`Share.MemberType.${memberStyle}`} />
            </Button>
        </Link>
    )
}
