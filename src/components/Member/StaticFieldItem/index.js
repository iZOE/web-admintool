import React from "react";
import { Form } from "antd";
import { FormattedMessage } from "react-intl";
import { NO_DATA } from "constants/noData";

export default function StaticFieldItem({ fieldName, value = NO_DATA }) {
  return (
    <Form.Item
      label={<FormattedMessage id={"MemberDetail.Fields." + fieldName} />}
    >
      <span className="ant-form-text">{value}</span>
    </Form.Item>
  );
}
