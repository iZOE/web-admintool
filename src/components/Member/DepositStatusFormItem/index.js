import React from "react";
import StatusFormItem from "components/Member/StatusFormItem";

export default function DepositStatusFormItem() {
  return <StatusFormItem name="IsDepositable" fieldName="Deposit" />;
}
