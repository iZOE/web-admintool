import React, { useContext } from "react";
import { MemberDetailContext } from "contexts/MemberDetailContext";
import StatusFormItem from "components/Member/StatusFormItem";
import { MEMBER_TYPE } from "constants/memberDetail/memberType";

export default function TransferStatusFormItem({ memberTypeId }) {
  const { memberIdentity: m } = useContext(MemberDetailContext);
  const id = memberTypeId ? memberTypeId : m && m.memberType.id;
  const isInternalMember = id === MEMBER_TYPE.INTERNAL;

  return (
    <StatusFormItem
      name="IsFundTransferable"
      fieldName="Transfer"
      isDisabled={isInternalMember}
    />
  );
}
