import React from "react";
import StatusFormItem from "components/Member/StatusFormItem";

export default function AccountStatusFormItem() {
  return <StatusFormItem name="Status" fieldName="AccountStatus" />;
}
