import React from "react";
import StatusFormItem from "components/Member/StatusFormItem";

export default function BetStatusFormItem() {
  return <StatusFormItem name="IsBetPlaceable" fieldName="Bet" />;
}
