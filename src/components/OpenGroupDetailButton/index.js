import React, { useContext } from 'react';
import { Button, Typography } from 'antd';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import { GroupDetailContext } from 'contexts/GroupDetailContext';

const { Text } = Typography;
const { SETTINGS_GROUPS_VIEW } = PERMISSION;

export default function OpenGroupDetailButton({
  groupId,
  groupName,
  displayText = groupName
}) {
  const { onDisplayGroupDetail } = useContext(GroupDetailContext);

  return (
    <Permission
      functionIds={[SETTINGS_GROUPS_VIEW]}
      failedRender={<Text>{displayText}</Text>}
    >
      <Button
        type="link"
        size="small"
        onClick={() => {
          onDisplayGroupDetail(groupId);
        }}
      >
        {displayText}
      </Button>
    </Permission>
  );
}
