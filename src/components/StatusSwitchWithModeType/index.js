import React from "react";
import SwitchWithDefaultName from "components/SwitchWithDefaultName";
import BadgeWithDefaultStatusName from "components/BadgeWithDefaultStatusName";

export default function StatusSwitchWithModeType({
  status,
  isEditMode = false,
  label,
  ...props
}) {
  return isEditMode ? (
    <SwitchWithDefaultName label={label} {...props} />
  ) : (
    <BadgeWithDefaultStatusName label={label} status={status} />
  );
}
