import React from "react";
import { FormattedNumber } from "react-intl";

export default function ReactIntlCurrencyWithFixedDecimal({ value, ...restProps }) {
  return (
    <FormattedNumber
      {...restProps}
      value={value}
      minimumFractionDigits="2"
      maximumFractionDigits="2"
    />
  );
}
