import React, { useContext, useEffect, useMemo, useState } from 'react';
import useSWR from 'swr';
import axios from 'axios';
import { TreeSelect } from 'antd';
import { FormattedMessage } from 'react-intl';
import { ConditionContext } from 'contexts/ConditionContext';

const reduceTreeData = (arr, parentId, needAll = false) =>
  arr.reduce(
    (list, curr) => [
      ...list,
      {
        key: `${parentId}-${curr.Id}`,
        title: curr.Name || (
          <FormattedMessage
            id={`PagePaymentSetting.QueryCondition.Options.DepositType.${curr.Id}`}
            defaultMessage={curr.Id}
          />
        ),
        value: `${parentId}-${curr.Id}`,
        children: curr.SubItems ? reduceTreeData(curr.SubItems, curr.Id) : null
      }
    ],
    needAll
      ? [
          {
            key: 'null',
            title: <FormattedMessage id="Share.Dropdown.All" />,
            value: null
          }
        ]
      : []
  );

const initalValue = value => {
  if (Array.isArray(value)) {
    if (value.length === 1) {
      value.unshift(PREFIX_CODE);
    }
    return value.join('-');
  }
  return null;
};

const PREFIX_CODE = 'T';

export default function TreeLevelSelect({
  url,
  value,
  onChange,
  placeholder,
  needAll,
  ...restProps
}) {
  const contextValues = useContext(ConditionContext);
  const { updateFormItems } = contextValues || { updateFormItems: null };
  const [treeValue, setTreeValue] = useState(null);
  const name = url
    .split('/')
    .pop()
    .split('?')
    .shift();
  const { data } = useSWR(url, url =>
    axios(url).then(res => res && res.data.Data)
  );

  const treeData = useMemo(() => {
    if (data) {
      return reduceTreeData(data, PREFIX_CODE, needAll);
    }
    return [];
  }, [data]);

  useEffect(() => {
    setTreeValue(initalValue(value));
  }, []);

  useEffect(() => {
    updateFormItems &&
      updateFormItems({
        [name]: !!data
      });
  }, [data]);

  function onTChange(value) {
    setTreeValue(value);
    onChange(
      value
        ? value
            .split('-')
            .reduce((a, c) => (c !== PREFIX_CODE ? [...a, Number(c)] : a), [])
        : null
    );
  }

  return (
    <TreeSelect
      value={treeValue}
      treeData={treeData}
      onChange={onTChange}
      placeholder={
        placeholder || (
          <FormattedMessage
            id="Share.PlaceHolder.Input.PleaseSelectSomething"
            values={{
              field: null
            }}
          />
        )
      }
      {...restProps}
    />
  );
}
