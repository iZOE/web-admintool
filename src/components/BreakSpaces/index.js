import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  white-space: break-spaces;
`;

export default function BreakSpaces({ children }) {
  return <Wrapper>{children}</Wrapper>;
}
