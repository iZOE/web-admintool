import { displayDateTime } from 'mixins/dateTime'
import { NO_DATA } from 'constants/noData'

export default function DateWithFormat({ time }) {
    return time ? displayDateTime(time) : NO_DATA
}
