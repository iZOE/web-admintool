import React, { createElement } from "react";

export default function ContextProviders({ providers, children }) {
  let previous = children;
  providers.reverse().forEach(ProviderComponent => {
    previous = <ProviderComponent>{previous}</ProviderComponent>;
  });
  return previous;
}
