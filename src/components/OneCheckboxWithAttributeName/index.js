import React from "react";
import { Checkbox } from "antd";

const OneCheckboxWithAttributeName = props => {
  const { data, name, setFieldValue, textOnTheRight, ...rest } = props;

  const handleChange = evt => {
    const { checked } = evt.target;
    setFieldValue && setFieldValue(checked);
  };

  return (
    <Checkbox {...rest} onChange={handleChange}>
      {textOnTheRight}
    </Checkbox>
  );
};

export default OneCheckboxWithAttributeName;
