import React, { useState } from 'react';
import { saveAs } from 'file-saver';
import { Button } from 'antd';
import { ExportOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';
import * as dayjs from 'dayjs';
import caller from 'utils/fetcher';

export default function ExportReportButton({
  condition,
  actionUrl,
  disabled = false
}) {
  const [loading, setLoading] = useState(false);

  const onExportReport = () => {
    setLoading(true);
    caller({
      method: 'post',
      endpoint: actionUrl,
      body: condition,
      responseType: 'arraybuffer',
      exportMode: true
    })
      .then(({ data }) => {
        var blob = new Blob([data], {
          type:
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        });
        const exportDate = dayjs();
        saveAs(blob, `Export_${exportDate.format('YYYYMMDDHHmmss')}.xlsx`);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Button
      loading={loading}
      type="primary"
      icon={<ExportOutlined style={{ marginRight: 6 }} />}
      onClick={onExportReport}
      disabled={disabled}
    >
      <FormattedMessage id="Share.ActionButton.Export" />
    </Button>
  );
}
