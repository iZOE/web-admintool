import React from "react";
import { Breadcrumb } from "antd";
import { HomeOutlined } from "@ant-design/icons";

export default function PageBreadCrumb({ title, more }) {
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item href="/">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item href="/operation-report/affiliate">
          {title}
        </Breadcrumb.Item>
        {more}
      </Breadcrumb>
      <div>{title}</div>
    </div>
  );
}
