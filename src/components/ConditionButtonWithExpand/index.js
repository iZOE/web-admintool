import React from "react";
import { Space, Button } from "antd";
import { DoubleRightOutlined } from "@ant-design/icons";
import QueryButton from "components/FormActionButtons/Query";

export default function ConditionButtonWithExpand({
  isExpand,
  onExpandButtonClick
}) {
  return (
    <Space>
      <QueryButton />
      {onExpandButtonClick && (
        <Button
          type="link"
          icon={<DoubleRightOutlined rotate={isExpand ? 270 : 90} />}
          onClick={onExpandButtonClick}
        />
      )}
    </Space>
  );
}
