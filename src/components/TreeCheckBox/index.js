import React from 'react';

export default function TreeCheckBox({
  label,
  checkStatus,
  onClick,
  disabled
}) {
  let status = '';
  switch (checkStatus) {
    case 1:
      status = ' ant-tree-checkbox-indeterminate';
      break;

    case 2:
      status = ' ant-tree-checkbox-checked';
      break;

    default:
      status = '';
      break;
  }
  return (
    <div className="ant-tree-treenode">
      <span
        className={`ant-tree-checkbox${status}${
          disabled ? ' ant-tree-checkbox-disabled' : ''
        }`}
        onClick={() => {
          onClick && onClick();
        }}
      >
        <span className="ant-tree-checkbox-inner"></span>
      </span>
      <span className="ant-tree-node-content-wrapper">
        <span className="ant-tree-title">{label}</span>
      </span>
    </div>
  );
}
