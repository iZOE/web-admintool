import React from "react";
import { Empty, Button, Divider } from "antd";
import { FormattedMessage } from "react-intl";
import { FileSearchOutlined } from "@ant-design/icons";

export default function PleaseSearch({ display, children, onClick }) {
  if (!display) {
    return (
      <>
        <Divider />
        <Empty
          image={<FileSearchOutlined style={{ fontSize: 100 }} />}
          imageStyle={{
            height: 140
          }}
          description={
            <FormattedMessage id="Share.CommonKeys.PleasePressSearchButton" />
          }
        >
          {onClick && (
            <Button type="primary">
              <FormattedMessage id="Share.ActionButton.Query" />
            </Button>
          )}
        </Empty>
      </>
    );
  }
  return children;
}
