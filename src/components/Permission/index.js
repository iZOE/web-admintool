import React from 'react';
import { useEffect, useMemo, useState } from 'react';
import { Result, Skeleton } from 'antd';
import PropTypes from 'prop-types';
import usePermission from 'hooks/usePermission';
import { useIntl } from 'react-intl';

const FailedComponent = () => {
  const intl = useIntl();
  return (
    <Result
      status="warning"
      title={intl.formatMessage({
        id: 'Share.ErrorMessage.Forbidden.Title'
      })}
      subTitle={intl.formatMessage({
        id: 'Share.ErrorMessage.Forbidden.Message'
      })}
    />
  );
};

/**
 * 需要參照權限顯示的組建，可透過此組件包裝；如果 functionIds 傳入的為「模組」的權限代碼而不是「模組功能」的代碼，那則會顯示 failedRender。
 *
 * @param {Object} props - Permission Props.
 * @param {number[]} props.functionIds - 「模組功能」的權限代碼 Array， 可從 ```constants/permissions import``` 權限代碼.
 * @param {boolean} [props.casual=false] - 如果使用將會用 or 來比對傳入的權限代碼，預設是用 and.
 * @param {boolean} [props.isPage=false] - 預設 ```false``` 如果有帶入，當沒有設定 ```failedRender``` 時會自動帶入預設的無權限畫面.
 * @param {React.Component} props.failedRender - 沒有權限或傳入的權限代碼無效時會顯示的 component. (不給的話會有預設的)
 * @param {React.Component} props.children - 有權限時會顯示的 component.
 * @returns {Element | React.Component} - 依照所判斷出的結果回傳 failedRender 或 children.
 * @example
 * import Permission from 'components/Permission';
 * import * as PERMISSION from 'constants/permissions';
 * const { OPERATION_ACTIVITY_TYPE_MANAGEMENT_EDIT } = PERMISSION;
 *
 * <Permission
 *  functionIds={[OPERATION_ACTIVITY_TYPE_MANAGEMENT_EDIT]}
 *  casual
 *  failedRender={<div> You don't have permission. </div>}
 * >
 *  <Link to={`/somewhere`}>
 *    Click Me
 *  </Link>
 * </Permission>
 */
function Permission({ functionIds, casual, failedRender, isPage, children }) {
  /**
   * 傳入的值如果是「模組」的權限代碼，則會得到 Map 物件
   *
   * @type {(boolean | Map<string, boolean>)[]}
   */

  const permissionList = usePermission(
    ...functionIds.filter(id => id !== undefined)
  );

  /**
   * 判斷權限後要回傳的 component
   *
   * @type {[Element | React.Component, React.Dispatch<Element | React.Component>]}
   */

  // const [childComponent, setChildComponent] = useState(null);

  failedRender = isPage
    ? failedRender || <FailedComponent />
    : failedRender || null;

  const childComponent = useMemo(() => {
    if (!!permissionList.length) {
      /**
       * 確認透過 usePermission 取得的結果是否都是 boolean
       *
       * @type {boolean}
       */
      const isAllBoolean = permissionList.every(p => typeof p === 'boolean');
      if (isAllBoolean) {
        /**
         * set 的結果只會有以下三種
         *
         * * Set(1) {true}
         * * Set(1) {false}
         * * Set(2) {true, false}
         * @type {Set<boolean>}
         */
        const set = new Set(permissionList);
        const hasTrue = set.has(true) ? 1 : 0;
        const setSize = set.size - 1;

        /**
         * 透過 set 內容組合出的字串
         *
         * * 100 - [通過] 提供的權限查詢都通過且使用 and 比對
         * * 101 - [通過] 提供的權限查詢都通過且使用 or 比對
         * * 000 - [拒絕] 提供的權限查詢都沒通過且使用 and 比對
         * * 001 - [拒絕] 提供的權限查詢都沒通過且使用 or 比對
         * * 110 - [拒絕] 提供的權限查詢裡至少有一個通過且使用 and 比對
         * * 111 - [通過] 提供的權限查詢裡至少有一個通過且使用 or 比對
         * * 010 - [拒絕] 提供的權限查詢裡至少有一個沒通過且使用 and 比對
         * * 011 - [通過] 提供的權限查詢裡至少有一個沒通過且使用 or 比對
         * @type {string}
         */
        const displayBasis = `${hasTrue}${setSize}${casual ? 1 : 0}`;

        switch (displayBasis) {
          case '100':
          case '101':
          case '111':
          case '011':
            return children;
          case '000':
          case '001':
          case '110':
          case '010':
            return failedRender;
          default:
            return <Skeleton active />;
        }
      } else {
        console.error(
          'Failed prop type: Supplied to type of `functionIds` of `<Permission>` is Invalid, expected `number`.'
        );
        return failedRender;
      }
    } else {
      return <Skeleton active />;
    }
  }, [permissionList]);

  return childComponent;
}

Permission.defaultProps = {
  casual: false,
  isPage: false
};

Permission.propTypes = {
  functionIds: PropTypes.arrayOf(PropTypes.number).isRequired,
  casual: PropTypes.bool,
  isPage: PropTypes.bool,
  failedRender: PropTypes.node,
  children: PropTypes.node.isRequired
};

export default Permission;
