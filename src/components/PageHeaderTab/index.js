import React from 'react'
import { Tabs } from './Styled'

export default function PageHeaderTab({ children, ...props }) {
    return (
        <Tabs size="small" tabBarStyle={{ margin: 0 }} {...props}>
            {children}
        </Tabs>
    )
}
