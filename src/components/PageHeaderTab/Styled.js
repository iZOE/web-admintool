import styled from 'styled-components'
import { Tabs as TabsComponent } from 'antd'

export const Tabs = styled(TabsComponent)`
    &.ant-tabs {
        background: #fff;
        padding: 8px 12px 0 12px;
        margin: -0.5rem -0.5rem 0.5rem;
    }
`
