import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import { toMatchDiffSnapshot } from 'snapshot-diff'
import { LanguageProvider } from 'i18n/LanguageProvider'
import BadgeWithDefaultStatusName from './index'

expect.extend({ toMatchDiffSnapshot })

const renderWithLanguageProvider = (ui: JSX.Element) => {
    return render(<LanguageProvider>{ui}</LanguageProvider>)
}

test('BadgeWithDefaultStatusName rendered', () => {
    const { asFragment } = renderWithLanguageProvider(
        <BadgeWithDefaultStatusName />
    )
    const firstRender = asFragment()
    expect(firstRender).toMatchDiffSnapshot(asFragment())
})

test('BadgeWithDefaultStatusName redered, with props: status=false', () => {
    const { asFragment } = renderWithLanguageProvider(
        <BadgeWithDefaultStatusName status={false} />
    )
    const firstRender = asFragment()
    expect(firstRender).toMatchDiffSnapshot(asFragment())
})
