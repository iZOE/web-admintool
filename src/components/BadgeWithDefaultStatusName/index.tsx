import React from 'react'
import { Badge } from 'antd'
import { FormattedMessage } from 'react-intl'

export interface Props {
    status?: boolean
    label?: string
}

export default function BadgeWithDefaultStatusName({
    status,
    label = 'IsEnable',
}: Props): JSX.Element {
    return (
        <Badge
            status={!!status ? 'success' : 'default'}
            text={
                !!status ? (
                    <FormattedMessage
                        id={'Share.SwitchButton.' + label + '.Yes'}
                        defaultMessage="Y"
                    />
                ) : (
                    <FormattedMessage
                        id={'Share.SwitchButton.' + label + '.No'}
                        defaultMessage="N"
                    />
                )
            }
        />
    )
}
