import React, { useContext, useEffect } from 'react'
import { Form, TreeSelect } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'

const { SHOW_CHILD } = TreeSelect

export default function MemberLevel({
    disabled,
    label,
    name = 'MemberLevelId',
    checkAll,
    initialValue = [],
    ...restProps
}) {
    const ALL_VALUE = []
    const contextValues = useContext(ConditionContext)
    const { updateFormItems } = contextValues || { updateFormItems: null }
    const { data } = useSWR('/api/Option/MemberLevel', url =>
        axios(url).then(res => res.data.Data)
    )

    const genTreeData = data => {
        if (!data) {
            return []
        }
        return data.map(opt => {
            ALL_VALUE.push(opt.Id)
            return {
                title: opt.Name,
                value: opt.Id,
                key: opt.Id,
            }
        })
    }

    const treeData = [
        {
            title: (
                <FormattedMessage id="Share.Dropdown.All" description="全部" />
            ),
            value: '',
            key: '',
            children: genTreeData(data),
        },
    ]

    useEffect(() => {
        updateFormItems &&
            updateFormItems({
                [name]: !!data,
            })
    }, [data])

    if (!data) {
        return null
    }

    return (
        <Form.Item
            noStyle
            shouldUpdate={(prev, curr) => prev[name] !== curr[name]}
        >
            {({ getFieldValue }) => {
                const formItemProps = {
                    name: name,
                    ...restProps,
                }
                if (getFieldValue(name) === undefined) {
                    formItemProps.initialValue = checkAll
                        ? ALL_VALUE
                        : initialValue
                }

                return (
                    <Form.Item
                        label={
                            label ? (
                                label
                            ) : (
                                <FormattedMessage
                                    id="PageLogTransaction.QueryCondition.MemberLevel"
                                    description="會員等級"
                                />
                            )
                        }
                        {...formItemProps}
                    >
                        <TreeSelect
                            disabled={disabled}
                            treeData={treeData}
                            treeCheckable
                            showCheckedStrategy={SHOW_CHILD}
                            placeholder={
                                <FormattedMessage
                                    id="Share.PlaceHolder.Input.PleaseSelectSomething"
                                    description="會員等級"
                                    values={{
                                        field: (
                                            <FormattedMessage
                                                id="Share.CommonKeys.MemberLevel"
                                                description="會員等級"
                                            />
                                        ),
                                    }}
                                />
                            }
                            style={{
                                width: '100%',
                            }}
                            maxTagCount={1}
                            maxTagPlaceholder={values => (
                                <span>
                                    <FormattedMessage
                                        id="Share.FormItem.TreeSelect.MaxTagPlaceholder"
                                        description="會員等級"
                                        values={{
                                            count: values.length + 1,
                                        }}
                                    />
                                </span>
                            )}
                        />
                    </Form.Item>
                )
            }}
        </Form.Item>
    )
}
