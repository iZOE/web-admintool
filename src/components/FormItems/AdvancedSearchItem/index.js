import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Form, Input, InputNumber, Select } from 'antd'

const { Option } = Select

export default function AdvancedSearchConditionItem({
    OptionItems,
    MessageId,
    AmountSet,
    OptionRequire,
    AdvancedSearchConditionItemName = 'AdvancedSearchConditionItem',
    AdvancedSearchConditionValueName = 'AdvancedSearchConditionValue',
    placeholders,
}) {
    const { formatMessage } = useIntl()

    const rules = [
        {
            required: true,
            message: (
                <FormattedMessage id="Share.FormValidate.Required.Input" />
            ),
        },
    ]

    return (
        <Form.Item
            label={<FormattedMessage id="Share.QueryCondition.AdvanceQuery" />}
        >
            <Input.Group compact style={{ display: 'flex' }}>
                <Form.Item name={AdvancedSearchConditionItemName} noStyle>
                    <Select style={{ width: '140px' }}>
                        {OptionItems.map(i => (
                            <Option key={i} value={i}>
                                <FormattedMessage id={`${MessageId}.${i}`} />
                            </Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item shouldUpdate noStyle>
                    {({ getFieldValue }) =>
                        AmountSet.has(
                            getFieldValue(AdvancedSearchConditionItemName)
                        ) ? (
                            <Form.Item
                                name={AdvancedSearchConditionValueName}
                                style={{
                                    paddingBottom: 0,
                                    marginBottom: 0,
                                    flex: 1,
                                }}
                            >
                                <div style={{ display: 'flex' }}>
                                    <Form.Item
                                        name="AdvancedSearchConditionFrom"
                                        noStyle
                                    >
                                        <InputNumber
                                            style={{
                                                flex: 1,
                                                textAlign: 'center',
                                                borderRadius: 'unset',
                                            }}
                                            placeholder={formatMessage({
                                                id:
                                                    'Share.PlaceHolder.Input.Minimum',
                                                description: '最小值',
                                            })}
                                        />
                                    </Form.Item>
                                    <Input
                                        className="site-input-split"
                                        style={{
                                            width: 30,
                                            borderLeft: 0,
                                            borderRight: 0,
                                            pointerEvents: 'none',
                                            borderRadius: 'unset',
                                        }}
                                        placeholder="~"
                                        disabled
                                    />
                                    <Form.Item
                                        name="AdvancedSearchConditionTo"
                                        noStyle
                                    >
                                        <InputNumber
                                            className="site-input-right"
                                            style={{
                                                flex: 1,
                                                textAlign: 'center',
                                                borderRadius: '0 2px 2px 0',
                                            }}
                                            placeholder={formatMessage({
                                                id:
                                                    'Share.PlaceHolder.Input.Maximum',
                                                description: '最大值',
                                            })}
                                        />
                                    </Form.Item>
                                </div>
                            </Form.Item>
                        ) : (
                            <Form.Item
                                name={AdvancedSearchConditionValueName}
                                noStyle
                                dependencies={[AdvancedSearchConditionItemName]}
                                rules={
                                    OptionRequire &&
                                    OptionRequire.has(
                                        getFieldValue(
                                            AdvancedSearchConditionItemName
                                        )
                                    )
                                        ? rules
                                        : null
                                }
                            >
                                <Input
                                    style={{ flex: 1 }}
                                    placeholder={
                                        placeholders[
                                            getFieldValue(
                                                AdvancedSearchConditionItemName
                                            )
                                        ]
                                    }
                                />
                            </Form.Item>
                        )
                    }
                </Form.Item>
            </Input.Group>
        </Form.Item>
    )
}

AdvancedSearchConditionItem.defaultProps = {
    AmountSet: new Set(),
    placeholders: Object.create(null),
}
