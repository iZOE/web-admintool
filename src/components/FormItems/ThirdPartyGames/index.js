import React from "react";
import { Form, Select } from "antd";
import useSWR from "swr";
import axios from "axios";
import { FormattedMessage } from "react-intl";

const { useForm } = Form;

function ThirdPartyGames({ name }) {
  const { Option, OptGroup } = Select;
  const [form] = useForm();
  const { data: lotteries } = useSWR(
    "/api/Option/V2/ThirdPartyLotteryTypes",
    (url) => axios(url).then((res) => res.data.Data)
  );

  return (
    <Form.Item
      form={form}
      label={<FormattedMessage id="Share.FormItem.LotteryItems" />}
      name={name || "LotteryCode"}
    >
      <Select>
        <Option value="">{<FormattedMessage id="Share.Dropdown.All" />}</Option>
        {lotteries &&
          lotteries.map((d) => (
            <OptGroup label={d.Name} key={d.Id}>
              {d.SubItems.map((s) => (
                <Option value={s.Id} key={s.Id}>
                  {s.Name}
                </Option>
              ))}
            </OptGroup>
          ))}
      </Select>
    </Form.Item>
  );
}

export default ThirdPartyGames;
