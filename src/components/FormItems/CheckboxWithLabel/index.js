import React from 'react';
import { Form, Checkbox } from 'antd';
import { FormattedMessage } from 'react-intl';

export default function CheckboxWithLabel({
  name,
  formattedMessageId,
  ...restProps
}) {
  return (
    <Form.Item name={name} valuePropName="checked" {...restProps}>
      <Checkbox>
        <FormattedMessage id={formattedMessageId} />
      </Checkbox>
    </Form.Item>
  );
}
