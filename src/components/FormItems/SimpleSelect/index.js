import React, { useContext, useEffect, useMemo } from 'react'
import { Form, Select } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'

export default function SimpleSelect({
    url,
    showSearch,
    needAll,
    optionAllValue = null,
    initialValue,
    name,
    disabled,
    placeholder,
    ...restProps
}) {
    const contextValues = useContext(ConditionContext)
    const { updateFormItems } = contextValues || { updateFormItems: null }
    const { data } = useSWR(url, url => axios(url).then(res => res.data.Data))

    const selectProps = showSearch
        ? {
              showSearch: true,
              optionFilterProp: 'label',
              filterOption: (input, option) =>
                  option.value !== null &&
                  option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0,
          }
        : null
    const optionItems = useMemo(
        () =>
            data &&
            data.reduce(
                (options, item) => [
                    ...options,
                    {
                        label: item.IsEnabled
                            ? item.Name
                            : `${item.Name}${
                                  item.IsEnabledText
                                      ? ` (${item.IsEnabledText})`
                                      : ''
                              }`,
                        value: item.Id,
                    },
                ],
                needAll
                    ? [
                          {
                              label: (
                                  <FormattedMessage id="Share.Dropdown.All" />
                              ),
                              value: optionAllValue,
                          },
                      ]
                    : []
            ),
        [data]
    )

    useEffect(() => {
        updateFormItems &&
            updateFormItems({
                [name]: !!data,
            })
    }, [data])

    if (!data) {
        return null
    }
    return (
        <Form.Item
            noStyle
            shouldUpdate={(prev, curr) => prev[name] !== curr[name]}
        >
            {({ getFieldValue }) => {
                const formItemProps = {
                    name: name,
                    ...restProps,
                }
                if (getFieldValue(name) === undefined && !placeholder) {
                    formItemProps.initialValue =
                        initialValue || (needAll ? optionAllValue : data[0].Id)
                }
                return (
                    <Form.Item {...formItemProps}>
                        <Select
                            {...selectProps}
                            disabled={disabled}
                            options={optionItems}
                            placeholder={placeholder}
                        />
                    </Form.Item>
                )
            }}
        </Form.Item>
    )
}
