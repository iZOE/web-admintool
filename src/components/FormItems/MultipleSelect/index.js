import React, { useContext, useEffect } from 'react'
import { Form, TreeSelect } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import { ConditionContext } from 'contexts/ConditionContext'
import { FormattedMessage } from 'react-intl'

const { SHOW_CHILD } = TreeSelect

export default function MultipleSelect({
    url,
    name,
    checkAll,
    disabled,
    initialValue = [],
    placeholder,
    ...restProps
}) {
    const ALL_VALUE = new Set()
    const contextValues = useContext(ConditionContext)
    const { updateFormItems } = contextValues || { updateFormItems: null }
    const { data } = useSWR(url, url =>
        axios(url).then(res => {
            return res.data.Data || null
        })
    )
    const genTreeData = data => {
        if (!data) {
            return []
        }
        return data.map(opt => {
            ALL_VALUE.add(opt.Id)
            return {
                title: opt.Name,
                value: opt.Id,
                key: opt.Id,
                children: opt.SubItems ? genTreeData(opt.SubItems) : null,
            }
        })
    }

    const treeData = [
        {
            title: (
                <FormattedMessage id="Share.Dropdown.All" description="全部" />
            ),
            value: '',
            key: '',
            children: genTreeData(data),
        },
    ]

    useEffect(() => {
        updateFormItems &&
            updateFormItems({
                [name]: !!data,
            })
    }, [data])

    if (!data) {
        return null
    }
    return (
        <Form.Item
            noStyle
            shouldUpdate={(prev, curr) => prev[name] !== curr[name]}
        >
            {({ getFieldValue }) => {
                const formItemProps = {
                    name: name,
                    ...restProps,
                }
                if (getFieldValue(name) === undefined) {
                    formItemProps.initialValue = checkAll
                        ? Array.from(ALL_VALUE)
                        : initialValue
                }
                return (
                    <Form.Item {...formItemProps}>
                        <TreeSelect
                            disabled={disabled}
                            treeData={treeData}
                            treeCheckable
                            showCheckedStrategy={SHOW_CHILD}
                            style={{
                                width: '100%',
                            }}
                            placeholder={placeholder}
                            maxTagCount={1}
                            maxTagPlaceholder={
                                <FormattedMessage id="Share.FormItem.TreeSelect.MaxTagPlaceholder" />
                            }
                        />
                    </Form.Item>
                )
            }}
        </Form.Item>
    )
}
