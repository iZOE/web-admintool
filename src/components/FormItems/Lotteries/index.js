import React, { useContext, useEffect } from 'react'
import { Form, TreeSelect } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'

const { SHOW_CHILD } = TreeSelect

export default function Lotteries({
    name = 'LotteryCode',
    checkAll,
    initialValue = [],
    label,
    disabled,
    ...restProps
}) {
    const ALL_VALUE = new Set()
    const contextValues = useContext(ConditionContext)
    const { updateFormItems } = contextValues || { updateFormItems: null }
    const { data } = useSWR('/api/Option/LotteryTypes/No3rdLotteries', url =>
        axios(url).then(res => res.data.Data)
    )

    const genTreeData = data => {
        if (!data) {
            return null
        }
        return data.map(opt => {
            if (!opt.SubItems) {
                ALL_VALUE.add(opt.Id)
            }
            return {
                title: opt.Name,
                value: opt.SubItems ? `Menu_${opt.Id}` : opt.Id,
                key: opt.SubItems ? `Menu_${opt.Id}` : opt.Id,
                children: genTreeData(opt.SubItems),
            }
        })
    }

    const treeData = [
        {
            title: (
                <FormattedMessage id="Share.Dropdown.All" description="全部" />
            ),
            value: '',
            key: '',
            children: genTreeData(data),
        },
    ]

    useEffect(() => {
        updateFormItems &&
            updateFormItems({
                [name]: !!data,
            })
    }, [data])

    if (!data) {
        return null
    }

    return (
        <Form.Item
            noStyle
            shouldUpdate={(prev, curr) => prev[name] !== curr[name]}
        >
            {({ getFieldValue }) => {
                const formItemProps = {
                    name: name,
                    ...restProps,
                }
                if (getFieldValue(name) === undefined) {
                    formItemProps.initialValue = checkAll
                        ? Array.from(ALL_VALUE)
                        : initialValue
                }
                return (
                    <Form.Item
                        {...formItemProps}
                        label={
                            label || (
                                <FormattedMessage
                                    id="Share.QueryCondition.LotteryCode"
                                    description="遊戲彩種"
                                />
                            )
                        }
                    >
                        <TreeSelect
                            disabled={disabled}
                            treeData={treeData}
                            treeCheckable
                            showCheckedStrategy={SHOW_CHILD}
                            placeholder={
                                <FormattedMessage
                                    id="Share.PlaceHolder.Input.PleaseSelectSomething"
                                    description="會員等級"
                                    values={{
                                        field: (
                                            <FormattedMessage
                                                id="Share.QueryCondition.LotteryCode"
                                                description="遊戲彩種"
                                            />
                                        ),
                                    }}
                                />
                            }
                            style={{
                                width: '100%',
                            }}
                            maxTagCount={1}
                            maxTagPlaceholder={values => (
                                <span>
                                    <FormattedMessage
                                        id="Share.FormItem.TreeSelect.MaxTagPlaceholder"
                                        description="placeholder"
                                        values={{
                                            count: values.length + 1,
                                        }}
                                    />
                                </span>
                            )}
                        />
                    </Form.Item>
                )
            }}
        </Form.Item>
    )
}
