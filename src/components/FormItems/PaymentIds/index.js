import React from 'react';
import { Form, TreeSelect } from 'antd';
import useSWR from 'swr';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';

const { SHOW_ALL } = TreeSelect;

function genTreeData(data) {
  if (!data) {
    return null;
  }

  return data.map(opt => ({
    title: opt.Name,
    value: opt.Id,
    key: opt.Id,
    children: genTreeData(opt.SubItems)
  }));
}

export default function PaymentIdsOptions() {
  const { data } = useSWR('/api/Option/PaymentIdList', url =>
    axios(url).then(res => res.data.Data)
  );

  const treeData = [
    {
      title: <FormattedMessage id="Share.Dropdown.All" description="全部" />,
      value: 'all',
      key: 'all',
      children: data ? genTreeData(data) : []
    }
  ];

  if (!data) {
    return null;
  }

  return (
    <Form.Item
      label={
        <FormattedMessage
          id="PagePaymentPreferentialSetting.DataTable.Title.DepositeTypeName"
          description="充值類型"
        />
      }
      name="PaymentIds"
      rules={[
        {
          required: true
        }
      ]}
    >
      <TreeSelect
        treeData={treeData}
        treeCheckable
        showCheckedStrategy={SHOW_ALL}
        placeholder={
          <FormattedMessage
            id="Share.PlaceHolder.Input.PleaseSelectSomething"
            description="請選擇"
            values={{
              field: (
                <FormattedMessage
                  id="PagePaymentPreferentialSetting.DataTable.Title.DepositeTypeName"
                  description="充值類型"
                />
              )
            }}
          />
        }
        style={{
          width: '100%'
        }}
        maxTagCount={1}
        maxTagPlaceholder={values => (
          <span>
            <FormattedMessage
              id="Share.FormItem.TreeSelect.MaxTagPlaceholder"
              description="共幾個"
              values={{
                count: values.length + 1
              }}
            />
          </span>
        )}
      />
    </Form.Item>
  );
}
