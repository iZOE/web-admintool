import React, { useMemo } from 'react';
import { Form, Input, Select } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';

const { Option } = Select;

const BANK_CARD_NUMBER_CONDITION_ID = 4;

function Condition({ initialValue = 1, ADVANCED_SEARCH_CONDITION_ITEMS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }) {
  const intl = useIntl();

  const PLACEHOLDER = useMemo(() => {
    return {
      plsEnter: intl.formatMessage({
        id: 'Share.PlaceHolder.Input.PlsEnter',
      }),
      bankCardNumber: intl.formatMessage({
        id: 'Share.PlaceHolder.Input.BankCardNumber',
      }),
    };
  }, []);

  return (
    <Form.Item label={<FormattedMessage id="Share.QueryCondition.AdvanceQuery" />}>
      <Input.Group compact>
        <Form.Item
          noStyle
          shouldUpdate={(prev, curr) => prev['AdvancedSearchConditionItem'] !== curr['AdvancedSearchConditionItem']}
        >
          {({ getFieldValue, setFieldsValue }) => {
            const formItemProps = {};
            if (getFieldValue('AdvancedSearchConditionItem') === undefined) {
              formItemProps.initialValue = initialValue || 1;
            }
            return (
              <Form.Item name="AdvancedSearchConditionItem" {...formItemProps} noStyle>
                <Select
                  style={{ width: '100px' }}
                  onChange={value => {
                    setFieldsValue({
                      AdvancedSearchConditionValue: value === 7 ? 0 : '',
                    });
                  }}
                >
                  {ADVANCED_SEARCH_CONDITION_ITEMS.map(index => (
                    <Option value={index} key={index.toString()}>
                      <FormattedMessage
                        id={`Share.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.${index}`}
                      />
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            );
          }}
        </Form.Item>
        <Form.Item shouldUpdate noStyle>
          {({ getFieldValue }) => {
            const itemValue = getFieldValue('AdvancedSearchConditionItem') || initialValue || 1;
            const advancedSearchItem = intl.formatMessage({
              id: `Share.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.${itemValue}`,
            });
            return (
              <Form.Item noStyle name="AdvancedSearchConditionValue">
                {itemValue === 7 ? (
                  <Select style={{ width: 'calc(100% - 100px)' }}>
                    <Option value={0}>
                      <FormattedMessage id="Share.QueryCondition.SelectItem.CurrentDividend.0" />
                    </Option>
                    <Option value={1}>
                      <FormattedMessage id="Share.QueryCondition.SelectItem.CurrentDividend.1" />
                    </Option>
                  </Select>
                ) : (
                  <Input
                    style={{ width: 'calc(100% - 100px)' }}
                    placeholder={
                      itemValue === BANK_CARD_NUMBER_CONDITION_ID
                        ? PLACEHOLDER.bankCardNumber
                        : PLACEHOLDER.plsEnter + advancedSearchItem
                    }
                  />
                )}
              </Form.Item>
            );
          }}
        </Form.Item>
      </Input.Group>
    </Form.Item>
  );
}

export default Condition;
