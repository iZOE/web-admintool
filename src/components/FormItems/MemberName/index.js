import React from "react";
import { Form, Input } from "antd";
import { FormattedMessage } from "react-intl";

export default function MemberNameFormItem({ required = true }) {
  return (
    <Form.Item
      label={<FormattedMessage id="MemberDetail.Fields.MemberAccount" />}
      name="MemberName"
      rules={[
        {
          required,
          message: <FormattedMessage id="Share.FormValidate.Required.Input" />
        }
      ]}
    >
      <Input />
    </Form.Item>
  );
}
