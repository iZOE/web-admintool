import React from 'react'
import { Form, Input } from 'antd'
import { FormattedMessage } from 'react-intl'

export const checkERC20Wallet = value => new RegExp(/^0x/g).test(value)
export const checkTRC20Wallet = value => new RegExp(/^T/g).test(value)
export const checkOmniWallet = value => new RegExp(/^1|^3/g).test(value)

const RULES = {
    REQUIRED: {
        required: true,
        message: <FormattedMessage id="Share.FormValidate.Required.Input" />,
    },
}

const CRYPTO_CURRENCY_TYPE = {
    ERC20: 1,
    TRC20: 2,
    OMNI: 3,
}

export default function AddCryptoCurrencyModalForm(props) {
    return (
        <Form.Item
            {...props}
            name="WalletAddress"
            label={
                props.noLabel ? null : (
                    <FormattedMessage id="MemberDetail.Tabs.BankData.Fields.WalletAddress" />
                )
            }
            shouldUpdate
            dependencies={['CryptoProtocolId']}
            rules={[
                RULES.REQUIRED,
                ({ getFieldValue }) => ({
                    validator(rule, value) {
                        const incorrectWalletAddress = (
                            <FormattedMessage
                                id="MemberDetail.Tabs.BankData.FormValidate.IncorrectWalletAddress"
                                defaultMessage="钱包地址格式错误"
                            />
                        )
                        const cryptoProtocolId = getFieldValue(
                            'CryptoProtocolId'
                        )
                        if (
                            cryptoProtocolId === CRYPTO_CURRENCY_TYPE.ERC20 &&
                            !checkERC20Wallet(value)
                        ) {
                            return Promise.reject(incorrectWalletAddress)
                        } else if (
                            cryptoProtocolId === CRYPTO_CURRENCY_TYPE.TRC20 &&
                            !checkTRC20Wallet(value)
                        ) {
                            return Promise.reject(incorrectWalletAddress)
                        } else if (
                            cryptoProtocolId === CRYPTO_CURRENCY_TYPE.OMNI &&
                            !checkOmniWallet(value)
                        ) {
                            return Promise.reject(incorrectWalletAddress)
                        } else {
                            return Promise.resolve(true)
                        }
                    },
                }),
            ]}
        >
            <Input maxLength={100} />
        </Form.Item>
    )
}
