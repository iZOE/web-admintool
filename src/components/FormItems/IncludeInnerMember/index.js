import React from 'react';
import { Form, Checkbox } from 'antd';
import { FormattedMessage } from 'react-intl';

export default function IncludeInnerMember({ name = 'IncludeInnerMember' }) {
  return (
    <Form.Item name={name} valuePropName="checked">
      <Checkbox>
        <FormattedMessage id="Share.QueryCondition.IncludeInnerMember" />
      </Checkbox>
    </Form.Item>
  );
}
