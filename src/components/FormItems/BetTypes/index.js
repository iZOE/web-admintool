import React, { useEffect, useMemo } from 'react';
import { Form, Select, Typography } from 'antd';
import useSWR from 'swr';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';

const { Option } = Select;
const { Text } = Typography;

export default function BetTypes({ name, ids }) {
  // 全部lottery資料
  const {
    data: lotteriesData
  } = useSWR('/api/Option/LotteryTypes/No3rdLotteries', url =>
    axios(url).then(res => res.data.Data)
  );

  // 要和lotteriesData做比對，如果有兩個以上的lottery group，也要回傳null
  const currentIds = useMemo(() => {
    const idsWithoutNull = ids && ids.filter(id => id);
    if (!idsWithoutNull || !idsWithoutNull.length) {
      return null;
    }

    const currentLotteries = new Set();
    if (lotteriesData) {
      idsWithoutNull.forEach(id => {
        const findLotteryFromSubItem = lotteriesData.filter(
          x => x.SubItems.filter(s => s.Id === id).length > 0
        );
        if (findLotteryFromSubItem) {
          if (!currentLotteries.has(findLotteryFromSubItem[0].Id)) {
            currentLotteries.add(findLotteryFromSubItem[0].Id);
          }
        }
      });
    }

    if (currentLotteries.size !== 1) {
      return null;
    }

    return idsWithoutNull;
  }, [ids, lotteriesData]);

  function genAxiosArr(idsArr) {
    return idsArr
      .filter(x => x !== null)
      .map(id =>
        axios.get(`/api/Option/BetTypes/${id}`).then(res => res.data.Data)
      );
  }

  const { data } = useSWR(
    currentIds ? ['/api/Option/BetTypes/', currentIds] : null,
    async url => {
      const betTypeGroup = {};
      const res = await Promise.all(genAxiosArr(currentIds));
      res.forEach(r => {
        r.forEach(data => {
          if (betTypeGroup[data.Name]) {
            betTypeGroup[data.Name].push(data.Id);
          } else {
            betTypeGroup[data.Name] = [data.Id];
          }
        });
      });
      return betTypeGroup;
    }
  );

  return (
    <>
      <Form.Item
        label={
          <FormattedMessage
            id="Share.QueryCondition.BetType"
            description="玩法"
          />
        }
        name={name || 'BetTypeId'}
      >
        {data && currentIds ? (
          <Select mode="multiple" style={{ width: '100%' }} defaultValue={[]}>
            {Object.keys(data).map(key => (
              <Option key={key} value={data[key]}>
                {key}
              </Option>
            ))}
          </Select>
        ) : (
          <Text>
            <FormattedMessage
              id="Share.FormItem.PleaseSelectSingleLottery"
              description="請先選擇單一彩種"
            />
          </Text>
        )}
      </Form.Item>
    </>
  );
}
