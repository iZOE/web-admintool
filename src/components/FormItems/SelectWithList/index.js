import React from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Spin } from 'antd'
import ListSelect from 'components/ListSelect'

export default function SelectWithList({ endpoint, ...props }) {
    let { data: list } = useSWR(endpoint, url =>
        axios(url).then(res => res.data.Data)
    )

    return list ? <ListSelect list={list} {...props} /> : <Spin />
}
