import React, { useContext, useEffect, useMemo } from 'react';
import { Form, Select } from 'antd';
import useSWR from 'swr';
import axios from 'axios';
import { FormattedMessage } from 'react-intl';
import { ConditionContext } from 'contexts/ConditionContext';

function LotteryItems({
  name = 'LotteryCode',
  needAll,
  initialValue,
  optionAllValue = null,
  ...restProps
}) {
  const contextValues = useContext(ConditionContext);
  const { updateFormItems } = contextValues || { updateFormItems: null };
  const { Option, OptGroup } = Select;
  const { data: lotteries } = useSWR(
    '/api/Option/LotteryTypes/Lotteries',
    url => axios(url).then(res => res.data.Data)
  );

  useEffect(() => {
    updateFormItems &&
      updateFormItems({
        LotteryCode: !!lotteries
      });
  }, [lotteries]);

  if (!lotteries) {
    return null;
  }
  return (
    <Form.Item noStyle shouldUpdate={(prev, curr) => prev[name] !== curr[name]}>
      {({ getFieldValue }) => {
        const formItemProps = {
          name: name,
          ...restProps
        };
        if (getFieldValue(name) === undefined) {
          formItemProps.initialValue =
            initialValue || (needAll ? optionAllValue : lotteries[0].Id);
        }
        return (
          <Form.Item
            {...formItemProps}
            label={<FormattedMessage id="Share.FormItem.LotteryItems" />}
          >
            <Select>
              {needAll && (
                <Option value={null}>
                  {<FormattedMessage id="Share.Dropdown.All" />}
                </Option>
              )}
              {lotteries &&
                lotteries.map(d => (
                  <OptGroup label={d.Name} key={d.Id}>
                    {d.SubItems.map(s => (
                      <Option value={s.Id} key={s.Id}>
                        {s.Name}
                      </Option>
                    ))}
                  </OptGroup>
                ))}
            </Select>
          </Form.Item>
        );
      }}
    </Form.Item>
  );
}

export default LotteryItems;
