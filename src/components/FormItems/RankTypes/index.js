import React from "react";
import { Form, Select, Typography } from "antd";
import useSWR from "swr";
import axios from "axios";
import { FormattedMessage } from "react-intl";

const { Option } = Select;

export default function RankTypes({ name }) {
  // 全部lottery資料
  const { data } = useSWR("/api/Option/Rank/RankTypes", (url) =>
    axios(url).then((res) => res.data.Data)
  );

  if (!data) {
    return null;
  }

  return (
    <>
      <Form.Item
        label={
          <FormattedMessage
            id="Share.QueryCondition.RankType"
            description="排行类型"
          />
        }
        name={name || "RankTypeId"}
        initialValue={data[0].Id}
      >
        {data && (
          <Select style={{ width: "100%" }}>
            {data.map(({ Id, Name }) => (
              <Option key={Id} value={Id}>
                {Name}
              </Option>
            ))}
          </Select>
        )}
      </Form.Item>
    </>
  );
}
