import { atom } from 'recoil'

export const permissionState = atom({
    key: 'permissionState',
    default: new Map(),
});