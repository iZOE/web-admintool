import moment from 'moment'
import { FORMAT } from 'constants/dateConfig'

// 給送去 API 用的
export const getISODateTimeString = (value, format = FORMAT.DEFAULT.FULL) => {
    return moment(value).format(format)
}

// 自 API 收進來，去掉時區並回傳成 moment 格式
export const getDateTimeStringWithoutTimeZone = value => {
    const _t = value && value.toString().split('')
    const isIsoFormat = _t.indexOf('+') > -1
    const result = isIsoFormat ? value.split('+')[0] : null

    return isIsoFormat ? moment(result) : null
}

// 顯示不同 locale 的時間格式
export const displayDateTime = value => {
    const isValid = value && typeof value === 'string'
    const result = isValid && value.split('+')[0]
    return result
        ? moment(result).format(FORMAT.DISPLAY.DEFAULT)
        : 'Invalid Date Time Format'
}
