import styled from 'styled-components';

export const Wrapper = styled.div`
  .container {
    height: 100vh;
    position: relative;
  }
  .login-panel {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 420px;
    padding: 60px 45px 30px;
    background: #fafafa;
    border: 1px solid #ccc;
    border-radius: 5px;
  }
  .title {
    margin-bottom: 16px;
    font-size: 36px;
    text-align: center;
  }
  .login-form-button {
    width: 100%;
    margin-top: 30px;
  }
  .icon-prefix {
    color: rgba(0, 0, 0, 0.25);
  }
`