import React, { useMemo, useState } from "react";
import { useCookies } from "react-cookie";
import { Form, Layout, Input, Button } from "antd";
import { LockOutlined, SafetyOutlined, UserOutlined } from "@ant-design/icons";
import { Formik } from "formik";
import * as Yup from "yup";
import { useIntl, FormattedMessage } from "react-intl";
import { mutate } from "swr";
import caller from "utils/fetcher";
import { Wrapper } from "./Styled";

const initialValues = {
  account: "",
  password: "",
  verifyCode: "",
};

export default function Login() {
  const intl = useIntl();

  const [loading, setLoading] = useState(false);
  const [_, setCookie] = useCookies();

  const validationSchema = useMemo(
    () =>
      Yup.object().shape({
        account: Yup.string().required(
          <FormattedMessage id="PageLogin.ErrorMessage.Account" />
        ),
        password: Yup.string().required(
          <FormattedMessage id="PageLogin.ErrorMessage.Password" />
        ),
        verifyCode: Yup.string().required(
          <FormattedMessage id="PageLogin.ErrorMessage.AuthencationCode" />
        ),
      }),
    [intl] // 必須放，不然localStorage.USER_LANG改變時，intl reference會變，才能生出新的錯誤訊息
  );

  function fetchLogin(payload) {
    setLoading(true);
    caller({
      method: "post",
      endpoint: "/api/User/Login",
      body: payload,
    })
      .then((data) => {
        setCookie("gameadmin_auth", data.Data, { path: "/" });
        setTimeout(() => {
          mutate("local/user");
        }, 1000);
      })
      .catch((err) => {
        console.log("ERR0>", err);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  return (
    <Wrapper>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          fetchLogin(values);
          setSubmitting(false);
        }}
      >
        {({
          values,
          touched,
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
        }) => (
          <Layout className="container">
            <Form className="login-panel">
              <h1 className="title">Admin Tool</h1>
              <Form.Item
                className={
                  errors.account && touched.account ? "has-error" : null
                }
              >
                <FormattedMessage id="PageLogin.Form.Account">
                  {(placeholder) => (
                    <Input
                      name="account"
                      value={values.account}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      prefix={<UserOutlined className="icon-prefix" />}
                      placeholder={placeholder}
                    />
                  )}
                </FormattedMessage>
                {errors.account && touched.account && (
                  <div className="ant-form-explain">{errors.account}</div>
                )}
              </Form.Item>
              <Form.Item
                className={
                  errors.password && touched.password ? "has-error" : null
                }
              >
                <FormattedMessage id="PageLogin.Form.Password">
                  {(placeholder) => (
                    <Input.Password
                      name="password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      prefix={<LockOutlined className="icon-prefix" />}
                      placeholder={placeholder}
                    />
                  )}
                </FormattedMessage>
                {errors.password && touched.password && (
                  <div className="ant-form-explain">{errors.password}</div>
                )}
              </Form.Item>
              <Form.Item
                className={
                  errors.verifyCode && touched.verifyCode ? "has-error" : null
                }
              >
                <FormattedMessage id="PageLogin.Form.AuthencationCode">
                  {(placeholder) => (
                    <Input
                      name="verifyCode"
                      value={values.verifyCode}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      prefix={<SafetyOutlined className="icon-prefix" />}
                      placeholder={placeholder}
                    />
                  )}
                </FormattedMessage>
                {errors.verifyCode && touched.verifyCode && (
                  <div className="ant-form-explain">{errors.verifyCode}</div>
                )}
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  loading={loading}
                  disabled={isSubmitting}
                  className="login-form-button"
                  onClick={handleSubmit}
                >
                  <FormattedMessage id="PageLogin.ActionButton.Confirm" />
                </Button>
              </Form.Item>
            </Form>
          </Layout>
        )}
      </Formik>
    </Wrapper>
  );
}
