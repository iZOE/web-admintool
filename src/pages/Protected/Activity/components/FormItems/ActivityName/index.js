import React, { useState } from 'react'
import caller from 'utils/fetcher'
import { Form, Input, AutoComplete } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'

import { OptionItem } from './Styled'

function ActivityName({ name }) {
    const [activityOptions, setActivityOptions] = useState([])
    const [currentKeyword, setCurrentKeyword] = useState('')
    const intl = useIntl()

    const renderTitle = title => <span>{title}</span>

    const renderItem = (activitys, keyword) => {
        const optionItems = []
        activitys.map(data => {
            const activityNameWithHint = data.ActivityName.replaceAll(
                keyword,
                `<span class="red">${keyword}</span>`
            )
            optionItems.push({
                value: data.ActivityName,
                key: data.ActivityId,
                label: (
                    <OptionItem
                        className="content"
                        dangerouslySetInnerHTML={{
                            __html: `${activityNameWithHint} (ID: ${data.ActivityId})`,
                        }}
                    ></OptionItem>
                ),
            })
        })
        return optionItems
    }

    function fetchActivity(keyword) {
        setCurrentKeyword(keyword)
        caller({
            method: 'post',
            endpoint: '/api/v2/Activity/Name/Search',
            body: {
                KeyWord: keyword,
            },
        }).then(res => {
            if (res.Data) {
                setActivityOptions([
                    {
                        label: renderTitle(
                            intl.formatMessage({
                                id:
                                    'PageOperationActivityManagement.QueryCondition.SelectItem.ActivityStatus.2',
                                defaultMessage: '进行中',
                            })
                        ),
                        options: renderItem(res.Data.Processing, keyword),
                    },
                    {
                        label: renderTitle(
                            intl.formatMessage({
                                id:
                                    'PageOperationActivityManagement.QueryCondition.SelectItem.ActivityStatus.1',
                                defaultMessage: '未开始',
                            })
                        ),
                        options: renderItem(res.Data.UnStarted, keyword),
                    },
                ])
            } else {
                setActivityOptions(null)
            }
        })
    }

    return (
        <Form.Item
            label={
                <FormattedMessage id="PageOperationActivityManagement.QueryCondition.Name" />
            }
            name={name || 'Subject'}
        >
            <AutoComplete
                dropdownClassName="certain-category-search-dropdown"
                dropdownMatchSelectWidth={500}
                options={activityOptions}
                onChange={fetchActivity}
                onFocus={() => fetchActivity(currentKeyword)}
            >
                <Input
                    placeholder={intl.formatMessage({
                        id:
                            'PageOperationActivityManagement.QueryCondition.NameHint',
                        defaultMessage: '請輸入活動名稱',
                    })}
                />
            </AutoComplete>
        </Form.Item>
    )
}

export default ActivityName
