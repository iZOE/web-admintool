import React, { createContext } from 'react'
import { Space, message } from 'antd'
import { useParams, useLocation } from 'react-router-dom'
import { useIntl, FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import LayoutPageBody from 'layouts/Page/Body'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import ReportScaffold from 'components/ReportScaffold'
import DataTable from 'components/DataTable'
import ExportReportButton from 'components/ExportReportButton'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import SummaryView from './Summary'

const { ACTIVITY_REPORT_VIEW, ACTIVITY_REPORT_EXPORT } = PERMISSION

export const PageContext = createContext()

const PageView = () => {
  const dispatchSuccessMsg = useIntl().formatMessage({
    id: 'PageOperationActivityReportDetail.Message.Dispatch',
  })
  const rejectSuccessMsg = useIntl().formatMessage({
    id: 'PageOperationActivityReportDetail.Message.Reject',
  })
  const errorMsg = useIntl().formatMessage({
    id: 'PageOperationActivityReportDetail.Message.Fail',
  })

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    onReady,
    boundedMutate,
  } = useGetDataSourceWithSWR({
    url: `/api/v2/Activity/Report/Detail/Search`,
    defaultSortKey: 'MemberName',
    autoFetch: true,
  })
  const { activityid } = useParams()
  const location = useLocation()

  function handleDispatch(value) {
    caller({
      method: 'post',
      endpoint: '/api/v2/Activity/BonusDispatch',
      body: value,
    })
      .then(() => {
        if (value.IsDispatch) {
          message.success(dispatchSuccessMsg)
        } else {
          message.success(rejectSuccessMsg)
        }
        boundedMutate()
      })
      .catch(err => {
        message.error(errorMsg)
      })
  }

  return (
    <PageContext.Provider value={{ onDispatch: handleDispatch }}>
      <Permission isPage functionIds={[ACTIVITY_REPORT_VIEW]}>
        <LayoutPageBody
          title={
            <FormattedMessage id="PageOperationActivityReportDetail.Title" />
          }
          subTitle={
            <span style={{ color: 'grey' }}>
              <FormattedMessage
                id="PageOperationActivityReportDetail.SubTitle"
                values={{
                  subject: location.state.Subject,
                  activityid: activityid,
                }}
              />
            </span>
          }
        >
          <ReportScaffold
            displayResult={condition}
            conditionComponent={
              <Condition onReady={onReady} onUpdate={onUpdateCondition} />
            }
            summaryComponent={
              <SummaryView summary={dataSource && dataSource.Summary} />
            }
            datatableComponent={
              <DataTable
                displayResult={condition}
                condition={condition}
                title={
                  <FormattedMessage
                    id="Share.Table.SearchResult"
                    description="查询结果"
                  />
                }
                config={COLUMNS_CONFIG}
                loading={fetching}
                dataSource={dataSource && dataSource.List}
                total={dataSource && dataSource.TotalCount}
                extendArea={
                  <Space>
                    <Permission functionIds={[ACTIVITY_REPORT_EXPORT]}>
                      <ExportReportButton
                        condition={condition}
                        actionUrl="/api/v2/Activity/Report/Detail/Export"
                      />
                    </Permission>
                  </Space>
                }
                onUpdate={onUpdateCondition}
              />
            }
          />
        </LayoutPageBody>
      </Permission>
    </PageContext.Provider>
  )
}

export default PageView
