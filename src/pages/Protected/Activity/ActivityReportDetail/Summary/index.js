import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary }) => {
    return summary ? (
        <Row gutter={24}>
            <Col span={8}>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageOperationActivityReportDetail.Summary.AchievedGoal"
                            description="达标人数"
                        />
                    }
                    value={summary.AchievedGoal}
                />
            </Col>
            <Col span={8}>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageOperationActivityReportDetail.Summary.Received"
                            description="已派发人数"
                        />
                    }
                    value={summary.Received}
                />
            </Col>
            <Col span={8}>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageOperationActivityReportDetail.Summary.ReceivedActivityBonus"
                            description="已派发金额"
                        />
                    }
                    value={summary.ReceivedActivityBonus}
                    precision={2}
                />
            </Col>
        </Row>
    ) : (
        <Skeleton active />
    )
}

export default SummaryView
