import React, { useContext } from 'react'
import { useParams, useLocation } from 'react-router-dom'
import { Button, Popconfirm } from 'antd'
import { FormattedMessage } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import { DISPATCH_REPORT_DETAIL_STATUS } from 'constants/dispatchType'
import { PageContext } from './../../index'

const { ACTIVITY_REPORT_DISPATCH, ACTIVITY_REPORT_REJECT } = PERMISSION

export default function ButtonItems({ record }) {
  const { ActivityBonusDetailId, MemberName, DispatchStatus } = record

  const { activityid } = useParams()
  const location = useLocation()

  const { onDispatch } = useContext(PageContext)

  const handleDispatch = () =>
    onDispatch({
      Id: ActivityBonusDetailId,
      IsDispatch: true,
      IsBatch: false,
    })
  const handleReject = () =>
    onDispatch({
      Id: ActivityBonusDetailId,
      IsDispatch: false,
      IsBatch: false,
    })

  return (
    <>
      <Permission functionIds={[ACTIVITY_REPORT_DISPATCH]}>
        {DispatchStatus !== DISPATCH_REPORT_DETAIL_STATUS.NOTDISPATCH ? (
          <Button
            type="link"
            disabled={
              DispatchStatus !== DISPATCH_REPORT_DETAIL_STATUS.NOTDISPATCH
            }
          >
            <FormattedMessage id="Share.ActionButton.Distribution" />
          </Button>
        ) : (
          <Popconfirm
            title={
              <>
                <div>
                  <FormattedMessage
                    id="PageOperationActivityReportDetail.Popconfirm.Title"
                    values={{
                      subject: location.state.Subject,
                      activityid: activityid,
                    }}
                  />
                </div>
                <span style={{ color: '#FF0000' }}>
                  <FormattedMessage id="PageOperationActivityReportDetail.Popconfirm.Dispatch" />
                </span>
                <span style={{ margin: '0 5px' }}>{MemberName}</span>
                <FormattedMessage id="PageOperationActivityReportDetail.Popconfirm.Hint1" />
                ?
              </>
            }
            onConfirm={() => handleDispatch()}
            okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
            cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
            placement="topLeft"
          >
            <Button
              type="link"
              disabled={
                DispatchStatus !== DISPATCH_REPORT_DETAIL_STATUS.NOTDISPATCH
              }
            >
              <FormattedMessage id="Share.ActionButton.Distribution" />
            </Button>
          </Popconfirm>
        )}
      </Permission>
      <Permission functionIds={[ACTIVITY_REPORT_REJECT]}>
        {DispatchStatus !== DISPATCH_REPORT_DETAIL_STATUS.NOTDISPATCH ? (
          <Button
            type="link"
            danger
            disabled={
              DispatchStatus !== DISPATCH_REPORT_DETAIL_STATUS.NOTDISPATCH
            }
          >
            <FormattedMessage id="Share.ActionButton.Refuse" />
          </Button>
        ) : (
          <Popconfirm
            title={
              <>
                <div>
                  <FormattedMessage
                    id="PageOperationActivityReportDetail.Popconfirm.Title"
                    values={{
                      subject: location.state.Subject,
                      activityid: activityid,
                    }}
                  />
                </div>
                <span style={{ color: '#FF0000' }}>
                  <FormattedMessage id="PageOperationActivityReportDetail.Popconfirm.Reject" />
                </span>
                <span style={{ margin: '0 5px' }}>{MemberName}</span>
                <FormattedMessage id="PageOperationActivityReportDetail.Popconfirm.Hint1" />
                ?
              </>
            }
            onConfirm={() => handleReject()}
            okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
            cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
            placement="topLeft"
          >
            <Button
              type="link"
              danger
              disabled={
                DispatchStatus !== DISPATCH_REPORT_DETAIL_STATUS.NOTDISPATCH
              }
            >
              <FormattedMessage id="Share.ActionButton.Refuse" />
            </Button>
          </Popconfirm>
        )}
      </Permission>
    </>
  )
}
