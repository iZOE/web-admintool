import React from 'react'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import DateRangeWithIcon from 'components/DateRangeWithIcon'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import ActionButton from './components/ActionButton'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'

export const COLUMNS_CONFIG = [
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityReportDetail.DataTable.MemberName"
        description="会员帐号"
      />
    ),
    dataIndex: 'MemberName',
    key: 'MemberName',
    width: 160,
    fixed: 'left',
    sorter: (a, b) => {
      return
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { MemberName }, _2) => {
      return <OpenMemberDetailButton memberName={MemberName} />
    },
  },
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityReportDetail.DataTable.Date"
        description="结算区间"
      />
    ),
    dataIndex: 'CalcuBeginDate',
    key: 'CalcuBeginDate',
    width: 400,
    sorter: (a, b) => {
      return
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CalcuBeginDate: s, CalcuEndDate: e }, _2) => (
      <DateRangeWithIcon startTime={s} endTime={e} />
    ),
  },
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityReportDetail.DataTable.ActivityBonus"
        description="活动奖励金额"
      />
    ),
    dataIndex: 'ActivityBonus',
    key: 'ActivityBonus',
    width: 160,
    sorter: (a, b) => {
      return
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityReportDetail.DataTable.DispatchStatus"
        description="状态"
      />
    ),
    dataIndex: 'DispatchStatusText',
    key: 'DispatchStatus',
    width: 120,
    sorter: (a, b) => {
      return
    },
  },
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityReportDetail.DataTable.ModifyTime"
        description="更新时间"
      />
    ),
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    width: 200,
    sorter: (a, b) => {
      return
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityReportDetail.DataTable.ModifyUser"
        description="派发人员"
      />
    ),
    dataIndex: 'ModifyUser',
    key: 'ModifyUserId',
    width: 120,
    sorter: (a, b) => {
      return
    },
  },
  {
    dataIndex: 'action',
    key: 'action',
    width: 160,
    fixed: 'right',
    render: (_1, record) => <ActionButton record={record} />,
  },
]
