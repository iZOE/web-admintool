import React, { useContext, useEffect } from 'react'
import { Form, Button, Row, Col, Input } from 'antd'
import { useParams, useLocation } from 'react-router-dom'
import { SearchOutlined } from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'

const { useForm } = Form

function Condition({ onReady, onUpdate }) {
    const { activityid } = useParams()
    const location = useLocation()
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()
    const { formatMessage } = useIntl()

    const CONDITION_INITIAL_VALUE = {
        ActivityBonusId: Number(location.state.ActivityBonusId),
        ActivityId: Number(activityid),
        DispatchStatus: null,
        MemberName: null,
    }

    const plsEnter = formatMessage({
        id: 'Share.PlaceHolder.Input.PlsEnter',
    })

    useEffect(() => {
        onUpdate(CONDITION_INITIAL_VALUE)
        onReady(isReady)
    }, [isReady])

    function onFinish(values) {
        const payload = {
            ...values,
            ActivityBonusId: Number(location.state.ActivityBonusId),
            ActivityId: Number(activityid),
            DispatchStatus: values.DispatchStatus,
            MemberName: values.MemberName,
        }
        onUpdate.bySearch(payload)
    }

    return (
        <Form
            labelAlign="left"
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <FormItemsSimpleSelect
                        needAll
                        url="/api/Option/Activity/DispatchStatus"
                        label={
                            <FormattedMessage
                                id="PageOperationActivityReportDetail.QueryCondition.DispatchStatus"
                                description="状态"
                            />
                        }
                        name={'DispatchStatus'}
                    />
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageOperationActivityReportDetail.QueryCondition.MemberName" />
                        }
                        name="MemberName"
                    >
                        <Input
                            placeholder={
                                `${plsEnter}` +
                                formatMessage({
                                    id:
                                        'PageOperationActivityReportDetail.QueryCondition.MemberName',
                                })
                            }
                        />
                    </Form.Item>
                </Col>
                <Col sm={2} align="right">
                    <Button
                        type="primary"
                        htmlType="submit"
                        icon={<SearchOutlined />}
                    >
                        {<FormattedMessage id="Share.ActionButton.Query" />}
                    </Button>
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
