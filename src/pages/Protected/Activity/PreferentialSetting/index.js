import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Create from './Create'
import Detail from './Detail'

export default function PreferentialSetting() {
    return (
        <Switch>
            <Route exact path="/activity/preferential-setting">
                <Home />
            </Route>
            <Route path="/activity/preferential-setting/create">
                <Create />
            </Route>
            <Route path="/activity/preferential-setting/detail/:id">
                <Detail />
            </Route>
        </Switch>
    )
}
