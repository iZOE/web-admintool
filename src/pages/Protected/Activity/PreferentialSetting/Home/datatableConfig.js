import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Tooltip } from 'antd';
import DateWithFormat from 'components/DateWithFormat';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import PreferentialNameButton from './PreferentialNameButton';
import EditButton from './EditButton';

export const COLUMNS_CONFIG = [
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.DepositPreferentialId" description="序號" />
    ),
    dataIndex: 'DepositPreferentialId',
    key: 'DepositPreferentialId',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialName" description="優惠名稱" />
    ),
    dataIndex: 'PreferentialName',
    key: 'PreferentialName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => (
      <PreferentialNameButton
        id={record.DepositPreferentialId}
        name={record.PreferentialName}
        memberLevelName={record.MemberLevelName}
      />
    ),
  },
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.DepositeTypeName" description="充值類型" />
    ),
    dataIndex: 'DepositeTypeName',
    key: 'DepositeTypeName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.MemberLevelName" description="會員等級" />
    ),
    dataIndex: 'MemberLevels',
    key: 'MemberLevels',
    isShow: true,
    render: (text, record) => (
      <Tooltip title={'點擊看更多'}>
        <PreferentialNameButton id={record.DepositPreferentialId}>
          <div
            style={{
              textOverflow: 'ellipsis',
              overflow: 'hidden',
              whiteSpace: 'nowrap',
              maxWidth: 190,
            }}
          >
            {record.MemberLevels.map(t => <span key={t.Id}>{t.Name}</span>).reduce((prev, curr) => [prev, ', ', curr])}
          </div>
        </PreferentialNameButton>
      </Tooltip>
    ),
  },
  {
    title: <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.IsFirstTime" description="僅限首儲" />,
    dataIndex: 'IsFirstTime',
    key: 'IsFirstTime',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (
      <FormattedMessage id={`PagePaymentPreferentialSetting.DataTable.Value.IsFirstTimeStatus.${text || 'false'}`} />
    ),
  },
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.StartLimitAmount" description="起始限額" />
    ),
    dataIndex: 'StartLimitAmount',
    key: 'StartLimitAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialRatio" description="優惠比例" />
    ),
    dataIndex: 'PreferentialRatio',
    key: 'PreferentialRatio',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: (
      <FormattedMessage
        id="PagePaymentPreferentialSetting.DataTable.Title.MaxPreferentialAmount"
        description="優惠上限"
      />
    ),
    dataIndex: 'MaxPreferentialAmount',
    key: 'MaxPreferentialAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialTime" description="優惠期間" />
    ),
    dataIndex: 'PreferentialTime',
    key: 'PreferentialTime',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { StartTime: s, EndTime: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.IsEnable" description="狀態" />,
    dataIndex: 'Enabled',
    key: 'Enabled',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (
      <FormattedMessage id={`PagePaymentPreferentialSetting.DataTable.Value.IsEnable.${text || 'false'}`} />
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage id="PagePaymentPreferentialSetting.DataTable.Title.ModifyUserAccount" description="異動人員" />
    ),
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
  },
  {
    title: ' ',
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => <EditButton id={record.DepositPreferentialId} />,
  },
];
