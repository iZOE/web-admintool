import React, { useState, createContext } from 'react'
import { Result, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import * as PERMISSION from 'constants/permissions'
import ReportScaffold from 'components/ReportScaffold'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import Condition from './Condition'
import DetailModal from './DetailModal'
import { COLUMNS_CONFIG } from './datatableConfig'

const {
    ACTIVITY_PREFERENTIAL_SETTING_VIEW,
    ACTIVITY_PREFERENTIAL_SETTING_CREATE,
} = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    const [modalData, setModalData] = useState({
        id: null,
        visible: false,
    })

    const {
        fetching,
        dataSource,
        condition,
        onReady,
        onUpdateCondition,
    } = useGetDataSourceWithSWR({
        url: '/api/DepositPreferentialSetting/Search',
        defaultSortKey: 'StartTime',
        autoFetch: true,
    })

    function onOpenDetailModal({ id, memberLevelName }) {
        setModalData({
            id,
            visible: true,
            memberLevelName,
        })
    }

    function onCloseDetailModal() {
        setModalData({
            id: null,
            visible: false,
        })
    }

    return (
        // 為了配合啟動全螢幕時 後續的流程 可以抓到domNode
        <PageContext.Provider
            value={{ test: 1, onCloseDetailModal, onOpenDetailModal }}
        >
            <DetailModal {...modalData} onOk={onCloseDetailModal} />
            <Permission
                functionIds={[ACTIVITY_PREFERENTIAL_SETTING_VIEW]}
                isPage
                failedRender={
                    <Result
                        status="warning"
                        title={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                        }
                        subTitle={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                        }
                    />
                }
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢結果"
                                />
                            }
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            onUpdate={onUpdateCondition}
                            rowKey={record => record.DepositPreferentialId}
                            extendArea={
                                <Permission
                                    functionIds={[
                                        ACTIVITY_PREFERENTIAL_SETTING_CREATE,
                                    ]}
                                >
                                    <Link
                                        to={
                                            '/activity/preferential-setting/create'
                                        }
                                    >
                                        <Button
                                            type="primary"
                                            icon={<PlusOutlined />}
                                        >
                                            <FormattedMessage
                                                id="PagePaymentPreferentialSetting.DataTable.ExtendArea.CreateButton"
                                                description="新增設定"
                                            />
                                        </Button>
                                    </Link>
                                </Permission>
                            }
                        />
                    }
                />
            </Permission>
        </PageContext.Provider>
    )
}

export default PageView

// /payment/preferential-setting/create
