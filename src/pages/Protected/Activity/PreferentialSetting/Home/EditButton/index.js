import React from 'react'
import { useHistory } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'

import { Button } from 'antd'

export default function EditButton({ id }) {
    const history = useHistory()
    return (
        <Button
            type="link"
            onClick={() => {
                history.push(`/activity/preferential-setting/detail/${id}`)
            }}
        >
            <FormattedMessage id="Share.ActionButton.Edit" />
        </Button>
    )
}
