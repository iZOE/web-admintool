import React from 'react'
import { Modal, Descriptions, Skeleton, List, Typography, Divider } from 'antd'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import DateRangeWithIcon from 'components/DateRangeWithIcon'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

export default function DetailModal({ visible, id, onOk }) {
    const { data } = useSWR(
        visible && id ? `/api/DepositPreferentialSetting/${id}` : null,
        url => axios(url).then(res => res && res.data.Data)
    )

    return (
        <Modal
            title={
                <FormattedMessage
                    id="PagePaymentPreferentialSetting.Modal.Title"
                    description="充值优惠订单明细"
                />
            }
            visible={visible}
            width={900}
            onOk={onOk}
            onCancel={onOk}
        >
            {data ? (
                <>
                    <Descriptions
                        title={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialName"
                                description="優惠名稱"
                            />
                        }
                    >
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialTime"
                                    description="時間"
                                />
                            }
                        >
                            {data.PreferentialName}
                        </Descriptions.Item>
                        <Descriptions.Item
                            span={2}
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialTime"
                                    description="優惠期間"
                                />
                            }
                        >
                            <DateRangeWithIcon
                                startTime={data.StartTime}
                                endTime={data.EndTime}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.IsEnable"
                                    description="狀態"
                                />
                            }
                        >
                            <FormattedMessage
                                id={`PagePaymentPreferentialSetting.DataTable.Value.IsEnable.${data.Enabled ||
                                    'false'}`}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            span={2}
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.MemberLevelName"
                                    description="會員等級"
                                />
                            }
                        >
                            {data.MemberLevels.map(({ Name }, index) => (
                                <span key={`name_${index}`}>{Name}</span>
                            )).reduce((prev, curr) => [prev, ', ', curr])}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.IsFirstTime"
                                    description="僅限首儲"
                                />
                            }
                        >
                            <FormattedMessage
                                id={`PagePaymentPreferentialSetting.DataTable.Value.IsFirstTimeStatus.${data.IsFirstTime ||
                                    'false'}`}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.StartLimitAmount"
                                    description="起始限額"
                                />
                            }
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={data.StartLimitAmount}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialRatio"
                                    description="優惠比例"
                                />
                            }
                        >
                            {data.PreferentialRatio}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.DataTable.Title.MaxPreferentialAmount"
                                    description="優惠上限"
                                />
                            }
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={data.MaxPreferentialAmount}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.Common.DailyPreferentialAmountLimit"
                                    description="每日優惠限額"
                                />
                            }
                        >
                            {data.DailyPreferentialAmountLimit ? (
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={data.DailyPreferentialAmountLimit}
                                />
                            ) : (
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.Common.DailyPreferentialAmountLimitInfinity"
                                    description="無限"
                                />
                            )}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.Common.PreferentialTurnoverMultiple"
                                    description="優惠流水倍數"
                                />
                            }
                        >
                            {data.PreferentialTurnoverMultiple}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.Common.NormalTurnoverMultiple"
                                    description="常態流水倍數"
                                />
                            }
                        >
                            {data.NormalTurnoverMultiple}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PagePaymentPreferentialSetting.Common.AdministrationRatio"
                                    description="行政費率"
                                />
                            }
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={data.AdministrationRatio}
                            />
                        </Descriptions.Item>
                    </Descriptions>
                    <Divider orientation="left">
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.DepositeTypeName"
                            description="充值类型"
                        />
                    </Divider>
                    <List
                        footer={<div>共 {data.Payments.length} 筆</div>}
                        bordered
                        dataSource={data.Payments}
                        renderItem={item => (
                            <List.Item>
                                <Typography.Text mark>
                                    {item.Id}
                                </Typography.Text>{' '}
                                {item.Name || (
                                    <FormattedMessage
                                        id="Share.CommonKeys.NoName"
                                        description="充值类型"
                                    />
                                )}
                            </List.Item>
                        )}
                    />
                </>
            ) : (
                <Skeleton />
            )}
        </Modal>
    )
}
