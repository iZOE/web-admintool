import React, { useContext } from 'react'
import { Button } from 'antd'
import { PageContext } from '../index'

export default function PreferentialNameButton({
    id,
    name,
    memberLevelName,
    children,
}) {
    const { onOpenDetailModal } = useContext(PageContext)

    return (
        <Button
            type="link"
            style={{ padding: 0, textAlign: 'left' }}
            onClick={() => onOpenDetailModal({ id, memberLevelName })}
        >
            {name || children}
        </Button>
    )
}
