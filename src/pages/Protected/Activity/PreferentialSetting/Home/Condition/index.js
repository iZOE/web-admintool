import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Checkbox, Select } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import FormItemsMemberLevel from 'components/FormItems/MemberLevel'
import QueryButton from 'components/FormActionButtons/Query'

const { useForm } = Form
const { Option } = Select

const DEPOSIT_TYPE_IDS = [0, 1, 2]
const CONDITION_INITIAL_VALUE = {
    DepositTypeId: null,
    IsFirstTime: true,
    IsEnable: true,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()

    useEffect(() => {
        onUpdate(form.getFieldsValue())
        onReady(isReady)
    }, [isReady])

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onUpdate.bySearch}
        >
            <Row gutter={[16, 8]}>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.QueryCondition.DepositType"
                                description="類型"
                            />
                        }
                        name="DepositTypeId"
                    >
                        <Select>
                            {DEPOSIT_TYPE_IDS.map(id => (
                                <Option value={id || null} key={id}>
                                    <FormattedMessage
                                        id={`PagePaymentPreferentialSetting.QueryCondition.SelectItem.DepositTypeId.${id}`}
                                        description="充值類型byId"
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <FormItemsMemberLevel checkAll />
                </Col>
                <Col sm={5}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.QueryCondition.IsFirstTime"
                                description="仅限首储"
                            />
                        }
                        name="IsFirstTime"
                        valuePropName="checked"
                    >
                        <Checkbox>
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.QueryCondition.SelectItem.OpenFirstDeposit"
                                description="仅限首储"
                            />
                        </Checkbox>
                    </Form.Item>
                </Col>
                <Col sm={5}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.QueryCondition.IsEnable"
                                description="仅限有效"
                            />
                        }
                        name="IsEnable"
                        valuePropName="checked"
                    >
                        <Checkbox>
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.QueryCondition.SelectItem.OpenEnableOnly"
                                description="仅限首储"
                            />
                        </Checkbox>
                    </Form.Item>
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
