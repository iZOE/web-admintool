import React, { useMemo } from 'react'
import { useHistory } from 'react-router-dom'
import {
    DatePicker,
    Switch,
    Form,
    Input,
    InputNumber,
    Button,
    Space,
    Modal,
} from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'react-intl'
import FormItemsMemberLevel from 'components/FormItems/MemberLevel'
import FormItemsPaymentIds from 'components/FormItems/PaymentIds'
import { Wrapper } from './Styled'
import { DEFAULT, FORMAT } from 'constants/dateConfig'
import {
    getISODateTimeString,
    getDateTimeStringWithoutTimeZone,
} from 'mixins/dateTime'

const { RangePicker } = DatePicker
const { confirm } = Modal
const { useForm } = Form

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 8,
    },
}
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
}

const DEFAULT_FORM_VALUES = {
    Enabled: false,
    IsFirstTime: false,
}

export default function ActiveForm({ defaultValues, onSubmit }) {
    const history = useHistory()
    const [form] = useForm()
    const intl = useIntl()

    const initialValues = useMemo(() => {
        const { StartTime, EndTime, ...restValues } = defaultValues || {}

        return {
            ...DEFAULT_FORM_VALUES,
            PreferentialTime: defaultValues
                ? [
                      getDateTimeStringWithoutTimeZone(StartTime),
                      getDateTimeStringWithoutTimeZone(EndTime),
                  ]
                : DEFAULT.RANGE.FROM_TOMORROW_TO_A_MONTH,
            ...restValues,
        }
    }, [defaultValues])

    const onFinish = values => {
        confirm({
            title: intl.formatMessage({
                id: 'Share.ActionButton.Save',
                description: '保存',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: 'Share.ConfirmTips.WhetherToSaveExistingChanges',
                description: '取消',
            }),
            okText: intl.formatMessage({
                id: 'Share.ActionButton.Save',
                description: '保存',
            }),
            cancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
                description: '取消',
            }),
            onOk() {
                values.StartTime = getISODateTimeString(
                    values.PreferentialTime[0]
                )
                values.EndTime = getISODateTimeString(
                    values.PreferentialTime[1]
                )
                values.PaymentIds = values.PaymentIds
                    ? values.PaymentIds.filter(id => id !== '' && !isNaN(id))
                    : null
                values.MemberLevelIds = values.MemberLevelIds
                    ? values.MemberLevelIds.filter(
                          id => id !== '' && !isNaN(id)
                      )
                    : null

                delete values.PreferentialTime
                onSubmit({ values })
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    function showResetConfirm() {
        confirm({
            title: intl.formatMessage({
                id: 'Share.CommonKeys.FillInAgain',
                description: '重新填写',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: 'Share.ConfirmTips.DoYouWantToReeditTheContent',
                description: '是否要重新编辑内容？',
            }),
            okText: intl.formatMessage({
                id: 'Share.ActionButton.Restart',
                description: '重來',
            }),
            okType: 'danger',
            cancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
                description: '取消',
            }),
            onOk() {
                form.resetFields()
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    function showCancelConfirm() {
        confirm({
            title: intl.formatMessage({
                id: 'Share.CommonKeys.GiveUpEditing',
                description: '放棄編輯',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage(
                {
                    id: 'Share.ConfirmTips.DoYouWantToGiveUpEditingAndReturn',
                    description: '是否要放弃编辑内容，回到充值優惠列表',
                },
                {
                    targetname: intl.formatMessage({
                        id: 'PagePaymentPreferentialSetting.PageHeader',
                        description: '充值優惠列表',
                    }),
                }
            ),
            okText: intl.formatMessage({
                id: 'Share.ActionButton.Abandon',
                description: '放棄',
            }),
            okType: 'danger',
            cancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
                description: '取消',
            }),
            onOk() {
                history.push('/activity/preferential-setting')
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    return (
        <Wrapper>
            <Form
                {...layout}
                form={form}
                name="control-hooks"
                onFinish={onFinish}
                initialValues={initialValues}
            >
                <Form.Item
                    name="PreferentialName"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialName"
                            description="優惠名稱"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                        {
                            max: 20,
                        },
                    ]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name="PreferentialTime"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialTime"
                            description="優惠期間"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <RangePicker format={FORMAT.DISPLAY.DEFAULT} showTime />
                </Form.Item>
                <Form.Item
                    name="Enabled"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.IsEnable"
                            description="狀態"
                        />
                    }
                    valuePropName="checked"
                >
                    <Switch
                        checkedChildren={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.DataTable.Value.IsEnable.true"
                                description="啟用"
                            />
                        }
                        unCheckedChildren={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.DataTable.Value.IsEnable.false"
                                description="停用"
                            />
                        }
                    />
                </Form.Item>
                <FormItemsPaymentIds />
                <FormItemsMemberLevel name="MemberLevelIds" />
                <Form.Item
                    name="IsFirstTime"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.IsFirstTime"
                            description="僅限首儲"
                        />
                    }
                    valuePropName="checked"
                >
                    <Switch
                        checkedChildren={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.DataTable.Value.IsFirstTimeStatus.true"
                                description="啟用"
                            />
                        }
                        unCheckedChildren={
                            <FormattedMessage
                                id="PagePaymentPreferentialSetting.DataTable.Value.IsFirstTimeStatus.false"
                                description="停用"
                            />
                        }
                    />
                </Form.Item>
                <Form.Item
                    name="StartLimitAmount"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.StartLimitAmount"
                            description="起始限額"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <InputNumber
                        placeholder=""
                        min={0}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    name="PreferentialRatio"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.PreferentialRatio"
                            description="优惠比例"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <InputNumber
                        placeholder=""
                        min={0}
                        formatter={value => `${value}%`}
                        parser={value => value.replace('%', '')}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    name="MaxPreferentialAmount"
                    type="number"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.DataTable.Title.MaxPreferentialAmount"
                            description="优惠上限"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <InputNumber
                        placeholder=""
                        min={0}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    name="DailyPreferentialAmountLimit"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.Common.DailyPreferentialAmountLimit"
                            description="每日優惠限額"
                        />
                    }
                    extra="沒填則視為沒有限制"
                >
                    <InputNumber
                        placeholder=""
                        min={0}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    name="PreferentialTurnoverMultiple"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.Common.PreferentialTurnoverMultiple"
                            description="優惠流水倍數"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <InputNumber
                        placeholder=""
                        min={0}
                        formatter={value => `${value}倍`}
                        parser={value => value.replace('倍', '')}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    name="NormalTurnoverMultiple"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.Common.NormalTurnoverMultiple"
                            description="常態流水倍數"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <InputNumber
                        placeholder=""
                        min={0}
                        formatter={value => `${value}倍`}
                        parser={value => value.replace('倍', '')}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    name="AdministrationRatio"
                    label={
                        <FormattedMessage
                            id="PagePaymentPreferentialSetting.Common.AdministrationRatio"
                            description="行政費率"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <InputNumber
                        placeholder=""
                        min={0}
                        formatter={value => `${value}%`}
                        parser={value => value.replace('%', '')}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Space>
                        <Button type="primary" htmlType="submit">
                            <FormattedMessage id="Share.ActionButton.Submit" />
                        </Button>
                        <Button htmlType="button" onClick={showResetConfirm}>
                            <FormattedMessage id="Share.ActionButton.Restart" />
                        </Button>
                        <Button
                            type="link"
                            htmlType="button"
                            onClick={showCancelConfirm}
                        >
                            <FormattedMessage id="Share.ActionButton.Cancel" />
                        </Button>
                    </Space>
                </Form.Item>
            </Form>
        </Wrapper>
    )
}
