import React from 'react'
import { message, Skeleton } from 'antd'
import { useHistory, useParams } from 'react-router-dom'
import useSWR from 'swr'
import axios from 'axios'
import { useIntl, FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'
import LayoutPageBody from 'layouts/Page/Body'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import Form from '../components/Form'

const { ACTIVITY_PREFERENTIAL_SETTING_VIEW } = PERMISSION

export default function Detail() {
    const history = useHistory()
    const intl = useIntl()
    const { id } = useParams()
    const { data } = useSWR(
        id ? `/api/DepositPreferentialSetting/${id}` : null,
        url => axios(url).then(res => res && res.data.Data)
    )

    if (!data) {
        return null
    }

    function onSave({ values }) {
        caller({
            endpoint: `/api/DepositPreferentialSetting/${id}`,
            method: 'PUT',
            body: values,
        })
            .then(data => {
                message.success(
                    intl.formatMessage({
                        id: 'Share.UIResponse.SaveSuccess',
                        description: '編輯成功',
                    }),
                    10
                )
                history.push('/activity/preferential-setting')
            })
            .catch(res => {
                message.warning(
                    intl.formatMessage({
                        id: 'Share.ErrorMessage.UnknownError',
                        description: '不明錯誤',
                    }),
                    10
                )
            })
    }

    return (
        <Permission isPage functionIds={[ACTIVITY_PREFERENTIAL_SETTING_VIEW]}>
            <LayoutPageBody
                title={
                    <FormattedMessage
                        id="PagePaymentPreferentialSettingDetail.PageHeader"
                        description="新增充值优惠设定"
                    />
                }
            >
                {data ? (
                    <Form defaultValues={data} onSubmit={onSave} />
                ) : (
                    <Skeleton />
                )}
            </LayoutPageBody>
        </Permission>
    )
}
