import React from 'react'
import { message } from 'antd'
import { useHistory } from 'react-router-dom'
import { useIntl, FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'
import LayoutPageBody from 'layouts/Page/Body'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import Form from '../components/Form'

const { ACTIVITY_PREFERENTIAL_SETTING_CREATE } = PERMISSION

export default function Create() {
    const intl = useIntl()
    const history = useHistory()

    function onSave({ values }) {
        caller({
            endpoint: '/api/DepositPreferentialSetting',
            method: 'POST',
            body: values,
        })
            .then(() => {
                message.success(
                    intl.formatMessage({
                        id: 'Share.UIResponse.CreateSuccess',
                        description: '新增成功',
                    }),
                    10
                )
                history.push('/activity/preferential-setting')
            })
            .catch(res => {
                message.warning(
                    intl.formatMessage({
                        id: 'Share.ErrorMessage.UnknownError',
                        description: '不明錯誤',
                    }),
                    10
                )
            })
    }

    return (
        <Permission isPage functionIds={[ACTIVITY_PREFERENTIAL_SETTING_CREATE]}>
            <LayoutPageBody
                title={
                    <FormattedMessage
                        id="PagePaymentPreferentialSettingCreate.PageHeader"
                        description="新增充值优惠设定"
                    />
                }
            >
                <Form onSubmit={onSave} />
            </LayoutPageBody>
        </Permission>
    )
}
