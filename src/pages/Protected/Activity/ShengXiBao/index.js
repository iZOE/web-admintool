import React, { createContext } from 'react'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ExportReportButton from 'components/ExportReportButton'
import ReportScaffold from 'components/ReportScaffold'
import Condition from './Condition'
import SummaryView from './Summary'
import { COLUMNS_CONFIG } from './datatableConfig'

const { ACTIVITY_SHENGXIBAO_VIEW, ACTIVITY_SHENGXIBAO_EXPORT } = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        onReady,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/MemberInterest/Search',
        defaultSortKey: 'CreateTime',
        autoFetch: true,
    })

    return (
        <PageContext.Provider
            value={{
                condition,
                dataSource,
            }}
        >
            <Permission functionIds={[ACTIVITY_SHENGXIBAO_VIEW]} isPage>
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    summaryComponent={
                        <SummaryView
                            summary={dataSource && dataSource.Summary}
                            totalCount={
                                dataSource && dataSource.Summary.TotalCount
                            }
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢結果"
                                />
                            }
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            rowKey={(record, index) =>
                                `${record.MemberName}_${index}`
                            }
                            extendArea={
                                <Permission
                                    functionIds={[ACTIVITY_SHENGXIBAO_EXPORT]}
                                >
                                    <ExportReportButton
                                        condition={condition}
                                        actionUrl="/api/MemberInterest/Export"
                                    />
                                </Permission>
                            }
                            dataSource={dataSource && dataSource.Container}
                            total={dataSource && dataSource.Summary.TotalCount}
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
        </PageContext.Provider>
    )
}

export default PageView
