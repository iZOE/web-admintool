import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary, totalCount }) => {
    return summary ? (
        <Row justify="space-between">
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageReportShengXiao.Summary.BetAmount"
                            description="总投注额"
                        />
                    }
                    value={summary.BetAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageReportShengXiao.Summary.DepositAmount"
                            description="总充值"
                        />
                    }
                    value={summary.DepositAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageReportShengXiao.Summary.InterestAmount"
                            description="收益总额"
                        />
                    }
                    value={summary.InterestAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageReportShengXiao.Summary.DispatchedInterestAmount"
                            description="已派收益"
                        />
                    }
                    value={summary.DispatchedInterestAmount}
                    precision={2}
                />
            </Col>
        </Row>
    ) : (
        <Skeleton active />
    )
}

export default SummaryView
