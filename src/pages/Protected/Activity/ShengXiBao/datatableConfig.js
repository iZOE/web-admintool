import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.MemberName" description="會員名稱" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.MemberLevelName" description="會員等級" />,
    dataIndex: 'MemberLevelName',
    key: 'MemberLevelName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.CreateTime" description="結算時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageReportShengXiao.DataTable.Title.CreateTime" description="結算時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemCreateTime',
    key: 'SystemCreateTime',
    render: (_1, { SystemCreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage id="PageReportShengXiao.DataTable.Title.LastDayBalanceAmount" description="前日餘額(結算)" />
    ),
    dataIndex: 'LastDayBalanceAmount',
    key: 'LastDayBalanceAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.DepositAmount" description="當日充值額" />,
    dataIndex: 'DepositAmount',
    key: 'DepositAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.BetAmount" description="當日投注額" />,
    dataIndex: 'BetAmount',
    key: 'BetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.BalanceAmount" description="當日餘額(結算)" />,
    dataIndex: 'BalanceAmount',
    key: 'BalanceAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.InterestRate" description="收益率" />,
    dataIndex: 'InterestRate',
    key: 'InterestRate',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => (
      <FormattedNumber style="percent" value={record.InterestRate} minimumFractionDigits={3} />
    ),
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.InterestAmount" description="實際派發收益" />,
    dataIndex: 'InterestAmount',
    key: 'InterestAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageReportShengXiao.DataTable.Title.Dispatcher" description="管理者" />,
    dataIndex: 'Dispatcher',
    key: 'Dispatcher',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
];
