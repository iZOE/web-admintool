import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, Select, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { ConditionContext } from 'contexts/ConditionContext'
import { getISODateTimeString } from 'mixins/dateTime'
import FormItemsMemberLevel from 'components/FormItems/MemberLevel'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import QueryButton from 'components/FormActionButtons/Query'

const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const STATUS_TYPE_IDS = [0, 1, 2, 3, 4]

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
    IncludeInnerMember: false,
    MemberName: null,
    Status: 0,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = Form.useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            StartDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        name="Date"
                        label={
                            <FormattedMessage id="PageReportShengXiao.QueryCondition.DateTime" />
                        }
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageReportShengXiao.QueryCondition.Status" />
                        }
                        name="Status"
                    >
                        <Select>
                            <Option value={null}>
                                {<FormattedMessage id="Share.Dropdown.All" />}
                            </Option>
                            {STATUS_TYPE_IDS.map(id => (
                                <Option value={id} key={id}>
                                    <FormattedMessage
                                        id={`PageReportShengXiao.QueryCondition.SelectItem.StatusType.${id}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <FormItemsMemberLevel checkAll />
                </Col>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageReportShengXiao.QueryCondition.MemberName" />
                        }
                        name="MemberName"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsIncludeInnerMember />
                </Col>
                <Col sm={{ span: 4, offset: 16 }} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
