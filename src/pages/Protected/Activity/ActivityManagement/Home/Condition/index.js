import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, Select, TreeSelect, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import { DEFAULT, FORMAT } from 'constants/dateConfig'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'
import FormItemsActivityName from '../../../components/FormItems/ActivityName'

const { Option } = Select
const { SHOW_CHILD } = TreeSelect
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const DATE_TYPE_IDS = [1, 2, 3, 4, 5]

const ACTIVITY_STATUS_IDS = [1, 2, 3]

const treeData = [
  {
    title: <FormattedMessage id="Share.Dropdown.All" description="全部" />,
    value: '',
    key: '',
    children: ACTIVITY_STATUS_IDS.map(id => ({
      title: (
        <FormattedMessage
          id={`PageOperationActivityManagement.QueryCondition.SelectItem.ActivityStatus.${id}`}
        />
      ),
      value: id,
      key: id,
    })),
  },
]

const CONDITION_INITIAL_VALUE = {
  DateType: 1,
  Date: RANGE.FROM_INITIAL_TO_TODAY,
  Status: [1, 2],
  Name: null,
}

function Condition({ onReady, onUpdate }) {
  const { isReady } = useContext(ConditionContext)
  const [form] = Form.useForm()

  useEffect(() => {
    onUpdate(resultCondition(form.getFieldsValue()))
    onReady(isReady)
  }, [isReady])

  function resultCondition({ Date, ...restValues }) {
    const [start, end] = Date || [null, null]

    return {
      ...restValues,
      StartTime: getISODateTimeString(start),
      EndTime: getISODateTimeString(end),
    }
  }

  function onFinish(values) {
    onUpdate.bySearch(resultCondition(values))
  }

  return (
    <Form
      form={form}
      onFinish={onFinish}
      initialValues={CONDITION_INITIAL_VALUE}
    >
      <Row gutter={[16, 8]}>
        <Col sm={10}>
          <Input.Group compact>
            <Form.Item name="DateType" noStyle>
              <Select className="option" style={{ width: '100px' }}>
                {DATE_TYPE_IDS.map(id => (
                  <Option value={id} key={id}>
                    <FormattedMessage
                      id={`PageOperationActivityManagement.QueryCondition.SelectItem.DateType.${id}`}
                    />
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="Date" noStyle>
              <RangePicker
                style={{ width: 'calc(100% - 100px)' }}
                showTime
                format={FORMAT.DISPLAY.DEFAULT}
              />
            </Form.Item>
          </Input.Group>
        </Col>
        <Col sm={7}>
          <FormItemsSimpleSelect
            needAll
            url="/api/v2/Activity/Type"
            name="Type"
            label={
              <FormattedMessage id="PageOperationActivityManagement.QueryCondition.Type" />
            }
          />
        </Col>
        <Col sm={7}>
          <Form.Item
            label={
              <FormattedMessage id="PageOperationActivityManagement.QueryCondition.Status" />
            }
            name="Status"
          >
            <TreeSelect
              treeData={treeData}
              treeCheckable
              showCheckedStrategy={SHOW_CHILD}
              style={{
                width: '100%',
              }}
              placeholder={
                <FormattedMessage id="PageOperationActivityManagement.QueryCondition.Status" />
              }
              maxTagCount={3}
              maxTagPlaceholder={
                <FormattedMessage id="Share.FormItem.TreeSelect.MaxTagPlaceholder" />
              }
            />
          </Form.Item>
        </Col>
        <Col sm={8}>
          <FormItemsActivityName name="Name" />
        </Col>
        <Col sm={{ span: 4, offset: 12 }} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  )
}

export default Condition
