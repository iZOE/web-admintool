import React, { createContext } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import * as PERMISSION from 'constants/permissions';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const { ACTIVITY_MANAGEMENT_VIEW, ACTIVITY_MANAGEMENT_CREATE } = PERMISSION;

export const PageContext = createContext();

const Home = () => {
  const { fetching, dataSource, onUpdateCondition, onReady, condition } = useGetDataSourceWithSWR({
    url: '/api/v2/Activity/Search',
    defaultSortKey: 'ActivityStartTime',
    autoFetch: true,
  });

  return (
    <Permission isPage functionIds={[ACTIVITY_MANAGEMENT_VIEW]}>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={<Condition onReady={onReady} onUpdate={onUpdateCondition} />}
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            title={<FormattedMessage id="Share.Table.SearchResult" description="查詢絝果" />}
            loading={fetching}
            config={COLUMNS_CONFIG}
            dataSource={dataSource?.List}
            total={dataSource?.TotalCount}
            rowKey={record => record.ActivityId}
            extendArea={
              <Permission
                functionIds={[ACTIVITY_MANAGEMENT_CREATE]}
                failedRender={
                  <Button type="primary" icon={<PlusOutlined />} disabled>
                    <FormattedMessage
                      id="PageOperationActivityManagement.DataTable.ExtendArea.CreateActivity"
                      description="新增活動"
                    />
                  </Button>
                }
              >
                <Link to={'/activity/activity-management/create'}>
                  <Button type="primary" icon={<PlusOutlined />}>
                    <FormattedMessage
                      id="PageOperationActivityManagement.DataTable.ExtendArea.CreateActivity"
                      description="新增活動"
                    />
                  </Button>
                </Link>
              </Permission>
            }
            onUpdate={onUpdateCondition}
          />
        }
      />
    </Permission>
  );
};

export default Home;
