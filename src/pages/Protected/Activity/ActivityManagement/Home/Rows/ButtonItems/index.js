import React, { useMemo } from 'react'
import moment from 'moment'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import usePermission from 'hooks/usePermission'
import * as PERMISSION from 'constants/permissions'
import { STATUS } from 'constants/activity'
import { Button } from 'antd'
import { FORMAT } from 'constants/dateConfig'

const { ACTIVITY_MANAGEMENT_EDIT, ACTIVITY_REPORT_VIEW } = PERMISSION

export default function ButtonItems({ record }) {
    const {
        ActivityStartTime,
        ActivityEndTime,
        ActivityId,
        ActivityStatus,
        Subject,
    } = record

    const [hasEditPermission, hasReportPermission] = usePermission(
        ACTIVITY_MANAGEMENT_EDIT,
        ACTIVITY_REPORT_VIEW
    )

    const startTime = useMemo(() => {
        if (ActivityStartTime) {
            const time = ActivityStartTime.split('+')[0]
            return moment(time).format(FORMAT.DEFAULT.FULL)
        }
    }, [ActivityStartTime])

    const endTime = useMemo(() => {
        if (ActivityEndTime) {
            const time = ActivityEndTime.split('+')[0]
            return moment(time).format(FORMAT.DEFAULT.FULL)
        }
    }, [ActivityEndTime])

    const isEditablt = useMemo(() => {
        if (ActivityEndTime) {
            const now = moment().format('X') * 1
            const activityEndTime = moment(ActivityEndTime).format('X') * 1

            return activityEndTime > now && hasEditPermission
        }
    }, [ActivityEndTime, hasEditPermission])

    const isShownReport = useMemo(() => {
        const hasAdvancedSetting = ActivityStatus === STATUS.ADVANCED
        return hasAdvancedSetting && hasReportPermission
    }, [ActivityStatus, hasReportPermission])

    return (
        <>
            <Link
                disabled={!isEditablt}
                style={{ marginLeft: 8 }}
                to={`/activity/activity-management/detail/${ActivityId}`}
            >
                <FormattedMessage id="Share.ActionButton.Edit" />
            </Link>
            {hasReportPermission && (
                <Button
                    disabled={!isShownReport}
                    href={`/activity/activity-report?id=${ActivityId}&name=${Subject}&startTime=${startTime}&endTime=${endTime}`}
                    style={{ color: isShownReport && '#faad14' }}
                    type="link"
                >
                    <FormattedMessage id="Share.ActionButton.Report" />
                </Button>
            )}
        </>
    )
}
