import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Space, Tag } from 'antd';
import DateWithFormat from 'components/DateWithFormat';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import ButtonItems from './Rows/ButtonItems';
import OpenActivityDetailDrawerButton from '../components/OpenActivityDetailDrawerButton';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.Name" />,
    dataIndex: 'Subject',
    key: 'Subject',
    fixed: 'left',
    render: (_1, { Subject, ActivityId }, _2) => <OpenActivityDetailDrawerButton name={Subject} id={ActivityId} />,
  },
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.Id" />,
    dataIndex: 'ActivityId',
    key: 'ActivityId',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.TypeName" />,
    dataIndex: 'ActivityDisplayTypeName',
    key: 'ActivityDisplayTypeName',
    render: (_1, { ActivityDisplayTypeName, ActivityDisplayTypeEnabled }, _2) => {
      return (
        <Space>
          {ActivityDisplayTypeName}
          {!ActivityDisplayTypeEnabled && (
            <Tag>
              <FormattedMessage id="Share.Status.Deactivated" />
            </Tag>
          )}
        </Space>
      );
    },
  },
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.DisplayTime" />,
    dataIndex: 'Display',
    key: 'ActivityDisplayStartDate',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ActivityDisplayStartDate: s, ActivityDisplayEndDate: e }, _2) => (
      <DateRangeWithIcon startTime={s} endTime={e} />
    ),
  },
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.ActivityTime" />,
    dataIndex: 'Activity',
    key: 'ActivityStartTime',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ActivityStartTime: s, ActivityEndTime: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.IsOnTop" />,
    dataIndex: 'IsShowOnTop',
    key: 'IsShowOnTop',
    render: (_1, { IsShowOnTop }, _2) => (
      <span>{IsShowOnTop ? <FormattedMessage id="Share.SwitchButton.IsTrue.Yes" /> : NO_DATA}</span>
    ),
  },
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.ModifyTime" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageOperationActivityManagement.DataTable.Title.ModifyUserName" />,
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.CommonKeys.Actions" />,
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => <ButtonItems record={record} />,
  },
];
