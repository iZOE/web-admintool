import React, { useMemo } from 'react'
import { Modal } from 'antd'
import { CheckCircleOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import { useHistory, Prompt } from 'react-router-dom'
import caller from 'utils/fetcher'
import LayoutPageBody from 'layouts/Page/Body'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import ActiveForm from '../components/ActiveForm'
import { TAB_INFO } from 'constants/activity'

const { ACTIVITY_TYPE_MANAGEMENT_CREATE } = PERMISSION
const { confirm } = Modal

export default function Create() {
    const history = useHistory()
    const intl = useIntl()

    const CANCEL_CONFIRM = useMemo(() => {
        return {
            CancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
            }),
            Content: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.Cancel.Content',
            }),
            OkText: intl.formatMessage({
                id: 'Share.ActionButton.Abandon',
            }),
            Title: intl.formatMessage({
                id: 'PageOperationActivityManagement.PopupConfirm.Cancel.Title',
            }),
        }
    }, [])

    const UPDATE_CONFIRM = useMemo(() => {
        return {
            CancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
            }),
            Content: intl.formatMessage(
                {
                    id:
                        'PageOperationActivityManagement.PopupConfirm.BasicSetting.SubmitSuccess.Content',
                },
                { br: <br /> }
            ),
            OKText: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.BasicSetting.SubmitSuccess.OkText',
            }),
            Title: intl.formatMessage(
                {
                    id:
                        'PageOperationActivityManagement.PopupConfirm.BasicSetting.SubmitSuccess.Title',
                },
                { br: <br /> }
            ),
        }
    }, [])

    function onSave({ values }) {
        caller({
            endpoint: '/api/v2/Activity',
            method: 'POST',
            body: values,
        }).then(res => {
            const { ActivityId } = res.Data

            confirm({
                cancelText: UPDATE_CONFIRM.CancelText,
                content: UPDATE_CONFIRM.Content,
                icon: <CheckCircleOutlined style={{ color: '#87d068' }} />,
                okText: UPDATE_CONFIRM.OKText,
                title: UPDATE_CONFIRM.Title,
                onCancel: () => {
                    history.push('/activity/activity-management')
                },
                onOk: () => {
                    history.push(
                        `/activity/activity-management/detail/${ActivityId}?setting=${TAB_INFO.ADVANCED}`
                    )
                },
            })
        })
    }

    return (
        <Permission isPage functionIds={[ACTIVITY_TYPE_MANAGEMENT_CREATE]}>
            <LayoutPageBody
                title={
                    <FormattedMessage id="PageOperationActivityManagement.Create.Title" />
                }
                subTitle={
                    <FormattedMessage id="PageOperationActivityManagement.Create.SubTitle" />
                }
            >
                <ActiveForm onSubmit={onSave} />
            </LayoutPageBody>
            <Prompt
                message={(location, action) => {
                    if (action === 'POP') {
                        confirm({
                            cancelText: CANCEL_CONFIRM.CancelText,
                            content: CANCEL_CONFIRM.Content,
                            okText: CANCEL_CONFIRM.OkText,
                            onOk: () => {
                                history.push('/activity/activity-management')
                            },
                            title: CANCEL_CONFIRM.Title,
                        })
                        return false
                    }
                    return true
                }}
            />
        </Permission>
    )
}
