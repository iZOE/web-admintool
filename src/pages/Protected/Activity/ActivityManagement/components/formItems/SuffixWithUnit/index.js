import React from 'react'
import { FormattedMessage } from 'react-intl'

export default function SuffixWithUnit({ unit = 'Currency', value = '' }) {
    return (
        <span className="ant-form-text">
            <FormattedMessage id={'Share.Unit.' + unit} values={{ value }} />
        </span>
    )
}
