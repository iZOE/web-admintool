import React from 'react'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import { Form, Input, InputNumber, Spin, Typography } from 'antd'
import SelectWithList from 'components/FormItems/SelectWithList'
import SuffixWithUnit from '../SuffixWithUnit'
import RewardSingleInputGroup from './SingleRewardInputGroup'
import { REWARD_RULE } from 'constants/activity'
import { RULES, getMaximum } from 'constants/activity/validators'

export default function RewardInputGroup({
    audienceList,
    disabled,
    rewardMode,
}) {
    const isMultipleSetting =
        rewardMode === REWARD_RULE.MODE.BY_MEMBER_LEVEL && audienceList.length

    const { data: memberLevelList } = useSWR(
        isMultipleSetting ? '/api/Option/MemberLevel' : null,
        url => axios(url).then(res => res.data.Data)
    )

    const getMemberLevelText = id => {
        const target = memberLevelList.find(({ Id }) => Id === id)
        return target.Name
    }

    return rewardMode ? (
        rewardMode === REWARD_RULE.MODE.ALL ? (
            <RewardSingleInputGroup disabled={disabled} />
        ) : !audienceList.length ? (
            <Typography.Paragraph type="warning">
                <FormattedMessage id="PageOperationActivityManagement.ExtraDescription.RewardMode" />
            </Typography.Paragraph>
        ) : (
            audienceList.map((item, key) => (
                <Input.Group compact key={key} style={{ marginTop: 8 }}>
                    <Form.Item noStyle>
                        <span
                            className="ant-form-text"
                            style={{ width: '32%' }}
                        >
                            {getMemberLevelText(item)}
                        </span>
                    </Form.Item>

                    <Form.Item
                        name={[
                            'ActivityBonus',
                            'Rules',
                            key,
                            'ActivityBonusMode',
                        ]}
                        noStyle
                        rules={[...RULES.REWARD_RULE.MODE]}
                    >
                        <SelectWithList
                            disabled={disabled}
                            endpoint="/api/Option/Activity/BonusMode"
                            placeholder={
                                <FormattedMessage id="PageOperationActivityManagement.Placeholder.BonusRuleMode" />
                            }
                            style={{ width: '32%' }}
                        />
                    </Form.Item>

                    <Form.Item noStyle shouldUpdate>
                        {({ getFieldValue }) => {
                            let currentActivityBonusModeId = getFieldValue([
                                'ActivityBonus',
                                'Rules',
                                key,
                                'ActivityBonusMode',
                            ])
                            let isAmount =
                                currentActivityBonusModeId ===
                                REWARD_RULE.TYPE.FIXED_AMOUNT.ID
                            let currentMaximum = isAmount
                                ? REWARD_RULE.TYPE.FIXED_AMOUNT.MAX
                                : REWARD_RULE.TYPE.BY_PERCENT.MAX
                            let max = getMaximum(currentMaximum)

                            return (
                                <Form.Item noStyle>
                                    <Form.Item
                                        name={[
                                            'ActivityBonus',
                                            'Rules',
                                            key,
                                            'ActivityBonus',
                                        ]}
                                        noStyle
                                        rules={[
                                            ...RULES.REWARD_RULE.AMOUNT,
                                            ...max,
                                        ]}
                                    >
                                        <InputNumber
                                            disabled={disabled}
                                            precision={2}
                                            style={{
                                                width: 'calc(36% - 60px)',
                                            }}
                                        />
                                    </Form.Item>
                                    <Form.Item noStyle>
                                        <SuffixWithUnit
                                            style={{ width: 60 }}
                                            unit={
                                                currentActivityBonusModeId &&
                                                !isAmount
                                                    ? 'Percent'
                                                    : 'Currency'
                                            }
                                        />
                                    </Form.Item>
                                </Form.Item>
                            )
                        }}
                    </Form.Item>
                </Input.Group>
            ))
        )
    ) : (
        <Spin />
    )
}
