import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Form, Input, InputNumber } from 'antd'
import SelectWithList from 'components/FormItems/SelectWithList'
import SuffixWithUnit from '../../SuffixWithUnit'
import { REWARD_RULE } from 'constants/activity'
import { RULES, getMaximum } from 'constants/activity/validators'

export default function RewardSingleInputGroup({ disabled }) {
    return (
        <Form.List name={['ActivityBonus', 'Rules']}>
            {fields => (
                <>
                    {fields.map((field, key) => (
                        <Input.Group compact key={key} style={{ marginTop: 8 }}>
                            <Form.Item
                                name={[field.name, 'ActivityBonusMode']}
                                noStyle
                                rules={[...RULES.REWARD_RULE.MODE]}
                            >
                                <SelectWithList
                                    disabled={disabled}
                                    endpoint="/api/Option/Activity/BonusMode"
                                    placeholder={
                                        <FormattedMessage id="PageOperationActivityManagement.Placeholder.BonusRuleMode" />
                                    }
                                    style={{ width: '48%' }}
                                />
                            </Form.Item>

                            <Form.Item noStyle shouldUpdate>
                                {({ getFieldValue }) => {
                                    let currentActivityBonusModeId = getFieldValue(
                                        [
                                            'ActivityBonus',
                                            'Rules',
                                            key,
                                            'ActivityBonusMode',
                                        ]
                                    )
                                    let isAmount =
                                        currentActivityBonusModeId ===
                                        REWARD_RULE.TYPE.FIXED_AMOUNT.ID
                                    let currentMaximum = isAmount
                                        ? REWARD_RULE.TYPE.FIXED_AMOUNT.MAX
                                        : REWARD_RULE.TYPE.BY_PERCENT.MAX
                                    let max = getMaximum(currentMaximum)

                                    return (
                                        <Form.Item noStyle>
                                            <Form.Item
                                                name={[
                                                    field.name,
                                                    'ActivityBonus',
                                                ]}
                                                noStyle
                                                rules={[
                                                    ...RULES.REWARD_RULE.AMOUNT,
                                                    ...max,
                                                ]}
                                            >
                                                <InputNumber
                                                    disabled={disabled}
                                                    precision={2}
                                                    style={{
                                                        width:
                                                            'calc(52% - 60px)',
                                                    }}
                                                />
                                            </Form.Item>
                                            <Form.Item noStyle>
                                                <SuffixWithUnit
                                                    style={{ width: 60 }}
                                                    unit={
                                                        currentActivityBonusModeId &&
                                                        !isAmount
                                                            ? 'Percent'
                                                            : 'Currency'
                                                    }
                                                />
                                            </Form.Item>
                                        </Form.Item>
                                    )
                                }}
                            </Form.Item>
                        </Input.Group>
                    ))}
                </>
            )}
        </Form.List>
    )
}
