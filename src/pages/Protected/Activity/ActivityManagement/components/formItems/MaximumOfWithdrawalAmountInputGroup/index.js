import React from 'react'
import { Form, Input, InputNumber } from 'antd'
import SelectWithList from 'components/FormItems/SelectWithList'
import SuffixWithUnit from '../SuffixWithUnit'
import { REWARD_RULE, WITHDRAWAL_AMOUNT_MAXIMUM } from 'constants/activity'
import { RULES } from 'constants/activity/validators'

export default function MaximumOfWithdrawalAmountInputGroup({ disabled }) {
    return (
        <Input.Group compact>
            <Form.Item noStyle shouldUpdate>
                {({ getFieldValue, setFieldsValue }) => {
                    const isBonusModeAll =
                        getFieldValue(['ActivityBonus', 'ModelId']) ===
                        REWARD_RULE.MODE.ALL
                    const isBonusAmountFixed =
                        getFieldValue([
                            'ActivityBonus',
                            'Rules',
                            0,
                            'ActivityBonusMode',
                        ]) === REWARD_RULE.TYPE.FIXED_AMOUNT.ID
                    const bonusTimesMaximum = getFieldValue([
                        'ActivityBonusReceiveTimes',
                    ])
                    const isLimitOnly =
                        isBonusModeAll &&
                        isBonusAmountFixed &&
                        bonusTimesMaximum
                    const isDisabled = disabled || isLimitOnly

                    return (
                        <>
                            <Form.Item
                                name="ActivityBonusLimitModeId"
                                noStyle
                                rules={[...RULES.REQUIRED]}
                            >
                                <SelectWithList
                                    disabled={isDisabled}
                                    endpoint="/api/Option/Activity/BonusUpperLimit"
                                    onChange={() => {
                                        getFieldValue(
                                            'ActivityBonusLimitModeId'
                                        ) ===
                                            WITHDRAWAL_AMOUNT_MAXIMUM.MODE
                                                .INFINITE &&
                                            setFieldsValue({
                                                ActivityBonusUpperLimit:
                                                    WITHDRAWAL_AMOUNT_MAXIMUM
                                                        .AMOUNT.MAX,
                                            })
                                    }}
                                    style={{ width: '48%' }}
                                />
                            </Form.Item>
                            <Form.Item noStyle>
                                <Form.Item
                                    name="ActivityBonusUpperLimit"
                                    noStyle
                                    rules={[...RULES.WITHDRAWAL_AMOUNT_MAXIMUM]}
                                >
                                    <InputNumber
                                        disabled={
                                            getFieldValue(
                                                'ActivityBonusLimitModeId'
                                            ) ===
                                                WITHDRAWAL_AMOUNT_MAXIMUM.MODE
                                                    .INFINITE || isDisabled
                                        }
                                        precision={2}
                                        style={{ width: 'calc(52% - 60px)' }}
                                    />
                                </Form.Item>
                                <SuffixWithUnit
                                    unit="Currency"
                                    style={{ width: 60 }}
                                />
                            </Form.Item>
                        </>
                    )
                }}
            </Form.Item>
        </Input.Group>
    )
}
