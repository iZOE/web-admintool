import React from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Form, Modal } from 'antd'
import { useIntl, FormattedMessage } from 'react-intl'
import { ExclamationCircleOutlined } from '@ant-design/icons'

const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
}

const { confirm } = Modal

export default function ButtonGroup({ form, isActivityStarted }) {
    const history = useHistory()

    const RESET_CONFIRM = {
        Title: useIntl().formatMessage({
            id: 'PageOperationActivityManagement.PopupConfirm.Reset.Title',
        }),
        Content: useIntl().formatMessage({
            id: 'PageOperationActivityManagement.PopupConfirm.Reset.Content',
        }),
        OkText: useIntl().formatMessage({
            id: 'Share.ActionButton.Restart',
        }),
        CancelText: useIntl().formatMessage({
            id: 'Share.ActionButton.Cancel',
        }),
    }

    const CANCEL_CONFIRM = {
        Title: useIntl().formatMessage({
            id: 'PageOperationActivityManagement.PopupConfirm.Cancel.Title',
        }),
        Content: useIntl().formatMessage({
            id: 'PageOperationActivityManagement.PopupConfirm.Cancel.Content',
        }),
        OkText: useIntl().formatMessage({
            id: 'Share.ActionButton.Abandon',
        }),
        CancelText: useIntl().formatMessage({
            id: 'Share.ActionButton.Cancel',
        }),
    }

    function showResetConfirm() {
        confirm({
            title: RESET_CONFIRM.Title,
            icon: <ExclamationCircleOutlined />,
            content: RESET_CONFIRM.Content,
            okText: RESET_CONFIRM.OkText,
            okType: 'danger',
            cancelText: RESET_CONFIRM.CancelText,
            onOk() {
                form.resetFields()
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    function showCancelConfirm() {
        confirm({
            title: CANCEL_CONFIRM.Title,
            icon: <ExclamationCircleOutlined />,
            content: CANCEL_CONFIRM.Content,
            okText: CANCEL_CONFIRM.OkText,
            okType: 'danger',
            cancelText: CANCEL_CONFIRM.CancelText,
            onOk() {
                history.push('/activity/activity-management')
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }
    return (
        <Form.Item {...tailLayout}>
            <Button
                type="primary"
                htmlType="submit"
                disabled={isActivityStarted}
            >
                <FormattedMessage
                    id="Share.ActionButton.Submit"
                    description="提交"
                />
            </Button>
            <Button htmlType="button" onClick={showResetConfirm}>
                <FormattedMessage
                    id="Share.ActionButton.Restart"
                    description="重来"
                />
            </Button>
            <Button type="link" htmlType="button" onClick={showCancelConfirm}>
                <FormattedMessage
                    id="Share.ActionButton.Cancel"
                    description="取消"
                />
            </Button>
        </Form.Item>
    )
}
