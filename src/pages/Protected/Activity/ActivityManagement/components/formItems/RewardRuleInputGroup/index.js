import React from 'react'
import { Form } from 'antd'
import SelectWithList from 'components/FormItems/SelectWithList'
import RewardInputGroup from '../RewardInputGroup'

export default function RewardRuleInputGroup({ disabled }) {
    return (
        <>
            <Form.Item name={['ActivityBonus', 'ModelId']} noStyle>
                <SelectWithList
                    disabled={disabled}
                    endpoint="/api/Option/Activity/BonusRule"
                />
            </Form.Item>
            <Form.Item noStyle shouldUpdate>
                {({ getFieldValue }) => {
                    const rewardMode = getFieldValue([
                        'ActivityBonus',
                        'ModelId',
                    ])
                    const audienceList = getFieldValue(
                        'ActivityDisplayTargetIds'
                    )
                    if (audienceList && audienceList.length > 0) {
                        audienceList.sort((a, b) => a - b)
                    }

                    return (
                        <RewardInputGroup
                            audienceList={audienceList}
                            disabled={disabled}
                            rewardMode={rewardMode}
                        />
                    )
                }}
            </Form.Item>
        </>
    )
}
