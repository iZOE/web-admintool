import React from 'react'
import { Form, Input, InputNumber } from 'antd'
import SelectWithList from 'components/FormItems/SelectWithList'
import SuffixWithUnit from '../SuffixWithUnit'
import { RULES } from 'constants/activity/validators'

export default function MaximumOfWithdrawalTimesInputGroup({ disabled }) {
    return (
        <Input.Group compact>
            <Form.Item
                name="ActivityBonusReceivePeriodId"
                rules={[...RULES.REQUIRED]}
                noStyle
            >
                <SelectWithList
                    disabled={disabled}
                    endpoint="/api/Option/Activity/ValidPeriod"
                    style={{ width: '48%' }}
                />
            </Form.Item>
            <Form.Item noStyle>
                <Form.Item
                    name="ActivityBonusReceiveTimes"
                    noStyle
                    rules={[...RULES.WITHDRAWAL_TIMES_MAXIMUM]}
                >
                    <InputNumber
                        disabled={disabled}
                        precision={0}
                        style={{ width: 'calc(52% - 60px)' }}
                    />
                </Form.Item>
                <SuffixWithUnit unit="Times" style={{ width: 60 }} />
            </Form.Item>
        </Input.Group>
    )
}
