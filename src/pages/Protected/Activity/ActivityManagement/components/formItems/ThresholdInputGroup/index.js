import React, { useMemo } from 'react'
import { Form, Input, InputNumber } from 'antd'
import SelectWithList from 'components/FormItems/SelectWithList'
import SuffixWithUnit from '../SuffixWithUnit'
import { THRESHOLD } from 'constants/activity'
import { RULES, getMinimum } from 'constants/activity/validators'

export default function ThresholdInputGroup({
    disabled,
    ruleName,
    periodName,
    amountName,
    hasTarget,
    isShowRequired,
}) {
    // 充值&流水 range 0.01~99999999.99
    // 盈虧range -99999999.99~-0.01, 0.01~99999999.99, 不得為0
    const rules = useMemo(() => {
        let isProfitLoss = amountName === THRESHOLD.TYPE.PROFIT_LOSS
        let min = getMinimum(
            isProfitLoss
                ? THRESHOLD.MINIMUM.IS_PROFIT_LOSS
                : THRESHOLD.MINIMUM.NOT_PROFIT_LOSS
        )

        const required = isShowRequired || hasTarget ? [...RULES.REQUIRED] : []
        const checkValueZero = isProfitLoss ? [...RULES.NOT_BE_ZERO] : []

        return [...RULES.THRESHOLDS, ...min, ...required, ...checkValueZero]
    }, [hasTarget, amountName, isShowRequired])

    return (
        <Input.Group compact>
            <Form.Item name={ruleName} noStyle>
                <SelectWithList
                    disabled={disabled}
                    endpoint="/api/Option/Activity/ValidRule"
                    style={{ width: '32%' }}
                />
            </Form.Item>
            <Form.Item name={periodName} noStyle>
                <SelectWithList
                    disabled={disabled}
                    endpoint="/api/Option/Activity/ValidPeriod"
                    style={{ width: '32%' }}
                />
            </Form.Item>
            <Form.Item noStyle shouldUpdate>
                <Form.Item name={amountName} noStyle rules={rules}>
                    <InputNumber
                        disabled={disabled}
                        precision={2}
                        style={{ width: 'calc(36% - 60px)' }}
                    />
                </Form.Item>
                <SuffixWithUnit unit="Currency" style={{ width: 60 }} />
            </Form.Item>
        </Input.Group>
    )
}
