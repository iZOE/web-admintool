import React, { lazy } from 'react'
import { FormattedMessage } from 'react-intl'
import { Form } from 'antd'

const TooltipLabelAsync = lazy(() => import('./TooltipLabel'))

export default function FormItemWrapper({
  labelName,
  hasDescription = false,
  children,
  ...restProps
}) {
  return (
    <Form.Item
      {...restProps}
      label={
        <>
          <FormattedMessage
            id={`PageOperationActivityManagement.FormItems.${labelName}`}
          />
          {hasDescription && <TooltipLabelAsync labelName={labelName} />}
        </>
      }
    >
      {children}
    </Form.Item>
  )
}
