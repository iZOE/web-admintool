import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Tooltip } from 'antd'
import { QuestionCircleOutlined } from '@ant-design/icons'

export default function TooltipLabel({ labelName }) {
  return (
    <Tooltip
      color="blue"
      title={
        <FormattedMessage
          id={`PageOperationActivityManagement.ExtraDescription.${labelName}`}
          values={{ br: <br /> }}
        />
      }
    >
      <QuestionCircleOutlined style={{ marginLeft: 4 }} />
    </Tooltip>
  )
}
