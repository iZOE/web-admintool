import React, { useEffect, useMemo } from 'react'
import { useParams } from 'react-router-dom'
import { useIntl, FormattedMessage } from 'react-intl'
import { useHistory } from 'react-router-dom'
import useSWR from 'swr'
import axios from 'axios'
import caller from 'utils/fetcher'
import moment from 'moment'
import { Form, InputNumber, Modal, message, Skeleton } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import FormItemWrapper from '../FromItemWrapper'
import TooltipLabel from '../FromItemWrapper/TooltipLabel'
import FormItemMemberLevel from 'components/FormItems/MemberLevel'
import SelectWithList from 'components/FormItems/SelectWithList'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import RewardRuleInputGroup from '../formItems/RewardRuleInputGroup'
import ThresholdInputGroup from '../formItems/ThresholdInputGroup'
import MaximumOfWithdrawalTimesInputGroup from '../formItems/MaximumOfWithdrawalTimesInputGroup'
import MaximumOfWithdrawalAmountInputGroup from '../formItems/MaximumOfWithdrawalAmountInputGroup'
import SuffixWithUnit from '../formItems/SuffixWithUnit'
import ButtonGroup from '../formItems/ButtonGroup'
import { getDateTimeStringWithoutTimeZone } from 'mixins/dateTime'
import {
    REWARD_RULE,
    SETTLEMENT,
    THRESHOLD,
    ALERT,
    WITHDRAWAL_AMOUNT_MAXIMUM,
} from 'constants/activity'
import { RULES } from 'constants/activity/validators'
import { FORMAT } from 'constants/dateConfig'

const { confirm } = Modal
const { useForm } = Form

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
}

export default function ActivityAdvancedSettingForm({ setIsFormTouched }) {
    const intl = useIntl()
    const history = useHistory()
    const { activityId } = useParams()
    const [form] = useForm()

    useEffect(() => {
        return () => {
            setIsFormTouched(false)
        }
    }, [])

    const { data: defaultValues } = useSWR(
        activityId ? `/api/v2/Activity/Rule/${activityId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    const isActivityStarted = useMemo(() => {
        if (defaultValues) {
            const now = moment().format('X') * 1
            const activityStartTime =
                moment(defaultValues.ActivityStartTime).format('X') * 1

            return activityStartTime < now
        }
    }, [defaultValues])

    const genActivityGameList = list => {
        if (!list) {
            return undefined
        } else {
            const data = list.map(({ GameProviderTypeId }) =>
                GameProviderTypeId.toString()
            )
            return data
        }
    }

    const initialValues = useMemo(() => {
        if (defaultValues) {
            const { ActivityGame, ActivityDisplayTargetIds } = defaultValues
            const activityGame = genActivityGameList(ActivityGame)

            return {
                ...defaultValues,
                ActivityGame: activityGame,
                ActivityDisplayTargetIds:
                    ActivityDisplayTargetIds &&
                    ActivityDisplayTargetIds.length > 0
                        ? ActivityDisplayTargetIds
                        : undefined,
            }
        }
    }, [defaultValues])

    const CONFIRM_MESSAGE = useMemo(() => {
        return {
            CancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            Content: intl.formatMessage(
                {
                    id:
                        'PageOperationActivityManagement.PopupConfirm.AdvancedSetting.Content',
                },
                {
                    startTime:
                        defaultValues &&
                        getDateTimeStringWithoutTimeZone(
                            defaultValues.ActivityStartTime
                        ).format(FORMAT.DISPLAY.DEFAULT),
                    br: <br />,
                }
            ),
            OkText: intl.formatMessage({ id: 'Share.ActionButton.Check' }),
            SuccessMsg: intl.formatMessage({
                id: 'Share.SuccessMessage.UpdateSuccess',
            }),
            Title: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.AdvancedSetting.Title',
            }),
        }
    }, [defaultValues])

    const CHECK_MESSAGE = useMemo(() => {
        return {
            CancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            Content: {
                Monthly: intl.formatMessage({
                    id:
                        'PageOperationActivityManagement.PopupConfirm.CheckSettlement.Content.Monthly',
                }),
                Weekly: intl.formatMessage({
                    id:
                        'PageOperationActivityManagement.PopupConfirm.CheckSettlement.Content.Weekly',
                }),
            },
            OkText: intl.formatMessage({ id: 'Share.ActionButton.Confirm' }),
        }
    }, [])

    const COMPLETED_MESSAGE = useMemo(() => {
        return {
            Title: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.Completed.Title',
            }),
            Content: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.Completed.Content',
            }),
            OkText: intl.formatMessage({ id: 'Share.ActionButton.Done' }),
            CancelText: intl.formatMessage({
                id: 'Share.ActionButton.Continue',
            }),
        }
    }, [])

    const showSettlementWarning = type => {
        confirm({
            content:
                type === ALERT.FOR_MONTHLY
                    ? CHECK_MESSAGE.Content.Monthly
                    : CHECK_MESSAGE.Content.Weekly,
            icon: <ExclamationCircleOutlined />,
            okText: CHECK_MESSAGE.OkText,
            title: CHECK_MESSAGE.Title,
        })
    }

    const checkSettlementSetting = values => {
        const {
            SettleOccursId,
            ValidBetAmountPeriodId,
            DepositPeriodId,
            WinLossPeriodId,
        } = values
        const { LAST_WEEK, LAST_MONTH } = THRESHOLD.PERIOD

        const isDailySettlement = SettleOccursId === SETTLEMENT.DAILY
        const isWeeklySettlement = SettleOccursId === SETTLEMENT.WEEKLY
        const settlementSetting = [
            ValidBetAmountPeriodId,
            DepositPeriodId,
            WinLossPeriodId,
        ]
            .sort()
            .toString()

        const monthlySetting = [LAST_MONTH, LAST_MONTH, LAST_MONTH].toString()
        const match1 = [LAST_WEEK, LAST_WEEK, LAST_WEEK].toString()
        const match2 = [LAST_WEEK, LAST_WEEK, LAST_MONTH].toString()
        const match3 = [LAST_WEEK, LAST_MONTH, LAST_MONTH].toString()

        if (
            settlementSetting === monthlySetting &&
            (isDailySettlement || isWeeklySettlement)
        ) {
            // Case I: [{ 4, 4, 4}]
            // 設定皆為前一個月 (status: 4) 需提醒改為「月結」
            showSettlementWarning(ALERT.FOR_MONTHLY)
        } else if (
            settlementSetting === match1 ||
            settlementSetting === match2 ||
            settlementSetting === match3
        ) {
            // Case II: [{ 3, 3, 3 }] || [{ 3, 3, 4 }] || [{ 3, 4, 4 }]
            // 相關設定符合上述排列組合需提醒改為「週結」
            // 前一週 (status: 3)
            showSettlementWarning(ALERT.FOR_WEEKLY)
        } else {
            onSubmit(values)
        }
    }

    const onFinish = values => {
        if (values) {
            confirm({
                title: CONFIRM_MESSAGE.Title,
                icon: <ExclamationCircleOutlined />,
                content: CONFIRM_MESSAGE.Content,
                okText: CONFIRM_MESSAGE.OkText,
                cancelText: CONFIRM_MESSAGE.CancelText,
                onOk: () => {
                    checkSettlementSetting(values)
                },
            })
        }
    }

    const getFormattedRules = ({ ActivityDisplayTargetIds, ActivityBonus }) => {
        const { Rules } = ActivityBonus

        const rules = Rules.map((item, key) => {
            return {
                ...item,
                MemberLevelId: ActivityDisplayTargetIds[key],
            }
        })

        return rules
    }

    const getFormattedGames = games => {
        const data = games.map(item => parseInt(item))
        const filteredData = data.filter(item => !isNaN(item))
        return filteredData
    }

    const onSubmit = values => {
        const { ActivityBonus } = values
        const isRewardModeByMemberLevel =
            values && ActivityBonus.ModelId === REWARD_RULE.MODE.BY_MEMBER_LEVEL
        const formattedRules = getFormattedRules(values)
        const formattedGames = getFormattedGames(values.ActivityGame)

        const payload = {
            ...values,
            ActivityGame: formattedGames,
            ActivityBonus: {
                ...ActivityBonus,
                Rules: isRewardModeByMemberLevel
                    ? formattedRules
                    : ActivityBonus.Rules,
            },
            ActivityId: parseInt(activityId),
        }

        caller({
            method: 'post',
            endpoint: `/api/v2/Activity/rule/Modify`,
            body: payload,
        }).then(() => {
            message.success(CONFIRM_MESSAGE.SuccessMsg, 5)
            confirm({
                title: COMPLETED_MESSAGE.Title,
                icon: <ExclamationCircleOutlined />,
                content: COMPLETED_MESSAGE.Content,
                okText: COMPLETED_MESSAGE.OkText,
                cancelText: COMPLETED_MESSAGE.CancelText,
                onOk: () => {
                    history.push('/activity/activity-management')
                },
                onCancel: () => {
                    console.log('submit values: ', values)
                },
            })
        })
        // .then(() => {
        //     history.push('/operation/activity-management')
        // })
    }

    const onValuesChange = v => {
        const isBonusModeAll =
            form.getFieldValue(['ActivityBonus', 'ModelId']) ===
            REWARD_RULE.MODE.ALL
        const isBonusAmountFixed =
            form.getFieldValue([
                'ActivityBonus',
                'Rules',
                0,
                'ActivityBonusMode',
            ]) === REWARD_RULE.TYPE.FIXED_AMOUNT.ID
        const bonus = form.getFieldValue([
            'ActivityBonus',
            'Rules',
            0,
            'ActivityBonus',
        ])
        const bonusTimesMaximum = form.getFieldValue([
            'ActivityBonusReceiveTimes',
        ])

        if (!!bonusTimesMaximum && isBonusModeAll && isBonusAmountFixed) {
            form.setFieldsValue({
                ActivityBonusUpperLimit: bonus * bonusTimesMaximum,
                ActivityBonusLimitModeId:
                    WITHDRAWAL_AMOUNT_MAXIMUM.MODE.LIMITED,
            })
        }

        if (
            !isNaN(v.ValidBetAmount) ||
            !isNaN(v.DepositAmount) ||
            !isNaN(v.WinLossAmount)
        ) {
            form.validateFields([
                'ValidBetAmount',
                'DepositAmount',
                'WinLossAmount',
            ])
        }

        if (form.isFieldsTouched()) {
            setIsFormTouched(true)
        }
    }

    return initialValues ? (
        <Form
            {...layout}
            form={form}
            initialValues={initialValues}
            name="activity-advanced-setting"
            onFinish={onFinish}
            onValuesChange={onValuesChange}
            requiredMark={false}
            scrollToFirstError
        >
            <FormItemMultipleSelect
                checkAll
                disabled={isActivityStarted}
                label={
                    <>
                        <FormattedMessage id="PageOperationActivityManagement.FormItems.AppliedGame" />
                        <TooltipLabel labelName="AppliedGame" />
                    </>
                }
                name="ActivityGame"
                placeholder={
                    <FormattedMessage id="PageOperationActivityManagement.Placeholder.AppliedGame" />
                }
                rules={[...RULES.REQUIRED]}
                url="/api/Option/V2/ThirdPartyLotteryTypes?ShowAll=true"
            />
            <FormItemMemberLevel
                checkAll
                disabled={isActivityStarted}
                label={
                    <>
                        <FormattedMessage id="PageOperationActivityManagement.FormItems.AppliedMember" />
                        <TooltipLabel labelName="AppliedMember" />
                    </>
                }
                name="ActivityDisplayTargetIds"
                rules={[...RULES.REQUIRED]}
            />
            <FormItemWrapper
                labelName="SettlementTime"
                name="SettleOccursId"
                rules={[...RULES.REQUIRED]}
                hasDescription
            >
                <SelectWithList
                    disabled={isActivityStarted}
                    endpoint="/api/Option/Activity/SettleOccurs"
                />
            </FormItemWrapper>
            <FormItemWrapper
                labelName="DispatchedType"
                name="DispatchModeId"
                rules={[...RULES.REQUIRED]}
                hasDescription
            >
                <SelectWithList
                    disabled={isActivityStarted}
                    endpoint="/api/Option/Activity/DispatchMode"
                />
            </FormItemWrapper>
            <FormItemWrapper labelName="RewardAmount" hasDescription>
                <RewardRuleInputGroup disabled={isActivityStarted} />
            </FormItemWrapper>
            <Form.Item noStyle shouldUpdate>
                {({ getFieldValue }) => {
                    const rules = getFieldValue(['ActivityBonus', 'Rules'])
                    const hasTarget = rules.some(
                        ({ ActivityBonusMode }) =>
                            ActivityBonusMode === REWARD_RULE.TYPE.BY_TURNOVER
                    )
                    const isShowRequired =
                        getFieldValue('ValidBetAmount') === null &&
                        getFieldValue('DepositAmount') === null &&
                        getFieldValue('WinLossAmount') === null

                    return (
                        <FormItemWrapper
                            labelName="TurnoverThreshold"
                            hasDescription
                        >
                            <ThresholdInputGroup
                                disabled={isActivityStarted}
                                ruleName="ValidBetAmountRuleId"
                                periodName="ValidBetAmountPeriodId"
                                amountName="ValidBetAmount"
                                hasTarget={hasTarget}
                                isShowRequired={isShowRequired}
                            />
                        </FormItemWrapper>
                    )
                }}
            </Form.Item>
            <Form.Item noStyle shouldUpdate>
                {({ getFieldValue }) => {
                    const rules = getFieldValue(['ActivityBonus', 'Rules'])
                    const hasTarget = rules.some(
                        ({ ActivityBonusMode }) =>
                            ActivityBonusMode === REWARD_RULE.TYPE.BY_DEPOSIT
                    )
                    const isShowRequired =
                        getFieldValue('ValidBetAmount') === null &&
                        getFieldValue('DepositAmount') === null &&
                        getFieldValue('WinLossAmount') === null

                    return (
                        <FormItemWrapper
                            labelName="DepositThreshold"
                            hasDescription
                        >
                            <ThresholdInputGroup
                                disabled={isActivityStarted}
                                ruleName="DepositRuleId"
                                periodName="DepositPeriodId"
                                amountName="DepositAmount"
                                hasTarget={hasTarget}
                                isShowRequired={isShowRequired}
                            />
                        </FormItemWrapper>
                    )
                }}
            </Form.Item>
            <Form.Item noStyle shouldUpdate>
                {({ getFieldValue }) => {
                    const rules = getFieldValue(['ActivityBonus', 'Rules'])
                    const hasTarget = rules.some(
                        ({ ActivityBonusMode }) =>
                            ActivityBonusMode ===
                            REWARD_RULE.TYPE.BY_PROFIT_LOSS
                    )
                    const isShowRequired =
                        getFieldValue('ValidBetAmount') === null &&
                        getFieldValue('DepositAmount') === null &&
                        getFieldValue('WinLossAmount') === null

                    return (
                        <FormItemWrapper
                            labelName="ProfitLossThreshold"
                            hasDescription
                        >
                            <ThresholdInputGroup
                                disabled={isActivityStarted}
                                ruleName="WinLossRuleId"
                                periodName="WinLossPeriodId"
                                amountName="WinLossAmount"
                                hasTarget={hasTarget}
                                isShowRequired={isShowRequired}
                            />
                        </FormItemWrapper>
                    )
                }}
            </Form.Item>
            <FormItemWrapper
                labelName="AdditionalCondition"
                name="ExtraAudit"
                hasDescription
            >
                <SelectWithList
                    disabled={isActivityStarted}
                    endpoint="/api/Option/Activity/ExtraAudit"
                    mode="multiple"
                    placeholder={
                        <FormattedMessage id="PageOperationActivityManagement.Placeholder.AdditionalCondition" />
                    }
                />
            </FormItemWrapper>
            <FormItemWrapper labelName="MaximumOfWithdrawal" hasDescription>
                <MaximumOfWithdrawalTimesInputGroup
                    disabled={isActivityStarted}
                />
            </FormItemWrapper>
            <FormItemWrapper labelName="MaximumOfReward" hasDescription>
                <MaximumOfWithdrawalAmountInputGroup
                    disabled={isActivityStarted}
                />
            </FormItemWrapper>
            <FormItemWrapper labelName="MultipleOfTurnover" hasDescription>
                <Form.Item
                    name="ValidBetMultiple"
                    noStyle
                    rules={[...RULES.TURNOVER_MULTIPLE]}
                >
                    <InputNumber
                        disabled={isActivityStarted}
                        precision={2}
                        style={{ width: 'calc(100% - 60px)' }}
                    />
                </Form.Item>
                <SuffixWithUnit unit="Multiple" style={{ width: 60 }} />
            </FormItemWrapper>
            <ButtonGroup form={form} isActivityStarted={isActivityStarted} />
        </Form>
    ) : (
        <Skeleton active />
    )
}
