import React, { Suspense, useCallback, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { PageHeader, Spin } from 'antd'
import BasicDrawerWrapper from 'layouts/BasicDrawer/index'
import { TAB_INFO } from 'constants/activity'
import TabHeader from './tab/Header'
import BasicSettingTab from './tab/Body/BasicSetting'
import AdvancedSettingTab from './tab/Body/AdvancedSetting'
import ChangeLogTab from './tab/Body/ChangeLog'

export default function ActivityDetailDrawer({
    activityId,
    isVisible,
    onClose,
}) {
    const [currentTab, setCurrentTab] = useState(TAB_INFO.BASIC)
    const CURRENT_TAB_COMPONENT = {
        Basic: activityId && <BasicSettingTab activityId={activityId} />,
        Advanced: activityId && <AdvancedSettingTab activityId={activityId} />,
        Log: activityId && <ChangeLogTab activityId={activityId} />,
    }

    const onTabChange = useCallback(tabKey => setCurrentTab(tabKey), [])

    return (
        <BasicDrawerWrapper
            title={
                <>
                    <PageHeader
                        title={
                            <FormattedMessage
                                id="PageOperationActivityManagement.Area.Detail"
                                description="活動詳情"
                            />
                        }
                    />
                    <TabHeader
                        currentTab={currentTab}
                        onTabChange={onTabChange}
                    />
                </>
            }
            onClose={onClose}
            visible={isVisible}
        >
            <Suspense fallback={<Spin />}>
                {activityId && CURRENT_TAB_COMPONENT[currentTab]}
            </Suspense>
        </BasicDrawerWrapper>
    )
}
