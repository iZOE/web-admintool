import React from 'react';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

export default function ChangeLogTab({ activityId }) {
  const { fetching, onUpdateCondition, condition, dataSource } = useGetDataSourceWithSWR({
    method: 'POST',
    url: '/api/v2/Activity/Log',
    defaultSortKey: 'ModifiedOn',
  });

  return (
    <ReportScaffold
      displayResult={condition}
      conditionComponent={<Condition onUpdate={onUpdateCondition} activityId={activityId} />}
      conditionHasCollapseWrapper={false}
      datatableComponent={
        <DataTable
          dataSource={dataSource}
          displayResult={condition}
          loading={fetching}
          config={COLUMNS_CONFIG}
          pagination={false}
        />
      }
    />
  );
}
