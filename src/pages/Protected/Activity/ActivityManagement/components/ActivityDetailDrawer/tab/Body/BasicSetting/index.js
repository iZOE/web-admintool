import React from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Image, Skeleton, Typography } from 'antd'
import DateRangeWithIcon from 'components/DateRangeWithIcon'
import BadgeWithDefaultStatusName from 'components/BadgeWithDefaultStatusName'
import FormItemWrapper from '../../../../FromItemWrapper'
import { NO_DATA } from 'constants/noData'

export default function BasicSettingTab({ activityId }) {
    const { data: d } = useSWR(
        activityId ? `/api/v2/Activity/${activityId}` : null,
        url => axios(url).then(res => res.data.Data)
    )
    return d ? (
        <>
            <FormItemWrapper labelName="Name">{d.Subject}</FormItemWrapper>
            <FormItemWrapper labelName="Summary">
                {d.Summary ? d.Summary : NO_DATA}
            </FormItemWrapper>
            <FormItemWrapper labelName="DisplayTime">
                <DateRangeWithIcon
                    startTime={d.ActivityDisplayStartDate}
                    endTime={d.ActivityDisplayEndDate}
                />
            </FormItemWrapper>
            <FormItemWrapper labelName="ActivityTime">
                <DateRangeWithIcon
                    startTime={d.ActivityStartTime}
                    endTime={d.ActivityEndTime}
                />
            </FormItemWrapper>
            <FormItemWrapper labelName="Type">
                {d.ActivityDisplayTypeName
                    ? d.ActivityDisplayTypeName
                    : NO_DATA}
            </FormItemWrapper>
            <FormItemWrapper labelName="IsOnTop">
                <BadgeWithDefaultStatusName
                    status={d.IsShowOnTop}
                    label="IsTrue"
                />
            </FormItemWrapper>
            <FormItemWrapper labelName="WebUrl">
                {d.WebUrl ? (
                    <Typography.Link
                        href={'http://' + d.WebUrl}
                        target="_blank"
                        copyable
                    >
                        {d.WebUrl}
                    </Typography.Link>
                ) : (
                    NO_DATA
                )}
            </FormItemWrapper>
            <FormItemWrapper labelName="WebImageUrl">
                <Image width={160} src={d.WebPicFileName} />
            </FormItemWrapper>
            <FormItemWrapper labelName="AppUrl">
                {d.AppUrl ? (
                    <Typography.Link
                        href={'http://' + d.AppUrl}
                        target="_blank"
                        copyable
                    >
                        {d.AppUrl}
                    </Typography.Link>
                ) : (
                    NO_DATA
                )}
            </FormItemWrapper>
            <FormItemWrapper labelName="AppImageUrl">
                <Image width={160} src={d.AppPicFileName} />
            </FormItemWrapper>
        </>
    ) : (
        <Skeleton active />
    )
}
