import React from 'react'
import { FormattedMessage } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import DateWithFormat from 'components/DateWithFormat'
import { NO_DATA } from 'constants/noData'

const COLUMN_TYPE = {
    TEXT: 1, // 原有內容
    AMOUNT: 2, // 金額
    DATE: 3, // 日期
}

const getFormattedContent = (type, value) => {
    if (value && value.length > 0) {
        switch (type) {
            case COLUMN_TYPE.TEXT:
                return value
            case COLUMN_TYPE.AMOUNT:
                return <ReactIntlCurrencyWithFixedDecimal value={value} />
            case COLUMN_TYPE.DATE:
                return <DateWithFormat time={value} />
            default:
                return 'Column Type Not Found'
        }
    } else {
        return NO_DATA
    }
}

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'ModifiedOn',
        key: 'ModifiedOn',
        render: (_1, { ModifiedOn: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.Modifier"
                description="異動人員"
            />
        ),
        dataIndex: 'ModifiedByName',
        key: 'ModifiedByName',
        render: name => (name ? name : NO_DATA),
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ChangeItem"
                description="異動項目"
            />
        ),
        dataIndex: 'Column',
        key: 'Column',
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.BeforeChange"
                description="異動前"
            />
        ),
        dataIndex: 'OldValue',
        key: 'OldValue',
        render: (_1, { ColumnType: type, OldValue: value }, _2) => {
            return getFormattedContent(type, value)
        },
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.AfterChange"
                description="異動後"
            />
        ),
        dataIndex: 'NewValue',
        key: 'NewValue',
        render: (_1, { ColumnType: type, NewValue: value }, _2) => {
            return getFormattedContent(type, value)
        },
    },
]
