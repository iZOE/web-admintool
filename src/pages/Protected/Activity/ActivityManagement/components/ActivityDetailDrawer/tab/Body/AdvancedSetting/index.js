import React from 'react'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import { List, Skeleton, Space } from 'antd'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import FormItemWrapper from '../../../../FromItemWrapper'
import { NO_DATA } from 'constants/noData'
import { REWARD_RULE } from 'constants/activity'

export default function AdvancedSettingTab({ activityId }) {
    const { data: d } = useSWR(
        activityId ? `/api/v2/Activity/Rule/${activityId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    return d ? (
        <>
            <FormItemWrapper labelName="AppliedGame" hasDescription>
                {d.ActivityGameText ? d.ActivityGameText : NO_DATA}
            </FormItemWrapper>
            <FormItemWrapper labelName="AppliedMember" hasDescription>
                {d.ActivityDisplayTargetText
                    ? d.ActivityDisplayTargetText
                    : NO_DATA}
            </FormItemWrapper>
            <FormItemWrapper labelName="SettlementTime" hasDescription>
                {d.SettleOccursText ? d.SettleOccursText : NO_DATA}
            </FormItemWrapper>
            <FormItemWrapper labelName="DispatchedType" hasDescription>
                {d.DispatchModeText ? d.DispatchModeText : NO_DATA}
            </FormItemWrapper>
            <FormItemWrapper labelName="RewardAmount" hasDescription>
                <Space>
                    {d.ActivityBonus.ModeText}
                    {d.ActivityBonus.ModelId === REWARD_RULE.MODE.ALL &&
                        d.ActivityBonus.Rules.map(
                            ({
                                ActivityBonusModeText,
                                ActivityBonus,
                                ActivityBonusMode,
                            }) => (
                                <Space>
                                    {ActivityBonusModeText
                                        ? ActivityBonusModeText
                                        : NO_DATA}
                                    {ActivityBonus ? (
                                        <FormattedMessage
                                            id={`Share.Unit.${
                                                ActivityBonusMode ===
                                                REWARD_RULE.TYPE.FIXED_AMOUNT.ID
                                                    ? 'Currency'
                                                    : 'Percent'
                                            }`}
                                            values={{
                                                value: (
                                                    <ReactIntlCurrencyWithFixedDecimal
                                                        value={ActivityBonus}
                                                    />
                                                ),
                                            }}
                                        />
                                    ) : (
                                        NO_DATA
                                    )}
                                </Space>
                            )
                        )}
                    {d.ActivityBonus.ModelId ===
                        REWARD_RULE.MODE.BY_MEMBER_LEVEL && (
                        <List
                            size="small"
                            dataSource={d.ActivityBonus.Rules}
                            renderItem={({
                                MemberLevelText,
                                ActivityBonusModeText,
                                ActivityBonus,
                            }) => (
                                <List.Item>
                                    <Space>
                                        {MemberLevelText
                                            ? MemberLevelText
                                            : NO_DATA}
                                        {ActivityBonusModeText
                                            ? ActivityBonusModeText
                                            : NO_DATA}
                                        {ActivityBonus ? (
                                            <FormattedMessage
                                                id="Share.Unit.Currency"
                                                values={{
                                                    value: (
                                                        <ReactIntlCurrencyWithFixedDecimal
                                                            value={
                                                                ActivityBonus
                                                            }
                                                        />
                                                    ),
                                                }}
                                            />
                                        ) : (
                                            NO_DATA
                                        )}
                                    </Space>
                                </List.Item>
                            )}
                        />
                    )}
                </Space>
            </FormItemWrapper>
            <FormItemWrapper labelName="TurnoverThreshold" hasDescription>
                <Space>
                    {d.ValidBetAmountRuleText
                        ? d.ValidBetAmountRuleText
                        : NO_DATA}
                    {d.ValidBetAmountPeriodText
                        ? d.ValidBetAmountPeriodText
                        : NO_DATA}
                    {d.ValidBetAmount ? (
                        <FormattedMessage
                            id="Share.Unit.Currency"
                            values={{
                                value: (
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={d.ValidBetAmount}
                                    />
                                ),
                            }}
                        />
                    ) : (
                        NO_DATA
                    )}
                </Space>
            </FormItemWrapper>
            <FormItemWrapper labelName="DepositThreshold" hasDescription>
                <Space>
                    {d.DepositRuleText ? d.DepositRuleText : NO_DATA}
                    {d.DepositPeriodText ? d.DepositPeriodText : NO_DATA}
                    {d.DepositAmount ? (
                        <FormattedMessage
                            id="Share.Unit.Currency"
                            values={{
                                value: (
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={d.DepositAmount}
                                    />
                                ),
                            }}
                        />
                    ) : (
                        NO_DATA
                    )}
                </Space>
            </FormItemWrapper>
            <FormItemWrapper labelName="ProfitLossThreshold" hasDescription>
                <Space>
                    {d.WinLossRuleText ? d.WinLossRuleText : NO_DATA}
                    {d.WinLossPeriodText ? d.WinLossPeriodText : NO_DATA}
                    {d.WinLossAmount ? (
                        <FormattedMessage
                            id="Share.Unit.Currency"
                            values={{
                                value: (
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={d.WinLossAmount}
                                    />
                                ),
                            }}
                        />
                    ) : (
                        NO_DATA
                    )}
                </Space>
            </FormItemWrapper>
            <FormItemWrapper labelName="AdditionalCondition" hasDescription>
                {d.ExtraAuditText ? d.ExtraAuditText : NO_DATA}
            </FormItemWrapper>
            <FormItemWrapper labelName="MaximumOfWithdrawal" hasDescription>
                <Space>
                    {d.ActivityBonusReceivePeriodText
                        ? d.ActivityBonusReceivePeriodText
                        : NO_DATA}
                    {d.ActivityBonusReceiveTimes ? (
                        <FormattedMessage
                            id="Share.Unit.Times"
                            values={{ value: d.ActivityBonusReceiveTimes }}
                        />
                    ) : (
                        NO_DATA
                    )}
                </Space>
            </FormItemWrapper>
            <FormItemWrapper labelName="MaximumOfReward" hasDescription>
                <Space>
                    {d.ActivityBonusLimitModeText
                        ? d.ActivityBonusLimitModeText
                        : NO_DATA}
                    {d.ActivityBonusUpperLimit ? (
                        <FormattedMessage
                            id="Share.Unit.Currency"
                            values={{
                                value: (
                                    <ReactIntlCurrencyWithFixedDecimal
                                        value={d.ActivityBonusUpperLimit}
                                    />
                                ),
                            }}
                        />
                    ) : (
                        NO_DATA
                    )}
                </Space>
            </FormItemWrapper>
            <FormItemWrapper labelName="MultipleOfTurnover" hasDescription>
                {d.ValidBetMultiple ? (
                    <FormattedMessage
                        id="Share.Unit.Multiple"
                        values={{
                            value: (
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={d.ValidBetMultiple}
                                />
                            ),
                        }}
                    />
                ) : (
                    NO_DATA
                )}
            </FormItemWrapper>
        </>
    ) : (
        <Skeleton active />
    )
}
