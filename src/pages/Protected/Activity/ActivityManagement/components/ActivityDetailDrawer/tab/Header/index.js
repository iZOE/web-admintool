import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Tabs } from 'antd'
import { TAB_INFO } from 'constants/activity'

const { TabPane } = Tabs
const tabPanes = [
    {
        key: TAB_INFO.BASIC,
        title: (
            <FormattedMessage
                id={'PageOperationActivityManagement.Tabs.' + TAB_INFO.BASIC}
            />
        ),
    },
    {
        key: TAB_INFO.ADVANCED,
        title: (
            <FormattedMessage
                id={'PageOperationActivityManagement.Tabs.' + TAB_INFO.ADVANCED}
            />
        ),
    },
    {
        key: TAB_INFO.LOG,
        title: (
            <FormattedMessage
                id={'PageOperationActivityManagement.Tabs.' + TAB_INFO.LOG}
            />
        ),
    },
]

export default function TabHeader({ currentTab, onTabChange }) {
    return (
        <Tabs defaultActiveKey={currentTab} onChange={key => onTabChange(key)}>
            {tabPanes.map(({ title, key }) => (
                <TabPane tab={title} key={key} />
            ))}
        </Tabs>
    )
}
