import React, { lazy, useCallback, useState } from 'react'
import { Typography } from 'antd'

const ActivityDetailDrawerAsync = lazy(() => import('../ActivityDetailDrawer'))

export default function OpenActivityDetailDrawerButton({ name, id }) {
    const [isDrawerShown, setIsDrawerShown] = useState(false)

    const handleToggleDrawer = useCallback(
        () => setIsDrawerShown(prevIsShown => !prevIsShown),
        []
    )

    return (
        <>
            <Typography.Link
                onClick={handleToggleDrawer}
                style={{ wordBreak: 'break-word' }}
            >
                {name}
            </Typography.Link>
            {isDrawerShown && (
                <ActivityDetailDrawerAsync
                    activityId={id}
                    isVisible={isDrawerShown}
                    onClose={handleToggleDrawer}
                />
            )}
        </>
    )
}
