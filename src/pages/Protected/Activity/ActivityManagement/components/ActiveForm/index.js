import React, { useMemo, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'react-intl';
import { DatePicker, Switch, Form, Input, Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import moment from 'moment';
import { Link } from 'react-router-dom';
import useSWR from 'swr';
import axios from 'axios';
import Uploader from 'components/Uploader';
import { DEFAULT, FORMAT } from 'constants/dateConfig';
import { RULES } from 'constants/activity/validators';
import FormItemWrapper from '../FromItemWrapper';
import MemberMultipleSelect from 'components/FormItems/MultipleSelect';
import ListSelect from 'components/ListSelect';
import ButtonGroup from '../formItems/ButtonGroup';
import {
  getISODateTimeString,
  getDateTimeStringWithoutTimeZone
} from 'mixins/dateTime';

const { TextArea } = Input;
const { RangePicker } = DatePicker;
const { confirm } = Modal;
const { useForm } = Form;

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 10
  }
};

export default function ActiveForm({
  defaultValues,
  onSubmit,
  setIsFormTouched
}) {
  const intl = useIntl();
  const [form] = useForm();
  const [currentActivityStartTime, setCurrentActivityStartTime] = useState(
    null
  );

  useEffect(() => {
    if (setIsFormTouched) {
      return () => {
        setIsFormTouched(false);
      };
    }
  }, []);

  const initialValues = useMemo(() => {
    if (defaultValues) {
      defaultValues.DisplayTime = [
        getDateTimeStringWithoutTimeZone(
          defaultValues.ActivityDisplayStartDate
        ),
        getDateTimeStringWithoutTimeZone(defaultValues.ActivityDisplayEndDate)
      ];
      defaultValues.ActivityTime = [
        getDateTimeStringWithoutTimeZone(defaultValues.ActivityStartTime),
        getDateTimeStringWithoutTimeZone(defaultValues.ActivityEndTime)
      ];

      return defaultValues;
    }

    return {
      DisplayTime: DEFAULT.RANGE.FROM_TOMORROW_TO_A_MONTH,
      ActivityTime: DEFAULT.RANGE.FROM_TOMORROW_TO_A_MONTH
    };
  }, [defaultValues]);

  const FINISH_CONFIRM = useMemo(() => {
    const startTime = currentActivityStartTime
      ? currentActivityStartTime
      : defaultValues?.ActivityStartTime
      ? getDateTimeStringWithoutTimeZone(
          defaultValues?.ActivityStartTime
        ).format(FORMAT.DISPLAY.DEFAULT)
      : initialValues.ActivityTime[0].format(FORMAT.DISPLAY.DEFAULT);

    return {
      Title: intl.formatMessage({
        id:
          'PageOperationActivityManagement.PopupConfirm.BasicSetting.Confirm.Title'
      }),
      Content: intl.formatMessage(
        {
          id:
            'PageOperationActivityManagement.PopupConfirm.AdvancedSetting.Content'
        },
        {
          br: <br />,
          startTime
        }
      ),
      OkText: intl.formatMessage({
        id: 'Share.ActionButton.Submit'
      }),
      CancelText: intl.formatMessage({
        id: 'Share.ActionButton.Cancel'
      })
    };
  }, [currentActivityStartTime, defaultValues, intl]);

  const PLACEHOLDER = useMemo(() => {
    return {
      Name: intl.formatMessage({
        id: 'PageOperationActivityManagement.FormItems.Placeholder.PlsEnterName'
      }),
      Summary: intl.formatMessage({
        id:
          'PageOperationActivityManagement.FormItems.Placeholder.PlsEnterSummary'
      }),
      WebUrl: intl.formatMessage({
        id:
          'PageOperationActivityManagement.FormItems.Placeholder.PlsEnterWebUrl'
      })
    };
  }, []);

  const isActivityStarted = useMemo(() => {
    if (defaultValues) {
      const now = moment().format('X') * 1;
      const activityStartTime =
        moment(defaultValues.ActivityStartTime).format('X') * 1;

      return activityStartTime < now;
    }
  }, [defaultValues]);

  function disabledDate(current) {
    return current < moment();
  }

  const { data: activityType } = useSWR('/api/v2/Activity/Type', url =>
    axios(url).then(res => {
      const activedList = res.data.Data.filter(item => !!item.IsEnabled);
      const sortedList = activedList.sort((a, b) => a.SortOrder - b.SortOrder);
      return sortedList;
    })
  );

  const onFinish = values => {
    confirm({
      title: FINISH_CONFIRM.Title,
      icon: <ExclamationCircleOutlined />,
      content: FINISH_CONFIRM.Content,
      okText: FINISH_CONFIRM.OkText,
      cancelText: FINISH_CONFIRM.CancelText,
      onOk() {
        values.ActivityDisplayStartDate = getISODateTimeString(
          values.DisplayTime[0]
        );
        values.ActivityDisplayEndDate = getISODateTimeString(
          values.DisplayTime[1]
        );
        values.ActivityStartTime = getISODateTimeString(values.ActivityTime[0]);
        values.ActivityEndTime = getISODateTimeString(values.ActivityTime[1]);
        values.TypeId = values.TypeId || activityType[0].Id;
        values.IsOnTop = values.IsOnTop || false;

        delete values.DisplayTime;
        delete values.ActivityTime;
        onSubmit({ values });
      },
      onCancel() {
        console.log('Cancel');
      }
    });
  };

  const onValuesChange = v => {
    if (form.isFieldsTouched() && setIsFormTouched) {
      setIsFormTouched(true);
    }

    if (v.ActivityTime) {
      setCurrentActivityStartTime(
        v.ActivityTime[0].format(FORMAT.DEFAULT.FULL)
      );
    }
  };

  return (
    <Form
      {...layout}
      form={form}
      name="control-hooks"
      onFinish={onFinish}
      onValuesChange={onValuesChange}
      requiredMark={false}
      initialValues={initialValues}
      scrollToFirstError
    >
      <FormItemWrapper
        labelName="Name"
        name="Subject"
        rules={[...RULES.SUBJECT]}
      >
        <Input placeholder={PLACEHOLDER.Name} />
      </FormItemWrapper>
      <FormItemWrapper
        labelName="Summary"
        name="Summary"
        rules={[...RULES.SUMMARY]}
      >
        <TextArea
          disabled={isActivityStarted}
          placeholder={PLACEHOLDER.Summary}
        />
      </FormItemWrapper>
      <MemberMultipleSelect
        checkAll
        disabled={isActivityStarted}
        label={
          <FormattedMessage id="PageOperationActivityManagement.FormItems.Audience" />
        }
        name="ActivityDisplayTargetIds"
        url="/api/Option/MemberLevel"
        rules={[...RULES.REQUIRED]}
      />
      <FormItemWrapper
        labelName="DisplayTime"
        name="DisplayTime"
        rules={[...RULES.REQUIRED]}
      >
        <RangePicker
          showTime
          disabled={isActivityStarted ? [true, false] : [false, false]}
          disabledDate={defaultValues && disabledDate}
          format={FORMAT.DISPLAY.DEFAULT}
          style={{ width: '100%' }}
        />
      </FormItemWrapper>
      <FormItemWrapper
        labelName="ActivityTime"
        name="ActivityTime"
        rules={[...RULES.REQUIRED]}
      >
        <RangePicker
          showTime
          disabled={isActivityStarted ? [true, false] : [false, false]}
          disabledDate={defaultValues && disabledDate}
          format={FORMAT.ISO}
          style={{ width: '100%' }}
        />
      </FormItemWrapper>
      <FormItemWrapper
        labelName="Type"
        name="ActivityDisplayTypeId"
        extra={
          <Link to="/activity/activity-type-management">
            <FormattedMessage
              id="PageOperationActivityManagement.ActionButton.ManageActivityType"
              description="管理活动类型"
            />
          </Link>
        }
        rules={[...RULES.REQUIRED]}
      >
        <ListSelect list={activityType} disabled={isActivityStarted} />
      </FormItemWrapper>
      <FormItemWrapper
        labelName="IsOnTop"
        name="IsShowOnTop"
        valuePropName="checked"
      >
        <Switch
          checkedChildren={
            <FormattedMessage
              id="PageOperationActivityManagement.FormItems.IsOnTopSwitch.Checked"
              description="置顶"
            />
          }
          disabled={isActivityStarted}
          unCheckedChildren={
            <FormattedMessage
              id="PageOperationActivityManagement.FormItems.IsOnTopSwitch.UnChecked"
              description="一般"
            />
          }
        />
      </FormItemWrapper>
      <FormItemWrapper labelName="WebUrl" name="WebUrl">
        <Input
          disabled={isActivityStarted}
          placeholder={PLACEHOLDER.WebUrl}
          maxLength="200"
        />
      </FormItemWrapper>

      <Form.Item shouldUpdate noStyle>
        {({ getFieldValue, setFieldsValue }) => (
          <FormItemWrapper
            labelName="WebImageUrl"
            name="WebPicFileName"
            rules={[...RULES.REQUIRED]}
          >
            <Uploader
              action="/api/v2/Image/upload/1"
              disabled={isActivityStarted}
              text={
                <FormattedMessage
                  id="Share.PlaceHolder.Uploader.Image"
                  values={{ width: 345, height: 120 }}
                />
              }
              onSuccess={response => {
                setFieldsValue({
                  WebPicFileName: response.Data
                });
              }}
              defaultUrl={getFieldValue('WebPicFileName')}
            />
          </FormItemWrapper>
        )}
      </Form.Item>

      <FormItemWrapper labelName="AppUrl" name="AppUrl">
        <Input
          disabled={isActivityStarted}
          placeholder={PLACEHOLDER.WebUrl}
          maxLength="200"
        />
      </FormItemWrapper>

      <Form.Item shouldUpdate noStyle>
        {({ getFieldValue, setFieldsValue }) => (
          <FormItemWrapper
            labelName="AppImageUrl"
            name="AppPicFileName"
            rules={[...RULES.REQUIRED]}
          >
            <Uploader
              action="/api/v2/Image/upload/1"
              disabled={isActivityStarted}
              text={
                <FormattedMessage
                  id="Share.PlaceHolder.Uploader.Image"
                  values={{ width: 345, height: 120 }}
                />
              }
              onSuccess={response => {
                setFieldsValue({
                  AppPicFileName: response.Data
                });
              }}
              defaultUrl={getFieldValue('AppPicFileName')}
            />
          </FormItemWrapper>
        )}
      </Form.Item>
      <ButtonGroup form={form} />
    </Form>
  );
}
