import React, { useMemo } from 'react'
import { useParams, Prompt, useHistory } from 'react-router-dom'
import { FormattedMessage, useIntl } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import LayoutPageBody from 'layouts/Page/Body'
import Form from './Form'
import { Modal } from 'antd'

const { confirm } = Modal

export default function Detail() {
    const intl = useIntl()
    const history = useHistory()
    const { activityId } = useParams()

    const CANCEL_CONFIRM = useMemo(() => {
        return {
            CancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
            }),
            Content: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.Cancel.Content',
            }),
            OkText: intl.formatMessage({
                id: 'Share.ActionButton.Abandon',
            }),
            Title: intl.formatMessage({
                id: 'PageOperationActivityManagement.PopupConfirm.Cancel.Title',
            }),
        }
    }, [])

    const { data: d } = useSWR(
        activityId ? `/api/v2/Activity/${activityId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    return (
        <>
            <LayoutPageBody
                title={
                    <FormattedMessage id="PageOperationActivityManagement.Detail.Title" />
                }
                subTitle={
                    d && (
                        <FormattedMessage
                            id="PageOperationActivityManagement.Detail.Subtitle"
                            values={{
                                subject: d.Subject,
                                activityId: activityId,
                            }}
                        />
                    )
                }
            >
                <Form defaultValues={d} />
            </LayoutPageBody>

            <Prompt
                message={(location, action) => {
                    if (action === 'POP') {
                        confirm({
                            cancelText: CANCEL_CONFIRM.CancelText,
                            content: CANCEL_CONFIRM.Content,
                            okText: CANCEL_CONFIRM.OkText,
                            onOk: () => {
                                history.push('/activity/activity-management')
                            },
                            title: CANCEL_CONFIRM.Title,
                        })
                        return false
                    }
                    return true
                }}
            />
        </>
    )
}
