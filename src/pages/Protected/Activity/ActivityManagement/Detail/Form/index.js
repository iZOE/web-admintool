import React, { useState, useCallback, Suspense, useMemo } from 'react'
import { useIntl } from 'react-intl'
import { useParams, useLocation, useHistory } from 'react-router-dom'
import queryString from 'query-string'
import { message, Card, Spin, Modal } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import caller from 'utils/fetcher'
import ActiveForm from '../../components/ActiveForm'
import ActivityAdvancedSettingForm from '../../components/ActivityAdvancedSettingForm'
import { TAB_INFO } from 'constants/activity'
import TabHeader from './TabHeader'
import { trigger } from 'swr'

const { confirm } = Modal

export default function Home({ defaultValues }) {
    const { activityId } = useParams()
    const intl = useIntl()
    const history = useHistory()
    const location = useLocation()
    const { setting } = queryString.parse(location.search)

    const [currentTab, setCurrentTab] = useState(
        setting === TAB_INFO.ADVANCED ? TAB_INFO.ADVANCED : TAB_INFO.BASIC
    )
    const onTabChange = useCallback(tabKey => setCurrentTab(tabKey), [])

    const [isFormTouched, setIsFormTouched] = useState(false)

    const UPDATE_CONFIRM = useMemo(() => {
        return {
            CancelText: intl.formatMessage({
                id: 'Share.SwitchButton.IsTrue.No',
            }),
            Content: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.Update.Content',
            }),
            OKText: intl.formatMessage({
                id: 'Share.SwitchButton.IsTrue.Yes',
            }),
            SuccessMsg: intl.formatMessage({
                id: 'Share.SuccessMessage.UpdateSuccess',
            }),
            Title: intl.formatMessage({
                id: 'PageOperationActivityManagement.PopupConfirm.Update.Title',
            }),
        }
    }, [])

    const COMPLETED_MESSAGE = useMemo(() => {
        return {
            Title: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.Completed.Title',
            }),
            Content: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.Completed.Content',
            }),
            OkText: intl.formatMessage({ id: 'Share.ActionButton.Done' }),
            CancelText: intl.formatMessage({
                id: 'Share.ActionButton.Continue',
            }),
        }
    }, [])

    function onSave({ values }) {
        caller({
            endpoint: `/api/v2/Activity`,
            method: 'PUT',
            body: { ...values, ActivityId: parseInt(activityId) },
        })
            .then(() => {
                message.success(UPDATE_CONFIRM.SuccessMsg, 10)
            })
            .then(() => {
                confirm({
                    title: COMPLETED_MESSAGE.Title,
                    cancelText: COMPLETED_MESSAGE.CancelText,
                    content: COMPLETED_MESSAGE.Content,
                    okText: COMPLETED_MESSAGE.OkText,
                    onOk: () => {
                        history.push('/activity/activity-management')
                    },
                    onCancel: () => {
                        console.log('submit values: ', values)
                    },
                })
            })
            .then(() => {
                trigger(`/api/v2/Activity/${activityId}`)
            })
    }

    const CURRENT_TAB_COMPONENT = {
        Basic: defaultValues && (
            <ActiveForm
                defaultValues={defaultValues}
                onSubmit={onSave}
                setIsFormTouched={setIsFormTouched}
            />
        ),
        Advanced: (
            <ActivityAdvancedSettingForm setIsFormTouched={setIsFormTouched} />
        ),
    }

    return (
        <Card bordered={false} style={{ margin: 24 }}>
            <TabHeader
                currentTab={currentTab}
                onTabChange={onTabChange}
                startTime={defaultValues && defaultValues.ActivityStartTime}
                isFormTouched={isFormTouched}
            />
            <Suspense fallback={<Spin />}>
                {defaultValues && CURRENT_TAB_COMPONENT[currentTab]}
            </Suspense>
        </Card>
    )
}
