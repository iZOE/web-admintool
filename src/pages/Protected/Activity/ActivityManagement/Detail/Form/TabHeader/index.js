import React, { useMemo } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { Modal, Tabs } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import moment from 'moment'
import { TAB_INFO } from 'constants/activity'

const { TabPane } = Tabs
const { confirm } = Modal

const TAB_PANES = [
    {
        key: TAB_INFO.BASIC,
        title: (
            <FormattedMessage
                id={'PageOperationActivityManagement.Tabs.' + TAB_INFO.BASIC}
            />
        ),
    },
    {
        key: TAB_INFO.ADVANCED,
        title: (
            <FormattedMessage
                id={'PageOperationActivityManagement.Tabs.' + TAB_INFO.ADVANCED}
            />
        ),
    },
]

export default function TabHeader({
    currentTab,
    onTabChange,
    startTime,
    isFormTouched,
}) {
    const intl = useIntl()

    const CONFIRM_MESSAGE = useMemo(() => {
        return {
            CancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            Content: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.TabChange.Content',
            }),
            OkText: intl.formatMessage({ id: 'Share.ActionButton.Abandon' }),
            Title: intl.formatMessage({
                id:
                    'PageOperationActivityManagement.PopupConfirm.TabChange.Title',
            }),
        }
    }, [])

    const isActivityStarted = useMemo(() => {
        const now = moment().format('X')
        return startTime && startTime < now
    }, [startTime])

    const onChange = key => {
        if (isFormTouched) {
            confirm({
                cancelText: CONFIRM_MESSAGE.CancelText,
                content: CONFIRM_MESSAGE.Content,
                icon: <ExclamationCircleOutlined />,
                okText: CONFIRM_MESSAGE.OkText,
                onOk: () => {
                    onTabChange(key)
                },
                title: CONFIRM_MESSAGE.Title,
            })
        } else {
            onTabChange(key)
        }
    }

    return (
        <Tabs
            activeKey={currentTab}
            defaultActiveKey={currentTab}
            onChange={onChange}
        >
            {TAB_PANES.map(({ title, key }) => (
                <TabPane
                    tab={title}
                    key={key}
                    disabled={key === TAB_INFO.ADVANCED && isActivityStarted}
                />
            ))}
        </Tabs>
    )
}
