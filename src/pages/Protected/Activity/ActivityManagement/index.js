import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Create from './Create'
import Detail from './Detail'

export default function Activity() {
    return (
        <Switch>
            <Route
                exact
                path="/activity/activity-management"
                breadCrumbName="活动列表"
            >
                <Home />
            </Route>
            <Route path="/activity/activity-management/create">
                <Create />
            </Route>
            <Route path="/activity/activity-management/detail/:activityId">
                <Detail />
            </Route>
        </Switch>
    )
}
