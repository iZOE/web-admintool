import React from 'react';
import { FormattedMessage } from 'react-intl';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import ActionButton from './components/ActionButton';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageOperationActivityReport.DataTable.ActivityId" description="活动ID" />,
    dataIndex: 'ActivityId',
    key: 'ActivityId',
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationActivityReport.DataTable.Subject" description="活动名称" />,
    dataIndex: 'Subject',
    key: 'Subject',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationActivityReport.DataTable.Date" description="结算区间" />,
    dataIndex: 'CalcuBeginDate',
    key: 'CalcuBeginDate',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CalcuBeginDate: s, CalcuEndDate: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationActivityReport.DataTable.ReceivedActivityBonus" description="活动奖励金额" />
    ),
    dataIndex: 'ReceivedActivityBonus',
    key: 'ReceivedActivityBonus',
    sorter: (a, b) => {
      return;
    },
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationActivityReport.DataTable.DispatchStatus.NotReceived" description="未派发" />
    ),
    dataIndex: 'NotReceived',
    key: 'NotReceived',
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationActivityReport.DataTable.DispatchStatus.Received" description="已派发" />,
    dataIndex: 'Received',
    key: 'Received',
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationActivityReport.DataTable.DispatchStatus.Reject" description="已拒绝" />,
    dataIndex: 'Reject',
    key: 'Reject',
    sorter: (a, b) => {
      return;
    },
  },
  {
    dataIndex: 'action',
    key: 'action',
    fixed: 'right',
    render: (_1, record) => <ActionButton record={record} />,
  },
];
