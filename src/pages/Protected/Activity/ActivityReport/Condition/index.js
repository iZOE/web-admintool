import React, { useEffect, useMemo } from 'react'
import { Form, Row, Col, Input, DatePicker, Checkbox } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import moment from 'moment'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { getISODateTimeString } from 'mixins/dateTime'
import FormItemsActivityName from '../../components/FormItems/ActivityName'
import QueryButton from 'components/FormActionButtons/Query'

const { useForm } = Form
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_A_MONTH_TO_TODAY,
    IsDispatched: false,
    ActivityId: null,
    Subject: null,
}

function Condition({
    initialValueForTrigger: { id, name, startTime, endTime },
    onReady,
    onUpdate,
}) {
    const [form] = useForm()
    const { formatMessage } = useIntl()
    const plsEnter = formatMessage({
        id: 'Share.PlaceHolder.Input.PlsEnter',
    })

    const dateRangeFromRoute = useMemo(() => {
        if (startTime && endTime && name) {
            return [moment(startTime), moment(endTime)]
        }
        return RANGE.FROM_A_MONTH_TO_TODAY
    }, [startTime, endTime, name])

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(true)
    }, [])

    function resultCondition({ IsDispatched, Date, Subject, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            ModifyBeginDate: getISODateTimeString(start),
            ModifyEndDate: getISODateTimeString(end),
            IsDispatched: !IsDispatched,
            Subject,
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            labelAlign="left"
            form={form}
            initialValues={{
                ...CONDITION_INITIAL_VALUE,
                ActivityId: id && parseInt(id),
                Subject: name && name,
                Date: dateRangeFromRoute,
            }}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageOperationActivityReport.QueryCondition.ActivityId" />
                        }
                        name="ActivityId"
                    >
                        <Input
                            type="number"
                            placeholder={
                                `${plsEnter}` +
                                formatMessage({
                                    id:
                                        'PageOperationActivityReport.QueryCondition.ActivityId',
                                })
                            }
                        />
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <FormItemsActivityName />
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageOperationActivityReport.QueryCondition.Date"
                                description="DateRange"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <Form.Item name="IsDispatched" valuePropName="checked">
                        <Checkbox>
                            <FormattedMessage id="PageOperationActivityReport.QueryCondition.IsDispatched" />
                        </Checkbox>
                    </Form.Item>
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
