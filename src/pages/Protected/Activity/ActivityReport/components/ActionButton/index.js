import React, { useContext } from 'react';
import { Button, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import { PageContext } from './../../index';
import DateRangeWithIcon from 'components/DateRangeWithIcon';

const { ACTIVITY_REPORT_DISPATCH, ACTIVITY_REPORT_REJECT } = PERMISSION;

export default function ButtonItems({ record }) {
  const {
    ActivityId,
    ActivityBonusId,
    Subject,
    CalcuBeginDate,
    CalcuEndDate,
    NotReceived,
  } = record;

  const { onDispatch } = useContext(PageContext);

  const handleDispatch = () =>
    onDispatch({
      Id: ActivityBonusId,
      IsDispatch: true,
      IsBatch: true,
    });
  const handleReject = () =>
    onDispatch({
      Id: ActivityBonusId,
      IsDispatch: false,
      IsBatch: true,
    });

  return (
    <>
      <Permission functionIds={[ACTIVITY_REPORT_DISPATCH]}>
        {NotReceived <= 0 ? (
          <Button type="link" disabled={NotReceived <= 0}>
            <FormattedMessage id="Share.ActionButton.BatchDistribution" />
          </Button>
        ) : (
          <Popconfirm
            title={
              <>
                <div>
                  <FormattedMessage
                    id="PageOperationActivityReport.Popconfirm.Title"
                    values={{
                      subject: Subject,
                      activityid: ActivityId,
                    }}
                  />
                </div>
                <span style={{ color: '#FF0000' }}>
                  <FormattedMessage id="PageOperationActivityReport.Popconfirm.Dispatch" />
                </span>
                <DateRangeWithIcon
                  startTime={CalcuBeginDate}
                  endTime={CalcuEndDate}
                />
                <FormattedMessage id="PageOperationActivityReport.Popconfirm.Hint1" />
                ?
              </>
            }
            onConfirm={() => handleDispatch()}
            okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
            cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
            placement="topLeft"
          >
            <Button type="link" disabled={NotReceived <= 0}>
              <FormattedMessage id="Share.ActionButton.BatchDistribution" />
            </Button>
          </Popconfirm>
        )}
      </Permission>
      <Permission functionIds={[ACTIVITY_REPORT_REJECT]}>
        {NotReceived <= 0 ? (
          <Button type="link" danger disabled={NotReceived <= 0}>
            <FormattedMessage id="Share.ActionButton.BatchReject" />
          </Button>
        ) : (
          <Popconfirm
            title={
              <>
                <div>
                  <FormattedMessage
                    id="PageOperationActivityReport.Popconfirm.Title"
                    values={{
                      subject: Subject,
                      activityid: ActivityId,
                    }}
                  />
                </div>
                <span style={{ color: '#FF0000' }}>
                  <FormattedMessage id="PageOperationActivityReport.Popconfirm.Reject" />
                </span>
                <DateRangeWithIcon
                  startTime={CalcuBeginDate}
                  endTime={CalcuEndDate}
                />
                <FormattedMessage id="PageOperationActivityReport.Popconfirm.Hint1" />
                ?
              </>
            }
            onConfirm={() => handleReject()}
            okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
            cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
            placement="topLeft"
          >
            <Button type="link" danger disabled={NotReceived <= 0}>
              <FormattedMessage id="Share.ActionButton.BatchReject" />
            </Button>
          </Popconfirm>
        )}
      </Permission>
      <Link
        to={{
          pathname: `/activity/activity-report/detail/${ActivityId}`,
          state: { Subject, ActivityBonusId },
        }}
      >
        <Button type="link" style={{ color: '#faad14' }}>
          <FormattedMessage id={`Share.ActionButton.Detail`} />
        </Button>
      </Link>
    </>
  );
}
