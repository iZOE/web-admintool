import React, { createContext, useMemo } from 'react'
import { Result, Space, message } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import caller from 'utils/fetcher'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ReportScaffold from 'components/ReportScaffold'
import ExportReportButton from 'components/ExportReportButton'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import SummaryView from './Summary'

const { ACTIVITY_REPORT_VIEW, ACTIVITY_REPORT_EXPORT } = PERMISSION

export const PageContext = createContext()

const PageView = () => {
  const location = useLocation()

  const dispatchSuccessMsg = useIntl().formatMessage({
    id: 'PageOperationActivityReport.Message.Dispatch',
  })
  const rejectSuccessMsg = useIntl().formatMessage({
    id: 'PageOperationActivityReport.Message.Reject',
  })
  const errorMsg = useIntl().formatMessage({
    id: 'PageOperationActivityReport.Message.Fail',
  })

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    onReady,
    boundedMutate,
  } = useGetDataSourceWithSWR({
    url: '/api/v2/Activity/Report/Search',
    defaultSortKey: 'CalcuBeginDate',
    autoFetch: true,
  })

  function handleDispatch(value) {
    caller({
      method: 'post',
      endpoint: '/api/v2/Activity/BonusDispatch',
      body: value,
    })
      .then(() => {
        if (value.IsDispatch) {
          message.success(dispatchSuccessMsg)
        } else {
          message.success(rejectSuccessMsg)
        }
        boundedMutate()
      })
      .catch(err => {
        message.error(errorMsg)
      })
  }

  const paramsFromRouteQuery = useMemo(() => {
    const { id, name, startTime, endTime } = queryString.parse(
      location.search.replace(/\+/g, '%2B'),
    )
    return {
      id,
      name,
      startTime,
      endTime,
    }
  }, [location])

  return (
    <PageContext.Provider value={{ onDispatch: handleDispatch }}>
      <Permission
        isPage
        functionIds={[ACTIVITY_REPORT_VIEW]}
        failedRender={
          <Result
            status="warning"
            title={<FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />}
            subTitle={
              <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
            }
          />
        }
      >
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition
              onReady={onReady}
              onUpdate={onUpdateCondition}
              initialValueForTrigger={paramsFromRouteQuery}
            />
          }
          summaryComponent={
            <SummaryView summary={dataSource && dataSource.Summary} />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查询结果"
                />
              }
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource && dataSource.List}
              rowKey={record => record.ActivityBonusId}
              total={dataSource && dataSource.TotalCount}
              extendArea={
                <Space>
                  <Permission functionIds={[ACTIVITY_REPORT_EXPORT]}>
                    <ExportReportButton
                      condition={condition}
                      actionUrl="/api/v2/Activity/Report/Export"
                    />
                  </Permission>
                </Space>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
      </Permission>
    </PageContext.Provider>
  )
}

export default PageView
