import React, { useEffect, useState, createContext } from 'react';
import { message, Form, Input, Modal, Switch, Space, Button, Typography } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import update from 'immutability-helper';
import caller from 'utils/fetcher';
import { FormattedMessage, useIntl } from 'react-intl';
import usePermission from 'hooks/usePermission';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import DragableBodyRow from './DragableBodyRow';
import * as PERMISSION from 'constants/permissions';
import { COLUMNS_CONFIG } from './datatableConfig';

const { ACTIVITY_TYPE_MANAGEMENT_VIEW, ACTIVITY_TYPE_MANAGEMENT_CREATE, ACTIVITY_TYPE_MANAGEMENT_EDIT } = PERMISSION;
const { Text } = Typography;

export const PageContext = createContext();

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};

export default function ActivityTypeManagement() {
  const [form] = Form.useForm();
  const [modalData, setModalData] = useState({
    visible: false,
    createMode: false,
  });
  const [addTypeDisabled, setAddTypeButtonDisabled] = useState(false);

  const successMsg = useIntl().formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess',
  });

  const placeholderName = useIntl().formatMessage({
    id: 'PageOperationActivityTypeManagement.FormItems.Placeholder.PlsEnterName',
  });

  const { dataSource, onReady, boundedMutate } = useGetDataSourceWithSWR({
    method: 'get',
    url: '/api/v2/Activity/Type',
    autoFetch: true,
    dataRefactor: data => {
      data.Data.sort((a, b) => a.SortOrder - b.SortOrder);
      if (data.Data.length > 9) {
        setAddTypeButtonDisabled(true);
      } else {
        setAddTypeButtonDisabled(false);
      }
      return data;
    },
  });

  useEffect(() => {
    onReady(true);
  }, []);

  function onModalOpen({ createMode, defaultValues }) {
    setModalData({
      visible: true,
      title: true,
      createMode: !!createMode,
    });
    if (defaultValues) {
      form.setFieldsValue(defaultValues);
    } else {
      form.resetFields();
    }
  }

  function onCreateActType(values) {
    values.SortOrder = dataSource.Data.length + 1;
    caller({
      endpoint: '/api/v2/Activity/Type',
      method: 'POST',
      body: values,
    }).then(() => {
      message.success(successMsg, 10);
      boundedMutate();
    });
  }

  function onEditActType(values) {
    caller({
      endpoint: `/api/v2/Activity/Type/${values.Id}`,
      method: 'PUT',
      body: values,
    }).then(() => {
      message.success(successMsg, 10);
      boundedMutate();
    });
  }

  function onModalSave(values) {
    if (modalData.createMode) {
      onCreateActType(values);
    } else {
      onEditActType(values);
    }
    setModalData({
      visible: false,
      createMode: false,
    });
  }

  function onModalClose() {
    setModalData({
      visible: false,
      title: true,
      defaultValues: null,
    });
  }

  const components = {
    body: {
      row: DragableBodyRow,
    },
  };

  function moveRow(dragIndex, hoverIndex) {
    const { Data } = dataSource;
    if (Data.length > 0) {
      const dragRow = Data[dragIndex];

      const newData = update(Data, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, dragRow],
        ],
      });

      const newSortData = newData.map((d, idx) => ({
        Id: d.Id,
        SortOrder: idx,
      }));
      caller({
        method: 'PUT',
        endpoint: '/api/v2/Activity/Type/SortOrder',
        body: newSortData,
      }).then(() => {
        boundedMutate();
      });
    }
  }

  // 額外計算編輯的權限決定是否可以拖曳Row
  const [canDrag] = usePermission(ACTIVITY_TYPE_MANAGEMENT_EDIT);

  return (
    <PageContext.Provider value={{ onModalOpen }}>
      <Permission isPage functionIds={[ACTIVITY_TYPE_MANAGEMENT_VIEW]}>
        <DndProvider backend={HTML5Backend}>
          <DataTable
            title={
              <Space>
                <FormattedMessage id="Share.Table.SearchResult" description="查詢結果" />
                <Text type="warning" style={{ fontSize: 10, marginLeft: 16 }}>
                  <FormattedMessage
                    id="PageOperationActivityTypeManagement.DataTable.Tips.Only10CanBeSet"
                    description="僅能設定10筆類別"
                    style={{
                      fontSize: '10px',
                      color: 'red',
                    }}
                  />
                </Text>
              </Space>
            }
            config={COLUMNS_CONFIG}
            dataSource={dataSource && dataSource.Data}
            components={canDrag && components}
            rowKey={record => record.Id}
            pagination={false}
            onRow={(record, index) => ({
              index,
              moveRow,
            })}
            extendArea={
              <Permission
                functionIds={[ACTIVITY_TYPE_MANAGEMENT_CREATE]}
                failedRender={
                  <Button type="primary" icon={<PlusOutlined />} disabled>
                    <FormattedMessage
                      id="PageOperationActivityTypeManagement.ActionButton.Create"
                      description="新增类别"
                    />
                  </Button>
                }
              >
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={() => onModalOpen({ createMode: true })}
                  disabled={addTypeDisabled}
                >
                  <FormattedMessage
                    id="PageOperationActivityTypeManagement.ActionButton.Create"
                    description="新增类别"
                  />
                </Button>
              </Permission>
            }
          />
        </DndProvider>
      </Permission>
      <Modal
        title={
          modalData.createMode ? (
            <FormattedMessage id="Share.ActionButton.Create" description="新增" />
          ) : (
            <FormattedMessage id="Share.ActionButton.Edit" description="編輯" />
          )
        }
        visible={modalData.visible}
        okText={<FormattedMessage id="Share.ActionButton.Submit" />}
        cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
        onOk={() => {
          form.submit();
        }}
        onCancel={onModalClose}
      >
        <Form {...layout} form={form} name="control-hooks" onFinish={onModalSave}>
          <Form.Item name="Id" noStyle />
          <Form.Item
            name="Name"
            label={
              <FormattedMessage id="PageOperationActivityTypeManagement.FormItems.Name" description="输入类别名称" />
            }
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input placeholder={placeholderName} />
          </Form.Item>
          <Form.Item
            name="IsEnabled"
            label={<FormattedMessage id="Share.CommonKeys.Status" description="状态" />}
            valuePropName="checked"
          >
            <Switch
              checkedChildren={<FormattedMessage id="Share.SwitchButton.IsEnable.Yes" />}
              unCheckedChildren={<FormattedMessage id="Share.SwitchButton.IsEnable.No" />}
            />
          </Form.Item>
        </Form>
      </Modal>
    </PageContext.Provider>
  );
}
