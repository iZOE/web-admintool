import React from 'react';
import { mutate } from 'swr';
import { Badge, Row, Col, Space, Tag } from 'antd';
import { FormattedMessage } from 'react-intl';
import caller from 'utils/fetcher';
import ActionButtons from './ActionButtons';
import DateWithFormat from 'components/DateWithFormat';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageOperationActivityTypeManagement.DataTable.Title.SortOrder" description="排序" />,
    dataIndex: 'SortOrder',
    key: 'SortOrder',
    isShow: true,
    render: (text, record, index) => {
      return text + 1;
    },
  },
  {
    title: <FormattedMessage id="PageOperationActivityTypeManagement.DataTable.Title.Name" description="類別名稱" />,
    dataIndex: 'Name',
    key: 'Name',
    render: (_1, { Name, IsEnabled }, _2) => {
      return (
        <Space>
          {Name}
          {!IsEnabled && (
            <Tag>
              <FormattedMessage id="Share.Status.Deactivated" />
            </Tag>
          )}
        </Space>
      );
    },
  },
  {
    title: <FormattedMessage id="PageOperationActivityTypeManagement.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'IsEnabled',
    key: 'IsEnabled',
    isShow: true,
    render: (text, record, index) => (
      <Badge
        status={record.IsEnabled ? 'success' : 'error'}
        text={
          <FormattedMessage
            id={`PageOperationActivityTypeManagement.DataTable.StatusList.${record.IsEnabled}`}
            description="狀態"
          />
        }
      />
    ),
    onCell: record => {
      return {
        onClick: event => {
          caller({
            endpoint: `/api/v2/Activity/Type/${record.Id}`,
            method: 'PUT',
            body: {
              Id: record.Id,
              Name: record.Name,
              IsEnabled: !record.IsEnabled,
            },
          }).then(() => {
            mutate('/api/v2/Activity/Type');
          });
        },
      };
    },
  },
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityTypeManagement.DataTable.Title.LastUpdatedTime"
        description="更新時間"
      />
    ),
    dataIndex: 'LastUpdatedTime',
    key: 'LastUpdatedTime',
    render: (_1, { LastUpdatedTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage
        id="PageOperationActivityTypeManagement.DataTable.Title.LastUpdatedUserName"
        description="更新人員"
      />
    ),
    dataIndex: 'LastUpdatedUserName',
    key: 'LastUpdatedUserName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageOperationActivityTypeManagement.DataTable.Title.action" description="操作" />,
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => (
      <Row>
        <Col>
          <ActionButtons record={record} />
        </Col>
      </Row>
    ),
  },
];
