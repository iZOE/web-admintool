import React, { useContext } from 'react'
import Permission from 'components/Permission'
import { FormattedMessage } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import { PageContext } from '../index'

const { ACTIVITY_TYPE_MANAGEMENT_EDIT } = PERMISSION

export default function ActionButtons({ record }) {
    const { onModalOpen } = useContext(PageContext)

    function onClick() {
        onModalOpen({ defaultValues: record })
    }

    return (
        <Permission functionIds={[ACTIVITY_TYPE_MANAGEMENT_EDIT]}>
            <div>
                <a onClick={onClick}>
                    <FormattedMessage
                        id="Share.ActionButton.Edit"
                        description="編輯"
                    />
                </a>
            </div>
        </Permission>
    )
}
