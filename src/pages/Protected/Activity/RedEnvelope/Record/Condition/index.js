import React, { useState, useEffect, useContext } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Form, Row, Col, Select, DatePicker, Input } from 'antd'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import QueryButton from 'components/FormActionButtons/Query'
import { PageContext } from '../../index.js'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const DATE_TYPE_IDS = [1, 2]
const TYPE_IDS = [1, 2]
const STATUS_IDS = [2, 0]

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_INITIAL_TO_TODAY,
    DateType: 1,
    GrantTypeId: null,
    Status: STATUS_IDS,
}

function Condition({ onReady, onUpdate }) {
    const intl = useIntl()
    const [form] = useForm()
    const [placeholder, setPlaceholder] = useState(1)
    const { detailID } = useContext(PageContext)

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(true)
    }, [])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            StartTime: getISODateTimeString(start),
            EndTime: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={{
                ...CONDITION_INITIAL_VALUE,
                AdvancedSearchConditionItem: detailID ? 3 : 1,
                AdvancedSearchConditionValue: detailID || null,
            }}
            onFinish={onFinish}
            onValuesChange={(changedValues, allValues) => {
                if (changedValues.AdvancedSearchConditionItem) {
                    form.setFieldsValue({
                        AdvancedSearchConditionValue: null,
                    })
                    setPlaceholder(changedValues.AdvancedSearchConditionItem)
                }
            }}
        >
            <Row gutter={[16, 8]}>
                <Col sm={10}>
                    <Input.Group compact>
                        <Form.Item name="DateType" noStyle>
                            <Select
                                className="option"
                                style={{ width: '100px' }}
                            >
                                {DATE_TYPE_IDS.map(id => (
                                    <Option value={id} key={id}>
                                        <FormattedMessage
                                            id={`PageOperationRedEnvelope.QueryCondition.SelectItem.DetailDateType.${id}`}
                                        />
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item name="Date" noStyle>
                            <RangePicker
                                style={{ width: 'calc(100% - 100px)' }}
                                showTime
                                format={FORMAT.DISPLAY.DEFAULT}
                            />
                        </Form.Item>
                    </Input.Group>
                </Col>
                <Col sm={7}>
                    <Form.Item
                        name="GrantTypeId"
                        label={
                            <FormattedMessage
                                id="PageOperationRedEnvelope.QueryCondition.Type"
                                description="红包类型"
                            />
                        }
                    >
                        <Select className="option">
                            <Option value={null} key="all">
                                <FormattedMessage id="Share.Dropdown.All" />
                            </Option>
                            {TYPE_IDS.map(id => (
                                <Option value={id} key={id}>
                                    <FormattedMessage
                                        id={`PageOperationRedEnvelope.QueryCondition.SelectItem.Type.${id}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={7}>
                    <Form.Item
                        name="Status"
                        label={
                            <FormattedMessage
                                id="PageOperationRedEnvelope.QueryCondition.Status"
                                description="红包状态"
                            />
                        }
                    >
                        <Select className="option" mode="multiple">
                            {STATUS_IDS.map(id => (
                                <Option value={id} key={id}>
                                    <FormattedMessage
                                        id={`PageOperationRedEnvelope.QueryCondition.SelectItem.Status.${id}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={12}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.AdvanceQuery"
                                description="進階查詢"
                            />
                        }
                        name="AdvancedSearchConditionValue"
                    >
                        <Input
                            addonBefore={
                                <Form.Item
                                    name="AdvancedSearchConditionItem"
                                    noStyle
                                >
                                    <Select>
                                        <Option value={1}>
                                            <FormattedMessage
                                                id="PageOperationRedEnvelope.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.1"
                                                description="会员帐号"
                                            />
                                        </Option>
                                        <Option value={2}>
                                            <FormattedMessage
                                                id="PageOperationRedEnvelope.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.2"
                                                description="红包来源帐号"
                                            />
                                        </Option>
                                        <Option value={3}>
                                            <FormattedMessage
                                                id="PageOperationRedEnvelope.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.3"
                                                description="红包ID"
                                            />
                                        </Option>
                                    </Select>
                                </Form.Item>
                            }
                            style={{ width: '100%' }}
                            placeholder={intl.formatMessage({
                                id: `PageOperationRedEnvelope.Placeholder.AdvancedSearch.${placeholder}`,
                            })}
                        />
                    </Form.Item>
                </Col>
                <Col sm={{ span: 4, offset: 8 }} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
