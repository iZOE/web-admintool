import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Result, Space } from 'antd'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ExportReportButton from 'components/ExportReportButton'
import ReportScaffold from 'components/ReportScaffold'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'

const {
    ACTIVITY_RED_ENVELOPE_MANAGEMENT_DETAILVIEW,
    ACTIVITY_RED_ENVELOPE_MANAGEMENT_DETAILEXPORT,
} = PERMISSION

const PageView = () => {
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        onReady,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/redbag/search-detail',
        defaultSortKey: 'ReceiveTime',
        autoFetch: true,
    })

    function onSelectChange(selectedRowKeys) {
        setSelectedRowKeys(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
        getCheckboxProps: record => ({
            disabled: record.Status !== 1,
            name: record.name,
        }),
    }

    return (
        <>
            <Permission
                functionIds={[ACTIVITY_RED_ENVELOPE_MANAGEMENT_DETAILVIEW]}
                failedRender={
                    <Result
                        status="warning"
                        title={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                        }
                        subTitle={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                        }
                    />
                }
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢結果"
                                />
                            }
                            rowKey={(record, index) =>
                                `${record.RedBagId}_${index}`
                            }
                            rowSelection={rowSelection}
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            extendArea={
                                <Space align="start">
                                    <Permission
                                        functionIds={[
                                            ACTIVITY_RED_ENVELOPE_MANAGEMENT_DETAILEXPORT,
                                        ]}
                                    >
                                        <ExportReportButton
                                            condition={condition}
                                            actionUrl="/api/redbag/export-detail"
                                        />
                                    </Permission>
                                </Space>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
        </>
    )
}

export default PageView
