import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.ID" description="ID" />,
    dataIndex: 'RedBagId',
    key: 'RedBagId',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.MemberName" description="会员帐号" />,
    dataIndex: 'ToMemberName',
    key: 'ToMemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.ReceiveTime" description="领取时间" />,
    dataIndex: 'ReceiveTime',
    key: 'ReceiveTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ReceiveTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageOperationRedEnvelope.DataTable.ReceiveTime" description="领取时间" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemReceiveTime',
    key: 'SystemReceiveTime',
    render: (_1, { SystemReceiveTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.SourceName" description="红包来源帐号" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.Type" description="红包类型" />,
    dataIndex: 'GrantTypeText',
    key: 'GrantTypeId',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.ReceiveAmount" description="领取金额" />,
    dataIndex: 'RedBagAmount',
    key: 'RedBagAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.Status" description="红包状态" />,
    dataIndex: 'StatusText',
    key: 'Status',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.CreateOn" description="建立时间" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.CreateBy" description="建立人员" />,
    dataIndex: 'CreateUserAccount',
    key: 'CreateUserAccount',
    isShow: true,
  },
];
