import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Button } from 'antd'
import Permission from 'components/Permission'
import * as PERMISSION from 'constants/permissions'
import EditRedEnvelopeModal from '../EditRedEnvelopeModal'

const { ACTIVITY_RED_ENVELOPE_MANAGEMENT_EDIT } = PERMISSION

function EditRedEnvelopeButton({ RedBagId }) {
    const [
        isEditRedEnvelopeModalOpen,
        setIsEditRedEnvelopeModalOpen,
    ] = useState(false)

    return (
        <>
            {isEditRedEnvelopeModalOpen && (
                <EditRedEnvelopeModal
                    RedBagId={RedBagId}
                    visible={isEditRedEnvelopeModalOpen}
                    onOk={() => setIsEditRedEnvelopeModalOpen(false)}
                />
            )}
            <Permission functionIds={[ACTIVITY_RED_ENVELOPE_MANAGEMENT_EDIT]}>
                <Button
                    type="link"
                    onClick={() => setIsEditRedEnvelopeModalOpen(true)}
                    style={{ padding: 0 }}
                >
                    <FormattedMessage id="Share.ActionButton.Edit" />
                </Button>
            </Permission>
        </>
    )
}

export default EditRedEnvelopeButton
