import React, { createContext, useState } from 'react'
import { Result, Space } from 'antd'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ExportReportButton from 'components/ExportReportButton'
import ReportScaffold from 'components/ReportScaffold'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import AddRedEnvelopeButton from './AddRedEnvelopeButton'

const {
    ACTIVITY_RED_ENVELOPE_MANAGEMENT_VIEW,
    ACTIVITY_RED_ENVELOPE_MANAGEMENT_EDIT,
    ACTIVITY_RED_ENVELOPE_MANAGEMENT_EXPORT,
} = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
        onReady,
        boundedMutate,
    } = useGetDataSourceWithSWR({
        url: '/api/redbag/search',
        defaultSortKey: 'CreateTime',
        autoFetch: true,
    })

    function onSelectChange(selectedRowKeys) {
        setSelectedRowKeys(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
        getCheckboxProps: record => ({
            disabled: record.Status !== 1,
            name: record.name,
        }),
    }

    return (
        <PageContext.Provider
            value={{
                boundedMutate,
            }}
        >
            <Permission
                functionIds={[ACTIVITY_RED_ENVELOPE_MANAGEMENT_VIEW]}
                failedRender={
                    <Result
                        status="warning"
                        title={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                        }
                        subTitle={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                        }
                    />
                }
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢結果"
                                />
                            }
                            rowKey={record => record.RedBagId}
                            rowSelection={rowSelection}
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            extendArea={
                                <Space align="start">
                                    <Permission
                                        functionIds={[
                                            ACTIVITY_RED_ENVELOPE_MANAGEMENT_EDIT,
                                        ]}
                                    >
                                        <AddRedEnvelopeButton />
                                    </Permission>
                                    <Permission
                                        functionIds={[
                                            ACTIVITY_RED_ENVELOPE_MANAGEMENT_EXPORT,
                                        ]}
                                    >
                                        <ExportReportButton
                                            condition={condition}
                                            actionUrl="/api/redbag/export"
                                        />
                                    </Permission>
                                </Space>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
        </PageContext.Provider>
    )
}

export default PageView
