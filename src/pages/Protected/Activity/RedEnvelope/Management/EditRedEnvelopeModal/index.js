import React, { useState, useEffect, useContext } from 'react'
import { Button, Modal, Form, Row, Col, message, Divider } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import QRious from 'qrious'
import _debounce from 'lodash/debounce'
import caller from 'utils/fetcher'
import SwitchWithDefaultName from 'components/SwitchWithDefaultName'
import { PageContext } from '../index'

export default function EditRedEnvelopeModal({ RedBagId, visible, onOk }) {
    const intl = useIntl()
    const { boundedMutate } = useContext(PageContext)
    const [form] = Form.useForm()
    const [redEnvelope, setRedEnvelope] = useState()
    const qr = new QRious({
        element: document.getElementById('qr'),
        value: redEnvelope && redEnvelope.QrCode,
        backgroundAlpha: 0,
        size: 180,
    })

    useEffect(() => {
        caller({
            method: 'get',
            endpoint: `/api/redbag/${RedBagId}`,
        }).then(res => {
            setRedEnvelope(res.Data)
        })
    }, [])

    const onSubmitClick = values => {
        if (typeof values.Status === 'boolean' && !values.Status) {
            caller({
                method: 'put',
                endpoint: `/api/redbag/${RedBagId}/0`,
            }).then(() => {
                message.success(
                    intl.formatMessage({
                        id: 'Share.SuccessMessage.UpdateSuccess',
                    }),
                    5
                )
                onOk()
                boundedMutate()
            })
        }
        onOk()
    }

    return (
        <Modal
            title={
                <FormattedMessage
                    id="PageOperationRedEnvelope.DataTable.EditRedEnvelope"
                    description="编辑红包"
                />
            }
            visible={visible}
            width={400}
            onOk={onOk}
            onCancel={onOk}
            footer={null}
        >
            {redEnvelope && (
                <Form form={form} onFinish={onSubmitClick}>
                    <Row gutter={24}>
                        <Col sm={24} xs={24}>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <FormattedMessage
                                    id="PageOperationRedEnvelope.Modal.SerialNumber"
                                    values={{
                                        SerialNumber:
                                            redEnvelope && redEnvelope.RedBagSn,
                                    }}
                                />
                                <CopyToClipboard
                                    text={redEnvelope && redEnvelope.RedBagSn}
                                    onCopy={(text, result) => {
                                        message[result ? 'success' : 'warning'](
                                            <FormattedMessage
                                                id={`PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Copy${
                                                    result ? 'Success' : 'Fail'
                                                }`}
                                                defaultMessage={`复制${
                                                    result ? '成功' : '失败'
                                                }`}
                                            />,
                                            5
                                        )
                                    }}
                                    style={{
                                        marginLeft: '1em',
                                    }}
                                >
                                    <Button>
                                        <FormattedMessage id="PageOperationRedEnvelope.Modal.Copy" />
                                    </Button>
                                </CopyToClipboard>
                            </div>
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col sm={24} xs={24}>
                            <div
                                style={{
                                    marginTop: '1em',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <img src={qr && qr.toDataURL()} alt="qr" />
                            </div>
                        </Col>
                    </Row>
                    {redEnvelope.Status !== 0 && (
                        <Row gutter={24}>
                            <Col sm={24} xs={24}>
                                <div
                                    style={{
                                        marginTop: '1em',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <Form.Item
                                        label={
                                            <FormattedMessage
                                                id="PageOperationRedEnvelope.DataTable.DisableRedEnvelope"
                                                description="停用红包"
                                            />
                                        }
                                        name="Status"
                                        valuePropName="checked"
                                        style={{ margin: 0 }}
                                    >
                                        <SwitchWithDefaultName />
                                    </Form.Item>
                                </div>
                            </Col>
                        </Row>
                    )}
                    <Divider />
                    <Row gutter={24}>
                        <Col sm={24} xs={24}>
                            <div
                                style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <Button type="primary" htmlType="submit">
                                    <FormattedMessage id="PageOperationRedEnvelope.DataTable.Confirm" />
                                </Button>
                                <Button
                                    onClick={onOk}
                                    style={{ marginLeft: '1em' }}
                                >
                                    <FormattedMessage id="PageOperationRedEnvelope.DataTable.Cancel" />
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            )}
        </Modal>
    )
}
