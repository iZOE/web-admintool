import React, { useState, useCallback, useContext } from 'react'
import {
    Button,
    message,
    Modal,
    Form,
    Row,
    Col,
    Input,
    InputNumber,
    Divider,
    DatePicker,
    Select,
} from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import moment from 'moment'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT } from 'constants/dateConfig'
import caller from 'utils/fetcher'
import { PageContext } from '../index'

const { Option } = Select
const TYPE_IDS = [1, 2]

const initialConditions = {
    MemberName: '',
    GrantTypeId: 1,
}

export default function AddRedEnvelopeModal({ visible, onOk }) {
    const intl = useIntl()
    const { boundedMutate } = useContext(PageContext)
    const [form] = Form.useForm()
    const [memberNameValidate, setMemberNameValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'PageOperationRedEnvelope.Placeholder.MemberName',
        }),
        help: null,
        status: null,
    })
    const [isRandomType, setRandomType] = useState(false)
    const [isTotalAmountValidate, setTotalAmountValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'Share.FormValidate.Value.Hint',
        }),
        help: null,
        status: null,
    })
    const [isCountValidate, setCountValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'Share.FormValidate.Value.Hint',
        }),
        help: null,
        status: null,
    })
    const [isMaxAmountValidate, setMaxAmountValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'Share.FormValidate.Value.Hint',
        }),
        help: null,
        status: null,
    })
    const [isMinAmountValidate, setMinAmountValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'Share.FormValidate.Value.Hint',
        }),
        help: null,
        status: null,
    })
    const [isTurnoverMultipleValidate, setTurnoverMultipleValidate] = useState({
        help: null,
        status: null,
    })
    const [isSubmitAble, setSubmitAble] = useState(false)
    const [StartTime, setSendStartDate] = useState()
    const [EndTime, setSendEndDate] = useState()
    const disabledStartDate = useCallback(
        current => {
            return (
                current && EndTime && current > moment(EndTime).add(-1, 'days')
            )
        },
        [EndTime]
    )
    const disabledEndDate = useCallback(
        current => {
            return (
                current &&
                StartTime &&
                current < moment(StartTime).add(1, 'days')
            )
        },
        [StartTime]
    )

    const handleValusChanges = (updatingField, allValues) => {
        // 檢查是否該輸入的資料都有輸入的flag
        let hasAllNormalTypeValues = true
        let hasAllRandomTypeValues = true
        if (!allValues['MemberName']) {
            setMemberNameValidate(prev => ({
                ...prev,
                status: null,
                help: null,
            }))
            hasAllNormalTypeValues = false
            hasAllRandomTypeValues = false
        }
        if (allValues['StartTime']) {
            setSendStartDate(allValues['StartTime'])
        } else {
            hasAllNormalTypeValues = false
            hasAllRandomTypeValues = false
        }
        if (allValues['EndTime']) {
            setSendEndDate(allValues['EndTime'])
        }
        if (allValues['GrantTypeId'] === 2) {
            setRandomType(true)
        } else {
            setRandomType(false)
        }
        if (allValues['TotalAmount']) {
            const TotalAmount = allValues['TotalAmount']
            if (TotalAmount > 0 && TotalAmount < 100000000) {
                setTotalAmountValidate(prev => ({
                    ...prev,
                    status: 'success',
                    help: null,
                }))
            }
        } else {
            setTotalAmountValidate(prev => ({
                ...prev,
                status: null,
                help: null,
            }))
            hasAllNormalTypeValues = false
            hasAllRandomTypeValues = false
        }
        if (allValues['TotalCount']) {
            setCountValidate(prev => ({
                ...prev,
                status: 'success',
                help: null,
            }))
        } else {
            setCountValidate(prev => ({
                ...prev,
                status: null,
                help: null,
            }))
            hasAllNormalTypeValues = false
        }
        if (allValues['MaxAmount']) {
            setMaxAmountValidate(prev => ({
                ...prev,
                status: 'success',
                help: null,
            }))
        } else {
            setMaxAmountValidate(prev => ({
                ...prev,
                status: null,
                help: null,
            }))
            hasAllRandomTypeValues = false
        }
        if (allValues['MinAmount']) {
            setMinAmountValidate(prev => ({
                ...prev,
                status: 'success',
                help: null,
            }))
        } else {
            setMinAmountValidate(prev => ({
                ...prev,
                status: null,
                help: null,
            }))
            hasAllRandomTypeValues = false
        }
        if (allValues['MaxAmount'] && allValues['MinAmount']) {
            if (allValues['MinAmount'] >= allValues['MaxAmount']) {
                setMinAmountValidate(prev => ({
                    ...prev,
                    status: 'error',
                    help: (
                        <FormattedMessage id="PageOperationRedEnvelope.Modal.MinMax" />
                    ),
                }))
                hasAllRandomTypeValues = false
            }
        } else {
        }
        if (allValues['TurnoverMultiple']) {
            setTurnoverMultipleValidate(prev => ({
                ...prev,
                status: 'success',
                help: null,
            }))
        } else {
            setTurnoverMultipleValidate(prev => ({
                ...prev,
                status: null,
                help: null,
            }))
            hasAllNormalTypeValues = false
            hasAllRandomTypeValues = false
        }
        // 更新按鈕可否submit
        if (allValues['GrantTypeId'] === 1) {
            setSubmitAble(hasAllNormalTypeValues)
        } else {
            setSubmitAble(hasAllRandomTypeValues)
        }
    }

    const onSubmitClick = values => {
        setMemberNameValidate(prev => ({ ...prev, status: 'validating' }))
        const url = '/api/Member/CheckMemberExists'
        caller({
            endpoint: url,
            method: 'post',
            body: { MemberName: values['MemberName'] },
        })
            .then(res => {
                if (!res.Status) {
                    setMemberNameValidate(prev => ({
                        ...prev,
                        status: 'error',
                        help: intl.formatMessage({
                            id: 'PageDepositList.Validation.MemberNameNotExist',
                        }),
                    }))
                } else {
                    setMemberNameValidate(prev => ({
                        ...prev,
                        status: null,
                        help: null,
                    }))

                    if (!values.EndTime) {
                        // 2099/12/31 23:59:59
                        values.EndTime = moment.unix(4102415999)
                    }

                    const payload = {
                        ...values,
                        StartTime: getISODateTimeString(values.StartTime),
                        EndTime: getISODateTimeString(values.EndTime),
                    }

                    caller({
                        method: 'post',
                        endpoint: '/api/redbag',
                        body: payload,
                    }).then(() => {
                        message.success(
                            intl.formatMessage({
                                id: 'Share.SuccessMessage.UpdateSuccess',
                            }),
                            5
                        )
                        // 手動清除表單驗證提示狀態
                        form.resetFields()
                        const defaultStatus = prev => ({
                            ...prev,
                            status: null,
                            help: null,
                        })
                        setMemberNameValidate(defaultStatus)
                        setRandomType(false)
                        setTotalAmountValidate(defaultStatus)
                        setCountValidate(defaultStatus)
                        setMaxAmountValidate(defaultStatus)
                        setMinAmountValidate(defaultStatus)
                        setTurnoverMultipleValidate(defaultStatus)

                        boundedMutate()
                    })
                    onOk()
                }
            })
            .catch(err => {
                setMemberNameValidate(prev => ({ ...prev, status: 'error' }))
            })
    }

    return (
        <Modal
            title={
                <FormattedMessage
                    id="PageOperationRedEnvelope.DataTable.AddRedEnvelope"
                    description="新增红包"
                />
            }
            visible={visible}
            width={600}
            onOk={onOk}
            onCancel={onOk}
            footer={null}
        >
            <Form
                form={form}
                labelCol={{
                    xs: { span: 24 },
                    sm: { span: 4 },
                }}
                wrapperCol={{ xs: { span: 24 }, sm: { span: 20 } }}
                onFinish={onSubmitClick}
                initialValues={initialConditions}
                onValuesChange={handleValusChanges}
            >
                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id:
                                    'PageOperationRedEnvelope.DataTable.MemberName',
                            })}：`}
                            name="MemberName"
                            hasFeedback
                            help={memberNameValidate.help}
                            validateStatus={memberNameValidate.status}
                        >
                            <Input
                                placeholder={memberNameValidate.placeholder}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id:
                                    'PageOperationRedEnvelope.DataTable.SendDate',
                            })}：`}
                        >
                            <Input.Group compact>
                                <Form.Item name="StartTime" noStyle>
                                    <DatePicker
                                        style={{ width: '50%' }}
                                        disabledDate={disabledStartDate}
                                        showTime
                                        format={FORMAT.DISPLAY.DEFAULT}
                                    />
                                </Form.Item>
                                <Form.Item name="EndTime" noStyle>
                                    <DatePicker
                                        style={{ width: '50%' }}
                                        disabledDate={disabledEndDate}
                                        showTime
                                        format={FORMAT.DISPLAY.DEFAULT}
                                    />
                                </Form.Item>
                            </Input.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id: 'PageOperationRedEnvelope.DataTable.Type',
                            })}：`}
                            name="GrantTypeId"
                        >
                            <Select>
                                {TYPE_IDS.map(id => (
                                    <Option value={id} key={id}>
                                        <FormattedMessage
                                            id={`PageOperationRedEnvelope.QueryCondition.SelectItem.Type.${id}`}
                                        />
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id:
                                    'PageOperationRedEnvelope.DataTable.TotalAmount',
                            })}：`}
                            name="TotalAmount"
                            hasFeedback
                            help={isTotalAmountValidate.help}
                            validateStatus={isTotalAmountValidate.status}
                        >
                            <InputNumber
                                placeholder={isTotalAmountValidate.placeholder}
                                min={1}
                                max={99999999}
                                precision={0}
                                style={{ width: '100%' }}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                {!isRandomType && (
                    <Row gutter={24}>
                        <Col sm={24} xs={24}>
                            <Form.Item
                                label={`${intl.formatMessage({
                                    id:
                                        'PageOperationRedEnvelope.DataTable.Count',
                                })}：`}
                                name="TotalCount"
                                hasFeedback
                                help={isCountValidate.help}
                                validateStatus={isCountValidate.status}
                            >
                                <InputNumber
                                    placeholder={isCountValidate.placeholder}
                                    min={1}
                                    max={99999999}
                                    precision={0}
                                    style={{ width: '100%' }}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                )}
                {isRandomType && (
                    <>
                        <Row gutter={24}>
                            <Col sm={24} xs={24}>
                                <Form.Item
                                    label={`${intl.formatMessage({
                                        id:
                                            'PageOperationRedEnvelope.DataTable.MaxAmount',
                                    })}：`}
                                    name="MaxAmount"
                                    hasFeedback
                                    help={isMaxAmountValidate.help}
                                    validateStatus={isMaxAmountValidate.status}
                                >
                                    <InputNumber
                                        placeholder={
                                            isMaxAmountValidate.placeholder
                                        }
                                        min={1}
                                        max={99999999}
                                        precision={0}
                                        style={{ width: '100%' }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={24}>
                            <Col sm={24} xs={24}>
                                <Form.Item
                                    label={`${intl.formatMessage({
                                        id:
                                            'PageOperationRedEnvelope.DataTable.MinAmount',
                                    })}：`}
                                    name="MinAmount"
                                    hasFeedback
                                    help={isMinAmountValidate.help}
                                    validateStatus={isMinAmountValidate.status}
                                >
                                    <InputNumber
                                        placeholder={
                                            isMinAmountValidate.placeholder
                                        }
                                        min={1}
                                        max={99999999}
                                        precision={0}
                                        style={{ width: '100%' }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </>
                )}
                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id:
                                    'PageOperationRedEnvelope.DataTable.TurnoverMultiple',
                            })}：`}
                            name="TurnoverMultiple"
                            hasFeedback
                            help={isTurnoverMultipleValidate.help}
                            validateStatus={isTurnoverMultipleValidate.status}
                        >
                            <InputNumber
                                placeholder={
                                    isTurnoverMultipleValidate.placeholder
                                }
                                min={1}
                                max={1000}
                                precision={2}
                                style={{ width: '100%' }}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Divider />
                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                            <Button
                                type="primary"
                                htmlType="submit"
                                disabled={!isSubmitAble}
                            >
                                <FormattedMessage id="PageOperationRedEnvelope.DataTable.Confirm" />
                            </Button>
                            <Button
                                onClick={onOk}
                                style={{ marginLeft: '1em' }}
                            >
                                <FormattedMessage id="PageOperationRedEnvelope.DataTable.Cancel" />
                            </Button>
                        </div>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )
}
