import React, { useContext } from 'react'
import { FormattedMessage } from 'react-intl'
import { Button } from 'antd'
import { PageContext } from '../../index.js'

export default function ChangeToDetailButton({ ID }) {
    const { changeToDetailTab } = useContext(PageContext)

    return (
        <Button type="primary" onClick={() => changeToDetailTab(ID)}>
            <FormattedMessage
                id="PageOperationRedEnvelope.DataTable.SendDetail"
                description="发送详情"
            />
        </Button>
    )
}
