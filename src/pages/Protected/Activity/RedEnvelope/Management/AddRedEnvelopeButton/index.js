import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Button } from 'antd'
import { AccountBookOutlined } from '@ant-design/icons'
import AddRedEnvelopeModal from '../AddRedEnvelopeModal'

function AddRedEnvelopeButton() {
    const [isAddRedEnvelopeModalOpen, setIsAddRedEnvelopeModalOpen] = useState(
        false
    )

    return (
        <>
            <AddRedEnvelopeModal
                visible={isAddRedEnvelopeModalOpen}
                onOk={() => setIsAddRedEnvelopeModalOpen(false)}
            />
            <Button
                type="primary"
                onClick={() => setIsAddRedEnvelopeModalOpen(true)}
                icon={<AccountBookOutlined style={{ marginRight: 6 }} />}
            >
                <FormattedMessage id="PageOperationRedEnvelope.DataTable.AddRedEnvelope" />
            </Button>
        </>
    )
}

export default AddRedEnvelopeButton
