import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import ChangeToDetailButton from './ChangeToDetailButton';
import EditRedEnvelopeButton from './EditRedEnvelopeButton';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.ID" description="ID" />,
    dataIndex: 'RedBagId',
    key: 'RedBagId',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.SendDate" description="发送时间" />,
    dataIndex: 'StartTime',
    key: 'StartTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { StartTime: s, EndTime: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageOperationRedEnvelope.DataTable.SendDate" description="发送时间" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemStartTime',
    key: 'SystemStartTime',
    render: (_1, { SystemStartTime: s, SystemEndTime: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.SourceName" description="红包来源帐号" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.Type" description="红包类型" />,
    dataIndex: 'GrantTypeId',
    key: 'GrantTypeId',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.GrantTypeText,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.TotalAmount" description="总金额" />,
    dataIndex: 'RedBagAmount',
    key: 'RedBagAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.Range" description="单笔可领金额范围" />,
    dataIndex: 'MaxAmount',
    key: 'MaxAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (text, record) => {
      if (record.GrantTypeId === 1) {
        return <ReactIntlCurrencyWithFixedDecimal value={text} />;
      } else {
        return (
          <>
            <ReactIntlCurrencyWithFixedDecimal value={record.MinAmount} />
            ~
            <ReactIntlCurrencyWithFixedDecimal value={text} />
          </>
        );
      }
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.AvailableCount" description="可领数量" />,
    dataIndex: 'TotalCount',
    key: 'TotalCount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.RemainCount" description="剩余数量" />,
    dataIndex: 'BalanceCount',
    key: 'BalanceCount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.Status" description="红包状态" />,
    dataIndex: 'StatusText',
    key: 'Status',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.SendDetail" description="发送详情" />,
    dataIndex: 'CurrentIssue',
    key: 'CurrentIssue',
    isShow: true,
    render: (text, record) => {
      if (record.Status === 0 || record.Status === 2) {
        return <ChangeToDetailButton ID={record.RedBagId} />;
      }
    },
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.CreateOn" description="建立时间" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.CreateBy" description="建立人员" />,
    dataIndex: 'CreateUserAccount',
    key: 'CreateUserAccount',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.UpdateOn" description="更新时间" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageOperationRedEnvelope.DataTable.UpdateBy" description="更新人员" />,
    dataIndex: 'MotifyUserAccount',
    key: 'MotifyUserAccount',
    isShow: true,
  },
  {
    title: <FormattedMessage id="Share.CommonKeys.Actions" />,
    key: 'actions',
    dataIndex: 'actions',
    fixed: 'right',
    render: (_, record) => {
      return (
        <EditRedEnvelopeButton RedBagId={record.RedBagId}>
          <FormattedMessage id="Share.ActionButton.Edit" />
        </EditRedEnvelopeButton>
      );
    },
  },
];
