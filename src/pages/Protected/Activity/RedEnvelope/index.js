import React, { useState, lazy, Suspense, createContext } from 'react'
import { FormattedMessage } from 'react-intl'
import { Tabs, Spin } from 'antd'
import PageHeaderTab from 'components/PageHeaderTab'

const { TabPane } = Tabs

export const PageContext = createContext()

const ManagementAsync = lazy(() => import('./Management'))
const RecordAsync = lazy(() => import('./Record'))

export default function MemberLevels() {
    const [tabKey, setTabKey] = useState('management')
    const [detailID, setDetailID] = useState()

    const changeToDetailTab = ID => {
        setDetailID(ID)
        setTabKey('record')
    }

    return (
        <>
            <PageHeaderTab
                defaultActiveKey={tabKey}
                activeKey={detailID ? 'record' : tabKey}
                onChange={key => {
                    setDetailID(null)
                    setTabKey(key)
                }}
            >
                <TabPane
                    tab={
                        <FormattedMessage id="PageOperationRedEnvelope.Tabs.Management" />
                    }
                    key="management"
                />
                <TabPane
                    tab={
                        <FormattedMessage id="PageOperationRedEnvelope.Tabs.Record" />
                    }
                    key="record"
                />
            </PageHeaderTab>
            <Suspense fallback={<Spin />}>
                <PageContext.Provider value={{ changeToDetailTab, detailID }}>
                    {tabKey === 'management' && <ManagementAsync />}
                    {tabKey === 'record' && <RecordAsync />}
                </PageContext.Provider>
            </Suspense>
        </>
    )
}
