import React, { useEffect } from 'react';
import { Form, Row, Col, Tag, Typography, Input, DatePicker, Space } from 'antd';
import { FormattedMessage } from 'react-intl';
import QueryButton from 'components/FormActionButtons/Query';
import { FORMAT, DEFAULT } from 'constants/dateConfig';
import { getISODateTimeString } from 'mixins/dateTime';

const { useForm } = Form;
const { Text } = Typography;
const { RANGE } = DEFAULT;

const CONDITION_INITIAL_VALUE = {
  Time: RANGE.FROM_TODAY_TO_TODAY,
  MemberName: null,
  IpAddress: null,
  DeviceID: null,
};

function Condition({ onUpdate, onReady }) {
  const [form] = useForm();
  const { RangePicker } = DatePicker;

  useEffect(() => {
    onUpdate(resultCondition(form.getFieldsValue()));
    onReady(true);
  }, []);

  function dateQueryRange(range) {
    return [];
  }

  function resultCondition({ Time, ...restValues }) {
    const [StartTime, EndTime] = Time || [null, null];
    return {
      StartTime: getISODateTimeString(StartTime),
      EndTime: getISODateTimeString(EndTime),
      ...restValues,
    };
  }

  function onFinish(values) {
    onUpdate.bySearch(resultCondition(values));
  }

  return (
    <Form form={form} initialValues={CONDITION_INITIAL_VALUE} onFinish={onFinish}>
      <Row gutter={[16, 8]}>
        <Col sm={8}>
          <Form.Item
            label={<FormattedMessage id="Share.QueryCondition.DateRange" description="時間區間" />}
            name="Time"
          >
            <RangePicker
              showTime
              style={{ width: '100%' }}
              format={FORMAT.ISO}
              renderExtraFooter={() => (
                <Space>
                  <Tag
                    onClick={() => {
                      form.setFieldsValue({
                        Time: RANGE.FROM_TODAY_TO_TODAY,
                      });
                    }}
                    color="blue"
                  >
                    <FormattedMessage id="Share.ActionButton.Today" description="今日" />
                  </Tag>
                  <Tag
                    onClick={() => {
                      form.setFieldsValue({
                        Time: RANGE.FROM_A_MONTH_TO_TODAY,
                      });
                    }}
                    color="blue"
                  >
                    <FormattedMessage id="Share.ActionButton.Nearly30Days" description="近30日" />
                  </Tag>
                  <Text>
                    <FormattedMessage
                      id="Share.Notify.BePatient30Day"
                      description="提醒：如查詢 30 日前資料，請耐心等待"
                    />
                  </Text>
                </Space>
              )}
            />
          </Form.Item>
        </Col>
        <Col sm={8}>
          <Form.Item
            label={<FormattedMessage id="Share.QueryCondition.MemberName" description="会员帐号" />}
            name="MemberName"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={8}>
          <Form.Item
            label={<FormattedMessage id="Share.QueryCondition.IpAddress" description="註冊時間" />}
            name="IpAddress"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={8}>
          <Form.Item
            label={<FormattedMessage id="Share.QueryCondition.DeviceID" description="裝置ID" />}
            name="DeviceID"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={16} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
