import React from 'react';
import { AndroidOutlined, AppleOutlined, MobileOutlined, DesktopOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import DateWithFormat from 'components/DateWithFormat';
import { NO_DATA } from 'constants/noData';

const device = [<DesktopOutlined />, <AndroidOutlined />, <AppleOutlined />, <MobileOutlined />];

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="Share.FormItem.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record) => {
      return <OpenMemberDetailButton memberId={record.MemberId} memberName={record.MemberName} />;
    },
  },
  {
    title: <FormattedMessage id="Share.FormItem.IPAddress" description="Ip位置" />,
    dataIndex: 'IPAddress',
    key: 'IPAddress',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.IPRegionName" description="IP地區" />,
    dataIndex: 'IPRegionName',
    key: 'IPRegionName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => (text ? text : NO_DATA),
  },
  {
    title: <FormattedMessage id="Share.FormItem.Device" description="裝置" />,
    dataIndex: 'Device',
    key: 'Device',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => device[text],
  },
  {
    title: <FormattedMessage id="Share.FormItem.DeviceID" description="裝置ID" />,
    dataIndex: 'DeviceID',
    key: 'DeviceID',
    isShow: true,
    render: (text, record) => (record.Device === 0 || record.Device === 3 ? '' : text),
  },
  {
    title: <FormattedMessage id="Share.FormItem.BrowserVersion" description="瀏覽器/APP版本" />,
    dataIndex: 'BrowserVersion',
    key: 'BrowserVersion',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => (text ? text : NO_DATA),
  },
  {
    title: <FormattedMessage id="Share.FormItem.SystemVersion" description="系統版本" />,
    dataIndex: 'SystemVersion',
    key: 'SystemVersion',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.Domain" description="登入網址" />,
    dataIndex: 'Domain',
    key: 'Domain',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.OperatingResult" description="操作結果" />,
    dataIndex: 'OperatingResult',
    key: 'OperatingResult',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.OperatingTime" description="操作時間" />,
    dataIndex: 'OperatingTime',
    key: 'OperatingTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: time => <DateWithFormat time={time} />,
  },
];
