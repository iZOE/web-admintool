import React, { createContext } from 'react'
import { Result, Space } from 'antd'
import { FormattedMessage } from 'react-intl'
import uid from 'utils/uid'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import Permission from 'components/Permission'
import DataTable from 'components/DataTable'
import ReportScaffold from 'components/ReportScaffold'
import ExportReportButton from 'components/ExportReportButton'
import * as PERMISSION from 'constants/permissions'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'

const { MEMBERS_LOGS_VIEW, MEMBERS_LOGS_EXPORT } = PERMISSION

export const PageContext = createContext()

const PageView = () => {
  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    onReady,
    boundedMutate,
  } = useGetDataSourceWithSWR({
    url: '/api/MemberPlayLog/Search',
    autoFetch: true,
  })
  return (
    <PageContext.Provider value={{ boundedMutate, onUpdateCondition }}>
      <Permission
        isPage
        functionIds={[MEMBERS_LOGS_VIEW]}
        failedRender={
          <Result
            status="warning"
            title={<FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />}
            subTitle={
              <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
            }
          />
        }
      >
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢結果"
                />
              }
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource?.List}
              total={dataSource?.TotalCount}
              rowKey={record => `${record.MemberName}-${uid()}`}
              extendArea={
                <Space>
                  <Permission functionIds={[MEMBERS_LOGS_EXPORT]}>
                    <ExportReportButton
                      condition={condition}
                      actionUrl="/api/MemberPlayLog/Export"
                    />
                  </Permission>
                </Space>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
      </Permission>
    </PageContext.Provider>
  )
}

export default PageView
