import React, { useState, forwardRef, useContext, useEffect } from 'react'
import {
    Drawer,
    Form,
    Button,
    message,
    Row,
    Col,
    Input,
    Radio,
    Descriptions,
    Typography,
    Divider,
    Tooltip,
    Space,
} from 'antd'
import { QuestionCircleOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import axios from 'axios'
import caller from 'utils/fetcher'
import { PageContext } from '../index'
import useSWR from 'swr'

const { useForm } = Form

export function TransferDrawer({}, ref) {
    const intl = useIntl()
    const [form] = useForm()
    const { getFieldValue } = form

    const unknownErrMsg = intl.formatMessage({
        id: 'Share.ErrorMessage.UnknownError',
        description: '不明错误',
    })

    const [visible, setVisible] = useState(false)
    const [sourceMemberName, setSourceMemberName] = useState()
    const [targetMember, setTargetMember] = useState()
    const [transferCheckError, setTransferCheckError] = useState(false)
    const [transferCheckErrMsg, setTransferCheckErrMsg] = useState(null)
    const [isSubmitable, setIsSubmitable] = useState(false)

    const { boundedMutate } = useContext(PageContext)

    const { data: sourceMember } = useSWR(
        sourceMemberName &&
            `/api/Affiliate/Transfer/Source/${sourceMemberName}`,
        url => axios(url).then(res => res && res.data.Data),
    )

    ref.current = {
        onOpenDrawer,
    }

    function onOpenDrawer({ record }) {
        setSourceMemberName(record.MemberName)
        setVisible(true)
    }

    function onCloseDrawer() {
        form.resetFields()
        setVisible(false)
        setTransferCheckError(false)
        setTargetMember(null)
    }

    const transferCheck = value => {
        setTransferCheckErrMsg(null)

        if (value) {
            const transferData = {
                SourceMemberName: sourceMemberName,
                TargetMemberName: value,
                TransferMode: getFieldValue('TopMemberName'),
            }
            caller({
                method: 'post',
                endpoint: '/api/Affiliate/Transfer/Check',
                body: transferData,
            })
                .then(() => {
                    getTransferTarget(transferData)
                    setTransferCheckError(false)
                })
                .catch(err => {
                    const msg = err.data.data.ResponseMessage
                        ? err.data.data.ResponseMessage
                        : unknownErrMsg
                    setTransferCheckErrMsg(msg)
                    setTransferCheckError(true)
                })
        }
    }

    function getTransferTarget(transferData) {
        setTransferCheckErrMsg(null)

        caller({
            method: 'get',
            endpoint: `/api/Affiliate/Transfer/Target/${transferData.TargetMemberName}`,
            body: transferData,
        })
            .then(res => {
                setIsSubmitable(true)
                setTargetMember(res.Data)
            })
            .catch(err => {
                const msg = err.data.data.ResponseMessage
                    ? err.data.data.ResponseMessage
                    : unknownErrMsg
                setTransferCheckErrMsg(msg)
                setTransferCheckError(true)
            })
    }

    function onFinish(values) {
        caller({
            method: 'put',
            endpoint: '/api/Affiliate/Transfer',
            body: {
                SourceMemberName: sourceMemberName,
                ...values,
            },
        })
            .then(() => {
                message.success(
                    intl.formatMessage({
                        id: 'Share.UIResponse.SaveSuccess',
                        description: '编辑成功',
                    }),
                    10,
                )
                onCloseDrawer()
                boundedMutate()
            })
            .catch(() => {
                message.warning(unknownErrMsg, 10)
            })
    }

    useEffect(() => {
        form.validateFields(['TargetMemberName'])
    }, [form, transferCheckError])

    if (!sourceMember) return null
    return (
        <Drawer
            width={700}
            headerStyle={{ border: 'none' }}
            title={
                <FormattedMessage id="PageAffiliatesManagement.Drawer.Title" />
            }
            visible={visible}
            onClose={onCloseDrawer}
            footer={
                <div
                    style={{
                        textAlign: 'right',
                    }}
                >
                    <Button onClick={onCloseDrawer} style={{ marginRight: 8 }}>
                        <FormattedMessage id="Share.ActionButton.Cancel" />
                    </Button>
                    <Button
                        type="primary"
                        disabled={!isSubmitable}
                        onClick={() => form.submit()}
                    >
                        <FormattedMessage id="Share.ActionButton.Confirm" />
                    </Button>
                </div>
            }
        >
            <Form
                form={form}
                wrapperCol={{ xs: { span: 24 }, sm: { span: 16 } }}
                onFinish={onFinish}
                initialValues={{ TopMemberName: 1 }}
            >
                <Descriptions column={2} label="">
                    <Descriptions.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesManagement.FormItems.SourceMemberName"
                                description="轉移代理"
                            />
                        }
                    >
                        <Typography.Text>
                            {sourceMember.MemberName}
                        </Typography.Text>
                    </Descriptions.Item>
                    <Descriptions.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesManagement.FormItems.TeamFollowingCount"
                                description="團隊人數"
                            />
                        }
                    >
                        <Typography.Text>
                            {sourceMember.TeamFollowingCount}
                        </Typography.Text>
                    </Descriptions.Item>
                    <Descriptions.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesManagement.FormItems.AffiliateLevelName"
                                description="代理等級"
                            />
                        }
                    >
                        <Typography.Text>
                            {sourceMember.AffiliateLevelName}
                        </Typography.Text>
                    </Descriptions.Item>
                    <Descriptions.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesManagement.FormItems.ReturnPoint"
                                description="返點等級"
                            />
                        }
                    >
                        <Typography.Text>
                            {sourceMember.ReturnPoint}
                        </Typography.Text>
                    </Descriptions.Item>
                </Descriptions>

                <Divider dashed />

                <Row>
                    <Col sm={24}>
                        <Form.Item
                            label={
                                <FormattedMessage
                                    id="PageAffiliatesManagement.FormItems.TransferMode"
                                    description="轉移模式"
                                />
                            }
                            name="TransferMode"
                            rules={[
                                {
                                    required: true,
                                    message: (
                                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                                    ),
                                },
                            ]}
                        >
                            <Radio.Group>
                                <Radio value={1}>
                                    <FormattedMessage
                                        id="PageAffiliatesManagement.QueryCondition.SelectItem.TransferModes.1"
                                        description="只轉移下級"
                                    />
                                </Radio>
                                <Radio value={2}>
                                    <FormattedMessage
                                        id="PageAffiliatesManagement.QueryCondition.SelectItem.TransferModes.2"
                                        description="代理線轉移"
                                    />
                                </Radio>
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col sm={24}>
                        <Form.Item
                            label={
                                <Space>
                                    <FormattedMessage
                                        id="PageAffiliatesManagement.FormItems.TargetMember"
                                        description="目標代理線"
                                    />
                                    <Tooltip
                                        title={
                                            <FormattedMessage
                                                id="PageAffiliatesManagement.Notes.ForTargetMemberName"
                                                description="請先檢查代理帳號是否符合"
                                            />
                                        }
                                    >
                                        <QuestionCircleOutlined />
                                    </Tooltip>
                                </Space>
                            }
                            name="TargetMemberName"
                            rules={[
                                {
                                    required: true,
                                    message: (
                                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                                    ),
                                },
                            ]}
                        >
                            <Input.Search
                                placeholder={intl.formatMessage({
                                    id:
                                        'PageAffiliatesManagement.FormItems.Placeholder.PlsEnterAffiliates',
                                    description: '请输入代理帳號',
                                })}
                                enterButton={
                                    <FormattedMessage
                                        id="PageAffiliatesManagement.ActionButton.CheckAffiliateAccount"
                                        description="檢查代理帳號"
                                    />
                                }
                                onSearch={transferCheck}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                {transferCheckError && (
                    <Typography.Text type="danger">
                        {transferCheckErrMsg}
                    </Typography.Text>
                )}
                {targetMember && (
                    <Descriptions column={2} label="">
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageAffiliatesManagement.FormItems.TargetMemberName"
                                    description="代理名稱"
                                />
                            }
                        >
                            <Typography.Text>
                                {targetMember.MemberName}
                            </Typography.Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageAffiliatesManagement.FormItems.TeamFollowingCount"
                                    description="團隊人數"
                                />
                            }
                        >
                            <Typography.Text>
                                {targetMember.TeamFollowingCount}
                            </Typography.Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageAffiliatesManagement.FormItems.AffiliateLevelName"
                                    description="代理等級"
                                />
                            }
                        >
                            <Typography.Text>
                                {targetMember.AffiliateLevelName}
                            </Typography.Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageAffiliatesManagement.FormItems.ReturnPoint"
                                    description="返點等級"
                                />
                            }
                        >
                            <Typography.Text>
                                {targetMember.ReturnPoint}
                            </Typography.Text>
                        </Descriptions.Item>
                    </Descriptions>
                )}
            </Form>
        </Drawer>
    )
}

export default forwardRef(TransferDrawer)
