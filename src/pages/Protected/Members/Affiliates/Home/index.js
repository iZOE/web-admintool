import React, { createContext, createRef } from 'react'
import { Space, Result } from 'antd'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import Permission from 'components/Permission'
import ReportScaffold from 'components/ReportScaffold'
import DataTable from 'components/DataTable'
import AddMemberButton from 'components/Member/AddMemberButton'
import ExportReportButton from 'components/ExportReportButton'
import * as PERMISSION from 'constants/permissions'
import { MEMBER_STYLE } from 'constants/memberDetail/memberType'
import Condition from './Condition'
import TransferDrawer from './TransferDrawer'
import { COLUMNS_CONFIG } from './datatableConfig'

const {
    MEMBERS_AFFILIATES_VIEW,
    MEMBERS_AFFILIATES_EXPORT,
    MEMBERS_AFFILIATES_CREATE,
} = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    let transferDrawerRef = createRef()

    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
        onReady,
        boundedMutate,
    } = useGetDataSourceWithSWR({
        url: '/api/Affiliate/Search',
        defaultSortKey: 'CreateTime',
        autoFetch: true,
    })
    return (
        <PageContext.Provider
            value={{ boundedMutate, transferDrawerRef, onUpdateCondition }}
        >
            <Permission
                isPage
                functionIds={[MEMBERS_AFFILIATES_VIEW]}
                failedRender={
                    <Result
                        status="warning"
                        title={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                        }
                        subTitle={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                        }
                    />
                }
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢結果"
                                />
                            }
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            extendArea={
                                <Space>
                                    <Permission
                                        functionIds={[
                                            MEMBERS_AFFILIATES_EXPORT,
                                        ]}
                                    >
                                        <ExportReportButton
                                            condition={condition}
                                            actionUrl="/api/Affiliate/Export"
                                        />
                                    </Permission>
                                    <Permission
                                        functionIds={[
                                            MEMBERS_AFFILIATES_CREATE,
                                        ]}
                                    >
                                        <AddMemberButton
                                            memberStyle={MEMBER_STYLE.AGENT}
                                        />
                                    </Permission>
                                </Space>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
            <TransferDrawer ref={transferDrawerRef} />
        </PageContext.Provider>
    )
}

export default PageView
