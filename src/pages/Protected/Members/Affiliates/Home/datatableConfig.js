import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { Link } from 'react-router-dom';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import DateWithFormat from 'components/DateWithFormat';
import StatusBadge from 'components/Member/StatusBadge';
import OnlineStatusBadge from 'components/Member/OnlineStatusBadge';
import ActionButtons from './ActionButtons';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="Share.FormItem.MemberName" description="会员帐号" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => {
      return <OpenMemberDetailButton memberId={record.MemberId} memberName={record.MemberName} />;
    },
  },
  {
    title: <FormattedMessage id="Share.FormItem.NickName" description="名称" />,
    dataIndex: 'NickName',
    key: 'NickName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.MemberBalanceAmount" description="余额" />,
    dataIndex: 'MemberBalanceAmount',
    key: 'MemberBalanceAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <FormattedNumber value={Math.floor(text * 100) / 100} minimumFractionDigits="2" />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.ReturnPoint" description="返点等级" />,
    dataIndex: 'ReturnPoint',
    key: 'ReturnPoint',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.MemberType" description="类型" />,
    dataIndex: 'MemberType',
    key: 'MemberType',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.MemberLevelName" description="等级" />,
    dataIndex: 'MemberLevelName',
    key: 'MemberLevelName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.AffiliateLevel" description="代理等级" />,
    dataIndex: 'AffiliateLevel',
    key: 'AffiliateLevel',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.ParentMemberName" description="所属上级" />,
    dataIndex: 'ParentMemberName',
    key: 'ParentMemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, _2) =>
      text ? <OpenMemberDetailButton memberId={record.ParentMemberId} memberName={record.ParentMemberName} /> : NO_DATA,
  },
  {
    title: <FormattedMessage id="Share.FormItem.FollowingCount" description="直属下级" />,
    dataIndex: 'FollowingCount',
    key: 'FollowingCount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.TeamFollowingCount" description="团队人数" />,
    dataIndex: 'TeamFollowingCount',
    key: 'TeamFollowingCount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.ActionButton.ViewAffiliates" description="查看代理报表" />,
    dataIndex: 'ViewAffiliates',
    key: 'ViewAffiliates',
    isShow: true,
    render: (text, record, index) => (
      <Link to={`/operation-report/affiliate?m=${record.MemberName}`}>
        <FormattedMessage id="PageAffiliatesManagement.ActionButton.ViewAffiliates" description="代理报表" />
      </Link>
    ),
  },
  {
    title: <FormattedMessage id="Share.FormItem.SignupTime" description="注册时间" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.PreLoginTime" description="最后登入时间" />,
    dataIndex: 'PreLoginTime',
    key: 'PreLoginTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { PreLoginTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.CurrLoginIp" description="登入IP" />,
    dataIndex: 'CurrLoginIP',
    key: 'CurrLoginIP',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CurrLoginIP }, _2) => (CurrLoginIP ? CurrLoginIP : NO_DATA),
  },
  {
    title: <FormattedMessage id="Share.FormItem.Area" description="地區" />,
    dataIndex: 'CurrLoginIPInfo',
    key: 'CurrLoginIPInfo',
    render: (_1, { CurrLoginIPInfo: info }, _2) =>
      info ? (
        <span>
          {!info?.Country || info?.Country === '' ? NO_DATA : info.Country}|
          {!info?.RegionName || info?.RegionName === '' ? NO_DATA : info.RegionName}|
          {!info?.City || info?.City === '' ? NO_DATA : info.City}|
          {!info?.DeviceTypeName || info?.DeviceTypeName === '' ? NO_DATA : info.DeviceTypeName}
        </span>
      ) : (
        NO_DATA
      ),
  },
  {
    title: <FormattedMessage id="Share.FormItem.Status" description="状态" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { Status, StatusText }, _2) => <StatusBadge status={Status} text={StatusText} />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.IsOnline" description="在线状态" />,
    dataIndex: 'OnlineStatus',
    key: 'OnlineStatus',
    isShow: true,
    render: (_1, { OnlineStatus, OnlineStatusText }, _2) => (
      <OnlineStatusBadge status={OnlineStatus} text={OnlineStatusText} />
    ),
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'edit',
    key: 'edit',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => <ActionButtons record={record} />,
  },
];
