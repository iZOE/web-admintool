import React, { useContext, useState } from 'react'
import { Popconfirm, Button, message, Modal, Form, Input } from 'antd'
import { UnlockOutlined, LockOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import { MEMBER_STATUS } from 'constants/memberDetail/memberStatus'
import { PageContext } from '../../index'
import caller from 'utils/fetcher'

export default function ToggleFreezeButton({ memberId, status }) {
    const { onUpdateCondition } = useContext(PageContext)
    const isCurrentMemberStatusNormal = status === MEMBER_STATUS.NORMAL
    const [visible, setVisible] = useState(false)
    const intl = useIntl()

    const successMsg = useIntl().formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })
    const errorMsg = useIntl().formatMessage({
        id: 'Share.ErrorMessage.UnknownError',
    })

    const [form] = Form.useForm()
    const onOk = () => form.submit()
    const onCancel = () => setVisible(false)
    const onConfirm = value => {
        const payload = {
            MemberId: memberId,
            LockType: isCurrentMemberStatusNormal
                ? MEMBER_STATUS.LOCK_BY_ADMINTOOL
                : MEMBER_STATUS.NORMAL,
        }
        if (value.Remarks) {
            payload.Remarks = value.Remarks
        }
        caller({
            method: 'patch',
            endpoint: '/api/Affiliate/Freeze',
            body: payload,
        })
            .then(() => {
                message.success(successMsg, 5)
            })
            .then(() => {
                onUpdateCondition()
                setVisible(false)
                form.resetFields()
            })
            .catch(err => {
                console.error('Toggle Freeze Affiliate Status Error: ', err)
                message.error(errorMsg, 5)
            })
    }

    return (
        <>
            {isCurrentMemberStatusNormal ? (
                <>
                    <Button
                        type="primary"
                        style={{
                            background: '#f50',
                            borderColor: '#f50',
                        }}
                        icon={
                            isCurrentMemberStatusNormal ? (
                                <LockOutlined style={{ marginRight: 6 }} />
                            ) : (
                                <UnlockOutlined style={{ marginRight: 6 }} />
                            )
                        }
                        onClick={() => setVisible(true)}
                    >
                        <FormattedMessage id="Share.ActionButton.Freeze" />
                    </Button>
                    <Modal
                        title={
                            <FormattedMessage id="PageMember.FreezeModal.Title" />
                        }
                        visible={visible}
                        onOk={onOk}
                        onCancel={onCancel}
                        okText={
                            <FormattedMessage id="Share.ActionButton.Confirm" />
                        }
                        cancelText={
                            <FormattedMessage id="Share.ActionButton.Cancel" />
                        }
                    >
                        <FormattedMessage id="PageMember.Messages.ConfirmToFreeze" />
                        <Form
                            form={form}
                            layout="vertical"
                            onFinish={onConfirm}
                        >
                            <Form.Item
                                name="Remarks"
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Input
                                    type="text"
                                    placeholder={intl.formatMessage({
                                        id:
                                            'PageMember.FreezeModal.EnterRemark',
                                    })}
                                />
                            </Form.Item>
                        </Form>
                    </Modal>
                </>
            ) : (
                <Popconfirm
                    placement="right"
                    title={
                        <FormattedMessage id="PageMember.Messages.ConfirmToUnFreeze" />
                    }
                    onConfirm={onConfirm}
                    okText={
                        <FormattedMessage id="Share.ActionButton.Confirm" />
                    }
                    cancelText={
                        <FormattedMessage id="Share.ActionButton.Cancel" />
                    }
                >
                    <Button
                        type="primary"
                        style={{
                            background: '#87d068',
                            borderColor: '#87d068',
                        }}
                        icon={
                            isCurrentMemberStatusNormal ? (
                                <LockOutlined style={{ marginRight: 6 }} />
                            ) : (
                                <UnlockOutlined style={{ marginRight: 6 }} />
                            )
                        }
                    >
                        <FormattedMessage id="Share.ActionButton.UnFreeze" />
                    </Button>
                </Popconfirm>
            )}
        </>
    )
}
