import React, { useContext } from 'react'
import { Popconfirm, Button, message } from 'antd'
import { KeyOutlined } from '@ant-design/icons'
import { useIntl, FormattedMessage } from 'react-intl'
import { MEMBER_STATUS } from 'constants/memberDetail/memberStatus'
import caller from 'utils/fetcher'
import { PageContext } from '../../index'

export default function LockButton({ memberId }) {
    const { onUpdateCondition } = useContext(PageContext)

    const successMsg = useIntl().formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })
    const errorMsg = useIntl().formatMessage({
        id: 'Share.ErrorMessage.UnknownError',
    })

    const onConfirm = () => {
        const payload = {
            MemberId: memberId,
            LockType: MEMBER_STATUS.NORMAL,
        }

        caller({
            method: 'patch',
            endpoint: '/api/Affiliate/Lock',
            body: payload,
        })
            .then(() => {
                message.success(successMsg, 5)
            })
            .then(() => {
                onUpdateCondition()
            })
            .catch(err => {
                console.error('UnLock Affiliate Status Error: ', err)
                message.error(errorMsg, 5)
            })
    }

    return (
        <Popconfirm
            placement="right"
            title={
                <FormattedMessage id="PageMember.Messages.ConfirmToUnlock" />
            }
            onConfirm={onConfirm}
            okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
            cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
        >
            <Button
                type="primary"
                icon={<KeyOutlined style={{ marginRight: 6 }} />}
            >
                <FormattedMessage id="Share.ActionButton.UnLock" />
            </Button>
        </Popconfirm>
    )
}
