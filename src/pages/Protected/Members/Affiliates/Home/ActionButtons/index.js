import React, { useContext } from 'react'
import { Space, Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'
import LockButton from './LockButton'
import ToggleFreezeButton from './ToggleFreezeButton'
import { MODE } from 'constants/mode'
import { PageContext } from '../index'
import { MEMBER_STATUS } from 'constants/memberDetail/memberStatus'
import usePermission from 'hooks/usePermission'

const {
    MEMBERS_AFFILIATES_TRANSFER_AFFILIATE,
    MEMBERS_AFFILIATES_EDIT,
    MEMBERS_AFFILIATES_UNLOCK,
    MEMBERS_AFFILIATES_FREEZE,
} = PERMISSION

export default function ActionButtons({ record }) {
    const { transferDrawerRef } = useContext(PageContext)
    const [hasUnlockPermission, hasFreezePermission] = usePermission(
        MEMBERS_AFFILIATES_UNLOCK,
        MEMBERS_AFFILIATES_FREEZE
    )
    const currentMemberStatus = record && record.Lock
    const isUnlockable =
        hasUnlockPermission &&
        currentMemberStatus === MEMBER_STATUS.LOCK_BY_MEMBER
    const isToggleFreezeable =
        hasFreezePermission &&
        (currentMemberStatus === MEMBER_STATUS.NORMAL ||
            currentMemberStatus === MEMBER_STATUS.LOCK_BY_ADMINTOOL)

    return (
        <Space>
            <Permission functionIds={[MEMBERS_AFFILIATES_TRANSFER_AFFILIATE]}>
                <Button
                    type="link"
                    onClick={() =>
                        transferDrawerRef.current.onOpenDrawer({
                            record,
                        })
                    }
                >
                    <FormattedMessage id="Share.ActionButton.Transfer" />
                </Button>
            </Permission>

            <Permission functionIds={[MEMBERS_AFFILIATES_EDIT]}>
                <OpenMemberDetailButton
                    memberId={record.MemberId}
                    memberName={record.MemberName}
                    displayText={
                        <FormattedMessage id="Share.ActionButton.Edit" />
                    }
                    mode={MODE.EDIT}
                    noDropdown
                />
            </Permission>

            {isUnlockable && <LockButton memberId={record.MemberId} />}
            {isToggleFreezeable && (
                <ToggleFreezeButton
                    memberId={record.MemberId}
                    status={currentMemberStatus}
                />
            )}
        </Space>
    )
}
