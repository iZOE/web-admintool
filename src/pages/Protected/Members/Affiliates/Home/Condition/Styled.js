import styled from 'styled-components'

export const Wrapper = styled.div`
    padding: 24px 24px;
    background: #ffffff;

    .textRight {
        text-align: right;
    }
`
