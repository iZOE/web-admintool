import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import FormItemCheckboxWithLabel from 'components/FormItems/CheckboxWithLabel'
import FormItemAdvanceQuery from 'components/FormItems/AdvanceQuery'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { MEMBER_STYLE } from 'constants/memberDetail/memberType'

const { useForm } = Form
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    CreateTime: RANGE.FROM_INITIAL_TO_TODAY,
    DisableMemberIsNotShown: false,
    MemberTypeId: MEMBER_STYLE.AGENT,
    PreLoginTime: RANGE.FROM_A_MONTH_TO_TODAY,
}

function Condition({ onUpdate, onReady }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()
    const { RangePicker } = DatePicker

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ CreateTime, PreLoginTime, ...restValues }) {
        const [CreateTimeStart, CreateTimeEnd] = CreateTime || [null, null]
        const [PreLoginTimeStart, PreLoginTimeEnd] = PreLoginTime || [
            null,
            null,
        ]
        return {
            CreateTimeStart: getISODateTimeString(CreateTimeStart),
            CreateTimeEnd: getISODateTimeString(CreateTimeEnd),
            PreLoginTimeStart: getISODateTimeString(PreLoginTimeStart),
            PreLoginTimeEnd: getISODateTimeString(PreLoginTimeEnd),
            ...restValues,
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.MemberType"
                                description="會員類型"
                            />
                        }
                        name="MemberTypeId"
                    >
                        <FormattedMessage
                            id={`Share.MemberType.${MEMBER_STYLE.AGENT}`}
                            description="代理"
                        />
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <FormItemMultipleSelect
                        checkAll
                        name="AffiliateLevelId"
                        label={
                            <FormattedMessage id="Share.QueryCondition.AffiliateLevel" />
                        }
                        url="/api/Option/AffiliateLevel?showAll=false"
                    />
                </Col>
                <Col sm={6}>
                    <FormItemsSimpleSelect
                        needAll
                        url="/api/Option/MemberLevel"
                        name="MemberLevelId"
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.MemberLevel"
                                description="會員等級"
                            />
                        }
                    />
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesManagement.QueryCondition.CreateTime"
                                description="註冊時間"
                            />
                        }
                        name="CreateTime"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesManagement.QueryCondition.PreLoginTime"
                                description="登入時間"
                            />
                        }
                        name="PreLoginTime"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemAdvanceQuery />
                </Col>
                <Col sm={4}>
                    <FormItemCheckboxWithLabel
                        name="DisableMemberIsNotShown"
                        formattedMessageId="Share.QueryCondition.IsOnlyEnabled"
                    />
                </Col>
                <Col sm={4} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
