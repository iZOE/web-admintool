import React, { Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'
import { MEMBER_STYLE } from 'constants/memberDetail/memberType'
import Home from './Home'

const CreateMemberAsync = React.lazy(() =>
    import('../../Members/Members/Create')
)

export default function Affiliates() {
    return (
        <Suspense>
            <Switch>
                <Route exact path="/members/affiliates">
                    <Home />
                </Route>
                <Route path="/members/affiliates/create">
                    <CreateMemberAsync memberStyle={MEMBER_STYLE.AGENT} />
                </Route>
            </Switch>
        </Suspense>
    )
}
