import React from 'react'
import { Form, Input } from 'antd'
import { FormattedMessage } from 'react-intl'

export default function ParentAccount() {
    return (
        <Form.Item
            name="ParentMemberName"
            label={<FormattedMessage id="MemberDetail.Fields.ParentAccount" />}
        >
            <Input />
        </Form.Item>
    )
}
