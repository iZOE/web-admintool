import React, { useContext } from 'react'
import { Form } from 'antd'
import { FormattedMessage } from 'react-intl'
import { UserContext } from 'contexts/UserContext'
import SelectWithAffiliateLevelList from 'components/Member/SelectWithAffiliateLevelList'
import { AGENT_TYPE } from 'constants/agentType'
import { FILTER_TYPE } from 'constants/memberDetail/affiliateFilterType'
import { MEMBER_STYLE } from 'constants/memberDetail/memberType'

const validatedAgentList = [
    AGENT_TYPE.CAILIFUN_II,
    AGENT_TYPE.XING_HUI,
    AGENT_TYPE.JIN_SHA,
    AGENT_TYPE.HL_101,
]

const getFilterTypeId = (agentId, memberStyleId) => {
    if (agentId === AGENT_TYPE.SHOU_MI) {
        return FILTER_TYPE.ONLY_AGENCY
    } else if (memberStyleId === MEMBER_STYLE.MEMBER) {
        return FILTER_TYPE.ONLY_MEMBER
    } else if (memberStyleId === MEMBER_STYLE.AGENT) {
        return FILTER_TYPE.ALL_AGENCY
    } else {
        return FILTER_TYPE.NONE
    }
}

export default function AffiliateLevel({ memberStyle }) {
    const { data: userData } = useContext(UserContext)
    const isShownAllAffiliateLevel = validatedAgentList.includes(
        userData.AgentId
    )

    const filterTypeId = getFilterTypeId(
        userData.AgentId,
        parseInt(memberStyle)
    )

    return (
        memberStyle && (
            <Form.Item
                label={
                    <FormattedMessage id="MemberDetail.Fields.AffiliateLevel" />
                }
                name="AffiliateLevelId"
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="Share.FormValidate.Required.Input" />
                        ),
                    },
                ]}
            >
                <SelectWithAffiliateLevelList
                    isShownAll={isShownAllAffiliateLevel}
                    filterType={filterTypeId}
                />
            </Form.Item>
        )
    )
}
