import React from 'react'
import BetStatusFormItem from 'components/Member/BetStatusFormItem'
import DepositStatusFormItem from 'components/Member/DepositStatusFormItem'
import TransferStatusFormItem from 'components/Member/TransferStatusFormItem'
import WithdrawalStatusFormItem from 'components/Member/WithdrawalStatusFormItem'

export default function FinancialPermissionSetting({ memberTypeId }) {
    return (
        <>
            <BetStatusFormItem />
            <DepositStatusFormItem />
            <TransferStatusFormItem memberTypeId={memberTypeId} />
            <WithdrawalStatusFormItem memberTypeId={memberTypeId} />
        </>
    )
}
