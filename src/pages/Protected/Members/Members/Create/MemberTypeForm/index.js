import React, { useContext, useState } from 'react'
import useSWR from 'swr'
import axios from 'axios'
import caller from 'utils/fetcher'
import { Form, Button, Row, Col, Skeleton, message } from 'antd'
import { useIntl, FormattedMessage } from 'react-intl'
import MemberType from './MemberType'
import AffiliateLevel from './AffiliateLevel'
import MemberAccount from './MemberAccount'
import Password from './Password'
import NickName from './NickName'
import AccountStatus from './AccountStatus'
import AllowSameMemberLevel from './AllowSameMemberLevel'
import ReturnPoint from './ReturnPoint'
import ParentAccount from './ParentAccount'
import FinancialPermissionSetting from './FinancialPermissionSetting'
import ThirdPartyTransferFormList from 'components/Member/ThirdPartyTransferFormList'
import { UserContext } from 'contexts/UserContext'
import { AGENT_TYPE } from 'constants/agentType'

import Remark from './Remark'
import {
    MEMBER_TYPE,
    AFFILIATE_LEVEL_TYPE,
    MEMBER_STYLE,
} from 'constants/memberDetail/memberType'

export default function MemberTypeForm({ memberStyle }) {
    let { data: GameTransferSetting } = useSWR(
        '/api/Option/GameExternalProviders',
        url =>
            axios(url).then(res => {
                const data = res.data.Data.map(
                    ({ GameProviderId, GameProviderName, Remark }) => {
                        return {
                            GameProviderId,
                            GameProviderText: Remark
                                ? Remark
                                : GameProviderName,
                            TransferLock: true,
                        }
                    }
                )
                return data
            })
    )

    const { data: userData } = useContext(UserContext)
    const isAgentShouMi = userData.AgentId === AGENT_TYPE.SHOU_MI
    const [isLoading, setIsLoading] = useState(false)

    const [form] = Form.useForm()
    const intl = useIntl()

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })
    const unknownErrorMsg = intl.formatMessage({
        id: 'Share.ErrorMessage.UnknownError',
    })
    const formItemLayout = {
        labelCol: {
            sm: { span: 24 },
            md: { span: 10 },
        },
        wrapperCol: {
            sm: { span: 24 },
            md: { span: 10 },
        },
    }

    const initialValues = {
        MemberTypeId: MEMBER_TYPE.DEFAULT,
        AffiliateLevelId:
            parseInt(memberStyle) === MEMBER_STYLE.MEMBER
                ? AFFILIATE_LEVEL_TYPE.MEMBER
                : AFFILIATE_LEVEL_TYPE.AGENCY,
        Status: true,
        ReturnPoint: 1960,
        IsDepositable: true,
        IsBetPlaceable: true,
        IsFundTransferable: true,
        IsWithdrawable: true,
        IsSameLevel: true,
        ParentMemberName: '',
        Remarks: '',
        GameTransferSetting,
    }

    const onValuesChange = ({ MemberTypeId }) => {
        if (MemberTypeId === MEMBER_TYPE.INTERNAL) {
            form.setFieldsValue({
                IsFundTransferable: false,
                IsWithdrawable: false,
            })
        }
    }

    const onFinish = values => {
        setIsLoading(true)
        const processedSettings = values.GameTransferSetting.map(
            ({ GameProviderId, TransferLock }) => {
                return {
                    GameProviderId,
                    TransferLock: !TransferLock,
                }
            }
        )

        const payload = {
            ...values,
            GameTransferSetting: processedSettings,
        }

        const endpoint =
            payload.AffiliateLevelId === AFFILIATE_LEVEL_TYPE.MEMBER
                ? '/api/Member/Create'
                : '/api/Affiliate/Create'

        caller({
            method: 'post',
            endpoint,
            body: payload,
        })
            .then(() => {
                message.success(successMsg)
            })
            .then(() => {
                form.resetFields()
            })
            .catch(err => {
                const errMsg = err.Message ? err.Message : unknownErrorMsg
                message.error(errMsg, 5)
                console.error('Fail To Add A New Member: ', err)
            })
            .finally(() => {
                setIsLoading(false)
            })
    }

    return GameTransferSetting ? (
        <Form
            initialValues={initialValues}
            onValuesChange={onValuesChange}
            form={form}
            onFinish={onFinish}
            {...formItemLayout}
            scrollToFirstError={true}
            style={{ margin: 24 }}
        >
            <Row gutter={24}>
                <Col span={13}>
                    <MemberType />
                    <MemberAccount />
                    <Password />
                    <NickName />
                    <AccountStatus />
                    <AffiliateLevel memberStyle={memberStyle} />
                    <Form.Item noStyle shouldUpdate>
                        {({ getFieldValue }) =>
                            getFieldValue('AffiliateLevelId') ===
                            AFFILIATE_LEVEL_TYPE.MEMBER ? null : (
                                <AllowSameMemberLevel />
                            )
                        }
                    </Form.Item>
                    <Form.Item noStyle shouldUpdate>
                        {({ getFieldValue }) => (
                            <ReturnPoint
                                affiliateLevelId={getFieldValue(
                                    'AffiliateLevelId'
                                )}
                                parentMemberName={getFieldValue(
                                    'ParentMemberName'
                                )}
                            />
                        )}
                    </Form.Item>
                    {!isAgentShouMi && <ParentAccount />}
                    <Remark />
                    <Form.Item noStyle shouldUpdate>
                        {({ getFieldValue }) => {
                            return (
                                <FinancialPermissionSetting
                                    memberTypeId={getFieldValue('MemberTypeId')}
                                />
                            )
                        }}
                    </Form.Item>
                </Col>
                <Col span={11}>
                    <ThirdPartyTransferFormList list={GameTransferSetting} />
                </Col>
                <Col span={24}>
                    <Form.Item noStyle>
                        <Button
                            style={{ display: 'block', margin: '0 auto' }}
                            type="primary"
                            htmlType="submit"
                            loading={isLoading}
                        >
                            <FormattedMessage id="Share.ActionButton.Submit" />
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    ) : (
        <Skeleton />
    )
}
