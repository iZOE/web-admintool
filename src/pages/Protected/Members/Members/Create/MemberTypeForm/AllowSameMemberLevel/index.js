import React from 'react'
import AllowSameMemberLevelFormItem from 'components/Member/AllowSameMemberLevelFormItem'

export default function AllowSameMemberLevel() {
    return <AllowSameMemberLevelFormItem />
}
