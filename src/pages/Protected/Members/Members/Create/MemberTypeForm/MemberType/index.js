import React from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Form, Spin } from 'antd'
import ListSelect from 'components/ListSelect'
import { FormattedMessage } from 'react-intl'

const MEMBER_TYPE_MODE = {
    NEW: 1, // 分內外部帳號
}

export default function MemberType({ onChange }) {
    let {
        data: memberTypeList,
    } = useSWR(`/api/Option/MemberType?modeId=${MEMBER_TYPE_MODE.NEW}`, url =>
        axios(url).then(res => res.data.Data)
    )

    return (
        <Form.Item
            label={<FormattedMessage id="MemberDetail.Fields.MemberType" />}
            name="MemberTypeId"
            hasFeedback
            rules={[
                {
                    required: true,
                    message: (
                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                    ),
                },
            ]}
        >
            {memberTypeList ? (
                <ListSelect list={memberTypeList} onChange={onChange} />
            ) : (
                <Spin />
            )}
        </Form.Item>
    )
}
