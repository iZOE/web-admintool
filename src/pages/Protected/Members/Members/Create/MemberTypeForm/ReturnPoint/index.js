import React, { useContext } from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Spin } from 'antd'
import { UserContext } from 'contexts/UserContext'
import { AGENT_TYPE } from 'constants/agentType'
import { RETURN_POINT_FIELD } from 'constants/memberDetail/validators'
import ReturnPointFormItem from 'components/Member/ReturnPointFormItem'

export default function ReturnPoint({ affiliateLevelId, parentMemberName }) {
    const { data: returnPointRange } = useSWR(
        affiliateLevelId
            ? [
                  '/api/Member/ReturnPointRange',
                  affiliateLevelId,
                  parentMemberName,
              ]
            : null,
        url =>
            axios({
                url,
                method: 'post',
                data: {
                    AffiliateLevelId: affiliateLevelId,
                    ParentMemberName: parentMemberName,
                },
            }).then(res => res && res.data.Data)
    )
    const { data: userData } = useContext(UserContext)
    const isDisabled = userData.AgentId === AGENT_TYPE.SHOU_MI

    return returnPointRange ? (
        <ReturnPointFormItem
            max={returnPointRange[RETURN_POINT_FIELD.MAX]}
            min={returnPointRange[RETURN_POINT_FIELD.MIN]}
            isDisabled={isDisabled}
        />
    ) : (
        <Spin />
    )
}
