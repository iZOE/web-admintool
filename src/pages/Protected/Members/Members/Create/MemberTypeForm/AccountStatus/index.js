import React from 'react'
import AccountStatusFormItem from 'components/Member/AccountStatusFormItem'

export default function AccountStatus() {
    return <AccountStatusFormItem />
}
