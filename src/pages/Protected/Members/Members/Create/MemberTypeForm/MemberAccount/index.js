import React from 'react'
import { Form, Input } from 'antd'
import { FormattedMessage } from 'react-intl'
import { MEMBER_ACCOUNT } from 'constants/memberDetail/validators'
import caller from 'utils/fetcher'

let delayTimer

const checkMemberExist = MemberName =>
    caller({
        endpoint: '/api/Member/CheckMemberExists',
        method: 'post',
        body: { MemberName },
    })

const rules = [
    {
        required: true,
        message: <FormattedMessage id="Share.FormValidate.Required.Input" />,
    },
    {
        min: MEMBER_ACCOUNT.MIN,
        message: (
            <FormattedMessage
                id="Share.FormValidate.Length.Minimum"
                values={{ min: MEMBER_ACCOUNT.MIN }}
            />
        ),
    },
    {
        max: MEMBER_ACCOUNT.MAX,
        message: (
            <FormattedMessage
                id="Share.FormValidate.Length.Maximum"
                values={{ max: MEMBER_ACCOUNT.MAX }}
            />
        ),
    },
    {
        validator(_1, MemberName) {
            const isMemberAccountValidated = MEMBER_ACCOUNT.PATTERN.test(
                MemberName
            )
            if (!isMemberAccountValidated) {
                return Promise.reject(
                    <FormattedMessage id="Share.FormValidate.Pattern.WordNumber" />
                )
            } else {
                clearTimeout(delayTimer)
                return new Promise((resolve, reject) => {
                    delayTimer = setTimeout(() => {
                        return checkMemberExist(MemberName)
                            .then(res => res.Status)
                            .catch(error => error?.data?.data?.Status)
                            .then(res =>
                                res
                                    ? reject(
                                          <FormattedMessage id="PageSystemUsers.FormValidate.AccountIsExist" />
                                      )
                                    : resolve(true)
                            )
                    }, 500)
                })
            }
        },
    },
]

export default function MemberAccount() {
    return (
        <Form.Item
            label={<FormattedMessage id="MemberDetail.Fields.MemberAccount" />}
            name="MemberName"
            rules={rules}
            hasFeedback
            // validateTrigger={'onBlur'}
        >
            <Input />
        </Form.Item>
    )
}
