import React from 'react'
import { Space } from 'antd'
import LayoutPageBody from 'layouts/Page/Body'
import MemberTypeForm from './MemberTypeForm'
import { FormattedMessage } from 'react-intl'

export default function CreateMember({ memberStyle }) {
  return (
    <LayoutPageBody
      title={
        <Space size={0}>
          <FormattedMessage id="Share.ActionButton.Create" />
          <FormattedMessage id={`Share.MemberType.${memberStyle}`} />
        </Space>
      }
    >
      <MemberTypeForm memberStyle={memberStyle} />
    </LayoutPageBody>
  )
}
