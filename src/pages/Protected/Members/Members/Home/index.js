import React, { createContext } from 'react'
import { Result, Space } from 'antd'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import Permission from 'components/Permission'
import DataTable from 'components/DataTable'
import ReportScaffold from 'components/ReportScaffold'
import ExportReportButton from 'components/ExportReportButton'
import AddMemberButton from 'components/Member/AddMemberButton'
import { MEMBER_STYLE } from 'constants/memberDetail/memberType'
import * as PERMISSION from 'constants/permissions'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'

const {
    MEMBERS_MEMBERS_CREATE,
    MEMBERS_MEMBERS_VIEW,
    MEMBERS_MEMBERS_EXPORT,
} = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
        onReady,
        boundedMutate,
    } = useGetDataSourceWithSWR({
        url: '/api/Member/Search',
        defaultSortKey: 'CreateTime',
        autoFetch: true,
    })
    return (
        <PageContext.Provider value={{ boundedMutate, onUpdateCondition }}>
            <Permission
                isPage
                functionIds={[MEMBERS_MEMBERS_VIEW]}
                failedRender={
                    <Result
                        status="warning"
                        title={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                        }
                        subTitle={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                        }
                    />
                }
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢結果"
                                />
                            }
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            extendArea={
                                <Space>
                                    <Permission
                                        functionIds={[MEMBERS_MEMBERS_EXPORT]}
                                    >
                                        <ExportReportButton
                                            condition={condition}
                                            actionUrl="/api/Member/Export"
                                        />
                                    </Permission>
                                    <Permission
                                        functionIds={[MEMBERS_MEMBERS_CREATE]}
                                    >
                                        <AddMemberButton
                                            memberStyle={MEMBER_STYLE.MEMBER}
                                        />
                                    </Permission>
                                </Space>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
        </PageContext.Provider>
    )
}

export default PageView
