import React from 'react'
import { Space } from 'antd'
import { FormattedMessage } from 'react-intl'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'
import LockButton from '../../Home/ActionButtons/LockButton'
import ToggleFreezeButton from './ToggleFreezeButton'
import { MODE } from 'constants/mode'
import { MEMBER_STATUS } from 'constants/memberDetail/memberStatus'
import * as PERMISSION from 'constants/permissions'
import usePermission from 'hooks/usePermission'

const {
    MEMBERS_MEMBERS_EDIT,
    MEMBERS_MEMBERS_UNLOCK,
    MEMBERS_MEMBERS_FREEZE,
} = PERMISSION

export default function ActionButtons({ record }) {
    const [
        hasEditPermission,
        hasUnlockPermission,
        hasFreezePermission,
    ] = usePermission(
        MEMBERS_MEMBERS_EDIT,
        MEMBERS_MEMBERS_UNLOCK,
        MEMBERS_MEMBERS_FREEZE
    )
    const currentMemberStatus = record && record.Lock
    const isUnlockable =
        hasUnlockPermission &&
        currentMemberStatus === MEMBER_STATUS.LOCK_BY_MEMBER
    const isToggleFreezeable =
        hasFreezePermission &&
        (currentMemberStatus === MEMBER_STATUS.NORMAL ||
            currentMemberStatus === MEMBER_STATUS.LOCK_BY_ADMINTOOL)

    return (
        <Space>
            {hasEditPermission && (
                <OpenMemberDetailButton
                    memberId={record.MemberId}
                    memberName={record.MemberName}
                    displayText={
                        <FormattedMessage id="Share.ActionButton.Edit" />
                    }
                    mode={MODE.EDIT}
                    noDropdown
                />
            )}
            {isUnlockable && <LockButton memberId={record.MemberId} />}
            {isToggleFreezeable && (
                <ToggleFreezeButton
                    memberId={record.MemberId}
                    status={currentMemberStatus}
                />
            )}
        </Space>
    )
}
