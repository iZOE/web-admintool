import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, DatePicker, Select } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import FormItemCheckboxWithLabel from 'components/FormItems/CheckboxWithLabel'
import FormItemAdvanceQuery from 'components/FormItems/AdvanceQuery'
import {
    MEMBER_STYLE,
    AFFILIATE_LEVEL_TYPE,
} from 'constants/memberDetail/memberType'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { Option } = Select
const { RANGE } = DEFAULT
const REGISTER_FORM = [null, 1, 2]

const CONDITION_INITIAL_VALUE = {
    AffiliateLevelId: AFFILIATE_LEVEL_TYPE.MEMBER,
    MemberLevelId: null,
    MemberTypeId: MEMBER_STYLE.MEMBER,
    RegisterFrom: null,
    CreateTime: RANGE.FROM_INITIAL_TO_TODAY,
    PreLoginTime: RANGE.FROM_A_MONTH_TO_TODAY,
}

function Condition({ onUpdate, onReady }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()
    const { RangePicker } = DatePicker

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ CreateTime, PreLoginTime, ...restValues }) {
        const [CreateTimeStart, CreateTimeEnd] = CreateTime || [null, null]
        const [PreLoginTimeStart, PreLoginTimeEnd] = PreLoginTime || [
            null,
            null,
        ]
        return {
            CreateTimeStart: getISODateTimeString(CreateTimeStart),
            CreateTimeEnd: getISODateTimeString(CreateTimeEnd),
            PreLoginTimeStart: getISODateTimeString(PreLoginTimeStart),
            PreLoginTimeEnd: getISODateTimeString(PreLoginTimeEnd),
            ...restValues,
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={3}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.MemberType"
                                description="會員類型"
                            />
                        }
                        name="MemberTypeId"
                    >
                        <FormattedMessage
                            id={`Share.MemberType.${MEMBER_STYLE.MEMBER}`}
                            description="會員"
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.AffiliateLevel"
                                description="代理等級"
                            />
                        }
                        name="AffiliateLevelId"
                    >
                        <FormattedMessage
                            id={`Share.AffiliateLevel.${AFFILIATE_LEVEL_TYPE.MEMBER}`}
                            description="一般會員"
                        />
                    </Form.Item>
                </Col>
                <Col sm={5}>
                    <FormItemsSimpleSelect
                        needAll
                        url="/api/Option/MemberLevel"
                        name="MemberLevelId"
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.MemberLevel"
                                description="會員等級"
                            />
                        }
                    />
                </Col>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.CommonKeys.RegisterFrom"
                                description="來源"
                            />
                        }
                        name="RegisterFrom"
                    >
                        <Select>
                            {REGISTER_FORM.map(id => (
                                <Option value={id} key={id || 'All'}>
                                    <FormattedMessage
                                        id={`Share.CommonKeys.SelectItem.RegisterFromType.${id ||
                                            'All'}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesManagement.QueryCondition.CreateTime"
                                description="註冊時間"
                            />
                        }
                        name="CreateTime"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.PreLoginTime"
                                description="登入時間"
                            />
                        }
                        name="PreLoginTime"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemAdvanceQuery />
                </Col>
                <Col sm={4}>
                    <FormItemCheckboxWithLabel
                        name="DisableMemberIsNotShown"
                        formattedMessageId="Share.QueryCondition.IsOnlyEnabled"
                    />
                </Col>
                <Col sm={4} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
