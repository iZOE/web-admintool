import React from 'react';
import { FormattedMessage } from 'react-intl';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import ActionButtons from './ActionButtons';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import StatusBadge from 'components/Member/StatusBadge';
import OnlineStatusBadge from 'components/Member/OnlineStatusBadge';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="Share.FormItem.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => {
      return <OpenMemberDetailButton memberId={record.MemberId} memberName={record.MemberName} />;
    },
  },
  {
    title: <FormattedMessage id="Share.FormItem.NickName" description="名稱" />,
    dataIndex: 'NickName',
    key: 'NickName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.MemberBalanceAmount" description="餘額" />,
    dataIndex: 'MemberBalanceAmount',
    key: 'MemberBalanceAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.ReturnPoint" description="返點等級" />,
    dataIndex: 'ReturnPoint',
    key: 'ReturnPoint',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.MemberType" description="類型" />,
    dataIndex: 'MemberType',
    key: 'MemberType',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.MemberLevelName" description="等級" />,
    dataIndex: 'MemberLevelName',
    key: 'MemberLevelName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.AffiliateLevel" description="代理等級" />,
    dataIndex: 'AffiliateLevel',
    key: 'AffiliateLevel',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.ParentMemberName" description="所屬上級" />,
    dataIndex: 'ParentMemberName',
    key: 'ParentMemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, _2) =>
      text ? <OpenMemberDetailButton memberId={record.ParentMemberId} memberName={record.ParentMemberName} /> : NO_DATA,
  },
  {
    title: <FormattedMessage id="Share.FormItem.SignupTime" description="註冊時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.PreLoginTime" description="登入時間" />,
    dataIndex: 'PreLoginTime',
    key: 'PreLoginTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { PreLoginTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.CurrLoginIp" description="登入IP" />,
    dataIndex: 'CurrLoginIp',
    key: 'CurrLoginIp',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CurrLoginIp }, _2) => (CurrLoginIp ? CurrLoginIp : NO_DATA),
  },
  {
    title: <FormattedMessage id="Share.FormItem.Area" description="地區" />,
    dataIndex: 'CurrLoginIPInfo',
    key: 'CurrLoginIPInfo',
    render: (_1, { CurrLoginIPInfo: info }, _2) =>
      info ? (
        <span>
          {!info?.Country || info?.Country === '' ? NO_DATA : info.Country}|
          {!info?.RegionName || info?.RegionName === '' ? NO_DATA : info.RegionName}|
          {!info?.City || info?.City === '' ? NO_DATA : info.City}|
          {!info?.DeviceTypeName || info?.DeviceTypeName === '' ? NO_DATA : info.DeviceTypeName}
        </span>
      ) : (
        NO_DATA
      ),
  },
  {
    title: <FormattedMessage id="Share.FormItem.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { Status, StatusText }, _2) => <StatusBadge status={Status} text={StatusText} />,
  },
  {
    title: <FormattedMessage id="Share.FormItem.IsOnline" description="在線狀態" />,
    dataIndex: 'OnlineStatus',
    key: 'OnlineStatus',
    isShow: true,
    render: (_1, { OnlineStatus, OnlineStatusText }, _2) => (
      <OnlineStatusBadge status={OnlineStatus} text={OnlineStatusText} />
    ),
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'edit',
    key: 'edit',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => <ActionButtons record={record} />,
  },
];
