import React, { Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'
import { MEMBER_STYLE } from 'constants/memberDetail/memberType'
import Home from './Home'

const CreateMemberAsync = React.lazy(() => import('./Create'))

export default function Members() {
    return (
        <Suspense>
            <Switch>
                <Route exact path="/members/members">
                    <Home />
                </Route>
                <Route path="/members/members/create">
                    <CreateMemberAsync memberStyle={MEMBER_STYLE.MEMBER} />
                </Route>
            </Switch>
        </Suspense>
    )
}
