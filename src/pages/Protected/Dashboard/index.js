import React, { useEffect, useMemo, useContext } from 'react';
import { Breadcrumb, Button, Modal, Space, Typography } from 'antd';
import {
  CheckCircleOutlined,
  StopOutlined,
  CloseCircleOutlined
} from '@ant-design/icons';
import { FormattedMessage, useIntl } from 'react-intl';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import { MODE } from 'constants/mode';
import ICON_MAPPER from 'constants/iconMapper';
import { TAB_INFO } from 'constants/memberDetail/tabInfo';
import { MemberDetailContext } from 'contexts/MemberDetailContext';
import { UserContext } from 'contexts/UserContext';
import { BreadCrumbsContext } from 'contexts/BreadcrumbsContext';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import ApprovalsService from './services';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const STATE_COLOR = ['#1890ff', '#ff5500', '#fa8c16'];
const { Text } = Typography;
const { confirm } = Modal;
const MODAL_ICOON = {
  1: <CheckCircleOutlined />,
  2: <StopOutlined />,
  3: <CloseCircleOutlined />
};
const { changeApprovalsState } = ApprovalsService();

export default function Approvals() {
  const { formatMessage } = useIntl();
  const { onSetsuffixBreadcrumbs } = useContext(BreadCrumbsContext);
  const { onDisplayMemberDetail } = useContext(MemberDetailContext);
  const {
    data: { UserName }
  } = useContext(UserContext);
  const location = useLocation();
  const search = queryString.parse(location.search);

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition
  } = useGetDataSourceWithSWR({
    url: '/api/Approve/Search',
    defaultSortKey: 'ApplyTime',
    initialCondition: Object.values(search).length ? search : null
  });

  useEffect(() => {
    onSetsuffixBreadcrumbs(
      <Breadcrumb.Item key="approvals">
        {ICON_MAPPER['approvals']}
        <span>
          <FormattedMessage id="PageApprovals.PageHeader" />
        </span>
      </Breadcrumb.Item>
    );
    return () => {
      onSetsuffixBreadcrumbs(null);
    };
  }, []);

  const columnsConfig = useMemo(
    () =>
      COLUMNS_CONFIG.map(item => {
        switch (item.dataIndex) {
          case 'Description':
            return {
              ...item,
              render: (description, record) =>
                record.UrlPathType ? (
                  <Button
                    type="link"
                    size="small"
                    onClick={() => {
                      showDetail(record);
                    }}
                  >
                    {description}
                  </Button>
                ) : (
                  <Text>{description}</Text>
                )
            };
          case 'Manage':
            return {
              ...item,
              render: (_, record) =>
                record.State === 0 && (
                  <Space size="small">
                    {UserName === record.ApplyUserName ? (
                      <Button
                        size="small"
                        type="link"
                        onClick={() => changeState(record, 3)}
                      >
                        <Text style={{ color: STATE_COLOR[2] }}>
                          <FormattedMessage id="PageApprovals.Table.State.3" />
                        </Text>
                      </Button>
                    ) : (
                      <>
                        <Button
                          size="small"
                          type="link"
                          onClick={() => changeState(record, 1)}
                        >
                          <Text style={{ color: STATE_COLOR[0] }}>
                            <FormattedMessage id="PageApprovals.Table.State.1" />
                          </Text>
                        </Button>
                        <Button
                          size="small"
                          type="link"
                          onClick={() => changeState(record, 2)}
                        >
                          <Text style={{ color: STATE_COLOR[1] }}>
                            <FormattedMessage id="PageApprovals.Table.State.2" />
                          </Text>
                        </Button>
                      </>
                    )}
                  </Space>
                )
            };
          default:
            return item;
        }
      }),
    []
  );

  const changeState = (record, stateType) => {
    confirm({
      title: formatMessage({
        id: `PageApprovals.Modal.ConfirmTitle.${stateType}`
      }),
      icon: MODAL_ICOON[stateType],
      content: formatMessage(
        {
          id: `PageApprovals.Modal.ConfirmMessage.${stateType}`
        },
        {
          name: record.ApplyUserName
        }
      ),
      okText: formatMessage({
        id: 'Share.ActionButton.Confirm',
        description: '保存'
      }),
      cancelText: formatMessage({
        id: 'Share.ActionButton.Cancel',
        description: '取消'
      }),
      onOk() {
        changeApprovalsState({ id: record.Id, state: stateType }).then(
          onUpdateCondition
        );
      },
      onCancel() {
        console.log('Cancel');
      }
    });
  };

  const showDetail = record => {
    switch (record.UrlPathType) {
      case 'Members/View/Memberbank':
      case 'Affiliates/View/MemberBank':
        onDisplayMemberDetail(
          record.UrlParameter,
          MODE.VIEW,
          TAB_INFO.BANK_DATA
        );
        break;
      case 'ActivityReport/View':
        console.log('???????');
        break;
      default:
        console.log('???????');
    }
  };

  return (
    <ReportScaffold
      displayResult={condition}
      conditionComponent={
        <Condition onUpdate={onUpdateCondition} condition={condition} />
      }
      datatableComponent={
        <DataTable
          displayResult={condition}
          condition={condition}
          title={<FormattedMessage id="Share.Table.SearchResult" />}
          loading={fetching}
          config={columnsConfig}
          dataSource={dataSource && dataSource.Data}
          total={dataSource && dataSource.TotalCount}
          onUpdate={onUpdateCondition}
        />
      }
    />
  );
}
