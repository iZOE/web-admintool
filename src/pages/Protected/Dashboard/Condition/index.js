import React, { useEffect } from 'react'
import { Col, Row, DatePicker, Form } from 'antd'
import { FormattedMessage } from 'react-intl'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT, DEFAULT } from 'constants/dateConfig'

const { useForm } = Form
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_A_MONTH_TO_TODAY,
    State: null,
}

function Condition({ onUpdate, condition }) {
    const [form] = useForm()

    useEffect(() => {
        onUpdate(
            {
                ...resultCondition(form.getFieldsValue()),
                ...condition,
            },
            true
        )
    }, [])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            ApplyStartDate: getISODateTimeString(start),
            ApplyEndDate: getISODateTimeString(end),
        }
    }

    function onFinish(data) {
        onUpdate.bySearch(resultCondition(data))
    }
    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={{
                ...CONDITION_INITIAL_VALUE,
                State:
                    condition && condition.State
                        ? Number(condition.State)
                        : null,
            }}
        >
            <Row gutter={[16, 8]}>
                <Col sm={12} md={12}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageApprovals.QueryCondition.ApplyTime" />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8} md={6}>
                    <FormItemsSimpleSelect
                        url="/api/Option/Group/ApproveState"
                        name="State"
                        needAll
                        label={
                            <FormattedMessage id="Share.CommonKeys.Status" />
                        }
                    />
                </Col>
                <Col
                    sm={4}
                    md={{
                        span: 4,
                        offset: 2,
                    }}
                    align="right"
                >
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
