import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Tag } from 'antd'
import DateWithFormat from 'components/DateWithFormat'

const TAG_STATE = ['processing', 'success', 'error', 'warning']

export const COLUMNS_CONFIG = [
    {
        key: 'ApplyUserName',
        dataIndex: 'ApplyUserName',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage id="PageApprovals.Table.Columns.ApplyUserName" />
        ),
    },
    {
        key: 'SubMenuName',
        dataIndex: 'SubMenuName',
        isShow: true,
        title: <FormattedMessage id="PageApprovals.Table.Columns.Process" />,
    },
    {
        key: 'FunctionName',
        dataIndex: 'FunctionName',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage id="PageApprovals.Table.Columns.FunctionName" />
        ),
    },
    {
        key: 'Description',
        dataIndex: 'Description',
        isShow: true,
        title: (
            <FormattedMessage id="PageApprovals.Table.Columns.Description" />
        ),
    },
    {
        key: 'ApplyTime',
        dataIndex: 'ApplyTime',
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: <FormattedMessage id="PageApprovals.Table.Columns.ApplyTime" />,
        width: 200,
        render: (_1, { ApplyTime: time }, _2) => <DateWithFormat time={time} />,
    },
    {
        key: 'ApproveTime',
        dataIndex: 'ApproveTime',
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage id="PageApprovals.Table.Columns.ApproveTime" />
        ),
        width: 200,
        render: (_1, { ApproveTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        key: 'State',
        dataIndex: 'State',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: <FormattedMessage id="PageApprovals.Table.Columns.State" />,
        render: State => (
            <Tag color={TAG_STATE[State]}>
                <FormattedMessage id={`PageApprovals.Table.State.${State}`} />
            </Tag>
        ),
    },
    {
        key: 'ApproveUserName',
        dataIndex: 'ApproveUserName',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage id="PageApprovals.Table.Columns.ApproveUserName" />
        ),
    },
    {
        key: '',
        dataIndex: 'Manage',
        width: 80,
        isShow: true,
        isManage: true,
        align: 'center',
    },
]
