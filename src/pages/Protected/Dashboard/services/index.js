import makeService from 'utils/makeService';

const ApprovalsService = () => {
  const changeApprovalsState = ({ id, state }) => {
    return makeService({
      method: 'put',
      url: `/api/Approve/ChangeState/${id}/${state}`
    })();
  };

  return {
    changeApprovalsState
  };
};

export default ApprovalsService;
