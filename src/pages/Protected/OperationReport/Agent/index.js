import React, { createContext } from 'react';
import * as PERMISSION from 'constants/permissions';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import Condition from './Condition';
import SummaryView from './Summary';
import Table from './Table';

const { OPERATION_REPORT_AGENT_VIEW } = PERMISSION;

export const ConditionContext = createContext();

const PageView = () => {
  const {
    fetching,
    dataSource,
    onUpdateCondition,
    onReady,
    condition
  } = useGetDataSourceWithSWR({
    url: '/api/AgentReport/Search',
    defaultSortKey: 'MemberName',
    autoFetch: true
  });

  return (
    <ConditionContext.Provider value={{ condition }}>
      <Permission isPage functionIds={[OPERATION_REPORT_AGENT_VIEW]}>
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          summaryComponent={
            <SummaryView summary={dataSource && dataSource.Summary} />
          }
          datatableComponent={
            <Table
              dataSource={dataSource && dataSource.Data}
              onUpdate={onUpdateCondition}
              condition={condition}
              fetching={fetching}
            />
          }
        />
      </Permission>
    </ConditionContext.Provider>
  );
};

export default PageView;
