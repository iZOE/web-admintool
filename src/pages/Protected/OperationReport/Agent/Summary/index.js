import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary }) => {
    return (
        <div>
            {summary ? (
                <>
                    <Row gutter={24}>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.DepositCnt"
                                        description="充值人數"
                                    />
                                }
                                value={summary.DepositCnt}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.DepositAmount"
                                        description="充值金额"
                                    />
                                }
                                value={summary.DepositAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.Fee"
                                        description="手续费"
                                    />
                                }
                                value={summary.Fee}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.AdministrationFee"
                                        description="行政费"
                                    />
                                }
                                value={summary.AdministrationFee}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.WithdrawalAmount"
                                        description="提现金额"
                                    />
                                }
                                value={summary.WithdrawalAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.DepWithAmount"
                                        description="存提差"
                                    />
                                }
                                value={summary.DepWithAmount}
                                precision={2}
                            />
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.RmDeAmountnt"
                                        description="扣款金额"
                                    />
                                }
                                value={summary.RmDeAmountnt}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.RmAddAmount"
                                        description="风控补分"
                                    />
                                }
                                value={summary.RmAddAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.BalanceAmount"
                                        description="会员余额"
                                    />
                                }
                                value={summary.BalanceAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.BetMemberCnt"
                                        description="投注人數"
                                    />
                                }
                                value={summary.BetMemberCnt}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.BetAmount"
                                        description="投注金额"
                                    />
                                }
                                value={summary.BetAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.ValidBetAmount"
                                        description="有效投注金额"
                                    />
                                }
                                value={summary.ValidBetAmount}
                                precision={2}
                            />
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.BonusAmount"
                                        description="结算金额"
                                    />
                                }
                                value={summary.BonusAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.WinLossAmount"
                                        description="派彩(输赢)"
                                    />
                                }
                                value={summary.WinLossAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.PreferentialAmount"
                                        description="优惠金额"
                                    />
                                }
                                value={summary.PreferentialAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.ReturnPointAmount"
                                        description="返点"
                                    />
                                }
                                value={summary.ReturnPointAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.CommissionAmount"
                                        description="代理分红"
                                    />
                                }
                                value={summary.CommissionAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage
                                        id="PageOperationReportAgent.Summary.TotalWinLossAmount"
                                        description="平台利润"
                                    />
                                }
                                value={summary.TotalWinLossAmount}
                                precision={2}
                            />
                        </Col>
                    </Row>
                </>
            ) : (
                <Skeleton active />
            )}
        </div>
    )
}

export default SummaryView
