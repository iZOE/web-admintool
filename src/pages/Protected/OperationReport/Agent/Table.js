import React, { Fragment } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import { Button, Table, Divider, Card } from 'antd';
import { ExportOutlined } from '@ant-design/icons';
import * as PERMISSION from 'constants/permissions';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ExportReportButton from 'components/ExportReportButton';

const { OPERATION_REPORT_AFFILIATE_EXPORT } = PERMISSION;

const makeColumnsConfig = totalCount => [
  {
    title: (
      <FormattedMessage
        id="PageOperationReportAgent.DataTable.Title.GameProviderTypeGroupName"
        description="遊戲類別"
      />
    ),
    dataIndex: 'GameProviderTypeGroupName',
    key: 'GameProviderTypeGroupName',
    isShow: true,
    render: (text, row, index) => {
      const obj = {
        children: <span>{text}</span>,
        props: {
          rowSpan: 0,
        },
      };
      if (index === 0) {
        obj.props.rowSpan = totalCount;
      }
      return obj;
    },
  },
  {
    title: <FormattedMessage id="PageOperationReportAgent.DataTable.Title.GameProviderTypeName" description="第三方" />,
    dataIndex: 'GameProviderTypeName',
    key: 'GameProviderTypeName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    render: (value, row, index) => {
      let compo = <span>{value || '-'}</span>;
      if (index !== 0) {
        compo = <span style={{ marginLeft: '-16px' }}>{value || '-'}</span>;
      }
      return compo;
    },
  },
  {
    title: <FormattedMessage id="PageOperationReportAgent.DataTable.Title.MemberCNT" description="投注人數" />,
    dataIndex: 'MemberCNT',
    key: 'MemberCNT',
    isShow: true,
    render: (_1, record, _2) => (
      <FormattedNumber value={record.MemberCNT} minimumFractionDigits="0" maximumFractionDigits="0" />
    ),
  },
  {
    title: <FormattedMessage id="PageOperationReportAgent.DataTable.Title.BetAmount" description="投注金額" />,
    dataIndex: 'BetAmount',
    key: 'BetAmount',
    isShow: true,
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.BetAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAgent.DataTable.Title.ValidBetAmount" description="有效投注金額" />,
    dataIndex: 'ValidBetAmount',
    key: 'ValidBetAmount',
    isShow: true,
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.ValidBetAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAgent.DataTable.Title.BonusAmount" description="結算金額" />,
    dataIndex: 'BonusAmount',
    key: 'BonusAmount',
    isShow: true,
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.BonusAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAgent.DataTable.Title.WinLossAmount" description="派彩(輸贏)" />,
    dataIndex: 'WinLossAmount',
    key: 'WinLossAmount',
    isShow: true,
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.WinLossAmount} />,
  },
];

const TableSummary = ({ dataSource }) => {
  return (
    <Table.Summary.Row style={{ background: '#fafafa', fontWeight: 500 }}>
      <Table.Summary.Cell index={0} colSpan={2}>
        <FormattedMessage id="PageOperationReportAgent.DataTable.Summary.Total" description="類別小計" />
      </Table.Summary.Cell>
      <Table.Summary.Cell index={1}>
        <FormattedNumber value={dataSource.MemberCNT} minimumFractionDigits="0" maximumFractionDigits="0" />
      </Table.Summary.Cell>
      <Table.Summary.Cell index={2}>
        <ReactIntlCurrencyWithFixedDecimal value={dataSource.BetAmountSummary} />
      </Table.Summary.Cell>
      <Table.Summary.Cell index={3}>
        <ReactIntlCurrencyWithFixedDecimal value={dataSource.ValidBetAmountSummary} />
      </Table.Summary.Cell>
      <Table.Summary.Cell index={4}>
        <ReactIntlCurrencyWithFixedDecimal value={dataSource.BonusAmountSummary} />
      </Table.Summary.Cell>
      <Table.Summary.Cell index={5}>
        <ReactIntlCurrencyWithFixedDecimal value={dataSource.WinLossAmountSummary} />
      </Table.Summary.Cell>
    </Table.Summary.Row>
  );
};

const TableView = ({ dataSource, onUpdate, condition, fetching }) => {
  return (
    <Card
      size="small"
      title={<FormattedMessage id="Share.Table.SearchResult" description="查询结果" />}
      extra={
        <Permission
          functionIds={[OPERATION_REPORT_AFFILIATE_EXPORT]}
          failedRender={
            <Button type="primary" icon={<ExportOutlined />} disabled>
              <FormattedMessage id="Share.ActionButton.Export" />
            </Button>
          }
        >
          <ExportReportButton actionUrl="/api/AgentReport/Export" condition={condition} />
        </Permission>
      }
    >
      {dataSource ? (
        dataSource.map((data, index) => {
          return (
            <Fragment key={`GroupedData_${index}`}>
              <DataTable
                loading={fetching}
                displayResult={condition}
                onUpdate={onUpdate}
                pagination={false}
                noHeader={true}
                config={makeColumnsConfig(data.GroupedData.length)}
                dataSource={data.GroupedData}
                summary={() => <TableSummary dataSource={data} />}
                rowKey={record => `${record.GameProviderTypeName}_${data.GameProviderTypeGroup}`}
              />
              {dataSource.length - 1 > index && <Divider />}
            </Fragment>
          );
        })
      ) : (
        <div style={{ color: '#c7c7c7', textAlign: 'center' }}>
          <FormattedMessage id="DataTable.PleaseSearch" defaultMessage="Please enter search" />
        </div>
      )}
    </Card>
  );
};

export default TableView;
