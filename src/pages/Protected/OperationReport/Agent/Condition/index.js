import React, { useEffect } from 'react'
import { Form, Button, Row, Col, DatePicker } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT, DEFAULT } from 'constants/dateConfig'

const { RangePicker } = DatePicker
const { RANGE } = DEFAULT
const { useForm } = Form

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
    IncludeInnerMember: false,
}

function Condition({ onReady, onUpdate }) {
    const [form] = useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(true)
    }, [])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            BeginDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={10}>
                    <Form.Item
                        label={
                            <FormattedMessage id="Share.QueryCondition.DateRange" />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={{ span: 4, offset: 6 }} align="right">
                    <FormItemsIncludeInnerMember />
                </Col>
                <Col sm={4} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
