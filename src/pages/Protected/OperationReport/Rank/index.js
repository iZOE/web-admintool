import React, { useMemo } from 'react'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ExportReportButton from 'components/ExportReportButton'
import ReportScaffold from 'components/ReportScaffold'
import Condition from './Condition'
import { SORT_DIRECTION } from 'constants/sortDirection'
import { COLUMNS_CONFIG } from './datatableConfig/index'
import { COLUMNS_CONFIG_TYPE_A } from './datatableConfig/typeA'
import { COLUMNS_CONFIG_TYPE_B } from './datatableConfig/typeB'

const { OPERATION_REPORT_RANK_VIEW } = PERMISSION

const PageView = () => {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        onReady,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/Rank/Search',
        defaultSortKey: 'Rank',
        defaultSortDirection: SORT_DIRECTION.Asc,
        autoFetch: true,
    })

    const columnConfig = useMemo(() => {
        if (condition) {
            if ([1, 2, 3, 4].indexOf(condition.RankTypeId) !== -1) {
                return [].concat(COLUMNS_CONFIG, COLUMNS_CONFIG_TYPE_A)
            } else {
                return [].concat(COLUMNS_CONFIG, COLUMNS_CONFIG_TYPE_B)
            }
        }
        return []
    }, [condition])

    return (
        <Permission functionIds={[OPERATION_REPORT_RANK_VIEW]} isPage>
            <ReportScaffold
                displayResult={condition}
                conditionComponent={
                    <Condition onReady={onReady} onUpdate={onUpdateCondition} />
                }
                datatableComponent={
                    <DataTable
                        displayResult={condition}
                        condition={condition}
                        title={
                            <FormattedMessage
                                id="Share.Table.SearchResult"
                                description="查詢結果"
                            />
                        }
                        config={columnConfig}
                        loading={fetching}
                        extendArea={
                            <ExportReportButton
                                condition={condition}
                                actionUrl="/api/Rank/Export"
                            />
                        }
                        rowKey={record => record.Rank}
                        dataSource={dataSource && dataSource.Data}
                        total={dataSource && dataSource.TotalCount}
                        onUpdate={onUpdateCondition}
                    />
                }
            />
        </Permission>
    )
}

export default PageView
