import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Select, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemsMemberLevel from 'components/FormItems/MemberLevel'
import FormItemsRankTypes from 'components/FormItems/RankTypes'
import FormItemsLotteries from 'components/FormItems/Lotteries'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT, DEFAULT } from 'constants/dateConfig'

const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const STAT_TYPE_IDS = [1, 2]

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
    IncludeInnerMember: false,
    StatisticsType: 1,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = Form.useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            StartDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        name="Date"
                        label={
                            <FormattedMessage id="PageReportRank.QueryCondition.DateTime" />
                        }
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsRankTypes />
                </Col>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageReportRank.QueryCondition.StatisticsType" />
                        }
                        name="StatisticsType"
                    >
                        <Select>
                            {STAT_TYPE_IDS.map(id => (
                                <Option value={id} key={id}>
                                    <FormattedMessage
                                        id={`PageReportRank.QueryCondition.SelectItem.StatisticsType.${id}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemsMemberLevel checkAll />
                </Col>
                <Col sm={10}>
                    <FormItemsLotteries checkAll />
                </Col>
                <Col sm={4}>
                    <FormItemsIncludeInnerMember />
                </Col>
                <Col sm={10} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
