import React from 'react'
import { FormattedMessage } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

export const COLUMNS_CONFIG_TYPE_A = [
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.TotalBetAmount"
                description="TotalBetAmount"
            />
        ),
        dataIndex: 'TotalBetAmount',
        key: 'TotalBetAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.TotalWinAmount"
                description="TotalWinAmount"
            />
        ),
        dataIndex: 'TotalWinAmount',
        key: 'TotalWinAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.TotalWinLossAmount"
                description="TotalWinLossAmount"
            />
        ),
        dataIndex: 'TotalWinLossAmount',
        key: 'TotalWinLossAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
]
