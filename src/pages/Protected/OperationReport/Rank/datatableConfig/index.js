import React from 'react'
import { FormattedMessage } from 'react-intl'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.Rank"
                description="排名"
            />
        ),
        dataIndex: 'Rank',
        key: 'Rank',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
    },
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.MemberName"
                description="會員名稱"
            />
        ),
        dataIndex: 'MemberName',
        key: 'MemberName',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: (text, record, index) => {
            return (
                <OpenMemberDetailButton
                    memberId={record.MemberId}
                    memberName={record.MemberName}
                />
            )
        },
    },
]
