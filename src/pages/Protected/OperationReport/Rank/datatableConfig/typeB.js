import React from 'react'
import { FormattedMessage } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

export const COLUMNS_CONFIG_TYPE_B = [
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.TotalDepositAmount"
                description="TotalBetAmount"
            />
        ),
        dataIndex: 'TotalDepositAmount',
        key: 'TotalDepositAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.TotalWithdrawAmount"
                description="TotalWithdrawAmount"
            />
        ),
        dataIndex: 'TotalWithdrawAmount',
        key: 'TotalWithdrawAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage
                id="PageReportRank.DataTable.Title.TotalPreferentialAmount"
                description="TotalPreferentialAmount"
            />
        ),
        dataIndex: 'TotalPreferentialAmount',
        key: 'TotalPreferentialAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
]
