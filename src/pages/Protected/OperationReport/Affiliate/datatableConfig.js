import React from 'react';
import { FormattedMessage } from 'react-intl';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import GetDirectFollowersButton from './GetDirectFollowersButton';

export const COLUMNS_CONFIG = [
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.ParentMemberName" description="代理帐号" />
    ),
    dataIndex: 'ParentMemberName',
    key: 'ParentMemberName',
    fixed: 'left',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => {
      return <OpenMemberDetailButton memberId={record.MemberId} memberName={record.ParentMemberName} />;
    },
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.DirectFollower" description="直属下级" />,
    dataIndex: 'DirectFollower',
    key: 'DirectFollower',
    isShow: true,
    render: (_1, { MemberTreeCnt, ParentMemberName }, _2) =>
      !!MemberTreeCnt && <GetDirectFollowersButton memberName={ParentMemberName} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.MemberTreeCnt" description="下级人数" />,
    dataIndex: 'MemberTreeCnt',
    key: 'MemberTreeCnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.RegisterCnt" description="注册人数" />,
    dataIndex: 'RegisterCnt',
    key: 'RegisterCnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.DepositFirstTimeCnt" description="首充人数" />
    ),
    dataIndex: 'DepositFirstTimeCnt',
    key: 'DepositFirstTimeCnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.DepositCnt" description="充值人数" />,
    dataIndex: 'DepositCnt',
    key: 'DepositCnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.Deposit3Cnt" description="三存人数" />,
    dataIndex: 'Deposit3Cnt',
    key: 'Deposit3Cnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.Deposit5Cnt" description="五存人数" />,
    dataIndex: 'Deposit5Cnt',
    key: 'Deposit5Cnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.BetMemberCnt" description="投注人数" />,
    dataIndex: 'BetMemberCnt',
    key: 'BetMemberCnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.BetOrderCnt" description="订单量" />,
    dataIndex: 'BetOrderCnt',
    key: 'BetOrderCnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.BetAmount" description="投注金额" />,
    dataIndex: 'BetAmount',
    key: 'BetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.BetAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.ValidBetAmount" description="有效投注金额" />
    ),
    dataIndex: 'ValidBetAmount',
    key: 'ValidBetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.ValidBetAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.BonusAmount" description="结算金额" />,
    dataIndex: 'BonusAmount',
    key: 'BonusAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.BonusAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.WinLossAmount" description="派彩(输赢)" />
    ),
    dataIndex: 'WinLossAmount',
    key: 'WinLossAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.WinLossAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.DepositAmount" description="充值金额" />,
    dataIndex: 'DepositAmount',
    key: 'DepositAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.DepositAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.WithdrawalAmount" description="提现金额" />
    ),
    dataIndex: 'WithdrawalAmount',
    key: 'WithdrawalAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.WithdrawalAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.DepWithAmount" description="存提差" />,
    dataIndex: 'DepWithAmount',
    key: 'DepWithAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.DepWithAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.DeductionAmount" description="扣款金额" />
    ),
    dataIndex: 'DeductionAmount',
    key: 'DeductionAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.DeductionAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.RmAdditionAmount" description="风控补分" />
    ),
    dataIndex: 'RmAdditionAmount',
    key: 'RmAdditionAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.RmAdditionAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.PrePaymentAmount" description="优惠金额" />
    ),
    dataIndex: 'PrePaymentAmount',
    key: 'PrePaymentAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.PrePaymentAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.ReturnPointAmount" description="返点" />,
    dataIndex: 'ReturnPointAmount',
    key: 'ReturnPointAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.ReturnPointAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.CommissionAmount" description="代理分红" />
    ),
    dataIndex: 'CommissionAmount',
    key: 'CommissionAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.CommissionAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.Fee" description="站台手续费" />,
    dataIndex: 'Fee',
    key: 'Fee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.AdministrationFee" description="行政費" />
    ),
    dataIndex: 'AdministrationFee',
    key: 'AdministrationFee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.MemberBalance" description="会员余额" />,
    dataIndex: 'MemberBalance',
    key: 'MemberBalance',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.MemberBalance} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportAffiliate.DataTable.Title.TotalWinLossAmount" description="平台利润" />
    ),
    dataIndex: 'TotalWinLossAmount',
    key: 'TotalWinLossAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.TotalWinLossAmount} />,
  },
];
