import React, { useEffect } from 'react'
import { Form, Row, Col, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { useHistory } from 'react-router-dom'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import AdvancedSearchItem from 'components/FormItems/AdvancedSearchItem'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { getISODateTimeString } from 'mixins/dateTime'

const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const { useForm } = Form
const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
    AdvanceSearchOption: 1,
}
const ADVANCED_SEARCH_CONDITION_ITEM = [1, 2]
const ADVANCE_ITEM_OPTIONS_REQUIRE = new Set([2])

function Condition({ initialValueForTrigger, onUpdate, onReady }) {
    const history = useHistory()
    const [form] = useForm()
    const { AffiliateAccount, AdvanceSearchOption } = initialValueForTrigger

    useEffect(() => {
        if (AffiliateAccount) {
            form.setFieldsValue({
                ...initialValueForTrigger,
            })
            onFinish(form.getFieldsValue())
        }
    }, [initialValueForTrigger])

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(true)
    }, [])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            StartDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        if (!values.AffiliateAccount) {
            history.push(history.location.pathname)
        }
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={
                AffiliateAccount
                    ? {
                          ...CONDITION_INITIAL_VALUE,
                          AdvanceSearchOption,
                          AffiliateAccount,
                      }
                    : CONDITION_INITIAL_VALUE
            }
        >
            <Row gutter={[16, 8]}>
                <Col sm={10}>
                    <Form.Item
                        label={
                            <FormattedMessage id="Share.QueryCondition.DateRange" />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <AdvancedSearchItem
                        OptionItems={ADVANCED_SEARCH_CONDITION_ITEM}
                        MessageId="PageOperationReportAffiliate.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions"
                        AdvancedSearchConditionItemName="AdvanceSearchOption"
                        AdvancedSearchConditionValueName="AffiliateAccount"
                        OptionRequire={ADVANCE_ITEM_OPTIONS_REQUIRE}
                    />
                </Col>
                <Col sm={3}>
                    <FormItemsIncludeInnerMember />
                </Col>
                <Col sm={3} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
