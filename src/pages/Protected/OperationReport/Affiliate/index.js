import React, { useContext, useMemo, createContext, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { Button, Breadcrumb } from 'antd';
import { ExportOutlined } from '@ant-design/icons';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';
import { SORT_DIRECTION } from 'constants/sortDirection';
import * as PERMISSION from 'constants/permissions';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import ExportReportButton from 'components/ExportReportButton';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import SummaryView from './Summary';
import { BreadCrumbsContext } from 'contexts/BreadcrumbsContext';

const {
  OPERATION_REPORT_AFFILIATE_VIEW,
  OPERATION_REPORT_AFFILIATE_EXPORT
} = PERMISSION;

export const ConditionContext = createContext();

const PageView = () => {
  const location = useLocation();

  const lastMemberAccountFromQuery = useMemo(() => {
    const { m, direct } = queryString.parse(location.search);
    const result = {
      AdvanceSearchOption: Number(direct === 'true') + 1
    };

    if (m) {
      const members = m.split(',');
      const lastMemberAccount =
        members.length > 0 ? members[members.length - 1] : null;
      return {
        ...result,
        AffiliateAccount: lastMemberAccount
      };
    }
    return result;
  }, [location]);

  const { onSetsuffixBreadcrumbs } = useContext(BreadCrumbsContext);

  useEffect(() => {
    const { m, direct } = queryString.parse(location.search);
    if (m) {
      const members = m.split(',');
      let query = '';
      const newBC = members.map((m, idx) => {
        query = `${query}${query ? ',' : ''}${m}`;
        return (
          <Breadcrumb.Item
            key="m"
            href={`/operation-report/affiliate?${queryString.stringify({
              m: query,
              direct: direct === 'true'
            })}`}
          >
            {m}
          </Breadcrumb.Item>
        );
      });
      onSetsuffixBreadcrumbs(newBC);
    }

    return () => {
      onSetsuffixBreadcrumbs(null);
    };
  }, [location]);

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    onReady,
    condition
  } = useGetDataSourceWithSWR({
    url: '/api/OperatingReport/Affiliate/Search',
    defaultSortKey: 'BetAmount',
    autoFetch: true
  });

  return (
    <ConditionContext.Provider value={{ condition }}>
      <Permission isPage functionIds={[OPERATION_REPORT_AFFILIATE_VIEW]}>
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition
              onReady={onReady}
              onUpdate={onUpdateCondition}
              initialValueForTrigger={lastMemberAccountFromQuery}
            />
          }
          summaryComponent={
            <SummaryView summary={dataSource && dataSource.Summary} />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢結果"
                />
              }
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource && dataSource.List}
              total={dataSource && dataSource.TotalCount}
              rowKey={record => record.ParentMemberName}
              extendArea={
                <Permission
                  functionIds={[OPERATION_REPORT_AFFILIATE_EXPORT]}
                  failedRender={
                    <Button type="primary" icon={<ExportOutlined />} disabled>
                      <FormattedMessage id="Share.ActionButton.Export" />
                    </Button>
                  }
                >
                  <ExportReportButton
                    condition={condition}
                    actionUrl="/api/OperatingReport/Affiliate/Export"
                  />
                </Permission>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
      </Permission>
    </ConditionContext.Provider>
  );
};

export default PageView;
