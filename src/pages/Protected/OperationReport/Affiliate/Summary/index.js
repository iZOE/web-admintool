import React from "react";
import { Statistic, Row, Col, Skeleton } from "antd";
import { FormattedMessage } from "react-intl";

const SummaryView = ({ summary }) => {
  return (
    <div>
      {summary ? (
        <>
          <Row gutter={24}>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.AffilateCnt" />
                }
                value={summary.AffilateCnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.MemberTreeCnt" />
                }
                value={summary.MemberTreeCnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.RegisterCnt" />
                }
                value={summary.RegisterCnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.DepositFirstTimeCnt" />
                }
                value={summary.DepositFirstTimeCnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.DepositCnt" />
                }
                value={summary.DepositCnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.Deposit3Cnt" />
                }
                value={summary.Deposit3Cnt}
              />
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.Deposit5Cnt" />
                }
                value={summary.Deposit5Cnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.BetMemberCnt" />
                }
                value={summary.BetMemberCnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.BetOrderCnt" />
                }
                value={summary.BetOrderCnt}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.BetAmount" />
                }
                value={summary.BetAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.ValidBetAmount" />
                }
                value={summary.ValidBetAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.BonusAmount" />
                }
                value={summary.BonusAmount}
                precision={2}
              />
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.WinLossAmount" />
                }
                value={summary.WinLossAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.DepositAmount" />
                }
                value={summary.DepositAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.WithdrawalAmount" />
                }
                value={summary.WithdrawalAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.DepWithAmount" />
                }
                value={summary.DepWithAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.DeductionAmount" />
                }
                value={summary.DeductionAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.RmAdditionAmount" />
                }
                value={summary.RmAdditionAmount}
                precision={2}
              />
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.PrePaymentAmount" />
                }
                value={summary.PrePaymentAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.ReturnPointAmount" />
                }
                value={summary.ReturnPointAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.CommissionAmount" />
                }
                value={summary.CommissionAmount}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.Fee" />
                }
                value={summary.Fee}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.AdministrationFee" />
                }
                value={summary.AdministrationFee}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.MemberBalance" />
                }
                value={summary.MemberBalance}
                precision={2}
              />
            </Col>
            <Col span={4}>
              <Statistic
                title={
                  <FormattedMessage id="PageOperationReportAffiliate.Summary.TotalWinLossAmount" />
                }
                value={summary.TotalWinLossAmount}
                precision={2}
              />
            </Col>
          </Row>
        </>
      ) : (
        <Skeleton active />
      )}
    </div>
  );
};

export default SummaryView;
