import React, { useContext } from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import { ConditionContext } from '../index';

export default function GetDirectFollowersButton({ memberName }) {
  const { condition } = useContext(ConditionContext);
  const { AdvanceSearchOption, AffiliateAccount } = condition;

  return (
    <Link
      to={location => ({
        ...location,
        search: `direct=true&m=${
          AdvanceSearchOption === 2 && AffiliateAccount
            ? `${queryString.parse(location.search)['m']},${memberName}`
            : memberName
        }`
      })}
    >
      <FormattedMessage id="PageOperationReportAffiliate.ActionButton.CheckSubordinate" />
    </Link>
  );
}
