import React, { useContext } from 'react'
import { Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import { PageContext } from '../../index'

export default function ButtonItems({ record }) {
    const { setDetailDrawer } = useContext(PageContext)
    return (
        <Button
            type="link"
            onClick={() =>
                setDetailDrawer({
                    visible: true,
                    record,
                })
            }
        >
            <FormattedMessage id="Share.ActionButton.Detail" />
        </Button>
    )
}
