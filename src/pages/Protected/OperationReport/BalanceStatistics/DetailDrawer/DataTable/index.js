import React, { createContext } from 'react';
import { Tooltip, Button } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';
import DataTableComponent from 'components/DataTable';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import { COLUMNS_CONFIG } from './datatableConfig';
import { Wrapper } from './Styled';

export const PageContext = createContext();

const DataTable = ({ record }) => {
  const { GameProviderCode } = record;
  const { fetching, dataSource, onUpdateCondition, boundedMutate } = useGetDataSourceWithSWR({
    url: `/api/GameProvider/${GameProviderCode}/Balance/Search`,
    defaultSortKey: 'UpdateTime',
    autoFetch: true,
    noCondition: true,
  });

  return (
    <Wrapper>
      <DataTableComponent
        config={COLUMNS_CONFIG}
        dataSource={dataSource && dataSource.Data}
        total={dataSource && dataSource.TotalCount}
        extendArea={
          <Tooltip placement="top" title={<FormattedMessage id="Share.ActionButton.Reload" />}>
            <Button type="link" size="large" onClick={boundedMutate}>
              <ReloadOutlined />
            </Button>
          </Tooltip>
        }
        loading={fetching}
        pagination={{
          position: ['bottomCenter'],
        }}
        onUpdate={onUpdateCondition}
      />
    </Wrapper>
  );
};

export default DataTable;
