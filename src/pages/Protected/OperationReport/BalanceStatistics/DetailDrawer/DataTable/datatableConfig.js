import React from 'react'
import { FormattedMessage } from 'react-intl'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="PageProductsBalanceStatistics.DataTable.Title.GameProviderName"
                description="平台"
            />
        ),
        dataIndex: 'GameProviderName',
        key: 'GameProviderName',
        width: 100,
    },
    {
        title: (
            <FormattedMessage
                id="PageProductsBalanceStatistics.DataTable.Title.MemberName"
                description="会员帐号"
            />
        ),
        dataIndex: 'MemberName',
        key: 'MemberName',
        width: 200,
        render: (_1, { MemberName, MemberId }, _2) => (
            <OpenMemberDetailButton
                memberName={MemberName}
                memberId={MemberId}
            />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageProductsBalanceStatistics.DataTable.Title.Balance"
                description="餘額"
            />
        ),
        dataIndex: 'Balance',
        key: 'Balance',
        width: 150,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
]
