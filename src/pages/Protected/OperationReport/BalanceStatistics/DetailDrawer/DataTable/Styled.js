import styled from 'styled-components'

export const Wrapper = styled.div`
    #pagination {
        padding: 1rem;
        text-align: center;
        background: #fff;
    }
    .header {
        text-align: right;
    }
`
