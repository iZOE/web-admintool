import React from 'react'
import { Drawer } from 'antd'
import { FormattedMessage } from 'react-intl'
import DataTable from './DataTable'

export default function DetailDrawer({ drawerData, setDrawer }) {
    const { record, visible } = drawerData

    const closeModal = () =>
        setDrawer({
            visible: false,
        })

    return (
        <Drawer
            title={
                <FormattedMessage id="PageProductsBalanceStatistics.Drawer.Title" />
            }
            width={800}
            onClose={() => closeModal()}
            visible={visible}
        >
            {record && <DataTable record={record} />}
        </Drawer>
    )
}
