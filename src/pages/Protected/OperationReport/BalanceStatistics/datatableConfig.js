import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import ButtonItems from './Rows/ButtonItems';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsBalanceStatistics.DataTable.Title.GameProviderName" description="平台" />,
    dataIndex: 'GameProviderName',
    key: 'GameProviderName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsBalanceStatistics.DataTable.Title.Balance" description="餘額" />,
    dataIndex: 'Balance',
    key: 'Balance',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: (
      <FormattedMessage id="PageProductsBalanceStatistics.DataTable.Title.UpdateTime" description="餘額最後更新時間" />
    ),
    dataIndex: 'UpdateTime',
    key: 'UpdateTime',
    render: (_1, { UpdateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsBalanceStatistics.DataTable.Title.QueryTime" description="查詢時間" />,
    dataIndex: 'QueryTime',
    key: 'QueryTime',
    render: (_1, { QueryTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsBalanceStatistics.DataTable.Title.Action" description="管理" />,
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => <ButtonItems record={record} />,
  },
];
