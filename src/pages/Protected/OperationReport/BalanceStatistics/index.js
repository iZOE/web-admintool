import React, { useEffect, useState, createContext } from 'react';
import { FormattedMessage } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ExportReportButton from 'components/ExportReportButton';
import DetailDrawer from './DetailDrawer';
import { COLUMNS_CONFIG } from './datatableConfig';

const { OPERATION_REPORT_BALANCE_STATISTICS_VIEW, OPERATION_REPORT_BALANCE_STATISTICS_EXPORT } = PERMISSION;

export const PageContext = createContext();

const PageView = () => {
  const [detailDrawer, setDetailDrawer] = useState({
    visible: false,
    record: null,
  });

  const { fetching, dataSource, onReady, onUpdateCondition } = useGetDataSourceWithSWR({
    url: '/api/GameProvider/Balance/Search',
    defaultSortKey: 'Balance',
    autoFetch: true,
  });

  useEffect(() => {
    onReady(true);
  }, []);

  return (
    <PageContext.Provider
      value={{
        dataSource,
        setDetailDrawer,
      }}
    >
      <Permission functionIds={[OPERATION_REPORT_BALANCE_STATISTICS_VIEW]} isPage>
        <DataTable
          title={<FormattedMessage id="Share.Table.SearchResult" description="查詢結果" />}
          config={COLUMNS_CONFIG}
          dataSource={dataSource && dataSource.Data}
          total={dataSource && dataSource.TotalCount}
          loading={fetching}
          rowKey={record => record.GameProviderCode}
          extendArea={
            <Permission functionIds={[OPERATION_REPORT_BALANCE_STATISTICS_EXPORT]}>
              <ExportReportButton actionUrl="/api/GameProvider/Balance/Export" />
            </Permission>
          }
          onUpdate={onUpdateCondition}
        />
        <DetailDrawer drawerData={detailDrawer} setDrawer={data => setDetailDrawer(data)} />
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
