import React from 'react';
import { Button } from 'antd';
import { ExportOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';
import { SORT_DIRECTION } from 'constants/sortDirection';
import * as PERMISSION from 'constants/permissions';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import ExportReportButton from 'components/ExportReportButton';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import SummaryView from './Summary';

const {
  OPERATION_REPORT_MEMBER_VIEW,
  OPERATION_REPORT_MEMBER_EXPORT
} = PERMISSION;

const PageView = () => {
  const {
    fetching,
    dataSource,
    onUpdateCondition,
    onReady,
    condition
  } = useGetDataSourceWithSWR({
    url: '/api/OperatingReport/Member/Search',
    defaultSortKey: 'BetAmount',
    autoFetch: true
  });

  return (
    <Permission isPage functionIds={[OPERATION_REPORT_MEMBER_VIEW]}>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={
          <Condition onReady={onReady} onUpdate={onUpdateCondition} />
        }
        summaryComponent={
          <SummaryView summary={dataSource && dataSource.Summary} />
        }
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            title={
              <FormattedMessage
                id="Share.Table.SearchResult"
                description="查詢結果"
              />
            }
            config={COLUMNS_CONFIG}
            loading={fetching}
            dataSource={dataSource && dataSource.List}
            total={dataSource && dataSource.TotalCount}
            rowKey={record => record.MemberId}
            extendArea={
              <Permission
                functionIds={[OPERATION_REPORT_MEMBER_EXPORT]}
                failedRender={
                  <Button type="primary" icon={<ExportOutlined />} disabled>
                    <FormattedMessage id="Share.ActionButton.Export" />
                  </Button>
                }
              >
                <ExportReportButton
                  condition={condition}
                  actionUrl="/api/OperatingReport/Member/Export"
                />
              </Permission>
            }
            onUpdate={onUpdateCondition}
          />
        }
      />
    </Permission>
  );
};

export default PageView;
