import React, { useEffect, useMemo } from 'react'
import { Form, Row, Col, DatePicker } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import AdvancedSearchItem from 'components/FormItems/AdvancedSearchItem'
import QueryButton from 'components/FormActionButtons/Query'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { getISODateTimeString } from 'mixins/dateTime'

const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
    AdvancedSearchConditionItem: 1,
    AdvancedSearchConditionValue: null,
}

const ADVANCE_SEARCH_CONDITION_ITEMS = [1, 2, 3] // 1:會員帳號 2:直屬上級  3:所屬團隊

function Condition({ onReady, onUpdate }) {
    const [form] = Form.useForm()
    const intl = useIntl()

    const PLACEHOLDERS = useMemo(
        () => ({
            1: intl.formatMessage(
                {
                    id: 'Share.PlaceHolder.Input.PlsEnterWith',
                },
                {
                    with: intl.formatMessage({
                        id: 'PageOperationReportMember.AdvanceQuery.1',
                    }),
                }
            ),
            2: intl.formatMessage(
                {
                    id: 'Share.PlaceHolder.Input.PlsEnterWith',
                },
                {
                    with: intl.formatMessage({
                        id: 'PageOperationReportMember.AdvanceQuery.2',
                    }),
                }
            ),
            3: intl.formatMessage(
                {
                    id: 'Share.PlaceHolder.Input.PlsEnterWith',
                },
                {
                    with: intl.formatMessage({
                        id: 'PageOperationReportMember.AdvanceQuery.3',
                    }),
                }
            ),
        }),
        []
    )

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(true)
    }, [])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            StartDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={10}>
                    <Form.Item
                        label={
                            <FormattedMessage id="Share.QueryCondition.DateRange" />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={7}>
                    <AdvancedSearchItem
                        OptionItems={ADVANCE_SEARCH_CONDITION_ITEMS}
                        MessageId="PageOperationReportMember.AdvanceQuery"
                        placeholders={PLACEHOLDERS}
                    />
                </Col>
                <Col sm={3}>
                    <FormItemsIncludeInnerMember />
                </Col>
                <Col sm={4} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
