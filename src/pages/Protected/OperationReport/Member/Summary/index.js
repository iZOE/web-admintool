import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary }) => {
    return (
        <div>
            {summary ? (
                <>
                    <Row gutter={24}>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.MemberCount" />
                                }
                                value={summary.MemberCnt}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.OrderCount" />
                                }
                                value={summary.BetOrderCnt}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.BetAmount" />
                                }
                                value={summary.BetAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.ValidBetAmount" />
                                }
                                value={summary.ValidBetAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.BonusAmount" />
                                }
                                value={summary.BonusAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.WinLossAmount" />
                                }
                                value={summary.WinLossAmount}
                                precision={2}
                            />
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.DepositCnt" />
                                }
                                value={summary.DepositCnt}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.DepositAmount" />
                                }
                                value={summary.DepositAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.WithdrawalAmount" />
                                }
                                value={summary.WithdrawalAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.DepWithAmount" />
                                }
                                value={summary.DepWithAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.DeductionAmount" />
                                }
                                value={summary.DeductionAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.RmAdditionAmount" />
                                }
                                value={summary.RmAdditionAmount}
                                precision={2}
                            />
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.PrePaymentAmount" />
                                }
                                value={summary.PrePaymentAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.ReturnPointAmount" />
                                }
                                value={summary.ReturnPointAmount}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.Fee" />
                                }
                                value={summary.Fee}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.AdministrationFee" />
                                }
                                value={summary.AdministrationFee}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.MemberBalance" />
                                }
                                value={summary.MemberBalance}
                                precision={2}
                            />
                        </Col>
                        <Col span={4}>
                            <Statistic
                                title={
                                    <FormattedMessage id="PageOperationReportMember.Summary.TotalWinLossAmount" />
                                }
                                value={summary.TotalWinLossAmount}
                                precision={2}
                            />
                        </Col>
                    </Row>
                </>
            ) : (
                <Skeleton active />
            )}
        </div>
    )
}

export default SummaryView
