import React from 'react';
import { FormattedMessage } from 'react-intl';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.MemberName" description="会员帐号" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => {
      return <OpenMemberDetailButton memberId={record.MemberId} memberName={record.MemberName} />;
    },
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.BetOrderCnt" description="订单量" />,
    dataIndex: 'BetOrderCnt',
    key: 'BetOrderCnt',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.BetAmount" description="投注金额" />,
    dataIndex: 'BetAmount',
    key: 'BetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.BetAmount} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportMember.DataTable.Title.ValidBetAmount" description="有效投注金额" />
    ),
    dataIndex: 'ValidBetAmount',
    key: 'ValidBetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.ValidBetAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.BonusAmount" description="结算金额" />,
    dataIndex: 'BonusAmount',
    key: 'BonusAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.BonusAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.WinLossAmount" description="派彩(输赢)" />,
    dataIndex: 'WinLossAmount',
    key: 'WinLossAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.WinLossAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.DepositAmount" description="充值金额" />,
    dataIndex: 'DepositAmount',
    key: 'DepositAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.DepositAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.WithdrawalAmount" description="提现金额" />,
    dataIndex: 'WithdrawalAmount',
    key: 'WithdrawalAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.WithdrawalAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.DepWithAmount" description="存提差" />,
    dataIndex: 'DepWithAmount',
    key: 'DepWithAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.DepWithAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.DeductionAmount" description="扣款金额" />,
    dataIndex: 'DeductionAmount',
    key: 'DeductionAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.DeductionAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.RmAdditionAmount" description="风控补分" />,
    dataIndex: 'RmAdditionAmount',
    key: 'RmAdditionAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.RmAdditionAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.PrePaymentAmount" description="优惠金额" />,
    dataIndex: 'PrePaymentAmount',
    key: 'PrePaymentAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.PrePaymentAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.ReturnPointAmount" description="返点" />,
    dataIndex: 'ReturnPointAmount',
    key: 'ReturnPointAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.ReturnPointAmount} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.Fee" description="站台手续费" />,
    dataIndex: 'Fee',
    key: 'Fee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.AdministrationFee" description="行政費" />,
    dataIndex: 'AdministrationFee',
    key: 'AdministrationFee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageOperationReportMember.DataTable.Title.MemberBalance" description="会员余额" />,
    dataIndex: 'MemberBalance',
    key: 'MemberBalance',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.MemberBalance} />,
  },
  {
    title: (
      <FormattedMessage id="PageOperationReportMember.DataTable.Title.TotalWinLossAmount" description="会员盈亏" />
    ),
    dataIndex: 'TotalWinLossAmount',
    key: 'TotalWinLossAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.TotalWinLossAmount} />,
  },
];
