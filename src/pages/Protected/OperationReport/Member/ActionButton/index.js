import React, { useContext } from "react";
import { MemberDetailContext } from "contexts/MemberDetailContext";

export default function ActionButton({ memberId, memberName }) {
  const { onDisplayMemberDetail } = useContext(MemberDetailContext);

  return <a onClick={() => onDisplayMemberDetail(memberId)}>{memberName}</a>;
}
