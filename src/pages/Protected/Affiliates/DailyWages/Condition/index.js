import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { ConditionContext } from 'contexts/ConditionContext'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_YESTERDAY_TO_TODAY,
    GroupName: null,
    Status: [2],
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()
    const { RangePicker } = DatePicker

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [BeginDate, EndDate] = Date || [null, null]

        return {
            ...restValues,
            BeginDate: getISODateTimeString(BeginDate),
            EndDate: getISODateTimeString(EndDate),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesAgencyReport.QueryCondition.DateTime"
                                description="結算時間"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemsSimpleSelect
                        url="/api/Option/DayPayType"
                        name="Type"
                        label={
                            <FormattedMessage id="PageAffiliatesDailyWages.QueryCondition.Type" />
                        }
                    />
                </Col>
                <Col sm={8}>
                    <FormItemMultipleSelect
                        checkAll
                        name="AffiliateLevel"
                        label={
                            <FormattedMessage id="Share.QueryCondition.AffiliateLevel" />
                        }
                        url="/api/Option/AffiliateLevel?showAll=false"
                    />
                </Col>
                <Col sm={8}>
                    <FormItemMultipleSelect
                        name="Status"
                        label={
                            <FormattedMessage id="Share.QueryCondition.Status" />
                        }
                        url="/api/Option/DayPayStatus"
                    />
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageAffiliatesDailyWages.QueryCondition.GroupName" />
                        }
                        name="GroupName"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={8} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
