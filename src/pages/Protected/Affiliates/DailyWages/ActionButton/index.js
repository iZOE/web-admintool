import React, { useContext } from 'react'
import { Button, Popconfirm } from 'antd'
import { FormattedMessage } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import { DISPATCH_TYPE } from 'constants/dispatchType'
import Permission from 'components/Permission'
import { PageContext } from '../index'

const { AFFILIATES_DIVIDENDS_REJECT } = PERMISSION

export default function ButtonItems({ record }) {
  const { DayPayId, DayPay, MemberName } = record
  const { onDispatch } = useContext(PageContext)

  const onHandleDispatch = action =>
    onDispatch({
      Action: action,
      List: [DayPayId],
    })

  return (
    <>
      <Popconfirm
        title={
          <FormattedMessage
            id="Share.CommonDispatch.Popconfirm.Dispatch"
            values={{
              memberName: MemberName,
              commission: DayPay,
            }}
          />
        }
        onConfirm={() => onHandleDispatch(DISPATCH_TYPE.DISPATCH)}
        okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
        cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
        placement="topLeft"
      >
        <Button type="link">
          <FormattedMessage id="Share.ActionButton.Dispatch" />
        </Button>
      </Popconfirm>
      <Permission functionIds={[AFFILIATES_DIVIDENDS_REJECT]}>
        <Popconfirm
          title={
            <FormattedMessage
              id="Share.CommonDispatch.Popconfirm.Cancel"
              values={{
                memberName: MemberName,
                commission: DayPay,
              }}
            />
          }
          onConfirm={() => onHandleDispatch(DISPATCH_TYPE.REJECT)}
          okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
          cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
          placement="topLeft"
        >
          <Button type="link" danger>
            <FormattedMessage id="Share.ActionButton.Refuse" />
          </Button>
        </Popconfirm>
      </Permission>
    </>
  )
}
