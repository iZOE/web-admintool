import React from 'react';
import { Statistic, Row, Col, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';

const SummaryView = ({ summary }) => {
  return summary ? (
    <>
      <Row gutter={24}>
        <Col span={8}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliatesDailyWages.Summary.SumOfTotalBetAmount"
                description="總投注金額"
              />
            }
            value={summary.SumOfTotalBetAmount}
            precision={2}
          />
        </Col>
        <Col span={8}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliatesDailyWages.Summary.SumOfDayPay"
                description="獎勵總額"
              />
            }
            value={summary.SumOfDayPay}
            precision={2}
          />
        </Col>
        <Col span={8}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliatesDailyWages.Summary.SumOfDayPayDispatched"
                description="已派金额"
              />
            }
            value={summary.SumOfDayPayDispatched}
            precision={2}
          />
        </Col>
      </Row>
    </>
  ) : (
    <Skeleton active />
  );
};

export default SummaryView;
