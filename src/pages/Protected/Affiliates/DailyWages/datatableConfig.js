import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import ActionButton from './ActionButton';
import { DISPATCHABLE_STATUS } from './index';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.MemberName" description="團隊名稱" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.AffiliateLevelName" description="代理等級" />,
    dataIndex: 'AffiliateLevelIDName',
    key: 'AffiliateLevelIDName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.Type" description="類型" />,
    dataIndex: 'TypeName',
    key: 'TypeName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.CalcuDate" description="結算日期" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.CalcuDate" description="結算日期" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemCreateTime',
    key: 'SystemCreateTime',
    render: (_1, { SystemCreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.TotalBetAmount" description="團隊投注額" />,
    dataIndex: 'TotalBetAmount',
    key: 'TotalBetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.ReturnPointAmount" description="獎勵金額" />,
    dataIndex: 'DayPay',
    key: 'DayPay',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.ReturnPointDetail" description="活躍人數" />,
    dataIndex: 'ReturnPointDetail',
    key: 'ReturnPointDetail',
    isShow: true,
    render: (_1, { ValuableMemberCount, ValuableMemberThreshold }) => (
      <>{`${ValuableMemberCount} / ${ValuableMemberThreshold}`}</>
    ),
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'StatusName',
    key: 'StatusName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDailyWages.DataTable.Title.ModifyUser" description="異動人員" />,
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'manage',
    key: 'manage',
    fixed: 'right',
    render: (_1, record) =>
      record.Status === DISPATCHABLE_STATUS ? <ActionButton record={record} /> : record.StatusName,
  },
];
