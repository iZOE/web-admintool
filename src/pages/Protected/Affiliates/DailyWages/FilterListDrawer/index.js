import React, { useState, forwardRef } from "react";
import { Drawer, Tabs } from "antd";
import { FormattedMessage } from "react-intl";
import Transfer from "./Transfer";
import Report from "./Report";

const { TabPane } = Tabs;

const TAB_MAPPER = {
  transfer: <Transfer />,
  report: <Report />
};

export function FilterListDrawer({}, ref) {
  const [visible, setVisible] = useState(false);
  const [tabKey, setTabKey] = useState("transfer");

  ref.current = {
    onOpenDrawer
  };

  function onOpenDrawer() {
    setVisible(true);
  }

  function onCloseDrawer() {
    setVisible(false);
  }

  return (
    <Drawer
      width={700}
      headerStyle={{ border: "none" }}
      title={
        <Tabs
          size="large"
          defaultActiveKey="form"
          onChange={key => {
            setTabKey(key);
          }}
          tabBarStyle={{ margin: 0 }}
        >
          <TabPane
            tab={
              <FormattedMessage
                id="Share.CommonDrawer.FilterList.Tabs.Transfer"
                description="過濾名單"
              />
            }
            key="transfer"
          />
          <TabPane
            tab={
              <FormattedMessage
                id="Share.CommonDrawer.FilterList.Tabs.Report"
                description="異動紀錄"
              />
            }
            key="report"
          />
        </Tabs>
      }
      visible={visible}
      onClose={onCloseDrawer}
    >
      {TAB_MAPPER[tabKey] || TAB_MAPPER["transfer"]}
    </Drawer>
  );
}

export default forwardRef(FilterListDrawer);
