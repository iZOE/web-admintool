import React from 'react'
import { DatePicker, Descriptions, Form, Table, Row, Col, Input } from 'antd'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { IGNORE_TYPE } from 'constants/ignoreType'
import PleaseSearch from 'components/PleaseSearch'
import DateWithFormat from 'components/DateWithFormat'
import QueryButton from 'components/FormActionButtons/Query'
import { Wrapper } from './Styled'
import { getISODateTimeString } from 'mixins/dateTime'

const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const COLUMN_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'ModifyTime',
        key: 'ModifyTime',
        width: 200,
        render: (_1, { ModifyTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Modifier" />
        ),
        dataIndex: 'ModifyUserAccount',
        key: 'ModifyUserAccount',
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.ModifiedItem" />
        ),
        dataIndex: 'Action',
        key: 'Action',
        render: (_1, record, _3) => (
            <FormattedMessage
                id={`Share.CommonDrawer.FilterList.TransferTab.DataTable.Action.${record.Action}`}
            />
        ),
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Detail" />
        ),
        dataIndex: 'MemberName',
        key: 'MemberName',
    },
]

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
    Type: IGNORE_TYPE.DAILY_WAGES,
}

export default function ReportTab() {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
    } = useGetDataSourceWithSWR({ url: '/api/Affiliate/Bonus/IgnoreHistory' })

    function onFinish({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        onUpdateCondition.bySearch({
            ...restValues,
            StartTime: getISODateTimeString(start),
            EndTime: getISODateTimeString(end),
        })
    }

    return (
        <Wrapper>
            <Row style={{ padding: '0 16px' }}>
                <Col span={24}>
                    <Descriptions
                        title={
                            <FormattedMessage id="Share.QueryCondition.Title" />
                        }
                        column={1}
                    >
                        <Descriptions.Item>
                            <Form
                                onFinish={onFinish}
                                initialValues={CONDITION_INITIAL_VALUE}
                            >
                                <Row gutter={24}>
                                    <Col span={20}>
                                        <Form.Item
                                            label={
                                                <FormattedMessage id="Share.QueryCondition.DateRange" />
                                            }
                                            name="Date"
                                        >
                                            <RangePicker
                                                showTime
                                                bordered={false}
                                                format={FORMAT.DISPLAY.DEFAULT}
                                            />
                                        </Form.Item>
                                        <Form.Item hidden name="Type">
                                            <Input type="hidden" />
                                        </Form.Item>
                                    </Col>
                                    <Col span={4}>
                                        <QueryButton />
                                    </Col>
                                </Row>
                            </Form>
                        </Descriptions.Item>
                    </Descriptions>
                    <PleaseSearch display={condition}>
                        <Descriptions
                            title={
                                <FormattedMessage id="Share.Table.SearchResult" />
                            }
                            column={2}
                        >
                            <Descriptions.Item className="merged-column">
                                <Table
                                    loading={fetching}
                                    columns={COLUMN_CONFIG}
                                    dataSource={
                                        dataSource && dataSource.Container
                                    }
                                    pagination={false}
                                    size="small"
                                />
                            </Descriptions.Item>
                        </Descriptions>
                    </PleaseSearch>
                </Col>
            </Row>
        </Wrapper>
    )
}
