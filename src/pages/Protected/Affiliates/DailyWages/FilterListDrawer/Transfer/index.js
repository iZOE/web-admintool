import React, { useEffect, useState } from "react";
import { Transfer, Button, Table, Divider, message } from "antd";
import useSWR from "swr";
import axios from "axios";
import difference from "lodash.difference";
import { useIntl, FormattedMessage } from "react-intl";
import { IGNORE_TYPE } from "constants/ignoreType";
import caller from "utils/fetcher";

const tableColumns = [
  {
    dataIndex: "title",
    title: (
      <FormattedMessage
        id="Share.CommonDrawer.FilterList.TransferTab.DataTable.Title.MemberName"
        description="会员帐号"
      />
    )
  },
  {
    dataIndex: "description",
    title: (
      <FormattedMessage
        id="Share.CommonDrawer.FilterList.TransferTab.DataTable.Title.Type"
        description="类型"
      />
    )
  }
];

export default function TransferTab() {
  const intl = useIntl();
  const [targetKeys, setTargetKeys] = useState();

  const { data } = useSWR(
    `/api/Affiliate/Bonus/Ignore/${IGNORE_TYPE.DAILY_WAGES}`,
    url =>
      axios(url).then(res => {
        if (res.data.Data) {
          const dataSource = res.data.Data.DispatchList.concat(
            res.data.Data.IgnoreList
          ).map(x => ({
            key: x.MemberId,
            title: x.MemberName
          }));
          const targetKeys = res.data.Data.IgnoreList.map(x => x.MemberId);
          return {
            dataSource,
            targetKeys
          };
        }
        return {
          dataSource: []
        };
      })
  );

  useEffect(() => {
    setTargetKeys(data ? data.targetKeys : []);
  }, [data && data.targetKeys]);

  function handleChange(nextTargetKeys) {
    setTargetKeys(nextTargetKeys);
  }

  function handleFilterOption(inputValue, option) {
    return option.title.indexOf(inputValue) > -1;
  }

  function putData() {
    caller({
      method: "post",
      endpoint: "/api/Affiliate/Bonus/Ignore",
      body: {
        List: targetKeys,
        Type: IGNORE_TYPE.DAILY_WAGES
      }
    }).then(() => {
      message.success(
        intl.formatMessage({
          id: "Share.SuccessMessage.UpdateSuccess"
        })
      );
      // trigger(`/api/Member/${memberId}`);
      // form.resetFields();
    });
  }
  if (!data) {
    return null;
  }

  return (
    <>
      <Transfer
        showSearch
        dataSource={data.dataSource}
        titles={[
          <FormattedMessage
            id="Share.CommonDrawer.FilterList.TransferTab.Titles.DistributeList"
            description="派發名單"
          />,
          <FormattedMessage
            id="Share.CommonDrawer.FilterList.TransferTab.Titles.FilterList"
            description="過濾名單"
          />
        ]}
        targetKeys={targetKeys}
        filterOption={handleFilterOption}
        onChange={handleChange}
      >
        {({
          direction,
          filteredItems,
          onItemSelectAll,
          onItemSelect,
          selectedKeys: listSelectedKeys,
          disabled: listDisabled
        }) => {
          const rowSelection = {
            getCheckboxProps: item => ({
              disabled: listDisabled || item.disabled
            }),
            onSelectAll(selected, selectedRows) {
              const treeSelectedKeys = selectedRows
                .filter(item => !item.disabled)
                .map(({ key }) => key);
              const diffKeys = selected
                ? difference(treeSelectedKeys, listSelectedKeys)
                : difference(listSelectedKeys, treeSelectedKeys);
              onItemSelectAll(diffKeys, selected);
            },
            onSelect({ key }, selected) {
              onItemSelect(key, selected);
            },
            selectedRowKeys: listSelectedKeys
          };

          return (
            <Table
              rowSelection={rowSelection}
              columns={tableColumns}
              dataSource={filteredItems}
              size="small"
            />
          );
        }}
      </Transfer>
      <Divider />
      <div style={{ textAlign: "right" }}>
        <Button type="primary" onClick={putData}>
          <FormattedMessage id="Share.ActionButton.Save" />
        </Button>
      </div>
    </>
  );
}
