import React, { useState } from 'react'
import { Row, Col, Button, Table, Drawer } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import { DISPATCH_TYPE } from 'constants/dispatchType'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'
import { NO_DATA } from 'constants/noData'

const columns = [
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesAgencyReport.DataTable.Title.MemberName"
                description="團隊名稱"
            />
        ),
        dataIndex: 'MemberId',
        key: 'MemberId',
        isShow: true,
        fixed: 'left',
        width: 200,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => {
            return
        },
        render: (text, record) => (
            <OpenMemberDetailButton memberName={record.MemberName} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesAgencyReport.DataTable.Title.CalcuDate"
                description="結算日期"
            />
        ),
        dataIndex: 'CreateTime',
        key: 'CreateTime',
        width: 200,
        render: (_1, { CreateTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesAgencyReport.DataTable.Title.GameProviderTypeGroup"
                description="類型"
            />
        ),
        dataIndex: 'TypeName',
        key: 'TypeName',
        isShow: true,
        width: 100,
        render: (_1, record, _2) => record.TypeName,
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesAgencyReport.DataTable.Title.ReturnPointAmount"
                description="獎勵金額"
            />
        ),
        dataIndex: 'DayPay',
        key: 'DayPay',
        isShow: true,
        width: 200,
        render: value =>
            value ? (
                <ReactIntlCurrencyWithFixedDecimal value={value} />
            ) : (
                NO_DATA
            ),
    },
]

export default function ModalButton({
    dataSource,
    modalData,
    setModalData,
    onDispatch,
}) {
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const [totalAmount, setTotalAmount] = useState()

    const totalSelected = useIntl().formatMessage(
        {
            id: 'Share.CommonDrawer.BatchDispatch.Footer',
        },
        {
            target: selectedRowKeys.length,
            amount: <ReactIntlCurrencyWithFixedDecimal value={totalAmount} />,
        }
    )

    const closeModal = () =>
        setModalData({
            visible: false,
        })

    /**
     * 派發
     * @param {[dayPayId]}}
     */
    function handleDispatch() {
        const body = {
            Action: DISPATCH_TYPE.DISPATCH,
            List: selectedRowKeys.map(idx => {
                const { DayPayId } = dataSource[idx]
                return DayPayId
            }),
        }
        onDispatch(body)
    }

    const onSelectChange = selectedRowKeys => {
        const sumOfAmount =
            selectedRowKeys && selectedRowKeys.length > 0
                ? dataSource
                      .filter(({ Index }) => selectedRowKeys.includes(Index))
                      .map(({ DayPay }) => DayPay)
                      .reduce((a, b) => a + b)
                : 0

        setTotalAmount(sumOfAmount)
        setSelectedRowKeys(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    }
    const hasSelected = selectedRowKeys.length > 0

    return (
        <Drawer
            title={<FormattedMessage id="Share.ActionButton.BatchDispatch" />}
            width={700}
            onClose={() => closeModal()}
            visible={modalData.visible}
            footer={
                <Row align="middle">
                    <Col span={12}>
                        {hasSelected && (
                            <span style={{ marginLeft: 8 }}>
                                {totalSelected}
                            </span>
                        )}
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <Button
                            onClick={() => closeModal()}
                            style={{ marginRight: 8 }}
                        >
                            <FormattedMessage id="Share.ActionButton.Cancel" />
                        </Button>
                        <Button type="primary" onClick={handleDispatch}>
                            <FormattedMessage id="Share.ActionButton.Confirm" />
                        </Button>
                    </Col>
                </Row>
            }
        >
            <Table
                rowKey={record => record.Index}
                rowSelection={rowSelection}
                dataSource={
                    dataSource &&
                    dataSource.map((d, index) => {
                        d.Index = index
                        return d
                    })
                }
                columns={columns}
                pagination={false}
                size="small"
            />
        </Drawer>
    )
}
