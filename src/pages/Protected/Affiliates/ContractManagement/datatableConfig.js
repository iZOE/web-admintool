import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ActionButton from './ActionButton';

export const COLUMNS_CONFIG = [
  {
    title: (
      <FormattedMessage
        id="PageAffiliatesContractManagement.DataTable.Title.TopMemberName"
        description="團隊名稱 (上級)"
      />
    ),
    dataIndex: 'TopMemberName',
    key: 'TopMemberName',
    isShow: true,
    fixed: 'left',
  },
  {
    title: (
      <FormattedMessage
        id="PageAffiliatesContractManagement.DataTable.Title.MemberName"
        description="使用者帳號 (下級)"
      />
    ),
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: (
      <FormattedMessage id="PageAffiliatesContractManagement.DataTable.Title.ContractType" description="契約類型" />
    ),
    dataIndex: 'ContractType',
    key: 'ContractType',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (
      <FormattedMessage
        id={`PageAffiliatesContractManagement.QueryCondition.SelectItem.Type.${text}`}
        description="契約類型"
      />
    ),
  },
  {
    title: (
      <FormattedMessage
        id="PageAffiliatesContractManagement.DataTable.Title.ValuableMemberCount"
        description="有效人數條件"
      />
    ),
    dataIndex: 'ValuableMemberCount',
    key: 'ValuableMemberCount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: (
      <FormattedMessage id="PageAffiliatesContractManagement.DataTable.Title.ContractRatio" description="分潤比例" />
    ),
    dataIndex: 'ContractRatio',
    key: 'ContractRatio',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <FormattedNumber value={text / 100} style="percent" minimumFractionDigits="1" />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesContractManagement.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'IsEnabled',
    key: 'IsEnabled',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (
      <FormattedMessage
        id={`PageAffiliatesContractManagement.QueryCondition.SelectItem.IsEnabled.${text}`}
        description="狀態"
      />
    ),
  },

  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage id="PageAffiliatesContractManagement.DataTable.Title.ModifyUserName" description="更新人員" />
    ),
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: '',
    dataIndex: 'manage',
    key: 'manage',
    isShow: true,
    fixed: 'right',
    render: (_1, record) => record.IsEnabled && <ActionButton record={record} />,
  },
];
