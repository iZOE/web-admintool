import React, { useState, forwardRef, useContext, useEffect } from "react";
import { Drawer, Form, Button, message, Select } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import caller from "utils/fetcher";
import { PageContext } from "../index";
import FormItemValuableMemberCount from "../FormItems/ValuableMemberCount";
import FormItemContractRatio from "../FormItems/ContractRatio";
import { LAYOUT_CONFIG, VALID_RULES } from "../constants";

export const CONTRACT_TYPE_IDS = [5, 1, 3];

const { useForm } = Form;
const { Option } = Select;

export function CreateDrawer({}, ref) {
  const intl = useIntl();
  const [form] = useForm();
  const [visible, setVisible] = useState(false);
  const [contractType, setContractType] = useState(5);
  const {
    boundedMutate,
    topAffiliate,
    getContractMember,
    contractMember
  } = useContext(PageContext);
  const { Members, RemainingContractRatio } = contractMember;

  ref.current = {
    onOpenDrawer
  };

  function onOpenDrawer() {
    setVisible(true);
  }

  function onCloseDrawer() {
    setVisible(false);
  }

  function onFinish(values) {
    caller({
      method: "post",
      endpoint: "/api/Affiliate/ContractSetting/Create",
      body: {
        contractType,
        ...values
      }
    })
      .then(() => {
        message.success(
          intl.formatMessage({
            id: "Share.UIResponse.SaveSuccess",
            description: "编辑成功"
          }),
          10
        );
        onCloseDrawer();
        boundedMutate();
      })
      .catch(() => {
        message.warning(
          intl.formatMessage({
            id: "Share.ErrorMessage.UnknownError",
            description: "不明错误"
          }),
          10
        );
      });
  }

  useEffect(() => {
    form.validateFields(["TopMemberId"]);
  }, [form, Members]);

  return (
    <Drawer
      width={700}
      headerStyle={{ border: "none" }}
      title={
        <FormattedMessage id="PageAffiliatesContractManagement.Drawer.Create.Title" />
      }
      visible={visible}
      onClose={onCloseDrawer}
      footer={
        <div
          style={{
            textAlign: "right"
          }}
        >
          <Button onClick={onCloseDrawer} style={{ marginRight: 8 }}>
            <FormattedMessage id="Share.ActionButton.Cancel" />
          </Button>
          <Button type="primary" onClick={() => form.submit()}>
            <FormattedMessage id="Share.ActionButton.Confirm" />
          </Button>
        </div>
      }
    >
      <Form
        form={form}
        {...LAYOUT_CONFIG}
        labelCol={{
          xs: { span: 24 },
          sm: { span: 8 }
        }}
        wrapperCol={{ xs: { span: 24 }, sm: { span: 16 } }}
        onFinish={onFinish}
      >
        <Form.Item
          label={
            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.Type" />
          }
          name="ContractType"
        >
          <Select defaultValue={contractType} onChange={setContractType}>
            {CONTRACT_TYPE_IDS.map(id => (
              <Option value={id} key={id}>
                <FormattedMessage
                  id={`PageAffiliatesContractManagement.QueryCondition.SelectItem.Type.${id}`}
                />
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.TopMemberName" />
          }
          name="TopMemberId"
          rules={[
            VALID_RULES.REQUIRED,
            {
              validator: () => {
                if (VALID_RULES.ACCOUNT_NO_MEMBER(Members)) {
                  return Promise.reject(
                    <FormattedMessage id="PageAffiliatesContractManagement.FormValidate.AccountNoMember" />
                  );
                }
                if (VALID_RULES.ALL_MEMBER_HAVE_CONTRACT(Members)) {
                  return Promise.reject(
                    <FormattedMessage id="PageAffiliatesContractManagement.FormValidate.AllMemberHaveContract" />
                  );
                }
                return Promise.resolve();
              }
            }
          ]}
        >
          <Select
            showSearch
            placeholder={
              <FormattedMessage id="PageAffiliatesContractManagement.FormItems.Placeholder.PlsSelectAffiliates" />
            }
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            onChange={value =>
              getContractMember({
                TopMemberId: value,
                TopMemberName: topAffiliate.find(d => d.MemberId === value)
                  .MemberName,
                contractType
              })
            }
          >
            {topAffiliate &&
              topAffiliate.map(({ MemberName, MemberId }) => (
                <Option value={MemberId} key={MemberName}>
                  {MemberName}
                </Option>
              ))}
          </Select>
        </Form.Item>

        {Members && Members.length > 0 && (
          <>
            <Form.Item
              label={
                <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.MemberName" />
              }
              name="MemberId"
              rules={[
                VALID_RULES.REQUIRED,
                {
                  validator: () =>
                    VALID_RULES.ALL_MEMBER_HAVE_CONTRACT(Members)
                      ? Promise.reject(
                          <FormattedMessage id="PageAffiliatesContractManagement.FormValidate.AllMemberHaveContract" />
                        )
                      : Promise.resolve()
                }
              ]}
            >
              <Select
                showSearch
                placeholder={
                  <FormattedMessage id="PageAffiliatesContractManagement.FormItems.Placeholder.PlsSelectMember" />
                }
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                {Members.filter(({ HaveContract }) => !HaveContract).map(
                  ({ MemberName, MemberId }) => (
                    <Option value={MemberId} key={MemberName}>
                      {MemberName}
                    </Option>
                  )
                )}
              </Select>
            </Form.Item>
            <FormItemValuableMemberCount />
            <FormItemContractRatio
              remainingContractRatio={RemainingContractRatio}
            />
          </>
        )}
      </Form>
    </Drawer>
  );
}

export default forwardRef(CreateDrawer);
