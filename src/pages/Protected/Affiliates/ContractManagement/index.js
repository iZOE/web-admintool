import React, { createContext, useState, createRef } from 'react';
import { Button, Space } from 'antd';
import { FormattedMessage } from 'react-intl';
import useSWR from 'swr';
import axios from 'axios';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import caller from 'utils/fetcher';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import ExportReportButton from 'components/ExportReportButton';
import Condition from './Condition';
import CreateDrawer from './CreateDrawer';
import UpdateDrawer from './UpdateDrawer';
import { COLUMNS_CONFIG } from './datatableConfig';

const {
  AFFILIATES_CONTRACT_MANAGEMENT_VIEW,
  AFFILIATES_CONTRACT_MANAGEMENT_EXPORT,
  AFFILIATES_CONTRACT_MANAGEMENT_CREATE
} = PERMISSION;

export const PageContext = createContext();

const PageView = () => {
  let createDrawerRef = createRef();
  let updateDrawerRef = createRef();
  const [contractMember, setContractMember] = useState([]);

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    boundedMutate
  } = useGetDataSourceWithSWR({
    url: '/api/Affiliate/ContractSetting/Search',
    defaultSortKey: 'MemberName'
  });

  const { data: topAffiliate } = useSWR(
    '/api/Affiliate/topAffiliate/Search',
    url => axios(url).then(res => res.data.Data)
  );

  function getContractMember(value) {
    caller({
      method: 'post',
      endpoint: '/api/Affiliate/ContractMember/Search',
      body: value
    }).then(data => data && setContractMember(data.Data));
  }

  return (
    <PageContext.Provider
      value={{
        boundedMutate,
        topAffiliate,
        updateDrawerRef,
        contractMember,
        getContractMember: getContractMember
      }}
    >
      <Permission isPage functionIds={[AFFILIATES_CONTRACT_MANAGEMENT_VIEW]}>
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onUpdate={onUpdateCondition.bySearch} />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢絝果"
                />
              }
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource?.Data}
              total={dataSource?.TotalCount}
              extendArea={
                <Space>
                  <Permission
                    functionIds={[AFFILIATES_CONTRACT_MANAGEMENT_CREATE]}
                  >
                    <Button
                      condition={condition}
                      onClick={() => createDrawerRef.current.onOpenDrawer()}
                    >
                      <FormattedMessage id="PageAffiliatesContractManagement.ActionButton.Create" />
                    </Button>
                  </Permission>
                  <Permission
                    functionIds={[AFFILIATES_CONTRACT_MANAGEMENT_EXPORT]}
                  >
                    <ExportReportButton
                      condition={condition}
                      disabled={
                        !condition?.TopMemberName || !dataSource?.Data.length
                      }
                      actionUrl="/api/Affiliate/ContractSetting/Export"
                    />
                  </Permission>
                </Space>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
        <CreateDrawer ref={createDrawerRef} />
        <UpdateDrawer ref={updateDrawerRef} />
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
