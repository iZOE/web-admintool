import React, { useContext } from 'react'
import { Form, Row, Col, Input, Select } from 'antd'
import { FormattedMessage } from 'react-intl'
import { PageContext } from '../index'
import QueryButton from 'components/FormActionButtons/Query'

export const CONTRACT_TYPE_IDS = [0, 5, 1, 3]

const { useForm } = Form
const { Option } = Select

const CONDITION_INITIAL_VALUE = {
    ContractType: 0,
    TopMemberName: null,
    MemberName: null,
}

function Condition({ onUpdate }) {
    const [form] = useForm()
    const { topAffiliate } = useContext(PageContext)

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onUpdate}
        >
            <Row gutter={[16, 8]}>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.Type" />
                        }
                        name="ContractType"
                    >
                        <Select>
                            {CONTRACT_TYPE_IDS.map(id => (
                                <Option value={id} key={id || 'All'}>
                                    <FormattedMessage
                                        id={`PageAffiliatesContractManagement.QueryCondition.SelectItem.Type.${id ||
                                            'All'}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.TopMemberName" />
                        }
                        name="TopMemberName"
                    >
                        <Select
                            showSearch
                            placeholder={
                                <FormattedMessage id="PageAffiliatesContractManagement.FormItems.Placeholder.PlsSelectAffiliates" />
                            }
                            filterOption={(input, option) =>
                                option.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {topAffiliate &&
                                topAffiliate.map(({ MemberName }) => (
                                    <Option value={MemberName} key={MemberName}>
                                        {MemberName}
                                    </Option>
                                ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.MemberName" />
                        }
                        name="MemberName"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
