import React from "react";
import { FormattedMessage } from "react-intl";

export const LAYOUT_CONFIG = {
  labelCol: {
    span: 4
  },
  wrapperCol: {
    span: 12
  }
};

export const VALID_RULES = {
  REQUIRED: {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  },
  IS_NUMBER: new RegExp("^\\d+$"),
  ACCOUNT_NO_MEMBER: member => member.length === 0,
  ALL_MEMBER_HAVE_CONTRACT: member =>
    member.filter(({ HaveContract }) => !HaveContract).length === 0
};
