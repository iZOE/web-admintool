import React, { useState, forwardRef, useContext, useEffect } from "react";
import { Drawer, Form, Button, message } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import caller from "utils/fetcher";
import { PageContext } from "../index";
import FormItemValuableMemberCount from "../FormItems/ValuableMemberCount";
import FormItemContractRatio from "../FormItems/ContractRatio";
import { LAYOUT_CONFIG } from "../constants";

const { useForm } = Form;

export function UpdateDrawer({}, ref) {
  const intl = useIntl();
  const [form] = useForm();
  const { boundedMutate, getContractMember, contractMember } = useContext(
    PageContext
  );
  const [visible, setVisible] = useState(false);
  const [record, setRecord] = useState({});
  const {
    ContractType,
    TopMemberId,
    TopMemberName,
    MemberId,
    MemberName,
    ValuableMemberCount,
    ContractRatio
  } = record;

  ref.current = {
    onOpenDrawer
  };

  function onOpenDrawer({ record }) {
    setRecord(record);
    setVisible(true);
  }

  function onCloseDrawer() {
    setVisible(false);
  }

  function onFinish(values) {
    caller({
      method: "put",
      endpoint: "/api/Affiliate/ContractSetting/Update",
      body: {
        MemberId,
        TopMemberId,
        ContractType,
        ...values
      }
    })
      .then(() => {
        message.success(
          intl.formatMessage({
            id: "Share.UIResponse.SaveSuccess",
            description: "编辑成功"
          }),
          10
        );
        onCloseDrawer();
        boundedMutate();
      })
      .catch(() => {
        message.warning(
          intl.formatMessage({
            id: "Share.ErrorMessage.UnknownError",
            description: "不明错误"
          }),
          10
        );
      });
  }

  useEffect(() => {
    getContractMember({ ContractType, TopMemberId, TopMemberName });
  }, [form, record]);

  return (
    <Drawer
      width={700}
      headerStyle={{ border: "none" }}
      title={
        <FormattedMessage id="PageAffiliatesContractManagement.Drawer.Update.Title" />
      }
      visible={visible}
      onClose={onCloseDrawer}
      footer={
        <div
          style={{
            textAlign: "right"
          }}
        >
          <Button onClick={onCloseDrawer} style={{ marginRight: 8 }}>
            <FormattedMessage id="Share.ActionButton.Cancel" />
          </Button>
          <Button type="primary" onClick={() => form.submit()}>
            <FormattedMessage id="Share.ActionButton.Confirm" />
          </Button>
        </div>
      }
    >
      <Form
        form={form}
        {...LAYOUT_CONFIG}
        labelCol={{
          xs: { span: 24 },
          sm: { span: 8 }
        }}
        wrapperCol={{ xs: { span: 24 }, sm: { span: 16 } }}
        onFinish={onFinish}
        initialValues={{
          ValuableMemberCount,
          ContractRatio
        }}
      >
        <Form.Item
          label={
            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.Type" />
          }
        >
          <FormattedMessage
            id={`PageAffiliatesContractManagement.QueryCondition.SelectItem.Type.${ContractType}`}
          />
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.TopMemberName" />
          }
        >
          {TopMemberName}
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.MemberName" />
          }
        >
          {MemberName}
        </Form.Item>
        <FormItemValuableMemberCount />
        <FormItemContractRatio
          remainingContractRatio={contractMember.RemainingContractRatio}
        />
      </Form>
    </Drawer>
  );
}

export default forwardRef(UpdateDrawer);
