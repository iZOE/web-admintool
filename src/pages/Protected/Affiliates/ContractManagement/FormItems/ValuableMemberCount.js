import React from "react";
import { Form, InputNumber } from "antd";
import { FormattedMessage, useIntl } from "react-intl";
import { VALID_RULES } from "../constants";

export function ValuableMemberCount() {
  const PlsEnterNumber = useIntl().formatMessage({
    id: "PageAffiliatesContractManagement.FormItems.Placeholder.PlsEnterNumber"
  });
  return (
    <Form.Item
      label={
        <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.ValuableMemberCount" />
      }
      name="ValuableMemberCount"
      rules={[VALID_RULES.REQUIRED]}
    >
      <InputNumber
        placeholder={PlsEnterNumber}
        min={0}
        max={9999999}
        step={1}
        formatter={value =>
          VALID_RULES.IS_NUMBER.test(value) ? parseInt(value) : ""
        }
        style={{
          width: "100%"
        }}
      />
    </Form.Item>
  );
}

export default ValuableMemberCount;
