import React from "react";
import { Form, InputNumber } from "antd";
import { FormattedMessage, useIntl } from "react-intl";
import { VALID_RULES } from "../constants";

export function ContractRatio({ remainingContractRatio }) {
  const remainingContractRatioText = useIntl().formatMessage(
    {
      id:
        "PageAffiliatesContractManagement.FormItems.Placeholder.RemainingContractRatio"
    },
    { ratio: remainingContractRatio }
  );
  return (
    <Form.Item
      label={
        <FormattedMessage id="PageAffiliatesContractManagement.QueryCondition.ContractRatio" />
      }
      name="ContractRatio"
      rules={[
        VALID_RULES.REQUIRED,
        {
          validator: () =>
            remainingContractRatio === 0
              ? Promise.reject(
                  <FormattedMessage id="PageAffiliatesContractManagement.FormValidate.ContractRatioZero" />
                )
              : Promise.resolve()
        }
      ]}
    >
      <InputNumber
        placeholder={remainingContractRatioText}
        min={0}
        max={remainingContractRatio}
        step={0.1}
        style={{
          width: "100%"
        }}
      />
    </Form.Item>
  );
}

export default ContractRatio;
