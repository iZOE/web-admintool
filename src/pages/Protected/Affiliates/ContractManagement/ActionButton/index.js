import React, { useContext } from 'react';
import { Button, message, Popconfirm } from 'antd';
import { FormattedMessage } from 'react-intl';
import * as PERMISSION from 'constants/permissions';
import caller from 'utils/fetcher';
import Permission from 'components/Permission';
import { PageContext } from '../index';

const {
  AFFILIATES_CONTRACT_MANAGEMENT_EDIT,
  AFFILIATES_CONTRACT_MANAGEMENT_RESET_CONTRACT
} = PERMISSION;

export default function ButtonItems({ record }) {
  const {
    TopMemberName,
    MemberName,
    ContractType,
    MemberId,
    TopMemberId
  } = record;
  const { boundedMutate, updateDrawerRef } = useContext(PageContext);

  /**
   * 重置契約
   */
  const onReset = () => {
    caller({
      method: 'POST',
      endpoint: '/api/Affiliate/ContractSetting',
      body: {
        ContractType,
        MemberId,
        TopMemberId
      }
    }).then(() => {
      message.success('success');
      boundedMutate();
    });
  };

  return (
    <>
      <Permission functionIds={[AFFILIATES_CONTRACT_MANAGEMENT_EDIT]}>
        <Button
          type="link"
          onClick={() =>
            updateDrawerRef.current.onOpenDrawer({
              record
            })
          }
        >
          <FormattedMessage id="Share.ActionButton.Edit" />
        </Button>
      </Permission>
      <Permission functionIds={[AFFILIATES_CONTRACT_MANAGEMENT_RESET_CONTRACT]}>
        <Popconfirm
          title={
            <FormattedMessage
              id="PageAffiliatesContractManagement.Popconfirm.Reset"
              values={{
                TopMemberName,
                MemberName,
                ContractType: (
                  <FormattedMessage
                    id={`PageAffiliatesContractManagement.QueryCondition.SelectItem.Type.${ContractType}`}
                    description="契約類型"
                  />
                )
              }}
            />
          }
          onConfirm={() => onReset()}
          okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
          cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
          placement="topLeft"
        >
          <Button type="link" danger>
            <FormattedMessage
              id={`PageAffiliatesContractManagement.ActionButton.Reset`}
            />
          </Button>
        </Popconfirm>
      </Permission>
    </>
  );
}
