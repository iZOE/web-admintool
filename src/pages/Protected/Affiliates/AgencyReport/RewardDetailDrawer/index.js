import React, { useState, forwardRef } from 'react'
import { Drawer, Descriptions, Form, Table, Input, Row, Col } from 'antd'
import { FormattedMessage } from 'react-intl'
import { PAGESIZE_OPTIONS } from 'constants/forList'
import PleaseSearch from 'components/PleaseSearch'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'
import ExportReportButton from 'components/ExportReportButton'
import Pagination from 'components/Pagination'
import QueryButton from 'components/FormActionButtons/Query'

const COLUMN_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'CalcuDate',
        key: 'CalcuDate',
        width: 200,
        render: (_1, { CalcuDate: time }, _2) => <DateWithFormat time={time} />,
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Modifier" />
        ),
        dataIndex: 'MemberName',
        key: 'MemberName',
        width: 120,
        render: text => <OpenMemberDetailButton memberName={text} />,
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.ModifiedItem" />
        ),
        dataIndex: 'FromMemberLevel',
        key: 'FromMemberLevel',
        width: 120,
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Detail" />
        ),
        dataIndex: 'TotalBetAmount',
        key: 'TotalBetAmount',
        width: 120,
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Detail" />
        ),
        dataIndex: 'ReturnPointAmount',
        key: 'ReturnPointAmount',
        width: 120,
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
]

const INITIAL_SORT_CONDITION = {
    sortInfo: { SortColumn: null, SortBy: 'Desc' },
    paginationInfo: { pageNumber: 1, pageSize: 25 },
}

export function FilterListDrawer({}, ref) {
    const [visible, setVisible] = useState(false)
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
    } = useGetDataSourceWithSWR('/api/Affiliate/AllAgent/Detail/Search')

    function onFinish(values) {
        const newCondition = Object.assign(INITIAL_SORT_CONDITION, values)
        onUpdateCondition(newCondition)
    }

    ref.current = {
        onOpenDrawer,
    }

    function onOpenDrawer({ ToMemberId, CommissionCategory, CloseDate }) {
        setVisible(true)
        onUpdateCondition({
            ...INITIAL_SORT_CONDITION,
            ToMemberId,
            CommissionCategory,
            CloseDate,
        })
    }

    function onCloseDrawer() {
        setVisible(false)
    }

    return (
        <Drawer
            title={
                <FormattedMessage
                    id="PageAffiliatesAgencyReport.DataTable.Title.ReturnPointDetail"
                    description="獎勵詳情"
                />
            }
            width={700}
            visible={visible}
            onClose={onCloseDrawer}
        >
            <Descriptions
                title={<FormattedMessage id="Share.QueryCondition.Title" />}
                column={1}
            >
                <Descriptions.Item>
                    <Form onFinish={onFinish}>
                        <Row gutter={24}>
                            <Col span={22}>
                                <Form.Item
                                    label={
                                        <FormattedMessage id="Share.QueryCondition.MemberName" />
                                    }
                                    name="FromMemberName"
                                    initialValue={null}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col span={2}>
                                <QueryButton />
                            </Col>
                        </Row>
                    </Form>
                </Descriptions.Item>
            </Descriptions>
            <PleaseSearch display={condition}>
                <Descriptions
                    title={<FormattedMessage id="Share.Table.SearchResult" />}
                    column={1}
                >
                    <Descriptions.Item style={{ textAlign: 'right' }}>
                        <ExportReportButton
                            condition={condition}
                            actionUrl="/api/Affiliate/AllAgent/Detail/Export"
                        />
                    </Descriptions.Item>
                    <Descriptions.Item className="merged-column">
                        <Table
                            loading={fetching}
                            columns={COLUMN_CONFIG}
                            dataSource={dataSource && dataSource.Container}
                            pagination={false}
                            size="middle"
                            width="100%"
                        />
                    </Descriptions.Item>
                    <Descriptions.Item>
                        <div style={{ textAlign: 'center' }}>
                            <Pagination
                                totalCount={dataSource && dataSource.TotalCount}
                                pageSizeOptions={PAGESIZE_OPTIONS}
                                onUpdateCondition={onUpdateCondition}
                            />
                        </div>
                    </Descriptions.Item>
                </Descriptions>
            </PleaseSearch>
        </Drawer>
    )
}

export default forwardRef(FilterListDrawer)
