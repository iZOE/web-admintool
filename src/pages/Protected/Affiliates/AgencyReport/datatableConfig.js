import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import RewardDetailButton from './RewardDetailButton';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import ActionButton from './ActionButton';
import { DISPATCHABLE_STATUS } from './index';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.MemberName" description="團隊名稱" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: (
      <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.AffiliateLevelName" description="代理等級" />
    ),
    dataIndex: 'AffiliateLevelId',
    key: 'AffiliateLevelId',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => record.AffiliateLevelName,
  },
  {
    title: (
      <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.GameProviderTypeGroup" description="類型" />
    ),
    dataIndex: 'GameProviderTypeGroup',
    key: 'GameProviderTypeGroup',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return 'DeviceType';
    },
    render: (_1, record, _2) => record.GameProviderTypeGroupName,
  },
  {
    title: <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.CalcuDate" description="結算日期" />,
    dataIndex: 'CalcuDate',
    key: 'CalcuDate',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CalcuDate: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.CalcuDate" description="結算日期" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemCalcuDate',
    key: 'SystemCalcuDate',
    render: (_1, { SystemCalcuDate: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.TotalBetAmount" description="團隊投注額" />,
    dataIndex: 'TotalBetAmount',
    key: 'TotalBetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: (
      <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.ReturnPointAmount" description="獎勵金額" />
    ),
    dataIndex: 'ReturnPointAmount',
    key: 'ReturnPointAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    // Status !== 3 就會有獎勵詳情button
    title: (
      <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.ReturnPointDetail" description="獎勵詳情" />
    ),
    dataIndex: 'ReturnPointDetail',
    key: 'ReturnPointDetail',
    isShow: true,
    render: (_1, record) => <RewardDetailButton record={record} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => record.StatusName,
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyDate',
    key: 'ModifyDate',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyDate: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesAgencyReport.DataTable.Title.ModifyUser" description="異動人員" />,
    dataIndex: 'ModifyUserId',
    key: 'ModifyUserId',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.ModifyUserName,
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'manage',
    key: 'manage',
    fixed: 'right',
    render: (_1, record) =>
      record.Status === DISPATCHABLE_STATUS ? <ActionButton record={record} /> : record.StatusName,
  },
];
