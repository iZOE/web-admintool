import React, { useContext } from 'react'
import { Button, Space, Popconfirm } from 'antd'
import { FormattedMessage } from 'react-intl'
import Permission from 'components/Permission'
import * as PERMISSION from 'constants/permissions'
import { DISPATCH_TYPE } from 'constants/dispatchType'
import { PageContext } from '../index'

const {
  AFFILIATES_AGENCY_REPORT_REJECT,
  AFFILIATES_AGENCY_REPORT_DISPATCH,
} = PERMISSION

export default function ActionButton({ record }) {
  const { onDispatch } = useContext(PageContext)

  function handleDispatch(type) {
    const {
      CalcuDate: CreateDate,
      MemberId,
      GameProviderTypeGroup: Type,
    } = record

    const body = {
      Dispatch: type,
      DispatchDetails: [
        {
          CreateDate,
          MemberId,
          Type,
        },
      ],
    }

    onDispatch(body)
  }
  return (
    <Space>
      <Permission functionIds={[AFFILIATES_AGENCY_REPORT_DISPATCH]}>
        <Popconfirm
          title={
            <FormattedMessage
              id="Share.CommonDispatch.Popconfirm.Dispatch"
              values={{
                memberName: record.GroupName,
                commission: record.ReturnPointAmount,
              }}
            />
          }
          onConfirm={() => handleDispatch(DISPATCH_TYPE.DISPATCH)}
          okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
          cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
          placement="topLeft"
        >
          <Button type="primary">
            <FormattedMessage id="Share.ActionButton.Dispatch" />
          </Button>
        </Popconfirm>
      </Permission>
      <Permission functionIds={[AFFILIATES_AGENCY_REPORT_REJECT]}>
        <Popconfirm
          title={
            <FormattedMessage
              id="Share.CommonDispatch.Popconfirm.Cancel"
              values={{
                memberName: record.GroupName,
                commission: record.ReturnPointAmount,
              }}
            />
          }
          onConfirm={() => handleDispatch(DISPATCH_TYPE.REJECT)}
          okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
          cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
          placement="topLeft"
        >
          <Button type="link">
            <FormattedMessage id="Share.ActionButton.Refuse" />
          </Button>
        </Popconfirm>
      </Permission>
    </Space>
  )
}
