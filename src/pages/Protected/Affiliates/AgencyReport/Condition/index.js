import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { ConditionContext } from 'contexts/ConditionContext'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_YESTERDAY_TO_TODAY,
    Type: null,
    Status: [0],
    GroupName: null,
}

function Condition({ onUpdate, onReady }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()), true)
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            BeginDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesAgencyReport.QueryCondition.DateTime"
                                description="結算時間"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsSimpleSelect
                        needAll
                        url="/api/Option/AllAgent/Types"
                        name="Type"
                        label={<FormattedMessage id="Share.FormItem.Type" />}
                    />
                </Col>
                <Col sm={6}>
                    <FormItemMultipleSelect
                        url="/api/Option/AllAgent/Status"
                        name="Status"
                        label={<FormattedMessage id="Share.FormItem.Status" />}
                    />
                </Col>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesAgencyReport.QueryCondition.GroupName"
                                description="代理團隊"
                            />
                        }
                        name="GroupName"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
