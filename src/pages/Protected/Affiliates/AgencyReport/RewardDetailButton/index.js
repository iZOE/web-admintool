import React, { useContext } from 'react'
import { Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import { PageContext } from '../index'
import { NO_DATA } from 'constants/noData'

export default function RewardDetailButton({ record }) {
    const { rewardDetailDrawerRef } = useContext(PageContext)
    if (record.Status !== 3) {
        return (
            <Button
                type="link"
                style={{ padding: 0 }}
                onClick={() => {
                    rewardDetailDrawerRef.current.onOpenDrawer({
                        ToMemberId: record.MemberId,
                        CommissionCategory: record.GameProviderTypeGroup,
                        CloseDate: record.CalcuDate,
                    })
                }}
            >
                <FormattedMessage id="PageAffiliatesAgencyReport.ActionButton.ReturnPointDetail" />
            </Button>
        )
    }
    return NO_DATA
}
