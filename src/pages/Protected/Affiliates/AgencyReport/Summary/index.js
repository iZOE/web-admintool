import React from 'react';
import { Statistic, Row, Col, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';

const SummaryView = ({ summary }) => {
  return summary ? (
    <>
      <Row gutter={24}>
        <Col span={8}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliatesAgencyReport.Summary.TotalBetAmount"
                description="總投注金額"
              />
            }
            value={summary.TotalBetAmount}
            precision={2}
          />
        </Col>
        <Col span={8}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliatesAgencyReport.Summary.ReturnPointAmount"
                description="獎勵總額"
              />
            }
            value={summary.ReturnPointAmount || 0}
            precision={2}
          />
        </Col>
        <Col span={8}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliatesAgencyReport.Summary.ReturnPointAmountDispatched"
                description="已派金額"
              />
            }
            value={summary.ReturnPointAmountDispatched || 0}
            precision={2}
          />
        </Col>
      </Row>
    </>
  ) : (
    <Skeleton active />
  );
};

export default SummaryView;
