import React from 'react'
import { DatePicker, Descriptions, Form, Table, Row, Col } from 'antd'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import DateWithFormat from 'components/DateWithFormat'
import PleaseSearch from 'components/PleaseSearch'
import QueryButton from 'components/FormActionButtons/Query'
import { Wrapper } from './Styled'

const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const COLUMN_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'modifiedTime',
        key: 'modifiedTime',
        width: 200,
        render: (_1, { modifiedTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Modifier" />
        ),
        dataIndex: 'modifier',
        key: 'modifier',
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.ModifiedItem" />
        ),
        dataIndex: 'modifiedItem',
        key: 'modifiedItem',
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Detail" />
        ),
        dataIndex: 'detail',
        key: 'detail',
    },
]

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
}

export default function ReportTab() {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/Affiliate/AllAgent/IgnoreHistory',
        defaultSortKey: 'modifiedTime',
    })

    function onFinish({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        onUpdateCondition.bySearch({
            ...restValues,
            StartTime: getISODateTimeString(start),
            EndTime: getISODateTimeString(end),
        })
    }

    return (
        <Wrapper>
            <Row style={{ padding: '0 16px' }}>
                <Col span={24}>
                    <Descriptions
                        title={
                            <FormattedMessage id="Share.QueryCondition.Title" />
                        }
                        column={1}
                    >
                        <Descriptions.Item>
                            <Form
                                onFinish={onFinish}
                                initialValues={CONDITION_INITIAL_VALUE}
                            >
                                <Row gutter={24}>
                                    <Col span={20}>
                                        <Form.Item
                                            label={
                                                <FormattedMessage id="Share.QueryCondition.DateRange" />
                                            }
                                            name="Date"
                                        >
                                            <RangePicker
                                                showTime
                                                bordered={false}
                                                format={FORMAT.DISPLAY.DEFAULT}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col span={4}>
                                        <QueryButton />
                                    </Col>
                                </Row>
                            </Form>
                        </Descriptions.Item>
                    </Descriptions>
                    <PleaseSearch display={condition}>
                        <Descriptions
                            title={
                                <FormattedMessage id="Share.Table.SearchResult" />
                            }
                            column={2}
                        >
                            <Descriptions.Item className="merged-column">
                                <Table
                                    loading={fetching}
                                    columns={COLUMN_CONFIG}
                                    dataSource={
                                        dataSource && dataSource.Container
                                    }
                                    pagination={false}
                                    size="middle"
                                    width="100%"
                                />
                            </Descriptions.Item>
                        </Descriptions>
                    </PleaseSearch>
                </Col>
            </Row>
        </Wrapper>
    )
}
