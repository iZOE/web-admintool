import React, { createContext, useState, createRef } from 'react';
import { Button, Result, Space, message } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import caller from 'utils/fetcher';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import ExportReportButton from 'components/ExportReportButton';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import SummaryView from './Summary';
import FilterListDrawer from './FilterListDrawer';
import RewardDetailDrawer from './RewardDetailDrawer';
import BatchDispatch from './BatchDispatch';

const {
  AFFILIATES_AGENCY_REPORT_VIEW,
  AFFILIATES_AGENCY_REPORT_DISPATCH,
  AFFILIATES_AGENCY_REPORT_EXPORT,
  AFFILIATES_AGENCY_REPORT_BATCH_DISPATCH
} = PERMISSION;

export const PageContext = createContext();

export const DISPATCHABLE_STATUS = 0;

const PageView = () => {
  let filterListDrawerRef = createRef();
  let rewardDetailDrawerRef = createRef();

  // 批次派發按鈕
  const [batchDispatch, setBatchDispatch] = useState({
    visible: false
  });

  const successMsg = useIntl().formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess'
  });
  const errorMsg = useIntl().formatMessage({
    id: 'Share.ErrorMessage.UnknownError'
  });

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    onReady,
    condition,
    boundedMutate
  } = useGetDataSourceWithSWR({
    url: '/api/Affiliate/AllAgent/Search',
    defaultSortKey: 'CalcuDate',
    autoFetch: true
  });

  function handleDispatch(value) {
    caller({
      method: 'post',
      endpoint: '/api/Affiliate/AllAgent/Dispatch',
      body: value
    })
      .then(() => {
        message.success(successMsg);
        setBatchDispatch({ visible: false });
        boundedMutate();
      })
      .catch(err => {
        message.error(errorMsg);
      });
  }

  return (
    <PageContext.Provider
      value={{ rewardDetailDrawerRef, onDispatch: handleDispatch }}
    >
      <Permission
        isPage
        functionIds={[AFFILIATES_AGENCY_REPORT_VIEW]}
        failedRender={
          <Result
            status="warning"
            title={<FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />}
            subTitle={
              <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
            }
          />
        }
      >
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          summaryComponent={
            <SummaryView
              summary={dataSource && dataSource.Summary}
              totalCount={dataSource && dataSource.TotalCount}
            />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢絝果"
                />
              }
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource?.Container}
              total={dataSource?.TotalCount}
              extendArea={
                <Space>
                  <Permission functionIds={[AFFILIATES_AGENCY_REPORT_DISPATCH]}>
                    <Button
                      onClick={() => filterListDrawerRef.current.onOpenDrawer()}
                    >
                      <FormattedMessage id="Share.ActionButton.EditFilterList" />
                    </Button>
                  </Permission>
                  <Permission functionIds={[AFFILIATES_AGENCY_REPORT_EXPORT]}>
                    <ExportReportButton
                      condition={condition}
                      actionUrl="/api/Affiliate/AllAgent/Export"
                    />
                  </Permission>
                  <Permission
                    functionIds={[AFFILIATES_AGENCY_REPORT_BATCH_DISPATCH]}
                  >
                    <Button
                      condition={condition}
                      onClick={() => setBatchDispatch({ visible: true })}
                    >
                      <FormattedMessage id="Share.ActionButton.BatchDispatch" />
                    </Button>
                  </Permission>
                </Space>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
        <FilterListDrawer ref={filterListDrawerRef} />
        <RewardDetailDrawer ref={rewardDetailDrawerRef} />
        <BatchDispatch
          modalData={batchDispatch}
          dataSource={
            dataSource &&
            dataSource.Container.filter(x => x.Status === DISPATCHABLE_STATUS)
          }
          setModalData={data => setBatchDispatch(data)}
          onDispatch={handleDispatch}
        />
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
