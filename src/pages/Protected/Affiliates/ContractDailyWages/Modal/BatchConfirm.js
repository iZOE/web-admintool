import React, { useContext } from "react";
import caller from "utils/fetcher";
import { Modal, Skeleton, message } from "antd";
import { FormattedMessage, useIntl } from "react-intl";
import { PageContext } from "../index";

export default function BatchConfirmModal({
  target,
  contract,
  visible,
  record
}) {
  const { onCancelBatchConfirmModal } = useContext(PageContext);

  const successMsg = useIntl().formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });

  function onDispatch() {
    caller({
      method: "put",
      endpoint: `/api/ContractDailyWages/Dispatch`,
      body: record
    }).then(() => {
      onCancelBatchConfirmModal();
      message.success(successMsg);
    });
  }

  return (
    <Modal
      title={
        <FormattedMessage
          id={`PageAffiliates.Contract.Modal.DailyWages.Title.Dispatch`}
          description="批次派发契约日工资"
        />
      }
      visible={visible}
      width={500}
      onOk={onDispatch}
      onCancel={onCancelBatchConfirmModal}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      {record ? (
        <>
          <FormattedMessage
            id="PageAffiliates.Contract.Modal.DailyWages.BatchDispatch"
            description="是否派发契约日工资"
            values={{ target, contract }}
          />
        </>
      ) : (
        <Skeleton />
      )}
    </Modal>
  );
}
