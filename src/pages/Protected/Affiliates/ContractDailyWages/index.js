import React, { useState, createContext } from 'react'
import { Button, Result } from 'antd'
import { ExportOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ReportScaffold from 'components/ReportScaffold'
import ExportReportButton from 'components/ExportReportButton'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import SummaryView from './Summary'
import ConfirmModal from './Modal/Confirm'
import BatchModal from './Modal/Batch'
import BatchConfirmModal from './Modal/BatchConfirm'

const {
    AFFILIATES_CONTRACT_DAILY_WAGES_VIEW,
    AFFILIATES_CONTRACT_DAILY_WAGES_EXPORT,
} = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        onReady,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/ContractDailyWages/Search',
        defaultSortKey: 'CalculateEndTime',
        autoFetch: true,
    })
    const [confirmModalData, setConfirmModalData] = useState({ visible: false })
    const [batchConfirmModalData, setBatchConfirmModalData] = useState({
        visible: false,
    })
    const [batchModalData, setBatchModalData] = useState({ visible: false })

    const batchButtonDisable =
        dataSource &&
        dataSource.Container.filter(item => item.Status === 2).length === 0

    function onCancelConfirmModal() {
        setConfirmModalData({
            visible: false,
            record: null,
            dispatchStatus: 'Dispatch',
        })
    }

    function onCancelBatchModal() {
        setBatchModalData({ visible: false, record: null })
    }

    function onCancelBatchConfirmModal() {
        setBatchConfirmModalData({ visible: false, record: null })
    }

    return (
        <PageContext.Provider
            value={{
                onCancelConfirmModal,
                onCancelBatchModal,
                setConfirmModalData,
                setBatchModalData,
                setBatchConfirmModalData,
                onCancelBatchConfirmModal,
            }}
        >
            <Permission
                functionIds={[AFFILIATES_CONTRACT_DAILY_WAGES_VIEW]}
                failedRender={
                    <Result
                        status="warning"
                        title={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                        }
                        subTitle={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                        }
                    />
                }
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    summaryComponent={
                        <SummaryView
                            summary={dataSource && dataSource.Summary}
                            totalCount={
                                dataSource && dataSource.Summary.TotalCount
                            }
                            TotalPeriodDividendContract={
                                dataSource &&
                                dataSource.Summary.TotalPeriodDividendContract
                            }
                            TotalRealDividendContract={
                                dataSource &&
                                dataSource.Summary.TotalRealDividendContract
                            }
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查询结果"
                                />
                            }
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Container}
                            total={dataSource && dataSource.Summary.TotalCount}
                            rowKey={record => record.DividendContractDetailId}
                            extendArea={
                                <Permission
                                    functionIds={[
                                        AFFILIATES_CONTRACT_DAILY_WAGES_EXPORT,
                                    ]}
                                    failedRender={
                                        <Button
                                            type="primary"
                                            icon={<ExportOutlined />}
                                            disabled
                                        >
                                            <FormattedMessage id="Share.ActionButton.Export" />
                                        </Button>
                                    }
                                >
                                    <Button
                                        type="primary"
                                        style={{ marginRight: 16 }}
                                        condition={condition}
                                        disabled={batchButtonDisable}
                                        onClick={() =>
                                            setBatchModalData({ visible: true })
                                        }
                                    >
                                        <FormattedMessage id="Share.ActionButton.BatchDispatch" />
                                    </Button>
                                    <ExportReportButton
                                        condition={condition}
                                        actionUrl="/api/ContractDailyWages/Export"
                                    />
                                </Permission>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
            <BatchModal
                {...batchModalData}
                visible={batchModalData.visible}
                condition={condition}
                dataSource={dataSource && dataSource}
                setBatchModalData={data => setBatchModalData(data)}
            />
            <ConfirmModal
                {...confirmModalData}
                setConfirmModalData={data => setConfirmModalData(data)}
            />
            <BatchConfirmModal
                {...batchConfirmModalData}
                setBatchConfirmModalData={data =>
                    setBatchConfirmModalData(data)
                }
            />
        </PageContext.Provider>
    )
}

export default PageView
