import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 1rem;
  margin-bottom: 1rem;
  background: #fff;
`;
