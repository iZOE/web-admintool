import React, { useContext, useState } from "react";
import { Modal, Result, Card, Statistic, Row, Col, Table } from "antd";
import * as PERMISSION from "constants/permissions";
import { FormattedMessage } from "react-intl";
import ReactIntlCurrencyWithFixedDecimal from "components/ReactIntlCurrencyWithFixedDecimal";
import Permission from "components/Permission";
import { PageContext } from "../index";

const { AFFILIATES_CONTRACT_DAILY_DIVIDENDS_VIEW } = PERMISSION;
const COLUMNS_CONFIG = [
  {
    title: (
      <FormattedMessage
        id="PageAffiliates.Contract.ColumnName.DividendContractDetailId"
        description="契约 ID"
      />
    ),
    dataIndex: "DividendContractDetailId",
    key: "DividendContractDetailId",
    isShow: true,
    width: 150,
    sortDirections: ["descend", "ascend"]
  },
  {
    title: (
      <FormattedMessage
        id="PageAffiliates.Contract.ColumnName.MemberName"
        description="使用者帐号 (下级)"
      />
    ),
    dataIndex: "MemberName",
    key: "MemberName",
    isShow: true,
    width: 200,
    sortDirections: ["descend", "ascend"]
  },
  {
    title: (
      <FormattedMessage
        id="PageAffiliates.Contract.ColumnName.PeriodDividendContract"
        description="应派发金额"
      />
    ),
    dataIndex: "PeriodDividendContract",
    key: "PeriodDividendContract",
    isShow: true,
    width: 200,
    sortDirections: ["descend", "ascend"],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />
  }
];

export default function BatchModal({ dataSource, visible }) {
  const { onCancelBatchModal, setBatchConfirmModalData } = useContext(
    PageContext
  );
  const [totalCount, setTotalCount] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const batchDispatchList =
    dataSource && dataSource.Container.filter(item => item.Status === 2);

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };

  function onSelectChange(selectedRowKeys) {
    const filterData = batchDispatchList.filter(
      item => selectedRowKeys.indexOf(item.DividendContractDetailId) !== -1
    );
    setSelectedRowKeys(selectedRowKeys);
    setTotalCount(selectedRowKeys.length);
    setTotalAmount(
      filterData
        .map(item => item.PeriodDividendContract)
        .reduce((a, b) => a + b, 0)
    );
  }

  function onOk() {
    setBatchConfirmModalData({
      visible: true,
      target: totalCount,
      contract: totalAmount.toFixed(2, 1),
      record: { DividendContractDetailIds: selectedRowKeys }
    });
    onCancelBatchModal();
  }

  return (
    <Modal
      title={
        <FormattedMessage
          id={`Share.ActionButton.BatchDispatch`}
          description="批次派发 契约日分红"
        />
      }
      visible={visible}
      width={800}
      height={700}
      onOk={onOk}
      onCancel={onCancelBatchModal}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Permission
        functionIds={[AFFILIATES_CONTRACT_DAILY_DIVIDENDS_VIEW]}
        failedRender={
          <Result
            status="warning"
            title={<FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />}
            subTitle={
              <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
            }
          />
        }
      >
        <Card bordered={false} className="card">
          <Row gutter={24}>
            <Col span={12}>
              <Statistic
                title={
                  <FormattedMessage
                    id="PageAffiliates.Contract.Modal.Number"
                    description="选择人数"
                  />
                }
                value={totalCount}
              />
            </Col>
            <Col span={12}>
              <Statistic
                title={
                  <FormattedMessage
                    id="PageAffiliates.Contract.Summary.TotalRealDividendContract"
                    description="派发总金额"
                  />
                }
                value={totalAmount}
                precision={2}
              />
            </Col>
          </Row>
          <Table
            size="middle"
            columns={COLUMNS_CONFIG}
            dataSource={batchDispatchList}
            scroll={{ x: "100%", y: 300 }}
            rowKey={record => record.DividendContractDetailId}
            rowSelection={rowSelection}
            pagination={false}
          />
        </Card>
      </Permission>
    </Modal>
  );
}
