import React, { useContext } from "react";
import caller from "utils/fetcher";
import { Modal, Skeleton, message } from "antd";
import { FormattedMessage, useIntl } from "react-intl";
import { PageContext } from "../index";

export default function ConfirmModal({ record, visible, dispatchStatus }) {
  const { onCancelConfirmModal } = useContext(PageContext);
  const target = record && record.MemberName;
  const contract = record && record.PeriodDividendContract;

  const successMsg = useIntl().formatMessage({
    id: "Share.SuccessMessage.UpdateSuccess"
  });

  function onDispatch() {
    caller({
      method: "put",
      endpoint: `/api/ContractDailyDividends/Dispatch`,
      body: {
        DividendContractDetailIds: [record && record.DividendContractDetailId]
      }
    }).then(() => {
      onCancelConfirmModal();
      message.success(successMsg);
    });
  }

  function onReject() {
    caller({
      method: "put",
      endpoint: `/api/ContractDailyDividends/Reject`,
      body: {
        DividendContractDetailId: record && record.DividendContractDetailId
      }
    }).then(() => {
      onCancelConfirmModal();
      message.success(successMsg);
    });
  }

  return (
    <Modal
      title={
        <FormattedMessage
          id={`PageAffiliates.Contract.Modal.DailyDividends.Title.${dispatchStatus}`}
          description="契约日工资"
        />
      }
      visible={visible}
      width={500}
      onOk={dispatchStatus === "Dispatch" ? onDispatch : onReject}
      onCancel={onCancelConfirmModal}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      {record ? (
        <>
          {dispatchStatus === "Dispatch" ? (
            <FormattedMessage
              id="PageAffiliates.Contract.Modal.DailyDividends.Dispatch"
              description="是否派发契约日工资"
              values={{ target, contract }}
            />
          ) : (
            <FormattedMessage
              id="PageAffiliates.Contract.Modal.DailyDividends.Reject"
              description="是否取消契约日工资"
              values={{ target, contract }}
            />
          )}
        </>
      ) : (
        <Skeleton />
      )}
    </Modal>
  );
}
