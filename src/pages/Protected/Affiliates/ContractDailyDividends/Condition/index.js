import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, DatePicker } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { ConditionContext } from 'contexts/ConditionContext'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_YESTERDAY_TO_TODAY,
    TopMemberName: null,
    Status: [2],
    MemberName: null,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const intl = useIntl()
    const [form] = useForm()
    const { RangePicker } = DatePicker

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [BeginDate, EndDate] = Date || [null, null]

        return {
            ...restValues,
            BeginDate: getISODateTimeString(BeginDate),
            EndDate: getISODateTimeString(EndDate),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliates.Contract.QueryCondition.DataRange"
                                description="结算日期"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemMultipleSelect
                        url="/api/Option/DividendContractDetailStatus"
                        name="Status"
                        label={<FormattedMessage id="Share.FormItem.Status" />}
                    />
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageAffiliates.Contract.QueryCondition.AffiliateName" />
                        }
                        name="TopMemberName"
                    >
                        <Input
                            placeholder={intl.formatMessage({
                                id:
                                    'PageAffiliates.Contract.QueryList.AffiliateNameInstruction',
                                description: '请输入代理商名称',
                            })}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageAffiliates.Contract.QueryCondition.MemberName" />
                        }
                        name="MemberName"
                    >
                        <Input
                            placeholder={intl.formatMessage({
                                id:
                                    'PageAffiliates.Contract.QueryList.MemberNameInstruction',
                                description: '请输入使用者帐号',
                            })}
                        />
                    </Form.Item>
                </Col>
                <Col sm={{ span: 8, offset: 8 }} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
