import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 24px 24px 0;
  background: #ffffff;
  .floatRight {
    float: right;
  }
  .includeInnerMember {
    width: 8rem;
  }
`;
