import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import ActionButtons from './ActionButtons';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.DividendContractDetailId" description="契约 ID" />,
    dataIndex: 'DividendContractDetailId',
    key: 'DividendContractDetailId',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.TopMemberName" description="团队名称 (上级)" />,
    dataIndex: 'TopMemberName',
    key: 'TopMemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.MemberName" description="使用者帐号 (下级)" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.DataRangeMonthly" description="结算月份" />,
    dataIndex: 'CalculateEndTime',
    key: 'CalculateEndTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CalculateStartTime: s, CalculateEndTime: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageAffiliates.Contract.ColumnName.DataRangeMonthly" description="结算月份" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemCalculateEndTime',
    key: 'SystemCalculateEndTime',
    render: (_1, { SystemCalculateStartTime: s, SystemCalculateEndTime: e }, _2) => (
      <DateRangeWithIcon startTime={s} endTime={e} />
    ),
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.ValuableMemberCount" description="有效人数条件" />,
    dataIndex: 'ValuableMemberCount',
    key: 'ValuableMemberCount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: (
      <FormattedMessage id="PageAffiliates.Contract.ColumnName.RealVaulableMemberCount" description="实际有效人数" />
    ),
    dataIndex: 'RealVaulableMemberCount',
    key: 'RealVaulableMemberCount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.ContractRatio" description="分润比例" />,
    dataIndex: 'ContractRatio',
    key: 'ContractRatio',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (text, record, index) => (
      <FormattedNumber value={record.ContractRatio / 100} style="percent" minimumFractionDigits={1} />
    ),
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.IsVaild" description="是否达标" />,
    dataIndex: 'IsVaild',
    key: 'IsVaild',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (
      <FormattedMessage id={`PageAffiliates.Contract.QueryList.IsVaild.${text}`} description="是否达标" />
    ),
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.PeriodDividendContract" description="应派发金额" />,
    dataIndex: 'PeriodDividendContract',
    key: 'PeriodDividendContract',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.RealDividendContract" description="实际派发金额" />,
    dataIndex: 'RealDividendContract',
    key: 'RealDividendContract',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.Status" description="状态" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (
      <FormattedMessage id={`PageAffiliates.Contract.QueryList.DispatchStatus.${text}`} description="状态" />
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageAffiliates.Contract.ColumnName.ModifyUserName" description="更新人员" />,
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => text || NO_DATA,
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'Action',
    key: 'Action',
    fixed: 'right',
    render: (text, record, index) => <ActionButtons record={record} />,
  },
];
