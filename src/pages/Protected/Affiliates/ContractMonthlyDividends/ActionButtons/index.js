import React, { useContext } from 'react';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import { FormattedMessage } from 'react-intl';
import { Button } from 'antd';
import { PageContext } from '../index';

const {
  AFFILIATES_CONTRACT_MONTHLY_DIVIDENDS_DISPATCH_BATCH_DISPATCH,
  AFFILIATES_CONTRACT_MONTHLY_DIVIDENDS_REJECT
} = PERMISSION;

export default function ActionButtons({ record }) {
  const { setConfirmModalData } = useContext(PageContext);

  return (
    <>
      <Permission
        functionIds={[
          AFFILIATES_CONTRACT_MONTHLY_DIVIDENDS_DISPATCH_BATCH_DISPATCH
        ]}
      >
        <Button
          type="link"
          onClick={() =>
            setConfirmModalData({
              visible: true,
              record,
              dispatchStatus: 'Dispatch'
            })
          }
          disabled={record.Status !== 2}
        >
          <FormattedMessage
            id="PageAffiliates.Contract.Action.Dispatch"
            description="派发"
          />
        </Button>
      </Permission>
      <Permission functionIds={[AFFILIATES_CONTRACT_MONTHLY_DIVIDENDS_REJECT]}>
        <Button
          type="link"
          onClick={() =>
            setConfirmModalData({
              visible: true,
              record,
              dispatchStatus: 'Reject'
            })
          }
          disabled={record.Status !== 2}
        >
          <FormattedMessage
            id="PageAffiliates.Contract.Action.Reject"
            description="拒绝"
          />
        </Button>
      </Permission>
    </>
  );
}
