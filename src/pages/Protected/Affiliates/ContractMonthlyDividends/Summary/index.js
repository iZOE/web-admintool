import React from 'react';
import { Statistic, Row, Col, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';

const SummaryView = ({
  summary,
  TotalPeriodDividendContract,
  TotalRealDividendContract
}) => {
  return summary ? (
    <>
      <Row gutter={24}>
        <Col span={12}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliates.Contract.Summary.TotalPeriodDividendContract"
                description="应派发总金额"
              />
            }
            value={TotalPeriodDividendContract}
            precision={2}
          />
        </Col>
        <Col span={12}>
          <Statistic
            title={
              <FormattedMessage
                id="PageAffiliates.Contract.Summary.TotalRealDividendContract"
                description="实际派发总金额"
              />
            }
            value={TotalRealDividendContract}
            precision={2}
          />
        </Col>
      </Row>
    </>
  ) : (
    <Skeleton active />
  );
};

export default SummaryView;
