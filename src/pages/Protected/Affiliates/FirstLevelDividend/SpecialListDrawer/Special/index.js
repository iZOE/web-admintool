import React, { useState } from 'react';
import {
  Descriptions,
  Button,
  Divider,
  message,
  Form,
  Row,
  Col,
  Input
} from 'antd';
import QueryButton from 'components/FormActionButtons/Query';
import { useIntl, FormattedMessage } from 'react-intl';
import caller from 'utils/fetcher';
import EditTable from './EditableTable';

export default function TransferTab() {
  const intl = useIntl();
  const [data, setData] = useState([]);
  const [updateList, setUpdateList] = useState([]);

  function getSpecialList({ memberName }) {
    caller({
      method: 'get',
      endpoint: `/api/Affiliate/Bonus/SpecialList/${memberName}`
    }).then(res => {
      setData(res.Data);
      message.success(
        intl.formatMessage({
          id: 'Share.SuccessMessage.UpdateSuccess'
        })
      );
    });
  }

  function postData() {
    caller({
      method: 'post',
      endpoint: '/api/Affiliate/Bonus/SpecialList',
      body: {
        UpdateList: updateList
      }
    }).then(res => {
      console.log(res);
      message.success(
        intl.formatMessage({
          id: 'Share.SuccessMessage.UpdateSuccess'
        })
      );
    });
  }

  if (!data) {
    return null;
  }

  return (
    <>
      <Row>
        <Col span={24}>
          <Descriptions
            title={<FormattedMessage id="Share.QueryCondition.Title" />}
            column={1}
          >
            <Descriptions.Item>
              <Form onFinish={getSpecialList}>
                <Row gutter={24}>
                  <Col span={20}>
                    <Form.Item
                      label={
                        <FormattedMessage id="Share.QueryCondition.AffiliateAccountName" />
                      }
                      name="memberName"
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={0}>
                    <QueryButton />
                  </Col>
                </Row>
              </Form>
            </Descriptions.Item>
          </Descriptions>
          <Descriptions
            title={<FormattedMessage id="Share.Table.SearchResult" />}
            column={1}
          >
            <Descriptions.Item className="merged-column">
              <EditTable originData={data} setUpdateList={setUpdateList} />
            </Descriptions.Item>
          </Descriptions>
        </Col>
      </Row>
      <Divider />
      <div style={{ textAlign: 'right' }}>
        <Button
          type="primary"
          onClick={postData}
          disabled={updateList.length === 0}
        >
          <FormattedMessage id="Share.ActionButton.Save" />
        </Button>
      </div>
    </>
  );
}
