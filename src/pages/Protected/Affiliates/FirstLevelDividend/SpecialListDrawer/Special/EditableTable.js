import React, { useState, useEffect } from "react";
import { FormattedMessage, FormattedNumber } from "react-intl";
import ReactIntlCurrencyWithFixedDecimal from "components/ReactIntlCurrencyWithFixedDecimal";
import { Table, Input, Popconfirm, Form } from "antd";

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0
          }}
        >
          <Input
            prefix={dataIndex !== "CommissionPercent" && "≧"}
            suffix={dataIndex === "CommissionPercent" && "%"}
          />
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

const EditableTable = ({ originData, setUpdateList }) => {
  const [form] = Form.useForm();
  const [data, setData] = useState(originData);
  const [editingKey, setEditingKey] = useState("");
  const isEditing = record => record.ListId === editingKey;

  const edit = record => {
    form.setFieldsValue({
      ...record
    });
    setEditingKey(record.ListId);
  };

  const cancel = () => {
    setEditingKey("");
  };

  const save = async key => {
    try {
      const row = await form.validateFields();
      const newData = [...data];
      const index = newData.findIndex(item => {
        return key === item.ListId;
      });

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
        saveSuccess(newData);
      } else {
        newData.push(row);
        saveSuccess(newData);
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  const saveSuccess = newData => {
    setData(newData);
    setEditingKey("");
    setUpdateList(newData);
  };

  const columns = [
    {
      dataIndex: "ListId",
      title: (
        <FormattedMessage
          id="PageAffiliatesFirstLevelDividend.Drawer.SpecialList.DataTable.Title.ListId"
          description="項目ID"
        />
      )
    },
    {
      dataIndex: "MemberName",
      title: (
        <FormattedMessage
          id="PageAffiliatesFirstLevelDividend.Drawer.SpecialList.DataTable.Title.MemberName"
          description="代理帳號"
        />
      )
    },
    {
      dataIndex: "NetProfit",
      title: (
        <FormattedMessage
          id="PageAffiliatesFirstLevelDividend.Drawer.SpecialList.DataTable.Title.NetProfit"
          description="團隊淨利"
        />
      ),
      editable: true,
      render: (_1, { NetProfit }, _2) => (
        <>
          ≧
          <ReactIntlCurrencyWithFixedDecimal value={NetProfit} />
        </>
      )
    },
    {
      dataIndex: "AvailableMember",
      title: (
        <FormattedMessage
          id="PageAffiliatesFirstLevelDividend.Drawer.SpecialList.DataTable.Title.AvailableMember"
          description="一級代理活躍人數"
        />
      ),
      editable: true,
      render: (_1, { AvailableMember }, _2) => <>≧{AvailableMember}</>
    },
    {
      dataIndex: "CommissionPercent",
      title: (
        <FormattedMessage
          id="PageAffiliatesFirstLevelDividend.Drawer.SpecialList.DataTable.Title.CommissionPercent"
          description="分紅比例"
        />
      ),
      editable: true,
      render: (text, { CommissionPercent }, index) => (
        <FormattedNumber
          value={CommissionPercent / 100}
          style="percent"
          minimumFractionDigits={0}
        />
      )
    },
    {
      dataIndex: "operation",
      title: (
        <FormattedMessage
          id="Share.ActionButton.Manage"
          description="分紅比例"
        />
      ),
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <a
              onClick={() => save(record.ListId)}
              style={{
                marginRight: 8
              }}
            >
              <FormattedMessage
                id="Share.ActionButton.Save"
                defaultMessage="儲存"
              />
            </a>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>
                <FormattedMessage
                  id="Share.ActionButton.Cancel"
                  defaultMessage="取消"
                />
              </a>
            </Popconfirm>
          </span>
        ) : (
          <a disabled={editingKey !== ""} onClick={() => edit(record)}>
            <FormattedMessage
              id="Share.ActionButton.Edit"
              defaultMessage="Edit"
            />
          </a>
        );
      }
    }
  ];

  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        inputType: col.dataIndex,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record)
      })
    };
  });

  useEffect(() => {
    setData(originData);
  }, [originData]);

  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell
          }
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={{
          onChange: cancel
        }}
      />
    </Form>
  );
};

export default EditableTable;
