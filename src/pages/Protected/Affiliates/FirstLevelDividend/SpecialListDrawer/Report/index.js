import React, { useState } from 'react'
import { Descriptions, Form, Table, Row, Col, message, Input } from 'antd'
import { FormattedMessage } from 'react-intl'
import { useIntl } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import QueryButton from 'components/FormActionButtons/Query'
import caller from 'utils/fetcher'
import { Wrapper } from './Styled'

const COLUMN_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'ModifyTime',
        key: 'ModifyTime',
        width: 200,
        render: (_1, { ModifyTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Modifier" />
        ),
        dataIndex: 'ModifyUserName',
        key: 'ModifyUserName',
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.ModifiedItem" />
        ),
        dataIndex: 'Column',
        key: 'Column',
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Detail" />
        ),
        dataIndex: 'Value',
        key: 'Value',
    },
]

export default function ReportTab() {
    const [data, setData] = useState()
    const intl = useIntl()

    function getSpecialListHistory({ memberName }) {
        caller({
            method: 'get',
            endpoint: `/api/Affiliate/Bonus/SpecialList/${memberName}`,
        }).then(res => {
            setData(res.Data)
            message.success(
                intl.formatMessage({
                    id: 'Share.SuccessMessage.UpdateSuccess',
                })
            )
        })
    }

    return (
        <Wrapper>
            <Row>
                <Col span={24}>
                    <Descriptions
                        title={
                            <FormattedMessage id="Share.QueryCondition.Title" />
                        }
                        column={1}
                    >
                        <Descriptions.Item>
                            <Form onFinish={getSpecialListHistory}>
                                <Row gutter={24}>
                                    <Col span={20}>
                                        <Form.Item
                                            label={
                                                <FormattedMessage id="Share.QueryCondition.AffiliateAccountName" />
                                            }
                                            name="memberName"
                                        >
                                            <Input />
                                        </Form.Item>
                                    </Col>
                                    <Col span={4}>
                                        <QueryButton />
                                    </Col>
                                </Row>
                            </Form>
                        </Descriptions.Item>
                    </Descriptions>
                    <Descriptions
                        title={
                            <FormattedMessage id="Share.Table.SearchResult" />
                        }
                        column={1}
                    >
                        <Descriptions.Item className="merged-column">
                            <Table
                                columns={COLUMN_CONFIG}
                                dataSource={data}
                                pagination={false}
                                size="middle"
                            />
                        </Descriptions.Item>
                    </Descriptions>
                </Col>
            </Row>
        </Wrapper>
    )
}
