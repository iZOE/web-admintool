import React, { useState, forwardRef } from "react";
import { Drawer, Tabs } from "antd";
import { FormattedMessage } from "react-intl";
import Special from "./Special";
import Report from "./Report";

const { TabPane } = Tabs;

const TAB_MAPPER = {
  special: <Special />,
  report: <Report />
};

export function FilterListDrawer({}, ref) {
  const [visible, setVisible] = useState(false);
  const [tabKey, setTabKey] = useState("special");

  ref.current = {
    onOpenDrawer
  };

  function onOpenDrawer() {
    setVisible(true);
  }

  function onCloseDrawer() {
    setVisible(false);
  }

  return (
    <Drawer
      width={700}
      headerStyle={{ border: "none" }}
      title={
        <Tabs
          size="large"
          defaultActiveKey="form"
          onChange={key => {
            setTabKey(key);
          }}
          tabBarStyle={{ margin: 0 }}
        >
          <TabPane
            tab={
              <FormattedMessage
                id="PageAffiliatesFirstLevelDividend.Tabs.Special.Title"
                description="特殊名單"
              />
            }
            key="special"
          />
          <TabPane
            tab={
              <FormattedMessage
                id="PageAffiliatesFirstLevelDividend.Tabs.Record.Title"
                description="異動紀錄"
              />
            }
            key="report"
          />
        </Tabs>
      }
      visible={visible}
      onClose={onCloseDrawer}
    >
      {TAB_MAPPER[tabKey] || TAB_MAPPER["special"]}
    </Drawer>
  );
}

export default forwardRef(FilterListDrawer);
