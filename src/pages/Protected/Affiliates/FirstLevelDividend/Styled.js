import styled from "styled-components";

export const Wrapper = styled.div`
  overflow: auto;
  .card {
    background-color: #eff2f5;
  }
  #condition {
    margin-bottom: 1rem;
  }
  #pagination {
    padding: 1rem;
    text-align: center;
    background: #fff;
  }
  .freezeText {
    color: red;
  }
`;
