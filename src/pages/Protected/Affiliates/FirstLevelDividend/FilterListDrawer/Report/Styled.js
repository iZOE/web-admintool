import styled from 'styled-components';

export const Wrapper = styled.div`
  .merged-column .ant-descriptions-item-content,
  .ant-table-wrapper {
    width: 100%;
  }
`;
