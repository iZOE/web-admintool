import React, { useState } from 'react'
import { Row, Col, Button, Drawer } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import { DISPATCH_TYPE } from 'constants/dispatchType'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'
import DataTable from 'components/DataTable'

const columns = [
    {
        title: (
            <FormattedMessage
                id="Share.FormItem.GroupName"
                description="團隊名稱"
            />
        ),
        dataIndex: 'MemberId',
        key: 'MemberId',
        isShow: true,
        fixed: 'left',
        width: 200,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => {
            return
        },
        render: (text, record) => (
            <OpenMemberDetailButton memberName={record.MemberName} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesDividend.DataTable.Title.CalcuExecDate"
                description="結算日期"
            />
        ),
        dataIndex: 'CalcuExecDate',
        key: 'CalcuExecDate',
        width: 200,
        sorter: (a, b) => {
            return
        },
        render: (_1, { CalcuExecDate: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesFirstLevelDividend.DataTable.Title.Type"
                description="類型"
            />
        ),
        dataIndex: 'CommissionMode',
        key: 'CommissionMode',
        isShow: true,
        width: 120,
        sortDirections: ['descend', 'ascend'],
        render: (_1, { CommissionMode }, _2) => (
            <FormattedMessage
                id={`PageAffiliatesFirstLevelDividend.QueryCondition.SelectItem.CommissionMode.${CommissionMode}`}
            />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesAgencyReport.DataTable.Title.ReturnPointAmount"
                description="金額"
            />
        ),
        dataIndex: 'Commission',
        key: 'Commission',
        isShow: true,
        width: 200,
        sorter: (a, b) => {
            return
        },
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
]

export default function ModalButton({
    dataSource,
    modalData,
    setModalData,
    onDispatch,
}) {
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const [totalAmount, setTotalAmount] = useState()

    const closeModal = () =>
        setModalData({
            visible: false,
        })

    /**
     * 派發
     * @param {[CommissionId]}
     */
    function handleDispatch() {
        const body = {
            IsDispatch: DISPATCH_TYPE.DISPATCH,
            CommissionMainIdList: selectedRowKeys.map(idx => {
                const { CommissionMainId } = dataSource[idx]
                return CommissionMainId
            }),
        }
        onDispatch(body)
    }

    const onSelectChange = selectedRowKeys => {
        const sumOfAmount =
            selectedRowKeys && selectedRowKeys.length > 0
                ? dataSource
                      .filter(({ Index }) => selectedRowKeys.includes(Index))
                      .map(({ Commission }) => Commission)
                      .reduce((a, b) => a + b)
                : 0

        setTotalAmount(sumOfAmount)
        setSelectedRowKeys(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    }
    const hasSelected = selectedRowKeys.length > 0
    const totalSelected = useIntl().formatMessage(
        {
            id: 'Share.CommonDrawer.BatchDispatch.Footer',
        },
        {
            target: selectedRowKeys.length,
            amount: <ReactIntlCurrencyWithFixedDecimal value={totalAmount} />,
        }
    )
    return (
        <Drawer
            title={<FormattedMessage id="Share.ActionButton.BatchDispatch" />}
            width={700}
            onClose={() => closeModal()}
            visible={modalData.visible}
            footer={
                <Row align="middle">
                    <Col span={12}>
                        {hasSelected && (
                            <span style={{ marginLeft: 8 }}>
                                {totalSelected}
                            </span>
                        )}
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <Button
                            onClick={() => closeModal()}
                            style={{ marginRight: 8 }}
                        >
                            <FormattedMessage id="Share.ActionButton.Cancel" />
                        </Button>
                        <Button type="primary" onClick={handleDispatch}>
                            <FormattedMessage id="Share.ActionButton.Confirm" />
                        </Button>
                    </Col>
                </Row>
            }
        >
            <DataTable
                rowKey={record => record.Index}
                rowSelection={rowSelection}
                dataSource={
                    dataSource &&
                    dataSource.map((d, index) => {
                        d.Index = index
                        return d
                    })
                }
                config={columns}
                size="small"
            />
        </Drawer>
    )
}
