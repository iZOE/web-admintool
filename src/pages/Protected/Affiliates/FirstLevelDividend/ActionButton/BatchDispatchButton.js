import React from "react";
import { Button } from "antd";
import { UsergroupAddOutlined } from "@ant-design/icons";
import { FormattedMessage } from "react-intl";

export default function BatchDispatchButton({ condition, onClick }) {
  return (
    <Button type="primary" icon={<UsergroupAddOutlined />} onClick={onClick}>
      <FormattedMessage id="Share.ActionButton.BatchDispatch" />
    </Button>
  );
}
