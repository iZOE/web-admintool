import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, DatePicker, Checkbox } from 'antd'
import { FormattedMessage } from 'react-intl'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { ConditionContext } from 'contexts/ConditionContext'
import { getISODateTimeString } from 'mixins/dateTime'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import QueryButton from 'components/FormActionButtons/Query'

const { useForm } = Form
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_A_MONTH_TO_TODAY,
    GroupName: null,
    Status: [2],
    ShowNegativeProfit: false,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [BeginDate, EndDate] = Date || [null, null]

        return {
            ...restValues,
            BeginDate: getISODateTimeString(BeginDate),
            EndDate: getISODateTimeString(EndDate),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageAffiliatesAgencyReport.QueryCondition.DateTime"
                                description="結算時間"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsSimpleSelect
                        url="/api/Option/AffiliateBonusType"
                        name="AffiliateBonusTypeId"
                        label={<FormattedMessage id="Share.FormItem.Type" />}
                    />
                </Col>
                <Col sm={4}>
                    <FormItemMultipleSelect
                        checkAll
                        name="AffiliateLevelList"
                        label={
                            <FormattedMessage id="Share.QueryCondition.AffiliateLevel" />
                        }
                        url="/api/Option/AffiliateLevel?showAll=false"
                    />
                </Col>
                <Col sm={4}>
                    <FormItemMultipleSelect
                        name="Status"
                        label={
                            <FormattedMessage id="Share.QueryCondition.Status" />
                        }
                        url="/api/Option/AffiliateBonusStatus"
                    />
                </Col>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageAffiliatesFirstLevelDividend.QueryCondition.GroupName" />
                        }
                        name="GroupName"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={{ span: 4, offset: 18 }} align="right">
                    <Form.Item
                        name="ShowNegativeProfit"
                        valuePropName="checked"
                    >
                        <Checkbox>
                            <FormattedMessage id="PageAffiliatesFirstLevelDividend.QueryCondition.ShowMinus" />
                        </Checkbox>
                    </Form.Item>
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
