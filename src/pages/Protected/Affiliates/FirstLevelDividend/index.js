import React, { createContext, useState, createRef } from 'react';
import { Button, Result, Space, message } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import caller from 'utils/fetcher';
import * as PERMISSION from 'constants/permissions';
import { DISPATCH_COMMISSION_MODE } from 'constants/dispatchType';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import ExportReportButton from 'components/ExportReportButton';
import Condition from './Condition';
import SummaryView from './Summary';
import FilterListDrawer from './FilterListDrawer';
import SpecialListDrawer from './SpecialListDrawer';
import BatchDispatch from './BatchDispatch';
import { COLUMNS_CONFIG } from './datatableConfig';

const {
  FIRST_LEVEL_DIVIDEND_VIEW,
  FIRST_LEVEL_DIVIDEND_DISPATCH_BATCH_DISPATCH,
  FIRST_LEVEL_DIVIDEND_IGNORE,
  FIRST_LEVEL_DIVIDEND_EXPORT
} = PERMISSION;

export const PageContext = createContext();
export const DISPATCHABLE_STATUS = 2;

const PageView = () => {
  let filterListDrawerRef = createRef();
  let specialListDrawerRef = createRef();

  // 批次派發按鈕
  const [batchDispatch, setBatchDispatch] = useState({
    visible: false
  });

  const successMsg = useIntl().formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess'
  });
  const errorMsg = useIntl().formatMessage({
    id: 'Share.ErrorMessage.UnknownError'
  });

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    onReady,
    condition,
    boundedMutate
  } = useGetDataSourceWithSWR({
    url: '/api/Affiliate/Bonus/Search',
    defaultSortKey: 'CalcuExecDate',
    autoFetch: true
  });

  function handleDispatch(value) {
    caller({
      method: 'post',
      endpoint: '/api/Affiliate/Dividends/Batch/Dispatch',
      body: {
        CommissionMode: DISPATCH_COMMISSION_MODE.FIRST_LEVEL_DIVIDEND,
        ...value
      }
    })
      .then(() => {
        message.success(successMsg);
        setBatchDispatch({ visible: false });
        boundedMutate();
      })
      .catch(err => {
        message.error(errorMsg);
      });
  }

  return (
    <PageContext.Provider value={{ onDispatch: handleDispatch }}>
      <Permission
        isPage
        functionIds={[FIRST_LEVEL_DIVIDEND_VIEW]}
        failedRender={
          <Result
            status="warning"
            title={<FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />}
            subTitle={
              <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
            }
          />
        }
      >
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          summaryComponent={
            <SummaryView
              summary={dataSource?.Summary}
              totalCount={dataSource?.TotalCount}
            />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢結果"
                />
              }
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource?.Container}
              rowKey={record => record.CommissionMainId}
              extendArea={
                <Space>
                  <Button
                    onClick={() => specialListDrawerRef.current.onOpenDrawer()}
                  >
                    <FormattedMessage id="Share.ActionButton.EditSpecialList" />
                  </Button>
                  <Permission functionIds={[FIRST_LEVEL_DIVIDEND_IGNORE]}>
                    <Button
                      onClick={() => filterListDrawerRef.current.onOpenDrawer()}
                    >
                      <FormattedMessage id="Share.ActionButton.EditFilterList" />
                    </Button>
                  </Permission>
                  <Permission functionIds={[FIRST_LEVEL_DIVIDEND_EXPORT]}>
                    <ExportReportButton
                      condition={condition}
                      actionUrl="/api/Affiliate/Bonus/Export"
                    />
                  </Permission>
                  <Permission
                    functionIds={[FIRST_LEVEL_DIVIDEND_DISPATCH_BATCH_DISPATCH]}
                  >
                    <Button
                      condition={condition}
                      onClick={() => setBatchDispatch({ visible: true })}
                    >
                      <FormattedMessage id="Share.ActionButton.BatchDispatch" />
                    </Button>
                  </Permission>
                </Space>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />

        <FilterListDrawer ref={filterListDrawerRef} />
        <SpecialListDrawer ref={specialListDrawerRef} />
        <BatchDispatch
          modalData={batchDispatch}
          dataSource={
            dataSource &&
            dataSource.Container.filter(
              x => x.AuditStatus === DISPATCHABLE_STATUS
            )
          }
          setModalData={data => setBatchDispatch(data)}
          onDispatch={handleDispatch}
        />
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
