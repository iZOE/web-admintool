import React from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import ActionButton from './ActionButton';
import { DISPATCHABLE_STATUS } from './index';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.CommissionMode" description="分紅類型" />,
    dataIndex: 'CommissionMode',
    key: 'CommissionMode',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CommissionMode }, _2) => (
      <FormattedMessage
        id={`PageAffiliatesFirstLevelDividend.QueryCondition.SelectItem.CommissionMode.${CommissionMode}`}
      />
    ),
  },
  {
    title: <FormattedMessage id="Share.FormItem.GroupName" description="團隊名稱" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { MemberName, MemberId }, _2) => (
      <OpenMemberDetailButton memberName={MemberName} memberId={MemberId} />
    ),
  },
  {
    title: <FormattedMessage id="Share.FormItem.AffiliatesLevel" description="代理等級" />,
    dataIndex: 'AffiliateLevelID',
    key: 'AffiliateLevelID',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { AffiliateLevelId }, _2) => (
      <FormattedMessage
        id={`PageAffiliatesFirstLevelDividend.QueryCondition.SelectItem.AffiliateLevelId.${AffiliateLevelId}`}
      />
    ),
  },

  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.CalcuExecDate" description="結算日期" />,
    dataIndex: 'CalcuExecDate',
    key: 'CalcuExecDate',
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CalcuExecDate: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.CalcuExecDate" description="結算日期" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemCalcuExecDate',
    key: 'SystemCalcuExecDate',
    render: (_1, { SystemCalcuExecDate: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.CalculatePeriod" description="統計週期" />,
    dataIndex: 'CalculatePeriod',
    key: 'CalculatePeriod',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CalcuStartDate: s, CalcuExecDate: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.TotalBetAmount" description="團隊投注額" />,
    dataIndex: 'TotalBetAmount',
    key: 'TotalBetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { TotalBetAmount }, _2) => <ReactIntlCurrencyWithFixedDecimal value={TotalBetAmount} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.HedgeAmount" description="上期團隊淨利" />,
    dataIndex: 'HedgeAmount',
    key: 'HedgeAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { HedgeAmount }, _2) => <ReactIntlCurrencyWithFixedDecimal value={HedgeAmount} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.NetProfit" description="當期團隊淨利" />,
    dataIndex: 'NetProfit',
    key: 'NetProfit',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { NetProfit }, _2) => <ReactIntlCurrencyWithFixedDecimal value={NetProfit} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.Ratio" description="分紅比例" />,
    dataIndex: 'Ratio',
    key: 'Ratio',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, { Ratio }, index) => (
      <FormattedNumber value={Ratio / 100} style="percent" minimumFractionDigits={0} />
    ),
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.Commission" description="當期分紅" />,
    dataIndex: 'Commission',
    key: 'Commission',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { Commission }, _2) => <ReactIntlCurrencyWithFixedDecimal value={Commission} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.RealCommission" description="實際派發分紅" />,
    dataIndex: 'DispatchCommission',
    key: 'DispatchCommission',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { DispatchCommission }, _2) => <ReactIntlCurrencyWithFixedDecimal value={DispatchCommission} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.ValuableMemberCount" description="活躍人數" />,
    dataIndex: 'AvailableMemberCount',
    key: 'AvailableMemberCount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.FormItem.Status" description="状态" />,
    dataIndex: 'AuditStatus',
    key: 'AuditStatus',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { AuditStatus }, _2) => (
      <FormattedMessage id={`PageAffiliatesFirstLevelDividend.QueryCondition.SelectItem.StatusType.${AuditStatus}`} />
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageAffiliatesDividend.DataTable.Title.ModifyUser" description="異動人員" />,
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, { ModifyUserName }, index) => <> {ModifyUserName ? ModifyUserName : NO_DATA} </>,
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'action',
    key: 'action',
    fixed: 'right',
    render: (_1, record) =>
      record.AuditStatus === DISPATCHABLE_STATUS ? <ActionButton record={record} /> : record.StatusName,
  },
];
