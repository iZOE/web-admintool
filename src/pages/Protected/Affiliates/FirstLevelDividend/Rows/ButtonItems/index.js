import React, { useContext } from 'react'
import { Button, message, Popconfirm } from 'antd'
import { FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import { PageContext } from '../../index'

const {
  FIRST_LEVEL_DIVIDEND_DISPATCH_BATCH_DISPATCH,
  FIRST_LEVEL_DIVIDEND_REJECT,
} = PERMISSION

export default function ButtonItems({ record }) {
  const {
    AuditStatus,
    CommissionMainId,
    CommissionMode,
    Commission,
    MemberName,
  } = record
  const { boundedMutate } = useContext(PageContext)

  const NOT_DISPATCHED = 2
  if (AuditStatus !== NOT_DISPATCHED) {
    return null
  }

  /**
   * 派發分紅 / 拒絕派發
   * @param {IsDispatch}
   */

  function onDispatch({ IsDispatch }) {
    caller({
      method: 'POST',
      endpoint: '/api/Affiliate/Bonus/Dispatch',
      body: {
        IsDispatch,
        CommissionMainIdList: [CommissionMainId],
        CommissionMode,
      },
    }).then(() => {
      message.success('success')
      boundedMutate()
    })
  }

  return (
    <>
      <Permission functionIds={[FIRST_LEVEL_DIVIDEND_DISPATCH_BATCH_DISPATCH]}>
        <Popconfirm
          title={
            <FormattedMessage
              id="Share.CommonDispatch.Popconfirm.Dispatch"
              values={{
                memberName: MemberName,
                commission: Commission,
              }}
            />
          }
          onConfirm={() => onDispatch({ IsDispatch: true })}
          okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
          cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
          placement="topLeft"
        >
          <Button type="link">
            <FormattedMessage id="Share.ActionButton.Dispatch" />
          </Button>
        </Popconfirm>
      </Permission>
      <Permission functionIds={[FIRST_LEVEL_DIVIDEND_REJECT]}>
        <Popconfirm
          title={
            <FormattedMessage
              id="Share.CommonDispatch.Popconfirm.Cancel"
              values={{
                memberName: MemberName,
                commission: Commission,
              }}
            />
          }
          onConfirm={() => onDispatch({ IsDispatch: false })}
          okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
          cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
          placement="topLeft"
        >
          <Button type="link" danger>
            <FormattedMessage id="Share.ActionButton.Refuse" />
          </Button>
        </Popconfirm>
      </Permission>
    </>
  )
}
