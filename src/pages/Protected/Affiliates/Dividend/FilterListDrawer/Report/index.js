import React from 'react'
import { DatePicker, Form, Table, Row, Col, Input } from 'antd'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { IGNORE_TYPE } from 'constants/ignoreType'
import PleaseSearch from 'components/PleaseSearch'
import DateWithFormat from 'components/DateWithFormat'
import QueryButton from 'components/FormActionButtons/Query'

const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const COLUMN_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'ModifyTime',
        key: 'ModifyTime',
        render: (_1, { ModifyTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Modifier" />
        ),
        dataIndex: 'ModifyUserAccount',
        key: 'ModifyUserAccount',
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.ModifiedItem" />
        ),
        dataIndex: 'Action',
        key: 'Action',
        render: (_1, record, _3) => (
            <FormattedMessage
                id={`Share.CommonDrawer.FilterList.TransferTab.DataTable.Action.${record.Action}`}
            />
        ),
    },
    {
        title: (
            <FormattedMessage id="MemberDetail.Tabs.ChangeLog.Fields.Detail" />
        ),
        dataIndex: 'MemberName',
        key: 'MemberName',
    },
]

export default function ReportTab() {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/Affiliate/Bonus/IgnoreHistory',
    })

    const CONDITION_INITIAL_VALUE = {
        Date: RANGE.FROM_TODAY_TO_TODAY,
        Type: IGNORE_TYPE.DIVIDEND,
    }

    function onFinish({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        onUpdateCondition.bySearch({
            ...restValues,
            StartTime: getISODateTimeString(start),
            EndTime: getISODateTimeString(end),
        })
    }

    return (
        <Row>
            <Col span={24}>
                <Form
                    onFinish={onFinish}
                    initialValues={CONDITION_INITIAL_VALUE}
                >
                    <Row gutter={24}>
                        <Col span={20}>
                            <Form.Item
                                label={
                                    <FormattedMessage id="Share.QueryCondition.DateRange" />
                                }
                                name="Date"
                            >
                                <RangePicker
                                    showTime
                                    bordered={false}
                                    format={FORMAT.DISPLAY.DEFAULT}
                                />
                            </Form.Item>
                            <Form.Item hidden name="Type">
                                <Input type="hidden" />
                            </Form.Item>
                        </Col>
                        <Col span={4}>
                            <QueryButton />
                        </Col>
                    </Row>
                </Form>
            </Col>
            <Col span={24}>
                <PleaseSearch display={condition}>
                    <Table
                        style={{ width: '100%' }}
                        loading={fetching}
                        columns={COLUMN_CONFIG}
                        dataSource={dataSource && dataSource.Container}
                        pagination={false}
                        size="middle"
                    />
                </PleaseSearch>
            </Col>
        </Row>
    )
}
