import React from 'react';
import { Statistic, Row, Col, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';

const SummaryView = ({ summary }) => {
  return summary ? (
    <Row gutter={24}>
      <Col span={6}>
        <Statistic
          title={
            <FormattedMessage
              id="PageAffiliatesFirstLevelDividend.Summary.TotalBetAmount"
              description="总投注额"
            />
          }
          value={summary.SumOfTotalBetAmount}
          precision={2}
        />
      </Col>
      <Col span={6}>
        <Statistic
          title={
            <FormattedMessage
              id="PageAffiliatesFirstLevelDividend.Summary.TotalBonusAmount"
              description="總盈虧"
            />
          }
          value={summary.SumOfNetProfit}
          precision={2}
        />
      </Col>
      <Col span={6}>
        <Statistic
          title={
            <FormattedMessage
              id="PageAffiliatesFirstLevelDividend.Summary.Commission"
              description="分紅總額"
            />
          }
          value={summary.SumOfCommission}
          precision={2}
        />
      </Col>
      <Col span={6}>
        <Statistic
          title={
            <FormattedMessage
              id="PageAffiliatesFirstLevelDividend.Summary.DispatchCommission"
              description="已派金额"
            />
          }
          value={summary.SumOfCommissionDispatched}
          precision={2}
        />
      </Col>
    </Row>
  ) : (
    <Skeleton active />
  );
};

export default SummaryView;
