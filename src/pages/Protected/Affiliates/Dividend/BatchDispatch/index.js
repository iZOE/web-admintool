import React, { useState } from 'react'
import { Row, Col, Button, Table, Drawer } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import { DISPATCH_TYPE } from 'constants/dispatchType'
import { DISPATCH_COMMISSION_MODE } from 'constants/dispatchType'
import DateWithFormat from 'components/DateWithFormat'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'

const columns = [
    {
        title: (
            <FormattedMessage
                id="Share.FormItem.GroupName"
                description="團隊名稱"
            />
        ),
        dataIndex: 'MemberId',
        key: 'MemberId',
        isShow: true,
        fixed: 'left',
        width: 200,
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => {
            return
        },
        render: (text, record) => (
            <OpenMemberDetailButton memberName={record.MemberName} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesDividend.DataTable.Title.CalcuExecDate"
                description="結算日期"
            />
        ),
        dataIndex: 'CalcuEndDate',
        key: 'CalcuEndDate',
        width: 200,
        render: (_1, { CalcuEndDate: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="PageAffiliatesAgencyReport.DataTable.Title.ReturnPointAmount"
                description="金額"
            />
        ),
        dataIndex: 'Commission',
        key: 'Commission',
        isShow: true,
        width: 200,
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
]

export default function ModalButton({
    dataSource,
    modalData,
    setModalData,
    onDispatch,
}) {
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const [totalAmount, setTotalAmount] = useState()

    const closeModal = () =>
        setModalData({
            visible: false,
        })

    /**
     * 派發
     * @param {[CommissionId]}
     */
    function handleDispatch() {
        const body = {
            CommissionMode: DISPATCH_COMMISSION_MODE.DIVIDEND,
            Action: DISPATCH_TYPE.DISPATCH,
            List: selectedRowKeys.map(idx => {
                const { CommissionId } = dataSource[idx]
                return CommissionId
            }),
        }
        onDispatch(body)
    }

    const onSelectChange = selectedRowKeys => {
        const sumOfAmount =
            selectedRowKeys && selectedRowKeys.length > 0
                ? dataSource
                      .filter(({ Index }) => selectedRowKeys.includes(Index))
                      .map(({ Commission }) => Commission)
                      .reduce((a, b) => a + b)
                : 0

        setTotalAmount(sumOfAmount)
        setSelectedRowKeys(selectedRowKeys)
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    }
    const hasSelected = selectedRowKeys.length > 0
    const totalSelected = useIntl().formatMessage(
        {
            id: 'Share.CommonDrawer.BatchDispatch.Footer',
        },
        {
            target: selectedRowKeys.length,
            amount: <ReactIntlCurrencyWithFixedDecimal value={totalAmount} />,
        }
    )
    return (
        <Drawer
            title={<FormattedMessage id="Share.ActionButton.BatchDispatch" />}
            width={700}
            onClose={() => closeModal()}
            visible={modalData.visible}
            footer={
                <Row align="middle">
                    <Col span={12}>
                        {hasSelected && (
                            <span style={{ marginLeft: 8 }}>
                                {totalSelected}
                            </span>
                        )}
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <Button
                            onClick={() => closeModal()}
                            style={{ marginRight: 8 }}
                        >
                            <FormattedMessage id="Share.ActionButton.Cancel" />
                        </Button>
                        <Button type="primary" onClick={handleDispatch}>
                            <FormattedMessage id="Share.ActionButton.Confirm" />
                        </Button>
                    </Col>
                </Row>
            }
        >
            <Table
                rowKey={record => record.Index}
                rowSelection={rowSelection}
                dataSource={
                    dataSource &&
                    dataSource.map((d, index) => {
                        d.Index = index
                        return d
                    })
                }
                columns={columns}
                pagination={false}
                size="small"
            />
        </Drawer>
    )
}
