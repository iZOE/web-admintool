import React from "react";
import { Table } from "antd";
import Row from "../components/Row";
import { COLUMN_CONFIG } from "./datatableConfig";

function SubTable({ data: dataSource }) {
  return (
    <Table
      columns={COLUMN_CONFIG}
      components={{
        body: {
          row: Row
        }
      }}
      dataSource={dataSource}
      pagination={false}
      onRow={record => ({
        record
      })}
      size="small"
    />
  );
}
export default SubTable;
