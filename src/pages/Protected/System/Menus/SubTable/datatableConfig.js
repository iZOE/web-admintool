import React from 'react'
import { Typography } from 'antd'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'

const { Text } = Typography

export const COLUMN_CONFIG = [
    {
        key: 'Name',
        dataIndex: 'Name',
        title: (
            <FormattedMessage
                id="PageSystemMenus.Table.Columns.Name"
                defaultMessage="功能頁面"
            />
        ),
    },
    {
        key: 'Link',
        dataIndex: 'Link',
        title: (
            <FormattedMessage
                id="PageSystemMenus.Table.Columns.Link"
                defaultMessage="連結設定"
            />
        ),
        render: Link => <Text code>{Link}</Text>,
    },
    {
        key: 'IsEnable',
        dataIndex: 'IsEnable',
        width: 150,
        title: (
            <FormattedMessage
                id="PageSystemMenus.Table.Columns.IsEnable"
                defaultMessage="狀態"
            />
        ),
    },
    {
        key: 'ModifiedOn',
        dataIndex: 'ModifiedOn',
        width: 200,
        title: (
            <FormattedMessage
                id="PageSystemMenus.Table.Columns.ModifiedOnStr"
                defaultMessage="修改時間"
            />
        ),
        render: (_1, { ModifiedOn: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        key: 'ModifiedByName',
        dataIndex: 'ModifiedByName',
        width: 200,
        title: (
            <FormattedMessage
                id="PageSystemMenus.Table.Columns.ModifiedByName"
                defaultMessage="修改者"
            />
        ),
    },
    {
        key: '',
        dataIndex: 'Manage',
        align: 'center',
        width: 200,
        manageCell: true,
        title: (
            <FormattedMessage
                id="PageSystemMenus.Table.Columns.Manage"
                defaultMessage="管理"
            />
        ),
    },
]
