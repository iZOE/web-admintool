import React, { useMemo, useState } from 'react';
import useSWR from 'swr';
import axios from 'axios';
import { Form, Skeleton, Table } from 'antd';
import Permission from 'components/Permission';
import * as PERMISSION from 'constants/permissions';
import Row from './components/Row';
import SubTable from './SubTable';
import { COLUMNS_CONFIG } from './datatableConfig';

const { SETTINGS_MENUS_VIEW } = PERMISSION;
const Menus = () => {
  const [form] = Form.useForm();
  const [expandedRow, setExpandedRow] = useState({
    expanded: false,
    key: ''
  });
  const { data, isValidating: fetching } = useSWR('/api/Menu', url =>
    axios(url).then(res => res.data)
  );

  const dataSource = useMemo(() => {
    if (data) {
      return data.Data.map((item, index) => {
        const { SubMenus, ...rest } = item;
        return {
          key: `main_${index}`,
          SubMenus: SubMenus.map((subItem, idx) => ({
            key: `sub_${idx}`,
            ...subItem
          })),
          ...rest
        };
      });
    }
    return [];
  }, [data]);

  const expandedRowRender = ({ SubMenus }) => <SubTable data={SubMenus} />;

  if (!dataSource) {
    return <Skeleton active />;
  }

  return (
    <Permission isPage functionIds={[SETTINGS_MENUS_VIEW]}>
      <Form form={form} component={false}>
        <Table
          components={{
            body: {
              row: Row
            }
          }}
          loading={fetching}
          columns={COLUMNS_CONFIG}
          dataSource={dataSource}
          expandable={{
            expandedRowRender,
            onExpand: (expanded, record) => {
              /**
               * 因為 antd 的 Table 沒有提供 row expanded 的狀態給 Row 所以自己寫了個狀態好傳給 Row
               */
              setExpandedRow({
                expanded,
                key: record.key
              });
            }
          }}
          onRow={record => ({
            record,
            expandedRow
          })}
          pagination={false}
        ></Table>
      </Form>
    </Permission>
  );
};

export default Menus;
