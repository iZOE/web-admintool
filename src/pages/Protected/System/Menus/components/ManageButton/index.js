/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Button, Popconfirm, Space } from "antd";
import { FormattedMessage } from "react-intl";

const ManageButton = ({ doCancel, doEdit, editing, onSave }) => {
  return editing ? (
    <Space size="small">
      <Popconfirm
        title={
          <FormattedMessage
            id="Share.ActionButton.SureToSave"
            defaultMessage="确定取消？"
          />
        }
        onConfirm={() => onSave()}
      >
        <Button type="link" size="small">
          <FormattedMessage
            id="Share.ActionButton.Save"
            defaultMessage="储存"
          />
        </Button>
      </Popconfirm>
      <Button type="link" size="small" onClick={() => doCancel()}>
        <FormattedMessage
          id="Share.ActionButton.Cancel"
          defaultMessage="取消"
        />
      </Button>
    </Space>
  ) : (
    <a href="#" onClick={() => doEdit()}>
      <FormattedMessage id="Share.ActionButton.Manage" defaultMessage="管理" />
    </a>
  );
};

export default ManageButton;
