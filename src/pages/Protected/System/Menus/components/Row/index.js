import React, {
  useState,
  useMemo,
  Children,
  isValidElement,
  cloneElement
} from "react";
import { FormattedMessage } from "react-intl";
import { Form, Switch, Tag } from "antd";
import { trigger } from "swr";
import update from "immutability-helper";
import ManageButton from "../ManageButton";
import menusService from "../../services";

const Row = ({ children, index, record, expandedRow, ...restProps }) => {
  const [form] = Form.useForm();
  const [editing, setEditing] = useState(false);
  const [cell, setCell] = useState(record);
  const [expanded, setExpanded] = useState(false);
  const { updateMainMenuItem, updateSubMenuItem } = menusService();

  /**
   * 判斷目前開啟的 Row key 與自己的 key 是否吻合
   * 如果吻合在比對 expanded 的狀態是否一樣
   * 不一樣就變更狀態
   */
  if (
    record &&
    expandedRow &&
    record.key === expandedRow.key &&
    expanded !== expandedRow.expanded
  ) {
    setExpanded(!expanded);
  }

  const doEdit = () => {
    setEditing(true);
    form.setFieldsValue(cell);
  };

  const doCancel = () => {
    setEditing(false);
  };

  const onSave = async () => {
    try {
      const { key, Id, Link, ParentMenuId, Is_NextVersion } = cell;
      const formData = await form.validateFields();
      if (RegExp("^main_", "g").test(key)) {
        updateMainMenuItem({ Id, ...formData }).then(result =>
          saveSuccess(formData)
        );
      } else {
        updateSubMenuItem({
          Id,
          Link,
          ParentMenuId,
          Is_NextVersion,
          ...formData
        }).then(result => saveSuccess(formData));
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };
  const saveSuccess = formData => {
    setCell(update(cell, { $merge: formData }));
    trigger("/api/Menu/AllowMenu");
    setEditing(false);
  };

  const manageButtonProps = {
    editing,
    doEdit,
    doCancel,
    onSave
  };

  const childrenWithProps = useMemo(() => {
    return Children.map(children, child => {
      if (isValidElement(child)) {
        switch (child.key) {
          // case "Name":
          //   return cloneElement(child, {
          //     render: () =>
          //       editing ? (
          //         <Form.Item
          //           name={child.props.dataIndex}
          //           rules={[
          //             {
          //               required: true,
          //               message: (
          //                 <FormattedMessage
          //                   id="Share.FormValidate.Required.Input"
          //                   defaultMessage="此為必填欄位"
          //                 />
          //               )
          //             }
          //           ]}
          //         >
          //           <Input />
          //         </Form.Item>
          //       ) : (
          //         cell.Name
          //       )
          //   });
          case "IsEnable":
            return cloneElement(child, {
              render: () =>
                editing ? (
                  <Form.Item
                    noStyle
                    name={child.props.dataIndex}
                    initialValue={true}
                    valuePropName="checked"
                  >
                    <Switch
                      checkedChildren={
                        <FormattedMessage
                          id="Share.Status.Enable"
                          defaultMessage="啟用"
                        />
                      }
                      unCheckedChildren={
                        <FormattedMessage
                          id="Share.Status.Disable"
                          defaultMessage="停用"
                        />
                      }
                    />
                  </Form.Item>
                ) : (
                  <Tag color={cell.IsEnable ? "processing" : "warning"}>
                    <FormattedMessage
                      id={`Share.Status.${
                        cell.IsEnable ? "Enable" : "Disable"
                      }`}
                      defaultMessage={cell.IsEnable ? "啟用" : "停用"}
                    />
                  </Tag>
                )
            });
          case "Manage":
            return cloneElement(child, {
              render: () => <ManageButton {...manageButtonProps} />
            });
          default:
            return child;
        }
      }
    });
  }, [children, editing, cell, manageButtonProps]);

  return (
    <tr {...restProps}>
      <Form form={form} component={false}>
        {childrenWithProps}
      </Form>
    </tr>
  );
};

export default Row;
