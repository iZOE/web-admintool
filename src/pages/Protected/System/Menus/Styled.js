import styled from "styled-components";

export const Wrapper = styled.div`
  overflow: auto;
  .card {
    background-color: #eff2f5;
  }
  .ant {
    &-form-item,
    &-tag {
      margin: 0;
    }
  }
`;
