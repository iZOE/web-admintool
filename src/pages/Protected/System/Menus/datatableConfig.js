import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';

export const COLUMNS_CONFIG = [
  {
    key: 'Name',
    dataIndex: 'Name',
    title: <FormattedMessage id="PageSystemMenus.Table.Columns.Name" defaultMessage="主選單" />,
  },
  {
    key: 'IsEnable',
    dataIndex: 'IsEnable',
    editable: true,
    title: <FormattedMessage id="PageSystemMenus.Table.Columns.IsEnable" defaultMessage="狀態" />,
  },
  {
    key: 'ModifiedOn',
    dataIndex: 'ModifiedOn',
    title: <FormattedMessage id="PageSystemMenus.Table.Columns.ModifiedOnStr" defaultMessage="修改時間" />,
    render: (_1, { ModifiedOn: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    key: 'ModifiedByName',
    dataIndex: 'ModifiedByName',
    title: <FormattedMessage id="PageSystemMenus.Table.Columns.ModifiedByName" defaultMessage="修改者" />,
    render: ModifiedByName => ModifiedByName || '-',
  },
  {
    key: '',
    align: 'center',
    dataIndex: 'Manage',
    title: <FormattedMessage id="PageSystemMenus.Table.Columns.Manage" defaultMessage="管理" />,
  },
];
