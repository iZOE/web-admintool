import makeService from 'utils/makeService';

const menusService = () => {
  const updateMainMenuItem = makeService({
    method: 'put',
    url: '/api/Menu/Main'
  });

  const updateSubMenuItem = makeService({
    method: 'put',
    url: '/api/Menu/Sub'
  });

  return {
    updateMainMenuItem,
    updateSubMenuItem
  };
};

export default menusService;
