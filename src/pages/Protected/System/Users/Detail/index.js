import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';
import { message } from 'antd';
import useSWR from 'swr';
import axios from 'axios';
import LayoutPageBody from 'layouts/Page/Body';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import Form from '../components/Form';
import usersService from '../services';

const { SETTINGS_USERS_CREATE } = PERMISSION;
const { updateUser } = usersService();

export default function Detail() {
  const intl = useIntl();
  const history = useHistory();
  const { id } = useParams();
  const { data: detail } = useSWR(id ? `/api/User/${id}` : null, url =>
    axios(url).then(res => res && res.data.Data)
  );

  function onSave(payload) {
    updateUser(payload)
      .then(() => {
        message.success(
          intl.formatMessage({
            id: 'Share.SuccessMessage.UpdateSuccess'
          }),
          10
        );
        history.push('/settings/users/');
      })
      .catch(({ ErrorKey, Message }) => {
        message.warning(
          intl.formatMessage({
            id: 'Share.ErrorMessage.UnknownError',
            description: '不明錯誤'
          }),
          10
        );
      });
  }

  return (
    <>
      <Permission isPage functionIds={[SETTINGS_USERS_CREATE]}>
        <LayoutPageBody
          title={
            <FormattedMessage
              id="PageSystemUsers.DataTable.Title.EditUser"
              description="编辑使用者"
            />
          }
        >
          <Form isEditMode defaultValues={detail} onSubmit={onSave} />
        </LayoutPageBody>
      </Permission>
    </>
  );
}
