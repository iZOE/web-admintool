import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Home";
import Create from "./Create";
import Detail from "./Detail";

export default function SettingsUsers() {
  return (
    <Switch>
      <Route exact path="/settings/users">
        <Home />
      </Route>
      <Route path="/settings/users/create">
        <Create />
      </Route>
      <Route path="/settings/users/detail/:id">
        <Detail />
      </Route>
    </Switch>
  );
}
