import React, { useContext } from 'react'
import { FormattedMessage } from 'react-intl'
import { Button } from 'antd'
import { DrawerContext } from 'contexts/DrawerContext'
import UserDetail from '../UserDetail'

export default function OpenUserDetailButton({
  userId,
  userName,
  displayText = userName,
}) {
  const { onOpenDrawer } = useContext(DrawerContext)

  return (
    <Button
      type="link"
      size="small"
      onClick={() => {
        onOpenDrawer({
          width: 720,
          title: (
            <FormattedMessage
              id="PageSystemUsers.UserDetail.Title.View"
              defaultMessage="檢視使用者資料"
            />
          ),
          component: <UserDetail userId={userId} />,
        })
      }}
    >
      {displayText}
    </Button>
  )
}
