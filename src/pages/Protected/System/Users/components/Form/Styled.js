import styled from "styled-components";

export const Wrapper = styled.div`
  #Users_Password {
    .ant-col {
      &:last-child {
        text-align: right;
      }
    }
  }
`;
