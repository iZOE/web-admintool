import React, { useContext, useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { useHistory } from 'react-router-dom'
import {
    Button,
    Col,
    Form,
    Input,
    Modal,
    Row,
    Select,
    Skeleton,
    Space,
    Switch,
} from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { UserContext } from 'contexts/UserContext'
import { AGENT_TYPE } from 'constants/agentType'
import DateWithFormat from 'components/DateWithFormat'
import optionsService from 'services/optionsServices'
import usersService from '../../services'
import { Wrapper } from './Styled'

const { confirm } = Modal
const { useForm } = Form
const { Option } = Select
const { getAgentOptions, getGroupOptions } = optionsService()
const { checkUserExist } = usersService()

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 8,
    },
}
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
}

const ACCOUNT_LENGTH_LIMIT = { min: 4, max: 10 }
const PASSWORD_LENGTH_LIMIT = { min: 6, max: 16 }

function ActiveForm({ isEditMode, defaultValues, onSubmit }) {
    const [changePassword, setChangePassword] = useState(!isEditMode)
    const [agentIds, setAgentIds] = useState([])
    const [groupOptions, setGroupOptions] = useState(null)
    const history = useHistory()
    const [form] = useForm()
    const intl = useIntl()
    const { data: userData } = useContext(UserContext)

    useEffect(() => {
        if (userData.AgentId === AGENT_TYPE.CAILIFUN_I) {
            getAgentOptions().then(result => setAgentIds(result))
        }
    }, [])

    useEffect(() => {
        if (defaultValues) {
            getGroupOptions({
                oldGroupId: -1,
                agentId: defaultValues.AgentId || userData.AgentId,
            }).then(result => setGroupOptions(result))
        }
    }, [defaultValues])

    const ACCOUNT_FORM_ITEM_NAME = isEditMode
        ? {}
        : {
              name: 'Account',
          }

    const RULES = useMemo(
        () => ({
            Account: [
                {
                    required: !isEditMode,
                    message: (
                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                    ),
                },
                {
                    ...ACCOUNT_LENGTH_LIMIT,
                    message: (
                        <FormattedMessage
                            id="PageSystemUsers.FormValidate.AccountLangth"
                            values={ACCOUNT_LENGTH_LIMIT}
                            description="帐号必须介于4到10个字符之间"
                        />
                    ),
                },
                {
                    pattern: RegExp('^[a-zA-Z0-9]*$', 'g'),
                    message: (
                        <FormattedMessage id="PageSystemUsers.FormValidate.AccountPattern" />
                    ),
                },
                () => ({
                    validator(_1, value) {
                        return value
                            ? checkUserExist(value).then(isExist => {
                                  if (isExist) {
                                      return Promise.reject(
                                          <FormattedMessage id="PageSystemUsers.FormValidate.AccountIsExist" />
                                      )
                                  }
                              })
                            : Promise.resolve(true)
                    },
                }),
            ],
            Password: [
                {
                    required: changePassword,
                    message: (
                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                    ),
                },
                {
                    ...PASSWORD_LENGTH_LIMIT,
                    message: (
                        <FormattedMessage
                            id="PageSystemUsers.FormValidate.PasswordLangth"
                            values={PASSWORD_LENGTH_LIMIT}
                            description="帐号必须介于4到10个字符之间"
                        />
                    ),
                },
                {
                    pattern: RegExp('^[a-zA-Z0-9\x21-\x7e]*$', 'g'),
                    message: (
                        <FormattedMessage id="PageSystemUsers.FormValidate.PasswordPattern" />
                    ),
                },
            ],
            ConfirmPassword: [
                {
                    required: changePassword,
                    message: (
                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                    ),
                },
                ({ getFieldValue }) => ({
                    validator(_1, value) {
                        if (!value || getFieldValue('Password') === value) {
                            return Promise.resolve()
                        }
                        return Promise.reject(
                            <FormattedMessage id="PageSystemUsers.FormValidate.PasswordNotMatch" />
                        )
                    },
                }),
            ],
            Group: [
                {
                    required: true,
                    message: (
                        <FormattedMessage id="PageSystemUsers.FormValidate.GroupRequired" />
                    ),
                },
            ],
        }),
        [changePassword]
    )

    const onChangePassword = () => {
        if (changePassword) {
            form.resetFields(['Password'])
        }
        setChangePassword(!changePassword)
    }

    const onFinish = values => {
        confirm({
            title: intl.formatMessage({
                id: isEditMode
                    ? 'Share.ActionButton.Save'
                    : 'Share.ActionButton.Submit',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: isEditMode
                    ? 'Share.ConfirmTips.WhetherToSaveExistingChanges'
                    : 'PageSystemUsers.Form.ConfirmAddUser',
            }),
            okText: intl.formatMessage({
                id: isEditMode
                    ? 'Share.ActionButton.Save'
                    : 'Share.ActionButton.Confirm',
                description: '保存',
            }),
            cancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
                description: '取消',
            }),
            onOk() {
                if (isEditMode) {
                    if (!values.Password) {
                        delete values.Password
                    }
                    values.Id = defaultValues.Id
                }
                delete values.ConfirmPassword

                onSubmit(values)
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    function agentOptionsChange(event) {
        if (event) {
            getGroupOptions({
                oldGroupId: -1,
                agentId: event,
            }).then(result => setGroupOptions(result))
        } else {
            setGroupOptions([])
        }
        form.setFieldsValue({
            GroupId: null,
        })
    }

    function showResetConfirm() {
        confirm({
            title: intl.formatMessage({
                id: 'Share.CommonKeys.FillInAgain',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: 'Share.ConfirmTips.DoYouWantToReeditTheContent',
            }),
            okText: intl.formatMessage({ id: 'Share.ActionButton.Restart' }),
            okType: 'danger',
            cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            onOk() {
                form.resetFields()
            },
            onCancel() {
                console.log('Cancel showResetConfirm')
            },
        })
    }

    function showCancelConfirm() {
        confirm({
            title: intl.formatMessage({ id: 'Share.CommonKeys.GiveUpEditing' }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage(
                {
                    id: 'Share.ConfirmTips.DoYouWantToGiveUpEditingAndReturn',
                },
                {
                    targetname: intl.formatMessage({
                        id: 'PageSystemUsers.Title',
                    }),
                }
            ),
            okText: '放弃',
            okType: 'danger',
            cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            onOk() {
                history.push('/settings/users/')
            },
            onCancel() {
                console.log('Cancel showCancelConfirm')
            },
        })
    }

    return defaultValues ? (
        <Wrapper>
            <Form
                {...layout}
                form={form}
                initialValues={{
                    ...defaultValues,
                    AgentId: defaultValues.AgentId || userData.AgentId,
                }}
                name="Users"
                onFinish={onFinish}
            >
                {isEditMode && (
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemUsers.Form.Id"
                                description="使用者 ID"
                            />
                        }
                    >
                        {defaultValues.Id}
                    </Form.Item>
                )}
                <Form.Item
                    {...ACCOUNT_FORM_ITEM_NAME}
                    label={
                        <FormattedMessage
                            id="PageSystemUsers.Form.Account"
                            description="使用者帐号"
                        />
                    }
                    rules={RULES.Account}
                >
                    {isEditMode ? (
                        defaultValues.Account
                    ) : (
                        <Input
                            placeholder={intl.formatMessage({
                                id: 'PageSystemUsers.Form.AccountPlaceholder',
                            })}
                        />
                    )}
                </Form.Item>
                <Form.Item
                    name="Password"
                    label={
                        <FormattedMessage
                            id="PageSystemUsers.Form.Password"
                            description="使用者密码"
                        />
                    }
                    rules={changePassword ? RULES.Password : null}
                >
                    <Row gutter={24}>
                        <Col span={isEditMode ? 18 : 24}>
                            <Input.Password
                                disabled={!changePassword}
                                placeholder={
                                    changePassword
                                        ? intl.formatMessage({
                                              id:
                                                  'PageSystemUsers.Form.PasswordPlaceholder',
                                          })
                                        : '••••••••'
                                }
                            />
                        </Col>
                        {isEditMode && (
                            <Col span={6}>
                                <Button
                                    type="primary"
                                    onClick={onChangePassword}
                                >
                                    <FormattedMessage
                                        id={`PageSystemUsers.Form.${
                                            changePassword ? 'Cancel' : ''
                                        }ModifyPassword`}
                                    />
                                </Button>
                            </Col>
                        )}
                    </Row>
                </Form.Item>
                {changePassword && (
                    <Form.Item
                        name="ConfirmPassword"
                        dependencies={['Password']}
                        label={
                            <FormattedMessage id="PageSystemUsers.Form.ConfirmPassword" />
                        }
                        rules={changePassword ? RULES.ConfirmPassword : null}
                    >
                        <Row gutter={24}>
                            <Col span={isEditMode ? 18 : 24}>
                                <Input.Password
                                    placeholder={intl.formatMessage({
                                        id:
                                            'PageSystemUsers.Form.PasswordPlaceholder',
                                    })}
                                />
                            </Col>
                        </Row>
                    </Form.Item>
                )}
                {userData.AgentId === AGENT_TYPE.CAILIFUN_I && (
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemUsers.QueryCondition.Agents"
                                defaultMessage="站台"
                            />
                        }
                        name="AgentId"
                    >
                        <Select onChange={agentOptionsChange}>
                            {agentIds.map(item => (
                                <Option
                                    key={`agent_${item.AgentId}`}
                                    value={item.AgentId}
                                >
                                    {item.AgentName}
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                )}
                <Form.Item
                    name="GroupId"
                    label={
                        <FormattedMessage
                            id="PageSystemUsers.Form.Group"
                            description="群组"
                        />
                    }
                    rules={RULES.Group}
                >
                    <Select loading={!groupOptions}>
                        <Option value={null}>
                            <FormattedMessage id="PageSystemUsers.Form.SelectGroup" />
                        </Option>
                        {groupOptions &&
                            groupOptions.map(option => (
                                <Option
                                    key={`group_${option.Id}`}
                                    value={option.Id}
                                >
                                    {option.Name}
                                </Option>
                            ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="IsEnable"
                    valuePropName="checked"
                    label={<FormattedMessage id="Share.CommonKeys.Status" />}
                >
                    <Switch
                        checkedChildren={
                            <FormattedMessage id="Share.Status.Enable" />
                        }
                        unCheckedChildren={
                            <FormattedMessage id="Share.Status.Disable" />
                        }
                    />
                </Form.Item>
                {isEditMode && (
                    <>
                        <Form.Item
                            label={
                                <FormattedMessage id="PageSystemUsers.Form.CreatedOn" />
                            }
                        >
                            <DateWithFormat time={defaultValues.CreatedOn} />
                        </Form.Item>
                        <Form.Item
                            label={
                                <FormattedMessage id="PageSystemUsers.Form.CreatedBy" />
                            }
                        >
                            {defaultValues.CreatedByName}
                        </Form.Item>
                    </>
                )}
                <Form.Item {...tailLayout}>
                    <Space>
                        <Button type="primary" htmlType="submit">
                            <FormattedMessage id="Share.ActionButton.Submit" />
                        </Button>
                        <Button htmlType="button" onClick={showResetConfirm}>
                            <FormattedMessage id="Share.ActionButton.Restart" />
                        </Button>
                        <Button
                            type="link"
                            htmlType="button"
                            onClick={showCancelConfirm}
                        >
                            <FormattedMessage id="Share.ActionButton.Cancel" />
                        </Button>
                    </Space>
                </Form.Item>
            </Form>
        </Wrapper>
    ) : (
        <Skeleton active />
    )
}

ActiveForm.defaultProps = {
    isEditMode: false,
}

ActiveForm.propTypes = {
    isEditMode: PropTypes.bool,
    defaultValues: PropTypes.shape({
        Account: PropTypes.string,
        AgentId: PropTypes.number,
        AgentName: PropTypes.string,
        CreatedOn: PropTypes.number,
        GroupId: PropTypes.number,
        GroupName: PropTypes.string,
        Id: PropTypes.number,
        IsEnable: PropTypes.bool,
        IsLocked: PropTypes.bool,
    }),
    onSubmit: PropTypes.func.isRequired,
}

export default ActiveForm
