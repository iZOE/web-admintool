import React from 'react'
import { FormattedMessage } from 'react-intl'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import {
    message,
    Avatar,
    Descriptions,
    Divider,
    PageHeader,
    Skeleton,
    Tooltip,
    Tag,
} from 'antd'
import DateWithFormat from 'components/DateWithFormat'

export default function BasicData({ detail }) {
    const {
        Id,
        Account,
        AgentName,
        GroupName,
        IsEnable,
        CreatedOn,
        CreatedByName,
        GoogleAuthQrCodeUrl,
        ManualEntrySetupCode,
    } = detail
    return (
        <>
            {detail ? (
                <PageHeader>
                    <Descriptions
                        title={
                            <FormattedMessage
                                id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.UserInfo"
                                defaultMessage="使用者資訊"
                            />
                        }
                        column={2}
                    >
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Id"
                                    defaultMessage="使用者ID"
                                />
                            }
                        >
                            {Id}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Account"
                                    defaultMessage="使用者帳號"
                                />
                            }
                        >
                            {Account}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Agent"
                                    defaultMessage="站台"
                                />
                            }
                        >
                            {AgentName}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Group"
                                    defaultMessage="群組"
                                />
                            }
                        >
                            {GroupName}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Status"
                                    defaultMessage="狀態"
                                />
                            }
                        >
                            <Tag color={IsEnable ? 'processing' : 'warning'}>
                                <FormattedMessage
                                    id={`Share.Status.${
                                        IsEnable ? 'Enable' : 'Disable'
                                    }`}
                                    defaultMessage={IsEnable ? '啟用' : '停用'}
                                />
                            </Tag>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.CreatedOn"
                                    defaultMessage="建立時間"
                                />
                            }
                        >
                            <DateWithFormat time={CreatedOn} />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.CreatedBy"
                                    defaultMessage="建立者"
                                />
                            }
                        >
                            {CreatedByName}
                        </Descriptions.Item>
                    </Descriptions>
                    <Divider dashed />{' '}
                    <Descriptions
                        title={
                            <FormattedMessage
                                id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Authenticator"
                                defaultMessage="兩步驟驗證碼"
                            />
                        }
                        column={2}
                    >
                        <Descriptions.Item>
                            <Tooltip
                                title={
                                    <FormattedMessage
                                        id="PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Copy"
                                        defaultMessage="點擊複製"
                                    />
                                }
                            >
                                <CopyToClipboard
                                    text={ManualEntrySetupCode}
                                    onCopy={(text, result) => {
                                        message[result ? 'success' : 'warning'](
                                            <FormattedMessage
                                                id={`PageSystemUsers.UserDetail.Tabs.BasicData.Areas.Copy${
                                                    result ? 'Success' : 'Fail'
                                                }`}
                                                defaultMessage={`複製${
                                                    result ? '成功' : '失敗'
                                                }`}
                                            />,
                                            5
                                        )
                                    }}
                                >
                                    <Avatar
                                        shape="square"
                                        size={300}
                                        src={GoogleAuthQrCodeUrl}
                                    />
                                </CopyToClipboard>
                            </Tooltip>
                        </Descriptions.Item>
                    </Descriptions>
                </PageHeader>
            ) : (
                <Skeleton active />
            )}
        </>
    )
}
