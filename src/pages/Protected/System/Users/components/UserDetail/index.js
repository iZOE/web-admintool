import React, { Suspense, lazy, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Tabs, Spin, Skeleton } from 'antd'
import useSWR from 'swr'
import axios from 'axios'

const BasicDataAsync = lazy(() => import('./BasicData'))
// const ModifyLogsAsync = lazy(() => import("./ModifyLogs"));
const ChangeLogAsync = lazy(() => import('./ChangeLog'))

const { TabPane } = Tabs

const UserDetail = ({ userId }) => {
    const DEFAULT_TAB = 'basicData'
    const [currentTab, setCurrentTab] = useState(DEFAULT_TAB)
    let { data: detail } = useSWR(userId ? `/api/User/${userId}` : null, url =>
        axios(url).then(res => res && res.data.Data)
    )

    return (
        <Tabs
            defaultActiveKey={DEFAULT_TAB}
            tabPosition="top"
            onChange={key => setCurrentTab(key)}
        >
            <TabPane
                tab={
                    <FormattedMessage
                        id="PageSystemUsers.UserDetail.Tabs.BasicData.Title"
                        defaultMessage="基本資料"
                    />
                }
                key="basicData"
            >
                <Suspense fallback={<Spin />}>
                    {!!detail ? (
                        <BasicDataAsync detail={detail} />
                    ) : (
                        <Skeleton active />
                    )}
                </Suspense>
            </TabPane>
            <TabPane
                tab={
                    <FormattedMessage
                        id="PageSystemUsers.UserDetail.Tabs.ModifyLogs.Title"
                        defaultMessage="異動紀錄"
                    />
                }
                key="modifyLogs"
            >
                <Suspense fallback={<Spin />}>
                    {!!detail ? (
                        <ChangeLogAsync userId={userId} />
                    ) : (
                        <Skeleton active />
                    )}
                </Suspense>
            </TabPane>
        </Tabs>
    )
}

export default UserDetail
