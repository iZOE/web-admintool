import makeService from 'utils/makeService';

const usersService = () => {
  const checkUserExist = account =>
    makeService({
      method: 'get',
      url: `/api/User/IsUserExist/${account}`
    })();

  const addUser = payload =>
    makeService({
      method: 'post',
      url: '/api/User'
    })(payload);

  const updateUser = payload =>
    makeService({
      method: 'put',
      url: '/api/User'
    })(payload);

  const unLockUser = Id =>
    makeService({
      method: 'patch',
      url: `/api/User/Lock/${Id}`
    })();

  return {
    checkUserExist,
    addUser,
    updateUser,
    unLockUser
  };
};

export default usersService;
