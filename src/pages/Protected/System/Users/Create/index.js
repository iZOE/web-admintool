import React from 'react';
import { useHistory } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';
import { message } from 'antd';
import LayoutPageBody from 'layouts/Page/Body';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import Form from '../components/Form';
import usersService from '../services';

const { SETTINGS_USERS_CREATE } = PERMISSION;
const DEFAULT_VALUES = {
  Account: null,
  Password: null,
  ConfirmPassword: null,
  GroupId: null,
  IsEnable: true
};

const { addUser } = usersService();

export default function Create() {
  const intl = useIntl();
  const history = useHistory();

  function onSave(payload) {
    addUser(payload)
      .then(() => {
        message.success(
          intl.formatMessage({
            id: 'Share.UIResponse.CreateSuccess'
          }),
          10
        );
        history.push('/settings/users/');
      })
      .catch(({ ErrorKey, Message }) => {
        message.warning(
          intl.formatMessage({
            id: 'Share.ErrorMessage.UnknownError',
            description: '不明錯誤'
          }),
          10
        );
      });
  }

  return (
    <Permission isPage functionIds={[SETTINGS_USERS_CREATE]}>
      <LayoutPageBody
        title={
          <FormattedMessage
            id="PageSystemUsers.DataTable.Title.CreateUser"
            description="新增使用者"
          />
        }
      >
        <Form defaultValues={DEFAULT_VALUES} onSubmit={onSave} />
      </LayoutPageBody>
    </Permission>
  );
}
