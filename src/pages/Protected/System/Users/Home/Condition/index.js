import React, { useContext, useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Row, Col, Form, Input, Select, DatePicker, Checkbox } from 'antd'
import { AGENT_TYPE } from 'constants/agentType'
import { UserContext } from 'contexts/UserContext'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import optionsService from 'services/optionsServices'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Account: null,
    AgentId: null,
    GroupId: null,
    Date: RANGE.FROM_INITIAL_TO_TODAY,
    IsOnlyEnabled: false,
}

const { getAgentOptions, getGroupOptions } = optionsService()

export default function Condition({ onUpdate, onReady }) {
    const { data: userData } = useContext(UserContext)
    const isCailifun1 = useRef(userData.AgentId === AGENT_TYPE.CAILIFUN_I)
    const [agentIds, setAgentIds] = useState([])
    const [groupIds, setGroupIds] = useState([])
    const intl = useIntl()
    const [form] = useForm()

    function agentOptionsChange(event) {
        if (event) {
            getGroupOptions({
                oldGroupId: -1,
                agentId: event,
            }).then(result => setGroupIds(result))
        } else {
            setGroupIds([])
        }
        form.setFieldsValue({
            GroupId: null,
        })
    }

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]
        return {
            ...restValues,
            StartDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    useEffect(() => {
        getAgentOptions().then(result => setAgentIds(result))
        !isCailifun1.current && agentOptionsChange(userData.AgentId)
        onUpdate({
            ...resultCondition(form.getFieldsValue()),
        })
        onReady(true)
    }, [])

    return (
        <Form
            form={form}
            initialValues={{
                ...CONDITION_INITIAL_VALUE,
                AgentId: !isCailifun1.current ? userData.AgentId : null,
            }}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={6} hidden={!isCailifun1.current}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemUsers.QueryCondition.Agents"
                                defaultMessage="站台"
                            />
                        }
                        name="AgentId"
                    >
                        <Select onChange={agentOptionsChange}>
                            <Option value={null}>
                                <FormattedMessage
                                    id="PageSystemUsers.QueryCondition.AllAgent"
                                    defaultMessage="全部站台"
                                />
                            </Option>
                            {agentIds.map(item => (
                                <Option
                                    key={`agent_${item.AgentId}`}
                                    value={item.AgentId}
                                >
                                    {item.AgentName}
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemUsers.QueryCondition.BelongGroups"
                                defaultMessage="所屬群組"
                            />
                        }
                        name="GroupId"
                    >
                        <Select searchValue={null} disabled={!groupIds.length}>
                            <Option value={null}>
                                <FormattedMessage
                                    id="PageSystemUsers.QueryCondition.AllGroup"
                                    defaultMessage="所有群組"
                                />
                            </Option>
                            {groupIds.map(item => (
                                <Option
                                    key={`group_${item.Id}`}
                                    value={item.Id}
                                >
                                    {item.Name}
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemUsers.QueryCondition.CreateTime"
                                defaultMessage="建立時間"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemUsers.QueryCondition.Account"
                                defaultMessage="使用者帳號"
                            />
                        }
                        name="Account"
                    >
                        <Input
                            placeholder={intl.formatMessage({
                                id: 'PageSystemUsers.Form.UserPlaceholder',
                            })}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <Form.Item name="IsOnlyEnabled" valuePropName="checked">
                        <Checkbox>
                            <FormattedMessage
                                id="Share.QueryCondition.IsOnlyEnabled"
                                defaultMessage="不顯示停用帳號"
                            />
                        </Checkbox>
                    </Form.Item>
                </Col>
                <Col sm={isCailifun1.current ? 20 : 2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}
