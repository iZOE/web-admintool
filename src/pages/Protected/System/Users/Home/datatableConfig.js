import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Tag } from 'antd';
import DateWithFormat from 'components/DateWithFormat';
import OpenGroupDetailButton from 'components/OpenGroupDetailButton';
import OpenUserDetailButton from '../components/OpenUserDetailButton';

export const COLUMNS_CONFIG = [
  {
    key: 'Account',
    dataIndex: 'Account',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    title: <FormattedMessage id="PageSystemUsers.DataTable.Columns.Account" defaultMessage="使用者帳號" />,
    render: (text, record, index) => <OpenUserDetailButton userId={record.Id} userName={record.Account} />,
  },
  {
    key: 'GroupName',
    dataIndex: 'GroupName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    title: <FormattedMessage id="PageSystemUsers.DataTable.Columns.BelongGroups" defaultMessage="所屬群組" />,
    render: (text, record, index) => <OpenGroupDetailButton groupId={record.GroupId} groupName={record.GroupName} />,
  },
  {
    key: 'AgentName',
    dataIndex: 'AgentName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    title: <FormattedMessage id="PageSystemUsers.DataTable.Columns.AgentName" defaultMessage="站台" />,
  },
  {
    key: 'CreatedOn',
    dataIndex: 'CreatedOn',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    title: <FormattedMessage id="PageSystemUsers.DataTable.Columns.CreatedOn" defaultMessage="建立時間" />,
    render: (_1, { CreatedOn: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    key: 'IsEnable',
    dataIndex: 'IsEnable',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    title: <FormattedMessage id="PageSystemUsers.DataTable.Columns.IsEnable" defaultMessage="狀態" />,
    render: IsEnable => (
      <Tag color={IsEnable ? 'processing' : 'warning'}>
        <FormattedMessage
          id={`Share.Status.${IsEnable ? 'Enable' : 'Disable'}`}
          defaultMessage={IsEnable ? '啟用' : '停用'}
        />
      </Tag>
    ),
  },
  {
    key: 'IsLocked',
    dataIndex: 'IsLocked',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    title: <FormattedMessage id="PageSystemUsers.DataTable.Columns.IsLocked" defaultMessage="在線狀態" />,
    render: IsLocked => (
      <Tag color={IsLocked ? 'error' : 'warning'}>
        <FormattedMessage
          id={`PageSystemUsers.DataTable.Columns.${IsLocked ? 'Lock' : 'Offline'}`}
          defaultMessage={IsLocked ? '線上' : '離線'}
        />
      </Tag>
    ),
  },
];
