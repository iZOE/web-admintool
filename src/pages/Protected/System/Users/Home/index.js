import React, { useMemo } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { Link } from 'react-router-dom';
import { Button, Modal, Space } from 'antd';
import { UnlockOutlined } from '@ant-design/icons';
import { PlusOutlined } from '@ant-design/icons';
import { SORT_DIRECTION } from 'constants/sortDirection';
import * as PERMISSION from 'constants/permissions';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import usePermission from 'hooks/usePermission';
import DrawerContextProvider from 'contexts/DrawerContext';
import usersService from '../services';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const { SETTINGS_USERS_VIEW, SETTINGS_USERS_CREATE, SETTINGS_USERS_EDIT_INFO, SETTINGS_USERS_UNLOCK } = PERMISSION;
const { unLockUser } = usersService();

const { confirm } = Modal;

const PageView = () => {
  const intl = useIntl();
  const { fetching, dataSource, condition, onReady, onUpdateCondition } = useGetDataSourceWithSWR({
    url: '/api/User/Search',
    defaultSortKey: 'Account',
    defaultSortDirection: SORT_DIRECTION.Asc,
    autoFetch: true,
  });

  const [systemUsersEdit, systemUsersUnlock] = usePermission(SETTINGS_USERS_EDIT_INFO, SETTINGS_USERS_UNLOCK);

  const columnsConfig = useMemo(() => {
    if (!systemUsersEdit && !systemUsersUnlock) {
      return COLUMNS_CONFIG;
    } else {
      return [
        ...COLUMNS_CONFIG,
        {
          key: '',
          width: 60,
          dataIndex: 'Manage',
          isShow: true,
          isManage: true,
          align: 'center',
          render: (_, record) => (
            <Space size="small">
              <Permission functionIds={[SETTINGS_USERS_EDIT_INFO]}>
                <Link to={`/settings/users/detail/${record.Id}`}>
                  <Button type="link">
                    <FormattedMessage id="Share.ActionButton.Edit" defaultMessage="編輯" />
                  </Button>
                </Link>
              </Permission>
              <Permission functionIds={[SETTINGS_USERS_UNLOCK]}>
                <Button type="link" disabled={!record.IsLocked} onClick={() => onUnLockUser(record)}>
                  <FormattedMessage id="Share.ActionButton.UnLock" defaultMessage="解鎖" />
                </Button>
              </Permission>
            </Space>
          ),
        },
      ];
    }
  }, []);

  const onUnLockUser = record => {
    confirm({
      title: intl.formatMessage({
        id: 'PageSystemUsers.Modal.Title',
      }),
      icon: <UnlockOutlined />,
      content: intl.formatMessage(
        {
          id: 'PageSystemUsers.Modal.Content',
        },
        { user: record.Account }
      ),
      okText: intl.formatMessage({
        id: 'Share.ActionButton.Confirm',
        description: '保存',
      }),
      cancelText: intl.formatMessage({
        id: 'Share.ActionButton.Cancel',
        description: '取消',
      }),
      onOk() {
        unLockUser(record.Id);
        onUpdateCondition();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  return (
    <DrawerContextProvider>
      <Permission isPage functionIds={[SETTINGS_USERS_VIEW]}>
        <ReportScaffold
          displayResult={condition}
          conditionComponent={<Condition onReady={onReady} onUpdate={onUpdateCondition} />}
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={<FormattedMessage id="Share.Table.SearchResult" defaultMessage="查詢結果" />}
              loading={fetching}
              config={columnsConfig}
              dataSource={dataSource && dataSource.Data}
              total={dataSource && dataSource.TotalCount}
              extendArea={
                <Permission
                  functionIds={[SETTINGS_USERS_CREATE]}
                  failedRender={
                    <Button type="primary" icon={<PlusOutlined />} disabled>
                      <FormattedMessage
                        id="PageSystemUsers.DataTable.ExtendArea.CreateUser"
                        defaultMessage="新增使用者"
                      />
                    </Button>
                  }
                >
                  <Link to={'/settings/users/create'}>
                    <Button type="primary" icon={<PlusOutlined />}>
                      <FormattedMessage
                        id="PageSystemUsers.DataTable.ExtendArea.CreateUser"
                        defaultMessage="新增使用者"
                      />
                    </Button>
                  </Link>
                </Permission>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
      </Permission>
    </DrawerContextProvider>
  );
};

export default PageView;
