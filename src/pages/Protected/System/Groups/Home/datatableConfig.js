import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Tag } from 'antd'
import DateWithFormat from 'components/DateWithFormat'
import OpenGroupDetailButton from 'components/OpenGroupDetailButton'

export const COLUMNS_CONFIG = [
    {
        key: 'GroupId',
        dataIndex: 'GroupId',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage id="PageSystemGroups.DataTable.Columns.GroupId" />
        ),
    },
    {
        key: 'Name',
        dataIndex: 'Name',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage id="PageSystemGroups.DataTable.Columns.GroupName" />
        ),
        render: (_1, record) => (
            <OpenGroupDetailButton
                groupId={record.GroupId}
                groupName={record.Name}
            />
        ),
    },
    {
        key: 'AgentName',
        dataIndex: 'AgentName',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage id="PageSystemGroups.DataTable.Columns.AgentName" />
        ),
    },
    {
        key: 'UserTotal',
        dataIndex: 'UserTotal',
        isShow: true,
        title: (
            <FormattedMessage
                id="PageSystemGroups.DataTable.Columns.GroupNumber"
                values={{
                    breakingLine: <br />,
                }}
            />
        ),
    },
    {
        key: 'CreatedOn',
        dataIndex: 'CreatedOn',
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage
                id="PageSystemGroups.DataTable.Columns.CreatedOn"
                defaultMessage="建立時間"
            />
        ),
        render: (_1, { CreatedOn: time }, _2) => <DateWithFormat time={time} />,
    },
    {
        key: 'IsEnable',
        dataIndex: 'IsEnable',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        sortDirections: ['descend', 'ascend'],
        title: (
            <FormattedMessage
                id="PageSystemGroups.DataTable.Columns.IsEnable"
                defaultMessage="狀態"
            />
        ),
        render: IsEnable => (
            <Tag color={IsEnable ? 'processing' : 'warning'}>
                <FormattedMessage
                    id={`Share.Status.${IsEnable ? 'Enable' : 'Disable'}`}
                    defaultMessage={IsEnable ? '啟用' : '停用'}
                />
            </Tag>
        ),
    },
]
