import React, { useMemo } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { Link } from 'react-router-dom';
import { Button, Modal, Space } from 'antd';
import { ApiOutlined } from '@ant-design/icons';
import { PlusOutlined } from '@ant-design/icons';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import usePermission from 'hooks/usePermission';
import ReportScaffold from 'components/ReportScaffold';
import * as PERMISSION from 'constants/permissions';
import groupsService from '../services';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const {
  SETTINGS_GROUPS_VIEW,
  SETTINGS_GROUPS_CREATE,
  SETTINGS_GROUPS_EDIT_INFO,
  SETTINGS_GROUPS_CHANGE_STATUS,
  SETTINGS_GROUPS_FUNCTIONS,
} = PERMISSION;
const { toggleActive } = groupsService();

const { confirm } = Modal;

const PageView = () => {
  const intl = useIntl();
  const { fetching, dataSource, condition, onReady, onUpdateCondition } = useGetDataSourceWithSWR({
    url: '/api/Group/Search',
    defaultSortKey: 'GroupId',
    autoFetch: true,
  });

  const [systemGroupsEdit, systemGroupsChangeStatus, settingsGroupsFunctions] = usePermission(
    SETTINGS_GROUPS_EDIT_INFO,
    SETTINGS_GROUPS_CHANGE_STATUS,
    SETTINGS_GROUPS_FUNCTIONS
  );

  const columnsConfig = useMemo(() => {
    if (!systemGroupsEdit && !systemGroupsChangeStatus && !settingsGroupsFunctions) {
      return COLUMNS_CONFIG;
    } else {
      return [
        ...COLUMNS_CONFIG,
        {
          key: '',
          dataIndex: 'Manage',
          width: 80,
          isShow: true,
          isManage: true,
          align: 'center',
          render: (_, record) => (
            <Space size="small">
              <Permission functionIds={[SETTINGS_GROUPS_EDIT_INFO]}>
                <Link to={`/settings/groups/detail/${record.GroupId}`}>
                  <Button type="link">
                    <FormattedMessage id="Share.ActionButton.Edit" defaultMessage="編輯" />
                  </Button>
                </Link>
              </Permission>
              <Permission functionIds={[SETTINGS_GROUPS_CHANGE_STATUS]}>
                <Button type="link" danger={record.IsEnable} onClick={() => onToggleActive(record)}>
                  <FormattedMessage id={`Share.Status.${record.IsEnable ? 'Inactive' : 'Active'}`} />
                </Button>
              </Permission>
            </Space>
          ),
        },
      ];
    }
  }, [systemGroupsEdit, systemGroupsChangeStatus, settingsGroupsFunctions]);

  const onToggleActive = record => {
    confirm({
      title: intl.formatMessage({
        id: 'PageSystemGroups.Modal.ToggleActive.Title',
      }),
      icon: <ApiOutlined />,
      content: intl.formatMessage(
        {
          id: 'PageSystemGroups.Modal.ToggleActive.Content',
        },
        {
          status: intl.formatMessage({
            id: `Share.Status.${record.IsEnable ? 'Inactive' : 'Active'}`,
          }),
          user: record.Name,
        }
      ),
      okText: intl.formatMessage({
        id: 'Share.ActionButton.Confirm',
        description: '保存',
      }),
      cancelText: intl.formatMessage({
        id: 'Share.ActionButton.Cancel',
        description: '取消',
      }),
      onOk() {
        toggleActive(record.GroupId);
        onUpdateCondition();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  return (
    <Permission isPage functionIds={[SETTINGS_GROUPS_VIEW]}>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={<Condition onReady={onReady} onUpdate={onUpdateCondition} />}
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            title={<FormattedMessage id="Share.Table.SearchResult" />}
            loading={fetching}
            config={columnsConfig}
            dataSource={dataSource && dataSource.Data}
            total={dataSource && dataSource.TotalCount}
            rowKey={record => record.GroupId}
            extendArea={
              <Permission
                functionIds={[SETTINGS_GROUPS_CREATE]}
                failedRender={
                  <Button type="primary" icon={<PlusOutlined />} disabled>
                    <FormattedMessage id="PageSystemGroups.DataTable.ExtendArea.CreateGroup" />
                  </Button>
                }
              >
                <Link to={'/settings/groups/create'}>
                  <Button type="primary" icon={<PlusOutlined />}>
                    <FormattedMessage id="PageSystemGroups.DataTable.ExtendArea.CreateGroup" />
                  </Button>
                </Link>
              </Permission>
            }
            onUpdate={onUpdateCondition}
          />
        }
      />
    </Permission>
  );
};

export default PageView;
