import React, { useContext, useEffect, useRef, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Form, Row, Col, Input, Select, DatePicker, Checkbox } from 'antd'
import { AGENT_TYPE } from 'constants/agentType'
import { UserContext } from 'contexts/UserContext'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import optionsService from 'services/optionsServices'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const { getAgentOptions } = optionsService()

const CONDITION_INITIAL_VALUE = {
    AgentId: null,
    DisableGroupIsNotShown: false,
    GroupName: null,
    Date: RANGE.FROM_INITIAL_TO_TODAY,
}

export default function Condition({ onUpdate, onReady }) {
    const { data: userData } = useContext(UserContext)
    const isCailifun1 = useRef(userData.AgentId === AGENT_TYPE.CAILIFUN_I)
    const { formatMessage } = useIntl()
    const [agentIds, setAgentIds] = useState([])
    const [form] = useForm()

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            StartDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    useEffect(() => {
        isCailifun1.current &&
            getAgentOptions().then(result => setAgentIds(result))
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(true)
    }, [])

    return (
        <Form
            form={form}
            initialValues={{
                ...CONDITION_INITIAL_VALUE,
                AgentId: isCailifun1.current ? null : userData.AgentId,
            }}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={5} hidden={!isCailifun1.current}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemGroups.QueryCondition.Agents"
                                defaultMessage="站台"
                            />
                        }
                        name="AgentId"
                    >
                        {isCailifun1.current ? (
                            <Select>
                                <Option value={null}>
                                    <FormattedMessage
                                        id="PageSystemGroups.QueryCondition.AllAgent"
                                        defaultMessage="全部站台"
                                    />
                                </Option>
                                {agentIds.map(item => (
                                    <Option
                                        key={`agent_${item.AgentId}`}
                                        value={item.AgentId}
                                    >
                                        {item.AgentName}
                                    </Option>
                                ))}
                            </Select>
                        ) : (
                            <Input type="hidden" />
                        )}
                    </Form.Item>
                </Col>
                <Col sm={7}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageSystemGroups.QueryCondition.CreateTime"
                                defaultMessage="建立時間"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DAILY}
                        />
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageSystemGroups.QueryCondition.GroupName" />
                        }
                        name="GroupName"
                    >
                        <Input
                            placeholder={formatMessage({
                                id:
                                    'PageSystemGroups.QueryCondition.GroupNamePlaceholder',
                            })}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <Form.Item
                        name="DisableGroupIsNotShown"
                        valuePropName="checked"
                    >
                        <Checkbox>
                            <FormattedMessage id="Share.QueryCondition.IsOnlyEnabled" />
                        </Checkbox>
                    </Form.Item>
                </Col>
                <Col
                    sm={{ span: 2, offset: isCailifun1.current ? 0 : 5 }}
                    align="right"
                >
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}
