import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import Create from './Create';
import Detail from './Detail';

export default function SettingsGroups() {
  return (
    <Switch>
      <Route exact path="/settings/groups">
        <Home />
      </Route>
      <Route path="/settings/groups/create">
        <Create />
      </Route>
      <Route path="/settings/groups/detail/:id">
        <Detail />
      </Route>
    </Switch>
  );
}
