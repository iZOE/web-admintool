import React from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { trigger } from 'swr';
import { Button, Col, Form, Modal, Row, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import GroupFunctionCascader from 'components/GroupFunctionCascader';
import groupsService from '../../services';

const { confirm } = Modal;
const { useForm } = Form;
const { updateGroupFunctionIds } = groupsService();

export default function Permissions({
  groupId,
  agentId,
  functionPermissionIds,
  onResetTouch
}) {
  const intl = useIntl();
  const history = useHistory();
  const [form] = useForm();

  const showSaveConfirm = () => {
    confirm({
      title: intl.formatMessage({
        id: 'Share.ActionButton.Save'
      }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage({
        id: 'Share.ConfirmTips.WhetherToSaveExistingChanges'
      }),
      okText: intl.formatMessage({
        id: 'Share.ActionButton.Confirm'
      }),
      cancelText: intl.formatMessage({
        id: 'Share.ActionButton.Cancel',
        description: '取消'
      }),
      onOk() {
        onResetTouch();
        updateGroupFunctionIds({
          GroupId: groupId,
          FunctionIds: form.getFieldValue('functionPermissionIds'),
          AgentId: agentId
        }).then(res => {
          trigger(`/api/Group/FunctionPermission/${groupId}`);
        });
      },
      onCancel() {
        console.log('Cancel');
      }
    });
  };
  const showResetConfirm = () => {
    confirm({
      title: intl.formatMessage({
        id: 'Share.CommonKeys.FillInAgain'
      }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage({
        id: 'Share.ConfirmTips.DoYouWantToReeditTheContent'
      }),
      okText: intl.formatMessage({ id: 'Share.ActionButton.Restart' }),
      okType: 'danger',
      cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
      onOk() {
        form.resetFields();
        onResetTouch();
      },
      onCancel() {
        console.log('Cancel showResetConfirm');
      }
    });
  };
  const showCancelConfirm = () => {
    confirm({
      title: intl.formatMessage({ id: 'Share.CommonKeys.GiveUpEditing' }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage(
        {
          id: 'Share.ConfirmTips.DoYouWantToGiveUpEditingAndReturn'
        },
        {
          targetname: intl.formatMessage({
            id: 'PageSystemGroups.Title'
          })
        }
      ),
      okText: intl.formatMessage({
        id: 'Share.ActionButton.Abandon'
      }),
      okType: 'danger',
      cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
      onOk() {
        history.push('/settings/groups/');
      },
      onCancel() {
        console.log('Cancel showCancelConfirm');
      }
    });
  };

  return (
    <Form form={form} name="GroupPermissions" onFinish={showSaveConfirm}>
      <Form.Item
        name="functionPermissionIds"
        initialValue={functionPermissionIds}
      >
        <GroupFunctionCascader />
      </Form.Item>
      <Row justify="center">
        <Col span={8}>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              disabled={!form.isFieldsTouched()}
            >
              <FormattedMessage id="Share.ActionButton.Submit" />
            </Button>
            <Button
              htmlType="button"
              onClick={showResetConfirm}
              disabled={!form.isFieldsTouched()}
            >
              <FormattedMessage id="Share.ActionButton.Restart" />
            </Button>
            <Button type="link" htmlType="button" onClick={showCancelConfirm}>
              <FormattedMessage id="Share.ActionButton.Cancel" />
            </Button>
          </Space>
        </Col>
      </Row>
    </Form>
  );
}
