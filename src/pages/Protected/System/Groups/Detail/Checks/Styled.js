import styled from 'styled-components';

export const Wrapper = styled.div`
  .ant-table-wrapper {
    margin-bottom: 24px;
    .ant-table-cell {
      .ant-form-item {
        margin: 0;
      }
    }
  }
`;
