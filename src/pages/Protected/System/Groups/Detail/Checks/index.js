import React, { useEffect, useMemo, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Form,
  Input,
  Modal,
  Select,
  Skeleton,
  Space,
  Table
} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import optionsService from 'services/optionsServices';
import groupsService from '../../services';
import { Wrapper } from './Styled';

const { confirm } = Modal;
const { useForm } = Form;
const { Option } = Select;
const { getTopGroupOptions } = optionsService();
const {
  getFunctionPermissionApproved,
  updateFunctionPermissionApproved
} = groupsService();

const APPROVED_OPTIONS = [0, 1, 2];

export default function Checks({ groupId, agentId, onResetTouch }) {
  const intl = useIntl();
  const history = useHistory();
  const [form] = useForm();
  const [approvedData, setApprovedData] = useState(null);
  const [topGroupOptions, setTopGroupOptions] = useState(null);

  const getTopGroupOptionsByAgentId = () =>
    getTopGroupOptions({
      selfGroupId: groupId || -1,
      oldGroupId: -1,
      agentId
    }).then(res => {
      setTopGroupOptions(res);
    });

  const showResetConfirm = () => {
    confirm({
      title: intl.formatMessage({
        id: 'Share.CommonKeys.FillInAgain'
      }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage({
        id: 'Share.ConfirmTips.DoYouWantToReeditTheContent'
      }),
      okText: intl.formatMessage({ id: 'Share.ActionButton.Restart' }),
      okType: 'danger',
      cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
      onOk() {
        form.resetFields();
        if (onResetTouch) {
          onResetTouch();
        }
      },
      onCancel() {
        console.log('Cancel showResetConfirm');
      }
    });
  };

  const showCancelConfirm = () => {
    confirm({
      title: intl.formatMessage({ id: 'Share.CommonKeys.GiveUpEditing' }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage(
        {
          id: 'Share.ConfirmTips.DoYouWantToGiveUpEditingAndReturn'
        },
        {
          targetname: intl.formatMessage({
            id: 'PageSystemGroups.Title'
          })
        }
      ),
      okText: intl.formatMessage({
        id: 'Share.ActionButton.Abandon'
      }),
      okType: 'danger',
      cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
      onOk() {
        history.push('/settings/groups/');
      },
      onCancel() {
        console.log('Cancel showCancelConfirm');
      }
    });
  };

  const onFinish = values => {
    confirm({
      title: intl.formatMessage({
        id: 'Share.ActionButton.Save'
      }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage({
        id: 'Share.ConfirmTips.WhetherToSaveExistingChanges'
      }),
      okText: intl.formatMessage({
        id: 'Share.ActionButton.Save'
      }),
      cancelText: intl.formatMessage({
        id: 'Share.ActionButton.Cancel'
      }),
      onOk() {
        const payload = Object.keys(values).reduce(
          (res, key) => {
            if (key === 'ParentGroupId') {
              return {
                ...res,
                ParentGroupId: values[key]
              };
            }
            if (!isNaN(Number(key))) {
              return {
                ...res,
                ApprovedList: [
                  ...res.ApprovedList,
                  {
                    ...values[key],
                    CountersignGroupId:
                      values[key]['ApprovedType'] !== 1
                        ? null
                        : values[key]['CountersignGroupId']
                  }
                ]
              };
            }
            return res;
          },
          {
            GroupId: groupId,
            AgentId: agentId,
            ParentGroupId: null,
            ApprovedList: []
          }
        );
        console.log('onOk -> payload', payload);
        updateFunctionPermissionApproved(payload).then(res => {
          if (onResetTouch) {
            onResetTouch();
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      }
    });
  };

  const onParentGroupChange = value => {
    const fieldsValue = form.getFieldsValue();
    form.setFields(
      Object.keys(fieldsValue).reduce((prev, curr) => {
        if (
          !value &&
          fieldsValue[curr] &&
          fieldsValue[curr]['ApprovedType'] !== null &&
          fieldsValue[curr]['ApprovedType'] < 2
        ) {
          let result = [
            ...prev,
            {
              name: [curr, 'ApprovedType'],
              value: null
            }
          ];

          if (fieldsValue[curr]['CountersignGroupId']) {
            result = [
              ...result,
              {
                name: [curr, 'CountersignGroupId'],
                value: null
              }
            ];
          }

          return result;
        }
        if (
          value &&
          fieldsValue[curr] &&
          fieldsValue[curr]['CountersignGroupId'] === value
        ) {
          return [
            ...prev,
            {
              name: [curr, 'CountersignGroupId'],
              value: null
            }
          ];
        }
        return prev;
      }, [])
    );
  };

  useEffect(() => {
    getTopGroupOptionsByAgentId();
    getFunctionPermissionApproved(groupId).then(
      ({ ParentGroupId, ApprovedList }) => {
        setApprovedData({
          ParentGroupId,
          ApprovedList: ApprovedList.map(item => {
            form.setFieldsValue({
              [item.FunctionId]: item
            });
            return { ...item, key: item.FunctionId };
          })
        });
      }
    );
  }, []);

  const COLUMNS_CONFIG = [
    {
      key: 'FunctionName',
      dataIndex: 'FunctionName',
      title: (
        <FormattedMessage id="PageSystemGroups.DataTable.Columns.ApprovedName" />
      ),
      width: 250,
      render: (FunctionName, record) => (
        <>
          {FunctionName}
          <Form.Item
            noStyle
            name={[record.FunctionId, 'FunctionId']}
            initialValue={record.FunctionId}
          >
            <Input type="hidden" />
          </Form.Item>
          <Form.Item
            noStyle
            name={[record.FunctionId, 'GroupFunctionPermissionId']}
            initialValue={record.GroupFunctionPermissionId}
          >
            <Input type="hidden" />
          </Form.Item>
        </>
      )
    },
    {
      key: 'ApprovedType',
      dataIndex: 'ApprovedType',
      title: (
        <FormattedMessage id="PageSystemGroups.DataTable.Columns.ApprovedType" />
      ),
      width: 300,
      render: (ApprovedType, record) => {
        return (
          <Form.Item
            noStyle
            shouldUpdate={(prevValues, curValues) =>
              prevValues.ParentGroupId !== curValues.ParentGroupId ||
              prevValues[record.FunctionId]['ApprovedType'] !==
                curValues[record.FunctionId]['ApprovedType']
            }
          >
            {({ getFieldValue }) => {
              return (
                <Input.Group>
                  <Form.Item
                    name={[record.FunctionId, 'ApprovedType']}
                    dependencies={['ParentGroupId']}
                    initialValue={
                      !ApprovedType
                        ? ApprovedType === 0
                          ? ApprovedType
                          : null
                        : ApprovedType
                    }
                    rules={[
                      {
                        required: true
                      }
                    ]}
                  >
                    <Select>
                      <Option value={null}>
                        <FormattedMessage
                          id="Share.PlaceHolder.Input.PleaseSelectSomething"
                          values={{
                            field: (
                              <FormattedMessage id="PageSystemGroups.DataTable.Columns.ApprovedType" />
                            )
                          }}
                        />
                      </Option>
                      {APPROVED_OPTIONS.map(option => (
                        <Option
                          key={option}
                          value={option}
                          disabled={
                            option < 2 && !getFieldValue('ParentGroupId')
                          }
                        >
                          <FormattedMessage
                            id={`PageSystemGroups.Form.ApprovedOptions.${option +
                              (option < 2 && !getFieldValue('ParentGroupId')
                                ? 3
                                : 0)}`}
                          />
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                  {getFieldValue(record.FunctionId)['ApprovedType'] === 1 && (
                    <Form.Item
                      name={[record.FunctionId, 'CountersignGroupId']}
                      initialValue={record.CountersignGroupId || null}
                      rules={[
                        {
                          required: true
                        }
                      ]}
                    >
                      <Select>
                        <Option value={null}>
                          <FormattedMessage
                            id="Share.PlaceHolder.Input.PleaseSelectSomething"
                            values={{
                              field: (
                                <FormattedMessage id="PageSystemGroups.DataTable.Columns.GroupName" />
                              )
                            }}
                          />
                        </Option>
                        {topGroupOptions &&
                          topGroupOptions.map(
                            option =>
                              option.Id !== getFieldValue('ParentGroupId') && (
                                <Option key={option.Id} value={option.Id}>
                                  {option.Name}
                                </Option>
                              )
                          )}
                      </Select>
                    </Form.Item>
                  )}
                </Input.Group>
              );
            }}
          </Form.Item>
        );
      }
    },
    {
      key: 'Restrict',
      dataIndex: 'Restrict',
      title: (
        <FormattedMessage id="PageSystemGroups.DataTable.Columns.Restrict" />
      )
    }
  ];

  return approvedData ? (
    <Wrapper>
      <Form form={form} name="GroupApproved" onFinish={onFinish}>
        <Form.Item
          name="ParentGroupId"
          label={<FormattedMessage id="PageSystemGroups.Form.ParentGroup" />}
          initialValue={approvedData.ParentGroupId}
        >
          <Select onChange={onParentGroupChange}>
            <Option value={null}>
              <FormattedMessage id="PageSystemGroups.Form.SelectParentGroup" />
            </Option>
            {topGroupOptions &&
              topGroupOptions.map(option => (
                <Option key={option.Id} value={option.Id}>
                  {option.Name}
                </Option>
              ))}
          </Select>
        </Form.Item>
        <Table
          bordered
          dataSource={approvedData.ApprovedList}
          columns={COLUMNS_CONFIG}
          pagination={false}
        />
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16
          }}
        >
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              disabled={!form.isFieldsTouched()}
            >
              <FormattedMessage id="Share.ActionButton.Submit" />
            </Button>
            <Button
              htmlType="button"
              onClick={showResetConfirm}
              disabled={!form.isFieldsTouched()}
            >
              <FormattedMessage id="Share.ActionButton.Restart" />
            </Button>
            <Button type="link" htmlType="button" onClick={showCancelConfirm}>
              <FormattedMessage id="Share.ActionButton.Cancel" />
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Wrapper>
  ) : (
    <Skeleton active />
  );
}
