import React from "react";
import { useIntl } from "react-intl";
import { message, Skeleton } from "antd";
import Form from "../../components/Form";
import groupsService from "../../services";

const { updateGroup } = groupsService();

export default function BasicData({ groupData, onResetTouch }) {
  const intl = useIntl();

  function onSave(payload) {
    updateGroup(payload)
      .then(() => {
        message.success(
          intl.formatMessage({
            id: "Share.SuccessMessage.UpdateSuccess"
          }),
          10
        );
        onResetTouch();
        // history.push('/settings/users/');
      })
      .catch(({ ErrorKey, Message }) => {
        onResetTouch();
        message.warning(
          intl.formatMessage({
            id: "Share.ErrorMessage.UnknownError",
            description: "不明錯誤"
          }),
          10
        );
      });
  }

  return groupData ? (
    <Form
      isEditMode
      defaultValues={groupData}
      onSubmit={onSave}
      onResetTouch={onResetTouch}
    />
  ) : (
    <Skeleton active />
  );
}
