import React, { useEffect, useState, lazy, Suspense } from 'react'
import { useParams, useLocation } from 'react-router-dom'
import useSWR from 'swr'
import axios from 'axios'
import { FormattedMessage, useIntl } from 'react-intl'
import { Form, Modal, Spin, Tabs } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import LayoutPageBody from 'layouts/Page/Body'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import PageHeaderTab from 'components/PageHeaderTab'
import groupsService from '../services'

const { SETTINGS_USERS_CREATE } = PERMISSION
const { TabPane } = Tabs
const { confirm } = Modal
const { getGroup } = groupsService()

const BasicDataAsync = lazy(() => import('./BasicData'))
const PermissionsAsync = lazy(() => import('./Permissions'))
const ChecksAsync = lazy(() => import('./Checks'))

export default function Detail() {
    const searchParams = new URLSearchParams(useLocation().search)
    const DEFAULT_TAB = searchParams.get('tab') || 'basicData'
    const [groupData, setGroupData] = useState(null)
    const [currentTab, setCurrentTab] = useState(DEFAULT_TAB)
    const [isFormTouch, setIsFormTouch] = useState(false)
    const { id } = useParams()
    const intl = useIntl()

    const {
        data: functionPermissionIds,
    } = useSWR(`/api/Group/FunctionPermission/${id}`, url =>
        axios(url).then(res => res && res.data.Data)
    )

    useEffect(() => {
        getGroup(id).then(res => setGroupData(res))
    }, [])

    function tabChange(tabKey) {
        if (isFormTouch) {
            confirm({
                title: intl.formatMessage({
                    id: 'PageSystemGroups.Modal.TabChange.Title',
                }),
                icon: <ExclamationCircleOutlined />,
                content: intl.formatMessage({
                    id: 'PageSystemGroups.Modal.TabChange.Content',
                }),
                okText: intl.formatMessage({
                    id: 'Share.ActionButton.Confirm',
                }),
                cancelText: intl.formatMessage({
                    id: 'Share.ActionButton.Cancel',
                }),
                onOk() {
                    setIsFormTouch(false)
                    setCurrentTab(tabKey)
                },
                onCancel() {
                    console.log('Cancel showResetConfirm')
                },
            })
        } else {
            setCurrentTab(tabKey)
        }
    }

    function formChange(formName, { forms }) {
        setIsFormTouch(forms[formName].isFieldsTouched())
    }

    return (
        <>
            <PageHeaderTab defaultActiveKey={DEFAULT_TAB} onChange={tabChange}>
                <TabPane
                    tab={
                        <FormattedMessage id="GroupDetail.Tabs.BasicData.Title" />
                    }
                    key="basicData"
                />
                <TabPane
                    tab={
                        <FormattedMessage id="GroupDetail.Tabs.Permissions.Title" />
                    }
                    key="permissions"
                />
                <TabPane
                    tab={
                        <FormattedMessage id="GroupDetail.Tabs.Checks.Title" />
                    }
                    key="checks"
                />
            </PageHeaderTab>
            <Permission isPage functionIds={[SETTINGS_USERS_CREATE]}>
                <LayoutPageBody
                    title={
                        <FormattedMessage id="PageSystemGroups.DataTable.Title.EditGroup" />
                    }
                >
                    <Form.Provider onFormChange={formChange}>
                        <Suspense fallback={<Spin />}>
                            {currentTab === 'basicData' && (
                                <BasicDataAsync
                                    groupData={groupData}
                                    onResetTouch={() => setIsFormTouch(false)}
                                />
                            )}
                            {currentTab === 'permissions' && groupData && (
                                <PermissionsAsync
                                    groupId={id}
                                    agentId={groupData.AgentId}
                                    functionPermissionIds={
                                        functionPermissionIds
                                    }
                                    onResetTouch={() => setIsFormTouch(false)}
                                />
                            )}
                            {currentTab === 'checks' && groupData && (
                                <ChecksAsync
                                    groupId={id}
                                    agentId={groupData.AgentId}
                                    functionPermissionIds={
                                        functionPermissionIds
                                    }
                                    onResetTouch={() => setIsFormTouch(false)}
                                />
                            )}
                        </Suspense>
                    </Form.Provider>
                </LayoutPageBody>
            </Permission>
        </>
    )
}
