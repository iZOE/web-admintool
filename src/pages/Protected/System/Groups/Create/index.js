import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';
import { message } from 'antd';
import LayoutPageBody from 'layouts/Page/Body';
import * as PERMISSION from 'constants/permissions';
import { UserContext } from 'contexts/UserContext';
import Permission from 'components/Permission';
import Form from '../components/Form';
import groupsService from '../services';

const { SETTINGS_USERS_CREATE } = PERMISSION;

const { addGroup } = groupsService();

export default function Create() {
  const intl = useIntl();
  const history = useHistory();
  const { data: userData } = useContext(UserContext);

  const DEFAULT_VALUES = {
    AgentId: userData.AgentId,
    AgentName: userData.AgentName,
    Name: null,
    ParentGroupId: null,
    IsEnable: true
  };

  function onSave(payload) {
    addGroup(payload)
      .then(res => {
        message.success(
          intl.formatMessage({
            id: 'Share.UIResponse.CreateSuccess'
          }),
          10
        );
        history.push(`/settings/groups/detail/${res}?tab=permissions`);
      })
      .catch(({ ErrorKey, Message }) => {
        message.warning(
          intl.formatMessage({
            id: 'Share.ErrorMessage.UnknownError',
            description: '不明錯誤'
          }),
          10
        );
      });
  }

  return (
    <Permission isPage functionIds={[SETTINGS_USERS_CREATE]}>
      <LayoutPageBody
        title={
          <FormattedMessage id="PageSystemGroups.DataTable.Title.CreateGroup" />
        }
      >
        <Form defaultValues={DEFAULT_VALUES} onSubmit={onSave} />
      </LayoutPageBody>
    </Permission>
  );
}
