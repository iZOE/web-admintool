import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { useHistory } from 'react-router-dom'
import {
    Button,
    Form,
    Input,
    Modal,
    Select,
    Skeleton,
    Space,
    Switch,
} from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import DateWithFormat from 'components/DateWithFormat'
import optionsService from 'services/optionsServices'

const { confirm } = Modal
const { useForm } = Form
const { Option } = Select
const { getAgentOptions } = optionsService()

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 8,
    },
}
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
}

function ActiveForm({ isEditMode, defaultValues, onSubmit, onResetTouch }) {
    const [agentOptions, setAgentOptions] = useState(null)
    const history = useHistory()
    const [form] = useForm()
    const intl = useIntl()

    useEffect(() => {
        if (defaultValues.AgentId === 1) {
            getAgentOptions().then(res => setAgentOptions(res))
        }
    }, [defaultValues])

    const onFinish = values => {
        confirm({
            title: intl.formatMessage({
                id: isEditMode
                    ? 'Share.ActionButton.Save'
                    : 'Share.ActionButton.Submit',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: isEditMode
                    ? 'Share.ConfirmTips.WhetherToSaveExistingChanges'
                    : 'PageSystemGroups.Form.ConfirmAddGroup',
            }),
            okText: intl.formatMessage({
                id: isEditMode
                    ? 'Share.ActionButton.Save'
                    : 'Share.ActionButton.Confirm',
                description: '保存',
            }),
            cancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
                description: '取消',
            }),
            onOk() {
                onSubmit(values)
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    function showResetConfirm() {
        confirm({
            title: intl.formatMessage({
                id: 'Share.CommonKeys.FillInAgain',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: 'Share.ConfirmTips.DoYouWantToReeditTheContent',
            }),
            okText: intl.formatMessage({ id: 'Share.ActionButton.Restart' }),
            okType: 'danger',
            cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            onOk() {
                form.resetFields()
                if (onResetTouch) {
                    onResetTouch()
                }
            },
            onCancel() {
                console.log('Cancel showResetConfirm')
            },
        })
    }

    function showCancelConfirm() {
        confirm({
            title: intl.formatMessage({ id: 'Share.CommonKeys.GiveUpEditing' }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage(
                {
                    id: 'Share.ConfirmTips.DoYouWantToGiveUpEditingAndReturn',
                },
                {
                    targetname: intl.formatMessage({
                        id: 'PageSystemGroups.Title',
                    }),
                }
            ),
            okText: intl.formatMessage({
                id: 'Share.ActionButton.Abandon',
            }),
            okType: 'danger',
            cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            onOk() {
                history.push('/settings/groups/')
            },
            onCancel() {
                console.log('Cancel showCancelConfirm')
            },
        })
    }

    return defaultValues ? (
        <Form
            {...layout}
            form={form}
            initialValues={defaultValues}
            name="GroupBasicData"
            onFinish={onFinish}
        >
            {isEditMode && (
                <Form.Item
                    name="GroupId"
                    label={<FormattedMessage id="PageSystemGroups.Form.Id" />}
                >
                    <>
                        <Input type="hidden" />
                        {defaultValues.GroupId}
                    </>
                </Form.Item>
            )}
            <Form.Item
                name="Name"
                label={
                    <FormattedMessage id="PageSystemGroups.Form.GroupName" />
                }
                rules={[
                    {
                        required: true,
                        message: (
                            <FormattedMessage id="Share.FormValidate.Required.Input" />
                        ),
                    },
                ]}
            >
                <Input
                    placeholder={intl.formatMessage({
                        id: 'PageSystemGroups.Form.GroupNamePlaceholder',
                    })}
                />
            </Form.Item>
            {defaultValues.AgentId !== 1 ? (
                <Form.Item
                    name="AgentId"
                    label={
                        <FormattedMessage id="PageSystemGroups.Form.Agent" />
                    }
                >
                    <>
                        <Input type="hidden" />
                        {defaultValues.AgentName}
                    </>
                </Form.Item>
            ) : (
                <Form.Item
                    name="AgentId"
                    label={
                        <FormattedMessage id="PageSystemGroups.Form.Agent" />
                    }
                    rules={[
                        {
                            required: true,
                            message: (
                                <FormattedMessage id="Share.FormValidate.Required.Input" />
                            ),
                        },
                    ]}
                >
                    <Select loading={!agentOptions}>
                        <Option value={null}>
                            <FormattedMessage id="PageSystemGroups.Form.SelectAgent" />
                        </Option>
                        {agentOptions &&
                            agentOptions.map(option => (
                                <Option
                                    key={option.AgentId}
                                    value={option.AgentId}
                                >
                                    {option.AgentName}
                                </Option>
                            ))}
                    </Select>
                </Form.Item>
            )}
            <Form.Item
                name="IsEnable"
                valuePropName="checked"
                label={<FormattedMessage id="Share.CommonKeys.Status" />}
            >
                <Switch
                    checkedChildren={
                        <FormattedMessage id="Share.Status.Enable" />
                    }
                    unCheckedChildren={
                        <FormattedMessage id="Share.Status.Disable" />
                    }
                />
            </Form.Item>
            <Form.Item
                name="Memo"
                label={<FormattedMessage id="PageSystemGroups.Form.Memo" />}
            >
                <Input.TextArea />
            </Form.Item>
            {isEditMode && (
                <>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageSystemGroups.Form.CreatedOn" />
                        }
                    >
                        <DateWithFormat time={defaultValues.CreatedOn} />
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageSystemGroups.Form.CreatedBy" />
                        }
                    >
                        {defaultValues.CreatedByName}
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageSystemGroups.Form.ModifyOn" />
                        }
                    >
                        <DateWithFormat time={defaultValues.ModifiedOn} />
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageSystemGroups.Form.ModifyBy" />
                        }
                    >
                        {defaultValues.ModifiedByName}
                    </Form.Item>
                </>
            )}
            <Form.Item {...tailLayout}>
                <Space>
                    <Button
                        type="primary"
                        htmlType="submit"
                        disabled={!form.isFieldsTouched() && isEditMode}
                    >
                        <FormattedMessage id="Share.ActionButton.Submit" />
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={showResetConfirm}
                        disabled={!form.isFieldsTouched() && isEditMode}
                    >
                        <FormattedMessage id="Share.ActionButton.Restart" />
                    </Button>
                    <Button
                        type="link"
                        htmlType="button"
                        onClick={showCancelConfirm}
                    >
                        <FormattedMessage id="Share.ActionButton.Cancel" />
                    </Button>
                </Space>
            </Form.Item>
        </Form>
    ) : (
        <Skeleton active />
    )
}

ActiveForm.defaultProps = {
    isEditMode: false,
}

ActiveForm.propTypes = {
    isEditMode: PropTypes.bool,
    defaultValues: PropTypes.shape({
        Name: PropTypes.string,
        GroupId: PropTypes.number,
        AgentId: PropTypes.number,
        AgentName: PropTypes.string,
        CreatedBy: PropTypes.string,
        ParentGroupId: PropTypes.number,
        ParentGroupName: PropTypes.string,
        IsEnable: PropTypes.bool,
        Memo: PropTypes.string,
        ModifiedByName: PropTypes.string,
        ModifiedOn: PropTypes.string,
    }),
    onSubmit: PropTypes.func.isRequired,
}

export default ActiveForm
