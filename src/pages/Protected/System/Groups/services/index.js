import makeService from "utils/makeService";

const groupsService = () => {
  const getGroup = groupId =>
    makeService({
      method: "get",
      url: `/api/Group/${groupId}`
    })();

  const addGroup = payload =>
    makeService({
      method: "post",
      url: "/api/Group"
    })(payload);

  const updateGroup = payload =>
    makeService({
      method: "put",
      url: "/api/Group"
    })(payload);

  const toggleActive = Id =>
    makeService({
      method: "patch",
      url: `/api/Group/ChangeState/${Id}`
    })();

  const getGroupFunctionIds = groupId =>
    makeService({
      method: "get",
      url: `/api/Group/FunctionPermission/${groupId}`
    })();

  const updateGroupFunctionIds = payload =>
    makeService({
      method: "post",
      url: `/api/Group/FunctionPermission`
    })(payload);

  const getFunctionPermissionApproved = groupId =>
    makeService({
      method: "get",
      url: `/api/Group/FunctionPermissionApproved/${groupId}`
    })();

  const updateFunctionPermissionApproved = payload =>
    makeService({
      method: "post",
      url: "/api/Group/FunctionPermissionApproved"
    })(payload);

  return {
    getGroup,
    addGroup,
    updateGroup,
    toggleActive,
    getGroupFunctionIds,
    updateGroupFunctionIds,
    getFunctionPermissionApproved,
    updateFunctionPermissionApproved
  };
};

export default groupsService;
