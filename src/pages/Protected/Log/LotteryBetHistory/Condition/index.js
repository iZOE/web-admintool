import React, { useContext, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { Form, Row, Col, Input, DatePicker } from 'antd';
import _flattendeep from 'lodash.flattendeep';
import { FORMAT, DEFAULT } from 'constants/dateConfig';
import { RULES } from 'constants/activity/validators';
import { ConditionContext } from 'contexts/ConditionContext';
import AdvancedSearchItem from 'components/FormItems/AdvancedSearchItem';
import FormItemsMemberLevel from 'components/FormItems/MemberLevel';
import FormItemsLotteries from 'components/FormItems/Lotteries';
import FormItemsBetTypes from 'components/FormItems/BetTypes';
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect';
import FormItemIncludeInnerMember from 'components/FormItems/IncludeInnerMember';
import QueryButton from 'components/FormActionButtons/Query';
import { getISODateTimeString } from 'mixins/dateTime';

const { useForm } = Form;
const { RANGE } = DEFAULT;

const ADVANCE_ITEM_WITH_AMOUNT = new Set([4, 5]);
const ADVANCED_SEARCH_CONDITION_ITEM = [1, 2, 3, 4, 5];

const CONDITION_INITIAL_VALUE = {
  AdvancedSearchConditionItem: 1,
  BetTypeId: [],
  Date: RANGE.FROM_A_QUARTER_TO_TODAY,
  IncludeInnerMember: false,
  IssueNumber: null,
  BetOrderNo: null,
  Source: null,
  Status: null,
};

function Condition({ onUpdate, onReady }) {
  const [form] = useForm();
  const { RangePicker } = DatePicker;
  const { isReady } = useContext(ConditionContext);

  useEffect(() => {
    onUpdate(resultCondition(form.getFieldsValue()));
    onReady(isReady);
  }, [isReady]);

  function resultCondition({ Date, BetTypeId, ...restValues }) {
    const [StartDate, EndDate] = Date || [null, null];

    return {
      ...restValues,
      BetTypeId: _flattendeep(BetTypeId),
      StartDate: getISODateTimeString(StartDate),
      EndDate: getISODateTimeString(EndDate),
    };
  }

  function onFinish(values) {
    onUpdate.bySearch(resultCondition(values));
  }

  return (
    <Form
      form={form}
      onFinish={onFinish}
      initialValues={CONDITION_INITIAL_VALUE}
      onValuesChange={changedValues => {
        if (changedValues.AdvancedSearchConditionItem) {
          form.setFieldsValue({
            AdvancedSearchConditionValue: null,
            AdvancedSearchConditionFrom: null,
            AdvancedSearchConditionTo: null,
          });
        }
        if (changedValues.LotteryCode) {
          form.setFieldsValue({
            BetTypeId: [],
          });
        }
      }}
    >
      <Row gutter={[16, 8]}>
        <Col sm={12}>
          <Form.Item
            label={<FormattedMessage id="PageProductsBet.QueryCondition.DateTime" description="查詢時間" />}
            name="Date"
            rules={[...RULES.REQUIRED]}
          >
            <RangePicker style={{ width: '100%' }} showTime format={FORMAT.DISPLAY.DEFAULT} />
          </Form.Item>
        </Col>
        <Col sm={4}>
          <FormItemsSimpleSelect
            url="/api/option/bet/source"
            name="Source"
            label={<FormattedMessage id="Share.FormItem.Device" description="裝置" />}
          />
        </Col>
        <Col sm={8}>
          <FormItemsLotteries checkAll />
        </Col>
        <Col sm={6}>
          <Form.Item shouldUpdate noStyle>
            {({ getFieldValue }) => <FormItemsBetTypes ids={getFieldValue('LotteryCode')} />}
          </Form.Item>
        </Col>
        <Col sm={4}>
          <FormItemsSimpleSelect
            url="/api/option/bet/status"
            name="Status"
            label={<FormattedMessage id="Share.FormItem.BetStatus" description="狀態" />}
          />
        </Col>
        <Col sm={6}>
          <Form.Item
            name="IssueNumber"
            label={<FormattedMessage id="PageProductsBet.QueryCondition.IssueNumber" description="投注期號" />}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={6}>
          <Form.Item
            name="BetOrderNo"
            label={<FormattedMessage id="PageProductsBet.QueryCondition.BetOrderNo" description="訂單編號" />}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={6}>
          <FormItemsMemberLevel checkAll />
        </Col>
        <Col sm={10}>
          <AdvancedSearchItem
            OptionItems={ADVANCED_SEARCH_CONDITION_ITEM}
            MessageId="PageProductsBet.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions"
            AmountSet={ADVANCE_ITEM_WITH_AMOUNT}
          />
        </Col>
        <Col sm={4}>
          <FormItemIncludeInnerMember />
        </Col>
        <Col sm={4} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
