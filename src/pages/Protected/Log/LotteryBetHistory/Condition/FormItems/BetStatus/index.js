import React from 'react'
import { Form, Select } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import { FormattedMessage } from 'react-intl'

const { Option } = Select

export default function LotteryStatus() {
    const { data } = useSWR('/api/option/bet/status', url =>
        axios(url).then(res => res.data.Data)
    )

    return (
        <Form.Item
            label={
                <FormattedMessage
                    id="Share.FormItem.BetStatus"
                    description="狀態"
                />
            }
            name="Status"
        >
            <Select>
                {data && data.map(d => <Option value={d.Id}>{d.Name}</Option>)}
            </Select>
        </Form.Item>
    )
}
