import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import OrderDetailButton from '../components/OrderDetailContextProvider/OrderDetailButton';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.BetOrderNo" description="订单ID" />,
    dataIndex: 'BetOrderNo',
    key: 'BetOrderNo',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OrderDetailButton id={text}>{text}</OrderDetailButton>,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.CreateTime" description="投注时间" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (text ? <DateWithFormat time={text} /> : null),
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.MemberName" description="会员帐号" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.IssueNumber" description="期号" />,
    dataIndex: 'IssueNumber',
    key: 'IssueNumber',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.LotteryName" description="彩种" />,
    dataIndex: 'LotteryName',
    key: 'LotteryName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => record.LotteryName,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.BetTypeName" description="玩法" />,
    dataIndex: 'BetTypeName',
    key: 'BetTypeName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.BetTypeName,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.BetSubTypeName" description="子玩法" />,
    dataIndex: 'BetSubTypeName',
    key: 'BetSubTypeName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.BetSubTypeName,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.BetContent" description="投注内容" />,
    dataIndex: 'BetContent',
    key: 'BetContent',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (text, record) => (
      <OrderDetailButton id={record.BetOrderNo}>
        <div
          style={{
            textAlign: 'left',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            maxWidth: 120,
          }}
        >
          {record.BetContentLocaleText}
        </div>
      </OrderDetailButton>
    ),
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.BonusNumber" description="开奖结果" />,
    dataIndex: 'BonusNumber',
    key: 'BonusNumber',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) =>
      record.BonusNumber ? (
        <div
          style={{
            textAlign: 'left',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            maxWidth: 170,
          }}
        >
          {record.BonusNumber}
        </div>
      ) : (
        NO_DATA
      ),
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.TotalBet" description="注数" />,
    dataIndex: 'TotalBet',
    key: 'TotalBet',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.BetUnitPrice" description="单注金额" />,
    dataIndex: 'BetUnitPrice',
    key: 'BetUnitPrice',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.TotalBetAmount" description="投注额" />,
    dataIndex: 'TotalBetAmount',
    key: 'TotalBetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.VaildBetAmount" description="有效投注金额" />,
    dataIndex: 'VaildBetAmount',
    key: 'VaildBetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.Profit" description="输赢" />,
    dataIndex: 'WinLossAmount',
    key: 'WinLossAmount',
    render: (_1, record) =>
      typeof record.WinLossAmount === 'number' && record.BonusNumber && record.Status !== 3 ? (
        <span style={{ color: record.WinLossAmount > 0 ? '#f50' : null }}>
          <ReactIntlCurrencyWithFixedDecimal value={record.WinLossAmount} />
        </span>
      ) : (
        NO_DATA
      ),
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.Odds" description="赔率" />,
    dataIndex: 'Odds',
    key: 'Odds',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.AnnounceTime" description="封盘时间" />,
    dataIndex: 'AnnounceTime',
    key: 'AnnounceTime',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (text ? <DateWithFormat time={text} /> : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.BonusDateTime" description="结算时间" />,
    dataIndex: 'BonusDateTime',
    key: 'BonusDateTime',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (text ? <DateWithFormat time={text} /> : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.Status" description="状态" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    render: (_1, record) => record.StatusText,
  },
  {
    title: <FormattedMessage id="PageProductsBet.DataTable.Title.Remarks" description="说明" />,
    dataIndex: 'Remarks',
    key: 'Remarks',
    isShow: true,
  },
];
