import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary }) => {
    return summary ? (
        <Row justify="space-between">
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageProductsBet.Summary.TotalOrderNumber"
                            description="總訂單數"
                        />
                    }
                    value={summary.TotalOrderNumber}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageProductsBet.Summary.TotalMemberCount"
                            description="投注人數"
                        />
                    }
                    value={summary.TotalMemberCount}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageProductsBet.Summary.TotalBetAmount"
                            description="投注金額"
                        />
                    }
                    value={summary.TotalBetAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageProductsBet.Summary.TotalWinAmount"
                            description="中獎金額"
                        />
                    }
                    value={summary.TotalWinAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageProductsBet.Summary.TotalProfit"
                            description="平台盈虧"
                        />
                    }
                    value={summary.TotalProfit}
                    precision={2}
                />
            </Col>
        </Row>
    ) : (
        <Skeleton active />
    )
}

export default SummaryView
