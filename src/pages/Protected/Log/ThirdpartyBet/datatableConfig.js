import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.BetOrderNo" description="訂單ID" />,
    dataIndex: 'BetOrderNo',
    key: 'BetOrderNo',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.BetTime" description="投注時間" />,
    dataIndex: 'BetTime',
    key: 'BetTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { BetTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageLogBet.DataTable.Title.BetTime" description="投注時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemBetTime',
    key: 'SystemBetTime',
    render: (_1, { SystemBetTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.DeviceTypeText" description="來源" />,
    dataIndex: 'DeviceTypeText',
    key: 'DeviceTypeText',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return 'DeviceType';
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.PlatformType" description="平台" />,
    dataIndex: 'PlatformType',
    key: 'PlatformType',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.GameDescription" description="遊戲" />,
    dataIndex: 'GameDescription',
    key: 'GameDescription',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.TableCode" description="桌號" />,
    dataIndex: 'TableCode',
    key: 'TableCode',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.GameIssueNumber" description="局號" />,
    dataIndex: 'GameIssueNumber',
    key: 'GameIssueNumber',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (text, record, index) => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.BetAmount" description="投注額" />,
    dataIndex: 'BetAmount',
    key: 'BetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.IswinText" description="輸贏" />,
    dataIndex: 'IswinText',
    key: 'IswinText',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.WinLossAmount" description="個人盈虧" />,
    dataIndex: 'WinLossAmount',
    key: 'WinLossAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.ValidBetAmount" description="有效投注額" />,
    dataIndex: 'ValidBetAmount',
    key: 'ValidBetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.BonusAmount" description="中獎金額" />,
    key: 'BonusAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (text, row) => <ReactIntlCurrencyWithFixedDecimal value={row.BetAmount + row.WinLossAmount} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.BetStatus" description="狀態" />,
    dataIndex: 'BetStatus',
    key: 'BetStatus',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.SettledTime" description="結算時間" />,
    dataIndex: 'SettledTime',
    key: 'SettledTime',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { SettledTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.ModifyTime" description="更新時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.FullEventName" description="賽事項目" />,
    dataIndex: 'FullEventName',
    key: 'FullEventName',
    isShow: true,
    render: text => (text ? <span dangerouslySetInnerHTML={{ __html: text }} /> : null),
  },
  {
    title: <FormattedMessage id="PageLogBet.DataTable.Title.BetContentDetail" description="投注內容" />,
    dataIndex: 'BetContentDetail',
    key: 'BetContentDetail',
    isShow: true,
    render: text => (text ? <span dangerouslySetInnerHTML={{ __html: text }} /> : null),
  },
];
