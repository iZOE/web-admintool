import React, { useContext, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { Form, Row, Col, Select, DatePicker, Input } from 'antd';
import { FORMAT, DEFAULT } from 'constants/dateConfig';
import { RULES } from 'constants/activity/validators';
import { ConditionContext } from 'contexts/ConditionContext';
import AdvancedSearchItem from 'components/FormItems/AdvancedSearchItem';
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember';
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect';
import QueryButton from 'components/FormActionButtons/Query';
import { getISODateTimeString } from 'mixins/dateTime';

const { useForm } = Form;
const { Option } = Select;

const { RangePicker } = DatePicker;
const { RANGE } = DEFAULT;

const ADVANCE_ITEM_WITH_AMOUNT = new Set([4, 5]);
const ADVANCED_SEARCH_CONDITION_ITEM_OPTIONS = [1, 2, 3];

const CONDITION_INITIAL_VALUE = {
  SearchDateType: 1,
  Date: RANGE.FROM_TODAY_TO_TODAY,
  AdvancedSearchConditionItem: 1,
  IncludeInnerMember: false,
  WinLoss: null,
  Source: null,
  MemberName: null,
  Status: null,
};

function Condition({ onReady, onUpdate }) {
  const { isReady } = useContext(ConditionContext);
  const [form] = useForm();

  useEffect(() => {
    onUpdate(resultCondition(form.getFieldsValue()));
    onReady(isReady);
  }, [isReady]);

  function resultCondition({ Date, ...restValues }) {
    const [StartTime, EndTime] = Date || [null, null];

    return {
      ...restValues,
      StartTime: getISODateTimeString(StartTime),
      EndTime: getISODateTimeString(EndTime),
    };
  }

  function onFinish(values) {
    onUpdate.bySearch(resultCondition(values));
  }

  return (
    <Form
      form={form}
      onFinish={onFinish}
      initialValues={CONDITION_INITIAL_VALUE}
      onValuesChange={changedValues => {
        if (changedValues.AdvancedSearchConditionItem) {
          form.setFieldsValue({
            AdvancedSearchConditionValue: null,
            AdvancedSearchConditionFrom: null,
            AdvancedSearchConditionTo: null,
          });
        }
      }}
    >
      <Row gutter={[16, 8]}>
        <Col sm={4}>
          <Form.Item
            label={<FormattedMessage id="PageLogBet.QueryCondition.DateTime" description="查詢時間" />}
            name="SearchDateType"
          >
            <Select>
              <Option value={1}>
                <FormattedMessage
                  id="PageLogBet.QueryCondition.SelectItem.DateTime.1"
                  description="投注時間(BetTime)"
                />
              </Option>
              <Option value={2}>
                <FormattedMessage
                  id="PageLogBet.QueryCondition.SelectItem.DateTime.2"
                  description="結算時間(SettledTime)"
                />
              </Option>
            </Select>
          </Form.Item>
        </Col>
        <Col sm={8}>
          <Form.Item name="Date" rules={[...RULES.REQUIRED]}>
            <RangePicker style={{ width: '100%' }} showTime format={FORMAT.DISPLAY.DEFAULT} />
          </Form.Item>
        </Col>
        <Col sm={6}>
          <FormItemMultipleSelect
            checkAll
            name="GameProviderIdList"
            label={<FormattedMessage id="PageLogBet.QueryCondition.GameProvider" description="遊戲" />}
            url="/api/Option/V2/ThirdPartyLotteryTypes"
          />
        </Col>
        <Col sm={6}>
          <AdvancedSearchItem
            OptionItems={ADVANCED_SEARCH_CONDITION_ITEM_OPTIONS}
            MessageId="PageLogBet.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions"
            AmountSet={ADVANCE_ITEM_WITH_AMOUNT}
          />
        </Col>
        <Col sm={6}>
          <Form.Item
            label={<FormattedMessage id="PageLogBet.QueryCondition.OrderId" description="訂單編號" />}
            name="BetOrderNo"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={{ span: 4, offset: 12 }} align="right">
          <FormItemsIncludeInnerMember />
        </Col>
        <Col sm={2} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
