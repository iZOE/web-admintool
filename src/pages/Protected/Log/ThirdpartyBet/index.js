import React, { createContext } from 'react'
import { Button, Result } from 'antd'
import { ExportOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ExportReportButton from 'components/ExportReportButton'
import ReportScaffold from 'components/ReportScaffold'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import SummaryView from './Summary'

const { LOG_THIRDPARTY_BET_VIEW, LOG_THIRDPARTY_BET_EXPORT } = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    const {
        fetching,
        dataSource,
        onUpdateCondition,
        onReady,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/ThirdPartyBetLog/Search',
        defaultSortKey: 'BetTime',
        autoFetch: true,
    })

    return (
        <Permission
            functionIds={[LOG_THIRDPARTY_BET_VIEW]}
            failedRender={
                <Result
                    status="warning"
                    title={
                        <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                    }
                    subTitle={
                        <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                    }
                />
            }
        >
            <ReportScaffold
                displayResult={condition}
                conditionComponent={
                    <Condition onReady={onReady} onUpdate={onUpdateCondition} />
                }
                summaryComponent={
                    <SummaryView
                        displayResult={condition}
                        summary={dataSource && dataSource.Summary}
                        totalCount={dataSource && dataSource.List.TotalCount}
                    />
                }
                datatableComponent={
                    <DataTable
                        displayResult={condition}
                        condition={condition}
                        title={
                            <FormattedMessage
                                id="Share.Table.SearchResult"
                                description="查詢絝果"
                            />
                        }
                        config={COLUMNS_CONFIG}
                        loading={fetching}
                        dataSource={dataSource && dataSource.List.Data}
                        rowKey={record => record.BetOrderNo}
                        extendArea={
                            <Permission
                                functionIds={[LOG_THIRDPARTY_BET_EXPORT]}
                                failedRender={
                                    <Button
                                        type="primary"
                                        icon={<ExportOutlined />}
                                        disabled
                                    >
                                        <FormattedMessage id="Share.ActionButton.Export" />
                                    </Button>
                                }
                            >
                                <ExportReportButton
                                    condition={condition}
                                    actionUrl="/api/ThirdPartyBetLog/Export"
                                />
                            </Permission>
                        }
                        onUpdate={onUpdateCondition}
                        total={dataSource && dataSource.List.TotalCount}
                    />
                }
            />
        </Permission>
    )
}

export default PageView
