import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary, totalCount }) => {
    return summary ? (
        <Row justify="space-between">
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogBet.Summary.TotalCount"
                            description="總訂單數"
                        />
                    }
                    value={totalCount}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogBet.Summary.NumberOfMembers"
                            description="投注人數"
                        />
                    }
                    value={summary.NumberOfMembers}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogBet.Summary.BetAmount"
                            description="投注金額"
                        />
                    }
                    value={summary.BetAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogBet.Summary.ActualBetAmount"
                            description="有效投注額"
                        />
                    }
                    value={summary.ActualBetAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogBet.Summary.BonusAmount"
                            description="中獎金額"
                        />
                    }
                    value={summary.BonusAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogBet.Summary.WinLoss"
                            description="平台盈虧"
                        />
                    }
                    value={summary.WinLoss}
                    precision={2}
                />
            </Col>
        </Row>
    ) : (
        <Skeleton active />
    )
}

export default SummaryView
