import React from 'react'
import { Modal, Descriptions, Skeleton, Typography } from 'antd'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'

const { Text } = Typography

export default function DetailModal({ visible, id, onOk }) {
    const { data } = useSWR(visible && id ? `/api/Bets/${id}` : null, url =>
        axios(url).then(res => res && res.data.Data)
    )

    return (
        <Modal
            title={
                <FormattedMessage
                    id="PageProductComponents.OrderDetailModal.Title"
                    description="投注详情"
                />
            }
            visible={visible}
            width={900}
            onOk={onOk}
            onCancel={onOk}
        >
            {data ? (
                <>
                    <Descriptions layout="vertical" column={{ md: 4 }} bordered>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.BetOrderNo" />
                            }
                        >
                            <Text>{data.BetOrderNo}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.CreateTime" />
                            }
                        >
                            <Text>
                                <DateWithFormat time={data.CreateTime} />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="Share.Fields.ModifiedTime" />
                            }
                        >
                            <Text>
                                <DateWithFormat time={data.ModifyDate} />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.Status" />
                            }
                        >
                            <Text>{data.StatusText}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.MemberName" />
                            }
                        >
                            <OpenMemberDetailButton
                                memberName={data.MemberName}
                            >
                                <Text>{data.MemberName}</Text>
                            </OpenMemberDetailButton>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.TotalBetAmount" />
                            }
                        >
                            <Text>
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={data.TotalBetAmount}
                                />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.BonusAmount" />
                            }
                        >
                            <Text>
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={data.BonusAmount}
                                />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.Profit" />
                            }
                        >
                            <Text>
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={data.Profit}
                                />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.IssueNumber" />
                            }
                        >
                            <Text>{data.IssueNumber}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.LotteryName" />
                            }
                        >
                            <Text>{data.LotteryName}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.BetTypeName" />
                            }
                        >
                            <Text>{data.BetTypeName}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.BetSubTypeName" />
                            }
                        >
                            <Text>{data.BetSubTypeName}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.Odds" />
                            }
                        >
                            <Text>
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={data.Odds}
                                />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.TotalBet" />
                            }
                        >
                            <Text>{data.TotalBet}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.Multiple" />
                            }
                        >
                            <Text>{data.Multiple}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductComponents.OrderDetailModal.BonusNumber" />
                            }
                        >
                            <Text>{data.BonusNumber}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            span={4}
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.BetContent" />
                            }
                        >
                            <Text>{data.BetContentLocaleText}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.Remarks" />
                            }
                        >
                            <Text>{data.Remarks}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.Mode" />
                            }
                        >
                            <Text>{data.Mode}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.ReturnPoint" />
                            }
                        >
                            <Text>{data.ReturnPoint}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsBet.DataTable.Title.Source" />
                            }
                        >
                            <Text>{data.SourceText}</Text>
                        </Descriptions.Item>
                    </Descriptions>
                </>
            ) : (
                <Skeleton />
            )}
        </Modal>
    )
}
