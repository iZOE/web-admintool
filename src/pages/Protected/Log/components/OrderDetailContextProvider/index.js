import React, { useState, createContext } from 'react'
import OrderModal from './OrderModal'

export const OrderDetailContext = createContext()

export default function OrderDetailContextProvider({ children }) {
    const [modalData, setModalData] = useState({ id: null, visible: false })

    function onOpenOrderModal({ id }) {
        setModalData({
            id,
            visible: true,
        })
    }

    function onCloseOrderModal() {
        setModalData({
            id: null,
            visible: false,
        })
    }

    return (
        <OrderDetailContext.Provider value={{ onOpenOrderModal }}>
            <OrderModal
                visible={modalData.visible}
                id={modalData.id}
                onOk={onCloseOrderModal}
            />
            {children}
        </OrderDetailContext.Provider>
    )
}
