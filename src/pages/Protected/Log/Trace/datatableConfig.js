import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import OrderDetailButton from './OrderDetailButton';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.TraceOrderNo" description="" />,
    dataIndex: 'TraceOrderNo',
    key: 'TraceOrderNo',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OrderDetailButton id={text}>{text}</OrderDetailButton>,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.CreateTime" description="投注時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.Source" description="裝置" />,
    dataIndex: 'Source',
    key: 'Source',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.SourceText,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.LotteryCodeName" description="彩種" />,
    dataIndex: 'LotteryCode',
    key: 'LotteryCode',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => record.LotteryCodeName,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.BeginIssueNumber" description="起始期號" />,
    dataIndex: 'BeginIssueNumber',
    key: 'BeginIssueNumber',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.MemberName" description="会员帐号" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.BetRuleName" description="玩法" />,
    dataIndex: 'BetRuleName',
    key: 'BetRuleName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.BetSubTypeName,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.TotalBetAmount" description="總投注額" />,
    dataIndex: 'TotalBetAmount',
    key: 'TotalBetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.TotalIssue" description="總期數" />,
    dataIndex: 'TotalIssue',
    key: 'TotalIssue',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.CurrentIssue" description="已追期數" />,
    dataIndex: 'CurrentIssue',
    key: 'CurrentIssue',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.IsStopAfterWin" description="中獎停止" />,
    dataIndex: 'TotalBetAmount',
    key: 'TotalBetAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.IsStopAfterWinText,
  },
  {
    title: <FormattedMessage id="PageProductsTrace.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record) => record.StatusText,
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyDate',
    key: 'ModifyDate',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyDate: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemModifyDate',
    key: 'SystemModifyDate',
    render: (_1, { SystemModifyDate: time }, _2) => <DateWithFormat time={time} />,
  },
];
