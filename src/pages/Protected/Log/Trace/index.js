import React, { createContext, useState } from 'react';
import { message, Button, Result, Space, Modal } from 'antd';
import { ExclamationCircleOutlined, ScissorOutlined } from '@ant-design/icons';
import { FormattedMessage, useIntl } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import caller from 'utils/fetcher';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ExportReportButton from 'components/ExportReportButton';
import ReportScaffold from 'components/ReportScaffold';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import OrderModal from './OrderModal';
import OrderDetailContextProvider from '../components/OrderDetailContextProvider';

const { LOG_TRACE_VIEW, LOG_TRACE_CANCEL_TRACE_ORDER } = PERMISSION;

export const PageContext = createContext();
const { confirm } = Modal;

const PageView = () => {
  const intl = useIntl();
  const successMsg = intl.formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess',
  });
  const [modalData, setModalData] = useState({
    visible: false,
  });
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    onReady,
    boundedMutate,
  } = useGetDataSourceWithSWR({
    url: '/api/Traces/Search',
    defaultSortKey: 'CreateTime',
    autoFetch: true,
  });

  function onOpenOrderModal({ id }) {
    setModalData({
      id,
      visible: true,
    });
  }

  function onCloseOrderModal() {
    setModalData({
      id: null,
      visible: false,
    });
  }

  function onSelectChange(selectedRowKeys) {
    setSelectedRowKeys(selectedRowKeys);
  }

  async function onCancelOrders() {
    try {
      confirm({
        title: intl.formatMessage({
          id: 'PageProductsTrace.CancelOrderModal.Cancel',
        }),
        icon: <ExclamationCircleOutlined />,
        content: intl.formatMessage({
          id: 'PageProductsTrace.CancelOrderModal.CancelConfirm',
        }),
        okText: intl.formatMessage({
          id: 'PageProductsTrace.CancelOrderModal.Cancel',
        }),
        cancelText: intl.formatMessage({
          id: 'Share.ActionButton.Cancel',
        }),
        async onOk() {
          try {
            await caller({
              method: 'patch',
              endpoint: '/api/Traces/Cancel',
              body: selectedRowKeys,
            });
            message.success(successMsg, 5);
            boundedMutate();
          } catch (error) {
            message.warning(
              intl.formatMessage({
                id: 'Share.ErrorMessage.UnknownError',
                description: '不明錯誤',
              }),
              10
            );
          }
        },
        onCancel() {
          console.log('Cancel');
        },
      });
    } catch (error) {}
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    getCheckboxProps: record => ({
      disabled: record.Status !== 1,
      name: record.name,
    }),
  };

  return (
    <OrderDetailContextProvider>
      <PageContext.Provider value={{ onOpenOrderModal }}>
        <OrderModal
          visible={modalData.visible}
          id={modalData.id}
          onOk={onCloseOrderModal}
        />
        <Permission
          functionIds={[LOG_TRACE_VIEW]}
          failedRender={
            <Result
              status="warning"
              title={
                <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
              }
              subTitle={
                <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
              }
            />
          }
        >
          <ReportScaffold
            displayResult={condition}
            conditionComponent={
              <Condition onReady={onReady} onUpdate={onUpdateCondition} />
            }
            datatableComponent={
              <DataTable
                displayResult={condition}
                condition={condition}
                title={
                  <FormattedMessage
                    id="Share.Table.SearchResult"
                    description="查詢絝果"
                  />
                }
                rowKey={record => record.TraceOrderNo}
                rowSelection={rowSelection}
                config={COLUMNS_CONFIG}
                loading={fetching}
                dataSource={dataSource && dataSource.Data}
                total={dataSource && dataSource.TotalCount}
                extendArea={
                  <Space align="start">
                    <ExportReportButton
                      condition={condition}
                      actionUrl="/api/Traces/Export"
                    />
                    <Permission functionIds={[LOG_TRACE_CANCEL_TRACE_ORDER]}>
                      <Button
                        type="primary"
                        danger
                        icon={<ScissorOutlined />}
                        disabled={selectedRowKeys.length === 0}
                        onClick={onCancelOrders}
                      >
                        <FormattedMessage id="PageProductsTrace.CancelOrderModal.Cancel" />
                      </Button>
                    </Permission>
                  </Space>
                }
                onUpdate={onUpdateCondition}
              />
            }
          />
        </Permission>
      </PageContext.Provider>
    </OrderDetailContextProvider>
  );
};

export default PageView;
