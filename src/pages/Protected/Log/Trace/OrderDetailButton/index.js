import React, { useContext } from 'react'
import { Button } from 'antd'
import { PageContext } from '../index'

export default function OrderDetailButton({ id, children }) {
    const { onOpenOrderModal } = useContext(PageContext)
    return (
        <Button
            type="link"
            style={{ padding: 0 }}
            onClick={() => onOpenOrderModal({ id })}
        >
            {children}
        </Button>
    )
}
