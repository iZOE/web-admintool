import React from 'react'
import { Descriptions, Skeleton, Typography } from 'antd'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'

const { Text } = Typography

export default function Info({ id }) {
    const { data } = useSWR(id ? `/api/Traces/${id}` : null, url =>
        axios(url).then(res => res && res.data.Data)
    )

    return (
        <>
            {data ? (
                <>
                    <Descriptions bordered>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.TraceOrderNo" />
                            }
                        >
                            <Text>{data.TraceOrderNo}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.Status" />
                            }
                        >
                            <Text>{data.StatusText}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.CreateTime" />
                            }
                            span={2}
                        >
                            <Text>
                                <DateWithFormat time={data.CreateTime} />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.ModifyDate" />
                            }
                            span={2}
                        >
                            <Text>
                                <DateWithFormat time={data.ModifyDate} />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.Source" />
                            }
                            span={2}
                        >
                            <Text>{data.SourceText}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.OrderModal.Info.TraceMode" />
                            }
                            span={2}
                        >
                            <Text>{data.TraceModeText}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.MemberName" />
                            }
                        >
                            <OpenMemberDetailButton
                                memberName={data.MemberName}
                            >
                                <Text>{data.MemberName}</Text>
                            </OpenMemberDetailButton>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.LotteryCodeName" />
                            }
                        >
                            <Text>{data.LotteryCodeName}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.BeginIssueNumber" />
                            }
                        >
                            <Text>{data.BeginIssueNumber}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.BetRuleName" />
                            }
                        >
                            <Text>{data.BetSubTypeName}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.IsStopAfterWin" />
                            }
                        >
                            <Text>{data.IsStopAfterWinText}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.OrderModal.Info.Mode" />
                            }
                        >
                            <Text>{data.Mode}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.OrderModal.Info.BetContent" />
                            }
                        >
                            <Text>{data.BetContentLocaleText}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.OrderModal.Info.TotalBet" />
                            }
                        >
                            <Text>{data.TotalBet}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.TotalIssue" />
                            }
                        >
                            <Text>{data.TotalIssue}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.CurrentIssue" />
                            }
                        >
                            <Text>{data.CurrentIssue}</Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.DataTable.Title.TotalBetAmount" />
                            }
                        >
                            <Text>
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={data.TotalBetAmount}
                                />
                            </Text>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PageProductsTrace.OrderModal.Info.BetAmountSoFar" />
                            }
                        >
                            <Text>
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={data.BetAmountSoFar}
                                />
                            </Text>
                        </Descriptions.Item>
                    </Descriptions>
                </>
            ) : (
                <Skeleton />
            )}
        </>
    )
}
