import React from 'react'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import DataTable from 'components/DataTable'
import { COLUMNS_CONFIG } from './datatableConfig'

export default function Info({ id }) {
    const { data: dataSource, isValidating } = useSWR(
        id ? `/api/Traces/${id}/Plan` : null,
        url => axios(url).then(res => res && res.data.Data)
    )

    return (
        <DataTable
            noHeader
            title={
                <FormattedMessage
                    id="Share.Table.SearchResult"
                    description="查詢絝果"
                />
            }
            config={COLUMNS_CONFIG}
            loading={isValidating}
            dataSource={dataSource}
        />
    )
}
