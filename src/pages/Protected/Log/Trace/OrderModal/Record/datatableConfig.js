import React from 'react'
import { FormattedMessage } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import OrderDetailButton from '../../../components/OrderDetailContextProvider/OrderDetailButton'

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="PageProductsTrace.OrderModal.Record.SortOrder"
                description=""
            />
        ),
        dataIndex: 'SortOrder',
        key: 'SortOrder',
        isShow: true,
    },
    {
        title: (
            <FormattedMessage
                id="PageProductsTrace.OrderModal.Record.IssueNumber"
                description=""
            />
        ),
        dataIndex: 'IssueNumber',
        key: 'IssueNumber',
        isShow: true,
    },
    {
        title: (
            <FormattedMessage
                id="PageProductsTrace.OrderModal.Record.Multiple"
                description=""
            />
        ),
        dataIndex: 'Multiple',
        key: 'Multiple',
        isShow: true,
    },
    {
        title: (
            <FormattedMessage
                id="PageProductsTrace.OrderModal.Record.BetAmount"
                description=""
            />
        ),
        dataIndex: 'BetAmount',
        key: 'BetAmount',
        isShow: true,
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage
                id="PageProductsTrace.OrderModal.Record.Status"
                description=""
            />
        ),
        dataIndex: 'Status',
        key: 'Status',
        isShow: true,
        render: (_1, record) => record.StatusText,
    },
    {
        title: (
            <FormattedMessage
                id="PageProductsTrace.OrderModal.Record.BetOrderNo"
                description=""
            />
        ),
        dataIndex: 'BetOrderNo',
        key: 'BetOrderNo',
        isShow: true,
        render: text => <OrderDetailButton id={text}>{text}</OrderDetailButton>,
    },
]
