import React from 'react'
import { Modal, Tabs } from 'antd'
import { FormattedMessage } from 'react-intl'
import Info from './Info'
import Record from './Record'

const { TabPane } = Tabs

export default function DetailModal({ visible, id, onOk }) {
    return (
        <Modal
            title={
                <FormattedMessage
                    id="PageProductsTrace.OrderModal.Title"
                    description=""
                />
            }
            visible={visible}
            width={900}
            onOk={onOk}
            onCancel={onOk}
        >
            <Tabs defaultActiveKey="1">
                <TabPane
                    tab={
                        <FormattedMessage
                            id="PageProductsTrace.Tabs.Title.0"
                            description="基本資料"
                        />
                    }
                    key="1"
                >
                    <Info id={id} />
                </TabPane>
                <TabPane
                    tab={
                        <FormattedMessage
                            id="PageProductsTrace.Tabs.Title.1"
                            description="追號紀錄"
                        />
                    }
                    key="2"
                >
                    <Record id={id} />
                </TabPane>
            </Tabs>
        </Modal>
    )
}
