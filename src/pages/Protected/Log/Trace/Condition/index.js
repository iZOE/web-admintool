import React, { useContext, useEffect } from 'react'
import { FormattedMessage } from 'react-intl'
import { Form, Row, Col, DatePicker } from 'antd'
import _flattendeep from 'lodash.flattendeep'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { ConditionContext } from 'contexts/ConditionContext'
import { getISODateTimeString } from 'mixins/dateTime'
import AdvancedSearchItem from 'components/FormItems/AdvancedSearchItem'
import FormItemsMemberLevel from 'components/FormItems/MemberLevel'
import FormItemsLotteries from 'components/FormItems/Lotteries'
import FormItemsBetTypes from 'components/FormItems/BetTypes'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import FormItemIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import QueryButton from 'components/FormActionButtons/Query'

const { useForm } = Form
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const ADVANCED_SEARCH_CONDITION_ITEM = [1, 2, 3, 4]

const CONDITION_INITIAL_VALUE = {
    AdvancedSearchConditionItem: 1,
    BetTypeId: [],
    Date: RANGE.FROM_TODAY_TO_TODAY,
    IncludeInnerMember: false,
    Source: null,
    Status: null,
}

const ADVANCE_ITEM_WITH_AMOUNT = new Set([4])

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ BetTypeId, Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            BetTypeId: _flattendeep(BetTypeId),
            StartDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
            onValuesChange={(changedValues, allValues) => {
                if (changedValues.AdvancedSearchConditionItem) {
                    form.setFieldsValue({
                        AdvancedSearchConditionValue: null,
                        AdvancedSearchConditionFrom: null,
                        AdvancedSearchConditionTo: null,
                    })
                }
                if (changedValues.LotteryCode) {
                    form.setFieldsValue({
                        BetTypeId: [],
                    })
                }
            }}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PageProductsBet.QueryCondition.DateTime"
                                description="查詢時間"
                            />
                        }
                        name="Date"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsSimpleSelect
                        url="/api/option/bet/source"
                        name="Source"
                        label={
                            <FormattedMessage
                                id="Share.FormItem.Device"
                                description="裝置"
                            />
                        }
                    />
                </Col>
                <Col sm={6}>
                    <FormItemsLotteries checkAll />
                </Col>
                <Col sm={6}>
                    <Form.Item shouldUpdate noStyle>
                        {({ getFieldValue }) => (
                            <FormItemsBetTypes
                                ids={getFieldValue('LotteryCode')}
                            />
                        )}
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsSimpleSelect
                        url="/api/option/trace-order/status"
                        name="Status"
                        label={
                            <FormattedMessage
                                id="Share.FormItem.BetStatus"
                                description="狀態"
                            />
                        }
                    />
                </Col>
                <Col sm={6}>
                    <FormItemsMemberLevel checkAll />
                </Col>
                <Col sm={8}>
                    <AdvancedSearchItem
                        OptionItems={ADVANCED_SEARCH_CONDITION_ITEM}
                        MessageId="PageProductsTrace.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions"
                        AmountSet={ADVANCE_ITEM_WITH_AMOUNT}
                    />
                </Col>
                <Col sm={4}>
                    <FormItemIncludeInnerMember />
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
