import React, { useState } from 'react';
import { message, Button, Result, Space, Modal } from 'antd';
import { ExclamationCircleOutlined, ScissorOutlined } from '@ant-design/icons';
import { FormattedMessage, useIntl } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import caller from 'utils/fetcher';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ExportReportButton from 'components/ExportReportButton';
import ReportScaffold from 'components/ReportScaffold';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import SummaryView from './Summary';
import OrderDetailContextProvider from '../components/OrderDetailContextProvider';

const { LOG_LOTTERY_BET_VIEW, LOG_LOTTERY_BET_CANCEL_ORDER } = PERMISSION;

const { confirm } = Modal;

const PageView = () => {
  const intl = useIntl();
  const successMsg = intl.formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess',
  });
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    onReady,
    boundedMutate,
  } = useGetDataSourceWithSWR({
    url: '/api/Bets/Search',
    defaultSortKey: 'CreateTime',
    autoFetch: true,
  });

  function onSelectChange(selectedRowKeys) {
    setSelectedRowKeys(selectedRowKeys);
  }

  async function onCancelOrders() {
    try {
      confirm({
        title: intl.formatMessage({
          id: 'PageProductsBet.CancelOrderModal.Cancel',
        }),
        icon: <ExclamationCircleOutlined />,
        content: intl.formatMessage({
          id: 'PageProductsBet.CancelOrderModal.CancelConfirm',
        }),
        okText: intl.formatMessage({
          id: 'PageProductsBet.CancelOrderModal.Cancel',
        }),
        cancelText: intl.formatMessage({
          id: 'Share.ActionButton.Cancel',
        }),
        async onOk() {
          try {
            await caller({
              method: 'patch',
              endpoint: '/api/Bets/Cancel',
              body: selectedRowKeys,
            });
            message.success(successMsg, 5);
            boundedMutate();
          } catch (error) {
            message.warning(
              intl.formatMessage({
                id: 'Share.ErrorMessage.UnknownError',
                description: '不明錯誤',
              }),
              10
            );
          }
        },
        onCancel() {
          console.log('Cancel');
        },
      });
    } catch (error) {}
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <OrderDetailContextProvider>
      <Permission
        functionIds={[LOG_LOTTERY_BET_VIEW]}
        failedRender={
          <Result
            status="warning"
            title={<FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />}
            subTitle={
              <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
            }
          />
        }
      >
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          summaryComponent={
            <SummaryView
              summary={dataSource && dataSource.Summary}
              totalCount={dataSource && dataSource.TotalCount}
            />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢絝果"
                />
              }
              rowKey={record => record.BetOrderNo}
              rowSelection={rowSelection}
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource && dataSource.List}
              total={dataSource && dataSource.TotalCount}
              extendArea={
                <Space align="start">
                  <ExportReportButton
                    condition={condition}
                    actionUrl="/api/Bets/Export"
                  />
                  <Permission functionIds={[LOG_LOTTERY_BET_CANCEL_ORDER]}>
                    <Button
                      type="primary"
                      danger
                      icon={<ScissorOutlined />}
                      disabled={selectedRowKeys.length === 0}
                      onClick={onCancelOrders}
                    >
                      <FormattedMessage id="PageProductsBet.CancelOrderModal.Cancel" />
                    </Button>
                  </Permission>
                </Space>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
      </Permission>
    </OrderDetailContextProvider>
  );
};

export default PageView;
