import React, { useEffect } from 'react'
import LayoutPageBody from 'layouts/Page/Body'
import { useParams, useHistory } from 'react-router-dom'
import { message, Card, Button, Switch, Form, Input, Space } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import caller from 'utils/fetcher'
import useSWR from 'swr'
import axios from 'axios'
const { useForm } = Form

export default function CreateMember() {
    const { providerId: ProviderId } = useParams()
    const history = useHistory()
    const editMode = !!ProviderId
    const [form] = useForm()
    const intl = useIntl()
    const MODE = {
        LINK: 1,
    }

    const { data: detailData } = useSWR(
        editMode ? `/api/SiteManagement/CustomerService/${ProviderId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    const LAYOUT_CONFIG = {
        labelCol: {
            span: 6,
        },
        wrapperCol: {
            span: 12,
        },
    }

    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    }

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })

    useEffect(() => {
        form.setFieldsValue({
            Url: detailData && detailData.Config,
            ...detailData,
        })
    }, [detailData])

    function onFinish(values) {
        const payload = {
            ...values,
            Mode: MODE.LINK,
        }
        if (editMode) {
            payload.ProviderId = ProviderId
            payload.Mode = form.getFieldValue('Mode')
        }

        caller({
            endpoint: '/api/SiteManagement/CustomerService',
            method: editMode ? 'PATCH' : 'POST',
            body: payload,
        }).then(data => {
            message.success(successMsg, 5)
            history.push('/all-site/site-setting')
        })
    }

    return (
        <LayoutPageBody
            title={
                <Space size={0}>
                    {editMode
                        ? intl.formatMessage({
                              id: 'PageAllSite.CustomerService.DetailPage.Edit',
                          })
                        : intl.formatMessage({
                              id:
                                  'PageAllSite.CustomerService.DetailPage.Create',
                          })}
                </Space>
            }
        >
            <Card bordered={false} style={{ margin: 24 }}>
                <Form
                    form={form}
                    onFinish={onFinish}
                    {...LAYOUT_CONFIG}
                    style={{ margin: '24 auto' }}
                    initialValues={{
                        ProviderId,
                        Enabled: true,
                    }}
                >
                    <Form.Item
                        name="Name"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                        label={
                            <FormattedMessage id="PageAllSite.CustomerService.FormItems.Name" />
                        }
                    >
                        <Input type="text" maxLength={30} />
                    </Form.Item>
                    {editMode && detailData && detailData.Mode !== 1 ? null : (
                        <Form.Item
                            name="Url"
                            rules={[
                                {
                                    required: true,
                                },
                            ]}
                            label={
                                <FormattedMessage id="PageAllSite.CustomerService.FormItems.Url" />
                            }
                        >
                            <Input type="text" />
                        </Form.Item>
                    )}
                    <Form.Item
                        name="Enabled"
                        label={
                            <FormattedMessage id="PageAllSite.CustomerService.FormItems.Status" />
                        }
                        valuePropName="checked"
                    >
                        <Switch
                            checkedChildren={intl.formatMessage({
                                id:
                                    'PageAllSite.CustomerService.FormItems.Enabled',
                            })}
                            unCheckedChildren={intl.formatMessage({
                                id:
                                    'PageAllSite.CustomerService.FormItems.UnEnabled',
                            })}
                            defaultChecked
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Space>
                            <Button type="primary" htmlType="submit">
                                <FormattedMessage id="Share.ActionButton.Submit" />
                            </Button>
                            <Button
                                htmlType="button"
                                onClick={() => form.resetFields()}
                            >
                                <FormattedMessage id="Share.ActionButton.Restart" />
                            </Button>
                            <Button
                                type="link"
                                htmlType="button"
                                onClick={() => history.goBack()}
                            >
                                <FormattedMessage id="Share.ActionButton.Cancel" />
                            </Button>
                        </Space>
                    </Form.Item>
                </Form>
            </Card>
        </LayoutPageBody>
    )
}
