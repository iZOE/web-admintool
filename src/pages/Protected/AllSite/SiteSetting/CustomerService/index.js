import React, { Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'

const CreateAsync = React.lazy(() => import('./Detail'))
const DetailAsync = React.lazy(() => import('./Detail'))

export default function Members() {
    return (
        <Suspense>
            <Switch>
                <Route exact path="/all-site/site-setting">
                    <Home />
                </Route>
                <Route path="/all-site/site-setting/create">
                    <CreateAsync />
                </Route>
                <Route path="/all-site/site-setting/detail/:providerId">
                    <DetailAsync />
                </Route>
            </Switch>
        </Suspense>
    )
}
