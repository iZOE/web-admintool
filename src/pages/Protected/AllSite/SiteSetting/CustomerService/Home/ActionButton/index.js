import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import * as PERMISSION from 'constants/permissions'
import usePermission from 'hooks/usePermission'

const { ALL_SITE_SITE_SETTING_EDIT } = PERMISSION

export default function ActionButton({ record }) {
    const [editAllSiteSettingPermission] = usePermission(
        ALL_SITE_SITE_SETTING_EDIT
    )
    return (
        <>
            {editAllSiteSettingPermission && (
                <Link to={`/all-site/site-setting/detail/${record.ProviderId}`}>
                    <FormattedMessage id="Share.ActionButton.Edit" />
                </Link>
            )}
        </>
    )
}
