import React, { useContext, useEffect } from 'react';
import { ConditionContext } from 'contexts/ConditionContext';
import { Form, Row, Col, Input } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';
import QueryButton from 'components/FormActionButtons/Query';
import FormItemCheckboxWithLabel from 'components/FormItems/CheckboxWithLabel';
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect';

const { useForm } = Form;

function Condition({ onUpdate, onReady }) {
  const intl = useIntl();
  const { isReady } = useContext(ConditionContext);
  const [form] = useForm();

  useEffect(() => {
    onUpdate(form.getFieldsValue());
    onReady(isReady);
  }, [isReady]);

  return (
    <Form form={form} onFinish={onUpdate.bySearch}>
      <Row gutter={[16, 8]}>
        <Col sm={8}>
          <FormItemMultipleSelect
            checkAll
            name="Mode"
            label={
              <FormattedMessage id="Share.Fields.Type" description="類型" />
            }
            url="/api/Option/CustomerServiceMode"
          />
        </Col>
        <Col sm={8}>
          <Form.Item
            name="Name"
            label={
              <FormattedMessage
                id="PageAllSite.CustomerService.FormItems.Name"
                description="項目名稱"
              />
            }
          >
            <Input
              placeholder={intl.formatMessage({
                id: 'PageAllSite.CustomerService.Placeholder.PlsEnterTitle'
              })}
            />
          </Form.Item>
        </Col>
        <Col sm={4}>
          <FormItemCheckboxWithLabel
            name="IsEnabled"
            formattedMessageId="Share.QueryCondition.IsOnlyEnabled"
          />
        </Col>
        <Col sm={4} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
