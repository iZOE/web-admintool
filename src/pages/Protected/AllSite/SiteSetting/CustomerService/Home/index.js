import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import * as PERMISSION from 'constants/permissions';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const { ALL_SITE_SITE_SETTING_VIEW, ALL_SITE_SITE_SETTING_EDIT } = PERMISSION;
const PageView = () => {
  const { fetching, dataSource, condition, onUpdateCondition, onReady } = useGetDataSourceWithSWR({
    url: '/api/SiteManagement/CustomerService/Search',
    defaultSortKey: 'ProviderId',
    defaultSortDirection: 'Asc',
    autoFetch: true,
  });

  return (
    <Permission functionIds={[ALL_SITE_SITE_SETTING_VIEW]} isPage>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={<Condition onReady={onReady} onUpdate={onUpdateCondition} />}
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            title={<FormattedMessage id="Share.Table.SearchResult" />}
            config={COLUMNS_CONFIG}
            loading={fetching}
            onUpdate={onUpdateCondition}
            dataSource={dataSource && dataSource.Data}
            total={dataSource && dataSource.TotalCount}
            pagination={false}
            extendArea={
              <Permission functionIds={[ALL_SITE_SITE_SETTING_EDIT]}>
                <Link to={`/all-site/site-setting/create`}>
                  <Button type="primary">
                    <FormattedMessage id="Share.ActionButton.Create" />
                  </Button>
                </Link>
              </Permission>
            }
          />
        }
      />
    </Permission>
  );
};

export default PageView;
