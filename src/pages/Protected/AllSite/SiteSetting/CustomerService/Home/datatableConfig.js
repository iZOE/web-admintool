import React from 'react';
import { FormattedMessage } from 'react-intl';
import BreakSpaces from 'components/BreakSpaces';
import ActionButton from './ActionButton';
import { Tag } from 'antd';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageAllSite.CustomerService.DataTable.Title.ProviderId" description="序號" />,
    dataIndex: 'ProviderId',
    key: 'ProviderId',

    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageAllSite.CustomerService.DataTable.Title.Mode" description="類型" />,
    dataIndex: 'Mode',
    key: 'Mode',

    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, { ModeText }, index) => ModeText,
  },

  {
    title: <FormattedMessage id="PageAllSite.CustomerService.DataTable.Title.Name" description="項目名稱" />,
    dataIndex: 'Name',
    key: 'Name',

    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },

  {
    title: <FormattedMessage id="PageAllSite.CustomerService.DataTable.Title.Config" description="設置" />,
    dataIndex: 'Config',
    key: 'Config',
    render: (_1, { Config }, _2) => <BreakSpaces>{Config}</BreakSpaces>,
  },

  {
    title: <FormattedMessage id="PageAllSite.CustomerService.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Enabled',
    key: 'Enabled',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: Enabled => (
      <Tag color={Enabled ? 'processing' : 'warning'}>
        <FormattedMessage
          id={`Share.Status.${Enabled ? 'Enable' : 'Disable'}`}
          defaultMessage={Enabled ? '啟用' : '停用'}
        />
      </Tag>
    ),
  },
  {
    title: <FormattedMessage id="PageAllSite.CustomerService.DataTable.Title.ModifiedUser" description="管理者" />,
    dataIndex: 'ModifiedUser',
    key: 'ModifiedUser',

    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'edit',
    key: 'edit',
    fixed: 'right',
    render: (text, record, index) => <ActionButton record={record} />,
  },
];
