import React, { createContext, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'
import { Button } from 'antd'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import * as PERMISSION from 'constants/permissions'
import ReportScaffold from 'components/ReportScaffold'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import DetailDrawer from './DetailDrawer'

export const PageContext = createContext();

const { ALL_SITE_SMS_VERIFICATION_VIEW } = PERMISSION
const PageView = () => {
    const {
        fetching,
        dataSource,
        condition,
        boundedMutate,
        onUpdateCondition,
        onReady,
    } = useGetDataSourceWithSWR({
        url: '/api/SiteManagement/Contact/Search',
        defaultSortKey: 'ContactId',
        defaultSortDirection: 'Asc',
        autoFetch: true,
    })

    const [modalState, setModalState] = useState({
        mode: null,
        visible: false
    })

    function onChangeModalState({mode, visible, data}) {
        setModalState({
            mode,
            visible,
            data
        })
    }

    return (
        <Permission functionIds={[ALL_SITE_SMS_VERIFICATION_VIEW]} isPage>
            <PageContext.Provider value={{modalState, mutate: boundedMutate, onChangeModalState}}>
                <DetailDrawer />
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition onReady={onReady} onUpdate={onUpdateCondition} />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage id="Share.Table.SearchResult" />
                            }
                            config={COLUMNS_CONFIG}
                            loading={fetching}
                            onUpdate={onUpdateCondition}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            pagination={false}
                            extendArea={
                                <Button type="primary" onClick={() => onChangeModalState({visible: true, mode: 'create'})}>
                                    <FormattedMessage id="Share.ActionButton.Create" />
                                </Button>
                            }
                        />
                    }
                />
            </PageContext.Provider>
        </Permission>
    )
}

export default PageView
