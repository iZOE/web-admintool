import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Tag } from 'antd';
import DateWithFormat from 'components/DateWithFormat';
import OpenEditButton from './OpenEditButton';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.ContactId" description="序號" />,
    dataIndex: 'ContactId',
    key: 'ContactId',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.Mode" description="類型" />,
    dataIndex: 'ModeText',
    key: 'ModeText',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.Name" description="顯示名稱" />,
    dataIndex: 'Name',
    key: 'Name',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.Content" description="鏈接" />,
    dataIndex: 'Content',
    key: 'Content',
    width: 300,
    render: (_1, { Content }, _2) => <span style={{ wordBreak: 'break-all' }}>{Content}</span>,
  },
  {
    title: <FormattedMessage id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Enabled',
    key: 'Enabled',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: Enabled => (
      <Tag color={Enabled ? 'processing' : 'warning'}>
        <FormattedMessage
          id={`Share.Status.${Enabled ? 'Enable' : 'Disable'}`}
          defaultMessage={Enabled ? '啟用' : '停用'}
        />
      </Tag>
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifiedTime',
    key: 'ModifiedTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    width: 200,
    render: (_1, { ModifiedTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.Modifier" description="異動人員" />,
    dataIndex: 'ModifiedUser',
    key: 'ModifiedUser',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'edit',
    key: 'edit',
    fixed: 'right',
    render: (_1, record, _2) => <OpenEditButton record={record} />,
  },
];
