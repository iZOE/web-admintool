import React, { useContext} from 'react'
import { FormattedMessage } from 'react-intl'
import { Button } from 'antd'
import { PageContext } from '../index'

type Props = {
    record: any
}

const OpenEditButton: React.FC<Props> = ({record}: Props) => {
    const { onChangeModalState } = useContext(PageContext)
    
    return (
        <Button type="link" onClick={() => {
            onChangeModalState({visible: true, data: record, mode: 'edit'})
        }}><FormattedMessage id="Share.ActionButton.Edit" /></Button>
    )
}

export default OpenEditButton