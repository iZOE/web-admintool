import React, { useMemo, useContext, forwardRef } from 'react'
import { Form, Input, Switch } from 'antd'
import { FormattedMessage } from 'react-intl'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import { PageContext } from '../../index'

export const LAYOUT_CONFIG = {
    labelCol: {
        span: 4,
    },
    wrapperCol: {
        span: 12,
    },
}

function CommonForm(props, ref) {
    const {
        modalState: { mode, data },
    } = useContext(PageContext)
    const [form] = Form.useForm()
    const defaultValue = useMemo(() => {
        if (mode === 'create') {
            return {
                Enabled: false,
            }
        } else if (mode === 'edit') {
            return {
                ...data,
            }
        } else {
            return null
        }
    }, [mode])

    async function getResult() {
        const res = await form.validateFields()

        if (res) {
            return res
        }
        return null
    }

    ref.current = {
        getResult,
    }

    return (
        <Form {...LAYOUT_CONFIG} form={form} initialValues={defaultValue}>
            <FormItemsSimpleSelect
                url="/api/Option/ContactMode"
                name="Mode"
                label={
                    <FormattedMessage
                        id="Share.Fields.Type"
                        description="类型"
                    />
                }
            />
            <Form.Item name="Enabled" hidden />
            <Form.Item
                name="Name"
                label={
                    <FormattedMessage
                        id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.Name"
                        description="显示名称"
                    />
                }
            >
                <Input maxLength={20} />
            </Form.Item>
            <Form.Item shouldUpdate noStyle>
                {({ getFieldValue }) => (
                    <Form.Item
                        name="Content"
                        label={
                            <FormattedMessage
                                id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.Content"
                                description="链接"
                            />
                        }
                    >
                        <Input disabled={getFieldValue('Mode') === 1} />
                    </Form.Item>
                )}
            </Form.Item>
            <Form.Item
                name="Enabled"
                valuePropName="checked"
                label={
                    <FormattedMessage
                        id="PageAllSite.ThirdPartyServiceLink.DataTable.Title.Status"
                        description="状态"
                    />
                }
            >
                <Switch
                    checkedChildren={
                        <FormattedMessage id="Share.SwitchButton.IsEnable.Yes" />
                    }
                    unCheckedChildren={
                        <FormattedMessage id="Share.SwitchButton.IsEnable.No" />
                    }
                />
            </Form.Item>
        </Form>
    )
}

export default forwardRef(CommonForm)
