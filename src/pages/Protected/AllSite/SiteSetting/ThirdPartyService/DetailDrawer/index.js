import React, { useState, useContext, useMemo, createRef } from 'react'
import { Drawer, Button, message } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import caller from 'utils/fetcher'
import CommonForm from './CommonForm'
import { PageContext } from '../index'

const DetailDrawer = () => {
    const intl = useIntl()
    const formEl = createRef()
    const [saving, setSaving] = useState(false)
    const {
        modalState: { visible, mode, data },
        mutate,
        onChangeModalState,
    } = useContext(PageContext)

    const onCloseDrawer = () => {
        onChangeModalState({
            visible: false,
            data: null,
            mode: null,
        })
    }

    async function onSave() {
        const values = await formEl.current.getResult()
        if (mode !== 'create') {
            caller({
                method: 'patch',
                endpoint: '/api/SiteManagement/Contact',
                body: { ...data, ...values },
            })
                .then(() => {
                    message.success(
                        intl.formatMessage({
                            id: 'Share.SuccessMessage.UpdateSuccess',
                        })
                    )
                    mutate()
                    onCloseDrawer()
                })
                .catch(err => {
                    message.error(err.Message, 5)
                })
                .finally(() => {
                    setSaving(false)
                })
        } else {
            caller({
                method: 'post',
                endpoint: '/api/SiteManagement/Contact',
                body: values,
            })
                .then(() => {
                    message.success(
                        intl.formatMessage({
                            id: 'Share.SuccessMessage.UpdateSuccess',
                        })
                    )
                    mutate()
                    onCloseDrawer()
                })
                .finally(() => {
                    setSaving(false)
                })
        }
    }

    return (
        <>
            <Drawer
                title={
                    mode !== 'create' ? (
                        <FormattedMessage id="Share.CommonKeys.Edit" />
                    ) : (
                        <FormattedMessage id="Share.CommonKeys.Create" />
                    )
                }
                placement="right"
                closable={false}
                onClose={onCloseDrawer}
                visible={visible}
                width={600}
                footer={
                    <div
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button
                            onClick={onCloseDrawer}
                            style={{ marginRight: 8 }}
                        >
                            <FormattedMessage id="Share.ActionButton.Cancel" />
                        </Button>
                        <Button
                            loading={saving}
                            type="primary"
                            onClick={onSave}
                        >
                            <FormattedMessage id="Share.ActionButton.Submit" />
                        </Button>
                    </div>
                }
            >
                {visible ? <CommonForm ref={formEl} /> : null}
            </Drawer>
        </>
    )
}

export default DetailDrawer
