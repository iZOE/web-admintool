import React, { useState, lazy } from 'react'
import { FormattedMessage } from 'react-intl'
import { Tabs } from 'antd'
import Permission from 'components/Permission'
import PageHeaderTab from 'components/PageHeaderTab'
import * as PERMISSION from 'constants/permissions'

const { ALL_SITE_SITE_SETTING_VIEW } = PERMISSION
const TABS = ['CustomerServiceLink', 'ThirdPartyServiceLink']

const CustomerServiceAsync = lazy(() => import('./CustomerService'))
const ThirdPartyServiceAsync = lazy(() => import('./ThirdPartyService'))

const COMPONENT_MAP = {
    CustomerServiceLink: CustomerServiceAsync,
    ThirdPartyServiceLink: ThirdPartyServiceAsync
}

export default function SiteSetting() {
    const [tabKey, setTabKey] = useState(TABS[0])
    const ViewComponent = COMPONENT_MAP[tabKey]
    return (
        <>
            <PageHeaderTab
                onChange={key => {
                    setTabKey(key)
                }}
            >
                {TABS.map(tab => (
                    <Tabs.TabPane
                        tab={
                            <FormattedMessage id={`PageAllSite.Tabs.${tab}`} />
                        }
                        key={tab}
                    />
                ))}
            </PageHeaderTab>
            <Permission isPage functionIds={[ALL_SITE_SITE_SETTING_VIEW]}>
                <ViewComponent />
            </Permission>
        </>
    )
}
