import React from 'react';
import { FormattedMessage } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import * as PERMISSION from 'constants/permissions';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const { ALL_SITE_SMS_VERIFICATION_VIEW } = PERMISSION;
const PageView = () => {
  const { fetching, dataSource, condition, onUpdateCondition } = useGetDataSourceWithSWR({
    method: 'get',
    url: '/api/Member/Security/smsLog',
    defaultSortKey: 'SendDate',
  });

  return (
    <Permission functionIds={[ALL_SITE_SMS_VERIFICATION_VIEW]} isPage>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={<Condition onUpdate={onUpdateCondition.bySearch} />}
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            title={<FormattedMessage id="Share.Table.SearchResult" />}
            config={COLUMNS_CONFIG}
            loading={fetching}
            dataSource={dataSource && [dataSource]}
            pagination={false}
          />
        }
      />
    </Permission>
  );
};

export default PageView;
