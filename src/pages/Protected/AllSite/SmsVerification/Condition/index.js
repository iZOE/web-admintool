import React from "react";
import { Form, Row, Col, Input } from "antd";
import { FormattedMessage } from "react-intl";
import QueryButton from "components/FormActionButtons/Query";

const { useForm } = Form;

const CONDITION_INITIAL_VALUE = {
  MemberName: ""
};

function Condition({ onUpdate }) {
  const [form] = useForm();

  return (
    <Form
      form={form}
      onFinish={onUpdate}
      initialValues={CONDITION_INITIAL_VALUE}
    >
      <Row gutter={[16, 8]}>
        <Col sm={8}>
          <Form.Item
            label={
              <FormattedMessage id="PageMemberSmsVerification.DataTable.Title.MemberName" />
            }
            name="MemberName"
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={{ span: 4, offset: 12 }} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
