import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageMemberSmsVerification.DataTable.Title.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageMemberSmsVerification.DataTable.Title.CellPhoneNo" description="驗證手機號" />,
    dataIndex: 'CellPhoneNo',
    key: 'CellPhoneNo',
    isShow: true,
  },

  {
    title: <FormattedMessage id="PageMemberSmsVerification.DataTable.Title.ValidationCode" description="驗證碼" />,
    dataIndex: 'ValidationCode',
    key: 'ValidationCode',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageMemberSmsVerification.DataTable.Title.SendDate" description="發送時間" />,
    dataIndex: 'SendDate',
    key: 'SendDate',
    render: (_1, { SendDate: time }, _2) => <DateWithFormat time={time} />,
  },
];
