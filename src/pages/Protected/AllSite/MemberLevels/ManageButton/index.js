import React from "react";
import { Popconfirm, Space, Button } from "antd";
import { FormattedMessage } from "react-intl";

const ManageButton = ({ doCancel, doEdit, editing, onSave }) => {
  return editing ? (
    <Space size="small">
      <Popconfirm
        title={
          <FormattedMessage
            id="Share.ActionButton.SureToSave"
            defaultMessage="确定储存？"
          />
        }
        onConfirm={() => onSave()}
      >
        <Button type="link">
          <FormattedMessage
            id="Share.ActionButton.Save"
            defaultMessage="儲存"
          />
        </Button>
      </Popconfirm>
      <Button type="link" onClick={() => doCancel()}>
        <FormattedMessage
          id="Share.ActionButton.Cancel"
          defaultMessage="取消"
        />
      </Button>
    </Space>
  ) : (
    <Button type="link" onClick={() => doEdit()}>
      <FormattedMessage id="Share.ActionButton.Manage" defaultMessage="管理" />
    </Button>
  );
};

export default ManageButton;
