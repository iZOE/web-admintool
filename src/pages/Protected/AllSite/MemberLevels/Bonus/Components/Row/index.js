import React, {
  useState,
  useMemo,
  Children,
  isValidElement,
  cloneElement
} from "react";
import { FormattedMessage, FormattedNumber } from "react-intl";
import { Form, Input, Select, InputNumber } from "antd";
import update from "immutability-helper";
import ManageButton from "../../../ManageButton";
import BonusService from "../../services";

const { Option } = Select;
const FREQUENCY_IDS = [0, 1, 2];

const CanEditKeys = new Set([
  "ActiveBonus",
  "ActiveBonusFrequency",
  "BirthdayBonus",
  "MonthlyBonus",
  "PromoBonus",
  "TurnoverTimes"
]);
const { updateMemberBonusItem } = BonusService();

const Row = ({ children, index, record, dataSource, ...restProps }) => {
  const [form] = Form.useForm();
  const [editing, setEditing] = useState(false);
  const [cell, setCell] = useState(record);

  const doEdit = () => {
    setEditing(true);
    form.setFieldsValue(cell);
  };

  const doCancel = () => {
    setEditing(false);
  };

  const onSave = async () => {
    try {
      const { MemberLevelId } = cell;
      const formData = await form.validateFields();
      if (formData) {
        updateMemberBonusItem({ MemberLevelId, ...formData }).then(result =>
          saveSuccess(formData)
        );
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };
  const saveSuccess = formData => {
    setCell(update(cell, { $merge: formData }));
    setEditing(false);
  };

  const manageButtonProps = {
    editing,
    doEdit,
    doCancel,
    onSave
  };

  const childrenWithProps = useMemo(() => {
    return Children.map(children, child => {
      if (isValidElement(child)) {
        switch (child.key) {
          case "TurnoverTimes":
            return (
              CanEditKeys.has(child.key) &&
              cloneElement(child, {
                render: () =>
                  editing ? (
                    <Form.Item
                      noStyle
                      name={child.props.dataIndex}
                      rules={[{ required: true }]}
                    >
                      <Input />
                    </Form.Item>
                  ) : (
                    cell[child.key]
                  )
              })
            );
          case "ActiveBonusFrequency":
            return (
              CanEditKeys.has(child.key) &&
              cloneElement(child, {
                render: () =>
                  editing ? (
                    <Form.Item
                      noStyle
                      name={child.props.dataIndex}
                      rules={[{ required: false }]}
                    >
                      <Select defaultValue={1} className="option">
                        {FREQUENCY_IDS.map(id => (
                          <Option value={id} key={id}>
                            <FormattedMessage
                              id={`PageMemberLevels.Bonus.FormItem.Select.${id}`}
                            />
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                  ) : (
                    <FormattedMessage
                      id={`PageMemberLevels.Bonus.FormItem.Select.${
                        cell[child.key]
                      }`}
                      defaultMessage="-"
                    />
                  )
              })
            );
          case "Manage":
            return cloneElement(child, {
              render: () => <ManageButton {...manageButtonProps} />
            });
          default:
            return CanEditKeys.has(child.key)
              ? cloneElement(child, {
                  render: () => {
                    const prevValue = dataSource[index - 1] || null;
                    const nextValue =
                      dataSource.length !== index + 1
                        ? dataSource[index + 1]
                        : Number.MAX_SAFE_INTEGER;
                    return editing ? (
                      <Form.Item
                        noStyle
                        name={child.props.dataIndex}
                        rules={[
                          {
                            required: true
                          }
                        ]}
                      >
                        <InputNumber
                          min={prevValue ? prevValue[child.key] : 0}
                          max={nextValue[child.key]}
                        />
                      </Form.Item>
                    ) : (
                      <FormattedNumber
                        value={cell[child.key]}
                        minimumFractionDigits="2"
                      />
                    );
                  }
                })
              : child;
        }
      }
    });
  }, [children, editing, cell, manageButtonProps, index, dataSource]);

  return (
    <tr {...restProps}>
      <Form form={form} component={false}>
        {childrenWithProps}
      </Form>
    </tr>
  );
};

export default Row;
