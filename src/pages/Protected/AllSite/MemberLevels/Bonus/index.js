import React, { useMemo } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import axios from 'axios';
import useSWR from 'swr';
import usePermission from 'hooks/usePermission';
import DateWithFormat from 'components/DateWithFormat';
import Permission from 'components/Permission';
import Row from './Components/Row';
import * as PERMISSION from 'constants/permissions';
import { Skeleton, Form, Table } from 'antd';

const { ALL_SITE_MEMBER_LEVELS_VIEW, ALL_SITE_MEMBER_LEVELS_EDIT } = PERMISSION;
const ExcludeKeys = new Set(['ModifyUserId']);

const Bonus = () => {
  const [allSiteMemberLevelsEdit] = usePermission(ALL_SITE_MEMBER_LEVELS_EDIT);
  const [form] = Form.useForm();
  const { data: dataSource, isValidating: fetching } = useSWR('/api/Member/LevelBonus', url =>
    axios(url).then(res => res.data.Data)
  );

  const columnConfig = useMemo(() => {
    if (dataSource) {
      const columns = Object.keys(dataSource[0]).reduce((obj, key, index) => {
        if (ExcludeKeys.has(key)) {
          return obj;
        }
        return [
          ...obj,
          {
            title: <FormattedMessage id={`PageMemberLevels.Bonus.Table.Columns.${key}`} defaultMessage={key} />,
            dataIndex: key,
            isShow: true,
            render: value => {
              if (key === 'ModifyTime') {
                return <DateWithFormat time={value} />;
              }
              if (isNaN(value)) {
                if (RegExp('Bonus', 'g').test(key)) {
                  return <FormattedNumber value={value} minimumFractionDigits="2" />;
                } else {
                  return value;
                }
              }
              return value;
            },
          },
        ];
      }, []);
      if (allSiteMemberLevelsEdit) {
        columns.push({
          dataIndex: 'Manage',
          key: 'Manage',
          isManage: true,
          isShow: true,
          fixed: 'right',
        });
      }
      return columns;
    }
    return [];
  }, [dataSource]);

  if (!dataSource) {
    return <Skeleton active />;
  }

  return (
    <Permission isPage functionIds={[ALL_SITE_MEMBER_LEVELS_VIEW]}>
      <Form form={form} component={false} id={123}>
        <Table
          size="small"
          components={{
            body: {
              row: Row,
            },
          }}
          loading={fetching}
          columns={columnConfig}
          dataSource={dataSource}
          rowKey={record => record.MemberLevelId}
          onRow={(record, index) => ({ record, index, dataSource })}
          pagination={false}
        ></Table>
      </Form>
    </Permission>
  );
};

export default Bonus;
