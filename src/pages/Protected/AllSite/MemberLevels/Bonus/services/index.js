import makeService from "utils/makeService";

const BonusService = () => {
  const updateMemberBonusItem = makeService({
    method: "post",
    url: "/api/Member/LevelBonus"
  });

  return {
    updateMemberBonusItem
  };
};

export default BonusService;
