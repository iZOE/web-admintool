import React, { useState, lazy, Suspense } from 'react'
import { FormattedMessage } from 'react-intl'
import { Tabs, Spin } from 'antd'
import PageHeaderTab from 'components/PageHeaderTab'

const { TabPane } = Tabs

const LevelsAsync = lazy(() => import('./Levels'))
const BonusAsync = lazy(() => import('./Bonus'))
const ReturnAsync = lazy(() => import('./Return'))

export default function MemberLevels() {
    const [tabKey, setTabKey] = useState('levels')
    return (
        <Suspense fallback={<Spin />}>
            <PageHeaderTab
                defaultActiveKey="levels"
                onChange={key => {
                    setTabKey(key)
                }}
            >
                <TabPane
                    tab={<FormattedMessage id="PageMemberLevels.Tabs.Levels" />}
                    key="levels"
                />
                <TabPane
                    tab={<FormattedMessage id="PageMemberLevels.Tabs.Bonus" />}
                    key="bonus"
                />
                <TabPane
                    tab={<FormattedMessage id="PageMemberLevels.Tabs.Return" />}
                    key="return"
                />
            </PageHeaderTab>
            {tabKey === 'levels' && <LevelsAsync />}
            {tabKey === 'bonus' && <BonusAsync />}
            {tabKey === 'return' && <ReturnAsync />}
        </Suspense>
    )
}
