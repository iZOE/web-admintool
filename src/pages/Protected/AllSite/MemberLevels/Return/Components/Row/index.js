import React, {
  useState,
  useMemo,
  Children,
  isValidElement,
  cloneElement
} from "react";
import { trigger } from "swr";
import { FormattedMessage, FormattedNumber, useIntl } from "react-intl";
import { message, Form, InputNumber } from "antd";
import update from "immutability-helper";
import ManageButton from "../../../ManageButton";
import ReturnService from "../../services";

const { updateMemberReturnItem } = ReturnService();

const Row = ({
  children,
  index,
  record,
  cantEditKey,
  providerTypeId,
  ...restProps
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [editing, setEditing] = useState(false);
  const [cell, setCell] = useState(record);

  const doEdit = () => {
    setEditing(true);
    form.setFieldsValue(cell);
  };

  const doCancel = () => {
    setEditing(false);
  };

  const onSave = async () => {
    try {
      const { MemberLevelId } = cell;
      const formData = await form.validateFields();
      if (formData) {
        const body = {
          Data: Object.keys(formData).map(key => ({
            GameProvider: providerTypeId[key],
            ReturnPoint: formData[key]
          })),
          MemberLevelId
        };
        updateMemberReturnItem(body).then(result => saveSuccess(formData));
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };
  const saveSuccess = formData => {
    setCell(update(cell, { $merge: formData }));
    setEditing(false);
    trigger("/api/Member/LevelReturn");
    message.success(
      intl.formatMessage({ id: "Share.SuccessMessage.UpdateSuccess" })
    );
  };

  const manageButtonProps = {
    editing,
    doEdit,
    doCancel,
    onSave
  };

  const childrenWithProps = useMemo(() => {
    return Children.map(children, child => {
      if (isValidElement(child)) {
        switch (child.key) {
          case "Manage":
            return cloneElement(child, {
              render: () => <ManageButton {...manageButtonProps} />
            });
          default:
            return !cantEditKey.has(child.key)
              ? cloneElement(child, {
                  render: () => {
                    return editing ? (
                      <Form.Item
                        noStyle
                        initialvalues={cell[child.key]}
                        name={child.props.dataIndex}
                        validateStatus={
                          cell[child.key] >= 0 && cell[child.key] < 1.2
                            ? "success"
                            : "error"
                        }
                        rules={[
                          {
                            required: true,
                            message: (
                              <FormattedMessage
                                id="Share.FormValidate.Required.Input"
                                defaultMessage="此為必填欄位"
                              />
                            )
                          }
                        ]}
                      >
                        <InputNumber
                          initialValues={cell[child.key]}
                          defaultValue={cell[child.key]}
                          min={0}
                          max={1.2}
                        />
                      </Form.Item>
                    ) : (
                      <FormattedNumber
                        value={cell[child.key]}
                        minimumFractionDigits="2"
                      />
                    );
                  }
                })
              : child;
        }
      }
    });
  }, [children, editing, cell, manageButtonProps]);

  return (
    <tr {...restProps}>
      <Form form={form} component={false}>
        {childrenWithProps}
      </Form>
    </tr>
  );
};

export default Row;
