import React, { useMemo, useState } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import axios from 'axios';
import useSWR from 'swr';
import { Skeleton, Form, Table } from 'antd';
import usePermission from 'hooks/usePermission';
import DateWithFormat from 'components/DateWithFormat';
import Permission from 'components/Permission';
import Row from './Components/Row';
import * as PERMISSION from 'constants/permissions';

const { ALL_SITE_MEMBER_LEVELS_VIEW, ALL_SITE_MEMBER_LEVELS_EDIT } = PERMISSION;
const ExcludeKeys = new Set(['ModifyUserId', 'CreateTime']);
const CantEditKey = new Set(['MemberLevelId', 'MemberLevelName', 'ModifyTime', 'ModifyUserId', 'ModifyUserName']);

let providerTypeIds = {};

const Return = () => {
  const [providerTypeId, setProviderTypeId] = useState({});
  const [allSiteMemberLevelsEdit] = usePermission(ALL_SITE_MEMBER_LEVELS_EDIT);
  const [form] = Form.useForm();
  const { data: dataSource, isValidating: fetching } = useSWR('/api/Member/LevelReturn', url =>
    axios(url).then(res => res.data.Data)
  );

  const getColumnsWidth = columnKey => {
    switch (columnKey) {
      case 'ModifyTime':
        return 200;
      case 'MemberLevelId':
        return 50;
      case 'MemberLevelName':
        return 100;
      default:
        return 130;
    }
  };

  const parsingKeyToObj = key => {
    const arr = key.split(':');
    const typeId = arr.splice(1).map(i => parseInt(i, 10));
    providerTypeIds = {
      ...providerTypeIds,
      [key]: typeId,
    };
    return `${arr[0]}(%)`;
  };

  const columnConfig = useMemo(() => {
    if (dataSource) {
      const columns = Object.keys(dataSource[0]).reduce((obj, key) => {
        if (ExcludeKeys.has(key)) {
          return obj;
        }
        return [
          ...obj,
          {
            title: CantEditKey.has(key) ? (
              <FormattedMessage id={`PageMemberLevels.Return.Table.Columns.${key}`} defaultMessage={key} />
            ) : (
              parsingKeyToObj(key)
            ),
            dataIndex: key,
            isShow: true,
            // width: getColumnsWidth(key),
            render: value => {
              if (key === 'ModifyTime' || key === 'CreateTime') {
                return <DateWithFormat time={value} />;
              } else if (!isNaN(value) && value) {
                if (RegExp('MemberLevelId', 'g').test(key)) {
                  return value;
                } else {
                  return <FormattedNumber value={value} minimumFractionDigits="2" />;
                }
              }
              return value;
            },
          },
        ];
      }, []);
      setProviderTypeId(providerTypeIds);
      if (allSiteMemberLevelsEdit) {
        columns.push({
          dataIndex: 'Manage',
          key: 'Manage',
          isManage: true,
          isShow: true,
          fixed: 'right',
        });
      }
      return columns;
    }
    return [];
  }, [allSiteMemberLevelsEdit, dataSource]);

  if (!dataSource) {
    return <Skeleton active />;
  }

  return (
    <Permission isPage functionIds={[ALL_SITE_MEMBER_LEVELS_VIEW]}>
      <Form form={form} component={false}>
        <Table
          size="small"
          components={{
            body: {
              row: Row,
            },
          }}
          loading={fetching}
          columns={columnConfig}
          dataSource={dataSource}
          rowKey={record => record.MemberLevelId}
          onRow={(record, index) => ({
            record,
            index,
            cantEditKey: CantEditKey,
            providerTypeId,
          })}
          pagination={false}
        />
      </Form>
    </Permission>
  );
};

export default Return;
