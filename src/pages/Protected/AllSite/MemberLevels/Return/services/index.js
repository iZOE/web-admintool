import makeService from "utils/makeService";

const ReturnService = () => {
  const updateMemberReturnItem = makeService({
    method: "PATCH",
    url: "/api/Member/LevelReturn"
  });

  const getGameProvider = makeService({
    method: "GET",
    url: "/api/GameProvider"
  });

  return {
    updateMemberReturnItem,
    getGameProvider
  };
};

export default ReturnService;
