import makeService from "utils/makeService";

const LevelService = () => {
  const updateMemberLevelItem = makeService({
    method: "patch",
    url: "/api/Member/Level"
  });

  return {
    updateMemberLevelItem
  };
};

export default LevelService;
