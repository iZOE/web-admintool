import React, { useMemo } from 'react';
import { FormattedMessage, FormattedNumber } from 'react-intl';
import axios from 'axios';
import useSWR from 'swr';
import usePermission from 'hooks/usePermission';
import DateWithFormat from 'components/DateWithFormat';
import Permission from 'components/Permission';
import Row from './Components/Row';
import * as PERMISSION from 'constants/permissions';
import movieArrayItem from 'utils/movieArrayItem';
import { Skeleton, Form, Table } from 'antd';

const { ALL_SITE_MEMBER_LEVELS_VIEW, ALL_SITE_MEMBER_LEVELS_EDIT } = PERMISSION;
const ExcludeKeys = new Set(['ModifyUserId']);

const Levels = () => {
  const [form] = Form.useForm();
  const [allSiteMemberLevelsEdit] = usePermission(ALL_SITE_MEMBER_LEVELS_EDIT);
  const { data: dataSource, isValidating: fetching } = useSWR('/api/Member/Level', url =>
    axios(url).then(res => res.data.Data)
  );

  const columnConfig = useMemo(() => {
    if (dataSource) {
      const columns = movieArrayItem(
        Object.keys(dataSource[0]),
        'FundTransferSMSLimitAmount',
        'FundTransferTimes'
      ).reduce((obj, key) => {
        if (ExcludeKeys.has(key)) {
          return obj;
        }
        return [
          ...obj,
          {
            title: <FormattedMessage id={`PageMemberLevels.Levels.Table.Columns.${key}`} defaultMessage={key} />,
            dataIndex: key,
            isShow: true,
            render: value => {
              if (key === 'ModifyTime') {
                return <DateWithFormat time={value} />;
              }
              if (!isNaN(value)) {
                if (RegExp('Amount', 'g').test(key)) {
                  return <FormattedNumber value={value} minimumFractionDigits="2" />;
                } else {
                  return value;
                }
              }
              return value;
            },
          },
        ];
      }, []);
      if (allSiteMemberLevelsEdit) {
        columns.push({
          dataIndex: 'Manage',
          key: 'Manage',
          isManage: true,
          isShow: true,
          width: 150,
          fixed: 'right',
        });
      }
      return columns;
    }
    return [];
  }, [dataSource]);

  if (!dataSource) {
    return <Skeleton active />;
  }

  return (
    <Permission isPage functionIds={[ALL_SITE_MEMBER_LEVELS_VIEW]}>
      <Form form={form} component={false}>
        <Table
          size="small"
          components={{
            body: {
              row: Row,
            },
          }}
          loading={fetching}
          columns={columnConfig}
          dataSource={dataSource}
          rowKey={record => record.MemberLevelId}
          onRow={record => ({ record })}
          pagination={false}
        ></Table>
      </Form>
    </Permission>
  );
};

export default Levels;
