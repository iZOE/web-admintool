import React, { useState, useMemo, Children, isValidElement, cloneElement } from 'react';
import { trigger } from 'swr';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import { Form, Input, InputNumber, message } from 'antd';
import update from 'immutability-helper';
import BreakSpaces from 'components/BreakSpaces';
import ManageButton from '../../../ManageButton';
import LevelService from '../../services';

const CanEditKeys = new Set([
  'WithdrawTimes',
  'WithdrawFee',
  'MinWithdrawAmount',
  'MaxWithdrawAmount',
  'FundTransferTimes',
  'FundTransferSMSLimitAmount',
  'NormalTurnoverRate',
  'KeepLevelValidBetAmount',
  'FundTransferMaxAmount',
  'Description',
]);

const { updateMemberLevelItem } = LevelService();

const Row = ({ children, index, record, ...restProps }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [editing, setEditing] = useState(false);
  const [cell, setCell] = useState(record);

  const doEdit = () => {
    setEditing(true);
    form.setFieldsValue(cell);
  };

  const doCancel = () => {
    setEditing(false);
  };

  const onSave = async () => {
    try {
      const { MemberLevelId } = cell;
      const formData = await form.validateFields();
      updateMemberLevelItem({ MemberLevelId, ...formData }).then(result => {
        saveSuccess(formData);
      });
    } catch (errInfo) {
      errInfo.errorFields.forEach(field => {
        message.error(
          intl.formatMessage({
            id: `PageMemberLevels.Levels.Table.Columns.${field.name[0]}`,
          }) + intl.formatMessage(field.errors[0].props)
        );
      });
    }
  };

  const saveSuccess = formData => {
    setCell(update(cell, { $merge: formData }));
    setEditing(false);
    trigger('/api/Member/Level');
    message.success(intl.formatMessage({ id: 'Share.SuccessMessage.UpdateSuccess' }));
  };

  const manageButtonProps = {
    editing,
    doEdit,
    doCancel,
    onSave,
  };

  const childrenWithProps = useMemo(() => {
    return Children.map(children, child => {
      if (isValidElement(child)) {
        switch (child.key) {
          case 'WithdrawTimes':
            return (
              CanEditKeys.has(child.key) &&
              cloneElement(child, {
                render: () =>
                  editing ? (
                    <Form.Item
                      noStyle
                      initialvalues={cell[child.key]}
                      name={child.props.dataIndex}
                      rules={[
                        {
                          required: true,
                          message: (
                            <FormattedMessage id="Share.FormValidate.Required.Input" defaultMessage="此為必填欄位" />
                          ),
                        },
                        {
                          validator: (_, value) =>
                            Number(value) >= 0
                              ? Promise.resolve(true)
                              : Promise.reject(
                                  <FormattedMessage
                                    id="Share.FormValidate.Value.Minimum"
                                    defaultMessage="已低于最小范围。"
                                  />
                                ),
                        },
                      ]}
                    >
                      <InputNumber />
                    </Form.Item>
                  ) : (
                    cell[child.key]
                  ),
              })
            );
          case 'FundTransferTimes':
            return (
              CanEditKeys.has(child.key) &&
              cloneElement(child, {
                render: () =>
                  editing ? (
                    <Form.Item
                      noStyle
                      initialvalues={cell[child.key]}
                      name={child.props.dataIndex}
                      rules={[
                        {
                          required: true,
                          message: (
                            <FormattedMessage id="Share.FormValidate.Required.Input" defaultMessage="此為必填欄位" />
                          ),
                        },
                        {
                          validator: (_, value) =>
                            Number(value) >= 0
                              ? Promise.resolve(true)
                              : Promise.reject(
                                  <FormattedMessage
                                    id="Share.FormValidate.Value.Minimum"
                                    defaultMessage="已低于最小范围。"
                                  />
                                ),
                        },
                      ]}
                    >
                      <InputNumber />
                    </Form.Item>
                  ) : (
                    cell[child.key]
                  ),
              })
            );
          case 'NormalTurnoverRate':
            return (
              CanEditKeys.has(child.key) &&
              cloneElement(child, {
                render: () =>
                  editing ? (
                    <Form.Item
                      noStyle
                      initialvalues={cell[child.key]}
                      name={child.props.dataIndex}
                      rules={[
                        {
                          required: true,
                          message: (
                            <FormattedMessage id="Share.FormValidate.Required.Input" defaultMessage="此為必填欄位" />
                          ),
                        },
                        {
                          validator: (_, value) =>
                            Number(value) >= 0
                              ? Promise.resolve(true)
                              : Promise.reject(
                                  <FormattedMessage
                                    id="Share.FormValidate.Value.Minimum"
                                    defaultMessage="已低于最小范围。"
                                  />
                                ),
                        },
                      ]}
                    >
                      <InputNumber />
                    </Form.Item>
                  ) : (
                    cell[child.key]
                  ),
              })
            );
          case 'Description':
            return (
              CanEditKeys.has(child.key) &&
              cloneElement(child, {
                render: () =>
                  editing ? (
                    <Form.Item
                      noStyle
                      initialvalues={cell[child.key]}
                      name={child.props.dataIndex}
                      rules={[{ required: false }]}
                    >
                      <Input style={{ minWidth: '180px' }} />
                    </Form.Item>
                  ) : (
                    <BreakSpaces>{cell[child.key]}</BreakSpaces>
                  ),
              })
            );
          case 'Manage':
            return cloneElement(child, {
              render: () => <ManageButton {...manageButtonProps} />,
            });
          default:
            return CanEditKeys.has(child.key)
              ? cloneElement(child, {
                  render: () =>
                    editing ? (
                      <Form.Item
                        noStyle
                        initialvalues={cell[child.key]}
                        name={child.props.dataIndex}
                        rules={[
                          {
                            required: true,
                            message: (
                              <FormattedMessage id="Share.FormValidate.Required.Input" defaultMessage="此為必填欄位" />
                            ),
                          },
                          {
                            validator: (_, value) =>
                              Number(value) >= 0
                                ? Promise.resolve(true)
                                : Promise.reject(
                                    <FormattedMessage
                                      id="Share.FormValidate.Value.Minimum"
                                      defaultMessage="已低于最小范围。"
                                    />
                                  ),
                          },
                        ]}
                      >
                        <InputNumber />
                      </Form.Item>
                    ) : (
                      <FormattedNumber value={cell[child.key]} minimumFractionDigits="2" />
                    ),
                })
              : child;
        }
      }
    });
  }, [children, manageButtonProps, editing, cell]);

  return (
    <tr {...restProps}>
      <Form form={form} component={false}>
        {childrenWithProps}
      </Form>
    </tr>
  );
};

export default Row;
