import React from 'react';
import { Statistic, Row, Col, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';

const SummaryView = ({ summary }) => {
  return summary ? (
    <Row gutter={24}>
      <Col span={4}>
        <Statistic
          title={<FormattedMessage id="PageThirdpartyBetting.Summary.TotalOrders" description="总订单数" />}
          value={summary.OrderTotal}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={<FormattedMessage id="PageThirdpartyBetting.Summary.NumberOfBets" description="投注人数" />}
          value={summary.NumberOfMembers}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={<FormattedMessage id="PageThirdpartyBetting.Summary.BetAmount" description="投注金额" />}
          value={summary.BetAmount}
          precision={2}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={<FormattedMessage id="PageThirdpartyBetting.Summary.EffectiveBet" description="有效投注额" />}
          value={summary.ActualBetAmount}
          precision={2}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={<FormattedMessage id="PageThirdpartyBetting.Summary.AmountOfWinnings" description="中奖金额" />}
          value={summary.BonusAmount}
          precision={2}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={<FormattedMessage id="PageThirdpartyBetting.Summary.PlatformProfitAndLoss" description="平台盈亏" />}
          value={summary.WinLoss}
          precision={2}
        />
      </Col>
    </Row>
  ) : (
    <Skeleton active />
  );
};

export default SummaryView;
