import React, { useContext, useEffect } from 'react';
import { Form, Row, Col, DatePicker, Input, Select } from 'antd';
import { FormattedMessage } from 'react-intl';
import { ConditionContext } from 'contexts/ConditionContext';
import QueryButton from 'components/FormActionButtons/Query';
import FormItemIncludeInnerMember from 'components/FormItems/IncludeInnerMember';
import FormItemAdvanceQuery from 'components/FormItems/AdvanceQuery';
import { FORMAT, DEFAULT } from 'constants/dateConfig';
import { RULES } from 'constants/activity/validators';
import { getISODateTimeString } from 'mixins/dateTime';

const { useForm } = Form;
const { Option } = Select;
const { RANGE } = DEFAULT;
const DATE_TYPE_IDS = [1, 2];
const ADVANCED_SEARCH_CONDITION_ITEMS = [1, 5, 6];

const CONDITION_INITIAL_VALUE = {
  AdvancedSearchConditionItem: 1,
  SearchDateType: DATE_TYPE_IDS[0],
  Date: RANGE.FROM_YESTERDAY_TO_TODAY,
  Source: null,
  Status: null,
  BetOrderNo: null,
  WinLoss: null,
  IncludeInnerMember: false,
};

function Condition({ onUpdate, onReady }) {
  const { isReady } = useContext(ConditionContext);
  const [form] = useForm();
  const { RangePicker } = DatePicker;

  useEffect(() => {
    onUpdate(resultCondition(form.getFieldsValue()));
    onReady(isReady);
  }, [isReady]);

  function resultCondition({ Date, ...restValues }) {
    const [StartTime, EndTime] = Date || [null, null];
    return {
      StartTime: getISODateTimeString(StartTime),
      EndTime: getISODateTimeString(EndTime),
      ...restValues,
    };
  }

  function onFinish(values) {
    onUpdate.bySearch(resultCondition(values));
  }

  return (
    <Form form={form} initialValues={CONDITION_INITIAL_VALUE} onFinish={onFinish}>
      <Row gutter={[16, 8]}>
        <Col sm={8}>
          <Form.Item label={<FormattedMessage id="PageThirdpartyBetting.Conditions.DateTime" />}>
            <Input.Group compact>
              <Form.Item name="SearchDateType" noStyle>
                <Select className="option" style={{ width: '100px' }}>
                  {DATE_TYPE_IDS.map(id => (
                    <Option value={id} key={id}>
                      <FormattedMessage id={`PageThirdpartyBetting.Conditions.DateType.${id - 1}`} />
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item name="Date" rules={[...RULES.REQUIRED]} noStyle>
                <RangePicker style={{ width: 'calc(100% - 100px)' }} showTime format={FORMAT.DISPLAY.DEFAULT} />
              </Form.Item>
            </Input.Group>
          </Form.Item>
        </Col>
        <Col sm={8}>
          <Form.Item
            name="BetOrderNo"
            label={<FormattedMessage id="PageThirdpartyBetting.Conditions.OrderNo" description="訂單編號" />}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col sm={8}>
          <FormItemAdvanceQuery ADVANCED_SEARCH_CONDITION_ITEMS={ADVANCED_SEARCH_CONDITION_ITEMS} />
        </Col>
        <Col sm={4}>
          <FormItemIncludeInnerMember />
        </Col>
        <Col sm={4} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
