import { useMemo } from 'react';
import { Space, Result } from 'antd';
import { FormattedMessage } from 'react-intl';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import ExportReportButton from 'components/ExportReportButton';
import * as PERMISSION from 'constants/permissions';
import Condition from '../Condition';
import SummaryView from '../Summary';
import ThirdpartyBettingDetailProvider from 'contexts/ThirdpartyBettingDetailContext';
import { COLUMNS_CONFIG } from './datatableConfig';

const { THIRDPARTY_BET_DETAIL_REALITY_VIEW, THIRDPARTY_BET_DETAIL_REALITY_EXPORT } = PERMISSION;

const TableView = ({ tabKey, condition, onReady, onUpdateCondition, fetching, dataSource, error }) => {
  const columnConfig = useMemo(() => {
    if (!dataSource || !dataSource.Fields) {
      return [];
    }
    return COLUMNS_CONFIG.map(column => ({
      ...column,
      title: dataSource.Fields[column.key],
    }));
  }, [dataSource]);

  return (
    <Permission isPage functionIds={[THIRDPARTY_BET_DETAIL_REALITY_VIEW]}>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={<Condition onReady={onReady} onUpdate={onUpdateCondition} />}
        summaryComponent={error ? null : <SummaryView summary={dataSource?.Summary} />}
        datatableComponent={
          error ? (
            <Result status={error.data.status} title={error.data.status} subTitle={error.data.statusText} />
          ) : (
            <ThirdpartyBettingDetailProvider defaultKey={tabKey}>
              <DataTable
                displayResult={condition}
                condition={condition}
                title={<FormattedMessage id="Share.Table.SearchResult" description="查詢結果" />}
                config={columnConfig}
                loading={fetching}
                dataSource={dataSource?.Data}
                total={dataSource?.TotalCount}
                rowKey={record => record.BetOrderNo}
                extendArea={
                  <Space>
                    <Permission functionIds={[THIRDPARTY_BET_DETAIL_REALITY_EXPORT]}>
                      <ExportReportButton
                        condition={condition}
                        actionUrl={`/api/ThirdPartyBetDetail/Export/${tabKey}`}
                      />
                    </Permission>
                  </Space>
                }
                onUpdate={onUpdateCondition}
              />
            </ThirdpartyBettingDetailProvider>
          )
        }
      />
    </Permission>
  );
};

export default TableView;
