import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenBettingDetailButton from 'components/OpenBettingDetailButton';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    //订单ID
    dataIndex: 'BetOrderNo',
    key: 'BetOrderNo',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { BetOrderNo }, _2) => BetOrderNo,
  },
  {
    //会员帐号
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { MemberId, MemberName }, _2) => {
      return <OpenMemberDetailButton memberId={MemberId} memberName={MemberName} />;
    },
  },
  {
    //投注时间
    dataIndex: 'BetTime',
    key: 'BetTime',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { BetTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    //結算时间
    dataIndex: 'SettledTime',
    key: 'SettledTime',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { SettledTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    //投注金额
    dataIndex: 'BetAmount',
    key: 'BetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    //有效投注额
    dataIndex: 'ValidBetAmount',
    key: 'ValidBetAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    //投注前餘額
    dataIndex: 'BeforeCredit',
    key: 'BeforeCredit',
    isShow: true,
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    //中奖金额
    dataIndex: 'BonusAmount',
    key: 'BonusAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    //大厅/游戏类别
    dataIndex: 'RoundGameType',
    key: 'RoundGameType',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { RoundGameType }, _2) => <span dangerouslySetInnerHTML={{ __html: RoundGameType }} /> || NO_DATA,
  },
  {
    //游戏局号
    dataIndex: 'GameIssueNumber',
    key: 'GameIssueNumber',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { BetOrderNo, GameIssueNumber }, _2) => (
      <OpenBettingDetailButton betOrderNo={BetOrderNo}>{GameIssueNumber || NO_DATA}</OpenBettingDetailButton>
    ),
  },
  {
    //桌號
    dataIndex: 'TableCode',
    key: 'TableCode',
    isShow: true,
    render: (_1, { TableCode }, _2) => TableCode || NO_DATA,
  },
  {
    //投注内容
    dataIndex: 'BetContent',
    key: 'BetContent',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { BetContent }, _2) => BetContent || NO_DATA,
  },
  {
    //裝置
    dataIndex: 'DeviceType',
    key: 'DeviceType',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { DeviceType }, _2) => DeviceType || NO_DATA,
  },
  {
    //IP位址
    dataIndex: 'LoginIP',
    key: 'LoginIP',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { LoginIP }, _2) => LoginIP || NO_DATA,
  },
  {
    //更新时间
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    isShow: true,
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    //状态
    dataIndex: 'BetStatus',
    key: 'BetStatus',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { BetStatus }, _2) => BetStatus || NO_DATA,
  },
];
