import { Tabs } from 'antd';
import { useHistory, useParams } from 'react-router-dom';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import useGetThirdpartyTabs from 'hooks/useGetThirdpartyTabs';
import PageHeaderTab from 'components/PageHeaderTab';
import TableView from './Table';

const { TabPane } = Tabs;

export default function PageView() {
  const history = useHistory();
  const { key = '2' } = useParams();
  const tabKeys = useGetThirdpartyTabs();
  const { fetching, dataSource, onUpdateCondition, condition, onReady, error } = useGetDataSourceWithSWR({
    url: `/api/ThirdPartyBetDetail/Search/${key}`,
    autoFetch: true,
    defaultSortKey: 'BetTime',
    swrOption: {
      shouldRetryOnError: false,
    },
  });

  return (
    <>
      <PageHeaderTab
        defaultActiveKey={key}
        activeKey={key}
        onChange={key => {
          history.push({
            pathname: `/thirdparty-betting/reality/${key}`,
          });
        }}
      >
        {tabKeys.map(({ Id, Name }) => (
          <TabPane tab={Name} key={Id} />
        ))}
      </PageHeaderTab>
      <TableView
        error={error}
        tabKey={key}
        condition={condition}
        onReady={onReady}
        onUpdateCondition={onUpdateCondition}
        fetching={fetching}
        dataSource={dataSource}
      />
    </>
  );
}
