import React, { useEffect } from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { useRecoilState } from 'recoil'
import { permissionState } from 'recoilsystem/atoms'

export default function userPermissionToRecoil() {
    const [_, setPermission] = useRecoilState(permissionState);
    const { data: permissionData } = useSWR(
        ['/api/User/FunctionIds', '/api/menu/functionId'],
        (userFuIds, allFuIds) =>
            axios
                .all([axios.get(userFuIds), axios.get(allFuIds)])
                .then(res => {
                    const userFuIds = res[0].data.Data
                    const allFuIds = res[1].data.Data
                    const userFnIdsSet = new Set(userFuIds)
                    return new Map(
                        Object.keys(allFuIds).map(key => [
                            allFuIds[key],
                            userFnIdsSet.has(allFuIds[key]),
                        ])
                    )
                })
    )

    useEffect(() => {
        if (permissionData) {
            setPermission(permissionData)
        }
    }, [permissionData])
}
