import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Badge } from 'antd';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import ButtonItems from './Rows/ButtonItems';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsThirdParty.DataTable.Title.GameProviderName" description="第三方名称" />,
    dataIndex: 'GameProviderTypeName',
    key: 'GameProviderTypeName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsThirdParty.DataTable.Title.LockTime" description="关闭时间" />,
    dataIndex: 'LockTime',
    key: 'LockTime',
    render: (_1, { LockStartTime: s, LockEndTime: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: <FormattedMessage id="PageProductsThirdParty.DataTable.Title.Status" description="状态" />,
    dataIndex: 'IsEnabled',
    key: 'IsEnabled',
    isShow: true,
    render: (_1, record, _2) => (
      <Badge
        status={record.IsEnabled ? 'success' : 'error'}
        text={<FormattedMessage id={`PageProductsThirdParty.DataTable.Items.IsEnable.${!!record.IsEnabled}`} />}
      />
    ),
  },
  {
    title: <FormattedMessage id="PageProductsThirdParty.DataTable.Title.Cache" description="緩存" />,
    dataIndex: 'IsCacheInApp',
    key: 'IsCacheInApp',
    isShow: true,
    render: (_1, { IsCacheInApp }, _2) => (
      <Badge
        status={IsCacheInApp === -1 ? 'default' : IsCacheInApp ? 'success' : 'error'}
        text={<FormattedMessage id={`PageProductsThirdParty.DataTable.Items.IsCacheInApp.${IsCacheInApp}`} />}
      />
    ),
  },
  {
    title: <FormattedMessage id="PageProductsThirdParty.DataTable.Title.Manager" description="管理者" />,
    dataIndex: 'LastModificator',
    key: 'LastModificator',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsThirdParty.DataTable.Title.Action" description="管理" />,
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => <ButtonItems record={record} />,
  },
];
