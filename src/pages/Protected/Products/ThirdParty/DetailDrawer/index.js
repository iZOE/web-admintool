import React, { useContext, useEffect } from 'react';
import { Drawer, Button, Form, message, Switch, DatePicker } from 'antd';
import { FORMAT } from 'constants/dateConfig';
import { trigger } from 'swr';
import { useIntl, FormattedMessage } from 'react-intl';
import caller from 'utils/fetcher';
import { PageContext } from '../index';
import { getISODateTimeString, getDateTimeStringWithoutTimeZone } from 'mixins/dateTime';

const { RangePicker } = DatePicker;

export const LAYOUT_CONFIG = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 12,
  },
};

export default function DetailDrawer({ drawerData, setDrawer }) {
  const intl = useIntl();
  const { cacheQty } = useContext(PageContext);
  const { record, visible } = drawerData;
  const { GameProviderId, GameProviderTypeId, LockStartTime, LockEndTime, IsCacheInApp } = record;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue(record);
  }, [form, record]);

  const closeModal = () =>
    setDrawer({
      visible: false,
    });

  /**
   * 編輯
   */
  function onFinish(values) {
    const { IsEnabled, IsCacheInApp, LockTime } = values;
    const LockStartTime = LockTime ? getISODateTimeString(LockTime[0]) : null;
    const LockEndTime = LockTime ? getISODateTimeString(LockTime[1]) : null;

    caller({
      method: 'put',
      endpoint: `/api/GameProvider/${GameProviderId}`,
      body: {
        GameProviderTypeId,
        IsCacheInApp,
        IsEnabled,
        LockStartTime,
        LockEndTime,
      },
    })
      .then(() => {
        message.success(
          intl.formatMessage({
            id: 'Share.UIResponse.SaveSuccess',
            description: '编辑成功',
          }),
          10
        );
      })
      .finally(() => {
        closeModal();
        trigger('/api/GameProvider');
      });
  }

  return (
    <Drawer
      title={
        <>
          <FormattedMessage id="PageProductsThirdParty.PageHeader" description="第三方管理" /> -{' '}
          <span>{record.GameProviderTypeName}</span>
        </>
      }
      width={600}
      onClose={() => closeModal()}
      visible={visible}
      footer={
        <div
          style={{
            textAlign: 'right',
          }}
        >
          <Button onClick={() => closeModal()} style={{ marginRight: 8 }}>
            <FormattedMessage id="Share.ActionButton.Cancel" />
          </Button>
          <Button type="primary" onClick={() => form.submit()}>
            <FormattedMessage id="Share.ActionButton.Confirm" />
          </Button>
        </div>
      }
    >
      <Form {...LAYOUT_CONFIG} form={form} onFinish={onFinish}>
        <Form.Item
          name="IsEnabled"
          valuePropName="checked"
          label={<FormattedMessage id="PageProductsThirdParty.DataTable.Title.Status" description="状态" />}
        >
          <Switch
            checkedChildren={<FormattedMessage id="Share.SwitchButton.IsEnable.Yes" />}
            unCheckedChildren={<FormattedMessage id="Share.SwitchButton.IsEnable.No" />}
          />
        </Form.Item>
        <Form.Item shouldUpdate noStyle>
          {({ getFieldValue }) =>
            getFieldValue('IsEnabled') ? (
              <Form.Item
                name="LockTime"
                label={<FormattedMessage id="PageProductsThirdParty.DataTable.Title.LockTime" description="關閉時間" />}
              >
                <RangePicker
                  showTime
                  defaultValue={
                    LockStartTime &&
                    LockEndTime && [
                      getDateTimeStringWithoutTimeZone(LockStartTime),
                      getDateTimeStringWithoutTimeZone(LockEndTime),
                    ]
                  }
                  format={FORMAT.DISPLAY.DEFAULT}
                  style={{ width: '100%' }}
                />
              </Form.Item>
            ) : null
          }
        </Form.Item>
        {IsCacheInApp !== -1 && (
          <Form.Item
            name="IsCacheInApp"
            valuePropName="checked"
            label={<FormattedMessage id="PageProductsThirdParty.DataTable.Title.Cache" description="緩存" />}
            normalize={value => Number(value)}
          >
            <Switch
              disabled={cacheQty >= 3 && !IsCacheInApp}
              checkedChildren={<FormattedMessage id="Share.SwitchButton.IsEnable.Yes" />}
              unCheckedChildren={<FormattedMessage id="Share.SwitchButton.IsEnable.No" />}
            />
          </Form.Item>
        )}
      </Form>
    </Drawer>
  );
}
