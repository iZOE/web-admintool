import React, { useMemo, useState, createContext } from 'react';
import { FormattedMessage } from 'react-intl';
import useSWR from 'swr';
import axios from 'axios';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import DetailDrawer from './DetailDrawer';
import { COLUMNS_CONFIG } from './datatableConfig';

const { PRODUCTS_THIRD_PARTY_VIEW } = PERMISSION;

export const PageContext = createContext();

const PageView = () => {
  const [isSortDesc, setIsSortDesc] = useState(true);
  const [detailDrawer, setDetailDrawer] = useState({
    visible: false,
    record: null,
  });

  const { data: dataSource } = useSWR(`/api/GameProvider`, url => axios(url).then(res => res.data.Data));

  const cacheQty = useMemo(() => {
    if (dataSource) {
      return dataSource.Data.filter(item => item.IsCacheInApp > 0).length;
    }
    return 0;
  }, [dataSource]);

  return (
    <PageContext.Provider
      value={{
        cacheQty,
        dataSource,
        detailDrawer,
        setDetailDrawer,
      }}
    >
      <Permission functionIds={[PRODUCTS_THIRD_PARTY_VIEW]} isPage>
        <DataTable
          title={<FormattedMessage id="Share.Table.SearchResult" description="查詢結果" />}
          config={COLUMNS_CONFIG}
          dataSource={dataSource && dataSource.Data}
          pagination={false}
          rowKey={record => record.GameProviderID}
          onUpdate={() => setIsSortDesc(!isSortDesc)}
        />
        {detailDrawer.record && <DetailDrawer drawerData={detailDrawer} setDrawer={data => setDetailDrawer(data)} />}
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
