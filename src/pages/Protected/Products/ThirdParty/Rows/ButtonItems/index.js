import React, { useContext } from 'react';
import { Button } from 'antd';
import { FormattedMessage } from 'react-intl';
import { PageContext } from '../../index';
import Permission from 'components/Permission';
import * as PERMISSION from 'constants/permissions';

const { PRODUCTS_THIRD_PARTY_EDIT } = PERMISSION;

export default function ButtonItems({ record }) {
  const { setDetailDrawer } = useContext(PageContext);
  return (
    <Permission
      functionIds={[PRODUCTS_THIRD_PARTY_EDIT]}
      failedRender={<>--</>}
    >
      <Button
        type="link"
        onClick={() =>
          setDetailDrawer({
            visible: true,
            record
          })
        }
      >
        <FormattedMessage id="Share.ActionButton.Edit" />
      </Button>
    </Permission>
  );
}
