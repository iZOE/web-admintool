import React from 'react';
import { Tabs } from 'antd';
import { useHistory, useParams } from 'react-router-dom';
import PageHeaderTab from 'components/PageHeaderTab';
import Info from './Info';
import SwitchView from './Switch';
import { FormattedMessage } from 'react-intl';

const { TabPane } = Tabs;

const PageView = () => {
  const history = useHistory();
  const { key } = useParams();

  return (
    <>
      <PageHeaderTab
        defaultActiveKey={key}
        onChange={key => {
          history.push({
            pathname: `/products/lottery/${key}`,
          });
        }}
      >
        <TabPane tab={<FormattedMessage id="PageProductsLottery.Tabs.Title.0" description="基本設置" />} key="info" />
        <TabPane tab={<FormattedMessage id="PageProductsLottery.Tabs.Title.1" description="彩種開關" />} key="switch" />
      </PageHeaderTab>
      {key === 'switch' ? <SwitchView /> : <Info />}
    </>
  );
};

export default PageView;
