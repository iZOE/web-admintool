import React, { useContext, useEffect } from 'react';
import { Form, Row, Col } from 'antd';
import { ConditionContext } from 'contexts/ConditionContext';
import FormItemsLotteries from 'components/FormItems/Lotteries';
import QueryButton from 'components/FormActionButtons/Query';

const { useForm } = Form;

function Condition({ onReady, onUpdate }) {
  const { isReady } = useContext(ConditionContext);
  const [form] = useForm();

  useEffect(() => {
    onUpdate(form.getFieldsValue());
    onReady(isReady);
  }, [isReady]);

  return (
    <Form form={form} onFinish={onUpdate.bySearch}>
      <Row gutter={[16, 8]}>
        <Col sm={8}>
          <FormItemsLotteries checkAll />
        </Col>
        <Col sm={{ span: 8, offset: 8 }} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
