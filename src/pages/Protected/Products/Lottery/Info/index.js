import React from 'react';
import { FormattedMessage } from 'react-intl';
import Permission from 'components/Permission';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import * as PERMISSION from 'constants/permissions';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const { AFFILIATES_CONTRACT_DAILY_DIVIDENDS_VIEW } = PERMISSION;

export default function Info() {
  const {
    fetching,
    dataSource,
    onUpdateCondition,
    onReady,
    condition
  } = useGetDataSourceWithSWR({
    url: '/api/lottery/search',
    defaultSortKey: 'ModifyTime',
    autoFetch: true
  });

  return (
    <Permission isPage functionIds={[AFFILIATES_CONTRACT_DAILY_DIVIDENDS_VIEW]}>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={
          <Condition onReady={onReady} onUpdate={onUpdateCondition} />
        }
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            title={
              <FormattedMessage
                id="Share.Table.SearchResult"
                description="查询结果"
              />
            }
            config={COLUMNS_CONFIG}
            loading={fetching}
            dataSource={dataSource && dataSource.Data}
            total={dataSource && dataSource.TotalCount}
            rowKey={record => record.LotteryCode}
            onUpdate={onUpdateCondition}
          />
        }
      />
    </Permission>
  );
}
