import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsLotteryInfo.DataTable.Title.Description" description="彩種名稱" />,
    dataIndex: 'Description',
    key: 'Description',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsLotteryInfo.DataTable.Title.BonusAmountLimit" description="限紅" />,
    dataIndex: 'BonusAmountLimit',
    key: 'BonusAmountLimit',
    isShow: true,
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="Share.CommonKeys.UpdateMember" description="異動人員" />,
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
    isShow: true,
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
];
