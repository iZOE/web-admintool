import React, { useContext } from 'react'
import {
    Button,
    Empty,
    Form,
    Input,
    List,
    Modal,
    Space,
    Switch,
    Table,
    Tooltip,
    Typography,
} from 'antd'
import {
    CaretRightOutlined,
    InfoCircleFilled,
    ExclamationCircleOutlined,
} from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import ClassNamer from 'utils/classNamer'
import { PageContext } from '../index'
import { Wrapper } from './Styled'
import lotterySwitchService from '../services'

const { confirm } = Modal
const { useForm } = Form
const { Text } = Typography
const { updateLotteryStatus } = lotterySwitchService()

const CellList = ({
    dataSource,
    selected,
    onListClick,
    onSwitchChange,
    upperLayerCloumnName,
}) => (
    <List
        size="small"
        dataSource={dataSource}
        locale={{
            emptyText: (
                <Empty
                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                    description={
                        <FormattedMessage
                            id="Share.PlaceHolder.Input.PleaseSelectSomething"
                            values={{
                                field: (
                                    <FormattedMessage
                                        id={`PageProductsLotterySwaitch.Table.Columns.${upperLayerCloumnName}`}
                                    />
                                ),
                            }}
                        />
                    }
                />
            ),
        }}
        renderItem={(item, listIndex) => (
            <List.Item
                className={ClassNamer({
                    selected: selected && selected(item),
                    edited: item.Edited,
                })}
                extra={
                    <div className="info">
                        {item.ModifyTime && item.Account ? (
                            <Tooltip
                                title={
                                    <FormattedMessage
                                        id="PageProductsLotterySwaitch.Table.Columns.ModifyInfo"
                                        values={{
                                            time: (
                                                <DateWithFormat
                                                    time={item.ModifyTime}
                                                />
                                            ),
                                            account: item.Account,
                                        }}
                                    />
                                }
                            >
                                <InfoCircleFilled />
                            </Tooltip>
                        ) : null}
                        {upperLayerCloumnName !== 'BetSubType' && (
                            <CaretRightOutlined />
                        )}
                    </div>
                }
                onClick={() => onListClick && onListClick(item)}
            >
                <Space align="baseline">
                    <Switch
                        size="small"
                        checked={item.Status === 1}
                        onChange={value => {
                            onSwitchChange(value, item, listIndex)
                        }}
                    />
                    <Text>{item.Name}</Text>
                </Space>
            </List.Item>
        )}
    />
)

export default function Container() {
    const [form] = useForm()
    const intl = useIntl()
    const {
        lotteryData,
        switchData,
        onUpdateSwitchData,
        onUpdateLotteryData,
        platform,
        requery,
        resetLotteryData,
    } = useContext(PageContext)

    const { BetPage, BetType, BetSubType, BetRule } = lotteryData

    const onCheckChange = (value, name) => {
        form.setFields([
            {
                name: 'switchTable',
                touched: true,
            },
            {
                name,
                value: [value[0] ? 1 : 2, value[1]],
                touched: true,
            },
        ])
    }

    const organizeResults = values => {
        return Object.keys(values).reduce((prev, curr) => {
            if (!Array.isArray(values[curr])) {
                return {
                    ...prev,
                    [curr]: organizeResults(values[curr]),
                }
            } else {
                if (values[curr][0] !== values[curr][1]) {
                    return {
                        ...prev,
                        [curr]: values[curr][0],
                    }
                }
                return prev
            }
        }, {})
    }

    const onFinish = () => {
        confirm({
            title: intl.formatMessage({
                id: 'Share.ActionButton.Save',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: 'Share.ConfirmTips.WhetherToSaveExistingChanges',
            }),
            okText: intl.formatMessage({
                id: 'Share.ActionButton.Save',
                description: '保存',
            }),
            cancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
                description: '取消',
            }),
            onOk() {
                updateLotteryStatus({
                    Platform: platform,
                    ...organizeResults(form.getFieldValue()),
                }).then(requery)
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    const showResetConfirm = () => {
        confirm({
            title: intl.formatMessage({
                id: 'Share.CommonKeys.FillInAgain',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: 'Share.ConfirmTips.DoYouWantToReeditTheContent',
            }),
            okText: intl.formatMessage({ id: 'Share.ActionButton.Restart' }),
            okType: 'danger',
            cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
            onOk() {
                resetLotteryData()
                form.resetFields()
            },
            onCancel() {
                console.log('Cancel showResetConfirm')
            },
        })
    }

    const COLUMNS_CONFIG = [
        {
            key: 'LotteryName',
            dataIndex: 'LotteryName',
            title: (
                <FormattedMessage id="PageProductsLotterySwaitch.Table.Columns.LotteryName" />
            ),
            render: (_1, record, index) => {
                const { Account, ModifyTime, Status, OrigiStatus, Id } = record
                return (
                    <List
                        size="small"
                        dataSource={[record]}
                        renderItem={item => (
                            <List.Item
                                className={
                                    Status !== OrigiStatus ? 'edited' : ''
                                }
                                extra={
                                    ModifyTime && Account ? (
                                        <Tooltip
                                            title={
                                                <FormattedMessage
                                                    id="PageProductsLotterySwaitch.Table.Columns.ModifyInfo"
                                                    values={{
                                                        time: (
                                                            <DateWithFormat
                                                                time={
                                                                    ModifyTime
                                                                }
                                                            />
                                                        ),
                                                        account: Account,
                                                    }}
                                                />
                                            }
                                        >
                                            <InfoCircleFilled />
                                        </Tooltip>
                                    ) : null
                                }
                            >
                                <Space align="baseline">
                                    <Switch
                                        size="small"
                                        checked={Status === 1}
                                        onChange={value => {
                                            onUpdateSwitchData(
                                                {
                                                    ...record,
                                                    Status: value ? 1 : 2,
                                                },
                                                index
                                            )
                                            onCheckChange(
                                                [value, item.OrigiStatus],
                                                ['LotteryType', `${Id}`]
                                            )
                                        }}
                                    />
                                    <Text>{item.Name}</Text>
                                </Space>
                            </List.Item>
                        )}
                    />
                )
            },
        },
        {
            key: 'BetMainPage',
            dataIndex: 'BetMainPage',
            title: (
                <FormattedMessage id="PageProductsLotterySwaitch.Table.Columns.BetMainPage" />
            ),
            render: (_1, record, index) => {
                const { BetMainPage, Id } = record
                const listData =
                    BetPage[Id] &&
                    Object.keys(BetPage[Id]).map(key => ({
                        Name: key,
                    }))
                return (
                    <List
                        size="small"
                        dataSource={listData}
                        renderItem={item => (
                            <List.Item
                                className={ClassNamer({
                                    selected: BetMainPage === item.Name,
                                    'child-edited': item.Edited,
                                })}
                                extra={<CaretRightOutlined />}
                                onClick={() => {
                                    onUpdateSwitchData(
                                        {
                                            ...record,
                                            BetMainPage: item.Name,
                                            BetPage: null,
                                            BetType: null,
                                            BetSubType: null,
                                            BetRule: null,
                                        },
                                        index
                                    )
                                }}
                            >
                                <FormattedMessage
                                    id={`PageProductsLotterySwaitch.ColumnList.${item.Name}`}
                                />
                            </List.Item>
                        )}
                    />
                )
            },
        },
        {
            key: 'BetPage',
            dataIndex: 'BetPage',
            title: (
                <FormattedMessage id="PageProductsLotterySwaitch.Table.Columns.BetPage" />
            ),
            render: (_1, record, index) => {
                const { BetMainPage, BetPage: pageName, Id } = record
                const listData = BetPage[Id] && BetPage[Id][BetMainPage]
                return (
                    <CellList
                        form={form}
                        name={['BetPageName', `${Id}`]}
                        dataSource={listData}
                        selected={item => pageName === item.PageName}
                        onListClick={item =>
                            onUpdateSwitchData(
                                {
                                    ...record,
                                    BetPage: item.PageName,
                                    BetType: `${Id}_${item.PageName}`,
                                    BetSubType: null,
                                    BetRule: null,
                                },
                                index
                            )
                        }
                        onSwitchChange={(value, item, listIndex) => {
                            onCheckChange(
                                [value, item.OrigiStatus],
                                ['BetPageName', `${Id}`, `${item.Id}`]
                            )
                            const fieldValue =
                                form.getFieldValue([
                                    'BetPageName',
                                    `${Id}`,
                                    `${item.Id}`,
                                ])[0] || item.Status
                            listData[listIndex].Edited =
                                fieldValue !== item.OrigiStatus
                            listData[listIndex].Status = fieldValue
                            onUpdateLotteryData({ BetPage })
                        }}
                        upperLayerCloumnName="BetMainPageName"
                    />
                )
            },
        },
        {
            key: 'BetType',
            dataIndex: 'BetType',
            title: (
                <FormattedMessage id="PageProductsLotterySwaitch.Table.Columns.BetType" />
            ),
            render: (_1, record, index) => {
                const { BetType: betType, BetSubType } = record
                const listData = betType ? BetType[betType] : []
                return (
                    <CellList
                        form={form}
                        name={['BetType']}
                        dataSource={listData}
                        selected={item => BetSubType === item.Id}
                        onListClick={item =>
                            onUpdateSwitchData(
                                {
                                    ...record,
                                    BetSubType: item.Id,
                                    BetRule: null,
                                },
                                index
                            )
                        }
                        onSwitchChange={(value, item, listIndex) => {
                            onCheckChange(
                                [value, item.OrigiStatus],
                                ['BetType', `${item.Id}`]
                            )
                            const fieldValue =
                                form.getFieldValue([
                                    'BetType',
                                    `${item.Id}`,
                                ])[0] || item.Status
                            listData[listIndex].Edited =
                                fieldValue !== item.OrigiStatus
                            listData[listIndex].Status = fieldValue
                            onUpdateLotteryData({ BetType })
                        }}
                        upperLayerCloumnName="BetPage"
                    />
                )
            },
        },
        {
            key: 'BetSubType',
            dataIndex: 'BetSubType',
            title: (
                <FormattedMessage id="PageProductsLotterySwaitch.Table.Columns.BetSubType" />
            ),
            render: (_1, record, index) => {
                const { BetSubType: betSubType, BetRule } = record
                const listData = betSubType ? BetSubType[betSubType] : []
                return (
                    <CellList
                        form={form}
                        name={['BetSubType']}
                        dataSource={listData}
                        selected={item => BetRule === item.Id}
                        onListClick={item =>
                            onUpdateSwitchData(
                                {
                                    ...record,
                                    BetRule: item.Id,
                                },
                                index
                            )
                        }
                        onSwitchChange={(value, item, listIndex) => {
                            onCheckChange(
                                [value, item.OrigiStatus],
                                ['BetSubType', `${item.Id}`]
                            )
                            const fieldValue =
                                form.getFieldValue([
                                    'BetSubType',
                                    `${item.Id}`,
                                ])[0] || item.Status
                            listData[listIndex].Edited =
                                fieldValue !== item.OrigiStatus
                            listData[listIndex].Status = fieldValue
                            onUpdateLotteryData({ BetSubType })
                        }}
                        upperLayerCloumnName="BetType"
                    />
                )
            },
        },
        {
            key: 'BetRule',
            dataIndex: 'BetRule',
            title: (
                <FormattedMessage id="PageProductsLotterySwaitch.Table.Columns.BetRule" />
            ),
            render: (_1, record) => {
                const { BetRule: betRule } = record
                const listData = betRule ? BetRule[betRule] : []
                return (
                    <CellList
                        form={form}
                        name={['BetRule']}
                        dataSource={listData}
                        onSwitchChange={(value, item, listIndex) => {
                            onCheckChange(
                                [value, item.OrigiStatus],
                                ['BetRule', `${item.Id}`]
                            )
                            const fieldValue =
                                form.getFieldValue([
                                    'BetRule',
                                    `${item.Id}`,
                                ])[0] || item.Status
                            listData[listIndex].Edited =
                                fieldValue !== item.OrigiStatus
                            listData[listIndex].Status = fieldValue
                            onUpdateLotteryData({ BetRule })
                        }}
                        upperLayerCloumnName="BetSubType"
                    />
                )
            },
        },
    ]

    return (
        <Wrapper>
            <Form form={form} onFinish={onFinish}>
                <Table
                    bordered
                    size="small"
                    title={() => (
                        <Space>
                            <Button
                                type="primary"
                                htmlType="submit"
                                disabled={!form.isFieldsTouched()}
                            >
                                <FormattedMessage id="Share.ActionButton.Submit" />
                            </Button>
                            <Button
                                htmlType="button"
                                onClick={showResetConfirm}
                                disabled={!form.isFieldsTouched()}
                            >
                                <FormattedMessage id="Share.ActionButton.Restart" />
                            </Button>
                        </Space>
                    )}
                    dataSource={switchData}
                    columns={COLUMNS_CONFIG}
                    pagination={false}
                    scroll={{ y: '64vh' }}
                />
                <Form.Item noStyle name="switchTable">
                    <Input type="hidden"></Input>
                </Form.Item>
            </Form>
        </Wrapper>
    )
}
