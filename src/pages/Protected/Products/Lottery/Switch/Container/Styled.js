import React from 'react';
import styled from 'styled-components';

export const Wrapper = styled.div`
  .ant-table {
    .ant-table-thead {
      th {
        text-align: center;
      }
    }
    .ant-table-title {
      text-align: right;
    }
    .ant-table-tbody {
      & > tr {
        & > td {
          &.ant-table-cell {
            padding: 0;
            vertical-align: top;
            & > .ant-list {
              height: 330px;
              overflow: scroll;
            }
          }
        }
      }
      .ant-list-item {
        cursor: pointer;
        &.selected {
          background-color: #e6f7ff;
          color: #1890ff;
        }
        &.edited {
          position: relative;
          &:after {
            content: '';
            position: absolute;
            display: block;
            top: 0;
            left: 0;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 12px 12px 0 0;
            border-color: #ffb700 transparent transparent transparent;
          }
        }
        &.child-edited {
          .anticon {
            color: #ffb700;
          }
        }
        & > .info {
          flex: 1;
          text-align: right;
        }
      }
    }
  }
`;
