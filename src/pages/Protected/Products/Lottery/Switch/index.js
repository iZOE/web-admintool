import React, {
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';
import { Skeleton } from 'antd';
import Permission from 'components/Permission';
import * as PERMISSION from 'constants/permissions';
import ReportScaffold from 'components/ReportScaffold';
import PleaseSearch from 'components/PleaseSearch';
import Container from './Container';
import Condition from './Condition';
import lotterySwitchService from './services';

const { AFFILIATES_CONTRACT_DAILY_DIVIDENDS_VIEW } = PERMISSION;

export const PageContext = createContext();

const { getLotteryStatusByPlatform } = lotterySwitchService();

const groupBy = (souce, key, keys) =>
  souce[key].reduce((groups, item) => {
    const val = keys.map(key => item[key]).join('_');
    let parentObj = '';
    switch (key) {
      case 'BetRule':
        parentObj = { ParentId: item.BetSubTypeId };
        break;
      case 'BetSubType':
        parentObj = { ParentId: item.BetTypeId };
        break;
      case 'BetType':
        parentObj = { ParentId: item.LotteryCode, PageName: item.PageName };
        break;
      default:
        parentObj = { ParentId: item.LotteryCode, PageName: item.PageName };
    }
    groups[val] = groups[val] || [];
    groups[val].push({
      Account: item.ModifyUserAccount,
      Id: item[`${key}Id`],
      Name: item[`${key}Name`] || item.BetPageNameText,
      Status: item[`${key}Status`],
      ModifyTime: item.ModifyTime,
      OrigiStatus: item[`${key}Status`],
      ...parentObj
    });
    return groups;
  }, {});

export default function Switch() {
  const [platform, setPlatform] = useState(1);
  const [condition, setCondition] = useState(null);
  const [dataSource, setDataSource] = useState(null);
  const [switchData, setSwitchData] = useState(null);
  const [lotteryData, setLotteryData] = useState(null);
  const [firstQuery, setFirstQuery] = useState(false);
  const dataSourceReduce = useMemo(() => {
    if (dataSource) {
      return Object.keys(dataSource).reduce((result, key) => {
        switch (key) {
          case 'BetRule':
            result[key] = groupBy(dataSource, key, ['BetSubTypeId']);
            return result;
          case 'BetSubType':
            result[key] = groupBy(dataSource, key, ['BetTypeId']);
            return result;
          case 'BetType':
            result[key] = groupBy(dataSource, key, ['LotteryCode', 'PageName']);
            return result;
          case 'BetPageName':
            const BetPageName = groupBy(dataSource, key, ['LotteryCode']);
            Object.keys(BetPageName).forEach(
              key =>
                (BetPageName[key] = BetPageName[key].reduce((groups, item) => {
                  const val =
                    item.PageName === 'twoface' ? 'twoface' : 'official';
                  groups[val] = groups[val] || [];
                  groups[val].push(item);
                  return groups;
                }, {}))
            );
            result['BetPage'] = BetPageName;
            return result;
          default:
            return result;
        }
      }, {});
    }
    return null;
  }, [dataSource]);

  useEffect(() => {
    setLotteryData(JSON.parse(JSON.stringify(dataSourceReduce)));
  }, [dataSourceReduce]);

  const onUpdateCondition = ({ platform, lotteryCode }) => {
    if (!firstQuery) {
      setFirstQuery(true);
    }
    setDataSource(null);
    getLotteryStatusByPlatform(platform, lotteryCode).then(res => {
      setPlatform(platform);
      setSwitchData(
        res['LotteryType'].map(item => ({
          key: item.LotteryCode,
          Status: item.LotteryStatus,
          OrigiStatus: item.LotteryStatus,
          Id: item.LotteryCode,
          Name: `[${item.Description}]${item.LotteryName}`,
          Account: item.ModifyUserAccount,
          ModifyTime: item.ModifyTime,
          BetMainPage: 'official',
          BetPage: null,
          BetType: null,
          BetSubType: null,
          BetRule: null
        }))
      );
      setDataSource(res);
    });
  };

  const onUpdateSwitchData = useCallback((newRecord, index) => {
    setSwitchData(prevRecord => {
      prevRecord.splice(index, 1, newRecord);
      return [...prevRecord];
    });
  }, []);

  const onUpdateLotteryData = useCallback(
    newData => {
      const key = Object.keys(newData).join();
      setLotteryData({
        ...lotteryData,
        [key]: newData[key]
      });
    },
    [lotteryData]
  );

  const resetLotteryData = useCallback(() => {
    setLotteryData(JSON.parse(JSON.stringify(dataSourceReduce)));
    setSwitchData(prev =>
      prev.map(item => ({ ...item, Status: item.OrigiStatus }))
    );
  }, [dataSourceReduce]);

  const requery = useCallback(() => {
    onUpdateCondition(condition);
  }, [condition]);

  return (
    <PageContext.Provider
      value={{
        dataSource,
        lotteryData,
        switchData,
        platform,
        onUpdateSwitchData,
        onUpdateLotteryData,
        requery,
        resetLotteryData
      }}
    >
      <Permission
        isPage
        functionIds={[AFFILIATES_CONTRACT_DAILY_DIVIDENDS_VIEW]}
      >
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition
              onUpdate={condition => {
                setCondition(condition);
                onUpdateCondition(condition);
              }}
            />
          }
          datatableComponent={
            <PleaseSearch display={!!firstQuery}>
              <Skeleton active loading={!lotteryData}>
                <Container />
              </Skeleton>
            </PleaseSearch>
          }
        />
      </Permission>
    </PageContext.Provider>
  );
}
