import React from 'react';
import { Col, Form, Row, Select } from 'antd';
import { FormattedMessage } from 'react-intl';
import FormItemsLotteries from 'components/FormItems/Lotteries';
import QueryButton from 'components/FormActionButtons/Query';

const { useForm } = Form;
const { Option } = Select;
const PLATFORMS = [
  {
    Id: 1,
    Name: 'iOS'
  },
  {
    Id: 2,
    Name: 'Andriod'
  },
  {
    Id: 3,
    Name: 'Web'
  },
  {
    Id: 4,
    Name: 'H5'
  }
];

function Condition({ onUpdate }) {
  const [form] = useForm();

  return (
    <Form form={form} onFinish={onUpdate}>
      <Row gutter={[16, 8]}>
        <Col sm={6}>
          <Form.Item
            name="platform"
            initialValue={1}
            label={<FormattedMessage id="Share.QueryCondition.Device" />}
          >
            <Select>
              {PLATFORMS.map(({ Id, Name }) => (
                <Option key={Id} value={Id}>
                  {Name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col sm={16}>
          <FormItemsLotteries checkAll name="lotteryCode" />
        </Col>
        <Col sm={2} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
