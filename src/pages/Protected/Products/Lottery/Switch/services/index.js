import makeService from 'utils/makeService';

const lotterySwitchService = () => {
  const getLotteryStatusByPlatform = (platform, payload) =>
    makeService({
      method: 'post',
      url: `/api/Lottery/Switch?platform=${platform}`
    })(payload);

  const updateLotteryStatus = payload =>
    makeService({
      method: 'patch',
      url: '/api/Lottery/Switch'
    })(payload);

  return {
    getLotteryStatusByPlatform,
    updateLotteryStatus
  };
};

export default lotterySwitchService;
