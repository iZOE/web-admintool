import React, { useContext } from 'react'
import { Button, message } from 'antd'
import { FormattedMessage } from 'react-intl'
import _cloneDeep from 'lodash.clonedeep'
import caller from 'utils/fetcher'
import Permission from 'components/Permission'
import * as PERMISSION from 'constants/permissions'
import PopconfirmButton from './PopconfirmButton'
import { PageContext } from '../../index'

const {
  PRODUCTS_ISSUE_NUMBER_EDIT,
  PRODUCTS_ISSUE_NUMBER_CANCEL_ISSUE_NUMBER,
  PRODUCTS_ISSUE_NUMBER_DELETE,
  PRODUCTS_ISSUE_NUMBER_MANUAL_ISSUE_NUMBER,
  PRODUCTS_ISSUE_NUMBER_RESET_ISSUE_NUMBER,
} = PERMISSION

export default function ButtonItems({ record }) {
  const { LotteryHistoryId, Status, IsOriginalLottery } = record
  const {
    setEditDrawer,
    setReSattleDrawer,
    dataSource,
    boundedMutate,
  } = useContext(PageContext)

  const newDataSource = _cloneDeep(dataSource)
  const getRowIndex = LotteryHistoryId =>
    newDataSource.Data.findIndex(x => x.LotteryHistoryId === LotteryHistoryId)

  if (!IsOriginalLottery) {
    return null
  }

  /**
   * 刪除期數
   * @param {IssueNumber}
   */
  function onDelete() {
    caller({
      method: 'delete',
      endpoint: `/api/IssueNumber/${LotteryHistoryId}`,
    }).then(() => {
      message.success('success')
      const rowIndex = getRowIndex(LotteryHistoryId)
      delete newDataSource.Data[rowIndex]
      boundedMutate(newDataSource, false)
    })
  }

  /**
   * 取消當期
   * @param {IssueNumber}
   */
  function onCancel() {
    caller({
      method: 'patch',
      endpoint: `/api/IssueNumber/${LotteryHistoryId}/Cancel`,
    }).then(() => {
      message.success('success')
      const rowIndex = getRowIndex(LotteryHistoryId)
      newDataSource.Data[rowIndex].Status = 6
      boundedMutate(newDataSource, false)
    })
  }

  const Cancel = () => (
    <Permission
      functionIds={[PRODUCTS_ISSUE_NUMBER_CANCEL_ISSUE_NUMBER]}
      failedRender={<>--</>}
    >
      <PopconfirmButton type="Cancel" onConfirm={onCancel} record={record} />
    </Permission>
  )

  const Edit = () => (
    <Permission
      functionIds={[PRODUCTS_ISSUE_NUMBER_EDIT]}
      failedRender={<>--</>}
    >
      <Button
        type="link"
        onClick={() =>
          setEditDrawer({
            visible: true,
            record,
          })
        }
      >
        <FormattedMessage id="Share.ActionButton.Edit" />
      </Button>
    </Permission>
  )

  const Delete = () => (
    <Permission
      functionIds={[PRODUCTS_ISSUE_NUMBER_DELETE]}
      failedRender={<>--</>}
    >
      <PopconfirmButton record={record} type="Delete" onConfirm={onDelete} />
    </Permission>
  )

  const ReSettle = () => (
    <Permission
      functionIds={[PRODUCTS_ISSUE_NUMBER_RESET_ISSUE_NUMBER]}
      failedRender={<>--</>}
    >
      <Button
        type="link"
        onClick={() =>
          setReSattleDrawer({
            visible: true,
            type: 'ReSettle',
            record,
          })
        }
      >
        <FormattedMessage id="PageProductsIssueNumber.ActionButton.ReSettle" />
      </Button>
    </Permission>
  )

  const ManualSettle = () => (
    <Permission
      functionIds={[PRODUCTS_ISSUE_NUMBER_MANUAL_ISSUE_NUMBER]}
      failedRender={<>--</>}
    >
      <Button
        type="link"
        onClick={() =>
          setReSattleDrawer({
            visible: true,
            type: 'ManualSettle',
            record,
          })
        }
      >
        <FormattedMessage id="PageProductsIssueNumber.ActionButton.ManualSettle" />
      </Button>
    </Permission>
  )

  function renderButtonByStatus() {
    // 1    Edit & Delete
    // 2&3  Cancel
    // 4    ReSettle & Cancel
    // 5    ManualSettle & Cancel
    // 6    null
    switch (Status) {
      case 1:
        return (
          <>
            <Edit />
            <Delete />
          </>
        )
      case 2:
        return <Cancel />
      case 3:
        return <Cancel />
      case 4:
        return (
          <>
            <ReSettle />
            <Cancel />
          </>
        )
      case 5:
        return (
          <>
            <ManualSettle />
            <Cancel />
          </>
        )
      default:
        return null
    }
  }

  return <>{renderButtonByStatus()}</>
}
