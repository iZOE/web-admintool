import React from "react";
import { Button, Popconfirm } from "antd";
import { FormattedMessage } from "react-intl";

export default function PopconfirmButton({ record, type, onConfirm }) {
  const { LotteryName, IssueNumber } = record;
  return (
    <Popconfirm
      title={
        <FormattedMessage
          id={`PageProductsIssueNumber.Popconfirm.${type}`}
          values={{
            LotteryName,
            IssueNumber,
            br: chunks => <>{chunks}<br /></>
          }}
        />
      }
      onConfirm={onConfirm}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
      placement="topLeft"
    >
      <Button type="link" danger>
        <FormattedMessage id={`PageProductsIssueNumber.ActionButton.${type}`} />
      </Button>
    </Popconfirm>
  );
}
