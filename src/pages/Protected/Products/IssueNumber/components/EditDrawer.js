import React, { useMemo } from 'react'
import { Button, DatePicker, Drawer, Form, message } from 'antd'
import { useIntl, FormattedMessage } from 'react-intl'
import { FORMAT } from 'constants/dateConfig'
import caller from 'utils/fetcher'
import DateWithFormat from 'components/DateWithFormat'
import { LAYOUT_CONFIG } from './layoutConfig'
import {
    getDateTimeStringWithoutTimeZone,
    getISODateTimeString,
} from 'mixins/dateTime'

export default function ModalButton({ modalData, setModalData }) {
    const intl = useIntl()
    const [form] = Form.useForm()
    const { record } = modalData

    const closeModal = () =>
        setModalData({
            visible: false,
        })

    const initialValues = useMemo(() => {
        if (record) {
            const defaultValues = Object.assign({}, record)
            defaultValues.StartTime = getDateTimeStringWithoutTimeZone(
                defaultValues.StartTime
            )
            defaultValues.AnnounceTime = getDateTimeStringWithoutTimeZone(
                defaultValues.AnnounceTime
            )
            defaultValues.EndTime = getDateTimeStringWithoutTimeZone(
                defaultValues.EndTime
            )
            return defaultValues
        }
    }, [record])

    if (!record) {
        return null
    }

    /**
     * 編輯
     * @param {IssueNumber}
     */
    function onSend(values) {
        values.StartTime = getISODateTimeString(values.StartTime)
        values.AnnounceTime = getISODateTimeString(values.AnnounceTime)
        delete values.EndTime
        caller({
            method: 'put',
            endpoint: `/api/IssueNumber/${record.LotteryHistoryId}`,
            body: {
                ...record,
                ...values,
            },
        }).then(() => {
            message.success(
                intl.formatMessage({
                    id: 'Share.UIResponse.SaveSuccess',
                    description: '编辑成功',
                }),
                10
            )
            closeModal()
        })
    }

    return (
        <Drawer
            title={<FormattedMessage id="Share.ActionButton.Edit" />}
            width={500}
            onClose={() => closeModal()}
            visible={modalData.visible}
            footer={
                <div
                    style={{
                        textAlign: 'right',
                    }}
                >
                    <Button
                        onClick={() => closeModal()}
                        style={{ marginRight: 8 }}
                    >
                        <FormattedMessage id="Share.ActionButton.Cancel" />
                    </Button>
                    <Button type="primary" onClick={() => form.submit()}>
                        <FormattedMessage id="Share.ActionButton.Confirm" />
                    </Button>
                </div>
            }
        >
            <Form
                {...LAYOUT_CONFIG}
                name="basic"
                form={form}
                onFinish={values => onSend(values)}
                initialValues={initialValues}
            >
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsIssueNumber.DataTable.Title.LotteryName"
                            description="彩種"
                        />
                    }
                >
                    {record.LotteryName}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsIssueNumber.DataTable.Title.IssueNumber"
                            description="期號"
                        />
                    }
                >
                    {record.IssueNumber}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsIssueNumber.DataTable.Title.BonusNumber"
                            description="開獎號碼"
                        />
                    }
                >
                    {record.BonusNumber}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsIssueNumber.DataTable.Title.AnnounceTime"
                            description="開獎時間"
                        />
                    }
                    name="AnnounceTime"
                >
                    <DatePicker
                        format={FORMAT.DISPLAY.DAILY}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsIssueNumber.DataTable.Title.StartTime"
                            description="開盤時間"
                        />
                    }
                    name="StartTime"
                >
                    <DatePicker
                        format={FORMAT.DISPLAY.DAILY}
                        style={{ width: '100%' }}
                    />
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsIssueNumber.DataTable.Title.EndTime"
                            description="封盘时间"
                        />
                    }
                >
                    <DateWithFormat time={record.EndTime} />
                </Form.Item>
            </Form>
        </Drawer>
    )
}
