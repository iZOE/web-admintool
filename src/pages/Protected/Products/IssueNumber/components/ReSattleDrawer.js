import React, { useState } from 'react';
import {
  Button,
  Statistic,
  Row,
  Col,
  Drawer,
  Input,
  Table,
  Form,
  message
} from 'antd';
import { useIntl, FormattedMessage } from 'react-intl';
import caller from 'utils/fetcher';
import { LAYOUT_CONFIG } from './layoutConfig';

export default function ReSattleDrawer({ modalData, setModalData }) {
  const { record, type } = modalData;
  const [bonusResult, setBonusResult] = useState();
  const intl = useIntl();
  const [form] = Form.useForm();

  const closeModal = () =>
    setModalData({
      visible: false
    });

  if (!record) {
    return null;
  }

  const { LotteryHistoryId } = record;

  const messageWarning = () =>
    message.warning(
      intl.formatMessage({
        id: 'Share.ErrorMessage.UnknownError',
        description: '不明错误'
      }),
      10
    );

  /**
   * 重新開獎
   * @param {IssueNumber}
   */
  function onSettle() {
    caller({
      method: 'patch',
      endpoint: `/api/IssueNumber/${LotteryHistoryId}/Settle`,
      body: {
        BonusNumber: form.getFieldValue('BonusNumber')
      }
    })
      .then(() => {
        message.success(
          intl.formatMessage({
            id: 'Share.UIResponse.SaveSuccess',
            description: '编辑成功'
          }),
          10
        );
        closeModal();
      })
      .catch(() => {
        messageWarning();
      });
  }

  /**
   * 試算
   * @param {IssueNumber}
   */
  function calculationBonus(values) {
    const { BonusNumber } = values;
    caller({
      method: 'post',
      endpoint: `/api/IssueNumber/${LotteryHistoryId}/TrailCalculationBonus`,
      body: {
        BonusNumber
      }
    })
      .then(res => {
        setBonusResult(res.Data);
      })
      .catch(err => {
        messageWarning();
      });
  }

  const columns = [
    {
      title: (
        <FormattedMessage
          id="PageProductsIssueNumber.DataTable.Title.TotalBet"
          description="總投注數"
        />
      ),
      dataIndex: 'TotalBet',
      key: 'TotalBet'
    },
    {
      title: (
        <FormattedMessage
          id="PageProductsIssueNumber.DataTable.Title.BonusTotalBet"
          description="中獎注數"
        />
      ),
      dataIndex: 'BonusTotalBet',
      key: 'BonusTotalBet'
    },
    {
      title: (
        <FormattedMessage
          id="PageProductsIssueNumber.DataTable.Title.BonusAmount"
          description="中奖奖金"
        />
      ),
      dataIndex: 'BonusAmount',
      key: 'BonusAmount'
    }
  ];

  return (
    <Drawer
      title={
        <FormattedMessage id={`PageProductsIssueNumber.ActionButton.${type}`} />
      }
      width={600}
      onClose={() => closeModal()}
      visible={modalData.visible}
      footer={
        <div
          style={{
            textAlign: 'right'
          }}
        >
          <Button onClick={() => closeModal()} style={{ marginRight: 8 }}>
            <FormattedMessage
              id="Share.ActionButton.Cancel"
              description="取消"
            />
          </Button>
          <Button onClick={() => onSettle()} type="primary">
            <FormattedMessage
              id="PageProductsIssueNumber.ActionButton.SettleConfirm"
              description="确认及结算"
            />
          </Button>
        </div>
      }
    >
      <Form
        {...LAYOUT_CONFIG}
        name="basic"
        form={form}
        initialValues={record}
        onFinish={calculationBonus}
      >
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsIssueNumber.DataTable.Title.LotteryName"
              description="彩種"
            />
          }
        >
          {record.LotteryName}
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsIssueNumber.DataTable.Title.IssueNumber"
              description="期號"
            />
          }
        >
          {record.IssueNumber}
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsIssueNumber.DataTable.Title.BonusNumber"
              description="開獎號碼"
            />
          }
        >
          {record.BonusNumber}
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsIssueNumber.DataTable.Title.NewBonusNumber"
              description="開獎結果"
            />
          }
          name="BonusNumber"
        >
          <Row gutter={24}>
            <Col span={20}>
              <Input />
            </Col>
            <Col span={4}>
              <Button type="primary" htmlType="submit">
                <FormattedMessage id="PageProductsIssueNumber.ActionButton.Calculate" />
              </Button>
            </Col>
          </Row>
        </Form.Item>
        <Row gutter={16}>
          <Col span={24}>
            <Table
              columns={columns}
              ellipsis={true}
              width="100%"
              dataSource={
                bonusResult && [
                  {
                    key: '1',
                    TotalBet: bonusResult.TotalBet,
                    BonusTotalBet: bonusResult.BonusTotalBet,
                    BonusAmount: (
                      <Statistic
                        value={bonusResult.BonusAmount}
                        precision={2}
                      />
                    )
                  }
                ]
              }
              pagination={false}
            />
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
}
