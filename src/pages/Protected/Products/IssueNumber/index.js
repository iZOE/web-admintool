import React, { useState, createContext } from 'react';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import { FULL_SCREENABLE_AREA_WRAPPER_ID } from 'constants/forList';
import { FormattedMessage } from 'react-intl';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import EditDrawer from './components/EditDrawer';
import ReSattleDrawer from './components/ReSattleDrawer';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

const { PRODUCTS_ISSUE_NUMBER_VIEW } = PERMISSION;

export const PageContext = createContext();

const PageView = () => {
  const [editDrawer, setEditDrawer] = useState({
    visible: false,
    record: null
  });
  const [reSattleDrawer, setReSattleDrawer] = useState({
    visible: false,
    record: null
  });

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    onReady,
    boundedMutate
  } = useGetDataSourceWithSWR({
    url: '/api/IssueNumber/Search',
    defaultSortKey: 'AnnounceTime',
    autoFetch: true
  });

  return (
    <PageContext.Provider
      value={{
        condition,
        dataSource,
        setEditDrawer,
        setReSattleDrawer,
        boundedMutate
      }}
    >
      <Permission functionIds={[PRODUCTS_ISSUE_NUMBER_VIEW]} isPage>
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢結果"
                />
              }
              rowKey={record => record.LotteryHistoryId}
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource && dataSource.Data}
              total={dataSource && dataSource.TotalCount}
              onUpdate={onUpdateCondition}
            />
          }
        />
        <EditDrawer
          modalData={editDrawer}
          setModalData={data => setEditDrawer(data)}
        />
        <ReSattleDrawer
          modalData={reSattleDrawer}
          setModalData={data => setReSattleDrawer(data)}
        />
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
