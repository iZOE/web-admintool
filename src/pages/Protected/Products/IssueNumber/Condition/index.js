import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, Select, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { getISODateTimeString } from 'mixins/dateTime'
import LotteryItems from 'components/FormItems/LotteryItems'
import QueryButton from 'components/FormActionButtons/Query'

const { Option } = Select
const { RangePicker } = DatePicker

const DATE_TYPE_IDS = [1, 2, 3, 4]
const STATUS_TYPE_IDS = [1, 2, 3, 4, 5, 6]

const CONDITION_INITIAL_VALUE = {
    RangeTypeBy: 4,
    Date: DEFAULT.RANGE.FROM_TODAY_TO_TODAY,
    Status: null,
    IssueNumber: null,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = Form.useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            RangeStartDate: getISODateTimeString(start),
            RangeEndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={12}>
                    <Input.Group compact>
                        <Form.Item name="RangeTypeBy" noStyle>
                            <Select
                                className="option"
                                style={{ width: '100px' }}
                            >
                                {DATE_TYPE_IDS.map(id => (
                                    <Option value={id} key={id}>
                                        <FormattedMessage
                                            id={`PageProductsIssueNumber.QueryCondition.SelectItem.RangeTypeBy.${id}`}
                                        />
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item name="Date" noStyle>
                            <RangePicker
                                showTime
                                style={{ width: 'calc(100% - 100px)' }}
                                format={FORMAT.DISPLAY.DEFAULT}
                            />
                        </Form.Item>
                    </Input.Group>
                </Col>
                <Col sm={6}>
                    <LotteryItems needAll />
                </Col>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageProductsIssueNumber.QueryCondition.Status" />
                        }
                        name="Status"
                    >
                        <Select>
                            <Option value={null}>
                                {<FormattedMessage id="Share.Dropdown.All" />}
                            </Option>
                            {STATUS_TYPE_IDS.map(id => (
                                <Option value={id} key={id}>
                                    <FormattedMessage
                                        id={`PageProductsIssueNumber.QueryCondition.SelectItem.StatusType.${id}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageProductsIssueNumber.QueryCondition.IssueNumber" />
                        }
                        name="IssueNumber"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={{ span: 4, offset: 12 }} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
