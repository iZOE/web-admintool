import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ButtonItems from './Rows/ButtonItems';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.LotteryName" description="彩種" />,
    dataIndex: 'LotteryCode',
    key: 'LotteryCode',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => record.LotteryName,
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.IssueNumber" description="期號" />,
    dataIndex: 'IssueNumber',
    key: 'IssueNumber',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.AnnounceTime" description="開獎時間" />,
    dataIndex: 'AnnounceTime',
    key: 'AnnounceTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { AnnounceTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.BonusNumber" description="開獎號碼" />,
    dataIndex: 'BonusNumber',
    key: 'BonusNumber',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.TotalBet" description="總投注數" />,
    dataIndex: 'TotalBet',
    key: 'TotalBet',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.BonusTotalBet" description="中獎注數" />,
    dataIndex: 'BonusTotalBet',
    key: 'BonusTotalBet',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.StartTime" description="開盤時間" />,
    dataIndex: 'StartTime',
    key: 'StartTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { StartTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.EndTime" description="封盤時間" />,
    dataIndex: 'EndTime',
    key: 'EndTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { EndTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => record.StatusText,
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyDate',
    key: 'ModifyDate',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyDate: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsIssueNumber.DataTable.Title.Action" description="管理" />,
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) => <ButtonItems record={record} />,
  },
];
