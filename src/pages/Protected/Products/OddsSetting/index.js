import React, { useState, createContext } from 'react'
import { Tabs } from 'antd'
import { FormattedMessage } from 'react-intl'
import PageHeaderTab from 'components/PageHeaderTab'
import BetRule from './BetRule'
import BetSection from './BetSection'

export const PageContext = createContext()

const PageView = () => {
    const { TabPane } = Tabs
    const [betDataType, setBetDataType] = useState(0) //查詢類型 0:玩法賠率 1:投注賠率

    const [betSectionDrawer, setBetSectionDrawer] = useState({
        visible: false,
        record: null,
    })

    const [betRuleDrawer, setBetRuleDrawer] = useState({
        visible: false,
        record: null,
    })

    return (
        <PageContext.Provider
            value={{
                betDataType,
                setBetDataType,
                betSectionDrawer,
                setBetSectionDrawer,
                betRuleDrawer,
                setBetRuleDrawer,
            }}
        >
            <PageHeaderTab
                defaultActiveKey="betSection"
                onChange={tabKey =>
                    setBetDataType(tabKey === 'betSection' ? 0 : 1)
                }
            >
                <TabPane
                    tab={
                        <FormattedMessage id="PageProductsOddsSetting.QueryList.Type.0" />
                    }
                    key="betSection"
                />
                <TabPane
                    tab={
                        <FormattedMessage id="PageProductsOddsSetting.QueryList.Type.1" />
                    }
                    key="betRule"
                />
            </PageHeaderTab>
            {betDataType === 0 ? <BetSection /> : <BetRule />}
        </PageContext.Provider>
    )
}

export default PageView
