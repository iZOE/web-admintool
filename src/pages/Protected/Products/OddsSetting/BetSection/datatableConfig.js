import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ActionButtons from './ActionButtons';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Product" description="彩种" />,
    dataIndex: 'LotteryName',
    key: 'LotteryName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Type" description="类型" />,
    dataIndex: 'PageName',
    key: 'PageName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.BetType" description="玩法" />,
    dataIndex: 'BetTypeName',
    key: 'BetTypeName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Mode" description="赔率模式" />,
    dataIndex: 'BetSectionId',
    key: 'BetSectionId',
    isShow: true,
    render: text => <FormattedMessage id={`PageProductsOddsSetting.ColumnList.Mode.${text}`} />,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Status" description="状态" />,
    dataIndex: 'IsEnabled',
    key: 'IsEnabled',
    isShow: true,
    render: text => {
      return <FormattedMessage id={`PageProductsOddsSetting.ColumnList.Status.${text}`} description="状态" />;
    },
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Editor" description="管理者" />,
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
    isShow: true,
  },
  {
    title: '',
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (_1, record, _2) => <ActionButtons record={record} />,
  },
];
