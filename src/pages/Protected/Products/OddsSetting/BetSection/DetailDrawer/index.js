import React from "react";
import { Button, Drawer, Form, message, Select } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import caller from "utils/fetcher";

export default function ModalButton({
  modalData,
  setModalData,
  boundedMutate,
}) {
  const intl = useIntl();
  const [form] = Form.useForm();
  const { record } = modalData;
  const { Option } = Select;
  const MODE_OPTION = [0, 1];

  const closeModal = () => {
    form.resetFields();
    setModalData({ visible: false });
  };

  if (!record) {
    return null;
  }

  /**
   * 编辑
   */
  function onSend(values) {
    if (record.Mode === values.Mode) {
      console.log("the same values");
      closeModal();
      return null;
    }
    const result = {};
    result.Mode = values.Mode;
    caller({
      method: "put",
      endpoint: `/api/BetSection/${record.BetTypeId}`,
      body: {
        BetSectionId: result.Mode,
        BetTypeId: record.BetTypeId,
      },
    }).then((res) => {
      if (res.Status) {
        message.success(
          intl.formatMessage({
            id: "Share.UIResponse.SaveSuccess",
            description: "编辑成功",
          }),
          10
        );
        boundedMutate();
        closeModal();
      } else {
        message.warning(
          intl.formatMessage({
            id: `Share.ErrorMessage.ErrorKey.${res.ErrorKey}`,
            description: "错误讯息",
          }),
          10
        );
      }
    });
  }

  return (
    <Drawer
      title={<FormattedMessage id="Share.ActionButton.Edit" />}
      width={500}
      onClose={() => closeModal()}
      visible={modalData.visible}
      footer={
        <div
          style={{
            textAlign: "right",
          }}
        >
          <Button onClick={() => closeModal()} style={{ marginRight: 8 }}>
            <FormattedMessage id="Share.ActionButton.Cancel" />
          </Button>
          <Button type="primary" onClick={() => form.submit()}>
            <FormattedMessage id="Share.ActionButton.Confirm" />
          </Button>
        </div>
      }
    >
      <Form
        name="basic"
        form={form}
        onFinish={(values) => onSend(values)}
        initialValues={record}
      >
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsOddsSetting.ColumnName.Product"
              description="彩种"
            />
          }
        >
          {record.LotteryName}
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsOddsSetting.ColumnName.Type"
              description="类型"
            />
          }
        >
          {record.PageName}
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsOddsSetting.ColumnName.BetType"
              description="玩法"
            />
          }
        >
          {record.BetTypeName}
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsOddsSetting.ColumnName.Status"
              description="状态"
            />
          }
        >
          <FormattedMessage
            id={`PageProductsOddsSetting.ColumnList.Status.${record.IsEnabled}`}
            description="状态"
          />
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage
              id="PageProductsOddsSetting.ColumnName.Mode"
              description="赔率模式"
            />
          }
          name="Mode"
        >
          <Select
            defaultValue={record.BetSectionId}
            initialvalues={record.BetSectionId}
            style={{ width: "40%" }}
          >
            {MODE_OPTION.map((id) => (
              <Option value={id} key={id}>
                <FormattedMessage
                  id={`PageProductsOddsSetting.ColumnList.Mode.${id}`}
                />
              </Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Drawer>
  );
}
