import React, { useContext } from 'react';
import { FormattedMessage } from 'react-intl';
import Permission from 'components/Permission';
import * as PERMISSION from 'constants/permissions';
import { Button } from 'antd';
import { PageContext } from '../../index';

const { PRODUCTS_ODDS_SETTING_EDIT } = PERMISSION;

export default function ActionButtons({ record }) {
  const { setBetSectionDrawer } = useContext(PageContext);

  return (
    <Permission functionIds={[PRODUCTS_ODDS_SETTING_EDIT]}>
      <Button
        type="link"
        onClick={() => setBetSectionDrawer({ visible: true, record })}
      >
        <FormattedMessage id="Share.ActionButton.Edit" />
      </Button>
    </Permission>
  );
}
