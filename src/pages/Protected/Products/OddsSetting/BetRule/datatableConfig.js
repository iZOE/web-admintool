import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import ActionButtons from './ActionButtons';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Product" description="彩种名称" />,
    dataIndex: 'LotteryName',
    key: 'LotteryName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Type" description="类型" />,
    dataIndex: 'PageName',
    key: 'PageName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.BetType" description="玩法" />,
    dataIndex: 'BetTypeName',
    key: 'BetTypeName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.BetSubTypeName" description="玩法子类" />,
    dataIndex: 'BetSubTypeName',
    key: 'BetSubTypeName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.BetRuleId" description="规则 ID" />,
    dataIndex: 'BetRuleId',
    key: 'BetRuleId',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.BetRuleName" description="规则名称" />,
    dataIndex: 'BetRuleName',
    key: 'BetRuleName',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Modulus" description="机率" />,
    dataIndex: 'Modulus',
    key: 'Modulus',
    isShow: true,
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.MaxBonusMode" description="最大赔率" />,
    dataIndex: 'MaxBonusMode',
    key: 'MaxBonusMode',
    isShow: true,
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Status" description="状态" />,
    dataIndex: 'IsEnabled',
    key: 'IsEnabled',
    isShow: true,
    render: (_1, record, _2) => (
      <FormattedMessage id={`PageProductsOddsSetting.ColumnList.Status.${record.IsEnabled}`} description="状态" />
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageProductsOddsSetting.ColumnName.Editor" description="管理者" />,
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
    render: text => (text ? text : NO_DATA),
  },
  {
    title: '',
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (_1, record, _2) => <ActionButtons record={record} />,
  },
];
