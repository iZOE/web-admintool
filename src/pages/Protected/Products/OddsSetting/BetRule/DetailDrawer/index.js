import React, { useEffect } from 'react'
import { Button, Drawer, Form, message, Input } from 'antd'
import { useIntl, FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'

export default function ModalButton({
    modalData,
    setModalData,
    boundedMutate,
}) {
    const intl = useIntl()
    const [form] = Form.useForm()
    const { record } = modalData

    const closeModal = () => {
        form.resetFields()
        setModalData({
            visible: false,
        })
    }

    useEffect(() => {
        form.setFieldsValue({
            ...record,
        })
    }, [record])

    if (!record) {
        return null
    }

    /**
     * 編輯
     */
    function onSend(values) {
        if (
            record.MaxBonusMode === values.MaxBonusMode &&
            record.Modulus === values.Modulus
        ) {
            closeModal()
            return null
        }
        const result = {}
        result.MaxBonusMode = values.MaxBonusMode || null
        result.Modulus = values.Modulus || null
        caller({
            method: 'put',
            endpoint: `/api/BetRule/${record.BetRuleId}`,
            body: {
                BetRuleId: record.BetRuleId,
                MaxBonusMode: result.MaxBonusMode,
                Modulus: result.Modulus,
            },
        }).then(res => {
            if (res.Status) {
                message.success(
                    intl.formatMessage({
                        id: 'Share.UIResponse.SaveSuccess',
                        description: '编辑成功',
                    }),
                    10
                )
                boundedMutate()
                closeModal()
            } else {
                message.warning(
                    intl.formatMessage({
                        id: `Share.ErrorMessage.ErrorKey.${res.ErrorKey}`,
                        description: '錯誤訊息',
                    }),
                    10
                )
            }
        })
    }

    return (
        <Drawer
            title={<FormattedMessage id="Share.ActionButton.Edit" />}
            width={500}
            onClose={() => closeModal()}
            visible={modalData.visible}
            footer={
                <div
                    style={{
                        textAlign: 'right',
                    }}
                >
                    <Button
                        onClick={() => closeModal()}
                        style={{ marginRight: 8 }}
                    >
                        <FormattedMessage id="Share.ActionButton.Cancel" />
                    </Button>
                    <Button type="primary" onClick={() => form.submit()}>
                        <FormattedMessage id="Share.ActionButton.Confirm" />
                    </Button>
                </div>
            }
        >
            <Form
                name="basic"
                form={form}
                onFinish={values => onSend(values)}
                initialValues={record}
            >
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.Product"
                            description="彩種名稱"
                        />
                    }
                >
                    {record.LotteryName}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.Type"
                            description="類型"
                        />
                    }
                >
                    {record.PageName}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.BetType"
                            description="玩法"
                        />
                    }
                >
                    {record.BetTypeName}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.BetSubTypeName"
                            description="玩法子類"
                        />
                    }
                >
                    {record.BetSubTypeName}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.BetRuleId"
                            description="規則 ID"
                        />
                    }
                >
                    {record.BetRuleId}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.BetRuleName"
                            description="規則名稱"
                        />
                    }
                >
                    {record.BetRuleName}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.Modulus"
                            description="機率"
                        />
                    }
                    name="Modulus"
                >
                    <Input value={record.Modulus} />
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.MaxBonusMode"
                            description="最大賠率"
                        />
                    }
                    name="MaxBonusMode"
                >
                    <Input value={record.MaxBonusMode} />
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PageProductsOddsSetting.ColumnName.Status"
                            description="狀態"
                        />
                    }
                >
                    <FormattedMessage
                        id={`PageProductsOddsSetting.ColumnList.Status.${record.IsEnabled}`}
                        description="狀態"
                    />
                </Form.Item>
            </Form>
        </Drawer>
    )
}
