import React, { useContext } from 'react';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import { FormattedMessage } from 'react-intl';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import DetailDrawer from './DetailDrawer';
import Condition from '../Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import { PageContext } from '../index';

const { PRODUCTS_ODDS_SETTING_VIEW } = PERMISSION;

const PageView = () => {
  const { fetching, dataSource, onUpdateCondition, condition, boundedMutate } = useGetDataSourceWithSWR({
    url: '/api/BetRule/Search',
    defaultSortKey: 'ModifyTime',
  });

  const { betRuleDrawer, setBetRuleDrawer } = useContext(PageContext);

  return (
    <Permission functionIds={[PRODUCTS_ODDS_SETTING_VIEW]} isPage>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={<Condition onUpdate={onUpdateCondition.bySearch} />}
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            title={<FormattedMessage id="Share.Table.SearchResult" description="查詢結果" />}
            config={COLUMNS_CONFIG}
            loading={fetching}
            dataSource={dataSource && dataSource.Data}
            total={dataSource && dataSource.TotalCount}
            rowKey={record => record.BetRuleId}
            onUpdate={onUpdateCondition}
          />
        }
      />
      <DetailDrawer
        modalData={betRuleDrawer}
        boundedMutate={boundedMutate}
        setModalData={data => setBetRuleDrawer(data)}
      />
    </Permission>
  );
};

export default PageView;
