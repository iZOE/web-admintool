import React, { useEffect, useState } from 'react';
import { Form, Select } from 'antd';
import { FormattedMessage } from 'react-intl';
import optionsService from 'services/optionsServices';

const { getBetSubTypes } = optionsService();

function BetSubTypes({ ids, form }) {
  const [betSubTypeId, setBetSubTypeId] = useState(null);
  const { Option } = Select;

  useEffect(() => {
    ids
      ? getBetSubTypes(ids).then(res => setBetSubTypeId(res))
      : setBetSubTypeId(null);
  }, [ids]);

  return (
    <Form.Item
      label={
        <FormattedMessage id="PageProductsOddsSetting.QueryCondition.BetSubTypes" />
      }
      name="BetSubTypeId"
      form={form}
    >
      <Select>
        <Option value={null}>
          {<FormattedMessage id="Share.Dropdown.All" />}
        </Option>
        {betSubTypeId &&
          betSubTypeId.map(d => (
            <Option value={d.Id} key={d.Id}>
              {d.Name}
            </Option>
          ))}
      </Select>
    </Form.Item>
  );
}

export default BetSubTypes;
