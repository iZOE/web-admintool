import React, { useEffect, useState } from 'react';
import { Form, Select } from 'antd';
import { FormattedMessage } from 'react-intl';
import optionsService from 'services/optionsServices';

const { getBetTypes } = optionsService();

function BetTypes({ ids, form }) {
  const [betTypeId, setBetTypeId] = useState(null);
  const { Option } = Select;

  useEffect(() => {
    console.log(ids);
    ids ? getBetTypes(ids).then(res => setBetTypeId(res)) : setBetTypeId(null);
  }, [ids]);

  return (
    <Form.Item
      label={
        <FormattedMessage id="PageProductsOddsSetting.QueryCondition.BetTypes" />
      }
      form={form}
      name="BetTypeId"
    >
      <Select>
        <Option value={null}>
          {<FormattedMessage id="Share.Dropdown.All" />}
        </Option>
        {betTypeId &&
          betTypeId.map(d => (
            <Option value={d.Id} key={d.Id}>
              {d.Name}
            </Option>
          ))}
      </Select>
    </Form.Item>
  );
}

export default BetTypes;
