import React, { useContext } from 'react'
import { Form, Row, Col, Select } from 'antd'
import { FormattedMessage } from 'react-intl'
import { PageContext } from '../index'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemLotteryItems from 'components/FormItems/LotteryItems'
import BetTypes from './FormItems/BetTypes'
import BetSubTypes from './FormItems/BetSubTypes'

const { useForm } = Form
const STATUS_TYPE_IDS = [true, false]
const CONDITION_INITIAL_VALUE = {
    BetTypeId: null,
    IsEnabled: null,
    LotteryCode: 1,
}

function Condition({ onUpdate }) {
    const [form] = useForm()
    const { Option } = Select
    const { betDataType } = useContext(PageContext)

    return (
        <Form
            form={form}
            onFinish={onUpdate}
            initialValues={
                betDataType !== 1
                    ? CONDITION_INITIAL_VALUE
                    : {
                          ...CONDITION_INITIAL_VALUE,
                          BetSubTypeId: null,
                      }
            }
            onValuesChange={changedValues => {
                if (changedValues.LotteryCode) {
                    form.setFieldsValue({
                        BetTypeId: null,
                        BetSubTypeId: null,
                    })
                }
                if (changedValues.LotteryCode === null) {
                    form.setFieldsValue({
                        LotteryCode: null,
                        BetTypeId: null,
                        BetSubTypeId: null,
                    })
                }
                if (
                    changedValues.BetTypeId ||
                    changedValues.BetTypeId === null
                ) {
                    form.setFieldsValue({
                        BetSubTypeId: null,
                    })
                }
            }}
        >
            <Row gutter={[16, 8]}>
                {/* 彩種 */}
                <Col sm={4}>
                    <FormItemLotteryItems />
                </Col>
                {/* 玩法 */}
                <Col sm={4}>
                    <Form.Item shouldUpdate noStyle>
                        {({ getFieldValue }) => (
                            <BetTypes
                                ids={getFieldValue('LotteryCode')}
                                form={form}
                            />
                        )}
                    </Form.Item>
                </Col>
                {/* 子玩法 */}
                {betDataType === 1 && (
                    <Col sm={4}>
                        <Form.Item shouldUpdate noStyle>
                            {({ getFieldValue }) => (
                                <BetSubTypes
                                    ids={getFieldValue('BetTypeId')}
                                    form={form}
                                />
                            )}
                        </Form.Item>
                    </Col>
                )}
                {/* 狀態 */}
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageProductsOddsSetting.QueryCondition.Status" />
                        }
                        name="IsEnabled"
                    >
                        <Select>
                            <Option value={null}>
                                <FormattedMessage id="Share.Dropdown.All" />
                            </Option>
                            {STATUS_TYPE_IDS.map(id => (
                                <Option value={id} key={id}>
                                    <FormattedMessage
                                        id={`PageProductsOddsSetting.QueryList.Status.${id}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                {/* 查詢按鈕 */}
                <Col sm={betDataType === 1 ? 8 : 12} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
