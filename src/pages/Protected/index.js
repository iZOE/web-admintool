import React, { Suspense, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Skeleton } from 'antd';
import useSWR from 'swr';
import axios from 'axios';
import { useRecoilState } from 'recoil';
import { permissionState } from 'recoilsystem/atoms';

import ContextProviders from 'components/ContextProviders';
import BreadCrumbsContextProvider from 'contexts/BreadcrumbsContext';
import MemberDetailContextProvider from 'contexts/MemberDetailContext';
import GroupDetailContextProvider from 'contexts/GroupDetailContext';
import BasicLayout from '../../layouts/Basic';

const Dashboard = React.lazy(() => import('./Dashboard'));
const LobbyConfig = React.lazy(() => import('./LobbyConfig'));

// 系統管理 - 功能設定
const SettingsMenus = React.lazy(() => import('./System/Menus'));
// 系統管理 - 群組管理
const SettingsGroups = React.lazy(() => import('./System/Groups'));
// 系統管理 - 使用者管理
const SettingsUsers = React.lazy(() => import('./System/Users'));

// 全站管理 - 會員等級管理
const MembersOfMemberLevels = React.lazy(() => import('./AllSite/MemberLevels'));
// 全站管理 - 簡訊驗證碼
const SmsVerification = React.lazy(() => import('./AllSite/SmsVerification'));
// 全站管理 - 站台設定
const SiteSettingAsync = React.lazy(() => import('./AllSite/SiteSetting'));

// 帳號管理 - 一般會員管理
const MembersOfMembers = React.lazy(() => import('./Members/Members'));
// 帳號管理 - 代理商管理
const Affiliates = React.lazy(() => import('./Members/Affiliates'));
// 帳號管理 - 會員日誌
const LogsOfMembers = React.lazy(() => import('./Members/Logs'));

// 代理商管理 - 分紅報表
const Dividend = React.lazy(() => import('./Affiliates/Dividend'));
// 代理商管理 - 日工資
const DailyWages = React.lazy(() => import('./Affiliates/DailyWages'));
// 代理商管理 - 契约日工资报表
const ContractDailyWages = React.lazy(() => import('./Affiliates/ContractDailyWages'));
// 代理商管理 - 契约日分红报表
const ContractDailyDividends = React.lazy(() => import('./Affiliates/ContractDailyDividends'));
// 代理商管理 - 契约月分红报表
const ContractMonthlyDividends = React.lazy(() => import('./Affiliates/ContractMonthlyDividends'));
// 代理商管理 - 契约工资 / 分红管理
const ContractManagement = React.lazy(() => import('./Affiliates/ContractManagement'));
// 代理商管理 - 全民代报表
const AgencyReport = React.lazy(() => import('./Affiliates/AgencyReport'));
// 代理商管理 - 一级分红报表
const FirstLevelDividend = React.lazy(() => import('./Affiliates/FirstLevelDividend'));

// 出入款管理 - 支付设定;
const PaymentSettingOfPayment = React.lazy(() => import('./DepositWithdraw/PaymentSetting'));
// 出入款管理 - 充值清单;
const DepositListPage = React.lazy(() => import('./DepositWithdraw/Deposit'));
// 出入款管理 - 流水审核;
const TurnoverPage = React.lazy(() => import('./DepositWithdraw/Turnover'));
// 出入款管理 - 提现处理;
const WithdrawPage = React.lazy(() => import('./DepositWithdraw/Withdraw'));
// 出入款管理 - 快速充值;
const FastDepositPage = React.lazy(() => import('./DepositWithdraw/FastDeposit'));
// 出入款管理 - 快速充值作业设定;
const QuickDepositSettingOfPayment = React.lazy(() => import('./DepositWithdraw/QuickDepositSetting'));
// 出入款管理 - 第三方转账管理;
const TransferManagementPage = React.lazy(() => import('./DepositWithdraw/TransferManagement'));
// 出入款管理 - 帐变纪录;
const TransactionOfLog = React.lazy(() => import('./DepositWithdraw/Transaction'));
// 出入款管理 - 会员转帐纪录;
const MemberTransferOfLog = React.lazy(() => import('./DepositWithdraw/MemberTransfer'));
// 出入款管理 - 虚拟币提现设定;
const CryptoCurrencyPage = React.lazy(() => import('./DepositWithdraw/CryptoCurrency'));

// 游戏纪录 - 彩票投注纪录
const BetOfProducts = React.lazy(() => import('./Log/LotteryBet'));
// 游戏纪录 - 彩票追号纪录
const TraceOfProducts = React.lazy(() => import('./Log/Trace'));
// 游戏纪录 - 第三方投注记录
const BetOfLog = React.lazy(() => import('./Log/ThirdpartyBet'));
// 游戏纪录 - 彩票投注记录历史
const LotteryBetHistory = React.lazy(() => import('./Log/LotteryBetHistory'));

// 游戏管理 - 彩票管理
const OddsLottery = React.lazy(() => import('./Products/Lottery'));
// 游戏管理 - 期数管理
const IssueNumber = React.lazy(() => import('./Products/IssueNumber'));
// 游戏管理 - 赔率设定
const OddsSetting = React.lazy(() => import('./Products/OddsSetting'));
// 游戏管理 - 第三方管理
const ThirdParty = React.lazy(() => import('./Products/ThirdParty'));

// 营运管理 - 公告设定
const NewsOfOperation = React.lazy(() => import('./Operation/News'));
// 营运管理 - 轮播图管理
const OperationBanner = React.lazy(() => import('./Operation/Banner'));
// 营运管理 - 版型设置
const OperationTemplate = React.lazy(() => import('./Operation/Template'));
// 营运管理 - 全民代推广图管理
const PromotionManagement = React.lazy(() => import('./Operation/PromotionManagement'));

// 活动管理 - 充值优惠活动设定
const PreferentialSettingOfPayment = React.lazy(() => import('./Activity/PreferentialSetting'));
// 活动管理 - 活动类别管理
const ActiveTypes = React.lazy(() => import('./Activity/ActivityTypeManagement'));
// 活动管理 - 活动管理
const Actives = React.lazy(() => import('./Activity/ActivityManagement'));
// 活动管理 - 活动报表
const ActivityReport = React.lazy(() => import('./Activity/ActivityReport'));
// 活动管理 - 活动报表詳情
const ActivityReportDetail = React.lazy(() => import('./Activity/ActivityReportDetail'));
// 活动管理 - 红包发送管理
const OperationRedEnvelope = React.lazy(() => import('./Activity/RedEnvelope'));
// 活动管理 - 生息宝报表
const ShengXiBao = React.lazy(() => import('./Activity/ShengXiBao'));

// 营运报表 - 代理报表
const AffiliateOfOperationReport = React.lazy(() => import('./OperationReport/Affiliate'));
// 营运报表 - 会员报表
const MemberOfOperationReport = React.lazy(() => import('./OperationReport/Member'));
// 营运报表 - 平台报表
const AgentOfOperationReport = React.lazy(() => import('./OperationReport/Agent'));
// 营运报表 - 余额统计
const BalanceStatistics = React.lazy(() => import('./OperationReport/BalanceStatistics'));
// 营运报表 - 盈亏排行
const RankOfReport = React.lazy(() => import('./OperationReport/Rank'));

// 第三方注單詳情 - 真人
const ThirdpartyBettingReality = React.lazy(() => import('./ThirdpartyBetting/Reality'));
// 第三方注單詳情 - 棋牌
const ThirdpartyBettingChess = React.lazy(() => import('./ThirdpartyBetting/Chess'));
// 第三方注單詳情 - 體育
const ThirdpartyBettingSports = React.lazy(() => import('./ThirdpartyBetting/Sports'));
// 第三方注單詳情 - 電子
const ThirdpartyBettingEgame = React.lazy(() => import('./ThirdpartyBetting/Egame'));
// 第三方注單詳情 - 捕魚
const ThirdpartyBettingFishing = React.lazy(() => import('./ThirdpartyBetting/Fishing'));
// 第三方注單詳情 - 彩票
const ThirdpartyBettingLottery = React.lazy(() => import('./ThirdpartyBetting/Lottery'));

const UnhandledRoute = React.lazy(() => import('../UnhandledRoute'));

export default function Protected() {
  const [_, setPermission] = useRecoilState(permissionState);
  const { data: permissionData } = useSWR(['/api/User/FunctionIds', '/api/menu/functionId'], (userFuIds, allFuIds) =>
    axios.all([axios.get(userFuIds), axios.get(allFuIds)]).then(res => {
      const userFuIds = res[0].data.Data;
      const allFuIds = res[1].data.Data;
      const userFnIdsSet = new Set(userFuIds);
      return new Map(Object.keys(allFuIds).map(key => [allFuIds[key], userFnIdsSet.has(allFuIds[key])]));
    })
  );

  useEffect(() => {
    if (permissionData) {
      setPermission(permissionData);
    }
  }, [permissionData]);

  return (
    <ContextProviders providers={[MemberDetailContextProvider, GroupDetailContextProvider, BreadCrumbsContextProvider]}>
      <Router>
        <BasicLayout>
          <Suspense fallback={<Skeleton active />}>
            <Switch>
              <Route exact path="/">
                <Dashboard />
              </Route>
              {/* 系統管理 - 功能設定 */}
              <Route path="/settings/menus">
                <SettingsMenus />
              </Route>
              {/* 系統管理 - 群組管理 */}
              <Route path="/settings/groups">
                <SettingsGroups />
              </Route>
              {/* 系統管理 - 使用者管理 */}
              <Route path="/settings/users">
                <SettingsUsers />
              </Route>

              {/* 全站管理 - 會員等級管理 */}
              <Route path="/all-site/member-levels">
                <MembersOfMemberLevels />
              </Route>
              {/* 全站管理 - 簡訊驗證碼 */}
              <Route path="/all-site/sms-verification">
                <SmsVerification />
              </Route>
              {/* 全站管理 - 站台設定 */}
              <Route path="/all-site/site-setting">
                <SiteSettingAsync />
              </Route>

              {/* 帳號管理 - 一般會員管理 */}
              <Route path="/members/members">
                <MembersOfMembers />
              </Route>
              {/* 帳號管理 - 代理商管理 */}
              <Route path="/members/affiliates">
                <Affiliates />
              </Route>
              {/* 帳號管理 - 會員日誌 */}
              <Route path="/members/logs">
                <LogsOfMembers></LogsOfMembers>
              </Route>

              {/* 代理商管理 - 分紅報表 */}
              <Route path="/affiliates/dividends">
                <Dividend />
              </Route>
              {/* 代理商管理 - 日工資/扶持獎金 */}
              <Route path="/affiliates/daily-wages">
                <DailyWages />
              </Route>
              {/* 代理商管理 - 契约日工资报表 */}
              <Route path="/affiliates/contract-daily-wages">
                <ContractDailyWages />
              </Route>
              {/* 代理商管理 - 契约日分红报表 */}
              <Route path="/affiliates/contract-daily-dividends">
                <ContractDailyDividends />
              </Route>
              {/* 代理商管理 - 契约月分红报表 */}
              <Route path="/affiliates/contract-monthly-dividends">
                <ContractMonthlyDividends />
              </Route>
              {/* 代理商管理 - 契约工资 / 分红管理 */}
              <Route path="/affiliates/contract-management">
                <ContractManagement />
              </Route>
              {/* 代理商管理 - 全民代报表 */}
              <Route path="/affiliates/agency-report">
                <AgencyReport />
              </Route>
              {/* 代理商管理 - 一级分红报表 */}
              <Route path="/affiliates/first-level-dividend">
                <FirstLevelDividend />
              </Route>

              {/* 出入款管理 - 支付设定 */}
              <Route path="/deposit-withdraw/payment-setting">
                <PaymentSettingOfPayment />
              </Route>
              {/* 出入款管理 - 充值清单 */}
              <Route path="/deposit-withdraw/deposit">
                <DepositListPage />
              </Route>
              {/* 出入款管理 - 流水审核 */}
              <Route path="/deposit-withdraw/turnover">
                <TurnoverPage />
              </Route>
              {/* 出入款管理 - 提现处理 */}
              <Route path="/deposit-withdraw/withdraw">
                <WithdrawPage />
              </Route>
              {/* 出入款管理 - 快速充值 */}
              <Route path="/deposit-withdraw/fast-deposit">
                <FastDepositPage />
              </Route>
              {/* 出入款管理 - 快速充值作业设定 */}
              <Route path="/deposit-withdraw/quick-deposit-setting">
                <QuickDepositSettingOfPayment />
              </Route>
              {/* 出入款管理 - 第三方转账管理 */}
              <Route path="/deposit-withdraw/transfer-management">
                <TransferManagementPage />
              </Route>
              {/* 出入款管理 - 帐变纪录 */}
              <Route path="/deposit-withdraw/transaction">
                <TransactionOfLog />
              </Route>
              {/* 出入款管理 - 会员转帐纪录 */}
              <Route path="/deposit-withdraw/member-transfer">
                <MemberTransferOfLog />
              </Route>
              {/* 出入款管理 - 虚拟币提现设定 */}
              <Route path="/deposit-withdraw/crypto-currency">
                <CryptoCurrencyPage />
              </Route>

              {/* 游戏纪录 - 彩票投注纪录 */}
              <Route path="/log/lottery-bet">
                <BetOfProducts />
              </Route>
              {/* 游戏纪录 - 彩票追号纪录 */}
              <Route path="/log/trace">
                <TraceOfProducts />
              </Route>
              {/* 游戏纪录 - 第三方投注记录 */}
              <Route path="/log/thirdparty-bet">
                <BetOfLog />
              </Route>
              {/* 游戏纪录 - 彩票投注记录历史 */}
              <Route path="/log/lottery-bet-history">
                <LotteryBetHistory />
              </Route>

              {/* 游戏管理 - 彩票管理 */}
              <Route path="/products/lottery/:key?">
                <OddsLottery />
              </Route>
              {/* 游戏管理 - 期数管理 */}
              <Route path="/products/issue-number">
                <IssueNumber />
              </Route>
              {/* 游戏管理 - 赔率设定 */}
              <Route path="/products/odds-setting">
                <OddsSetting />
              </Route>
              {/* 游戏管理 - 第三方管理 */}
              <Route path="/products/third-party">
                <ThirdParty />
              </Route>

              {/* 营运管理 - 公告设定 */}
              <Route path="/operation/news">
                <NewsOfOperation />
              </Route>
              {/* 营运管理 - 轮播图管理 */}
              <Route path="/banner/banner">
                <OperationBanner />
              </Route>
              {/* 营运管理 - 版型设置 */}
              <Route path="/template/template">
                <OperationTemplate />
              </Route>
              {/* 营运管理 - 全民代推广图管理 */}
              <Route path="/operation/promotion-management">
                <PromotionManagement />
              </Route>

              {/* 活动管理 - 充值优惠活动设定 */}
              <Route path="/activity/preferential-setting">
                <PreferentialSettingOfPayment />
              </Route>
              {/* 活动管理 - 活动类别管理 */}
              <Route path="/activity/activity-type-management">
                <ActiveTypes />
              </Route>
              {/* 活动管理 - 活动管理 */}
              <Route path="/activity/activity-management">
                <Actives />
              </Route>
              {/* 活动管理 - 活动报表詳情 */}
              <Route path="/activity/activity-report/detail/:activityid">
                <ActivityReportDetail />
              </Route>
              {/* 活动管理 - 活动报表 */}
              <Route path="/activity/activity-report">
                <ActivityReport />
              </Route>
              {/* 活动管理 - 红包发送管理 */}
              <Route path="/activity/red-envelope-management">
                <OperationRedEnvelope />
              </Route>
              {/* 活动管理 - 生息宝报表 */}
              <Route path="/activity/shengxibao">
                <ShengXiBao />
              </Route>

              {/* 营运报表 - 代理报表 */}
              <Route path="/operation-report/affiliate">
                <AffiliateOfOperationReport />
              </Route>
              {/* 营运报表 - 会员报表 */}
              <Route path="/operation-report/member">
                <MemberOfOperationReport />
              </Route>
              {/* 营运报表 - 平台报表 */}
              <Route path="/operation-report/agent">
                <AgentOfOperationReport />
              </Route>
              {/* 营运报表 - 余额统计 */}
              <Route path="/operation-report/balance-statistics">
                <BalanceStatistics />
              </Route>
              {/* 营运报表 - 盈亏排行 */}
              <Route path="/operation-report/rank">
                <RankOfReport />
              </Route>

              {/* 第三方注單詳情 - 真人 */}
              <Route path="/thirdparty-betting/reality/:key?">
                <ThirdpartyBettingReality />
              </Route>
              {/* 第三方注單詳情 - 棋牌 */}
              <Route path="/thirdparty-betting/chess/:key?">
                <ThirdpartyBettingChess />
              </Route>
              {/* 第三方注單詳情 - 體育 */}
              <Route path="/thirdparty-betting/sports/:key?">
                <ThirdpartyBettingSports />
              </Route>
              {/* 第三方注單詳情 - 電子 */}
              <Route path="/thirdparty-betting/egame/:key?">
                <ThirdpartyBettingEgame />
              </Route>
              {/* 第三方注單詳情 - 捕魚 */}
              <Route path="/thirdparty-betting/fishing/:key?">
                <ThirdpartyBettingFishing />
              </Route>
              {/* 第三方注單詳情 - 彩票 */}
              <Route path="/thirdparty-betting/lottery/:key?">
                <ThirdpartyBettingLottery />
              </Route>

              <Route path="/poc/lobby">
                <LobbyConfig />
              </Route>
              <Route path="*">
                <UnhandledRoute />
              </Route>
            </Switch>
          </Suspense>
        </BasicLayout>
      </Router>
    </ContextProviders>
  );
}
