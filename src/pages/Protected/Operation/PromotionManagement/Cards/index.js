import React, { useContext } from 'react'
import { Row, Col, Card, Tag, Popconfirm, Space, message } from 'antd'
import {
    EditOutlined,
    DeleteOutlined,
    DeleteFilled,
    PlusOutlined,
    LinkOutlined,
    FileImageOutlined,
} from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import caller from 'utils/fetcher'
import DateWithFormat from 'components/DateWithFormat'
import DateRangeWithIcon from 'components/DateRangeWithIcon'
import { PageContext } from '../index'
import TimeStatus from './TimeStatus'
import { TARGET_PAGE, TARGET_PAGE_FROM_ID } from '../index'

const { OPERATION_PROMOTION_MANAGEMENT_EDIT } = PERMISSION

export default function Cards({
    items,
    label,
    currentPlatform,
    promoteType,
    onMutate,
}) {
    const { editDrawerRef } = useContext(PageContext)
    const successMsg = useIntl().formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })

    function onDeleteById({ id }) {
        caller({
            method: 'delete',
            endpoint: `/api/promote/${id}`,
        }).then(() => {
            message.success(successMsg)
            onMutate()
        })
    }
    return (
        <Card
            title={label}
            bordered={false}
            size="small"
            style={{ marginBottom: '0.5rem' }}
        >
            <Row gutter={[16, 16]}>
                {items &&
                    items.map(data => (
                        <Col key={data.Id}>
                            <Card
                                hoverable
                                style={{ width: 240, padding: 0 }}
                                bodyStyle={{ padding: 0 }}
                                actions={[
                                    <EditOutlined
                                        key="edit"
                                        onClick={() =>
                                            editDrawerRef.current.onOpenDrawer({
                                                data,
                                            })
                                        }
                                    />,
                                    <Popconfirm
                                        placement="top"
                                        title={
                                            <FormattedMessage id="Share.ConfirmTips.Delete" />
                                        }
                                        icon={
                                            <DeleteFilled
                                                style={{ color: 'red' }}
                                            />
                                        }
                                        onConfirm={() =>
                                            onDeleteById({ id: data.Id })
                                        }
                                        okText={
                                            <FormattedMessage id="Share.ActionButton.Confirm" />
                                        }
                                        cancelText={
                                            <FormattedMessage id="Share.ActionButton.Cancel" />
                                        }
                                    >
                                        <DeleteOutlined key="delete" />
                                    </Popconfirm>,
                                ]}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        width: '100%',
                                        height: 160,
                                        position: 'relative',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <TimeStatus
                                        startTime={data.StartTime}
                                        endTime={data.EndTime}
                                    />
                                    <Tag
                                        color="blue"
                                        icon={
                                            data.TargetPage ===
                                            TARGET_PAGE.LINK ? (
                                                <LinkOutlined />
                                            ) : (
                                                <FileImageOutlined />
                                            )
                                        }
                                        style={{
                                            position: ' absolute',
                                            top: 32,
                                            left: 8,
                                        }}
                                    >
                                        <FormattedMessage
                                            id={`PageOperationPromotedManagement.Options.TargetPage.${
                                                TARGET_PAGE_FROM_ID[
                                                    data.TargetPage
                                                ]
                                            }`}
                                            description="類型"
                                        />
                                    </Tag>
                                    <img
                                        alt="banner"
                                        src={data.ImageUrl}
                                        style={{
                                            maxWidth: '100%',
                                            maxHeight: 160,
                                        }}
                                    />
                                </div>
                                <Card.Meta
                                    style={{ padding: 16 }}
                                    title={data.Title}
                                    description={
                                        <Space direction="vertical">
                                            <DateRangeWithIcon
                                                startTime={data.StartTime}
                                                endTime={data.EndTime}
                                            />
                                            <div>
                                                <FormattedMessage id="Share.CommonKeys.UpdateMember" />
                                                ：{data.Account}
                                            </div>
                                            <div>
                                                <FormattedMessage id="Share.Fields.ModifiedTime" />
                                                ：
                                                <DateWithFormat
                                                    time={data.UpdatedTime}
                                                />
                                            </div>
                                        </Space>
                                    }
                                />
                            </Card>
                        </Col>
                    ))}
                <Permission functionIds={[OPERATION_PROMOTION_MANAGEMENT_EDIT]}>
                    <Col>
                        <Card
                            hoverable
                            style={{
                                display: 'flex',
                                width: 240,
                                textAlign: 'center',
                                height: 322,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                            bodyStyle={{ width: '100%' }}
                            onClick={() =>
                                editDrawerRef.current.onOpenDrawer({
                                    create: true,
                                    currentPlatform,
                                    currentPromoteType: promoteType,
                                })
                            }
                        >
                            <PlusOutlined />
                            <div>
                                <FormattedMessage id="Share.ActionButton.Create" />
                            </div>
                        </Card>
                    </Col>
                </Permission>
            </Row>
        </Card>
    )
}
