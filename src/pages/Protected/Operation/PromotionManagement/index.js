import React, {
    useState,
    createRef,
    createContext,
    useContext,
    useEffect,
} from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Tabs } from 'antd'
import { FormattedMessage } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import Permission from 'components/Permission'
import PageHeaderTab from 'components/PageHeaderTab'
import Cards from './Cards'
import EditDrawer from './EditDrawer'
import { UserContext } from 'contexts/UserContext'

const { OPERATION_PROMOTION_MANAGEMENT_VIEW } = PERMISSION
const { TabPane } = Tabs

export const TARGET_PAGE = {
    LINK: 0, // 鏈結
    IMAGE: 4, // 圖片
}

export const TARGET_PAGE_FROM_ID = {
    0: 'LINK', // 鏈結
    4: 'IMAGE', // 圖片
}

export const PageContext = createContext()

const PLATFORMS = ['web', 'h5', 'ios', 'android']

export default function PromotionManagement() {
    let editDrawerRef = createRef()
    const [tabKey, setTabKey] = useState(PLATFORMS[0])
    const { data: userData } = useContext(UserContext)
    const [agentId, setAgentId] = useState()
    const { data, mutate } = useSWR(
        agentId ? `/api/promote/${agentId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    useEffect(() => {
        setAgentId(userData.AgentId)
    }, [userData.AgentId])

    function handleChangeAgent({ id }) {
        setAgentId(id)
    }

    return (
        <>
            <PageHeaderTab
                onChange={key => {
                    setTabKey(key)
                }}
            >
                {PLATFORMS.map(platform => (
                    <TabPane tab={platform} key={platform} />
                ))}
            </PageHeaderTab>
            <Permission
                isPage
                functionIds={[OPERATION_PROMOTION_MANAGEMENT_VIEW]}
            >
                <PageContext.Provider value={{ editDrawerRef }}>
                    <Cards
                        label={
                            <FormattedMessage
                                id="PageOperationPromotedManagement.Tabs.Slides"
                                description="輪播版位"
                            />
                        }
                        promoteType={1}
                        currentPlatform={tabKey}
                        items={data && data[tabKey] && data[tabKey]['slides']}
                        onMutate={mutate}
                    />
                    <Cards
                        label={
                            <FormattedMessage
                                id="PageOperationPromotedManagement.Tabs.Promote"
                                description="分享推廣版位"
                            />
                        }
                        promoteType={2}
                        currentPlatform={tabKey}
                        items={data && data[tabKey] && data[tabKey]['promote']}
                        onMutate={mutate}
                    />
                    <EditDrawer
                        ref={editDrawerRef}
                        mutate={mutate}
                        agentId={agentId}
                    />
                </PageContext.Provider>
            </Permission>
        </>
    )
}
