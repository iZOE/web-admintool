import React, { useMemo, forwardRef } from 'react';
import { DatePicker, Switch, Form, Input, Button, Select } from 'antd';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import Uploader from 'components/Uploader';
import { TARGET_PAGE } from '../../../index';
import { FORMAT } from 'constants/dateConfig';
import { getDateTimeStringWithoutTimeZone } from 'mixins/dateTime';

const { Option } = Select;
const { useForm } = Form;

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

export default forwardRef(function ActiveForm(
  { displaySaveButtons, defaultValues, platform, onSubmit },
  ref
) {
  const [form] = useForm();

  ref.current = {
    getValues,
  };

  const initialValues = useMemo(() => {
    if (defaultValues) {
      const newDefaultValues = Object.assign({}, defaultValues);
      newDefaultValues.StartTime = getDateTimeStringWithoutTimeZone(
        newDefaultValues.StartTime
      );
      newDefaultValues.EndTime = newDefaultValues.EndTime
        ? getDateTimeStringWithoutTimeZone(newDefaultValues.EndTime)
        : null;
      return newDefaultValues;
    } else {
      return {
        TargetPage: TARGET_PAGE.LINK,
      };
    }
  }, [defaultValues]);

  async function getValues() {
    const formValues = await form.validateFields();

    formValues.StartTime = formValues.StartTime.format('x');
    const EndOfTheWorldTime = moment('2099/12/31').format('x');
    if (formValues.IsForever) {
      formValues.EndTime = EndOfTheWorldTime;
    } else {
      formValues.EndTime = formValues.EndTime
        ? formValues.EndTime.format('x')
        : EndOfTheWorldTime;
    }

    delete formValues.IsForever;

    if (formValues.StartTime) {
      formValues.StartTime = Number(formValues.StartTime);
    }
    if (formValues.EndTime) {
      formValues.EndTime = Number(formValues.EndTime);
    }

    return formValues;
  }

  return (
    <>
      <Form
        {...layout}
        form={form}
        name="control-hooks"
        initialValues={initialValues}
      >
        <Form.Item
          name="TargetPage"
          label={
            <FormattedMessage
              id="PageOperationPromotedManagement.Form.TargetPage"
              description="類型"
            />
          }
        >
          <Select>
            {Object.keys(TARGET_PAGE).map(target => (
              <Option key={target} value={TARGET_PAGE[target]}>
                <FormattedMessage
                  id={`PageOperationPromotedManagement.Options.TargetPage.${target}`}
                  description="類型"
                />
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item shouldUpdate noStyle>
          {({ getFieldValue }) =>
            getFieldValue('TargetPage') === TARGET_PAGE.LINK ? (
              <Form.Item
                name="TargetUrl"
                label={
                  <FormattedMessage
                    id="PageOperationPromotedManagement.Form.TargetUrl"
                    description="鏈結"
                  />
                }
                rules={[
                  { required: true },
                  {
                    max: 100,
                  },
                ]}
              >
                <Input />
              </Form.Item>
            ) : null
          }
        </Form.Item>
        <Form.Item
          name="StartTime"
          label={
            <FormattedMessage
              id="PageOperationPromotedManagement.Form.StartTime"
              description="開始時間"
            />
          }
          rules={[
            {
              required: true,
            },
          ]}
        >
          <DatePicker
            format={FORMAT.DISPLAY.DEFAULT}
            style={{ width: '100%' }}
            showTime
          />
        </Form.Item>
        <Form.Item
          name="IsForever"
          label={
            <FormattedMessage
              id="PageOperationPromotedManagement.Form.IsForever"
              description="永久刊登"
            />
          }
          valuePropName="checked"
        >
          <Switch />
        </Form.Item>
        <Form.Item shouldUpdate noStyle>
          {({ getFieldValue }) =>
            !getFieldValue('IsForever') ? (
              <Form.Item
                name="EndTime"
                label={
                  <FormattedMessage
                    id="PageOperationPromotedManagement.Form.EndTime"
                    description="結束時間"
                  />
                }
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <DatePicker
                  format={FORMAT.DISPLAY.DEFAULT}
                  style={{ width: '100%' }}
                  showTime
                />
              </Form.Item>
            ) : null
          }
        </Form.Item>
        <Form.Item shouldUpdate noStyle>
          {({ setFieldsValue, getFieldValue }) => (
            <Form.Item
              name="ImageUrl"
              label={
                <FormattedMessage
                  id="PageOperationPromotedManagement.Form.ImageUrl"
                  description="圖片"
                />
              }
              rules={[{ required: true }]}
            >
              <Uploader
                action={`/api/v2/Image/upload/3/${platform}`}
                text={
                  <FormattedMessage
                    id="Share.Uploader.Text"
                    description="只支持{exts}格式，尺寸为{size}"
                    values={{
                      exts: 'jpg,png',
                      size: '345pt x 120pt',
                    }}
                  />
                }
                onSuccess={response => {
                  setFieldsValue({
                    ImageUrl: response.Data,
                  });
                }}
                defaultUrl={getFieldValue('ImageUrl')}
              />
            </Form.Item>
          )}
        </Form.Item>
        {displaySaveButtons && (
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              <FormattedMessage
                id="Share.ActionButton.Submit"
                description="提交"
              />
            </Button>
          </Form.Item>
        )}
      </Form>
    </>
  );
});
