import React, { createRef, useState, forwardRef } from 'react';
import { message, Drawer, Button } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';
import 'braft-editor/dist/index.css';
import caller from 'utils/fetcher';
import EditForm from './components/Form';
import { getISODateTimeString } from 'mixins/dateTime';

const PLATFORM_ID_MAPPER = {
  ios: 1,
  android: 2,
  web: 3,
  h5: 4,
  1: 'ios',
  2: 'android',
  3: 'web',
  4: 'h5',
};

export default forwardRef(function EditDrawer({ mutate, agentId }, ref) {
  let editForm = createRef();
  const intl = useIntl();
  const [data, setData] = useState();
  const [visible, setVisible] = useState();
  const [createMode, setCreateMode] = useState(false);
  const [createInitData, setCreateInitData] = useState();
  const [saving, setSaving] = useState();

  ref.current = {
    onOpenDrawer,
  };

  function onOpenDrawer({ data, create, currentPlatform, currentPromoteType }) {
    setVisible(true);
    if (create) {
      setData(null);
      setCreateMode(true);
      setCreateInitData({
        platform: currentPlatform,
        promoteType: currentPromoteType,
      });
    } else if (data) {
      setData(data);
      setCreateMode(false);
    }
  }

  function onCloseDrawer() {
    setVisible(false);
    setData(null);
    setCreateInitData(null);
  }

  async function onSave() {
    setSaving(true);
    const values = await editForm.current.getValues();

    if (!createMode) {
      values.Id = data.Id;
      values.StartTime = getISODateTimeString(values.StartTime);
      values.EndTime = getISODateTimeString(values.EndTime);

      caller({
        method: 'put',
        endpoint: '/api/promote',
        body: values,
      })
        .then(() => {
          message.success(
            intl.formatMessage({
              id: 'Share.SuccessMessage.UpdateSuccess',
            })
          );
          mutate();
          onCloseDrawer();
        })
        .catch(err => {
          console.error('err', err);
        })
        .finally(() => {
          setSaving(false);
        });
    } else {
      const newBody = Object.assign(values, {
        AgentId: agentId,
        Platform: PLATFORM_ID_MAPPER[createInitData.platform],
        PromoteType: createInitData.promoteType,
        StartTime: getISODateTimeString(values.StartTime),
        EndTime: getISODateTimeString(values.EndTime),
      });
      caller({
        method: 'post',
        endpoint: '/api/promote',
        body: newBody,
      })
        .then(() => {
          message.success(
            intl.formatMessage({
              id: 'Share.SuccessMessage.UpdateSuccess',
            })
          );
          mutate();
          onCloseDrawer();
        })
        .catch(err => {
          console.error(err);
        })
        .finally(() => {
          setSaving(false);
        });
    }
  }

  return (
    <Drawer
      title={
        createMode ? (
          <FormattedMessage id="Share.ActionButton.Create" />
        ) : (
          <FormattedMessage id="Share.ActionButton.Edit" />
        )
      }
      visible={visible}
      onClose={onCloseDrawer}
      width={700}
      footer={
        <div
          style={{
            textAlign: 'right',
          }}
        >
          <Button onClick={onCloseDrawer} style={{ marginRight: 8 }}>
            <FormattedMessage id="Share.ActionButton.Cancel" />
          </Button>
          <Button loading={saving} type="primary" onClick={onSave}>
            <FormattedMessage id="Share.ActionButton.Submit" />
          </Button>
        </div>
      }
    >
      {visible ? (
        <EditForm
          ref={editForm}
          defaultValues={createMode ? null : data}
          platform={
            createInitData
              ? createInitData.platform
              : PLATFORM_ID_MAPPER[data.PlatformId]
          }
        />
      ) : null}
    </Drawer>
  );
});
