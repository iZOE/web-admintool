import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Home";
import Create from "./Create";

export default function News() {
  return (
    <Switch>
      <Route exact path="/operation/news" breadCrumbName="新聞公告">
        <Home />
      </Route>
      <Route path="/operation/news/create">
        <Create />
      </Route>
    </Switch>
  );
}
