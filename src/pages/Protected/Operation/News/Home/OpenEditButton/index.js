import React, { useContext } from "react";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";
import { PageContext } from "../index";

export default function OpenEditButton({ id, name }) {
  const { onOpenModal } = useContext(PageContext);
  return (
    <Button
      type="link"
      style={name ? { padding: 0 } : null}
      onClick={() =>
        onOpenModal({
          id: id,
        })
      }
    >
      {name ? name : <FormattedMessage id="Share.ActionButton.Edit" />}
    </Button>
  );
}
