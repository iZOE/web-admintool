import React from "react";
import useSWR from "swr";
import axios from "axios";
import { Modal } from "antd";
import { Wrapper } from "./Styled";

export default function PreviewModal({ data, visible, onClose }) {
  return (
    <Modal visible={visible} onCancel={onClose} onOk={onClose}>
      {data ? <Wrapper dangerouslySetInnerHTML={{ __html: data }} /> : null}
    </Modal>
  );
}
