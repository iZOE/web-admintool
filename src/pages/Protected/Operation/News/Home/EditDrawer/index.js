import React, { createRef, useState } from "react";
import useSWR from "swr";
import axios from "axios";
import { message, Drawer, Button, Row, Col } from "antd";
import { FormattedMessage, useIntl } from "react-intl";
import "braft-editor/dist/index.css";
import caller from "utils/fetcher";
import NewsForm from "../../components/Form";
import PreviewModal from "./PreviewModal";

export default function EditDrawer({ id, visible, onClose, boundedMutate }) {
  let newsForm = createRef();
  const intl = useIntl();
  const [previewModal, setPreviewModal] = useState({
    visible: false,
    data: null
  });
  const { data } = useSWR(id && visible ? `/api/News/${id}` : null, url =>
    axios(url).then(res => res.data.Data)
  );

  async function onOpenPreview() {
    const values = await newsForm.current.getValues();
    setPreviewModal({ visible: true, data: values.Content });
  }

  function onClosePreview() {
    setPreviewModal({ visible: false, data: null });
  }

  async function onSave() {
    const values = await newsForm.current.getValues();

    caller({
      method: "put",
      endpoint: "/api/News",
      body: values
    })
      .then(() => {
        message.success(
          intl.formatMessage({ id: "Share.SuccessMessage.UpdateSuccess" })
        );
        boundedMutate();
        onClose({ type: "EDIT" });
      })
      .catch(err => {
        console.log("err", err);
        message.error(err.Message, 5);
      });
  }

  return (
    <Drawer
      title={<FormattedMessage id="Share.CommonKeys.Edit" />}
      visible={visible}
      onClose={onClose}
      width={700}
      footer={
        <Row>
          <Col span={12}>
            <Button onClick={onOpenPreview}>
              <FormattedMessage id="Share.ActionButton.Previewing" />
            </Button>
          </Col>
          <Col span={12}>
            <div
              style={{
                textAlign: "right"
              }}
            >
              <Button onClick={onClose} style={{ marginRight: 8 }}>
                <FormattedMessage id="Share.ActionButton.Cancel" />
              </Button>
              <Button type="primary" onClick={onSave}>
                <FormattedMessage id="Share.ActionButton.Submit" />
              </Button>
            </div>
          </Col>
        </Row>
      }
    >
      {data && visible ? (
        <>
          <NewsForm ref={newsForm} initialValues={data} />
          <PreviewModal {...previewModal} onClose={onClosePreview} />
        </>
      ) : null}
    </Drawer>
  );
}
