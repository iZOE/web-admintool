import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, Select, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import { getISODateTimeString } from 'mixins/dateTime'
import { FORMAT, DEFAULT } from 'constants/dateConfig'

const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT
const BOOLEAN_OPTIONS = [null, true, false]

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_A_QUARTER_TO_TODAY,
    Title: null,
    Status: null,
    IsMarqueeEnable: null,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = Form.useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ReleaseTo, ...restValues }) {
        const [start, end] = Date || [null, null]

        return {
            ...restValues,
            ReleaseTo:
                Array.isArray(ReleaseTo) &&
                ReleaseTo.some(v => typeof v === 'string')
                    ? ReleaseTo.filter(v => typeof v === 'number')
                    : ReleaseTo,
            ReleasedStartDate: getISODateTimeString(start),
            ReleasedEndDate: getISODateTimeString(end),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        name="Date"
                        label={
                            <FormattedMessage id="PageOperationNews.QueryCondition.DateTime" />
                        }
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsSimpleSelect
                        needAll
                        url="/api/Option/NewsType"
                        name="NewsTypeId"
                        label={
                            <FormattedMessage id="Share.FormItem.NewsType" />
                        }
                    />
                </Col>
                <Col sm={4}>
                    <FormItemMultipleSelect
                        checkAll
                        name="ReleaseTo"
                        label={
                            <FormattedMessage id="PageOperationNews.DataTable.Title.ReleasedTo" />
                        }
                        url="/api/Option/News/post-to"
                    />
                </Col>
                <Col sm={4}>
                    <Form.Item
                        name="Status"
                        label={
                            <FormattedMessage id="PageOperationNews.DataTable.Title.Status" />
                        }
                    >
                        <Select>
                            {BOOLEAN_OPTIONS.map(item => (
                                <Option value={item} key={item}>
                                    <FormattedMessage
                                        id={
                                            item === null
                                                ? 'Share.Dropdown.All'
                                                : `PageOperationNews.QueryCondition.SelectItem.Status.${item}`
                                        }
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <Form.Item
                        name="IsMarqueeEnable"
                        label={
                            <FormattedMessage id="PageOperationNews.DataTable.Title.IsMarqueeEnable" />
                        }
                    >
                        <Select>
                            {BOOLEAN_OPTIONS.map(item => (
                                <Option value={item} key={item}>
                                    <FormattedMessage
                                        id={
                                            item === null
                                                ? 'Share.Dropdown.All'
                                                : `PageOperationNews.QueryCondition.SelectItem.Status.${item}`
                                        }
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        name="Title"
                        label={
                            <FormattedMessage id="PageOperationNews.QueryCondition.Title" />
                        }
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={{ span: 8, offset: 8 }} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
