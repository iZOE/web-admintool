import React, { createContext, useState } from 'react';
import { Button, message } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { PlusOutlined } from '@ant-design/icons';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import * as PERMISSION from 'constants/permissions';
import caller from 'utils/fetcher';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import EditDrawer from './EditDrawer';

const { OPERATION_NEWS_CREATE, OPERATION_NEWS_VIEW } = PERMISSION;

export const PageContext = createContext();

const PageView = () => {
  const history = useHistory();
  const [modalsData, setModalsData] = useState({
    visible: false,
    id: null
  });
  const successMsg = useIntl().formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess'
  });

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
    onReady,
    boundedMutate
  } = useGetDataSourceWithSWR({
    url: '/api/News/Search',
    defaultSortKey: 'PostBeginTime',
    autoFetch: true
  });

  function onOpenModal({ id }) {
    setModalsData({ id, visible: true });
  }

  function onCloseModal() {
    setModalsData({ id: null, visible: false });
  }

  function onDeleteNews({ id }) {
    caller({
      method: 'delete',
      endpoint: `/api/News/${id}`
    }).then(() => {
      message.success(successMsg, 5);
      boundedMutate();
    });
  }

  return (
    <PageContext.Provider value={{ modalsData, onOpenModal, onDeleteNews }}>
      <Permission functionIds={[OPERATION_NEWS_VIEW]} isPage>
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={
                <FormattedMessage
                  id="Share.Table.SearchResult"
                  description="查詢結果"
                />
              }
              config={COLUMNS_CONFIG}
              loading={fetching}
              dataSource={dataSource?.List}
              total={dataSource?.TotalCount}
              onUpdate={onUpdateCondition}
              rowKey={record => record.IMNoticeBoardId}
              extendArea={
                <Permission functionIds={[OPERATION_NEWS_CREATE]}>
                  <Button
                    type="primary"
                    icon={<PlusOutlined />}
                    onClick={() => {
                      history.push('/operation/news/create');
                    }}
                  >
                    <FormattedMessage id="Share.ActionButton.Create" />
                  </Button>
                </Permission>
              }
            />
          }
        />
        <EditDrawer
          visible={modalsData.visible}
          id={modalsData && modalsData.id}
          onClose={onCloseModal}
          boundedMutate={boundedMutate}
        />
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
