import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Tag, Typography } from 'antd';
import DateWithFormat from 'components/DateWithFormat';
import DateRangeWithIcon from 'components/DateRangeWithIcon';
import OpenEditButton from './OpenEditButton';
import DeleteButton from './DeleteButton';

const { Text } = Typography;

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.NewsId" description="公告ID" />,
    dataIndex: 'IMNoticeBoardId',
    key: 'IMNoticeBoardId',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.Title" description="標題" />,
    dataIndex: 'Subject',
    key: 'Subject',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record) => <OpenEditButton id={record.IMNoticeBoardId} name={text} />,
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.IsMarqueeEnable" description="跑馬燈" />,
    dataIndex: 'IsMarqueeEnable',
    key: 'IsMarqueeEnable',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text =>
      text ? (
        <Tag color="magenta">
          <FormattedMessage id="PageOperationNews.DataTable.Title.IsMarqueeEnable" description="跑馬燈" />
        </Tag>
      ) : null,
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.IsPinned" description="置頂" />,
    dataIndex: 'Sticky',
    key: 'Sticky',
    isShow: true,
    render: text =>
      text ? (
        <Tag color="magenta">
          <FormattedMessage id="PageOperationNews.DataTable.Title.IsPinned" description="置頂" />
        </Tag>
      ) : null,
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.NewsTypeId" description="類別" />,
    dataIndex: 'IMNoticeBoardTypeId',
    key: 'IMNoticeBoardTypeId',
    isShow: true,
    render: (_1, record) => record.IMNoticeBoardTypeText,
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.ReleasedTo" description="發佈對象" />,
    dataIndex: 'PostTo',
    key: 'PostTo',
    isShow: true,
    render: (_1, record) => (
      <Text ellipsis style={{ maxWidth: 100 }}>
        {record.PostToText}
      </Text>
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.ReleaseDate" description="公告時間" />,
    dataIndex: 'ReleasedDate',
    key: 'PostBeginTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { PostBeginTime: s, PostEndTime: e }, _2) => <DateRangeWithIcon startTime={s} endTime={e} />,
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Post',
    key: 'Post',
    isShow: true,
    render: (_1, record) => (
      <Text ellipsis style={{ maxWidth: 100 }}>
        {record.PostText}
      </Text>
    ),
  },
  {
    title: <FormattedMessage id="PageOperationNews.DataTable.Title.ClickCount" description="點擊數" />,
    dataIndex: 'PageView',
    key: 'PageView',
    isShow: true,
  },
  {
    title: ' ',
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record) => (
      <>
        <OpenEditButton id={record.IMNoticeBoardId} />
        <DeleteButton id={record.IMNoticeBoardId} />
      </>
    ),
  },
];
