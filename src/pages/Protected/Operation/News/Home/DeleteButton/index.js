import React, { useContext } from "react";
import { Button, Popconfirm } from "antd";
import { FormattedMessage } from "react-intl";
import { DeleteFilled } from "@ant-design/icons";
import { PageContext } from "../index";

export default function DeleteButton({ id }) {
  const { onDeleteNews } = useContext(PageContext);
  return (
    <Popconfirm
      placement="top"
      title={<FormattedMessage id="Share.ActionButton.Delete" />}
      icon={<DeleteFilled style={{ color: "red" }} />}
      onConfirm={() => {
        onDeleteNews({ id });
      }}
      okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
      cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
    >
      <Button type="link">
        <FormattedMessage id="Share.ActionButton.Delete" />
      </Button>
    </Popconfirm>
  );
}
