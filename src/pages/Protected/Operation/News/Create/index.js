import React, { createRef } from 'react'
import { message, Modal } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom'
import caller from 'utils/fetcher'
import LayoutPageBody from 'layouts/Page/Body'
import NewsForm from '../components/Form'

const { confirm } = Modal

export default function Create() {
    let newsForm = createRef()
    const history = useHistory()
    const intl = useIntl()

    async function onSave() {
        const values = await newsForm.current.getValues()

        caller({
            method: 'post',
            endpoint: '/api/News',
            body: values,
        })
            .then(() => {
                message.success(
                    intl.formatMessage({
                        id: 'Share.SuccessMessage.UpdateSuccess',
                    })
                )
            })
            .finally(() => {
                history.push('/operation/news')
            })
    }

    function onShowCancelConfirm() {
        confirm({
            title: intl.formatMessage({
                id: 'Share.CommonKeys.FillInAgain',
                description: '重新填写',
            }),
            icon: <ExclamationCircleOutlined />,
            content: intl.formatMessage({
                id: 'Share.ConfirmTips.DoYouWantToReeditTheContent',
                description: '是否要重新编辑内容？',
            }),
            okText: intl.formatMessage({
                id: 'Share.ActionButton.Restart',
                description: '重來',
            }),
            okType: 'danger',
            cancelText: intl.formatMessage({
                id: 'Share.ActionButton.Cancel',
                description: '取消',
            }),
            onOk() {
                newsForm.current.form.resetFields()
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }

    return (
        <LayoutPageBody
            title={
                <FormattedMessage
                    id="PageOperationNewsCreate.PageHeader"
                    description="新增新闻公告"
                />
            }
        >
            <NewsForm
                displaySaveButtons
                ref={newsForm}
                onShowCancelConfir={onShowCancelConfirm}
                onSave={onSave}
            />
        </LayoutPageBody>
    )
}
