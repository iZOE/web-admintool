import styled from "styled-components";

export const Wrapper = styled.div`
  .bf-container {
    border: 1px solid #ccc;
  }
  .bf-controlbar {
    background: #777;
    .dropdown-handler,
    .control-item.button {
      color: #fff;
    }
  }
`;
