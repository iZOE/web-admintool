import React, { forwardRef } from 'react'
import { Form, Switch, DatePicker, Input, Divider, Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import Uploader from 'components/Uploader'
import { Wrapper } from './Styled'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import {
  getISODateTimeString,
  getDateTimeStringWithoutTimeZone,
} from 'mixins/dateTime'

const { RangePicker } = DatePicker
const { TextArea } = Input
const { useForm } = Form
const { RANGE } = DEFAULT

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
}

export default forwardRef(function NewsForm(
  { displaySaveButtons, initialValues, onSave },
  ref,
) {
  let defaultValues = Object.assign({}, initialValues)
  let editorInstance
  const [form] = useForm()
  ref.current = {
    getValues,
    form,
  }

  if (initialValues) {
    defaultValues.date = [
      getDateTimeStringWithoutTimeZone(initialValues.ReleasedStartDate),
      getDateTimeStringWithoutTimeZone(initialValues.ReleasedEndDate),
    ]
  } else {
    defaultValues = {
      date: RANGE.FROM_TOMORROW_TO_A_MONTH,
      NewsTypeId: 1,
      Status: false,
      IsPinned: false,
      IsMarqueeEnable: false,
    }
  }

  async function getValues() {
    const formValues = await form.validateFields()

    if (formValues.date) {
      formValues.ReleasedStartDate = getISODateTimeString(formValues.date[0])
      formValues.ReleasedEndDate = getISODateTimeString(formValues.date[1])
      delete formValues.date
    }

    return {
      ...formValues,
      Content: editorInstance.getValue().toHTML(),
    }
  }

  return (
    <Wrapper>
      <Form
        form={form}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={defaultValues}
      >
        <Form.Item name="NewsId" hidden />
        <Form.Item
          name="Title"
          label={
            <FormattedMessage id="PageOperationNews.DataTable.Title.Title" />
          }
          rules={[{ required: true, min: 4, max: 40 }]}
        >
          <Input />
        </Form.Item>
        <FormItemsSimpleSelect
          url="/api/Option/NewsType"
          name="NewsTypeId"
          label={<FormattedMessage id="Share.FormItem.NewsType" />}
          rules={[{ required: true }]}
        />
        <FormItemMultipleSelect
          name="ReleasedTo"
          label={
            <FormattedMessage id="PageOperationNews.DataTable.Title.ReleasedTo" />
          }
          url="/api/Option/News/post-to"
          rules={[{ required: true }]}
          customAllValue={{
            title: (
              <FormattedMessage id="Share.Dropdown.All" description="全部" />
            ),
            key: -1,
            value: -1,
          }}
        />
        <Form.Item
          name="AnnounceDept"
          label={
            <FormattedMessage
              id="PageOperationNews.Modal.AnnounceDept"
              description="發布部門"
            />
          }
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="Status"
          label={
            <FormattedMessage id="PageOperationNews.DataTable.Title.Status" />
          }
          valuePropName="checked"
        >
          <Switch defaultChecked />
        </Form.Item>
        <Form.Item
          name="date"
          label={
            <FormattedMessage id="PageOperationNews.DataTable.Title.ReleaseDate" />
          }
          rules={[{ required: true }]}
        >
          <RangePicker
            showTime
            format={FORMAT.DISPLAY.DEFAULT}
            style={{ width: '100%' }}
          />
        </Form.Item>
        <Form.Item
          name="IsPinned"
          label={
            <FormattedMessage id="PageOperationNews.DataTable.Title.IsPinned" />
          }
          valuePropName="checked"
        >
          <Switch defaultChecked />
        </Form.Item>
        <Form.Item
          name="IsMarqueeEnable"
          label={
            <FormattedMessage id="PageOperationNews.DataTable.Title.IsMarqueeEnable" />
          }
          valuePropName="checked"
        >
          <Switch defaultChecked />
        </Form.Item>
        <Form.Item
          name="TextContent"
          label={<FormattedMessage id="PageOperationNews.Modal.TextContent" />}
        >
          <TextArea />
        </Form.Item>
        <Form.Item
          name="ImageUrl"
          label={
            <FormattedMessage
              id="PageOperationNews.Modal.ImageUrl"
              description="圖片上傳"
            />
          }
        >
          <Uploader
            action="/api/v2/Image/upload/1"
            text={
              <FormattedMessage
                id="Share.PlaceHolder.Uploader.Image"
                values={{ width: 1035, height: 480 }}
              />
            }
            onSuccess={response => {
              form.setFieldsValue({
                ImageUrl: response.Data,
              })
            }}
            defaultUrl={initialValues ? initialValues.ImageUrl : null}
          />
        </Form.Item>
        <Divider />
        <BraftEditor
          ref={instance => (editorInstance = instance)}
          value={
            initialValues
              ? BraftEditor.createEditorState(initialValues.Content)
              : null
          }
          onChange={() => {}}
          onSave={() => {}}
        />
        {displaySaveButtons && (
          <>
            <Divider />
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit" onClick={onSave}>
                <FormattedMessage
                  id="Share.ActionButton.Submit"
                  description="提交"
                />
              </Button>
            </Form.Item>
          </>
        )}
      </Form>
    </Wrapper>
  )
})
