import React, { createContext, useState, useEffect, useCallback } from 'react'
import { useIntl } from 'react-intl'
import { Skeleton, Button, PageHeader, Card } from 'antd'
import { UnorderedListOutlined } from '@ant-design/icons'
import * as dayjs from 'dayjs'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import caller from 'utils/fetcher'
import AddSMSDrawer from './AddSMSDrawer'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'

const { ACTIVITY_MANAGEMENT_VIEW, ACTIVITY_MANAGEMENT_CREATE } = PERMISSION
// condition initial value
const now = dayjs()
const CONDITION_INITIAL_VALUE = {
    DateType: 1,
    StartTime: now
        .subtract(30, 'day')
        .startOf('day')
        .format(),
    EndTime: now.endOf('day').format(),
    Type: null,
    Status: null,
    Name: null,
    sortColumn: null,
    'paginationInfo.pageNumber': 1,
    'paginationInfo.pageSize': 10,
}

export const PageContext = createContext()

const Home = () => {
    const intl = useIntl()
    const [dataSource, setDataSource] = useState(null)
    const [showDrawer, setShowDrawer] = useState(false)
    // 查詢參數 有三種：form表單, 分頁區, table排序區
    const [condition, setCondition] = useState(CONDITION_INITIAL_VALUE)

    function getActivityData() {
        caller({
            endpoint: '/api/v2/Activity/',
            params: condition,
        }).then(data => setDataSource(data.Data))
    }
    useEffect(() => {
        // 打API 取資料
        getActivityData()
    }, [condition])

    // 更新condition
    const onUpdateCondition = useCallback(newCondition => {
        setCondition(prevCondition => ({
            ...prevCondition,
            ...newCondition,
        }))
    }, [])

    const onClose = useCallback(() => {
        setShowDrawer(false)
    }, [])

    const onShowDrawer = useCallback(() => {
        setShowDrawer(true)
    }, [])

    return (
        <>
            <PageHeader
                ghost={false}
                title={intl.formatMessage({
                    id: `PageOperationSMSManagement.SMSManagement`,
                })}
            />
            <Permission isPage functionIds={[ACTIVITY_MANAGEMENT_VIEW]}>
                <Card bordered={false} className="card">
                    <div id="condition">
                        <Condition
                            onUpdate={onUpdateCondition}
                            condition={condition}
                        />
                    </div>
                    {dataSource ? (
                        <DataTable
                            title={intl.formatMessage({
                                id: `PageOperationSMSManagement.Result`,
                            })}
                            config={COLUMNS_CONFIG}
                            dataSource={dataSource.Data}
                            total={dataSource.TotalCount}
                            extendArea={
                                <Permission
                                    functionIds={[ACTIVITY_MANAGEMENT_CREATE]}
                                    failedRender={
                                        <Button
                                            type="primary"
                                            icon={<UnorderedListOutlined />}
                                            disabled
                                        >
                                            {intl.formatMessage({
                                                id: `PageOperationSMSManagement.AddNewSMS`,
                                            })}
                                        </Button>
                                    }
                                >
                                    <Button
                                        type="primary"
                                        icon={<UnorderedListOutlined />}
                                        onClick={onShowDrawer}
                                    >
                                        {intl.formatMessage({
                                            id: `PageOperationSMSManagement.AddNewSMS`,
                                        })}
                                    </Button>
                                </Permission>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    ) : (
                        <Skeleton />
                    )}
                </Card>
                <AddSMSDrawer showDrawer={showDrawer} onClose={onClose} />
            </Permission>
        </>
    )
}

export default Home
