import styled from "styled-components";

export const Footer = styled.div`
  display: flex;
  justify-content: flex-end;
  .marginRight {
    margin-right: 0.5rem;
  }
`;

export const ExpirationDateComponent = styled.div`
  margin-top: 0.5rem;
`;
