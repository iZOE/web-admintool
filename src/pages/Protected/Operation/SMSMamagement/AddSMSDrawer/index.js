import React, { useState, useContext } from 'react'
import {
    Drawer,
    Form,
    Button,
    Col,
    Row,
    Radio,
    Input,
    Select,
    DatePicker,
} from 'antd'
import { useIntl } from 'react-intl'
import moment from 'moment'
import { UserContext } from 'contexts/UserContext'
import Permission from 'components/Permission'
import * as PERMISSION from 'constants/permissions'
import { Footer, ExpirationDateComponent } from './Styled'

const { ACTIVITY_MANAGEMENT_VIEW } = PERMISSION

const { Option } = Select

const TO_TYPE_LIST = [
    { Name: 'ToTypeList', ID: 1 },
    { Name: 'ToTypeList', ID: 2 },
    { Name: 'ToTypeList', ID: 3 },
    { Name: 'ToTypeList', ID: 4 },
]
const MEMBER_LEVEL_LIST = [
    { Name: 'MemberLevelList', ID: 0 },
    { Name: 'MemberLevelList', ID: 11 },
    { Name: 'MemberLevelList', ID: 12 },
    { Name: 'MemberLevelList', ID: 13 },
    { Name: 'MemberLevelList', ID: 14 },
    { Name: 'MemberLevelList', ID: 15 },
    { Name: 'MemberLevelList', ID: 16 },
    { Name: 'MemberLevelList', ID: 17 },
    { Name: 'MemberLevelList', ID: 18 },
    { Name: 'MemberLevelList', ID: 19 },
    { Name: 'MemberLevelList', ID: 20 },
]
const SME_MEMBER_LEVEL_LIST = [
    { Name: 'SMEMemberLevelList', ID: 0 },
    { Name: 'SMEMemberLevelList', ID: 1 },
    { Name: 'SMEMemberLevelList', ID: 2 },
    { Name: 'SMEMemberLevelList', ID: 3 },
]
const AFFILIATE_LEVEL_LIST = [
    { Name: 'AffiliateLevelList', ID: 0 },
    { Name: 'AffiliateLevelList', ID: 1 },
    { Name: 'AffiliateLevelList', ID: 2 },
    { Name: 'AffiliateLevelList', ID: 3 },
]

export default function AddSMSDrawer({ showDrawer, onClose }) {
    const intl = useIntl()
    const [toType, setToType] = useState(1)
    const [expirationDate, setExpirationDate] = useState(
        moment()
            .add(60, 'days')
            .format('YYYY/MM/DD HH:MM')
    )
    const { data: userData } = useContext(UserContext)
    let levelList
    switch (userData.AgentName) {
        case '彩立方2':
            levelList = MEMBER_LEVEL_LIST
            break
        case '收米':
            levelList = SME_MEMBER_LEVEL_LIST
            break
    }
    const radioStyle = {
        display: 'block',
        height: '1.875rem',
        lineHeight: '1.875rem',
    }
    const disabledDate = current => {
        return (
            current < moment().add(-1, 'days') ||
            current > moment().add(1, 'years')
        )
    }
    return (
        <Drawer
            title={intl.formatMessage({
                id: `PageOperationSMSManagement.AddNewSMS`,
            })}
            placement="right"
            width={720}
            closable={false}
            onClose={onClose}
            visible={showDrawer}
            footer={
                <Footer>
                    <Button
                        onClick={onClose}
                        type="primary"
                        className="marginRight"
                    >
                        {intl.formatMessage({
                            id: `PageOperationSMSManagement.AddSMSDrawer.Submit`,
                        })}
                    </Button>
                    <Button onClick={onClose}>
                        {intl.formatMessage({
                            id: `PageOperationSMSManagement.AddSMSDrawer.Cancel`,
                        })}
                    </Button>
                </Footer>
            }
        >
            <Permission isPage functionIds={[ACTIVITY_MANAGEMENT_VIEW]}>
                <Form
                    labelCol={{
                        xs: { span: 24 },
                        sm: { span: 4 },
                    }}
                    wrapperCol={{ xs: { span: 24 }, sm: { span: 20 } }}
                >
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="title"
                                label={intl.formatMessage({
                                    id: `PageOperationSMSManagement.AddSMSDrawer.Title`,
                                })}
                                rules={[{ required: true }]}
                            >
                                <Input
                                    placeholder={intl.formatMessage({
                                        id: `PageOperationSMSManagement.AddSMSDrawer.Placeholder1`,
                                    })}
                                    maxLength="20"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={0}>
                        <Col span={8}>
                            <Form.Item
                                name="ToType"
                                label={intl.formatMessage({
                                    id: `PageOperationSMSManagement.AddSMSDrawer.To`,
                                })}
                                labelCol={{
                                    xs: { span: 24 },
                                    sm: { span: 12 },
                                }}
                                wrapperCol={{
                                    xs: { span: 24 },
                                    sm: { span: 12 },
                                }}
                            >
                                <Select
                                    defaultValue={1}
                                    className="option"
                                    onSelect={id => {
                                        setToType(id)
                                    }}
                                >
                                    {TO_TYPE_LIST.map(type => (
                                        <Option
                                            value={type.ID}
                                            key={type.ID + type.Name}
                                        >
                                            {intl.formatMessage({
                                                id: `PageOperationSMSManagement.${type.Name}.${type.ID}`,
                                            })}
                                        </Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={16}>
                            {toType === 1 && (
                                <Form.Item name="" noStyle>
                                    <Input disabled />
                                </Form.Item>
                            )}
                            {toType === 2 && (
                                <Form.Item name="ToText" noStyle>
                                    <Input maxLength="16" />
                                </Form.Item>
                            )}
                            {toType === 3 && (
                                <Form.Item name="ToText" noStyle>
                                    <Select
                                        defaultValue={0}
                                        style={{ width: '100%' }}
                                    >
                                        {levelList.map(type => (
                                            <Option
                                                value={type.ID}
                                                key={type.ID + type.Name}
                                            >
                                                {intl.formatMessage({
                                                    id: `PageOperationSMSManagement.${type.Name}.${type.ID}`,
                                                })}
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            )}
                            {toType === 4 && (
                                <Form.Item name="ToText" noStyle>
                                    <Select
                                        defaultValue={0}
                                        style={{ width: '100%' }}
                                    >
                                        {AFFILIATE_LEVEL_LIST.map(type => (
                                            <Option
                                                value={type.ID}
                                                key={type.ID + type.Name}
                                            >
                                                {intl.formatMessage({
                                                    id: `PageOperationSMSManagement.${type.Name}.${type.ID}`,
                                                })}
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            )}
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="sendDate"
                                label={intl.formatMessage({
                                    id: `PageOperationSMSManagement.AddSMSDrawer.SendDate`,
                                })}
                                rules={[{ required: true }]}
                            >
                                <Radio.Group>
                                    <Radio style={radioStyle} value={1}>
                                        {intl.formatMessage({
                                            id: `PageOperationSMSManagement.AddSMSDrawer.Immediately`,
                                        })}
                                    </Radio>
                                    <Radio style={radioStyle} value={2}>
                                        {intl.formatMessage({
                                            id: `PageOperationSMSManagement.AddSMSDrawer.Reserve`,
                                        })}
                                        <DatePicker
                                            style={{
                                                width: '100%',
                                                marginLeft: '1rem',
                                            }}
                                            showTime
                                            disabledDate={disabledDate}
                                        />
                                    </Radio>
                                </Radio.Group>
                                <ExpirationDateComponent>
                                    {intl.formatMessage({
                                        id: `PageOperationSMSManagement.AddSMSDrawer.ExpirationDate`,
                                    })}
                                    {expirationDate}
                                </ExpirationDateComponent>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="role"
                                label={intl.formatMessage({
                                    id: `PageOperationSMSManagement.AddSMSDrawer.Role`,
                                })}
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Input
                                    placeholder={intl.formatMessage({
                                        id: `PageOperationSMSManagement.AddSMSDrawer.Placeholder2`,
                                    })}
                                    maxLength="8"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="image"
                                label={intl.formatMessage({
                                    id: `PageOperationSMSManagement.AddSMSDrawer.Image`,
                                })}
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Input
                                    placeholder={intl.formatMessage({
                                        id: `PageOperationSMSManagement.AddSMSDrawer.Placeholder2`,
                                    })}
                                    maxLength="8"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="content"
                                label={intl.formatMessage({
                                    id: `PageOperationSMSManagement.AddSMSDrawer.Content`,
                                })}
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Input
                                    placeholder={intl.formatMessage({
                                        id: `PageOperationSMSManagement.AddSMSDrawer.Placeholder2`,
                                    })}
                                    maxLength="8"
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Permission>
        </Drawer>
    )
}
