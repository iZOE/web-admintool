import React, { useMemo } from 'react'
import { Link } from 'react-router-dom'
import { useIntl } from 'react-intl'
import * as dayjs from 'dayjs'
import { Button } from 'antd'
import { EditFilled } from '@ant-design/icons'
import Permission from 'components/Permission'
import * as PERMISSION from 'constants/permissions'

const { ACTIVITY_MANAGEMENT_EDIT } = PERMISSION

export default function ButtonItems({ record }) {
    const intl = useIntl()
    const disabled = useMemo(() => {
        const now = dayjs()
        const endTime = dayjs(record.ActivityEndTime)
        if (endTime < now) {
            return true
        }
        return false
    })

    return (
        <Permission
            functionIds={[ACTIVITY_MANAGEMENT_EDIT]}
            failedRender={
                <Button type="primary" icon={<EditFilled />} disabled>
                    {intl.formatMessage({
                        id: `PageOperationSMSManagement.View`,
                    })}
                </Button>
            }
        >
            <Link to={`/activity/activity-management/detail/${record.Id}`}>
                <Button
                    type="primary"
                    icon={<EditFilled />}
                    disabled={disabled}
                >
                    {intl.formatMessage({
                        id: `PageOperationSMSManagement.View`,
                    })}
                </Button>
            </Link>
        </Permission>
    )
}
