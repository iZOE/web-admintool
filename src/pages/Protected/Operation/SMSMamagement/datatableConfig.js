import React from "react";
import ButtonItems from "./Rows/ButtonItems";
import { FormattedMessage } from "react-intl";

export const COLUMNS_CONFIG = [
  {
    title: "ID",
    dataIndex: "ID",
    key: "ID",
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ["descend", "ascend"],
  },
  {
    title: (
      <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.Title" />
    ),
    dataIndex: "Title",
    key: "Title",
    isShow: true,
  },
  {
    title: (
      <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.To" />
    ),
    dataIndex: "To",
    key: "To",
    isShow: true,
  },
  {
    title: (
      <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.From" />
    ),
    dataIndex: "From",
    key: "From",
    isShow: true,
  },
  {
    title: (
      <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.Date" />
    ),
    dataIndex: "Date",
    key: "Date",
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ["descend", "ascend"],
  },
  {
    title: (
      <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.Content" />
    ),
    dataIndex: "Content",
    key: "Content",
    isShow: true,
  },
  {
    title: (
      <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.Status" />
    ),
    dataIndex: "Status",
    key: "Status",
    isShow: true,
    render: (text, record) => {
      let result = text;
      switch (text) {
        case 1:
          result = (
            <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.StatusType.1" />
          );
          break;
        case 2:
          result = (
            <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.StatusType.2" />
          );
          break;
      }
      return result;
    },
  },
  {
    title: (
      <FormattedMessage id="PageOperationSMSManagement.ColumnsConfigTitle.Manage" />
    ),
    dataIndex: "Manage",
    key: "Manage",
    isShow: true,
    render: (text, record, index) => <ButtonItems record={record} />,
  },
];
