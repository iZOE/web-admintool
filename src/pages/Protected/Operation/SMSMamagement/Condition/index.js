import React, { useState, useContext } from "react";
import { useIntl } from "react-intl";
import { Form, Button, Row, Col, Input, Select, DatePicker } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import moment from "moment";
import * as dayjs from "dayjs";
import { UserContext } from "contexts/UserContext";
import useRequest from "hooks/useRequest";
import { Wrapper } from "./Styled";

const { Option } = Select;
const { RangePicker } = DatePicker;

const TO_TYPE_LIST = [
  { Name: "ToTypeList", ID: 1 },
  { Name: "ToTypeList", ID: 2 },
  { Name: "ToTypeList", ID: 3 },
  { Name: "ToTypeList", ID: 4 },
  { Name: "ToTypeList", ID: 5 },
];
const MEMBER_LEVEL_LIST = [
  { Name: "MemberLevelList", ID: 0 },
  { Name: "MemberLevelList", ID: 11 },
  { Name: "MemberLevelList", ID: 12 },
  { Name: "MemberLevelList", ID: 13 },
  { Name: "MemberLevelList", ID: 14 },
  { Name: "MemberLevelList", ID: 15 },
  { Name: "MemberLevelList", ID: 16 },
  { Name: "MemberLevelList", ID: 17 },
  { Name: "MemberLevelList", ID: 18 },
  { Name: "MemberLevelList", ID: 19 },
  { Name: "MemberLevelList", ID: 20 },
];
const SME_MEMBER_LEVEL_LIST = [
  { Name: "SMEMemberLevelList", ID: 0 },
  { Name: "SMEMemberLevelList", ID: 1 },
  { Name: "SMEMemberLevelList", ID: 2 },
  { Name: "SMEMemberLevelList", ID: 3 },
];
const AFFILIATE_LEVEL_LIST = [
  { Name: "AffiliateLevelList", ID: 0 },
  { Name: "AffiliateLevelList", ID: 1 },
  { Name: "AffiliateLevelList", ID: 2 },
  { Name: "AffiliateLevelList", ID: 3 },
];
const STATUS_TYPE_LIST = [
  { Name: "StatusType", ID: 1 },
  { Name: "StatusType", ID: 2 },
];

function Condition({ onUpdate }) {
  const intl = useIntl();
  const [toType, setToType] = useState(1);
  const { data: userData } = useContext(UserContext);
  const now = dayjs();
  let levelList;
  switch (userData.AgentName) {
    case "彩立方2":
      levelList = MEMBER_LEVEL_LIST;
      break;
    case "收米":
      levelList = SME_MEMBER_LEVEL_LIST;
      break;
  }

  const { data: activityType } = useRequest({
    url: "/api/v2/Activity/Type",
  });

  function onFinish(values) {
    const result = {};
    result.DateType = values.DateType || 1;
    if (values.date) {
      result.StartTime = values.date[0].format();
      result.EndTime = values.date[1].format();
    } else {
      result.StartTime = now
        .subtract(30, "day")
        .startOf("day")
        .format();
      result.EndTime = now.endOf("day").format();
    }
    result.Type = values.Type || null;
    result.Status = values.Status || null;
    result.Name = values.Name || null;
    onUpdate(result);
  }

  const onChange = (e) => {
    e.target.value = e.target.value.replace(/[^[\u4E00-\u9FA5A-Za-z0-9]+$/, "");
  };

  return (
    <Wrapper>
      <Form
        labelCol={{
          xs: { span: 24 },
          sm: { span: 8 },
        }}
        wrapperCol={{ xs: { span: 24 }, sm: { span: 16 } }}
        onFinish={onFinish}
      >
        <Row gutter={24}>
          <Col sm={8} xs={24}>
            <Row>
              <Col span={8}>
                <Form.Item name="ToType" noStyle>
                  <Select
                    defaultValue={1}
                    className="option"
                    onSelect={(id) => {
                      setToType(id);
                    }}
                  >
                    {TO_TYPE_LIST.map((type) => (
                      <Option value={type.ID} key={type.ID + type.Name}>
                        {intl.formatMessage({
                          id: `PageOperationSMSManagement.${type.Name}.${type.ID}`,
                        })}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              {toType === 1 && (
                <Col span={16}>
                  <Form.Item name="" noStyle>
                    <Input disabled />
                  </Form.Item>
                </Col>
              )}
              {toType === 2 && (
                <Col span={16}>
                  <Form.Item name="ToText" noStyle>
                    <Input maxLength="16" />
                  </Form.Item>
                </Col>
              )}
              {toType === 3 && (
                <Col span={16}>
                  <Form.Item name="ToText" noStyle>
                    <Select defaultValue={0} style={{ width: "100%" }}>
                      {levelList.map((type) => (
                        <Option value={type.ID} key={type.ID + type.Name}>
                          {intl.formatMessage({
                            id: `PageOperationSMSManagement.${type.Name}.${type.ID}`,
                          })}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              )}
              {toType === 4 && (
                <Col span={16}>
                  <Form.Item name="ToText" noStyle>
                    <Select defaultValue={0} style={{ width: "100%" }}>
                      {AFFILIATE_LEVEL_LIST.map((type) => (
                        <Option value={type.ID} key={type.ID + type.Name}>
                          {intl.formatMessage({
                            id: `PageOperationSMSManagement.${type.Name}.${type.ID}`,
                          })}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              )}
              {/* 視發信群組的設定依序顯示 */}
              {toType === 5 && (
                <Col span={16}>
                  <Form.Item name="ToText" noStyle>
                    <Select defaultValue={0} style={{ width: "100%" }}>
                      {MEMBER_LEVEL_LIST.map((type) => (
                        <Option value={type.ID} key={type.ID + type.Name}>
                          {type.Name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              )}
            </Row>
          </Col>
          <Col sm={8} xs={24}>
            <Form.Item
              label={intl.formatMessage({
                id: `PageOperationSMSManagement.From`,
              })}
              name="From"
            >
              <Input maxLength="16" onChange={onChange} onKeyUp={onChange} />
            </Form.Item>
          </Col>
          <Col sm={8} xs={24}>
            <Form.Item
              label={intl.formatMessage({
                id: `PageOperationSMSManagement.Title`,
              })}
              name="Title"
            >
              <Input
                maxLength="20"
                placeholder={intl.formatMessage({
                  id: `PageOperationSMSManagement.PlaceHolder`,
                })}
              />
            </Form.Item>
          </Col>
          <Col sm={8} xs={24}>
            <Form.Item
              label={intl.formatMessage({
                id: `PageOperationSMSManagement.Date`,
              })}
              name="Date"
            >
              <RangePicker
                showTime
                defaultValue={[
                  moment("2020-01-01 00:00:00"),
                  moment(
                    now.endOf("day").format("YYYY-MM-DD HH:mm:ss"),
                    "YYYY-MM-DD HH:mm:ss"
                  ),
                ]}
              />
            </Form.Item>
          </Col>
          <Col sm={8} xs={24}>
            <Form.Item
              label={intl.formatMessage({
                id: `PageOperationSMSManagement.Status`,
              })}
              name="Status"
            >
              <Select defaultValue={""}>
                {STATUS_TYPE_LIST.map((type) => (
                  <Option value={type.ID} key={type.ID + type.Name}>
                    {intl.formatMessage({
                      id: `PageOperationSMSManagement.${type.Name}.${type.ID}`,
                    })}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col sm={8} xs={24}>
            <Button
              type="primary"
              htmlType="submit"
              icon={<SearchOutlined />}
              className="floatRight"
            >
              {intl.formatMessage({
                id: `PageOperationSMSManagement.Search`,
              })}
            </Button>
          </Col>
        </Row>
      </Form>
    </Wrapper>
  );
}

export default Condition;
