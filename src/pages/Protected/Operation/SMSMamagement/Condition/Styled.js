import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 16px;
  background: #ffffff;
  .option {
    width: 100%;
  }
  .floatRight {
    float: right;
  }
  .marginZero {
    margin: 0;
  }
`;
