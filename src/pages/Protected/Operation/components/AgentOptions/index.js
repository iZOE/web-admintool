import React, { useMemo } from "react";
import { Dropdown, Menu, Button } from "antd";
import { DownOutlined } from "@ant-design/icons";
import useSWR from "swr";
import axios from "axios";

export default function AgentOptions({ agentId, onChange }) {
  const { data } = useSWR("/api/Option/AgentIdName", (url) =>
    axios(url)
      .then((res) => {
        if (res.data.Data) {
          return res.data.Data;
        }
        return [];
      })
      .catch(() => {
        return [];
      })
  );

  const menu = (
    <Menu>
      {data &&
        data.map((d) => (
          <Menu.Item key={d.AgentId}>
            <Button type="link" onClick={() => onChange({ id: d.AgentId })}>
              {d.AgentName}
            </Button>
          </Menu.Item>
        ))}
    </Menu>
  );

  const currentAgentName = useMemo(() => {
    if (!agentId || !data) {
      return null;
    }
    const findAgentById = data.filter((x) => x.AgentId === agentId);
    if (findAgentById.length > 0) {
      return findAgentById[0].AgentName;
    }
  }, [agentId, data]);

  if (!data || data.length < 2) {
    return null;
  }

  return (
    <Dropdown overlay={menu} placement="bottomRight">
      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
        {currentAgentName} <DownOutlined />
      </a>
    </Dropdown>
  );
}
