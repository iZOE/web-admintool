import React, { useState, useEffect } from 'react';
import { message, Row, Col, Card, Button, Layout, Select, Divider } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';
import { QuestionOutlined } from '@ant-design/icons';
import Uploader from 'components/Uploader';
import caller from 'utils/fetcher';
const { Content } = Layout;
const { Option } = Select;

export default function ImageConfig({ agentId, data, platformName, mutate }) {
  const platform = platformName.charAt(0).toUpperCase() + platformName.slice(1);
  const [loginTemplate, setLoginTemplate] = useState(
    data ? data.LoginTemplate[platform].Template : null
  );
  const [diffLoginTemplateImageUrl, setDiffLoginTemplateImageUrl] = useState(
    null
  );
  const [diffLogoImageUrl, setDiffLogoImageUrl] = useState(null);
  const successMsg = useIntl().formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess',
  });

  useEffect(() => {
    if (platformName === 'web') {
      setLoginTemplate(1);
    } else {
      setLoginTemplate(data ? data.LoginTemplate[platform].Template : null);
    }
  }, [platformName]);

  useEffect(() => {
    setDiffLoginTemplateImageUrl(null);
  }, [loginTemplate]);

  function handleSave() {
    const body = {
      AgentId: agentId,
      LoginTemplateId: loginTemplate,
      LoginTemplateUrl: diffLoginTemplateImageUrl,
      LogoUrl: diffLogoImageUrl,
    };
    caller({
      method: 'put',
      endpoint: `/api/Template/Settings/${platform}`,
      body,
    })
      .then(() => {
        message.success(successMsg, 5);
        mutate();
      })
      .catch(err => {
        console.error(err);
      });
  }

  if (!data) {
    return null;
  }
  return (
    <Layout style={{ padding: 24 }}>
      <Content>
        <Row gutter={{ xs: 12, sm: 12, md: 12, lg: 32 }}>
          <Col span={12}>
            <Card
              title={
                <FormattedMessage
                  id="PageOperationTemplate.CardType.Login"
                  description="Login"
                />
              }
              bordered={false}
              extra={
                <Button
                  shape="circle"
                  icon={<QuestionOutlined />}
                  size="small"
                />
              }
            >
              {platformName === 'mobile' && (
                <Select
                  value={loginTemplate}
                  onChange={val => {
                    setLoginTemplate(val);
                  }}
                >
                  <Option value={1}>
                    <FormattedMessage
                      id="PageOperationTemplate.Options.Layout.Normal"
                      description="一般版型"
                    />
                  </Option>
                  <Option value={2}>
                    <FormattedMessage
                      id="PageOperationTemplate.Options.Layout.Full"
                      description="一般版型"
                    />
                  </Option>
                </Select>
              )}
              <Uploader
                action="/api/v2/Image/upload/4"
                text={
                  <FormattedMessage
                    id="Share.Uploader.Text"
                    description="只支持{exts}格式，尺寸为{size}"
                    values={{
                      exts: 'jpg,png',
                      size: '345pt x 120pt',
                    }}
                  />
                }
                defaultUrl={
                  data.LoginTemplate[platform].ImageUrl[loginTemplate]
                }
                onSuccess={response => {
                  setDiffLoginTemplateImageUrl(response.Data);
                }}
              />
            </Card>
          </Col>
          <Col span={12}>
            <Card
              title={
                <FormattedMessage
                  id="PageOperationTemplate.CardType.AppBar"
                  description="AppBar"
                />
              }
              bordered={false}
              extra={
                <Button
                  shape="circle"
                  icon={<QuestionOutlined />}
                  size="small"
                />
              }
            >
              <Uploader
                action="/api/v2/Image/upload/4"
                text={
                  <FormattedMessage
                    id="Share.Uploader.Text"
                    description="只支持{exts}格式，尺寸为{size}"
                    values={{
                      exts: 'jpg,png',
                      size: '345pt x 120pt',
                    }}
                  />
                }
                defaultUrl={data.Logo[platform]}
                onSuccess={response => {
                  setDiffLogoImageUrl(response.Data);
                }}
              />
            </Card>
          </Col>
          <Col span={24}>
            <Divider />
            <div style={{ textAlign: 'right' }}>
              <Button type="primary" onClick={handleSave}>
                <FormattedMessage
                  id="Share.ActionButton.Save"
                  description="儲存"
                />
              </Button>
            </div>
          </Col>
        </Row>
      </Content>
    </Layout>
  );
}
