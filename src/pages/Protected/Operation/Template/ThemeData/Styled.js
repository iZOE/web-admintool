import styled from "styled-components";

export const Wrapper = styled.div`
  margin: 24px 24px 0;
  height: 64px;
  img {
    height: 64px;
  }
`;
