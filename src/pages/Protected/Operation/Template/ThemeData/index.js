import React from "react";
import { Row, Col, Typography } from "antd";
import { FormattedMessage } from "react-intl";
import { CheckCircleTwoTone, CloseCircleTwoTone } from "@ant-design/icons";
import { Wrapper } from "./Styled";

const { Title, Text } = Typography;

export default function ThemeData({ data }) {
  return (
    <Wrapper>
      {data && (
        <Row gutter={16}>
          <Col>
            <img
              src={`${process.env.PUBLIC_URL}/assets/page/operation/template/${data.Theme}.png`}
            />
          </Col>
          <Col>
            <Title level={4}>
              <FormattedMessage id="PageOperationTemplate.ThemeData.ThemeType" />{" "}
              {data.Theme}
            </Title>
            <Text>
              iOS/Android/H5{" "}
              <FormattedMessage id="PageOperationTemplate.ThemeData.IsDisplayMemberInfo" />{" "}
              {data.Info ? (
                <CheckCircleTwoTone twoToneColor="#52c41a" />
              ) : (
                <CloseCircleTwoTone twoToneColor="red" />
              )}
            </Text>
          </Col>
        </Row>
      )}
    </Wrapper>
  );
}
