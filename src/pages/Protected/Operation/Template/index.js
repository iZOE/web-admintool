import React, { useState, createContext, useContext, useEffect } from 'react'
import useSWR from 'swr'
import axios from 'axios'
import { Tabs, Space } from 'antd'
import { FormattedMessage } from 'react-intl'
import { UserContext } from 'contexts/UserContext'
import PageHeaderTab from 'components/PageHeaderTab'
import ImageConfig from './ImageConfig'
import ThemeData from './ThemeData'

const { TabPane } = Tabs

export const PageContext = createContext()

export default function PromotionManagement() {
    const [tabKey, setTabKey] = useState('web')
    const { data: userData } = useContext(UserContext)
    const [agentId, setAgentId] = useState()
    const { data, mutate } = useSWR(
        agentId ? `/api/Template/Settings/?agentId=${agentId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    useEffect(() => {
        setAgentId(userData.AgentId)
    }, [userData.AgentId])

    return (
        <>
            <PageHeaderTab
                defaultActiveKey={tabKey}
                onChange={key => {
                    setTabKey(key)
                }}
            >
                <TabPane
                    tab={
                        <FormattedMessage id="PageOperationTemplate.Tabs.Web" />
                    }
                    key="web"
                />
                <TabPane
                    tab={
                        <FormattedMessage id="PageOperationTemplate.Tabs.Mobile" />
                    }
                    key="mobile"
                />
            </PageHeaderTab>
            <Space />
            <ThemeData data={data} />
            <Space />
            <ImageConfig
                agentId={agentId}
                data={data}
                platformName={tabKey}
                mutate={mutate}
            />
        </>
    )
}
