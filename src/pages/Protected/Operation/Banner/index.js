import React, {
  useState,
  createRef,
  createContext,
  useContext,
  useEffect
} from 'react';
import useSWR from 'swr';
import axios from 'axios';
import { Tabs } from 'antd';
import { FormattedMessage } from 'react-intl';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import PageHeaderTab from 'components/PageHeaderTab';
import Cards from './Cards';
import EditDrawer from './EditDrawer';
import { UserContext } from 'contexts/UserContext';

const { BANNER_BANNER_VIEW } = PERMISSION;
const { TabPane } = Tabs;

export const TARGET_PAGE = {
  LINK: 0, // 鏈結
  LOTTERY: 1,
  IMAGE: 4, // 圖片
  THIRDPARTY_GAME: 5
};

export const BANNER_TYPES = ['sliders', 'gameButtons', 'links'];
export const DISPLAY_ORDER = {
  sliders: 1,
  gameButtons: 2,
  links: 3
};

export const TARGET_PAGE_FROM_ID = {
  0: 'LINK', // 鏈結
  1: 'LOTTERY',
  4: 'IMAGE', // 圖片
  5: 'THIRDPARTY_GAME'
};

export const PageContext = createContext();

const PLATFORMS = ['web', 'h5', 'ios', 'android'];

function capitalize(s) {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
}

export default function PromotionManagement() {
  let editDrawerRef = createRef();
  const [tabKey, setTabKey] = useState(PLATFORMS[0]);
  const { data: userData } = useContext(UserContext);
  const [agentId, setAgentId] = useState();
  const { data, mutate } = useSWR(
    agentId ? `/api/Banner/BannerSetting/${agentId}` : null,
    url => axios(url).then(res => res.data.Data)
  );

  useEffect(() => {
    setAgentId(userData.AgentId);
  }, [userData.AgentId]);

  return (
    <>
      <PageHeaderTab
        onChange={key => {
          setTabKey(key);
        }}
      >
        {PLATFORMS.map(platform => (
          <TabPane tab={platform} key={platform} />
        ))}
      </PageHeaderTab>
      <Permission isPage functionIds={[BANNER_BANNER_VIEW]}>
        <PageContext.Provider value={{ editDrawerRef }}>
          {BANNER_TYPES.map(type => (
            <Cards
              key={type}
              label={
                <FormattedMessage
                  id={`PageOperationBanner.Tabs.${capitalize(type)}`}
                  description="輪播版位"
                />
              }
              type={type}
              currentPlatform={tabKey}
              currentDisplayOrder={DISPLAY_ORDER[type]}
              items={data && data[tabKey] && data[tabKey][type]}
              agentId={agentId}
              onMutate={mutate}
            />
          ))}
          <EditDrawer ref={editDrawerRef} mutate={mutate} agentId={agentId} />
        </PageContext.Provider>
      </Permission>
    </>
  );
}
