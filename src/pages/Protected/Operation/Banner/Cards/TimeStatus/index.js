import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Tag } from 'antd'
import moment from 'moment'

const STATUS_COLOR = {
    OnGoing: 'green',
    Expired: 'magenta',
    NotLaunch: null,
}

const getTimestamp = time => {
    return Number(moment(moment(time).format('YYYY/MM/DD HH:mm:ss Z')))
}

export default function TimeStatus({ startTime, endTime }) {
    const _start = getTimestamp(startTime)
    const _end = getTimestamp(endTime)
    const _now = getTimestamp()

    let timeStatus

    if (_start > _now) {
        timeStatus = 'NotLaunch'
    } else if (_end && _start <= _now && _end >= _now) {
        timeStatus = 'OnGoing'
    } else {
        timeStatus = 'Expired'
    }

    return (
        <Tag
            color={STATUS_COLOR[timeStatus]}
            style={{ position: 'absolute', top: 8, left: 8 }}
        >
            <FormattedMessage
                id={`PageOperationBanner.Card.TimeStatus.${timeStatus}`}
                defaultMessage={timeStatus}
            />
        </Tag>
    )
}
