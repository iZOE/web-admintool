import styled from "styled-components";

export const Wrapper = styled.div`
  .frame {
    width: 320px;
    height: 650px;
    background-color: #ffffff;
    position: relative;
    margin: 50px auto;
    border: 6px solid #bdbbbb;
    border-radius: 50px;
    box-shadow: 14px 21px 42px 10px rgba(0, 0, 0, 0.2);
    &-content {
      box-sizing: content-box;
      overflow: hidden;
      width: 292px;
      height: 622px;
      background-color: #ffffff;
      border: 16px solid #202020;
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      border-radius: 47px;
      .frame-header {
        position: absolute;
        width: 170px;
        height: 24px;
        top: -2px;
        left: 50%;
        transform: translateX(-50%);
        background-color: #202020;
        border-radius: 0 0 23px 23px;

        &-speaker {
          margin-top: 6px;
          width: 40px;
          height: 4px;
          background-color: #404040;
          margin-left: auto;
          margin-right: auto;
        }

        &-camera {
          position: absolute;
          width: 7px;
          height: 7px;
          right: 40px;
          top: 2px;
          background-color: #090a50;
          border: 3px solid #404040;
          border-radius: 50%;
        }
      }
    }
  }
  .footer {
    text-align: center;
    button {
      margin-right: 5px;
    }
  }
`;
