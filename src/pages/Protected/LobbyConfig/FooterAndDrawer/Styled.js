import styled from "styled-components";

export const Wrapper = styled.div`
  text-align: center;
  button {
    margin-right: 5px;
  }
`;
