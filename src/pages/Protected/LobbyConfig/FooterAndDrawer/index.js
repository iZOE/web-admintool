import React, { useState } from "react";
import { PlusOutlined, SaveOutlined } from "@ant-design/icons";
import { Button } from "antd";
import ElementDrawer from "./ElementDrawer";
import { Wrapper } from "./Styled";

export default function FooterAndDrawer({ onSave }) {
  const [drawerVisible, setDrawerVisible] = useState(false);

  return (
    <Wrapper>
      <Button
        icon={<PlusOutlined />}
        onClick={() => {
          setDrawerVisible(true);
        }}
      >
        添加Banner
      </Button>
      <ElementDrawer
        visible={drawerVisible}
        onClose={() => setDrawerVisible(false)}
      />
      <Button type="primary" icon={<SaveOutlined />} onClick={onSave}>
        儲存
      </Button>
    </Wrapper>
  );
}
