import React from "react";
import { Drawer } from "antd";
import { Banner } from "./Styled";

export default function ElementDrawer({ visible, onClose }) {
  return (
    <Drawer
      title="選取要新增的物件"
      width={300}
      onClose={onClose}
      visible={visible}
      bodyStyle={{ paddingBottom: 80 }}
    >
      <Banner
        draggable={true}
        unselectable="on"
        onDragStart={e => {
          e.dataTransfer.setData("text/plain", "This text may be dragged");
          window.draggingData = {
            type: "game-button"
          };
          onClose();
        }}
      >
        <h2>彩種按鈕</h2>
      </Banner>
      <Banner
        draggable={true}
        unselectable="on"
        onDragStart={e => {
          e.dataTransfer.setData("text/plain", "This text may be dragged");
          window.draggingData = {
            type: "banner-link"
          };
          onClose();
        }}
      >
        <h2>Banner連結</h2>
      </Banner>
      <Banner
        draggable={true}
        unselectable="on"
        onDragStart={e => {
          e.dataTransfer.setData("text/plain", "This text may be dragged");
          window.draggingData = {
            type: "caor"
          };
          onClose();
        }}
      >
        <h2>輪播</h2>
      </Banner>
    </Drawer>
  );
}
