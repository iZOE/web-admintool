import styled from "styled-components";

export const Banner = styled.div`
  padding: 10px;
  border: 1px solid #ccc;
  height: 60px;
  &:hover {
    border: 2px solid #ccc;
  }
`;
