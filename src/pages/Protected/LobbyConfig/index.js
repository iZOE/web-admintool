import React, { createRef } from "react";
import { PageHeader } from "antd";
import SettingArea from "./SettingArea";
import { Wrapper } from "./Styled";
import FooterAndDrawer from "./FooterAndDrawer";
import { db } from "../../../configs/firebase";

export default function LobbyConfig() {
  const settingAreaRef = createRef();
  function onSave() {
    db.ref("lobby/ios/config").set(settingAreaRef.current);
  }

  return (
    <Wrapper>
      <PageHeader
        style={{
          border: "1px solid rgb(235, 237, 240)"
        }}
        onBack={() => null}
        title="iPhone"
        subTitle="設定iPhone大廳Banner"
      />
      <div className="frame">
        <div className="frame-content">
          <div className="frame-header">
            <div className="frame-header-speaker" />
            <div className="frame-header-camera" />
          </div>
          <div className="container">
            <SettingArea ref={settingAreaRef} />
          </div>
        </div>
      </div>
      <FooterAndDrawer onSave={onSave} />
    </Wrapper>
  );
}
