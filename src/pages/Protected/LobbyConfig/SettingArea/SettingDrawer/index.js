import React, { useState, useEffect } from "react";
import { UploadOutlined } from "@ant-design/icons";
import {
  Form,
  Drawer,
  Button,
  Col,
  Row,
  Select,
  Upload,
  Typography
} from "antd";
import uuidv1 from "uuid/v1";
import { storage } from "../../../../../configs/firebase";

const { Option } = Select;
const { Title, Paragraph } = Typography;

function SettingDrawer({
  form: { getFieldDecorator },
  config,
  onClose,
  onSubmit
}) {
  let submitData = {};

  async function onImageUpload({ onError, onSuccess, file }) {
    const storageRef = storage.ref();
    const metadata = {
      contentType: "image/jpeg"
    }; //a unique name for the image
    const imgFile = storageRef.child(`banners/${uuidv1()}.png`);
    try {
      const image = await imgFile.put(file, metadata);
      const url = await imgFile.getDownloadURL();
      onSuccess(url, image);
    } catch (e) {
      onError(e);
    }
  }

  function onChange(info) {
    if (info.file.status === "done") {
      submitData["imgUrl"] = info.file.response;
      submitData["lotteryCode"] = 1;
    }
  }

  return (
    <Drawer
      title="編輯"
      width={500}
      onClose={onClose}
      visible={config.visible}
      bodyStyle={{ paddingBottom: 80 }}
    >
      <Typography>
        <Title level={4}>圖片預覽</Title>
        <Paragraph>
          <Row gutter={16}>
            <Col span={12}>
              <Upload
                name="file"
                customRequest={onImageUpload}
                onChange={onChange}
              >
                <Button type="button">
                  <UploadOutlined /> Click to Upload
                </Button>
              </Upload>
            </Col>
          </Row>
        </Paragraph>
        <Title level={4}>導航設定</Title>
        <Paragraph>
          <Form layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={16}>
                <Form.Item label="導航類型">
                  {getFieldDecorator("owner", {
                    rules: [{ required: true, message: "請選擇遊戲" }]
                  })(
                    <Select placeholder="請選擇遊戲">
                      <Option value="1">彩票</Option>
                      <Option value="2">第三方遊戲</Option>
                      <Option value="3">一般連結</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col span={16}>
                <Form.Item label="遊戲">
                  {getFieldDecorator("owner", {
                    rules: [{ required: true, message: "請選擇遊戲" }]
                  })(
                    <Select placeholder="請選擇遊戲">
                      <Option value="1">天津時時彩</Option>
                      <Option value="2">歡樂生肖</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Paragraph>
        <div
          style={{
            position: "absolute",
            right: 0,
            bottom: 0,
            width: "100%",
            borderTop: "1px solid #e9e9e9",
            padding: "10px 16px",
            background: "#fff",
            textAlign: "right"
          }}
        >
          <Button onClick={onClose} style={{ marginRight: 8 }}>
            Cancel
          </Button>
          <Button
            onClick={() => onSubmit(config.index, submitData)}
            type="primary"
          >
            Submit
          </Button>
        </div>
      </Typography>
    </Drawer>
  );
}

export default SettingDrawer;
