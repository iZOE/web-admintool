import React, { useState, useEffect, createContext, forwardRef } from "react";
import _ from "lodash";
import RGL, { WidthProvider } from "react-grid-layout";
import uuidv1 from "uuid/v1";
import GridItem from "./GridItem";
import SettingDrawer from "./SettingDrawer";
import { db } from "../../../../configs/firebase";
import { Wrapper } from "./Styled";

export const ReactGridLayout = WidthProvider(RGL);

const config = {
  className: "layout",
  items: 6,
  cols: 4,
  width: 292,
  height: 1000,
  rowHeight: 25
};

export const GridDataCtx = createContext();

function SettingArea(_, ref) {
  const [layout, setLayout] = useState([]);
  const [gridData, setGridData] = useState({});
  const [editDrawer, setEditDrawer] = useState({
    visible: false
  });

  useEffect(async () => {
    const layoutReq = await db.ref("lobby/ios/config").once("value");
    const layoutReqVal = layoutReq.val();
    if (layoutReqVal) {
      setLayout(layoutReqVal.layout);
      setGridData(layoutReqVal.gridData);
      ref.current = layoutReqVal;
    }
  }, []);

  function generateDOM() {
    return layout.map(item => (
      <div key={item.i} data-grid={item} className="grid-box" isResizable>
        <GridItem i={item.i} onOpenEditDrawer={onOpenEditDrawer} />
      </div>
    ));
  }

  function onLayoutChange(layout) {
    setLayout(layout);
  }

  function onOpenEditDrawer({ i }) {
    setEditDrawer({
      visible: true,
      index: i
    });
  }

  function onDrawerClose() {
    setEditDrawer({
      visible: false
    });
  }

  function onSubmit(index, data) {
    setEditDrawer({
      visible: false
    });
    const newGridData = Object.assign({}, gridData);
    newGridData[String(index)] = {
      ...newGridData[String(index)],
      ...data
    };
    setGridData(newGridData);
  }

  function onDrop(elemParams) {
    const newId = uuidv1();
    const draggingData = Object.assign({}, window.draggingData);
    delete window.draggingData;
    const newLayout = layout
      .filter(x => x.i !== "__dropping-elem__")
      .concat([
        {
          x: elemParams.x,
          y: elemParams.y,
          w: 4,
          h: 2,
          i: newId
        }
      ]);
    const newGridData = Object.assign({}, gridData);
    newGridData[newId] = {
      type: draggingData.type
    };
    setGridData(newGridData);
    setLayout(newLayout);
    ref.current = {
      gridData: newGridData,
      layout: newLayout
    };
  }

  return (
    <Wrapper>
      <GridDataCtx.Provider value={gridData}>
        <ReactGridLayout
          onLayoutChange={onLayoutChange}
          onDrop={onDrop}
          isDroppable
          isResizable
          {...config}
        >
          {generateDOM()}
        </ReactGridLayout>
        <SettingDrawer
          config={editDrawer}
          onClose={onDrawerClose}
          onSubmit={onSubmit}
        />
      </GridDataCtx.Provider>
    </Wrapper>
  );
}

export default forwardRef(SettingArea);
