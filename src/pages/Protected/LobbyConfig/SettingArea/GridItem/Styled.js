import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  width: inherit;
  height: inherit;
  border: 1px solid #f7f7f7;
  box-sizing: content-box !important;
  button {
    opacity: 0;
    position: absolute;
    top: 10px;
    left: 10px;
  }

  &:hover {
    border: 1px solid #ccc;
    button {
      opacity: 0.8;
    }
  }
`;
