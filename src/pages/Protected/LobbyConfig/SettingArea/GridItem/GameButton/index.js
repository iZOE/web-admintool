import React from "react";
import { NoData, Wrapper } from "./Styled";

export default function GameButton({ data }) {
  if (!data.lotteryCode) {
    return <NoData>請設置遊戲</NoData>;
  }
  return <Wrapper image={data.imgUrl} />;
}
