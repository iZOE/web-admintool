import styled from "styled-components";

export const Wrapper = styled.div`
  width: inherit;
  height: inherit;
  background-size: cover;
  background-image: url(${props => props.image});
`;

export const NoData = styled.div`
  display: flex;
  background: #fff;
  flex-direction: column;
  justify-content: center;
  height: inherit;
  text-align: center;
`;
