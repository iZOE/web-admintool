import React, { useContext } from "react";
import { SettingOutlined } from "@ant-design/icons";
import { Button } from "antd";
import GameButton from "./GameButton";
import { Wrapper } from "./Styled";
import { GridDataCtx } from "../index";

const VIEW_MAPPER = {
  "game-button": GameButton
};

export default function GridItem({ i, onOpenEditDrawer }) {
  const ctx = useContext(GridDataCtx);
  if (!ctx[i]) {
    return null;
  }

  const ViewComponent = VIEW_MAPPER[ctx[i].type];
  return (
    <Wrapper>
      <Button
        icon={<SettingOutlined />}
        size="small"
        onClick={() => {
          onOpenEditDrawer({
            i: i
          });
        }}
      >
        設置
      </Button>
      <ViewComponent data={ctx[i]} />
    </Wrapper>
  );
}
