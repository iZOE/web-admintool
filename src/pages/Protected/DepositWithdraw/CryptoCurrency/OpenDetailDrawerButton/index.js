import React, { useContext } from 'react'
import { Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import { PageContext } from '../index'

export default function OpenDetailDrawerButton({ id }) {
    const { detailDrawerRef } = useContext(PageContext)

    return (
        <Button
            onClick={() =>
                detailDrawerRef.current.onOpenDrawer({
                    id,
                    isEditMode: true,
                })
            }
            size="small"
            type="link"
        >
            <FormattedMessage id="Share.ActionButton.Edit" description="編輯" />
        </Button>
    )
}
