import React, { createContext, useEffect, createRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { Button, Space } from 'antd';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ReportScaffold from 'components/ReportScaffold';
import * as PERMISSION from 'constants/permissions';
import { COLUMNS_CONFIG } from './datatableConfig';
import DetailDrawer from './DetailDrawer';
import FilterListDrawer from './FilterListDrawer';

const { DEPOSIT_WITHDRAW_CRYPTO_CURRENCY_VIEW, DEPOSIT_WITHDRAW_CRYPTO_CURRENCY_EDIT } = PERMISSION;
export const PageContext = createContext();

export const QUERY_API_URL = '/api/CryptoCurrency/Search';

// 充值清單
const DepositPage = () => {
  let detailDrawerRef = createRef();
  let filterListDrawerRef = createRef();

  const { fetching, dataSource, boundedMutate, onUpdateCondition, onReady, condition } = useGetDataSourceWithSWR({
    url: QUERY_API_URL,
    defaultSortKey: 'CurrencyId',
    autoFetch: true,
  });

  useEffect(() => {
    onReady();
    onUpdateCondition.bySearch();
  }, []);

  return (
    <PageContext.Provider
      value={{
        dataSource,
        boundedMutate,
        detailDrawerRef,
        filterListDrawerRef,
      }}
    >
      <Permission functionIds={[DEPOSIT_WITHDRAW_CRYPTO_CURRENCY_VIEW]}>
        <ReportScaffold
          displayResult={condition}
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              loading={fetching}
              title={<FormattedMessage id="Share.Table.SearchResult" />}
              config={COLUMNS_CONFIG}
              dataSource={dataSource && dataSource.List}
              total={dataSource && dataSource.List.TotalCount}
              rowKey={record => record.Id}
              extendArea={
                <Space>
                  <Permission functionIds={[DEPOSIT_WITHDRAW_CRYPTO_CURRENCY_EDIT]}>
                    <Button
                      type="primary"
                      onClick={() =>
                        detailDrawerRef.current.onOpenDrawer({
                          isEditMode: false,
                        })
                      }
                    >
                      <FormattedMessage id="Share.ActionButton.Create" description="新增" />
                    </Button>
                  </Permission>
                </Space>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
        <DetailDrawer ref={detailDrawerRef} />
        <FilterListDrawer ref={filterListDrawerRef} />
      </Permission>
    </PageContext.Provider>
  );
};

export default DepositPage;
