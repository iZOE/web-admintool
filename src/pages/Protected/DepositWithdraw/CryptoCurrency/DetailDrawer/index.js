import React, {
  useState,
  forwardRef,
  useEffect,
  useContext,
  useMemo,
} from 'react';
import {
  Popconfirm,
  Form,
  message,
  InputNumber,
  Switch,
  Button,
  Space,
} from 'antd';
import useSWR from 'swr';
import axios from 'axios';
import { useIntl, FormattedMessage } from 'react-intl';
import caller from 'utils/fetcher';
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect';
import BasicDrawerWrapper from 'layouts/BasicDrawer';
import { PageContext } from '../index';
import { RULES, getMinimum } from 'constants/activity/validators';

const { useForm } = Form;

export function DetailDrawer({}, ref) {
  const intl = useIntl();
  const [form] = useForm();
  const [visible, setVisible] = useState(false);
  const [submitButtonStauts, setSubmitButtonStauts] = useState(false);
  const { boundedMutate } = useContext(PageContext);

  const [detail, setDetail] = useState({
    isEditMode: false,
  });
  const { isEditMode } = detail;

  ref.current = {
    onOpenDrawer,
  };

  function onOpenDrawer({ isEditMode, id }) {
    setVisible(true);
    setDetail({
      id,
      isEditMode,
    });
  }

  function onCloseDrawer() {
    setVisible(false);
    form.resetFields();
  }

  const { data = null } = useSWR(
    detail.id ? `/api/CryptoCurrency/${detail.id}` : null,
    url => axios(url).then(res => res && res.data.Data)
  );

  useEffect(() => {
    if (data) {
      form.setFieldsValue({
        ...data,
        CryptoProtocol: data.CryptoProtocolId,
      });
      setSubmitButtonStauts(!form.getFieldValue('ExchangeRate'));
    } else {
      form.resetFields();
    }
  }, [data, visible]);

  const successMsg = intl.formatMessage({
    id: 'Share.SuccessMessage.UpdateSuccess',
  });

  const onDrawerSave = values => {
    setVisible(false);
    caller({
      method: isEditMode ? 'put' : 'post',
      endpoint: isEditMode
        ? `/api/CryptoCurrency/${detail.id}`
        : '/api/CryptoCurrency',
      body: { ...values },
    })
      .then(() => {
        message.success(successMsg, 5);
      })
      .then(() => {
        onCloseDrawer();
        form.resetFields();
        boundedMutate();
      });
  };

  const rules = useMemo(() => {
    let min = getMinimum(0.01);
    return [...RULES.REQUIRED, ...min];
  }, []);

  return (
    <BasicDrawerWrapper
      width={500}
      headerStyle={{ border: 'none' }}
      title={
        isEditMode ? (
          <FormattedMessage id="Share.CommonKeys.Edit" />
        ) : (
          <FormattedMessage id="Share.CommonKeys.Create" />
        )
      }
      visible={visible}
      onClose={onCloseDrawer}
      footer={
        <div style={{ textAlign: 'right' }}>
          <Space>
            <Button htmlType="button" onClick={onCloseDrawer}>
              <FormattedMessage
                id="Share.ActionButton.Cancel"
                description="取消"
              />
            </Button>
            <Popconfirm
              placement="topRight"
              title={
                <FormattedMessage
                  id="PageCryptoCurrency.Popconfirm.Title"
                  values={{ br: <br /> }}
                />
              }
              onConfirm={() => form.submit()}
              okText={<FormattedMessage id="Share.ActionButton.Confirm" />}
              cancelText={<FormattedMessage id="Share.ActionButton.Cancel" />}
              overlayStyle={{ width: '300px' }}
            >
              <Button type="primary" disabled={submitButtonStauts}>
                <FormattedMessage
                  id="Share.ActionButton.Submit"
                  description="提交"
                />
              </Button>
            </Popconfirm>
          </Space>
        </div>
      }
    >
      <Form
        form={form}
        labelCol={{
          xs: { span: 24 },
          sm: { span: 4 },
        }}
        wrapperCol={{ xs: { span: 24 }, sm: { span: 20 } }}
        onValuesChange={changedValues => {
          if (Object.keys(changedValues)[0] === 'ExchangeRate') {
            setSubmitButtonStauts(!changedValues?.ExchangeRate);
          }
        }}
        onFinish={onDrawerSave}
        initialValues={{
          ExchangeRate: 1,
        }}
      >
        <FormItemsSimpleSelect
          url="/api/Option/CryptoCurrency"
          name="CurrencyId"
          label={
            <FormattedMessage id="PageCryptoCurrency.DataTable.Title.CurrencyName" />
          }
          placeholder={
            <FormattedMessage
              id="Share.PlaceHolder.Switch.Default"
              description="请选择"
            />
          }
          disabled={isEditMode && true}
        />
        <FormItemsSimpleSelect
          url="/api/Option/CryptoCurrency/CryptoProtocol"
          name="CryptoProtocol"
          label={
            <FormattedMessage id="PageCryptoCurrency.DataTable.Title.CryptoProtocol" />
          }
          placeholder={
            <FormattedMessage
              id="Share.PlaceHolder.Switch.Default"
              description="请选择"
            />
          }
          disabled={isEditMode && true}
        />
        <Form.Item
          label={
            <FormattedMessage id="PageCryptoCurrency.DataTable.Title.ExchangeRate" />
          }
          name="ExchangeRate"
          rules={rules}
        >
          <InputNumber type="number" step={0.01} />
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage id="PageCryptoCurrency.DataTable.Title.MinWithdrawAmount" />
          }
          name="MinWithdrawAmount"
          rules={[...RULES.REQUIRED]}
        >
          <InputNumber
            min={0}
            max={999999999.99}
            step={0.01}
            formatter={value =>
              `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
            }
            parser={value => value.replace(/\$\s?|(,*)/g, '')}
            style={{ width: '160px' }}
          />
        </Form.Item>
        <Form.Item
          label={
            <FormattedMessage id="PageCryptoCurrency.DataTable.Title.IsEnable" />
          }
          name="IsEnable"
          valuePropName="checked"
        >
          <Switch
            checkedChildren={
              <FormattedMessage id="Share.SwitchButton.IsEnable.Yes" />
            }
            unCheckedChildren={
              <FormattedMessage id="Share.SwitchButton.IsEnable.No" />
            }
          />
        </Form.Item>
      </Form>
    </BasicDrawerWrapper>
  );
}

export default forwardRef(DetailDrawer);
