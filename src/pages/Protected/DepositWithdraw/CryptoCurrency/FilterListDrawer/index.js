import React, { useState, forwardRef } from 'react';
import {
  Drawer,
  Descriptions,
  Form,
  Row,
  Col,
  DatePicker,
  Input,
  Table,
} from 'antd';
import { FormattedMessage } from 'react-intl';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import { FORMAT, DEFAULT } from 'constants/dateConfig';
import PleaseSearch from 'components/PleaseSearch';
import QueryButton from 'components/FormActionButtons/Query';
import DateWithFormat from 'components/DateWithFormat';
import { getISODateTimeString } from 'mixins/dateTime';

const COLUMN_CONFIG = [
  {
    title: (
      <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />
    ),
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage id="PageCryptoCurrency.ChangeLog.Fields.ModifiedUserName" />
    ),
    dataIndex: 'ModifiedUserName',
    key: 'ModifiedUserName',
  },
  {
    title: (
      <FormattedMessage id="PageCryptoCurrency.ChangeLog.Fields.PropertyNameText" />
    ),
    dataIndex: 'PropertyNameText',
    key: 'PropertyNameText',
  },
  {
    title: (
      <FormattedMessage id="PageCryptoCurrency.ChangeLog.Fields.OldValue" />
    ),
    dataIndex: 'OldValue',
    key: 'OldValue',
  },
  {
    title: (
      <FormattedMessage id="PageCryptoCurrency.ChangeLog.Fields.NewValue" />
    ),
    dataIndex: 'NewValue',
    key: 'NewValue',
  },
];

export function FilterListDrawer({}, ref) {
  const [visible, setVisible] = useState(false);
  const [logId, setLogId] = useState(null);

  ref.current = {
    onOpenDrawer,
  };

  function onOpenDrawer({ id }) {
    setLogId(id);
    setVisible(true);
  }

  function onCloseDrawer() {
    setVisible(false);
  }

  const {
    fetching,
    dataSource,
    onUpdateCondition,
    condition,
  } = useGetDataSourceWithSWR({
    url: `/api/CryptoCurrency/${logId}/ChangeLog`,
    autoFetch: true,
  });

  function onFinish({ Date, ...restValues }) {
    const [start, end] = Date || [null, null];

    onUpdateCondition.bySearch({
      ...restValues,
      StartTime: getISODateTimeString(start),
      EndTime: getISODateTimeString(end),
    });
  }

  const { RangePicker } = DatePicker;

  const CONDITION_INITIAL_VALUE = {
    Date: DEFAULT.RANGE.FROM_TODAY_TO_TODAY,
  };

  return (
    <Drawer
      width={700}
      headerStyle={{ border: 'none' }}
      title={
        <FormattedMessage
          id="Share.CommonDrawer.FilterList.Tabs.Report"
          description="異動紀錄"
        />
      }
      visible={visible}
      onClose={onCloseDrawer}
    >
      <Descriptions
        title={<FormattedMessage id="Share.QueryCondition.Title" />}
        column={1}
      >
        <Descriptions.Item>
          <Form onFinish={onFinish} initialValues={CONDITION_INITIAL_VALUE}>
            <Row gutter={24}>
              <Col span={20}>
                <Form.Item
                  label={
                    <FormattedMessage id="Share.QueryCondition.DateRange" />
                  }
                  name="Date"
                >
                  <RangePicker
                    showTime
                    bordered={false}
                    format={FORMAT.DISPLAY.DEFAULT}
                  />
                </Form.Item>
                <Form.Item hidden name="Type">
                  <Input type="hidden" />
                </Form.Item>
              </Col>
              <Col span={4}>
                <QueryButton />
              </Col>
            </Row>
          </Form>
        </Descriptions.Item>
      </Descriptions>
      <PleaseSearch display={condition}>
        <Descriptions
          title={<FormattedMessage id="Share.Table.SearchResult" />}
          column={1}
        >
          <Descriptions.Item className="merged-column">
            <Table
              loading={fetching}
              columns={COLUMN_CONFIG}
              dataSource={dataSource && dataSource.List}
              rowKey={record => record.ModifyTime}
              pagination={false}
              size="small"
              style={{ width: '100%' }}
            />
          </Descriptions.Item>
        </Descriptions>
      </PleaseSearch>
    </Drawer>
  );
}

export default forwardRef(FilterListDrawer);
