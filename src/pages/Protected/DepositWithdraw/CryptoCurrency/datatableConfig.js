import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Badge } from 'antd';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenDepositDetailDrawerButton from './OpenDetailDrawerButton';
import OpenFilterListDrawerButton from './OpenFilterListDrawerButton';
import { NO_DATA } from 'constants/noData';
import { MODE } from 'constants/mode';

export const COLUMNS_CONFIG = [
  {
    title: (
      <FormattedMessage
        id="PageCryptoCurrency.DataTable.Title.CurrencyName"
        description="幣別"
      />
    ),
    dataIndex: 'CurrencyName',
    key: 'CurrencyName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => (value ? value : NO_DATA),
  },
  {
    title: (
      <FormattedMessage
        id="PageCryptoCurrency.DataTable.Title.CryptoProtocol"
        description="錢包協議"
      />
    ),
    dataIndex: 'CryptoProtocol',
    key: 'CryptoProtocol',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: (
      <FormattedMessage
        id="PageCryptoCurrency.DataTable.Title.ExchangeRate"
        description="匯率"
      />
    ),
    dataIndex: 'ExchangeRate',
    key: 'ExchangeRate',
  },
  {
    title: (
      <FormattedMessage
        id="PageCryptoCurrency.DataTable.Title.MinWithdrawAmount"
        description="提領最低金額"
      />
    ),
    dataIndex: 'MinWithdrawAmount',
    key: 'MinWithdrawAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: (
      <FormattedMessage
        id="PageCryptoCurrency.DataTable.Title.IsEnable"
        description="狀態"
      />
    ),
    dataIndex: 'IsEnable',
    key: 'IsEnable',
    render: (text, record, index) => (
      <Badge
        status={record.IsEnable ? 'success' : 'error'}
        text={
          <FormattedMessage
            id={`PageCryptoCurrency.DataTable.Value.IsEnable.${record.IsEnable}`}
            description="狀態"
          />
        }
      />
    ),
  },
  {
    title: (
      <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />
    ),
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage
        id="Share.CommonKeys.UpdateMember"
        description="管理人員"
      />
    ),
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
  },

  {
    title: (
      <FormattedMessage id="Share.ActionButton.Manage" description="管理" />
    ),
    dataIndex: 'action',
    key: 'action',
    render: (_1, { Id }, _2) => {
      return (
        <>
          <OpenDepositDetailDrawerButton id={Id} mode={MODE.EDIT} />
          <OpenFilterListDrawerButton id={Id} />
        </>
      );
    },
  },
];
