import React, { useContext } from 'react'
import { Button } from 'antd'
import { PageContext } from '../index'
import { FormattedMessage } from 'react-intl'

export default function OpenFilterListDrawerButton({ id }) {
    const { filterListDrawerRef } = useContext(PageContext)

    return (
        <Button
            onClick={() => filterListDrawerRef.current.onOpenDrawer({ id })}
            size="small"
            type="link"
        >
            <FormattedMessage
                id="Share.CommonDrawer.FilterList.Tabs.Report"
                description="異動紀錄"
            />
        </Button>
    )
}
