import React, { createContext } from 'react';
import { useLocation } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import queryString from 'query-string';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import ExportReportButton from 'components/ExportReportButton';
import ReportScaffold from 'components/ReportScaffold';
import Permission from 'components/Permission';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import SummaryView from './Summary';

const { DEPOSIT_WITHDRAW_WITHDRAW_EXPORT } = PERMISSION;

export const PageContext = createContext();

export const QUERY_API_URL = '/api/Withdrawal/Search';

// 充值清單
const WithdrawPage = props => {
  const location = useLocation();
  let search = queryString.parse(location.search);
  search = Object.keys(search).reduce(
    (result, key) => ({
      ...result,
      [key]: isNaN(search[key]) ? search[key] : Number(search[key]),
    }),
    {}
  );

  const { fetching, dataSource, boundedMutate, onUpdateCondition, onReady, condition } = useGetDataSourceWithSWR({
    url: QUERY_API_URL,
    defaultSortKey: 'ModifyTime',
    autoFetch: true,
    initialCondition: Object.values(search).length ? search : null,
  });

  return (
    <PageContext.Provider value={{ dataSource, boundedMutate }}>
      <ReportScaffold
        displayResult={condition}
        conditionComponent={
          <Condition
            condition={condition}
            onReady={onReady}
            onUpdate={onUpdateCondition}
            initialValueForTrigger={{ ...search }}
          />
        }
        summaryComponent={<SummaryView summary={dataSource && dataSource.Summary} />}
        datatableComponent={
          <DataTable
            displayResult={condition}
            condition={condition}
            loading={fetching}
            title={<FormattedMessage id="Share.Table.SearchResult" />}
            config={COLUMNS_CONFIG}
            dataSource={dataSource && dataSource.List.Data}
            total={dataSource && dataSource.List.TotalCount}
            rowKey={record => record.WithdrawalNo}
            extendArea={
              <Permission functionIds={[DEPOSIT_WITHDRAW_WITHDRAW_EXPORT]}>
                <ExportReportButton condition={condition} actionUrl="/api/Withdrawal/Export" />
              </Permission>
            }
            onUpdate={onUpdateCondition}
          />
        }
      />
    </PageContext.Provider>
  );
};

export default WithdrawPage;
