import React, { useState, useMemo } from 'react';
import useSWR from 'swr';
import axios from 'axios';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  Button,
  Col,
  Descriptions,
  Divider,
  message,
  Skeleton,
  Form,
  Input,
  InputNumber,
  Row,
  Table,
  Select,
  Space,
} from 'antd';
import { AccountBookOutlined } from '@ant-design/icons';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';

const Tab5WalletBalance = ({ memberName }) => {
  const intl = useIntl();
  const [isLoading, setIsLoading] = useState(-1);
  const [gameProviderBalance, setGameProviderBalance] = useState(null);

  const { data: detailData } = useSWR(
    `/api/Withdrawal/${memberName}/Wallets`,
    url => axios(url).then(res => res.data.Data)
  );

  const gamesBalance = useMemo(() => {
    if (detailData?.BalanceDetails) {
      return detailData.BalanceDetails.reduce(
        (acc, curr) => ({
          ...acc,
          [curr.GameProviderId]: curr,
        }),
        {}
      );
    }
    return [];
  }, [detailData, gameProviderBalance]);

  const getBalance = GameProviderId => {
    setIsLoading(GameProviderId);
    axios(`/api/Withdrawal/${memberName}/${GameProviderId}/Wallet`)
      .then(res => setGameProviderBalance(res.data.Data))
      .finally(() => setIsLoading(-1));
  };

  const renderThirdPartyGameBalance = balanceDetails =>
    Object.keys(balanceDetails).map(key => {
      const {
        GameProviderId,
        Balance,
        GameProviderName,
        Remark,
      } = balanceDetails[key];
      return (
        <Descriptions.Item
          key={GameProviderId}
          label={
            <>
              {Remark ? Remark : GameProviderName}
              <FormattedMessage
                id={'PageWithdraw.WithdrawRecordDetailModal.Balance'}
                description="餘額"
              />
            </>
          }
        >
          {Number(Balance) === -1 ? (
            <Button
              type="primary"
              icon={<AccountBookOutlined />}
              size="small"
              loading={isLoading === GameProviderId}
              onClick={() => getBalance(GameProviderId)}
            >
              <FormattedMessage id="MemberDetail.Tabs.AccountSetting.Action.UpdateBalace" />
            </Button>
          ) : (
            <ReactIntlCurrencyWithFixedDecimal value={Balance} />
          )}
        </Descriptions.Item>
      );
    });

  if (!detailData) {
    return <Skeleton active />;
  }
  return (
    <>
      <Descriptions
        title={intl.formatMessage({
          id: 'PageWithdraw.WithdrawRecordDetailModal.CentralWalletTitle',
        })}
      >
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.Tab5.MasterAccountBalance"
              description="主帐户余额"
            />
          }
        >
          {detailData?.BalanceAmount ||
          Number(detailData?.BalanceAmount) === 0 ? (
            <ReactIntlCurrencyWithFixedDecimal
              value={detailData.BalanceAmount}
            />
          ) : (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.NotGet" />
          )}
        </Descriptions.Item>
      </Descriptions>
      <Descriptions
        column={2}
        title={intl.formatMessage({
          id: 'PageWithdraw.WithdrawRecordDetailModal.OtherAccountTitle',
        })}
      >
        {renderThirdPartyGameBalance(gamesBalance)}
      </Descriptions>
      <Divider />
    </>
  );
};

export default Tab5WalletBalance;
