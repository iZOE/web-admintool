import React, { useCallback } from 'react'
import { Descriptions, Divider, message, Skeleton, Table } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import axios from 'axios'
import useSWR from 'swr'

const Fetcher = (intl, url) => () => {
    return axios
        .get(url)
        .then(response => {
            if (response.data.ErrorKey) {
                message.error(
                    `${intl.formatMessage({
                        id: 'Share.ErrorMessage.ErrorKey.JohnsonApiException',
                    })}: ${response.ErrorKey}`
                )
            } else {
                return response.data.Data
            }
        })
        .catch(axiosErr => {
            message.error(
                `${intl.formatMessage({
                    id: 'Share.ErrorMessage.ErrorKey.JohnsonApiException',
                })}: ${axiosErr.message.status}`
            )
        })
}

const loginHistoryTableColumns = [
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab2.LoginHistory.LoginTime" />
        ),
        dataIndex: 'LoginTime',
        key: 'LoginTime',
        width: 200,
        fixed: true,
        render: (_1, { LoginTime: time }, _2) => <DateWithFormat time={time} />,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab2.LoginHistory.DeviceModel" />
        ),
        dataIndex: 'DeviceModel',
        key: 'DeviceModel',
        fixed: true,
        render: text => text,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab2.LoginHistory.IpAddress" />
        ),
        dataIndex: 'IpAddress',
        key: 'IpAddress',
        fixed: true,
        render: text => text,
    },
]

const LoginHistoryTables = ({ isLoginHistoryFetching, loginHistoryData }) => {
    if (isLoginHistoryFetching || !loginHistoryData) {
        return <Skeleton active />
    } else {
        return (
            <Table
                columns={loginHistoryTableColumns}
                rowKey={d => d.LoginTime}
                dataSource={loginHistoryData}
                pagination={false}
            />
        )
    }
}

function Tab2_VariousRecord({ memberName }) {
    const intl = useIntl()

    const SummarizeURL = `/api/Withdrawal/Member/${memberName}/Summarize` // get
    const LoginHistoryURL = `/api/Withdrawal/Member/${memberName}/LoginHistory` // get
    const summarizeFetcher = useCallback(Fetcher(intl, SummarizeURL))
    const loginHistoryFetcher = useCallback(Fetcher(intl, LoginHistoryURL))

    const { data: summarizeData, isValidating: isSummarizeFetching } = useSWR(
        SummarizeURL,
        summarizeFetcher
    )
    const {
        data: loginHistoryData,
        isValidating: isLoginHistoryFetching,
    } = useSWR(LoginHistoryURL, loginHistoryFetcher)
    const Last = summarizeData && summarizeData.Last
    const AccumulativeAmount = summarizeData && summarizeData.AccumulativeAmount

    return (
        <section>
            {isSummarizeFetching || !Last ? (
                <Skeleton active />
            ) : (
                <>
                    <Descriptions
                        title={
                            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab2.Last.LastTitle" />
                        }
                    >
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.Last.BetAmount"
                                    description="总投注金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={Last.BetAmount}
                            />
                        </Descriptions.Item>

                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.Last.BonusAmount"
                                    description="总赢得金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={Last.BonusAmount}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.Last.WinLoss"
                                    description="平台输赢"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={Last.WinLoss}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.Last.DepositAmount"
                                    description="总充值金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={Last.DepositAmount}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.Last.WithdrawalAmount"
                                    description="申请提现金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={Last.BetAmount}
                            />
                        </Descriptions.Item>

                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.Last.DepositWithdrawalDiff"
                                    description="充提差额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={Last.DepositWithdrawalDiff}
                            />
                        </Descriptions.Item>
                    </Descriptions>
                </>
            )}
            <Divider />
            {isLoginHistoryFetching || !AccumulativeAmount ? (
                <Skeleton active />
            ) : (
                <>
                    <Descriptions
                        title={
                            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab2.AccumulativeAmount.AccumulativeAmountTitle" />
                        }
                    >
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.AccumulativeAmount.BetAmount"
                                    description="总投注金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={AccumulativeAmount.BetAmount}
                            />
                        </Descriptions.Item>

                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.AccumulativeAmount.BonusAmount"
                                    description="总赢得金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={AccumulativeAmount.BonusAmount}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.AccumulativeAmount.WinLoss"
                                    description="平台输赢"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={AccumulativeAmount.WinLoss}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.AccumulativeAmount.DepositAmount"
                                    description="总充值金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={AccumulativeAmount.DepositAmount}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.AccumulativeAmount.WithdrawalAmount"
                                    description="申请提现金额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={AccumulativeAmount.BetAmount}
                            />
                        </Descriptions.Item>

                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageWithdraw.WithdrawRecordDetailModal.Tab2.AccumulativeAmount.DepositWithdrawalDiff"
                                    description="充提差额"
                                />
                            }
                            span="3"
                        >
                            <ReactIntlCurrencyWithFixedDecimal
                                value={AccumulativeAmount.DepositWithdrawalDiff}
                            />
                        </Descriptions.Item>
                    </Descriptions>
                    <Divider />

                    <Descriptions
                        title={
                            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab2.LoginHistory.LoginHistoryTitle" />
                        }
                    />
                    <LoginHistoryTables
                        isLoginHistoryFetching={isLoginHistoryFetching}
                        loginHistoryData={loginHistoryData}
                    />
                </>
            )}
        </section>
    )
}

Tab2_VariousRecord.propTypes = {}

export default Tab2_VariousRecord
