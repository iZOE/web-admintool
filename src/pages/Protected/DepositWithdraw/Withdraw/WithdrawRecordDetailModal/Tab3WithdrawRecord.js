import React, { useMemo } from 'react'
import { Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'
import { Decimal } from 'decimal.js'
import styled from 'styled-components'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import DataTable from 'components/DataTable'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'

const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.CreateTime" />
        ),
        dataIndex: 'CreateTime',
        key: 'CreateTime',
        width: 200,
        sorter: (a, b) => {
            return
        },
        render: (_1, { CreateTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.RequestAmount" />
        ),
        dataIndex: 'RequestAmount',
        key: 'RequestAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.RealAmount" />
        ),
        dataIndex: 'RealAmount',
        key: 'RealAmount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.BankName" />
        ),
        dataIndex: 'BankName',
        key: 'BankName',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        render: text => text,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.AccountName" />
        ),
        dataIndex: 'AccountName',
        key: 'AccountName',
        sorter: (a, b) => {
            return
        },
        render: text => text,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.CardNumber" />
        ),
        dataIndex: 'CardNumber',
        key: 'CardNumber',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        render: text => text,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.ModifyUserAccount" />
        ),
        dataIndex: 'ModifyUserAccount',
        key: 'ModifyUserAccount',
        isShow: true,
        sorter: (a, b) => {
            return
        },
        render: text => text,
    },
]

const EasySummaryWrapper = styled.div`
    display: flex;
    justify-content: space-around;
`

const CONDITION_INITIAL_VALUE = {
    sortInfo: { SortColumn: 'LoginTime', SortBy: 'Desc' },
}

function Tab3WithdrawRecord({ memberName }) {
    const getWithDrawHistoryURL = `/api/Withdrawal/Member/${memberName}/History` // post

    const {
        fetching,
        dataSource: withdrawHistoryData,
        onUpdateCondition,
    } = useGetDataSourceWithSWR({
        url: getWithDrawHistoryURL,
        autoFetch: true,
        noCondition: true,
        initialCondition: CONDITION_INITIAL_VALUE,
    })

    const [
        accumulativeWithdrawaTimes,
        accumulativeWithdrawaAmount,
    ] = useMemo(() => {
        if (!withdrawHistoryData) {
            return [null, null]
        }
        const accumulativeWithdrawaTimes = withdrawHistoryData.length
        const accumulativeWithdrawaAmount = withdrawHistoryData
            .reduce((acc, curr) => acc.plus(curr.RealAmount), new Decimal(0))
            .toFixed(2, 1)
        return [accumulativeWithdrawaTimes, accumulativeWithdrawaAmount]
    }, [withdrawHistoryData])

    return fetching || !withdrawHistoryData ? (
        <Skeleton active />
    ) : (
        <>
            <EasySummaryWrapper>
                <div>
                    <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.AccumulativeWithdrawaTimes" />
                    ：{accumulativeWithdrawaTimes}
                </div>
                <div>
                    <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab3.AccumulativeWithdrawaAmount" />
                    ：{accumulativeWithdrawaAmount}
                </div>
            </EasySummaryWrapper>
            <DataTable
                config={COLUMNS_CONFIG}
                dataSource={withdrawHistoryData}
                onUpdate={onUpdateCondition}
            />
        </>
    )
}

Tab3WithdrawRecord.propTypes = {}

export default Tab3WithdrawRecord
