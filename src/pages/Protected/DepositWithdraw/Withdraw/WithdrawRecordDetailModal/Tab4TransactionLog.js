import React, { useCallback } from 'react'
import { message, Skeleton, Table } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import axios from 'axios'
import useSWR from 'swr'
import DateWithFormat from 'components/DateWithFormat'

const Fetcher = (intl, url) => () => {
    return axios
        .get(url)
        .then(response => {
            if (response.data.ErrorKey) {
                message.error(
                    `${intl.formatMessage({
                        id: 'Share.ErrorMessage.ErrorKey.JohnsonApiException',
                    })}: ${response.ErrorKey}`
                )
            } else {
                return response.data.Data
            }
        })
        .catch(axiosErr => {
            message.error(
                `${intl.formatMessage({
                    id: 'Share.ErrorMessage.ErrorKey.JohnsonApiException',
                })}: ${axiosErr.message.status}`
            )
        })
}

const lastTransactionTableColumns = [
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab4.TransactionTime" />
        ),
        dataIndex: 'TransactionTime',
        key: 'TransactionTime',
        fixed: true,
        width: 200,
        render: (_1, { TransactionTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab4.UserName" />
        ),
        dataIndex: 'UserName',
        key: 'UserName',
        fixed: true,
        render: text => text,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab4.TypeName" />
        ),
        dataIndex: 'TypeName',
        key: 'TypeName',
        fixed: true,
        render: text => text,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab4.BeforeValue" />
        ),
        dataIndex: 'BeforeValue',
        key: 'BeforeValue',
        fixed: true,
        render: text => text,
    },
    {
        title: (
            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab4.AfterValue" />
        ),
        dataIndex: 'AfterValue',
        key: 'AfterValue',
        fixed: true,
        render: text => text,
    },
]

function Tab4TransactionLog({ recordId }) {
    const intl = useIntl()
    const getTransactionLogURL = `/api/Withdrawal/transactionLog/${recordId}` // post
    const TransactionLogFetcher = useCallback(
        Fetcher(intl, getTransactionLogURL)
    )

    const {
        data: transactionLogData,
        isValidating: isTransactionLogFetching,
    } = useSWR(getTransactionLogURL, TransactionLogFetcher)

    return (
        <section>
            {isTransactionLogFetching || !transactionLogData ? (
                <Skeleton active />
            ) : (
                <>
                    {transactionLogData.length > 0 && (
                        <div>
                            <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.Tab4.LastTransactionTime" />
                            ：
                            <DateWithFormat
                                time={transactionLogData[0].TransactionTime}
                            />
                        </div>
                    )}
                    <Table
                        columns={lastTransactionTableColumns}
                        rowKey={d => d.TransactionTime}
                        dataSource={transactionLogData}
                        pagination={false}
                    />
                </>
            )}
        </section>
    )
}

Tab4TransactionLog.propTypes = {}

export default Tab4TransactionLog
