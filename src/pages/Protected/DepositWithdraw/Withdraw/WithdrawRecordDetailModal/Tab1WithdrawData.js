import React, { useContext, useState } from 'react';
import useSWR from 'swr';
import axios from 'axios';
import { Link } from 'react-router-dom';
import DateWithFormat from 'components/DateWithFormat';
import { FormattedMessage, useIntl } from 'react-intl';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import { EditOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons';
import {
  Button,
  Col,
  Descriptions,
  Divider,
  message,
  Skeleton,
  Form,
  Input,
  InputNumber,
  Row,
  Table,
  Select,
  Space,
} from 'antd';
import uid from 'utils/uid';
import { PageContext } from '../index';
import { MODAL_TYPE } from './index';

const { TextArea } = Input;
const { Option } = Select;

const WITHDRAWL_TYPE = {
  BANKCARD: 1,
  VIRTUAL_CURRENCY: 2,
};

const mutateTheDataRowByOrderId = (clonedSource, orderId, newValue) => {
  clonedSource.List.Data.forEach(d => {
    if (d.WithdrawalNo === orderId) {
      // 改成  2或4 2: 通過, 4: 拒絕 那一筆資料就不會再出現管理按鈕
      d = newValue;
    }
  });
  return clonedSource;
};

const BankInfoTables = ({ detailData }) => {
  detailData[0].uid = uid();
  const BANK_INFO_COLUMNS_CONFIG = [
    {
      title: (
        <FormattedMessage
          id="PageWithdraw.WithdrawRecordDetailModal.BankName"
          description="银行"
        />
      ),
      dataIndex: 'BankName',
      key: 'BankName',
      fixed: true,
      render: text => text,
    },
    {
      title: (
        <FormattedMessage
          id="PageWithdraw.WithdrawRecordDetailModal.BankBranch"
          description="开户支行"
        />
      ),
      dataIndex: 'BankBranch',
      key: 'BankBranch',
      render: text => text,
    },
    {
      title: (
        <FormattedMessage
          id="PageWithdraw.WithdrawRecordDetailModal.AccountName"
          description="户名"
        />
      ),
      dataIndex: 'AccountName',
      key: 'AccountName',
      render: text => text,
    },
    {
      title: (
        <FormattedMessage
          id="PageWithdraw.WithdrawRecordDetailModal.CardNumber"
          description="入款账户"
        />
      ),
      dataIndex: 'CardNumber',
      key: 'CardNumber',
      render: text => text,
    },
  ];

  if (detailData[0].WithdrawalTypeId === WITHDRAWL_TYPE.VIRTUAL_CURRENCY) {
    BANK_INFO_COLUMNS_CONFIG[1] = {
      title: (
        <FormattedMessage
          id="PageWithdraw.WithdrawRecordDetailModal.WalletNickName"
          description="钱包昵称"
        />
      ),
      dataIndex: 'WalletNickName',
      key: 'WalletNickName',
      uid: uid(),
    };
  }

  return (
    <Table
      size="small"
      bordered
      columns={BANK_INFO_COLUMNS_CONFIG}
      rowKey={d => d.uid}
      dataSource={detailData}
      pagination={false}
    />
  );
};

function Tab1WithdrawData({ visible, withdrawRecordId, onOk, record, type }) {
  const [formInstance] = Form.useForm();
  const [beforeUpdateWithdrawalFee, setBeforeUpdateWithdrawalFee] = useState(
    null
  ); // 為了讓按下取消時能回到原來的值而使用
  const intl = useIntl();
  const { data: detailData } = useSWR(
    visible ? `/api/Withdrawal/${withdrawRecordId}/Data` : null,
    url => axios(url).then(res => res.data.Data)
  );

  const [isFeeUpdate, setIsFeeUpdate] = useState(false);

  const { dataSource, boundedMutate } = useContext(PageContext);

  // 管理欄 按下畫面上的交易確認 出現的Modal裡的 發起 時
  const onIssueOrderConfirm = values => {
    axios
      .patch(`/api/Withdrawal/${withdrawRecordId}/Status`, values)
      .then(res => {
        boundedMutate(
          mutateTheDataRowByOrderId(
            JSON.parse(JSON.stringify(dataSource)),
            withdrawRecordId,
            values
          ),
          true // 重新查詢還是有必要，不然user會覺得畫面資料有錯
        );
        message.success(
          intl.formatMessage({
            id: 'Share.SuccessMessage.UpdateSuccess',
          })
        );
        onOk();
      })
      .catch(err => {
        message.error(
          `${intl.formatMessage({
            id: 'Share.ErrorMessage.ErrorKey.JohnsonApiException',
          })}: ${err.message.status}`
        );
      });
  };
  if (!detailData) {
    return <Skeleton active />;
  }
  return (
    <Form
      initialValues={{ ...detailData, Status: 2 }}
      onFinish={onIssueOrderConfirm}
      form={formInstance}
    >
      <Descriptions>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.WithdrawID"
              description="訂單ID"
            />
          }
          span={3}
        >
          {detailData.WithdrawalNo}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.CreateTime"
              description="建立时间"
            />
          }
        >
          <DateWithFormat time={record.CreateTime} />
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.Status"
              description="申請狀態"
            />
          }
        >
          <FormattedMessage
            id={`PageWithdraw.Conditions.StatusOptions.${record.Status}`}
            description="申請狀態子項目"
          />
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.ModifyTime"
              description="更新时间"
            />
          }
        >
          <DateWithFormat time={record.ModifyTime} />
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.MemberName"
              description="会员帐号"
            />
          }
        >
          {record.MemberName}
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.NickName"
              description="名稱"
            />
          }
        >
          {record.NickName}
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.MemberType"
              description="会员类型"
            />
          }
        >
          <FormattedMessage
            id={`Share.MemberType.${detailData.MemberTypeId}`}
            description="会员类型子項目"
          />
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.MemberLevel"
              description="会员等級"
            />
          }
        >
          <FormattedMessage
            id={`PageOperationSMSManagement.MemberLevelList.${detailData.MemberLevelId}`}
            description="会员等級子項目"
          />
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.CurrencyName"
              description="币别"
            />
          }
        >
          {detailData.CurrencyName}
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.ExchangeRate"
              description="当下汇率"
            />
          }
        >
          <ReactIntlCurrencyWithFixedDecimal value={detailData.ExchangeRate} />
        </Descriptions.Item>
      </Descriptions>
      <Divider />
      <Descriptions
        title={intl.formatMessage({
          id: 'PageWithdraw.WithdrawRecordDetailModal.WithdrawAmountTitle',
        })}
      >
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.PreferentialAmount"
              description="优惠扣除"
            />
          }
        >
          <Link
            to={`/deposit-withdraw/turnover?MemberName=${record.MemberName}&DoQuery=true`}
            target="_blank"
          >
            <ReactIntlCurrencyWithFixedDecimal
              value={detailData.PreferentialAmount}
            />
          </Link>
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.AdministrationFee"
              description="行政费"
            />
          }
        >
          <Link
            to={`/deposit-withdraw/turnover?MemberName=${record.MemberName}&DoQuery=true`}
            target="_blank"
          >
            <ReactIntlCurrencyWithFixedDecimal
              value={detailData.AdministrationFee}
            />
          </Link>
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.NumberOfWithdrawals"
              description="提现次数(日)"
            />
          }
        >
          {detailData.NumberOfWithdrawals}
        </Descriptions.Item>

        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.WithdrawalFee"
              description="手续费"
            />
          }
        >
          <Form.Item noStyle shouldUpdate>
            {({ getFieldValue, setFieldsValue }) => (
              <Row>
                <Col hidden={!isFeeUpdate}>
                  <Row gutter={[4, 0]}>
                    <Col>
                      <Form.Item
                        style={{ marginBottom: 0 }}
                        name="WithdrawalFee"
                        rules={[
                          {
                            validator: (_, value) => {
                              if (
                                value >
                                detailData.Fee + detailData.RealPlatformAmount
                              ) {
                                return Promise.reject(
                                  <FormattedMessage id="PageWithdraw.WithdrawRecordDetailModal.FormValidate.WithdrawalFeeOverRealAmount" />
                                );
                              }
                              return Promise.resolve();
                            },
                          },
                        ]}
                      >
                        <InputNumber
                          size="small"
                          min={0}
                          formatter={value => value.replace(/^[^\d|.]+/g, '')}
                          precision={2}
                          onPressEnter={evt => {
                            evt.preventDefault(); // 避免造成form直接submit
                            setIsFeeUpdate(false);
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col>
                      <Button
                        size="small"
                        type="text"
                        icon={<CheckOutlined />}
                        onClick={() => {
                          setIsFeeUpdate(false);
                        }}
                      />
                      <Button
                        size="small"
                        type="text"
                        icon={<CloseOutlined />}
                        onClick={() => {
                          setFieldsValue({
                            WithdrawalFee: Number(beforeUpdateWithdrawalFee),
                          });
                          setIsFeeUpdate(false);
                        }}
                      />
                    </Col>
                  </Row>
                </Col>
                <Col hidden={isFeeUpdate}>
                  <Space>
                    <ReactIntlCurrencyWithFixedDecimal
                      value={
                        type === MODAL_TYPE.CONFIRM_ORDER
                          ? getFieldValue('WithdrawalFee')
                          : detailData.WithdrawalFee
                      }
                    />
                    {type === MODAL_TYPE.CONFIRM_ORDER && (
                      <EditOutlined
                        onClick={() => {
                          setBeforeUpdateWithdrawalFee(
                            getFieldValue('WithdrawalFee')
                          );
                          setIsFeeUpdate(true);
                        }}
                      />
                    )}
                  </Space>
                </Col>
              </Row>
            )}
          </Form.Item>
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.TransactionFee"
              description="金流手续费"
            />
          }
        >
          {`${detailData.CurrencyName} `}
          <ReactIntlCurrencyWithFixedDecimal
            value={detailData.TransactionFee}
          />
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.RequestAmount"
              description="申请金额"
            />
          }
        >
          <ReactIntlCurrencyWithFixedDecimal value={detailData.RequestAmount} />
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.SumOfFee"
              description="扣除总额"
            />
          }
        >
          <ReactIntlCurrencyWithFixedDecimal value={detailData.Fee} />
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.RealPlatformAmount"
              description="换算前小计"
            />
          }
        >
          <ReactIntlCurrencyWithFixedDecimal
            value={detailData.RealPlatformAmount}
          />
        </Descriptions.Item>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.RealCurrencyAmount"
              description="出款金額"
            />
          }
        >
          {`${detailData.CurrencyName} `}
          <ReactIntlCurrencyWithFixedDecimal
            value={detailData.RealCurrencyAmount}
          />
        </Descriptions.Item>
      </Descriptions>
      <Divider />

      <Descriptions>
        <Descriptions.Item
          label={
            <FormattedMessage
              id="PageWithdraw.WithdrawRecordDetailModal.WithdrawType"
              description="提現類型"
            />
          }
        >
          {detailData.WithdrawalTypeName}
        </Descriptions.Item>
      </Descriptions>
      <Divider />
      <BankInfoTables detailData={[detailData]} />
      {type === MODAL_TYPE.CONFIRM_ORDER && (
        <>
          <Divider />
          <Row gutter={[16, 8]}>
            <Col sm={8}>
              <Form.Item
                name="Status"
                label={
                  <FormattedMessage
                    id="PageWithdraw.WithdrawRecordDetailModal.AuditResults"
                    description="审核结果"
                  />
                }
              >
                <Select>
                  <Option value={2}>
                    <FormattedMessage
                      id="PageWithdraw.WithdrawRecordDetailModal.StatusOptions.2"
                      description="通過"
                    />
                  </Option>
                  <Option value={4}>
                    <FormattedMessage
                      id="PageWithdraw.WithdrawRecordDetailModal.StatusOptions.4"
                      description="拒絕"
                    />
                  </Option>
                </Select>
              </Form.Item>
            </Col>
            <Col sm={16}>
              <Form.Item
                name="Remarks"
                label={
                  <FormattedMessage
                    id="Share.CommonKeys.Remarks"
                    description="备註"
                  />
                }
              >
                <TextArea />
              </Form.Item>
            </Col>
          </Row>
        </>
      )}
      <Divider />
      <Space>
        {type === MODAL_TYPE.CONFIRM_ORDER ? (
          <>
            <Button type="primary" htmlType="submit">
              <FormattedMessage id="Share.ActionButton.Submit2" />
            </Button>
            <Button onClick={onOk}>
              <FormattedMessage id="Share.ActionButton.Cancel" />
            </Button>
          </>
        ) : (
          <Button type="primary" onClick={onOk}>
            <FormattedMessage id="Share.ActionButton.Close" />
          </Button>
        )}
      </Space>
    </Form>
  );
}

Tab1WithdrawData.propTypes = {};

export default Tab1WithdrawData;
