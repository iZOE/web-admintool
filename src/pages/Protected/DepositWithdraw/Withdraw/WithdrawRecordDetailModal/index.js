import React from 'react';
import { Modal, Tabs } from 'antd';
import { FormattedMessage, useIntl } from 'react-intl';
import Tab1WithdrawData from './Tab1WithdrawData';
import Tab2VariousRecord from './Tab2VariousRecord';
import Tab3WithdrawRecord from './Tab3WithdrawRecord';
import Tab4TransactionLog from './Tab4TransactionLog';
import Tab5WalletBalance from './Tab5WalletBalance';
import TurnoverDetail from 'components/TurnoverDetail';

const { TabPane } = Tabs;

const TAB_INFO = {
  TURNOVER_LOG: 'TurnoverLog',
};

export const MODAL_TYPE = { WATCH: 'WATCH', CONFIRM_ORDER: 'CONFIRM_ORDER' };

export default function WithdrawRecordDetailModal({
  type,
  visible,
  onOk,
  record,
  recordId: withdrawRecordId,
}) {
  const intl = useIntl();

  return (
    <Modal
      title={
        <FormattedMessage
          id="PageWithdraw.WithdrawRecordDetailModal.Title"
          description="提現處理作業"
        />
      }
      visible={visible}
      width={900}
      onOk={onOk}
      onCancel={onOk}
      footer={null}
    >
      <Tabs>
        <TabPane
          tab={intl.formatMessage({
            id: 'PageWithdraw.WithdrawRecordDetailModal.Tab1Title',
          })}
          key="1"
        >
          <Tab1WithdrawData
            type={type}
            visible={visible}
            withdrawRecordId={withdrawRecordId}
            onOk={onOk}
            record={record}
          />
        </TabPane>
        <TabPane
          tab={<FormattedMessage id="PageTurnover.Title" />}
          key={TAB_INFO.TURNOVER_LOG}
        >
          <TurnoverDetail memberName={record.MemberName} />
        </TabPane>
        <TabPane
          tab={intl.formatMessage({
            id: 'PageWithdraw.WithdrawRecordDetailModal.Tab2Title',
          })}
          key="2"
        >
          <Tab2VariousRecord memberName={record.MemberName} />
        </TabPane>
        <TabPane
          tab={intl.formatMessage({
            id: 'PageWithdraw.WithdrawRecordDetailModal.Tab3Title',
          })}
          key="3"
        >
          <Tab3WithdrawRecord memberName={record.MemberName} />
        </TabPane>
        <TabPane
          tab={intl.formatMessage({
            id: 'PageWithdraw.WithdrawRecordDetailModal.Tab4Title',
          })}
          key="4"
        >
          <Tab4TransactionLog recordId={withdrawRecordId} />
        </TabPane>
        <TabPane
          tab={intl.formatMessage({
            id: 'PageWithdraw.WithdrawRecordDetailModal.Tab5Title',
          })}
          key="5"
        >
          <Tab5WalletBalance memberName={record.MemberName} />
        </TabPane>
      </Tabs>
    </Modal>
  );
}
