import React, { useState } from "react";
import { message } from "antd";
import { NotificationOutlined, StopOutlined } from "@ant-design/icons";
import axios from "axios";
import { useIntl } from "react-intl";

const isShowNotify = (statusCode) => {
  return Number(statusCode) === 1;
};

// RemindStatus 預設是1  即每筆自動開啟提醒
function NotifyButton({ statusCode, withdrawNo, remindStatus }) {
  const [remindStatusState, setRemindStatusState] = useState(remindStatus);
  const intl = useIntl();

  const handleChangeNotifyStatus = () => {
    const setIsNotifyURL = `/api/Withdrawal/${withdrawNo}/RemindStatus/${
      remindStatusState === 0 ? 1 : 0
    }`;
    axios
      .get(setIsNotifyURL)
      .then((res) => {
        if (res.data.ErrorKey) {
          message.error(
            `${intl.formatMessage({
              id: "Share.ErrorMessage.ErrorKey.JohnsonApiException",
            })}: ${res.data.ErrorKey}`
          );
        } else {
          setRemindStatusState(remindStatusState === 0 ? 1 : 0);
        }
      })
      .catch((err) => {
        message.error(
          `${intl.formatMessage({
            id: "Share.ErrorMessage.ErrorKey.JohnsonApiException",
          })}: ${err.message.status}`
        );
      });
  };

  if (!isShowNotify(statusCode)) {
    return "-";
  }

  return remindStatusState === 0 ? (
    <StopOutlined onClick={handleChangeNotifyStatus} />
  ) : (
    <NotificationOutlined onClick={handleChangeNotifyStatus} />
  );
}

export default NotifyButton;
