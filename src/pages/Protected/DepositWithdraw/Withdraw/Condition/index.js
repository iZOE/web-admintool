import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, Select, DatePicker } from 'antd'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { FormattedMessage, useIntl } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import FormItemMemberLevel from 'components/FormItems/MemberLevel'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'

const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const ApplyStatusData = [null, 1, 2, 3, 4, 5]

const CONDITION_INITIAL_VALUE = {
    UpdateDate: RANGE.FROM_TODAY_TO_TODAY,
    Status: null,
    WithdrawalNo: null,
    MemberName: null,
    IncludeInnerMember: false,
}

const Condition = ({
    onReady,
    onUpdate,
    condition,
    initialValueForTrigger,
}) => {
    const { isReady } = useContext(ConditionContext)
    const intl = useIntl()
    const [form] = Form.useForm()
    const { WithdrawalNo, Status = null } = initialValueForTrigger

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ UpdateDate, ...restValues }) {
        const [StartDate, EndDate] = UpdateDate || [null, null]
        return {
            ...restValues,
            StartDate: getISODateTimeString(StartDate),
            EndDate: getISODateTimeString(EndDate),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            onFinish={onFinish}
            form={form}
            initialValues={
                initialValueForTrigger
                    ? {
                          ...CONDITION_INITIAL_VALUE,
                          WithdrawalNo,
                          Status,
                      }
                    : { ...CONDITION_INITIAL_VALUE, ...condition }
            }
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageWithdraw.Conditions.UpdateDate',
                        })}：`}
                        name="UpdateDate"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemsSimpleSelect
                        url="/api/Option/WithdrawalType"
                        name="WithdrawTypeId"
                        label={
                            <FormattedMessage id="PageWithdraw.Conditions.WithdrawTypeId" />
                        }
                    />
                </Col>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage id="PageWithdraw.Conditions.Status" />
                        }
                        name="Status"
                    >
                        <Select>
                            {ApplyStatusData.map(d => (
                                <Option key={d} value={d}>
                                    <FormattedMessage
                                        id={`PageWithdraw.Conditions.StatusOptions.${
                                            d ? d : 0
                                        }`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        name="WithdrawalNo"
                        label={
                            <FormattedMessage id="PageWithdraw.Conditions.WithdrawalNo" />
                        }
                    >
                        <Input
                            placeholder={intl.formatMessage({
                                id: 'PageWithdraw.Conditions.WithdrawalNo',
                            })}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemMemberLevel checkAll />
                </Col>
                <Col sm={8}>
                    <Form.Item
                        name="MemberName"
                        label={
                            <FormattedMessage id="PageWithdraw.Conditions.MemberName" />
                        }
                    >
                        <Input
                            placeholder={intl.formatMessage({
                                id:
                                    'PageWithdraw.Conditions.MemberNamePlaceholder',
                            })}
                        />
                    </Form.Item>
                </Col>
                <Col sm={5}>
                    <FormItemsIncludeInnerMember />
                </Col>
                <Col sm={3} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
