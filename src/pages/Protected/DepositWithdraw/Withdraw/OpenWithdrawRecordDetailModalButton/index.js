import React, { useState } from "react";
import WithdrawRecordDetailModal, {
  MODAL_TYPE
} from "../WithdrawRecordDetailModal";

export default function OpenWithdrawRecordDetailModalButton({
  recordId,
  record
}) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <>
      <WithdrawRecordDetailModal
        type={MODAL_TYPE.WATCH}
        visible={isModalOpen}
        onOk={() => setIsModalOpen(false)}
        record={record}
        recordId={recordId}
      />
      <a onClick={() => setIsModalOpen(true)}>{recordId}</a>
    </>
  );
}
