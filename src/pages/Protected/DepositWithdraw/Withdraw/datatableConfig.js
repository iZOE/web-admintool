import React from 'react';
import { FormattedMessage } from 'react-intl';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenWithdrawRecordDetailModalButton from './OpenWithdrawRecordDetailModalButton';
import ManageButton from './ManageButton';
import NotifyButton from './NotifyButton';
import * as PERMISSION from 'constants/permissions';
import Permission from 'components/Permission';
import { NO_DATA } from 'constants/noData';

const { DEPOSIT_WITHDRAW_WITHDRAW_VIEW, DEPOSIT_WITHDRAW_WITHDRAW_HANDLE } = PERMISSION;
const onFalsyValueRenderDash = value => value || NO_DATA;

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.WithdrawalNo" description="訂單ID" />,
    dataIndex: 'WithdrawalNo',
    key: 'WithdrawalNo',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => <OpenWithdrawRecordDetailModalButton record={record} recordId={text} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.CreateTime" description="申請時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => {
      return <OpenMemberDetailButton memberId={text} memberName={text} />;
    },
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.NickName" description="名稱" />,
    dataIndex: 'MemberNickName',
    key: 'MemberNickName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.ParentMemberName" description="所屬上級" />,
    dataIndex: 'ParentMemberName',
    key: 'ParentMemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => {
      return text ? <OpenMemberDetailButton memberId={text} memberName={text} /> : NO_DATA;
    },
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.WithdrawTypeName" description="提現類型" />,
    dataIndex: 'WithdrawTypeText',
    key: 'WithdrawTypeText',
    isShow: true,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.BankName" description="銀行" />,
    dataIndex: 'BankName',
    key: 'BankName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: onFalsyValueRenderDash,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.AccountName" description="戶名" />,
    dataIndex: 'BankAccountName',
    key: 'BankAccountName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: onFalsyValueRenderDash,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.CardNumber" description="入款賬戶" />,
    dataIndex: 'CardNumber',
    key: 'CardNumber',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: onFalsyValueRenderDash,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.RequestAmount" description="申請金額" />,
    dataIndex: 'RequestAmount',
    key: 'RequestAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.WithdrawalFee" description="手續費" />,
    dataIndex: 'Fee',
    key: 'Fee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.PreferentialAmount" description="優惠扣除" />,
    dataIndex: 'PreferentialAmount',
    key: 'PreferentialAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.AdministrationFee" description="行政費" />,
    dataIndex: 'AdministrationFee',
    key: 'AdministrationFee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.TransactionFee" description="金流手續費" />,
    dataIndex: 'TransactionFee',
    key: 'TransactionFee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.RealAmount" description="出款金額" />,
    dataIndex: 'RealAmount',
    key: 'RealAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => {
      return <FormattedMessage id={`PageWithdraw.Conditions.StatusOptions.${text}`} />;
    },
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.IsIpRepeated" description="投注IP" />,
    dataIndex: 'IsIpRepeated',
    key: 'IsIpRepeated',
    isShow: true,
    render: text => {
      return <FormattedMessage id={`PageWithdraw.IsIpRepeatedOptions.${text}`} />;
    },
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.ModifyTime" description="更新時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageWithdraw.DataTable.ModifyTime" description="更新時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemModifyTime',
    key: 'SystemModifyTime',
    render: (_1, { SystemModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageWithdraw.DataTable.ModifyUserAccount" description="管理者" />,
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyUserAccount }, _2) => ModifyUserAccount || NO_DATA,
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'action',
    key: 'action',
    fixed: 'right',
    render: (text, record, index) => (
      <Permission functionIds={[DEPOSIT_WITHDRAW_WITHDRAW_VIEW, DEPOSIT_WITHDRAW_WITHDRAW_HANDLE]}>
        <ManageButton record={record} statusCode={record.Status} withdrawNo={record.WithdrawalNo} />
        <NotifyButton statusCode={record.Status} withdrawNo={record.WithdrawalNo} remindStatus={record.RemindStatus} />
      </Permission>
    ),
  },
];
