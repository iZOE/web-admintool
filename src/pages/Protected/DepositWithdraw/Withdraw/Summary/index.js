import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary }) => {
    if (!summary) {
        return <Skeleton active />
    }
    const {
        AdministrationFee,
        RealAmount,
        RealVirtualAmount,
        RequestAmount,
        WithdrawalCount,
        WithdrawalFee,
    } = summary

    return (
        <Row gutter={24} justify="space-between">
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageWithdraw.Summary.WithdrawalCount"
                            description="提现人数"
                        />
                    }
                    value={WithdrawalCount || 0}
                    precision={0}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageWithdraw.Summary.RequestAmount"
                            description="申请金额"
                        />
                    }
                    value={RequestAmount || 0}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageWithdraw.Summary.WithdrawalFee"
                            description="手续费"
                        />
                    }
                    value={WithdrawalFee || 0}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageWithdraw.Summary.AdministrationFee"
                            description="行政费"
                        />
                    }
                    value={AdministrationFee || 0}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageWithdraw.Summary.RealAmount"
                            description="出款金额（一般）"
                        />
                    }
                    value={RealAmount || 0}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageWithdraw.Summary.RealVirtualAmount"
                            description="出款金额(虚拟货币) "
                        />
                    }
                    value={RealVirtualAmount || 0}
                    precision={2}
                />
            </Col>
        </Row>
    )
}

export default SummaryView
