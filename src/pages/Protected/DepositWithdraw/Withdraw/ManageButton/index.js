import React, { useState } from "react";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";
import WithdrawRecordDetailModal, {
  MODAL_TYPE
} from "../WithdrawRecordDetailModal";

const isShowConfirm = statusCode => {
  return Number(statusCode) === 1;
};

function ManageButton({ statusCode, withdrawNo, record }) {
  const [isManageModalOpen, setIsManageModalOpen] = useState(false);

  if (!isShowConfirm(statusCode)) {
    return "-";
  }
  return (
    <>
      <WithdrawRecordDetailModal
        type={MODAL_TYPE.CONFIRM_ORDER}
        visible={isManageModalOpen}
        onOk={() => setIsManageModalOpen(false)}
        record={record}
        recordId={withdrawNo}
      />
      <Button type="link" onClick={() => setIsManageModalOpen(true)}>
        <FormattedMessage id="PageWithdraw.AuditButton" description="審單" />
      </Button>
    </>
  );
}

export default ManageButton;
