import React, { useState } from "react";
import { Button } from "antd";
import { InsertRowLeftOutlined } from "@ant-design/icons";
import { FormattedMessage } from "react-intl";
import ReviewDetailDrawer from "../ReviewDetailDrawer";

export default function OpenReviewDetailButton({ auditId }) {
  const [isDrawerShown, setIsDrawerShown] = useState(false);

  return (
    <>
      <Button
        type="primary"
        icon={<InsertRowLeftOutlined style={{ marginRight: 6 }} />}
        onClick={() => setIsDrawerShown(true)}
      >
        <FormattedMessage id="Share.ActionButton.Detail" description="詳情" />
      </Button>
      <ReviewDetailDrawer
        auditId={auditId}
        visible={isDrawerShown}
        onClose={() => setIsDrawerShown(false)}
      />
    </>
  );
}
