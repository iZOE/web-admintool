import React from 'react';
import useSWR from 'swr';
import axios from 'axios';

import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ExportReportButton from 'components/ExportReportButton';

import { COLUMN_CONFIG } from './columnConfig';
import * as PERMISSION from 'constants/permissions';

const { DEPOSIT_WITHDRAW_TURNOVER_EXPORT } = PERMISSION;

const INITIAL_CONDITION = {
  paginationInfo: { pageNumber: 1, pageSize: 25 },
  sortInfo: { SortBy: 'Desc', SortColumn: 'CreateTime' },
};

export default function History({ condition, onUpdate }) {
  const { data: result, isValidating: fetching } = useSWR(
    condition ? ['/api/v2/Turnover/Settlement/Search', condition] : null,
    url =>
      axios({
        url,
        method: 'post',
        data: {
          ...INITIAL_CONDITION,
          ...condition,
        },
      }).then(res => res.data.Data)
  );

  return (
    <DataTable
      config={COLUMN_CONFIG}
      dataSource={result && result.Container}
      total={result && result.TotalCount}
      displayResult={result}
      loading={fetching}
      onUpdate={onUpdate}
      rowKey={record => record.ValidBetAmountAuditSettlementID}
      extendArea={
        <Permission functionIds={[DEPOSIT_WITHDRAW_TURNOVER_EXPORT]}>
          <ExportReportButton
            condition={condition}
            disabled={!condition?.MemberName}
            actionUrl="/api/v2/Turnover/Settlement/Export"
          />
        </Permission>
      }
    />
  );
}
