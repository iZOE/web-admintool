import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenReviewDetailButton from './OpenReviewDetailButton';
import RedirectToDetailLink from './RedirectToDetailLink';
import { NO_DATA } from 'constants/noData';

const TYPE = {
  WITHDRAW: 1, // 提現
  TRANSFER: 2, // 轉帳
};

const WITHDRAW_STATUS = {
  // 1:未處理 ,2:審核中(待出款)已送給財務系統審核 ,3:已出款(已完成)財務系統回的狀態, 4:已拒絕, 5:出款失敗財務系統回的狀態
  DONE: 3,
};

const TRANSFER_STATUS = {
  SUCCESS: 2, // 2:已完成
};

export const COLUMN_CONFIG = [
  {
    title: <FormattedMessage id="Share.Fields.CreateTime" description="申請時間" />,
    fixed: 'left',
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.Type" description="類型" />,
    dataIndex: 'DataTypeName',
    key: 'DataTypeName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.OrderId" description="訂單 ID" />,
    dataIndex: 'OrderNO',
    key: 'OrderNO',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { DataType, OrderNO }, _2) => <RedirectToDetailLink orderId={OrderNO} typeId={DataType} />,
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.ApplyAmount" description="申請金額" />,
    dataIndex: 'RequestAmount',
    key: 'RequestAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.Discount" description="優惠扣除" />,
    dataIndex: 'MinusPreferentialAmount',
    key: 'MinusPreferentialAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.AdministrativeFee" description="行政費" />,
    dataIndex: 'AdministrationFee',
    key: 'AdministrationFee',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.ProcessingFee" description="手續費" />,
    dataIndex: 'WithdrawalFee',
    key: 'WithdrawalFee',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.AvailableWithdrawalAmount" description="實際提領金額" />,
    dataIndex: 'RealAmount',
    key: 'RealAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTurnover.Fields.ReviewTimeRange" description="審核時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.Modifier" description="異動人員" />,
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: author => (author ? author : NO_DATA),
  },
  {
    title: <FormattedMessage id="Share.Fields.Status" description="狀態" />,
    fixed: 'right',
    dataIndex: 'StatusName',
    key: 'StatusName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.Fields.Action" description="管理" />,
    fixed: 'right',
    render: (_1, { DataType, Status, ValidBetAmountAuditSettlementID }, _2) => {
      const isShownDetailButton =
        (DataType === TYPE.WITHDRAW && Status === WITHDRAW_STATUS.DONE) ||
        (DataType === TYPE.TRANSFER && Status === TRANSFER_STATUS.SUCCESS);
      return isShownDetailButton && <OpenReviewDetailButton auditId={ValidBetAmountAuditSettlementID} />;
    },
  },
];
