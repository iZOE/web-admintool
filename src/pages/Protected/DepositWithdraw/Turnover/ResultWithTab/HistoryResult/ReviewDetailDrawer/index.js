import React from "react";
import { FormattedMessage } from "react-intl";
import useSWR from "swr";
import axios from "axios";
import { Drawer } from "antd";
import DetailTable from "components/TurnoverDetail/Result/DetailTable";

export default function ReviewDetailDrawer({ visible, onClose, auditId }) {
  const payload = {
    ValidBetAuditID: auditId,
    paginationInfo: {
      pageNumber: 1,
      pageSize: 50
    },
    sortInfo: {
      SortColumn: "CreateTime",
      SortBy: "Desc"
    }
  };

  const { data: result, isValidating: fetching } = useSWR(
    auditId ? ["/api/v2/Turnover/Settlement/Detail/Search", auditId] : null,
    url =>
      axios({
        url,
        method: "post",
        data: payload
      }).then(res => res.data.Data)
  );

  return (
    <Drawer
      placement="right"
      width={720}
      closable={false}
      onClose={onClose}
      visible={visible}
    >
      <DetailTable
        title={
          <FormattedMessage
            id="PageTurnover.Fields.ReviewDetail"
            description="審核詳情"
          />
        }
        dataSource={result && result.Container}
        loading={fetching}
        pagination={false}
      />
    </Drawer>
  );
}
