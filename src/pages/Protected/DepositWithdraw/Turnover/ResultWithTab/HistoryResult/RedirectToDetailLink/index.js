import React from 'react'
import { Link } from 'react-router-dom'
import { NO_DATA } from 'constants/noData'

const TYPE = {
    WITHDRAW: 1, // 提現
    TRANSFER: 2, // 轉帳
}

export default function RedirectToDetailLink({ orderId, typeId }) {
    return orderId ? (
        <Link
            to={
                typeId === TYPE.WITHDRAW
                    ? `/deposit-withdraw/withdraw?WithdrawalNo=${orderId}`
                    : `/deposit-withdraw/member-transfer?transferId=${orderId}`
            }
        >
            {orderId}
        </Link>
    ) : (
        NO_DATA
    )
}
