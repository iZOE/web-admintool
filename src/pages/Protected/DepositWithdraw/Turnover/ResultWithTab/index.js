import React, { useState, useCallback } from 'react';
import { Tabs } from 'antd';
import { FormattedMessage } from 'react-intl';
import TurnoverResult from 'components/TurnoverDetail/Result';
import HistoryResult from './HistoryResult';
import { TAB_INFO, AUDIT_TYPE } from 'constants/turnover';

const { TabPane } = Tabs;
const DEFAULT_TAB = TAB_INFO.PENDING;
const tabPanes = [
  {
    title: (
      <FormattedMessage
        id={'PageTurnover.Tabs.' + TAB_INFO.PENDING}
        description="待審核列表"
      />
    ),
    key: TAB_INFO.PENDING
  },
  {
    title: (
      <FormattedMessage
        id={'PageTurnover.Tabs.' + TAB_INFO.FINISHED}
        description="已審核列表"
      />
    ),
    key: TAB_INFO.FINISHED
  },
  {
    title: (
      <FormattedMessage
        id={'PageTurnover.Tabs.' + TAB_INFO.HISTORY}
        description="已審核列表"
      />
    ),
    key: TAB_INFO.HISTORY
  }
];

export default function ResultWithTab({ condition, onUpdateCondition }) {
  const [currentTab, setCurrentTab] = useState(DEFAULT_TAB);

  const onUpdate = useCallback(
    newCondition => {
      onUpdateCondition(prevCondition => ({
        ...prevCondition,
        ...newCondition
      }));
    },
    [onUpdateCondition]
  );

  const onChange = useCallback(tabIndex => {
    setCurrentTab(tabIndex);
  }, []);

  return (
    <Tabs
      defaultActiveKey={currentTab}
      onChange={onChange}
      tabBarStyle={{ background: '#fff', marginBottom: 0, padding: '0 16px' }}
    >
      {tabPanes.map(({ title, key }) => (
        <TabPane tab={title} key={key}>
          {currentTab === TAB_INFO.HISTORY ? (
            <HistoryResult
              condition={condition && condition}
              onUpdate={onUpdate}
            />
          ) : (
            <TurnoverResult
              memberName={condition?.MemberName}
              condition={condition && condition}
              auditType={
                currentTab === TAB_INFO.PENDING
                  ? AUDIT_TYPE.PENDING
                  : AUDIT_TYPE.FINISHED
              }
              onUpdate={onUpdate}
            />
          )}
        </TabPane>
      ))}
    </Tabs>
  );
}
