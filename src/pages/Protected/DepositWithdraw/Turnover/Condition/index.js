import React from "react";
import { Form, Row, Col } from "antd";
import MemberNameFormItem from "components/FormItems/MemberName";
import QueryButton from "components/FormActionButtons/Query";
import {
  LAYOUT,
  GUTTER,
  SINGLE_ITEM,
  SINGLE_ITEM_OFFSET
} from "constants/layouts/condition";

export default function Condition({ onUpdate }) {
  const onFinish = ({ ...restValue }) => {
    const { MemberName } = restValue;

    if (!!MemberName) {
      onUpdate({ MemberName });
    }
  };

  return (
    <Form {...LAYOUT} onFinish={onFinish}>
      <Row gutter={GUTTER}>
        <Col {...SINGLE_ITEM}>
          <MemberNameFormItem />
        </Col>
        <Col {...SINGLE_ITEM_OFFSET} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}
