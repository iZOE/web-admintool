import React, { useState, useCallback } from "react";
import { Space, Button } from "antd";
import { InsertRowLeftOutlined, CalculatorOutlined } from "@ant-design/icons";
import { FormattedMessage } from "react-intl";
import TurnoverSummary from "components/TurnoverDetail/Summary";
import ChangeLogDrawer from "../ChangeLogDrawer";
import CalculationModalForm from "../CalculationModalForm";

export default function Summary({ memberName }) {
  const [isDrawerShown, setIsDrawerShown] = useState(false);
  const [isModalShown, setIsModalShown] = useState(false);

  const handleToggleDrawer = useCallback(
    () => setIsDrawerShown(prevIsShown => !prevIsShown),
    []
  );
  const handleToggleModal = useCallback(
    () => setIsModalShown(prevIsShown => !prevIsShown),
    []
  );

  return (
    <>
      <TurnoverSummary memberName={memberName} />
      <Space>
        <Button
          type="primary"
          icon={<InsertRowLeftOutlined style={{ marginRight: 6 }} />}
          onClick={handleToggleDrawer}
        >
          <FormattedMessage
            id="PageTurnover.Actions.GetChangeLog"
            description="所需流水異動紀錄"
          />
        </Button>
        <Button
          type="primary"
          icon={<CalculatorOutlined style={{ marginRight: 6 }} />}
          onClick={handleToggleModal}
          style={{ background: "#faad14", border: "#faad14" }}
        >
          <FormattedMessage
            id="PageTurnover.Actions.WithdrawalCalculation"
            description="提領審核試算"
          />
        </Button>
      </Space>
      <ChangeLogDrawer
        memberName={memberName}
        visible={isDrawerShown}
        onClose={handleToggleDrawer}
      />
      <CalculationModalForm
        memberName={memberName}
        visible={isModalShown}
        onCancel={handleToggleModal}
      />
    </>
  );
}
