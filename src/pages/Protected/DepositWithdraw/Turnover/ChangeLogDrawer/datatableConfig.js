import React from 'react'
import { Space } from 'antd'
import { SwapRightOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import { NO_DATA } from 'constants/noData'

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'ModifyTime',
        key: 'ModifyTime',
        render: (_1, { ModifyTime: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.Modifier"
                description="異動人員"
            />
        ),
        dataIndex: 'ModifyUserName',
        key: 'ModifyUserName',
        render: name => (name ? name : NO_DATA),
    },
    {
        title: (
            <FormattedMessage
                id="PageTurnover.Fields.ChangeItemId"
                description="異動項目 ID"
            />
        ),
        dataIndex: 'ValidBetAuditID',
        key: 'ValidBetAuditID',
    },
    {
        title: (
            <FormattedMessage
                id="PageTurnover.Fields.TurnoverLog"
                description="所需流水異動"
            />
        ),
        dataIndex: 'DiscountValidBetAmount',
        key: 'DiscountValidBetAmount',
        render: (_1, record, _2) => (
            <Space direction="vertical">
                <ReactIntlCurrencyWithFixedDecimal
                    value={record.OldDiscountValidBetAmount}
                />
                <SwapRightOutlined style={{ color: '#108ee9' }} />
                <ReactIntlCurrencyWithFixedDecimal
                    value={record.DiscountValidBetAmount}
                />
            </Space>
        ),
    },
]
