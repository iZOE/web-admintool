import React from 'react';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import { PageHeader } from 'antd';
import { FormattedMessage } from 'react-intl';
import Condition from './Condition';
import BasicDrawerWrapper from 'layouts/BasicDrawer/index';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import { COLUMNS_CONFIG } from './datatableConfig';

export default function ChangeLogDrawer({ memberName, visible, onClose }) {
  const { dataSource, fetching, onUpdateCondition, condition } = useGetDataSourceWithSWR({
    url: '/api/v2/Turnover/Log',
    defaultSortKey: 'ModifyTime',
  });

  return (
    <BasicDrawerWrapper
      title={
        <PageHeader
          title={<FormattedMessage id="PageTurnover.Fields.ChangeLog" description="異動紀錄" />}
          subTitle={memberName.toUpperCase()}
        />
      }
      onClose={onClose}
      visible={visible}
    >
      <ReportScaffold
        displayResult={condition}
        conditionComponent={<Condition onUpdate={onUpdateCondition} memberName={memberName} />}
        conditionHasCollapseWrapper={false}
        datatableComponent={
          <DataTable
            displayResult={condition}
            dataSource={dataSource && dataSource}
            loading={fetching}
            config={COLUMNS_CONFIG}
            pagination={false}
          />
        }
      />
    </BasicDrawerWrapper>
  );
}
