import React from 'react'
import { Form, Row, Col } from 'antd'
import DateRangeFormItem from 'components/Member/DateRangeFormItem'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'
import { DEFAULT } from 'constants/dateConfig'

const CONDITION_INITIAL_VALUE = {
    UpdateDate: DEFAULT.RANGE.FROM_A_MONTH_TO_TODAY,
}

export default function Condition({ onUpdate, memberName }) {
    const onFinish = ({ UpdateDate }) => {
        const [start, end] = UpdateDate || [null, null]

        onUpdate.bySearch({
            BeginDate: getISODateTimeString(start),
            EndDate: getISODateTimeString(end),
            MemberName: memberName,
        })
    }

    return (
        <Form onFinish={onFinish} initialValues={CONDITION_INITIAL_VALUE}>
            <Row>
                <Col>
                    <DateRangeFormItem />
                </Col>
                <Col>
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}
