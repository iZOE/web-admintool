import React, { useState } from 'react';
import Permission from 'components/Permission';
import Condition from './Condition';
import Summary from './Summary';
import ResultWithTab from './ResultWithTab';
import ReportScaffold from 'components/ReportScaffold';
import * as PERMISSION from 'constants/permissions';

const { DEPOSIT_WITHDRAW_TURNOVER_VIEW } = PERMISSION;

export default function TurnoverPage() {
  const [condition, setCondition] = useState(null);

  return (
    <Permission isPage functionIds={[DEPOSIT_WITHDRAW_TURNOVER_VIEW]}>
      <ReportScaffold
        displayResult={condition && condition.MemberName}
        conditionComponent={<Condition onUpdate={setCondition} />}
        summaryComponent={
          condition && <Summary memberName={condition.MemberName} />
        }
        datatableComponent={
          <ResultWithTab
            condition={condition}
            onUpdateCondition={setCondition}
          />
        }
      />
    </Permission>
  );
}
