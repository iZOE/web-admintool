import React, { useState } from "react";
import { useIntl, FormattedMessage } from "react-intl";
import {
  Modal,
  Form,
  Row,
  Col,
  Input,
  Button,
  Typography,
  message,
  Spin
} from "antd";
import useSWR from "swr";
import axios from "axios";

const rules = [
  {
    required: true,
    message: <FormattedMessage id="Share.FormValidate.Required.Input" />
  }
];

export default function CalculationModalForm({
  visible,
  onCancel,
  memberName
}) {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [payload, setPayload] = useState(null);

  const errorMsg = intl.formatMessage({
    id: "Share.ErrorMessage.UnknownError"
  });

  const { data: calculatedResult, isValidating: fetching } = useSWR(
    payload ? ["/api/v2/Turnover/TrailCalculation", payload] : null,
    url =>
      axios({
        url,
        method: "post",
        data: payload
      })
        .then(res => {
          return res.data.Data;
        })
        .catch(err => {
          console.error("Get Calculated Result Error: ", err);
          message.error(errorMsg);
        })
  );

  const onFinish = value => {
    setPayload({
      ...value,
      MemberName: memberName
    });
  };

  const handleCancel = () => {
    form.resetFields();
    setPayload(null);
    onCancel();
  };

  return (
    <Modal
      title={
        <FormattedMessage
          id="PageTurnover.Actions.WithdrawalCalculation"
          description="提領審核試算"
        />
      }
      centered
      destroyOnClose
      visible={visible}
      onCancel={handleCancel}
      footer={null}
    >
      <Form form={form} onFinish={onFinish}>
        <Row gutter={24}>
          <Col span={16}>
            <Form.Item
              labelCol={{ span: 8 }}
              label={
                <FormattedMessage
                  id="PageTurnover.Fields.WithdrawalAmount"
                  description="提領金額"
                />
              }
              name="Amount"
              rules={rules}
              hasFeedback
            >
              <Input type="number" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                <FormattedMessage id="PageTurnover.Actions.Calculate" />
              </Button>
            </Form.Item>
          </Col>
          {calculatedResult && (
            <Col span={16}>
              <Form.Item
                labelCol={{ span: 8 }}
                label={
                  <FormattedMessage
                    id="PageTurnover.Fields.ReviewResult"
                    description="審核結果"
                  />
                }
              >
                <span className="ant-form-text">
                  {fetching ? <Spin /> : calculatedResult.SettlementMessage}
                </span>
                {calculatedResult.SettlementMessageDetail &&
                  calculatedResult.SettlementMessageDetail.map(detail => (
                    <Typography.Paragraph>
                      <ol>{detail}</ol>
                    </Typography.Paragraph>
                  ))}
              </Form.Item>
            </Col>
          )}
        </Row>
      </Form>
    </Modal>
  );
}
