import React, { useState, createContext, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';
import { Button, Result } from 'antd';
import { ExportOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ExportReportButton from 'components/ExportReportButton';
import ReportScaffold from 'components/ReportScaffold';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import uid from 'utils/uid';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';
import SummaryView from './Summary';
import LogModal from './LogModal';

const { DEPOSIT_WITHDRAW_MEMBER_TRANSFER_VIEW, DEPOSIT_WITHDRAW_MEMBER_TRANSFER_EXPORT } = PERMISSION;

export const PageContext = createContext();

const PageView = () => {
  const location = useLocation();
  const [modalData, setModalData] = useState({ visible: false });

  const { fetching, dataSource, condition, onReady, onUpdateCondition } = useGetDataSourceWithSWR({
    url: '/api/FundTransferLog/Search',
    defaultSortKey: 'CreateTime',
    autoFetch: true,
  });

  // 呼叫modal
  function onOpenLogModal({ record }) {
    setModalData({ visible: true, record });
  }

  // 關閉modal
  function onCancelLogModal() {
    setModalData({ visible: false, record: null });
  }

  const orderIdFromRouteQuery = useMemo(() => {
    const { transferId } = queryString.parse(location.search);
    return transferId;
  }, [location]);

  return (
    <PageContext.Provider value={{ onOpenLogModal }}>
      <Permission
        functionIds={[DEPOSIT_WITHDRAW_MEMBER_TRANSFER_VIEW]}
        failedRender={
          <Result
            status="warning"
            title={<FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />}
            subTitle={<FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />}
          />
        }
      >
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition
              onReady={onReady}
              onUpdate={onUpdateCondition}
              initialValueForTrigger={{
                transferId: orderIdFromRouteQuery,
              }}
            />
          }
          summaryComponent={
            <SummaryView
              summary={dataSource && dataSource.Summary}
              totalCount={dataSource && dataSource.List.TotalCount}
            />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              title={<FormattedMessage id="Share.Table.SearchResult" description="查詢結果" />}
              config={COLUMNS_CONFIG}
              loading={fetching}
              rowKey={uid}
              dataSource={dataSource && dataSource.List.Data}
              total={dataSource && dataSource.List.TotalCount}
              extendArea={
                <Permission
                  functionIds={[DEPOSIT_WITHDRAW_MEMBER_TRANSFER_EXPORT]}
                  failedRender={
                    <Button type="primary" icon={<ExportOutlined />} disabled>
                      <FormattedMessage id="Share.ActionButton.Export" />
                    </Button>
                  }
                >
                  <ExportReportButton condition={condition} actionUrl="/api/FundTransferLog/Export" />
                </Permission>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
        <LogModal {...modalData} onOk={onCancelLogModal} />
      </Permission>
    </PageContext.Provider>
  );
};

export default PageView;
