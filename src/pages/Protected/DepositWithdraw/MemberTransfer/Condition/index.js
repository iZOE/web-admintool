import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, Select, DatePicker } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import FormItemsMemberLevel from 'components/FormItems/MemberLevel'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'

const { useForm } = Form
const { Option } = Select
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    Date: RANGE.FROM_TODAY_TO_TODAY,
    IncludeInnerMember: false,
    FundTransferTypeId: null,
    Status: null,
    AdvancedSearchConditionItem: 1,
}

function Condition({
    onReady,
    initialValueForTrigger: { transferId },
    onUpdate,
}) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()
    const { RangePicker } = DatePicker

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [StartTime, EndTime] = Date || [null, null]

        return {
            ...restValues,
            StartTime: getISODateTimeString(StartTime),
            EndTime: getISODateTimeString(EndTime),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <div>
            <Form
                form={form}
                initialValues={
                    transferId
                        ? {
                              ...CONDITION_INITIAL_VALUE,
                              AdvancedSearchConditionItem: 3,
                              AdvancedSearchConditionValue: transferId,
                          }
                        : CONDITION_INITIAL_VALUE
                }
                onFinish={onFinish}
            >
                <Row gutter={[16, 8]}>
                    <Col sm={8}>
                        <Form.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.QueryCondition.TransferDate"
                                    description="轉賬時間"
                                />
                            }
                            name="Date"
                        >
                            <RangePicker
                                style={{ width: '100%' }}
                                showTime
                                format={FORMAT.DISPLAY.DEFAULT}
                            />
                        </Form.Item>
                    </Col>
                    <Col sm={4}>
                        <Form.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.QueryCondition.FundTransferType"
                                    description="類型"
                                />
                            }
                            name="FundTransferTypeId"
                        >
                            <Select
                                style={{ width: '100%' }}
                                onChange={() => {}}
                            >
                                <Option value={null}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.All"
                                        description="全部"
                                    />
                                </Option>
                                <Option value={1}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.1"
                                        description="轉賬"
                                    />
                                </Option>
                                <Option value={4}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.4"
                                        description="紅包"
                                    />
                                </Option>
                                <Option value={9}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.9"
                                        description="彩票契約日工資"
                                    />
                                </Option>
                                <Option value={10}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.10"
                                        description="彩票契約日分紅"
                                    />
                                </Option>
                                <Option value={11}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.11"
                                        description="遊戲契約月分紅"
                                    />
                                </Option>
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col sm={6}>
                        <FormItemsMemberLevel checkAll />
                    </Col>
                    <Col sm={6}>
                        <Form.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.QueryCondition.Status"
                                    description="狀態"
                                />
                            }
                            name="Status"
                        >
                            <Select style={{ width: '100%' }}>
                                <Option value={null}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.StatusOptions.All"
                                        description="全部"
                                    />
                                </Option>
                                <Option value={0}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.StatusOptions.0"
                                        description="待領取"
                                    />
                                </Option>
                                <Option value={1}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.StatusOptions.1"
                                        description="已領取"
                                    />
                                </Option>
                                <Option value={2}>
                                    <FormattedMessage
                                        id="PageLogMemberTransfer.QueryCondition.SelectItem.StatusOptions.2"
                                        description="已逾期"
                                    />
                                </Option>
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col sm={10}>
                        <Form.Item
                            label={
                                <FormattedMessage
                                    id="Share.QueryCondition.AdvanceQuery"
                                    description="進階查詢"
                                />
                            }
                            name="AdvancedSearchConditionValue"
                        >
                            <Input
                                addonBefore={
                                    <Form.Item
                                        name="AdvancedSearchConditionItem"
                                        noStyle
                                    >
                                        <Select>
                                            <Option value={1}>
                                                <FormattedMessage
                                                    id="PageLogMemberTransfer.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.1"
                                                    description="發起會員帳號"
                                                />
                                            </Option>
                                            <Option value={2}>
                                                <FormattedMessage
                                                    id="PageLogMemberTransfer.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.2"
                                                    description="領取會員帳號"
                                                />
                                            </Option>
                                            <Option value={3}>
                                                <FormattedMessage
                                                    id="PageLogMemberTransfer.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.3"
                                                    description="訂單ID"
                                                />
                                            </Option>
                                        </Select>
                                    </Form.Item>
                                }
                                style={{ width: '100%' }}
                            />
                        </Form.Item>
                    </Col>
                    <Col sm={4}>
                        <FormItemsIncludeInnerMember />
                    </Col>
                    <Col sm={10} align="right">
                        <QueryButton />
                    </Col>
                </Row>
            </Form>
        </div>
    )
}

export default Condition
