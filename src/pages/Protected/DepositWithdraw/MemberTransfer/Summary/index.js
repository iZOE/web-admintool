import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary, totalCount }) => {
    return summary ? (
        <Row justify="space-between">
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogMemberTransfer.Summary.TotalCount"
                            description="總交易筆數"
                        />
                    }
                    value={totalCount}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogMemberTransfer.Summary.RequestAmount"
                            description="總轉帳額"
                        />
                    }
                    value={summary.RequestAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogMemberTransfer.Summary.Fee"
                            description="行政費用"
                        />
                    }
                    value={summary.Fee}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogMemberTransfer.Summary.ReceivedAmount"
                            description="已領取"
                        />
                    }
                    value={summary.ReceivedAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogMemberTransfer.Summary.WaitingAmount"
                            description="待領取"
                        />
                    }
                    value={summary.WaitingAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogMemberTransfer.Summary.RefundedAmount"
                            description="已退還"
                        />
                    }
                    value={summary.RefundedAmount}
                    precision={2}
                />
            </Col>
        </Row>
    ) : (
        <Skeleton active />
    )
}

export default SummaryView
