import React, { useContext } from 'react'
import { PageContext } from '../index'

export default function OpenLogModalButton({ record }) {
    const { onOpenLogModal } = useContext(PageContext)

    return (
        <a onClick={() => onOpenLogModal({ record })}>
            {record.FundTransferNo}
        </a>
    )
}
