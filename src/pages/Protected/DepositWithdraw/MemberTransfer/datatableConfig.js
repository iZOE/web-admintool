import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import OpenLogModalButton from './OpenLogModalButton';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.FundTransferNo" description="訂單ID" />,
    dataIndex: 'FundTransferNo',
    key: 'FundTransferNo',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => <OpenLogModalButton record={record} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.CreateTime" description="發起時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.EndTime" description="結束時間" />,
    dataIndex: 'EndTime',
    key: 'EndTime',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { EndTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.Type" description="類型" />,
    dataIndex: 'Type',
    key: 'Type',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => (
      <FormattedMessage
        id={`PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.${record.Type}`}
        description="類型byType"
      />
    ),
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.MemberName" description="發起帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (text, record, index) => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.NumberOfMembers" description="領取人數" />,
    dataIndex: 'NumberOfMembers',
    key: 'NumberOfMembers',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => (
      <>
        {record.NumberOfMembers} / {record.NumberOfTotalMembers}
      </>
    ),
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.ToMemberName" description="可領帳號" />,
    dataIndex: 'ToMemberName',
    key: 'ToMemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return 'ToMemberName';
    },
    render: (text, record, index) =>
      record.ToMemberName.map(memberName => <OpenMemberDetailButton memberName={memberName} />),
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.RequestAmount" description="轉賬金額" />,
    dataIndex: 'RequestAmount',
    key: 'RequestAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.RequestAmount} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.Fee" description="行政費用" />,
    dataIndex: 'Fee',
    key: 'Fee',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.Fee} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.ReceivedAmount" description="已領金額" />,
    dataIndex: 'ReceivedAmount',
    key: 'ReceivedAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.ReceivedAmount} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.RefundedAmount" description="退還金額" />,
    dataIndex: 'RefundedAmount',
    key: 'RefundedAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    render: (_1, record, _2) => <ReactIntlCurrencyWithFixedDecimal value={record.RefundedAmount} />,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => (
      <FormattedMessage
        id={`PageLogMemberTransfer.QueryCondition.SelectItem.StatusOptions.${record.Status}`}
        description="狀態"
      />
    ),
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.Remarks" description="附注" />,
    dataIndex: 'Remarks',
    key: 'Remarks',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { Remarks: text }, _2) => text || NO_DATA,
  },
  {
    title: <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.ModifyTime" description="更新時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageLogMemberTransfer.DataTable.Title.ModifyTime" description="更新時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemModifyTime',
    key: 'SystemModifyTime',
    render: (_1, { SystemModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
];
