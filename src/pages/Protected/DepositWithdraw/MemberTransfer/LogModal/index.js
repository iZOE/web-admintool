import React from 'react'
import { Typography, Timeline, Modal, Descriptions, Divider } from 'antd'
import { FormattedMessage, FormattedNumber } from 'react-intl'
import axios from 'axios'
import useSWR from 'swr'
import DateWithFormat from 'components/DateWithFormat'
import OpenMemberDetailButton from 'components/OpenMemberDetailButton'

const { Title } = Typography

export default function LogModal({ visible, onOk, record }) {
    const { data: logData } = useSWR(
        record ? `/api/FundTransferLog/${record.FundTransferNo}/Log` : null,
        url => axios(url).then(res => res.data.Data)
    )
    return (
        <Modal
            title={
                <FormattedMessage
                    id="PageLogMemberTransfer.LogModal.Title"
                    description="轉帳訂單明細"
                />
            }
            visible={visible}
            width={900}
            onOk={onOk}
            onCancel={onOk}
        >
            {record && (
                <>
                    <Descriptions
                        title={
                            <FormattedMessage
                                id="PageLogMemberTransfer.LogModal.DescriptionsTitleBasic"
                                description="轉帳訂單明細"
                            />
                        }
                    >
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.FundTransferNo"
                                    description="訂單ID"
                                />
                            }
                            span="3"
                        >
                            {record.FundTransferNo}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.Type"
                                    description="類型"
                                />
                            }
                        >
                            <FormattedMessage
                                id={`PageLogMemberTransfer.QueryCondition.SelectItem.FundTransferTypeOptions.${record.Type}`}
                                description="類型byType"
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.QueryCondition.Status"
                                    description="狀態"
                                />
                            }
                        >
                            <FormattedMessage
                                id={`PageLogMemberTransfer.QueryCondition.SelectItem.StatusOptions.${record.Status}`}
                                description="狀態"
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.CreateTime"
                                    description="發起時間"
                                />
                            }
                        >
                            <DateWithFormat time={record.CreateTime} />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.EndTime"
                                    description="結束時間"
                                />
                            }
                        >
                            <DateWithFormat time={record.EndTime} />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.ModifyTime"
                                    description="更新時間"
                                />
                            }
                        >
                            <DateWithFormat time={record.ModifyTime} />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.MemberName"
                                    description="發起帳號"
                                />
                            }
                            span="3"
                        >
                            <OpenMemberDetailButton
                                memberName={record.MemberName}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.ToMemberName"
                                    description="可領帳號"
                                />
                            }
                            span="3"
                        >
                            {record.ToMemberName.map(memberName => (
                                <OpenMemberDetailButton
                                    memberName={memberName}
                                />
                            )).reduce((prev, curr) => [prev, ', ', curr])}
                        </Descriptions.Item>
                    </Descriptions>
                    <Divider />
                    <Descriptions
                        title={
                            <FormattedMessage
                                id="PageLogMemberTransfer.LogModal.DescriptionsTitleAmount"
                                description="金額計算"
                            />
                        }
                    >
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.RequestAmount"
                                    description="轉賬金額"
                                />
                            }
                        >
                            {record.RequestAmount}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.Fee"
                                    description="行政費用"
                                />
                            }
                        >
                            {record.Fee}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.ReceivedAmount"
                                    description="已領金額"
                                />
                            }
                        >
                            {record.ReceivedAmount}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage
                                    id="PageLogMemberTransfer.DataTable.Title.RefundedAmount"
                                    description="退還金額"
                                />
                            }
                        >
                            {record.RefundedAmount}
                        </Descriptions.Item>
                    </Descriptions>
                    <Divider />
                    <div>
                        <Title style={{ fontSize: 16 }}>
                            <FormattedMessage
                                id="PageLogMemberTransfer.LogModal.LogTimeLine"
                                description="領取紀錄"
                            />
                        </Title>
                        <Timeline mode="left">
                            {logData &&
                                logData.map(log => (
                                    <Timeline.Item
                                        key={log.Time}
                                        label={
                                            <DateWithFormat time={log.Time} />
                                        }
                                    >
                                        <OpenMemberDetailButton
                                            memberName={log.MemberName}
                                        />
                                        <br />
                                        <span style={{ fontSize: 16 }}>
                                            <FormattedMessage
                                                id="PageLogMemberTransfer.LogModal.Withdraw"
                                                description="領取"
                                            />
                                            <FormattedNumber
                                                value={log.Amount}
                                            />
                                        </span>
                                    </Timeline.Item>
                                ))}
                        </Timeline>
                    </div>
                </>
            )}
        </Modal>
    )
}
