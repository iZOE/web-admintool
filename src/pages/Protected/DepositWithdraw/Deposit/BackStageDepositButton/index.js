import React, { useState } from "react";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import { MoneyCollectOutlined } from "@ant-design/icons";
import BackStageDepositModal from "../BackStageDepositModal";

function BackStageDepositButton(props) {
  const [
    isBackStageDepositModalOpen,
    setIsBackStageDepositModalOpen
  ] = useState(false);

  return (
    <>
      <BackStageDepositModal
        visible={isBackStageDepositModalOpen}
        onOk={() => setIsBackStageDepositModalOpen(false)}
      />
      <Button
        type="primary"
        onClick={() => setIsBackStageDepositModalOpen(true)}
        icon={<MoneyCollectOutlined />}
      >
        <FormattedMessage
          id="PageDepositList.BackStageDeposit"
          description="後台充值"
        />
      </Button>
    </>
  );
}

BackStageDepositButton.propTypes = {};

export default BackStageDepositButton;
