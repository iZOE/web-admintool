import React from 'react';
import { Statistic, Row, Col, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';

const SummaryView = ({ summary, totalCount }) => {
  if (!summary) {
    return <Skeleton active />;
  }
  const {
    NumberOfDeposits,
    NumberOfMembers,
    RequestAmount,
    CompanyCommissionFee,
    MemberCommissionFee,
    DepositBankAmount,
    DepositVirtaulAmount,
    PreferentialAmount
  } = summary;

  return (
    <>
      <Row gutter={24}>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.NumberOfDeposits"
                description="總訂單數"
              />
            }
            value={NumberOfDeposits || 0}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.NumberOfMembers"
                description="充值人数"
              />
            }
            value={NumberOfMembers || 0}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.RequestAmount"
                description="订单金额"
              />
            }
            value={RequestAmount || 0}
            precision={2}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.DepositBankAmount"
                description="充值金额(一般)"
              />
            }
            value={DepositBankAmount || 0}
            precision={2}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.DepositVirtaulAmount"
                description="充值金额(虛擬貨幣)"
              />
            }
            value={DepositVirtaulAmount || 0}
            precision={2}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.PreferentialAmount"
                description="优惠金额"
              />
            }
            value={PreferentialAmount || 0}
            precision={2}
          />
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.CompanyCommissionFee"
                description="手续费(站台)"
              />
            }
            value={CompanyCommissionFee || 0}
            precision={2}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageDepositList.Summary.MemberCommissionFee"
                description="手续费(会员)"
              />
            }
            value={MemberCommissionFee || 0}
            precision={2}
          />
        </Col>
      </Row>
    </>
  );
};

export default SummaryView;
