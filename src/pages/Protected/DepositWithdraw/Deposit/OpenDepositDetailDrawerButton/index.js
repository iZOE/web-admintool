import React, { useState, useCallback, lazy } from 'react'
import { Button } from 'antd'

const DepositDetailDrawerAsync = lazy(() => import('../DepositDetailDrawer'))

export default function OpenDepositDetailDrawerButton({
    depositId,
    displayText,
    mode,
}) {
    const [isDrawerShown, setIsDrawerShown] = useState(false)

    const handleToggleDrawer = useCallback(
        () => setIsDrawerShown(prevIsShown => !prevIsShown),
        []
    )

    return (
        <>
            <Button onClick={handleToggleDrawer} size="small" type="link">
                {displayText ? displayText : depositId}
            </Button>
            {isDrawerShown && (
                <DepositDetailDrawerAsync
                    depositId={depositId}
                    isVisible={isDrawerShown}
                    onClose={handleToggleDrawer}
                    mode={mode}
                />
            )}
        </>
    )
}
