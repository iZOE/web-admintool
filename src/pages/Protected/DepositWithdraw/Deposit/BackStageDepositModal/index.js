import React, { useState, useCallback } from 'react'
import { Button, Modal, Form, message, Row, Col, Input, Divider } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import axios from 'axios'
import _debounce from 'lodash/debounce'
import usePaymentIdTwoLevelSelect from './usePaymentIdTwoLevelSelect'

const { TextArea } = Input

const initialConditions = {
    MemberName: '',
    DepositType: 0,
    PaymentId: null, // Number, if has data
    DepositAmount: null, // Number, if has data
    ForceDeposit: false,
    AppendMessage: '',
}

const checkIsMemberExist = (
    enteringMemberName,
    intl,
    setMemberNameValidate
) => {
    setMemberNameValidate(prev => ({ ...prev, status: 'validating' }))
    const url = '/api/Member/CheckMemberExists'
    axios
        .post(url, { MemberName: enteringMemberName })
        .then(res => {
            if (!res.data.Status) {
                setMemberNameValidate(prev => ({
                    ...prev,
                    status: 'error',
                    help: intl.formatMessage({
                        id: 'PageDepositList.Validation.MemberNameNotExist',
                    }),
                }))
            } else {
                setMemberNameValidate(prev => ({
                    ...prev,
                    status: 'success',
                    help: '',
                }))
            }
        })
        .catch(err => {
            message.error(
                `${intl.formatMessage({
                    id: 'Share.ErrorMessage.ErrorKey.JohnsonApiException',
                })}: ${err.message.status}, ${err.message.statusText}`
            )
            setMemberNameValidate(prev => ({ ...prev, status: 'error' }))
        })
}

const debouncedCheckIsMemberExist = _debounce(checkIsMemberExist, 1500)

export default function BackStageDepositModal({ visible, onOk }) {
    const intl = useIntl()
    const [form] = Form.useForm()
    const [
        PaymentIdTwoLevelSelectJSXLiteral,
        groupedByDepositTypeIdData,
        paymentIdKeyDataMap,
    ] = usePaymentIdTwoLevelSelect(form)
    const [memberNameValidate, setMemberNameValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'PageDepositList.PlaceHolder.MemberName',
        }),
        help: (
            <FormattedMessage
                id="Share.FormValidate.Required.InputWithValue"
                values={{
                    name: (
                        <FormattedMessage id="PageDepositList.BackendDepositModal.MemberName" />
                    ),
                }}
            />
        ),
        status: 'error',
    })
    const [depositAmountValidate, setDepositAmountValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'PageDepositList.PlaceHolder.MemberName',
        }),
        help: (
            <FormattedMessage
                id="PageDepositList.Validation.DepositAmountNotEnough"
                values={{ amount: 10 }}
            />
        ),
        status: 'error',
    })

    const isSubmitAble = !memberNameValidate.help && !depositAmountValidate.help

    const submitBackStageDepositRequest = useCallback(parameter => {
        const url = '/api/Deposit/BackendDeposit'
        axios
            .post(url, parameter)
            .then(res => {
                message.success(
                    intl.formatMessage({
                        id: 'Share.SuccessMessage.UpdateSuccess',
                    })
                )
                onOk()
            })
            .catch(err => {
                message.error(
                    `${intl.formatMessage({
                        id: 'Share.ErrorMessage.ErrorKey.JohnsonApiException',
                    })}: ${err.message.status}, ${err.message.statusText}`
                )
            })
    }, [])

    const handleValusChanges = updatingField => {
        console.log('updatingField', updatingField)
        if (updatingField['MemberName']) {
            const memberNameValue = updatingField['MemberName']
            debouncedCheckIsMemberExist(
                memberNameValue,
                intl,
                setMemberNameValidate
            )
        }
        if (updatingField['DepositType']) {
            const selectedValue = updatingField['DepositType']
            const defaultValuePaymentId =
                groupedByDepositTypeIdData[selectedValue][0]['PaymentId']
            console.log('defaultValuePaymentId', defaultValuePaymentId)
            form.setFieldsValue({ PaymentId: defaultValuePaymentId })
        }
        if (updatingField['PaymentId']) {
            const PaymentId = updatingField['PaymentId']
            const selectedPaymentIdData = paymentIdKeyDataMap.get(PaymentId)
            console.log('selectedPaymentIdData', selectedPaymentIdData)
            const nowDAmount = form.getFieldValue('DepositAmount')
            if (nowDAmount < selectedPaymentIdData.MinPaymentAmount) {
                setDepositAmountValidate(prev => ({
                    ...prev,
                    status: 'error',
                    help: (
                        <FormattedMessage
                            id="PageDepositList.Validation.DepositAmountNotEnough"
                            values={{
                                amount: selectedPaymentIdData.MinPaymentAmount,
                            }}
                        />
                    ),
                }))
            }
        }
        if (updatingField['DepositAmount']) {
            const dAmount = updatingField['DepositAmount']
            const paymentId = form.getFieldValue('PaymentId')
            const nowSelectedPaymentIdData = paymentIdKeyDataMap.get(paymentId)
            if (
                dAmount >= nowSelectedPaymentIdData.MinPaymentAmount &&
                dAmount <= nowSelectedPaymentIdData.MaxPaymentAmount
            ) {
                setDepositAmountValidate(prev => ({
                    ...prev,
                    status: 'success',
                    help: '',
                }))
            }
            if (dAmount < nowSelectedPaymentIdData.MinPaymentAmount) {
                setDepositAmountValidate(prev => ({
                    ...prev,
                    status: 'error',
                    help: (
                        <FormattedMessage
                            id="PageDepositList.Validation.DepositAmountNotEnough"
                            values={{
                                amount:
                                    nowSelectedPaymentIdData.MinPaymentAmount,
                            }}
                        />
                    ),
                }))
            }
            if (dAmount > nowSelectedPaymentIdData.MaxPaymentAmount) {
                setDepositAmountValidate(prev => ({
                    ...prev,
                    status: 'error',
                    help: (
                        <FormattedMessage
                            id="PageDepositList.Validation.DepositAmountExceed"
                            values={{
                                amount:
                                    nowSelectedPaymentIdData.MaxPaymentAmount,
                            }}
                        />
                    ),
                }))
            }
        }
    }

    // 按下發起 後台充值 時
    const onSubmitClick = values => {
        const { MemberName, PaymentId, DepositAmount, AppendMessage } = values
        console.log('values', values)
        submitBackStageDepositRequest({
            MemberName,
            PaymentId,
            DepositAmount: Number(DepositAmount),
            ForceDeposit: false,
            AppendMessage: AppendMessage || ' ',
        })
    }

    return (
        <Modal
            title={
                <FormattedMessage
                    id="PageDepositList.Areas.DepositFromBackOffice"
                    description="後台充值"
                />
            }
            visible={visible}
            width={900}
            onOk={onOk}
            onCancel={onOk}
            footer={null}
        >
            <Form
                form={form}
                labelCol={{
                    xs: { span: 24 },
                    sm: { span: 4 },
                }}
                wrapperCol={{ xs: { span: 24 }, sm: { span: 20 } }}
                onFinish={onSubmitClick}
                initialValues={initialConditions}
                onValuesChange={handleValusChanges}
            >
                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id:
                                    'PageDepositList.BackendDepositModal.MemberName',
                            })}：`}
                            name="MemberName"
                            hasFeedback
                            help={memberNameValidate.help}
                            validateStatus={memberNameValidate.status}
                        >
                            <Input
                                placeholder={memberNameValidate.placeholder}
                            />
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        {PaymentIdTwoLevelSelectJSXLiteral}
                    </Col>
                </Row>

                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id:
                                    'PageDepositList.BackendDepositModal.DepositAmount',
                            })}：`}
                            name="DepositAmount"
                            hasFeedback
                            help={depositAmountValidate.help}
                            validateStatus={depositAmountValidate.status}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={24}>
                    <Col sm={24} xs={24}>
                        <Form.Item
                            label={`${intl.formatMessage({
                                id:
                                    'PageDepositList.BackendDepositModal.AppendMessage',
                            })}：`}
                            name="AppendMessage"
                        >
                            <TextArea />
                        </Form.Item>
                    </Col>
                </Row>
                <Divider />
                <Row gutter={24}>
                    <Button
                        type="primary"
                        htmlType="submit"
                        disabled={!isSubmitAble}
                    >
                        <FormattedMessage id="Share.ActionButton.Submit2" />
                    </Button>
                    <Button onClick={onOk}>
                        <FormattedMessage id="Share.ActionButton.Cancel" />
                    </Button>
                </Row>
            </Form>
        </Modal>
    )
}
