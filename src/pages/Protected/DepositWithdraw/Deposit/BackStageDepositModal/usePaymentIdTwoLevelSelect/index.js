import React, { useMemo, useState, useEffect } from "react";
import { Select, Form, message } from "antd";
import PropTypes from "prop-types";
import useSWR from "swr";
import axios from "axios";
import { FormattedMessage, FormattedNumber, useIntl } from "react-intl";
import _groupBy from "lodash.groupby";

const { Option } = Select;

const PaymentIdTwoLevelSelect = ({
  onFirstLevelSelectChange,
  selectedIndex,
  firstLevelData,
  dataGroupedByDepositId,
  intl,
}) => (
  <>
    <Form.Item
      name="DepositType"
      label={`${intl.formatMessage({
        id: "PageDepositList.BackendDepositModal.PaymentId",
      })}`}
    >
      <Select value={selectedIndex} onChange={onFirstLevelSelectChange}>
        {firstLevelData.map((d) => (
          <Option value={d.value} key={d.text}>
            {d.text}
          </Option>
        ))}
      </Select>
    </Form.Item>

    <Form.Item name="PaymentId" label=" " colon={false}>
      <Select>
        {dataGroupedByDepositId[selectedIndex].map((d) => (
          <Option key={d.PaymentId} value={d.PaymentId}>
            {d.DisplayName}
          </Option>
        ))}
      </Select>
    </Form.Item>
  </>
);

// PaymentId兩層下拉選單，但是只有要拿到第二層時的值  外部搭配 ANTD Form 和給定name一起使用
// 參數formInstance: ANTD Form的實體   為了用來imperative式地操縱連動欄位的狀態
function useTwoLevelSelect(formInstance) {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const intl = useIntl();
  const { data: backStageDepositSiteData } = useSWR(
    "/api/Option/BackendDepositeChannels",
    (url) => {
      return axios
        .get(url)
        .then((result) => {
          const data = result.data;
          if (data && data.Status !== true) {
            throw new Error({
              message: { status: data.ErrorKey, statusText: data.Message },
            });
          }
          return data;
        })
        .catch((err) => {
          message.error(
            `${intl.formatMessage({
              id: "Share.ErrorMessage.ErrorKey.JohnsonApiException",
            })}: ${err.message.status}`
          );
        });
    }
  );

  const [
    dataGroupedByDepositId,
    firstLevelData,
    paymentIdKeyDataMap,
  ] = useMemo(() => {
    if (
      !backStageDepositSiteData ||
      !backStageDepositSiteData.Data ||
      backStageDepositSiteData.Data.length === 0
    ) {
      return [null, null, null];
    }
    const dataGroupedByDepositId = _groupBy(
      backStageDepositSiteData.Data,
      "DepositTypeID"
    ); // 變 {1: [{...}, {...}], 2: [{...}, ...]}
    const flattenData = backStageDepositSiteData.Data.map((d) => ({
      PaymentId: d.PaymentId,
      DisplayName: d.DisplayName,
      MaxPaymentAmount: d.MaxPaymentAmount,
      MinPaymentAmount: d.MinPaymentAmount,
    }));
    const firstLevelData = Object.keys(dataGroupedByDepositId).map((k) => ({
      value: dataGroupedByDepositId[k][0]["DepositTypeID"],
      text: dataGroupedByDepositId[k][0]["DepositType"],
    }));
    firstLevelData.unshift({
      value: 0,
      text: <FormattedMessage id="Share.Dropdown.All" />,
    });
    dataGroupedByDepositId[0] = flattenData;

    // 把array變 Map, key是 PaymentId
    const paymentIdKeyDataMap = backStageDepositSiteData.Data.reduce(
      (acc, curr) => {
        acc.set(curr.PaymentId, curr);
        return acc;
      },
      new Map()
    );

    return [dataGroupedByDepositId, firstLevelData, paymentIdKeyDataMap];
  }, [backStageDepositSiteData]);

  const onFirstLevelSelectChange = (v) => {
    setSelectedIndex(Number(v));
    formInstance.setFieldsValue({ DepositType: Number(v) });
    formInstance.setFieldsValue({
      PaymentId: dataGroupedByDepositId[v][0]["PaymentId"],
    });
  };

  useEffect(() => {
    // 有取到資料後  把第二層下拉選單的值設定到第一筆
    console.log("dataGroupedByDepositId", dataGroupedByDepositId);
    dataGroupedByDepositId &&
      formInstance.setFieldsValue({
        PaymentId: dataGroupedByDepositId[0][0]["PaymentId"],
      });
  }, [dataGroupedByDepositId]);

  if (!dataGroupedByDepositId || !firstLevelData) {
    return [null, null, null];
  }

  return [
    <PaymentIdTwoLevelSelect
      onFirstLevelSelectChange={onFirstLevelSelectChange}
      selectedIndex={selectedIndex}
      firstLevelData={firstLevelData}
      dataGroupedByDepositId={dataGroupedByDepositId}
      intl={intl}
    />,
    dataGroupedByDepositId,
    paymentIdKeyDataMap,
  ];
}

export default useTwoLevelSelect;
