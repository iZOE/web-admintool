import React, { useContext, useEffect } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Form, Row, Col, Input, Select, DatePicker } from 'antd'
import { ConditionContext } from 'contexts/ConditionContext'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import AdvancedSearchItem from 'components/FormItems/AdvancedSearchItem'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemChannelOptions from './FormItems/ChannelOptions'
import { getISODateTimeString } from 'mixins/dateTime'

const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const DATE_TYPE_IDS = [1, 2] // 1:更新時間 2:充值時間

const ADVANCE_SEARCH_CONDITION_ITEMS = [1, 2, 3, 4]

const ALLOW_DEPOSITE_SITE_OPTIONs = [1, 2]

const DEPOSIT_TYPE_OPTIONs = [0, 1, 2, 3]

const ADVANCE_ITEM_WITH_AMOUNT = new Set([4])

const CONDITION_INITIAL_VALUE = {
    DateType: DATE_TYPE_IDS[0],
    Date: RANGE.FROM_TODAY_TO_TODAY,
    AllowDepositeSite: ALLOW_DEPOSITE_SITE_OPTIONs,
    DepositTypeId: 0,
    AdvancedSearchConditionItem: 1,
    DepositNo: null,
    IncludeInnerMember: false,
}

const Condition = ({
    onReady,
    onUpdate,
    condition,
    initialValueForTrigger,
}) => {
    const { isReady } = useContext(ConditionContext)
    const [form] = Form.useForm()
    const intl = useIntl()
    const { Status } = initialValueForTrigger

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ Date, ...restValues }) {
        const [StartTime, EndTime] = Date || [null, null]

        return {
            ...restValues,
            StartTime: getISODateTimeString(StartTime),
            EndTime: getISODateTimeString(EndTime),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={
                Status
                    ? {
                          ...CONDITION_INITIAL_VALUE,
                          Status: [Number(Status)],
                      }
                    : { ...CONDITION_INITIAL_VALUE, ...condition }
            }
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="Share.QueryCondition.DateRange" />
                        }
                    >
                        <Input.Group compact>
                            <Form.Item name="DateType" noStyle>
                                <Select
                                    className="option"
                                    style={{ width: '100px' }}
                                >
                                    {DATE_TYPE_IDS.map(id => (
                                        <Option value={id} key={id}>
                                            <FormattedMessage
                                                id={`PageDepositList.Conditions.DateType.${id -
                                                    1}`}
                                            />
                                        </Option>
                                    ))}
                                </Select>
                            </Form.Item>
                            <Form.Item name="Date" noStyle>
                                <RangePicker
                                    style={{ width: 'calc(100% - 100px)' }}
                                    showTime
                                    format={FORMAT.DISPLAY.DEFAULT}
                                />
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageDepositList.Conditions.AllowDepositeSite',
                        })}：`}
                        name="AllowDepositeSite"
                    >
                        <Select mode="multiple" style={{ width: '100%' }}>
                            {ALLOW_DEPOSITE_SITE_OPTIONs.map(v => (
                                <Option value={v} key={v}>
                                    <FormattedMessage
                                        id={`PageDepositList.Conditions.SelectItem.AllowDepositeSiteOptions.${v}`}
                                    />
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <Form.Item
                        shouldUpdate
                        label={
                            <FormattedMessage
                                id="PageDepositList.Conditions.DepositTypeId"
                                description="充值金流途徑"
                            />
                        }
                    >
                        {({ getFieldValue }) => (
                            <Input.Group compact>
                                <Form.Item noStyle name="DepositTypeId">
                                    <Select
                                        style={{
                                            width:
                                                getFieldValue(
                                                    'DepositTypeId'
                                                ) === 0
                                                    ? '100%'
                                                    : '35%',
                                        }}
                                    >
                                        {DEPOSIT_TYPE_OPTIONs.map(v => (
                                            <Option value={v} key={v}>
                                                <FormattedMessage
                                                    id={`PageDepositList.Conditions.SelectItem.DepositTypeIdOptions.${v}`}
                                                />
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                {getFieldValue('DepositTypeId') === 0 ? null : (
                                    <FormItemChannelOptions
                                        form={form}
                                        depositTypeId={getFieldValue(
                                            'DepositTypeId'
                                        )}
                                    />
                                )}
                            </Input.Group>
                        )}
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemMultipleSelect
                        checkAll
                        name="PaymentTypeId"
                        label={
                            <FormattedMessage
                                id="PageDepositList.Conditions.PaymentType"
                                description="支付類型"
                            />
                        }
                        url="/api/Option/PaymentTypes"
                    />
                </Col>
                <Col sm={8}>
                    <FormItemMultipleSelect
                        checkAll
                        name="Status"
                        label={
                            <FormattedMessage
                                id="PageDepositList.OrderStatus"
                                description="訂單狀態"
                            />
                        }
                        url="/api/Option/DepositStatus"
                    />
                </Col>
                <Col sm={8}>
                    <Form.Item
                        label={
                            <FormattedMessage id="Share.QueryCondition.DepositNo" />
                        }
                        name="DepositNo"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={10}>
                    <AdvancedSearchItem
                        OptionItems={ADVANCE_SEARCH_CONDITION_ITEMS}
                        MessageId="PageDepositList.Conditions.SelectItem.AdvancedSearchConditionItemOptions"
                        AmountSet={ADVANCE_ITEM_WITH_AMOUNT}
                    />
                </Col>
                <Col sm={8}>
                    <FormItemsIncludeInnerMember />
                </Col>
                <Col sm={6} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
