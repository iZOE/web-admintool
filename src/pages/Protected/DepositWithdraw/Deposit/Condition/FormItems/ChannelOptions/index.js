import React, { useState, useEffect, useMemo } from 'react'
import { Form, TreeSelect } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import { FormattedMessage } from 'react-intl'

const { SHOW_CHILD } = TreeSelect

function findIds(data) {
    let ids = []
    function findIdWithoutChildren(d) {
        d.forEach(dd => {
            if (dd.children) {
                findIdWithoutChildren(dd.children)
            } else {
                ids.push(dd.value)
            }
        })
    }

    findIdWithoutChildren(data)
    return ids
}

function genTreeData(data) {
    if (!data) {
        return null
    }

    return data.map(opt => ({
        title: opt.Name,
        value: opt.Id,
        key: opt.Id,
        children: opt.SubItems ? genTreeData(opt.SubItems) : null,
    }))
}

export default function ChannelOptions({ form, depositTypeId }) {
    const { data } = useSWR('/api/Option/Channels', url =>
        axios(url).then(res => {
            return res.data.Data || null
        })
    )

    const treeData = useMemo(() => {
        if (data && depositTypeId) {
            const filteredData = data.filter(x => x.Id === depositTypeId)
            return [
                {
                    title: (
                        <FormattedMessage
                            id="Share.Dropdown.All"
                            description="全部"
                        />
                    ),
                    value: '',
                    key: '',
                    children:
                        Array.isArray(filteredData) &&
                        filteredData.length &&
                        filteredData[0].SubItems
                            ? genTreeData(filteredData[0].SubItems)
                            : [],
                },
            ]
        }
    }, [data, depositTypeId])

    useEffect(() => {
        if (treeData) {
            form.setFieldsValue({
                PaymentBankListId: treeData[0].children.map(x => x.value),
            })
        }
    }, [treeData])

    if (!treeData) {
        return null
    }

    return (
        <Form.Item name="PaymentBankListId" noStyle>
            <TreeSelect
                style={{ width: '65%' }}
                treeData={treeData}
                treeCheckable
                showCheckedStrategy={SHOW_CHILD}
                maxTagCount={1}
                maxTagPlaceholder="..."
            />
        </Form.Item>
    )
}
