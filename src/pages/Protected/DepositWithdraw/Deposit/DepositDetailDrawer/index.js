import React from 'react'
import useSWR, { trigger } from 'swr'
import axios from 'axios'
import caller from 'utils/fetcher'
import { useIntl, FormattedMessage } from 'react-intl'
import {
    Button,
    Descriptions,
    Divider,
    Form,
    Image,
    Input,
    PageHeader,
    Skeleton,
    Space,
    Typography,
    message,
} from 'antd'
import { SaveOutlined } from '@ant-design/icons'
import BasicDrawerWrapper from 'layouts/BasicDrawer/index'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import { NO_DATA } from 'constants/noData'
import { MODE } from 'constants/mode'

const { useForm } = Form

export default function DepositDetailDrawer({
    depositId,
    isVisible,
    onClose,
    mode = MODE.VIEW,
}) {
    const intl = useIntl()
    const [form] = useForm()
    const isEditMode = mode && mode === MODE.EDIT
    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })

    const { data: d } = useSWR(
        depositId ? `/api/Deposit/${depositId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    const onDrawerSave = values => {
        caller({
            method: 'patch',
            endpoint: `/api/Deposit/${depositId}`,
            body: { ...values },
        })
            .then(() => {
                message.success(successMsg, 5)
            })
            .then(() => {
                trigger(`/api/Deposit/${depositId}`)
                trigger('/api/Deposit/Search')
            })
    }

    return d ? (
        <BasicDrawerWrapper
            footer={
                isEditMode && (
                    <Button
                        icon={<SaveOutlined style={{ marginRight: 6 }} />}
                        onClick={() => form.submit()}
                        type="primary"
                    >
                        <FormattedMessage
                            id="Share.ActionButton.Submit"
                            description="提交"
                        />
                    </Button>
                )
            }
            footerStyle={{ textAlign: 'right' }}
            title={
                <PageHeader
                    title={
                        <FormattedMessage
                            id="PageDepositList.Areas.Title"
                            description="訂單充值明細"
                        />
                    }
                />
            }
            onClose={onClose}
            visible={isVisible}
        >
            <Descriptions
                title={
                    <FormattedMessage
                        id="PageDepositList.Areas.OrderDetail"
                        description="訂單訊息"
                    />
                }
                column={2}
            >
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.OrderId"
                            description="訂單編號"
                        />
                    }
                    span={2}
                >
                    {d.DepositNo}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="Share.Fields.MemberName"
                            description="會員帳號"
                        />
                    }
                >
                    {d.MemberName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="Share.Fields.MemberNickName"
                            description="會員名稱"
                        />
                    }
                >
                    {d.MemberNickName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.ParentMemberName"
                            description="所屬上級"
                        />
                    }
                >
                    {d.ParentMemberName ? d.ParentMemberName : NO_DATA}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.PaymentType"
                            description="支付類型"
                        />
                    }
                >
                    {d.PaymentTypeName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="Share.Fields.Bank"
                            description="銀行"
                        />
                    }
                >
                    {d.BankName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.AccountName"
                            description="戶名"
                        />
                    }
                >
                    {d.AccountName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.CurrencyType"
                            description="幣別"
                        />
                    }
                >
                    {d.CurrencyName ? d.CurrencyName : NO_DATA}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.RequestAmount"
                            description="訂單金額"
                        />
                    }
                >
                    <ReactIntlCurrencyWithFixedDecimal
                        value={d.RequestAmount}
                    />
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.ReceivedAmount"
                            description="訂單到賬金額"
                        />
                    }
                >
                    <ReactIntlCurrencyWithFixedDecimal value={d.Amount} />
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.CurrencyRate"
                            description="匯率"
                        />
                    }
                >
                    <ReactIntlCurrencyWithFixedDecimal value={d.ExchangeRate} />
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="Share.Fields.CreateTime"
                            description="建立時間"
                        />
                    }
                >
                    <DateWithFormat time={d.CreateTime} />
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.CreatedUser"
                            description="後台充值人員"
                        />
                    }
                >
                    {d.CreatedUserId ? d.CreatedUserId : NO_DATA}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.ReceivedAccount"
                            description="收款賬戶"
                        />
                    }
                    span={2}
                >
                    {d.BankAccount}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.Remarks"
                            description="訂單附言"
                        />
                    }
                    span={2}
                >
                    {d.AppendMessage ? d.AppendMessage : NO_DATA}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.Image"
                            description="圖片"
                        />
                    }
                    span={2}
                >
                    {d.ImageUrl ? (
                        <Space>
                            <Image
                                width={32}
                                src={d.ImageUrl}
                                style={{ cursor: 'pointer' }}
                            />
                            <Typography.Text type="secondary">
                                <FormattedMessage
                                    id="PageDepositList.Note.ForImage"
                                    description="单击图像可以放大显示"
                                />
                            </Typography.Text>
                        </Space>
                    ) : (
                        NO_DATA
                    )}
                </Descriptions.Item>
            </Descriptions>
            <Divider dashed />
            <Descriptions
                title={
                    <FormattedMessage
                        id="PageDepositList.Areas.PaymentDetail"
                        description="支付訊息"
                    />
                }
                column={2}
            >
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.TransactionId"
                            description="交易序號"
                        />
                    }
                >
                    {d.TransactionId ? d.TransactionId : NO_DATA}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.PaymentBankAccount"
                            description="支付賬號"
                        />
                    }
                >
                    {d.PaymentBankAccount ? d.PaymentBankAccount : NO_DATA}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.ActualReceivedAmount"
                            description="實際上分金額"
                        />
                    }
                >
                    {d.DepositAmount ? (
                        <ReactIntlCurrencyWithFixedDecimal
                            value={d.DepositAmount}
                        />
                    ) : (
                        NO_DATA
                    )}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.Status"
                            description="訂單狀態"
                        />
                    }
                >
                    {d.StatusText}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.FeeForSite"
                            description="手續費(站台)"
                        />
                    }
                >
                    <ReactIntlCurrencyWithFixedDecimal value={d.DepositFee} />
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="PageDepositList.Fields.FeeForMember"
                            description="手續費(會員)"
                        />
                    }
                >
                    <ReactIntlCurrencyWithFixedDecimal
                        value={d.MemberFeeAmount}
                    />
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage
                            id="Share.Fields.ModifiedTime"
                            description="異動時間"
                        />
                    }
                >
                    <DateWithFormat time={d.ModifyTime} />
                </Descriptions.Item>
                {!isEditMode && (
                    <Descriptions.Item
                        label={
                            <FormattedMessage
                                id="Share.Fields.Remarks"
                                description="備註"
                            />
                        }
                        span={2}
                    >
                        {d.Remarks ? d.Remarks : NO_DATA}
                    </Descriptions.Item>
                )}
            </Descriptions>
            {isEditMode && (
                <Form form={form} onFinish={onDrawerSave}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.Fields.Remarks"
                                description="備註"
                            />
                        }
                        name="Remarks"
                    >
                        <Input.TextArea />
                    </Form.Item>
                </Form>
            )}
        </BasicDrawerWrapper>
    ) : (
        <Skeleton active />
    )
}
