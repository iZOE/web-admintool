import React, { createContext } from 'react'
import { FormattedMessage } from 'react-intl'
import { Space } from 'antd'
import { useLocation } from 'react-router-dom'
import queryString from 'query-string'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import DataTable from 'components/DataTable'
import ExportReportButton from 'components/ExportReportButton'
import Permission from 'components/Permission'
import ReportScaffold from 'components/ReportScaffold'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import SummaryView from './Summary'
import BackStageDepositButton from './BackStageDepositButton'
import * as PERMISSION from 'constants/permissions'

const {
    DEPOSIT_WITHDRAW_DEPOSIT_VIEW,
    DEPOSIT_WITHDRAW_DEPOSIT_EXPORT,
    DEPOSIT_WITHDRAW_DEPOSIT_BACKEND_DEPOSIT,
} = PERMISSION

export const PageContext = createContext()

export const QUERY_API_URL = '/api/Deposit/Search'

// 充值清單
const DepositPage = props => {
    // 查詢參數 有三種：form表單, 分頁區, table排序區
    const location = useLocation()
    let search = queryString.parse(location.search)
    search = Object.keys(search).reduce((result, key) => {
        return {
            ...result,
            [key]: isNaN(parseInt(search[key], 10))
                ? search[key]
                : Number(search[key]),
        }
    }, {})

    const {
        fetching,
        dataSource,
        boundedMutate,
        onUpdateCondition,
        onReady,
        condition,
    } = useGetDataSourceWithSWR({
        url: QUERY_API_URL,
        defaultSortKey: 'ModifyTime',
        autoFetch: true,
        initialCondition: Object.values(search).length ? search : null,
    })

    return (
        <PageContext.Provider value={{ dataSource, boundedMutate }}>
            <Permission functionIds={[DEPOSIT_WITHDRAW_DEPOSIT_VIEW]}>
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            condition={condition}
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                            initialValueForTrigger={{ ...search }}
                        />
                    }
                    summaryComponent={
                        <SummaryView
                            summary={dataSource && dataSource.Summary}
                            totalCount={
                                dataSource && dataSource.List.TotalCount
                            }
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            loading={fetching}
                            title={
                                <FormattedMessage id="Share.Table.SearchResult" />
                            }
                            config={COLUMNS_CONFIG}
                            dataSource={dataSource && dataSource.List.Data}
                            total={dataSource && dataSource.List.TotalCount}
                            rowKey={record => record.DepositNo}
                            extendArea={
                                <Space>
                                    <Permission
                                        functionIds={[
                                            DEPOSIT_WITHDRAW_DEPOSIT_EXPORT,
                                        ]}
                                    >
                                        <ExportReportButton
                                            condition={condition}
                                            actionUrl="/api/Deposit/Export"
                                        />
                                    </Permission>
                                    <Permission
                                        functionIds={[
                                            DEPOSIT_WITHDRAW_DEPOSIT_BACKEND_DEPOSIT,
                                        ]}
                                    >
                                        <BackStageDepositButton />
                                    </Permission>
                                </Space>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
        </PageContext.Provider>
    )
}

export default DepositPage
