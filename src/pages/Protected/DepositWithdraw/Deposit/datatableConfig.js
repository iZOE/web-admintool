import React from 'react';
import { FormattedMessage } from 'react-intl';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenDepositDetailDrawerButton from './OpenDepositDetailDrawerButton';
import { NO_DATA } from 'constants/noData';
import { MODE } from 'constants/mode';

const isEditable = statusId => {
  // 1. 待確認 / 4. 交易逾期
  return [1, 4].indexOf(Number(statusId)) > -1;
};

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageDepositList.Fields.OrderId" description="訂單編號" />,
    dataIndex: 'DepositNo',
    key: 'DepositNo',
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { DepositNo }, _2) => <OpenDepositDetailDrawerButton depositId={DepositNo} mode={MODE.VIEW} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.CreateTime" description="建立時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, _1, _2) => {
      return <OpenMemberDetailButton memberId={text} memberName={text} />;
    },
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.DepositType" description="充值類型" />,
    dataIndex: 'DepositTypeName',
    key: 'DepositTypeName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.Fields.Bank" description="銀行" />,
    dataIndex: 'BankName',
    key: 'BankName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => (text ? text : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.PaymentType" description="支付類型" />,
    dataIndex: 'PaymentType',
    key: 'PaymentType',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.ReceivedAccount" description="收款賬戶" />,
    dataIndex: 'BankAccount',
    key: 'BankAccount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.CurrencyType" description="幣別" />,
    dataIndex: 'CurrencyName',
    key: 'CurrencyName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => (value ? value : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.RequestAmount" description="訂單金額" />,
    dataIndex: 'RequestAmount',
    key: 'RequestAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.ReceivedAmount" description="訂單到賬金額" />,
    dataIndex: 'Amount',
    key: 'Amount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.CurrencyRate" description="匯率" />,
    dataIndex: 'ExchangeRate',
    key: 'ExchangeRate',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => (value ? <ReactIntlCurrencyWithFixedDecimal value={value} /> : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.FeeForSite" description="手續費(站台) " />,
    dataIndex: 'DepositFee',
    key: 'DepositFee',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => (value !== null ? <ReactIntlCurrencyWithFixedDecimal value={value} /> : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.FeeForMember" description="手續費(會員)" />,
    dataIndex: 'MemberFeeAmount',
    key: 'MemberFeeAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => (value !== null ? <ReactIntlCurrencyWithFixedDecimal value={value} /> : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.DepositPreferentialAmount" description="充值優惠金額" />,
    dataIndex: 'PreferentialAmount',
    key: 'PreferentialAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.ActualReceivedAmount" description="實際上分金額" />,
    dataIndex: 'DepositAmount',
    key: 'DepositAmount',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageDepositList.Fields.Status" description="訂單狀態" />,
    dataIndex: 'StatusText',
    key: 'StatusText',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemModifyTime',
    key: 'SystemModifyTime',
    render: (_1, { SystemModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.Modifier" description="異動人員" />,
    dataIndex: 'ModifyUserId',
    key: 'ModifyUserId',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyUserAccount }, _2) => (ModifyUserAccount ? ModifyUserAccount : NO_DATA),
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'action',
    key: 'action',
    fixed: 'right',
    render: (_1, { Status, DepositNo }, _2) => {
      const isShownActionButton = isEditable(Status);
      return isShownActionButton ? (
        <OpenDepositDetailDrawerButton
          depositId={DepositNo}
          displayText={<FormattedMessage id="PageDepositList.Actions.Confirm" description="交易確認" />}
          mode={MODE.EDIT}
        />
      ) : (
        NO_DATA
      );
    },
  },
];
