import makeService from 'utils/makeService'

const paymentService = () => {
    const getPaymentSetting = paymentId =>
        makeService({
            method: 'get',
            url: `/api/PaymentSetting/${paymentId}`,
        })()

    const updateAmountSetting = (paymentId, payload) =>
        makeService({
            method: 'put',
            url: `/api/paymentSetting/depositAmountSetting/${paymentId}`,
        })(payload)

    const addAmountSetting = (paymentId, payload) =>
        makeService({
            method: 'post',
            url: `/api/paymentSetting/depositAmountSetting/${paymentId}`,
        })(payload)

    const deleteAmountSetting = DpId =>
        makeService({
            method: 'delete',
            url: `/api/paymentSetting/depositAmountSetting/${DpId}`,
        })()

    const updatePaymentSetting = (paymentId, payload) =>
        makeService({
            method: 'put',
            url: `/api/PaymentSetting/${paymentId}`,
        })(payload)

    const getPaymentTypeTags = () =>
        makeService({
            method: 'get',
            url: `/api/Option/PaymentTypes/Tags`,
        })()

    const putPaymentTypes = payload =>
        makeService({
            method: 'put',
            url: `/api/PaymentSetting/paymentTypes`,
        })(payload)

    const getPaymentTypes = () =>
        makeService({
            method: 'get',
            url: `/api/PaymentSetting/paymentTypes`,
        })()

    return {
        getPaymentSetting,
        updateAmountSetting,
        addAmountSetting,
        deleteAmountSetting,
        updatePaymentSetting,
        getPaymentTypeTags,
        putPaymentTypes,
        getPaymentTypes,
    }
}

export default paymentService
