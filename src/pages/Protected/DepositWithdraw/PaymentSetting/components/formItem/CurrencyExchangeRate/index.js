import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Form } from 'antd'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'

export default function CurrencyExangeRateFormItem({ label, value }) {
    return (
        <Form.Item
            label={
                label || (
                    <FormattedMessage
                        id="PagePaymentSetting.Form.Label.CurrencyExangeRate"
                        description="當前匯率"
                    />
                )
            }
        >
            <ReactIntlCurrencyWithFixedDecimal value={value} />
        </Form.Item>
    )
}
