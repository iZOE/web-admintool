import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Form } from 'antd'
import { NO_DATA } from 'constants/noData'

export default function WalletProtocolFormItem({ label, value }) {
    return (
        <Form.Item
            label={
                label || (
                    <FormattedMessage
                        id="PagePaymentSetting.Form.Label.WalletProtocol"
                        description="錢包協議"
                    />
                )
            }
        >
            {value ? value : NO_DATA}
        </Form.Item>
    )
}
