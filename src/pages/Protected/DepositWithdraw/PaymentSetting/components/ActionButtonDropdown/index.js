import React, { useMemo, useState, useCallback, lazy } from 'react'
import { Link } from 'react-router-dom'
import { FormattedMessage } from 'react-intl'
import { Button, Dropdown, Menu, Space, Spin } from 'antd'
import {
    DollarCircleOutlined,
    DownOutlined,
    EditOutlined,
    InteractionOutlined,
} from '@ant-design/icons'

import OpenDetailButton from '../../Home/components/OpenDetailButton'
import CONTENT_TYPE from '../../constants/contentType'

const UpdateCurrencyRateModalFormAsync = lazy(() =>
    import('../UpdateCurrencyRateModalForm')
)

export default function ActionButtonDropdown({
    paymentId,
    hasEditPermission,
    hasAmountEditPermission,
    hasCurrencyRateEditPermission,
}) {
    const [isShownModal, setIsShownModal] = useState(false)

    const handleToggleModal = useCallback(() => {
        setIsShownModal(prevIsShown => !prevIsShown)
    }, [])

    const menu = useMemo(
        () => (
            <Menu>
                {hasEditPermission && (
                    <Menu.Item>
                        <Link
                            to={`/deposit-withdraw/payment-setting/detail/${paymentId}`}
                        >
                            <Button type="link">
                                <Space>
                                    <EditOutlined />
                                    <FormattedMessage
                                        id="Share.ActionButton.Edit"
                                        description="編輯"
                                    />
                                </Space>
                            </Button>
                        </Link>
                    </Menu.Item>
                )}

                {hasAmountEditPermission && (
                    <Menu.Item>
                        <OpenDetailButton
                            paymentId={paymentId}
                            contentType={CONTENT_TYPE.DEPOSIT_AMOUNT_SETTING}
                            displayText={
                                <Space>
                                    <DollarCircleOutlined />
                                    <FormattedMessage
                                        id="PagePaymentSetting.DataTable.ActionButton.AmountEdit"
                                        description="金额设置"
                                    />
                                </Space>
                            }
                        />
                    </Menu.Item>
                )}

                {hasCurrencyRateEditPermission && (
                    <Menu.Item onClick={handleToggleModal}>
                        <Button type="link">
                            <Space>
                                <InteractionOutlined />
                                <FormattedMessage
                                    id="PagePaymentSetting.DataTable.ActionButton.UpdateCurrencyRate"
                                    description="變更匯率"
                                />
                            </Space>
                        </Button>
                    </Menu.Item>
                )}
            </Menu>
        ),
        [
            hasEditPermission,
            hasAmountEditPermission,
            hasCurrencyRateEditPermission,
            paymentId,
            handleToggleModal,
        ]
    )

    return (
        <>
            {menu ? (
                <Dropdown arrow overlay={menu} placement="bottomCenter">
                    <Button type="link">
                        <FormattedMessage
                            id="Share.ActionButton.Manage"
                            description="管理"
                        />
                        <DownOutlined style={{ marginLeft: 6 }} />
                    </Button>
                </Dropdown>
            ) : (
                <Spin />
            )}
            {isShownModal && (
                <UpdateCurrencyRateModalFormAsync
                    paymentId={paymentId}
                    isVisible={isShownModal}
                    handleToggleModal={handleToggleModal}
                />
            )}
        </>
    )
}
