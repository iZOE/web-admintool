import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Detail from './Detail'

export default function PaymentSetting() {
    return (
        <Switch>
            <Route exact path="/deposit-withdraw/payment-setting">
                <Home />
            </Route>
            <Route path="/deposit-withdraw/payment-setting/detail/:paymentId">
                <Detail />
            </Route>
        </Switch>
    )
}
