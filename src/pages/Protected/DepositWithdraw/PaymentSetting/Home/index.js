import React, { useMemo } from 'react'
import { UnorderedListOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ReportScaffold from 'components/ReportScaffold'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import DrawerContextProvider from 'contexts/DrawerContext'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import CONTENT_TYPE from '../constants/contentType'
import OpenDetailButton from './components/OpenDetailButton'
import ActionButtonDropdown from '../components/ActionButtonDropdown'
import usePermission from 'hooks/usePermission'

const {
    DEPOSIT_WITHDRAW_PAYMENT_SETTING_VIEW,
    DEPOSIT_WITHDRAW_PAYMENT_SETTING_EDIT,
    DEPOSIT_WITHDRAW_PAYMENT_SETTING_AMOUNT_SETTING,
    DEPOSIT_WITHDRAW_PAYMENT_SETTING_UPDATE_CURRENCY_RATE,
} = PERMISSION

const PageView = () => {
    const [
        hasEditPermission,
        hasAmountEditPermission,
        hasCurrencyRateEditPermission,
    ] = usePermission(
        DEPOSIT_WITHDRAW_PAYMENT_SETTING_EDIT,
        DEPOSIT_WITHDRAW_PAYMENT_SETTING_AMOUNT_SETTING,
        DEPOSIT_WITHDRAW_PAYMENT_SETTING_UPDATE_CURRENCY_RATE
    )

    const {
        fetching,
        dataSource,
        condition,
        onReady,
        onUpdateCondition,
    } = useGetDataSourceWithSWR({
        url: '/api/PaymentSetting/Search',
        defaultSortKey: 'PaymentId',
        autoFetch: true,
    })

    const columnsConfig = useMemo(() => {
        const hasManageColumn =
            hasEditPermission ||
            hasAmountEditPermission ||
            hasCurrencyRateEditPermission

        if (hasManageColumn) {
            return [
                ...COLUMNS_CONFIG,
                {
                    title: (
                        <FormattedMessage
                            id="Share.ActionButton.Manage"
                            description="管理"
                        />
                    ),
                    width: 100,
                    fixed: 'right',
                    align: 'center',
                    isManage: true,
                    render: (_1, { PaymentId }, _2) => (
                        <ActionButtonDropdown
                            paymentId={PaymentId}
                            hasEditPermission={hasEditPermission}
                            hasAmountEditPermission={hasAmountEditPermission}
                            hasCurrencyRateEditPermission={
                                hasCurrencyRateEditPermission
                            }
                        />
                    ),
                },
            ]
        }

        return [...COLUMNS_CONFIG]
    }, [
        hasEditPermission,
        hasAmountEditPermission,
        hasCurrencyRateEditPermission,
    ])

    return (
        <DrawerContextProvider>
            <Permission
                isPage
                functionIds={[DEPOSIT_WITHDRAW_PAYMENT_SETTING_VIEW]}
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢絝果"
                                />
                            }
                            config={columnsConfig}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            onUpdate={onUpdateCondition}
                            rowKey={record => record.PaymentId}
                            extendArea={
                                <OpenDetailButton
                                    type="primary"
                                    icon={
                                        <UnorderedListOutlined
                                            style={{ marginRight: 6 }}
                                        />
                                    }
                                    contentType={
                                        CONTENT_TYPE.PAYMENT_TYPE_SETTING
                                    }
                                    displayText={
                                        <FormattedMessage id="PagePaymentSetting.DataTable.ActionButton.PaymentTypeSetting" />
                                    }
                                />
                            }
                        />
                    }
                />
            </Permission>
        </DrawerContextProvider>
    )
}

export default PageView
