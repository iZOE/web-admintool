import React, { useContext, useEffect } from 'react'
import { Checkbox, Col, Form, Input, Row } from 'antd'
import { FormattedMessage } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import FormItemsSimpleSelect from 'components/FormItems/SimpleSelect'
import TreeLevelSelect from 'components/TreeLevelSelect'
import QueryButton from 'components/FormActionButtons/Query'

const { useForm } = Form
const CONDITION_INITIAL_VALUE = {
    DepositOrBankTypeId: null,
    PaymentTypeId: null,
    BankAccountName: null,
    IsEnableOnly: false,
}

function Condition({ onReady, onUpdate }) {
    const { isReady } = useContext(ConditionContext)
    const [form] = useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ DepositOrBankTypeId, ...restValues }) {
        const [DepositTypeId, BankTypeId] = DepositOrBankTypeId || [null, null]
        return {
            ...restValues,
            DepositTypeId,
            BankTypeId,
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onFinish}
        >
            <Row gutter={[16, 8]}>
                <Col sm={4}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.QueryCondition.Type"
                                description="類型或銀行"
                            />
                        }
                        name="DepositOrBankTypeId"
                    >
                        <TreeLevelSelect needAll url="/api/Option/BankList" />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <FormItemsSimpleSelect
                        showSearch
                        needAll
                        url="/api/Option/PaymentTypes"
                        name="PaymentTypeId"
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.QueryCondition.PaymentTypes"
                                description="支付類型"
                            />
                        }
                    />
                </Col>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.QueryCondition.Account"
                                defaultMessage="使用者帳號"
                            />
                        }
                        name="BankAccountName"
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <Form.Item name="IsEnableOnly" valuePropName="checked">
                        <Checkbox>
                            <FormattedMessage
                                id="Share.QueryCondition.IsOnlyEnabled"
                                defaultMessage="不显示停用帐号"
                            />
                        </Checkbox>
                    </Form.Item>
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
