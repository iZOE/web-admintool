import React from 'react';
import { Tag } from 'antd';
import { FormattedMessage } from 'react-intl';
import { NO_DATA } from 'constants/noData';
import { ENABLE_STATUS } from 'constants/stastus';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenDetailButton from './components/OpenDetailButton';
import CONTENT_TYPE from '../constants/contentType';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.PaymentId" description="支付ID" />,
    dataIndex: 'PaymentId',
    key: 'PaymentId',
    fixed: 'left',
    render: (_1, { PaymentId }, _2) => (
      <OpenDetailButton contentType={CONTENT_TYPE.VIEW_DETAIL} paymentId={PaymentId} displayText={PaymentId} />
    ),
  },
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.DisplayName" description="显示名称" />,
    dataIndex: 'DisplayName',
    key: 'DisplayName',
    render: (_1, { DisplayName }, _2) => (DisplayName ? DisplayName : NO_DATA),
  },
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.DepositTypeName" description="類型" />,
    dataIndex: 'DepositTypeName',
    key: 'DepositTypeName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  // 銀行
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.BankName" description="銀行" />,
    dataIndex: 'BankName',
    key: 'BankName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  // 帳戶
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.BankAccount" description="帳戶" />,
    dataIndex: 'BankAccount',
    key: 'BankAccount',
    isShow: true,
  },
  // 戶名
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.AccountName" description="戶名" />,
    dataIndex: 'AccountName',
    key: 'AccountName',
    isShow: true,
  },
  // 支付類型
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.PaymentType" description="支付類型" />,
    dataIndex: 'PaymentType',
    key: 'PaymentType',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  // 可用會員
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.MemberLevelName" description="可用會員" />,
    dataIndex: 'MemberLevelName',
    key: 'MemberLevelName',
    isShow: true,
  },
  // 可用裝置
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.AvailableDevice" description="可用裝置" />,
    dataIndex: 'AvailableDevice',
    key: 'AvailableDevice',
    isShow: true,
    render: value => {
      if (value && Array.isArray(value)) {
        if (!value.length) {
          return NO_DATA;
        }
        return value.map((d, i) => (
          <>
            <FormattedMessage id={`PagePaymentSetting.DataTable.Cell.Device.${d}`} />
            {i !== value.length - 1 && ', '}
          </>
        ));
      }
    },
  },
  // 可充站台
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.AllowDepositeSite" description="可充站台" />,
    dataIndex: 'AllowDepositeSite',
    key: 'AllowDepositeSite',
    isShow: true,
    render: value => {
      if (value && Array.isArray(value)) {
        return value.map((d, i) => (
          <>
            <FormattedMessage id={`PagePaymentSetting.DataTable.Cell.AllowDepositeSite.${d}`} />
            {i !== value.length - 1 && ', '}
          </>
        ));
      }
    },
  },
  // 來源狀態
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.SourceStatus" description="來源狀態" />,
    dataIndex: 'SourceStatus',
    key: 'SourceStatus',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => (
      <Tag color={value ? 'processing' : 'warning'}>
        <FormattedMessage
          id={`Share.Status.${value ? 'Enable' : 'Disable'}`}
          defaultMessage={value ? '啟用' : '停用'}
        />
      </Tag>
    ),
  },
  // 狀態
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.EnableStatus" description="狀態" />,
    dataIndex: 'EnableStatus',
    key: 'EnableStatus',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => (
      <Tag color={value ? (value === 1 ? 'processing' : 'default') : 'warning'}>
        <FormattedMessage id={`Share.Status.${ENABLE_STATUS[value]}`} defaultMessage={ENABLE_STATUS[value]} />
      </Tag>
    ),
  },
  // 累計收款(上次啟用後)
  {
    title: (
      <FormattedMessage id="PagePaymentSetting.DataTable.Title.AccumulatedAmount" description="累計收款(上次啟用後) " />
    ),
    dataIndex: 'AccumulatedAmount',
    key: 'AccumulatedAmount',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  // 異動人員
  {
    title: <FormattedMessage id="PagePaymentSetting.DataTable.Title.ModifyUserAccount" description="異動人員" />,
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
    isShow: true,
  },
];
