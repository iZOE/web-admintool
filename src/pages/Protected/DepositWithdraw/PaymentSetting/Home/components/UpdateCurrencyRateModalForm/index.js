import React from 'react'
import useSWR from 'swr'
import axios from 'axios'
import caller from 'utils/fetcher'
import { useIntl, FormattedMessage } from 'react-intl'
import { Form, InputNumber, Modal, Spin, message } from 'antd'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import DoubleConfirmButton from 'components/FormActionButtons/DoubleConfirmButton'

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 10 },
}

const rules = [
    {
        required: true,
        message: <FormattedMessage id="Share.FormValidate.Required.Input" />,
    },
]

export default function UpdateCurrencyRateModalForm({
    paymentId,
    isVisible,
    handleToggleModal,
}) {
    const [form] = Form.useForm()
    const intl = useIntl()

    const successMsg = intl.formatMessage({
        id: 'Share.SuccessMessage.UpdateSuccess',
    })

    const { data: d, isValidating } = useSWR(
        paymentId ? `/api/PaymentSetting/ExchangeRate/${paymentId}` : null,
        url => axios(url).then(res => res && res.data.Data)
    )

    const onModalSave = values => {
        const payload = {
            ...values,
            PaymentId: paymentId,
        }

        caller({
            method: 'put',
            endpoint: 'api/PaymentSetting/ExchangeRate',
            body: payload,
        })
            .then(() => {
                message.success(successMsg, 5)
            })
            .then(() => {
                handleToggleModal()
            })
    }

    return (
        <Modal
            centered
            destroyOnClose
            footer={[
                <DoubleConfirmButton
                    key="ok"
                    onOk={() => form.submit()}
                    confirmMsg={
                        <FormattedMessage
                            id="PagePaymentSetting.Modal.CurrencyRateUpdate.DoubleConfirmMsg"
                            description="新匯率將於上級簽核通過後生效，確認要變更嗎？"
                        />
                    }
                />,
            ]}
            onCancel={() => handleToggleModal()}
            title={
                <FormattedMessage
                    id="PagePaymentSetting.Modal.CurrencyRateUpdate.Title"
                    description="變更匯率"
                />
            }
            visible={isVisible}
        >
            {!isValidating && d ? (
                <Form {...layout} form={form} onFinish={onModalSave}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.Fields.Type"
                                description="類型"
                            />
                        }
                    >
                        <span className="ant-form-text">
                            {d.DepositTypeName}
                        </span>
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.Detail.BankName"
                                description="銀行"
                            />
                        }
                    >
                        <span className="ant-form-text">{d.BankName}</span>
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.Detail.BankAccount"
                                description="帳戶"
                            />
                        }
                    >
                        <span className="ant-form-text">{d.BankAccount}</span>
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.Detail.AccountName"
                                description="戶名"
                            />
                        }
                    >
                        <span className="ant-form-text">{d.AccountName}</span>
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.Detail.CurrencyType"
                                description="幣別"
                            />
                        }
                    >
                        <span className="ant-form-text">{d.CurrencyName}</span>
                    </Form.Item>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.Detail.PresentCurrencyRate"
                                description="當前匯率"
                            />
                        }
                    >
                        <span className="ant-form-text">
                            <ReactIntlCurrencyWithFixedDecimal
                                value={d.ExchangeRate}
                            />
                        </span>
                    </Form.Item>
                    <Form.Item
                        hasFeedback
                        label={
                            <FormattedMessage
                                id="PagePaymentSetting.Detail.NewCurrencyRate"
                                description="新匯率"
                            />
                        }
                        rules={rules}
                        name="CurrencyRate"
                    >
                        <InputNumber
                            autoFocus
                            precision={2}
                            step={0.02}
                            style={{ width: '100%' }}
                            type="number"
                        />
                    </Form.Item>
                </Form>
            ) : (
                <Spin />
            )}
        </Modal>
    )
}
