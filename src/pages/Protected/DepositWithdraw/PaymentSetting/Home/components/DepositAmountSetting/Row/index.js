import React, {
    useState,
    useMemo,
    Children,
    useContext,
    isValidElement,
    cloneElement,
} from 'react'
import { FormattedMessage } from 'react-intl'
import { Form, Button, Space, Popconfirm } from 'antd'
import { mutate } from 'swr'
import { DrawerContext } from 'contexts/DrawerContext'
import EditableAmountCell from '../EditableAmountCell'
import { Wrapper } from './Styled'
import menusService from '../../../../services'

const Row = ({
    children,
    paymentId,
    onCancelCreate,
    getExistedAmount,
    maxPaymentAmount,
    minPaymentAmount,
    record,
    ...restProps
}) => {
    const { onEditing } = useContext(DrawerContext)
    const [form] = Form.useForm()
    const [editing, setEditing] = useState(false)

    const {
        addAmountSetting,
        updateAmountSetting,
        deleteAmountSetting,
    } = menusService()

    const doEdit = () => {
        setEditing(true)
        onEditing(true)
        form.setFieldsValue(record)
    }

    const doCancel = isCreateing => {
        setEditing(false)
        onEditing(false)
        if (isCreateing) {
            onCancelCreate()
        }
    }

    const onSave = async isCreateing => {
        try {
            const formData = await form.validateFields()
            if (isCreateing) {
                addAmountSetting(paymentId, {
                    Amount: formData.Amount,
                }).then(result => actionSuccess())
            } else {
                updateAmountSetting(paymentId, {
                    DpId: record.DpId,
                    ...formData,
                }).then(result => actionSuccess())
            }
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo)
        }
    }

    const onDelete = DpId => {
        deleteAmountSetting(DpId).then(result => actionSuccess())
    }

    const actionSuccess = () => {
        mutate(`/api/paymentSetting/depositAmountSetting/${paymentId}`)
        setEditing(false)
        onEditing(false)
    }

    const childrenWithProps = useMemo(() => {
        return Children.map(children, child => {
            if (isValidElement(child)) {
                switch (child.key) {
                    case 'Amount':
                        return cloneElement(child, {
                            render: (text, record, index) => (
                                <EditableAmountCell
                                    onPressEnter={() =>
                                        onSave(record.isCreateing)
                                    }
                                    record={{
                                        ...record,
                                        maxPaymentAmount,
                                        minPaymentAmount,
                                        existedAmount: getExistedAmount(index),
                                    }}
                                    editing={editing || record.isCreateing}
                                />
                            ),
                        })
                    case 'Manage':
                        return cloneElement(child, {
                            render: (text, record, index) => (
                                <Space size="small">
                                    {editing || record.isCreateing ? (
                                        <>
                                            <Button
                                                type="link"
                                                onClick={() =>
                                                    onSave(record.isCreateing)
                                                }
                                            >
                                                <FormattedMessage id="Share.ActionButton.Save" />
                                            </Button>
                                            <Button
                                                type="link"
                                                onClick={() =>
                                                    doCancel(record.isCreateing)
                                                }
                                            >
                                                <FormattedMessage id="Share.ActionButton.Cancel" />
                                            </Button>
                                        </>
                                    ) : (
                                        <>
                                            <Button
                                                type="link"
                                                onClick={doEdit}
                                            >
                                                <FormattedMessage id="Share.ActionButton.Edit" />
                                            </Button>
                                            <Popconfirm
                                                title={
                                                    <FormattedMessage id="Share.ActionButton.SureToDelete" />
                                                }
                                                onConfirm={() =>
                                                    onDelete(record.DpId)
                                                }
                                            >
                                                <Button type="link" danger>
                                                    <FormattedMessage id="Share.ActionButton.Delete" />
                                                </Button>
                                            </Popconfirm>
                                        </>
                                    )}
                                </Space>
                            ),
                        })
                    default:
                        return child
                }
            }
        })
    }, [children, editing])

    return (
        <>
            <Wrapper {...restProps}>
                <Form form={form} component={false}>
                    {childrenWithProps}
                </Form>
            </Wrapper>
        </>
    )
}

export default Row
