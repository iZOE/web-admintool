import React, { useMemo } from 'react'
import { FormattedMessage } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import { Form, Input } from 'antd'

export default function EditableAmountCell({ record, editing, onPressEnter }) {
    const { Amount, existedAmount, maxPaymentAmount, minPaymentAmount } = record

    const RULES = useMemo(
        () => [
            () => ({
                validator(rule, value) {
                    const amount = Number(value)
                    if (amount) {
                        if (
                            amount >= minPaymentAmount &&
                            amount <= maxPaymentAmount
                        ) {
                            if (existedAmount.has(amount)) {
                                return Promise.reject(
                                    <FormattedMessage id="PagePaymentSetting.Form.Validator.Repeat" />
                                )
                            }
                            return Promise.resolve()
                        } else if (amount < minPaymentAmount) {
                            return Promise.reject(
                                <FormattedMessage
                                    id="PagePaymentSetting.Form.Validator.AmountMin"
                                    values={{
                                        min: (
                                            <ReactIntlCurrencyWithFixedDecimal
                                                value={minPaymentAmount}
                                            />
                                        ),
                                    }}
                                />
                            )
                        } else if (amount > maxPaymentAmount) {
                            return Promise.reject(
                                <FormattedMessage
                                    id="PagePaymentSetting.Form.Validator.AmountMax"
                                    values={{
                                        max: (
                                            <ReactIntlCurrencyWithFixedDecimal
                                                value={maxPaymentAmount}
                                            />
                                        ),
                                    }}
                                />
                            )
                        }
                    } else {
                        return Promise.reject(
                            <FormattedMessage id="Share.FormValidate.Required.Input" />
                        )
                    }
                },
            }),
        ],
        [existedAmount]
    )

    return editing ? (
        <Form.Item name="Amount" hasFeedback rules={RULES}>
            <Input type="number" onPressEnter={onPressEnter} />
        </Form.Item>
    ) : (
        <ReactIntlCurrencyWithFixedDecimal value={Amount} />
    )
}
