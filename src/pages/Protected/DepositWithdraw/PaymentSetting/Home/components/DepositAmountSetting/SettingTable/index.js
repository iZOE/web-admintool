import React, { useState, useEffect, useContext } from 'react'
import { FormattedMessage } from 'react-intl'
import { Table, Row, Col, Button } from 'antd'
import { NO_DATA } from 'constants/noData'
import { UserContext } from 'contexts/UserContext'
import DateWithFormat from 'components/DateWithFormat'
import TableRow from '../Row'

export default function SettingTable({
    data,
    paymentId,
    maxPaymentAmount,
    minPaymentAmount,
}) {
    const { data: userData } = useContext(UserContext)
    const { DepositAmountList, ModifyTime } = data
    const [dataSource, setDataSource] = useState([])
    const [isCreateing, setIsCreateing] = useState(false)

    const getExistedAmount = index =>
        new Set(
            DepositAmountList.reduce(
                (arr, { Amount }, idx) =>
                    idx !== index ? [...arr, Amount] : arr,
                []
            )
        )

    const COLUMNS_CONFIG = [
        {
            title: (
                <FormattedMessage id="PagePaymentSetting.Detail.SettingTable.Column.Index" />
            ),
            key: 'Index',
            dataIndex: 'Index',
        },
        {
            title: (
                <FormattedMessage id="PagePaymentSetting.Detail.SettingTable.Column.Amount" />
            ),
            key: 'Amount',
            dataIndex: 'Amount',
            width: 300,
        },
        {
            title: (
                <FormattedMessage id="PagePaymentSetting.Detail.SettingTable.Column.ModifyUserAccount" />
            ),
            key: 'ModifyUserAccount',
            dataIndex: 'ModifyUserAccount',
        },
        {
            key: 'Manage',
            dataIndex: 'Manage',
            width: 70,
        },
    ]

    const onCancelCreate = () => {
        setIsCreateing(false)
        setDataSource(dataSource.slice(0, dataSource.length - 1))
    }

    const onCreate = () => {
        setIsCreateing(true)
        setDataSource([
            ...dataSource,
            {
                isCreateing: true,
                Index: NO_DATA,
                Amount: null,
                ModifyUserAccount: userData.UserName,
                key: `new_${dataSource.length + 1}`,
            },
        ])
    }

    useEffect(() => {
        setIsCreateing(false)
        setDataSource(
            DepositAmountList.reduce(
                (arr, cur, index) => [
                    ...arr,
                    {
                        Index: index + 1,
                        key: cur.DpId,
                        ...cur,
                    },
                ],
                []
            )
        )
    }, [DepositAmountList])

    return (
        <Table
            title={() => (
                <Row>
                    <Col span={16}>
                        <FormattedMessage
                            id="PagePaymentSetting.Detail.SettingTable.PaymentModifyTime"
                            values={{
                                time: <DateWithFormat time={ModifyTime} />,
                            }}
                        />
                    </Col>
                    {dataSource.length < 8 && !isCreateing && (
                        <Col style={{ textAlign: 'right' }} span={8}>
                            <Button type="link" onClick={onCreate}>
                                <FormattedMessage id="Share.ActionButton.Create" />
                            </Button>
                        </Col>
                    )}
                </Row>
            )}
            size="small"
            components={{
                body: {
                    row: TableRow,
                },
            }}
            onRow={(record, index) => ({
                paymentId,
                onCancelCreate,
                maxPaymentAmount,
                minPaymentAmount,
                getExistedAmount,
                record,
            })}
            columns={COLUMNS_CONFIG}
            dataSource={dataSource}
            bordered
            pagination={false}
        />
    )
}
