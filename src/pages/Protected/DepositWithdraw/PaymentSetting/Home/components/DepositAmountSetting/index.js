import React from 'react'
import { Skeleton } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import BasicDetail from '../BasicDetail'
import SettingTable from './SettingTable'

export default function DepositAmountSetting({ paymentId }) {
    const { data: detail } = useSWR(
        paymentId ? `/api/PaymentSetting/${paymentId}` : null,
        url => axios(url).then(res => res && res.data.Data)
    )

    const { data: depositAmountData } = useSWR(
        paymentId
            ? `/api/paymentSetting/depositAmountSetting/${paymentId}`
            : null,
        url => axios(url).then(res => res && res.data.Data)
    )

    return detail ? (
        <BasicDetail detail={detail}>
            {depositAmountData ? (
                <SettingTable
                    data={depositAmountData}
                    paymentId={paymentId}
                    maxPaymentAmount={detail.MaxPaymentAmount}
                    minPaymentAmount={detail.MinPaymentAmount}
                />
            ) : (
                <Skeleton active />
            )}
        </BasicDetail>
    ) : (
        <Skeleton active />
    )
}
