import React, { useEffect, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Descriptions, Tag } from 'antd'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import { NO_DATA } from 'constants/noData'
import { ENABLE_STATUS } from 'constants/stastus'
import optionsServices from 'services/optionsServices'

export default function BasicDetail({ detail, isView, children }) {
    const { getMemberLevel } = optionsServices()
    const [memberLevels, setMemberLevels] = useState([])

    useEffect(() => {
        if (isView) {
            getMemberLevel().then(res => {
                setMemberLevels(
                    res.reduce(
                        (obj, item) => ({
                            ...obj,
                            [item.Id]: item.Name,
                        }),
                        {}
                    )
                )
            })
        }
    }, [])

    const availableMember = members =>
        members
            .reduce(
                (arr, id) =>
                    memberLevels[id] ? [...arr, memberLevels[id]] : arr,
                []
            )
            .join(', ')

    const availableDevice = devices =>
        devices.map((d, i) => (
            <>
                <FormattedMessage
                    key={d}
                    id={`PagePaymentSetting.DataTable.Cell.Device.${d}`}
                />
                {i !== devices.length - 1 && ', '}
            </>
        ))

    const allowDepositeSite = sites =>
        sites.map((s, i) => (
            <>
                <FormattedMessage
                    key={s}
                    id={`PagePaymentSetting.DataTable.Cell.AllowDepositeSite.${s}`}
                />
                {i < sites.length - 1 && ', '}
            </>
        ))

    return (
        <>
            <Descriptions column={2}>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="PagePaymentSetting.Detail.DepositType" />
                    }
                >
                    {detail.DepositTypeName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="PagePaymentSetting.Detail.BankName" />
                    }
                >
                    {detail.BankName}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="PagePaymentSetting.Detail.BankAccount" />
                    }
                >
                    {detail.BankAccount}
                </Descriptions.Item>
                <Descriptions.Item
                    label={
                        <FormattedMessage id="PagePaymentSetting.Detail.AccountName" />
                    }
                >
                    {detail.AccountName}
                </Descriptions.Item>
                <Descriptions.Item
                    span={2}
                    label={
                        <FormattedMessage id="PagePaymentSetting.Detail.RechargeSetting" />
                    }
                />
                <Descriptions.Item span={2}>
                    <Descriptions layout="vertical" bordered>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.MinPaymentAmount" />
                            }
                        >
                            {detail.MinPaymentAmount ? (
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={detail.MinPaymentAmount}
                                />
                            ) : (
                                NO_DATA
                            )}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.MaxPaymentAmount" />
                            }
                        >
                            {detail.MaxPaymentAmount ? (
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={detail.MaxPaymentAmount}
                                />
                            ) : (
                                NO_DATA
                            )}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.ThirdPartyCommissionRate" />
                            }
                        >
                            {detail.ThirdPartyCommissionRate ? (
                                <ReactIntlCurrencyWithFixedDecimal
                                    value={detail.ThirdPartyCommissionRate}
                                />
                            ) : (
                                NO_DATA
                            )}
                        </Descriptions.Item>
                    </Descriptions>
                </Descriptions.Item>
                {isView && (
                    <>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.SourceStatus" />
                            }
                        >
                            <Tag
                                color={
                                    detail.SourceStatus
                                        ? 'processing'
                                        : 'warning'
                                }
                            >
                                <FormattedMessage
                                    id={`Share.Status.${
                                        detail.SourceStatus
                                            ? 'Enable'
                                            : 'Disable'
                                    }`}
                                    defaultMessage={
                                        detail.SourceStatus ? '啟用' : '停用'
                                    }
                                />
                            </Tag>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.RandomAmount" />
                            }
                        >
                            <Tag
                                color={
                                    detail.IsRandomAmount
                                        ? 'processing'
                                        : 'warning'
                                }
                            >
                                <FormattedMessage
                                    id={`Share.Status.${
                                        detail.IsRandomAmount
                                            ? 'Active'
                                            : 'Inactive'
                                    }`}
                                />
                            </Tag>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.DisplayName" />
                            }
                        >
                            {detail.DisplayName}(
                            {
                                <FormattedMessage
                                    id={`PagePaymentSetting.Detail.Display.${
                                        detail.Display ? 'Show' : 'Hide'
                                    }`}
                                />
                            }
                            )
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.AvailableMembers" />
                            }
                        >
                            {availableMember(detail.MemberLevelId)}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.AvailableDevice" />
                            }
                        >
                            {availableDevice(detail.AvailableDevice)}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.AllowDepositeSite" />
                            }
                        >
                            {allowDepositeSite(detail.AllowDepositeSite)}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="Share.CommonKeys.Status" />
                            }
                        >
                            <Tag
                                color={
                                    detail.EnableStatus
                                        ? detail.EnableStatus === 1
                                            ? 'processing'
                                            : 'default'
                                        : 'warning'
                                }
                            >
                                <FormattedMessage
                                    id={`Share.Status.${
                                        ENABLE_STATUS[detail.EnableStatus]
                                    }`}
                                />
                            </Tag>
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="Share.CommonKeys.Remarks" />
                            }
                        >
                            {detail.Remarks}
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="Share.Fields.ModifiedTime" />
                            }
                        >
                            <DateWithFormat time={detail.ModifyTime} />
                        </Descriptions.Item>
                        <Descriptions.Item
                            label={
                                <FormattedMessage id="PagePaymentSetting.Detail.ModifyUser" />
                            }
                        >
                            {detail.ModifyUserAccount}
                        </Descriptions.Item>
                    </>
                )}
            </Descriptions>
            {children}
        </>
    )
}
