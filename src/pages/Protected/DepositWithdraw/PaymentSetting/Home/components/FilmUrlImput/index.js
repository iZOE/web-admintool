import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { Button, Input, Modal, Tooltip } from 'antd'
import { PlaySquareOutlined } from '@ant-design/icons'

export default function FilmUrlImput({ url, onChange }) {
    const [visible, setVisible] = useState(false)
    const [videoSource, setVideoSource] = useState(url)
    function handleCancel() {
        setVisible(false)
    }
    return (
        <>
            <Input
                addonAfter={
                    <Tooltip
                        title={
                            <FormattedMessage id="Share.ActionButton.Previewing" />
                        }
                    >
                        <Button
                            size="small"
                            type="link"
                            icon={<PlaySquareOutlined />}
                            disabled={
                                !videoSource ||
                                !RegExp('(https?://[^s]+)').test(videoSource)
                            }
                            onClick={() => setVisible(true)}
                        />
                    </Tooltip>
                }
                defaultValue={url}
                onBlur={event => {
                    setVideoSource(event.target.value)
                    onChange(event.target.value)
                }}
            />
            <Modal
                title={
                    <FormattedMessage id="PagePaymentSetting.Modal.Previewing.Title" />
                }
                footer={false}
                destroyOnClose={true}
                visible={visible}
                onCancel={handleCancel}
            >
                <video width="100%" controls>
                    <source src={videoSource} type="video/mp4" />
                    Your browser does not support the video tag.
                </video>
            </Modal>
        </>
    )
}
