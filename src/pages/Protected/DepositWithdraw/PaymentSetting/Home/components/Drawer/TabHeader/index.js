import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Tabs } from 'antd'
import { TAB_PANES } from '../../../../constants/tabInfo'

const { TabPane } = Tabs

export default function TabHeader({ onSelectedTab, currentTabKey }) {
    return (
        <Tabs
            defaultActiveKey={currentTabKey}
            onChange={onSelectedTab}
            tabBarStyle={{ margin: '16px 0 0' }}
        >
            {TAB_PANES.map(({ title, key }) => (
                <TabPane
                    tab={
                        <FormattedMessage
                            id={'PagePaymentSetting.Drawer.Tab.' + title}
                        />
                    }
                    key={key}
                />
            ))}
        </Tabs>
    )
}
