import styled from 'styled-components'

export const Wrapper = styled.div`
    .ant-descriptions-item {
        & > .ant-descriptions-item-content {
            width: 100%;
        }
    }
`
