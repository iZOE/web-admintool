import React, { Suspense } from 'react'
import { Skeleton } from 'antd'
import { Wrapper } from './Styled'
import CONTENT_TYPE from '../../../constants/contentType'
import PaymentDetail from '../PaymentDetail'
import DepositAmountSetting from '../DepositAmountSetting'
import PaymentTypeSetting from '../PaymentTypeSetting'

export default function Drawer({ paymentId, contentType, currentTabKey }) {
    return (
        <Wrapper>
            <Suspense fallback={<Skeleton active />}>
                {contentType === CONTENT_TYPE.PAYMENT_TYPE_SETTING && (
                    <PaymentTypeSetting />
                )}
                {contentType === CONTENT_TYPE.DEPOSIT_AMOUNT_SETTING && (
                    <DepositAmountSetting paymentId={paymentId} />
                )}
                {contentType === CONTENT_TYPE.VIEW_DETAIL && (
                    <PaymentDetail paymentId={paymentId} />
                )}
            </Suspense>
        </Wrapper>
    )
}
