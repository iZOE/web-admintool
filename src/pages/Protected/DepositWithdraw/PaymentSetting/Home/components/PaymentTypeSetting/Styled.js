import styled from 'styled-components'

export const Wrapper = styled.div`
    text-align: right;
    .ant-table-wrapper {
        text-align: initial;
        margin-bottom: 24px;
    }
`
