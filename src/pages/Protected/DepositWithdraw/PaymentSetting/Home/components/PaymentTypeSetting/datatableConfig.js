import React from 'react'
import { FormattedMessage } from 'react-intl'
import { MenuOutlined } from '@ant-design/icons'

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="PagePaymentSetting.DataTable.Title.Order"
                description="顺序"
            />
        ),
        dataIndex: 'SortOrder',
        key: 'SortOrder',
        width: 50,
    },
    {
        title: (
            <FormattedMessage
                id="PagePaymentSetting.DataTable.Title.PaymentTypeNmae"
                description="支付类型名称"
            />
        ),
        dataIndex: 'PaymentTypeNmae',
        key: 'PaymentTypeNmae',
        width: 150,
    },
    {
        title: (
            <FormattedMessage
                id="PagePaymentSetting.DataTable.Title.Tags"
                description="标签"
            />
        ),
        dataIndex: 'Tags',
        key: 'Tags',
        width: 200,
    },
    {
        title: (
            <FormattedMessage
                id="PagePaymentSetting.DataTable.Title.Device"
                description="裝置"
            />
        ),
        dataIndex: 'Device',
        key: 'Device',
        width: 100,
        render: () => {
            return (
                <>
                    <div>PC</div>
                    <div>Mobile</div>
                </>
            )
        },
    },
    {
        title: (
            <FormattedMessage
                id="PagePaymentSetting.DataTable.Title.FilmUrl"
                description="影片連結"
            />
        ),
        dataIndex: 'FilmUrl',
        key: 'FilmUrl',
        width: 350,
    },
    {
        title: (
            <FormattedMessage
                id="PagePaymentSetting.DataTable.Title.ModifyUser"
                description="修改者"
            />
        ),
        dataIndex: 'ModifyUser',
        key: 'ModifyUser',
        width: 100,
    },
    {
        title: (
            <FormattedMessage
                id="PagePaymentSetting.DataTable.Title.Sort"
                description="排序"
            />
        ),
        align: 'center',
        dataIndex: 'Sort',
        key: 'Sort',
        width: 50,
        render: () => <MenuOutlined />,
    },
]
