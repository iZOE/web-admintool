import React, { useEffect, useState, useContext } from 'react'
import { DndProvider } from 'react-dnd'
import { SaveOutlined } from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'react-intl'
import { Button, Table, Skeleton, message } from 'antd'
import HTML5Backend from 'react-dnd-html5-backend'
import update from 'immutability-helper'
import DateWithFormat from 'components/DateWithFormat'
import { UserContext } from 'contexts/UserContext'
import { COLUMNS_CONFIG } from './datatableConfig'
import DragableBodyRow from '../DragableBodyRow'
import paymentService from '../../../services'
import { Wrapper } from './Styled'
import moment from 'moment'
import { FORMAT } from 'constants/dateConfig'

const sortPaymentTypes = types =>
    types
        .map(type => ({
            ...type,
            key: type.PaymentTypeId,
            Tags: type.Tags.map(t => `T-${t}`),
        }))
        .sort((a, b) => a.SortOrder - b.SortOrder)

export default function PaymentTypeSetting() {
    const intl = useIntl()
    const { data: userData } = useContext(UserContext)
    const [allTags, setAllTags] = useState()
    const [paymentTypeTags, setPaymentTypeTags] = useState()
    const [lastModifiedTime, setLastModifiedTime] = useState()
    const [dataSource, setDataSource] = useState()
    const {
        getPaymentTypes,
        getPaymentTypeTags,
        putPaymentTypes,
    } = paymentService()

    const { UserName, UserId } = userData

    useEffect(() => {
        getPaymentTypeTags().then(res => {
            const { ALL_TAGS, TREE_DATA } = res.reduce(
                (obj, cur) => {
                    obj.ALL_TAGS.push(cur.Id)
                    obj.TREE_DATA[0].children.push({
                        title: cur.Name,
                        value: `T-${cur.Id}`,
                        key: `T-${cur.Id}`,
                    })
                    return obj
                },
                {
                    ALL_TAGS: [],
                    TREE_DATA: [
                        {
                            title: <FormattedMessage id="Share.Dropdown.All" />,
                            value: 'T',
                            key: 'T',
                            children: [],
                        },
                    ],
                }
            )
            setAllTags(ALL_TAGS)
            setPaymentTypeTags(TREE_DATA)
        })
    }, [])

    useEffect(() => {
        getPaymentTypes().then(res => {
            console.log('PaymentTypeSetting -> res', res)
            setLastModifiedTime(res.LastModifiedTime)
            setDataSource(sortPaymentTypes(res.Types))
        })
        // if (detail) {
        //   setDataSource(sortPaymentTypes(detail.types));
        // }
    }, [])

    function onMoveRow(dragIndex, hoverIndex) {
        const newSource = JSON.parse(JSON.stringify(dataSource))
        newSource[dragIndex] = Object.assign(newSource[dragIndex], {
            ModifyUser: UserName,
            ModifyUserId: UserId,
        })
        newSource[hoverIndex] = Object.assign(newSource[hoverIndex], {
            ModifyUser: UserName,
            ModifyUserId: UserId,
        })
        setDataSource(
            update(newSource, {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, newSource[dragIndex]],
                ],
            })
        )
    }

    function onChangeWebFilmUrl(url, index) {
        const dragRow = dataSource[index]
        setDataSource(
            update(dataSource, {
                $merge: {
                    [index]: {
                        ...dragRow,
                        FilmUrlWeb: url,
                        ModifyUser: UserName,
                        ModifyUserId: UserId,
                    },
                },
            })
        )
    }

    function onChangeAppFilmUrl(url, index) {
        const dragRow = dataSource[index]
        setDataSource(
            update(dataSource, {
                $merge: {
                    [index]: {
                        ...dragRow,
                        FilmUrlApp: url,
                        ModifyUser: UserName,
                        ModifyUserId: UserId,
                    },
                },
            })
        )
    }

    function onChangeTags(Tags, index) {
        const dragRow = dataSource[index]
        setDataSource(
            update(dataSource, {
                $merge: {
                    [index]: {
                        ...dragRow,
                        Tags,
                        ModifyUser: UserName,
                        ModifyUserId: UserId,
                    },
                },
            })
        )
    }

    function parsingTag(tags) {
        const isChanged = tags.some(t => RegExp('T-', 'g').test(t))
        const isAll = tags.some(t => RegExp('^T$', 'g').test(t))
        if (isChanged) {
            return tags.map(t => Number(t.replace('T-', '')))
        }
        if (isAll) {
            return allTags
        }
        return tags
    }

    function onSave() {
        putPaymentTypes({
            Types: dataSource.map((data, index) => ({
                ...data,
                SortOrder: index + 1,
                Tags: parsingTag(data.Tags),
            })),
            LastModifiedTime: moment().format(FORMAT.DEFAULT.FULL),
        }).then(() =>
            message.success(
                intl.formatMessage({
                    id: 'Share.SuccessMessage.UpdateSuccess',
                })
            )
        )
    }

    return dataSource ? (
        <Wrapper>
            <DndProvider backend={HTML5Backend}>
                <Table
                    scroll={{ y: 'calc(100vh - 250px)' }}
                    title={() => (
                        <FormattedMessage
                            id="PagePaymentSetting.Detail.SettingTable.PaymentModifyTime"
                            values={{
                                time: (
                                    <DateWithFormat time={lastModifiedTime} />
                                ),
                            }}
                        />
                    )}
                    size="small"
                    columns={COLUMNS_CONFIG}
                    dataSource={dataSource}
                    components={{
                        body: {
                            row: DragableBodyRow,
                        },
                    }}
                    onRow={(record, index) => ({
                        index,
                        paymentTypeTags,
                        onMoveRow,
                        onChangeTags,
                        onChangeWebFilmUrl,
                        onChangeAppFilmUrl,
                    })}
                    pagination={false}
                />
                <Button
                    type="primary"
                    shape="circle"
                    size="large"
                    icon={<SaveOutlined />}
                    onClick={onSave}
                />
            </DndProvider>
        </Wrapper>
    ) : (
        <Skeleton active />
    )
}
