import React, { useState, useCallback, Suspense } from 'react'
import { Skeleton, Spin } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import BasicDetail from '../BasicDetail'
import ChangeLogTab from './ChangeLogTab'
import TabHeader from '../../../components/TabHeader'
import { TAB_PANES } from '../../../constants/tabInfo'

export default function PaymentDetail({ paymentId }) {
    const [currentTabKey, setCurrentTabKey] = useState(TAB_PANES[0].key)

    const onSelectedTab = useCallback(tabIndex => {
        setCurrentTabKey(tabIndex)
    }, [])

    const { data: detail } = useSWR(
        paymentId ? `/api/PaymentSetting/${paymentId}` : null,
        url => axios(url).then(res => res && res.data.Data)
    )

    return (
        <>
            <TabHeader
                currentTabKey={currentTabKey}
                onSelectedTab={onSelectedTab}
            />
            <Suspense fallback={<Spin />}>
                {!!detail ? (
                    currentTabKey === TAB_PANES[0].key ? (
                        <BasicDetail detail={detail} isView />
                    ) : (
                        <ChangeLogTab paymentId={paymentId} />
                    )
                ) : (
                    <Skeleton active />
                )}
            </Suspense>
        </>
    )
}
