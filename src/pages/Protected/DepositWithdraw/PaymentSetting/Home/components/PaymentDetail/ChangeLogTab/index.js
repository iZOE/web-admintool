import React from 'react';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import ReportScaffold from 'components/ReportScaffold';
import DataTable from 'components/DataTable';
import Condition from './Condition';
import { COLUMNS_CONFIG } from './datatableConfig';

export default function ChangeLogTab({ paymentId }) {
  const { fetching, onUpdateCondition, condition, dataSource } = useGetDataSourceWithSWR({
    url: '/api/PaymentSetting/ExchangeRate/Log',
  });

  return (
    <ReportScaffold
      displayResult={condition}
      conditionComponent={<Condition onUpdate={onUpdateCondition.bySearch} paymentId={paymentId} />}
      conditionHasCollapseWrapper={false}
      datatableComponent={
        <DataTable
          dataSource={dataSource}
          displayResult={condition}
          loading={fetching}
          config={COLUMNS_CONFIG}
          pagination={false}
          rowKey={(_1, index) => `Log_${index}`}
        />
      }
    />
  );
}
