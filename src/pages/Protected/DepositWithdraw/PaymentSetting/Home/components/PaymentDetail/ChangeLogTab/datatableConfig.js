import React from 'react'
import { FormattedMessage } from 'react-intl'
import DateWithFormat from 'components/DateWithFormat'
import { NO_DATA } from 'constants/noData'

export const COLUMNS_CONFIG = [
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ModifiedTime"
                description="異動時間"
            />
        ),
        dataIndex: 'ModifiedOn',
        key: 'ModifiedOn',
        width: 200,
        render: (_1, { ModifiedOn: time }, _2) => (
            <DateWithFormat time={time} />
        ),
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.Modifier"
                description="異動人員"
            />
        ),
        dataIndex: 'ModifiedByName',
        key: 'ModifiedByName',
        render: name => (name ? name : NO_DATA),
    },
    {
        title: (
            <FormattedMessage
                id="Share.Fields.ChangeItem"
                description="異動項目"
            />
        ),
        dataIndex: 'Column',
        key: 'Column',
    },
    {
        title: <FormattedMessage id="Share.Fields.Detail" description="明細" />,
        dataIndex: 'Value',
        key: 'Value',
    },
]
