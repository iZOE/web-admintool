import React, { useContext } from 'react'
import { Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import { DrawerContext } from 'contexts/DrawerContext'
import Drawer from '../Drawer'
import CONTENT_TYPE from '../../../constants/contentType'

export default function OpenDetailButton({
    type = 'link',
    paymentId,
    displayText,
    contentType,
    ...restProps
}) {
    const { onOpenDrawer } = useContext(DrawerContext)

    const handleUpdateDrawer = () => {
        onOpenDrawer({
            width:
                contentType === CONTENT_TYPE.PAYMENT_TYPE_SETTING ? 980 : 720,
            title: (
                <FormattedMessage
                    id={`PagePaymentSetting.Drawer.Title.${contentType}`}
                />
            ),
            component: (
                <Drawer contentType={contentType} paymentId={paymentId} />
            ),
        })
    }

    return (
        <Button type={type} onClick={handleUpdateDrawer} {...restProps}>
            {displayText}
        </Button>
    )
}
