import React, {
    useRef,
    useMemo,
    Children,
    isValidElement,
    cloneElement,
} from 'react'
import { useDrag, useDrop } from 'react-dnd'
import PaymentTypeTagSelect from '../PaymentTypeTagSelect'
import FilmUrlInput from '../FilmUrlImput'
import { Wrapper } from './Styled'

const TYPE = 'DragableBodyRow'

export default function DragableBodyRow({
    children,
    index,
    onMoveRow,
    onChangeWebFilmUrl,
    onChangeAppFilmUrl,
    onChangeTags,
    paymentTypeTags,
    className,
    style,
    ...restProps
}) {
    const ref = useRef()

    const [, dragRef] = useDrag({
        item: { type: TYPE, index },
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
    })

    const [{ isOver, dropClassName }, dropRef] = useDrop({
        accept: TYPE,
        collect: monitor => {
            const { index: dragIndex } = monitor.getItem() || {}
            return dragIndex === index
                ? {}
                : {
                      isOver: monitor.isOver(),
                      dropClassName:
                          dragIndex < index
                              ? ' drop-over-downward'
                              : ' drop-over-upward',
                  }
        },
        drop: item => {
            if (item.index !== index) {
                onMoveRow(item.index, index)
            }
        },
    })

    dropRef(dragRef(ref))

    const childrenWithProps = useMemo(() => {
        return Children.map(children, child => {
            if (isValidElement(child)) {
                switch (child.key) {
                    case 'Tags':
                        return cloneElement(child, {
                            render: (_1, record, index) => (
                                <PaymentTypeTagSelect
                                    onChangeTags={tags => {
                                        onChangeTags(tags, index)
                                    }}
                                    treeData={paymentTypeTags}
                                    tags={record.Tags}
                                />
                            ),
                        })
                    case 'FilmUrl':
                        return cloneElement(child, {
                            render: (_1, record) => (
                                <>
                                    <FilmUrlInput
                                        url={record.FilmUrlWeb}
                                        onChange={url =>
                                            onChangeWebFilmUrl(url, index)
                                        }
                                    />
                                    <FilmUrlInput
                                        url={record.FilmUrlApp}
                                        onChange={url =>
                                            onChangeAppFilmUrl(url, index)
                                        }
                                    />
                                </>
                            ),
                        })
                    default:
                        return child
                }
            }
        })
    }, [children])

    return (
        <Wrapper
            ref={ref}
            className={`${className}${isOver ? dropClassName : ''}`}
            {...restProps}
        >
            {childrenWithProps}
        </Wrapper>
    )
}
