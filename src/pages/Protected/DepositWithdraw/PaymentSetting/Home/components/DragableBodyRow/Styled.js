import styled from 'styled-components'

export const Wrapper = styled.tr`
    &.drop-over-downward td,
    &.drop-over-downward + .ant-table-cell td {
        border-bottom: 2px dashed #1890ff;
    }
    &.drop-over-upward td {
        border-top: 2px dashed #1890ff;
    }
    .ant-table-cell {
        .ant-tree-select {
            width: 100%;
        }
    }
    .anticon-menu {
        cursor: move;
    }
`
