import React, { useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { TreeSelect } from 'antd'

const { SHOW_PARENT } = TreeSelect

export default function PaymentTypeTagSelect({ onChangeTags, treeData, tags }) {
    const [selectedTags, setSelectedTags] = useState(tags)

    const onSelectTags = value => {
        setSelectedTags(value)
        onChangeTags(value)
    }

    return (
        <TreeSelect
            treeDefaultExpandAll
            treeData={treeData}
            value={selectedTags}
            onChange={onSelectTags}
            treeCheckable
            showCheckedStrategy={SHOW_PARENT}
            maxTagCount={5}
            placeholder={
                <FormattedMessage
                    id="Share.PlaceHolder.Input.PleaseSelectSomething"
                    values={{
                        field: (
                            <FormattedMessage id="PagePaymentSetting.DataTable.Title.Tags" />
                        ),
                    }}
                />
            }
        />
    )
}
