import React, { useState, useEffect } from 'react';
import {
  Button,
  Checkbox,
  Descriptions,
  Form,
  Input,
  InputNumber,
  Modal,
  Radio,
  Skeleton,
  Space,
  Switch,
  Tag,
  TreeSelect
} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useHistory, useParams } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';
import Permission from 'components/Permission';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import { NO_DATA } from 'constants/noData';
import * as PERMISSION from 'constants/permissions';
import optionsServices from 'services/optionsServices';
import paymentService from '../services';
import { Wrapper } from './Styled';
import CurrencyExangeRateFormItem from '../components/formItem/CurrencyExchangeRate';
import WalletProtocolFormItem from '../components/formItem/WalletProtocol';

const {
  DEPOSIT_WITHDRAW_PAYMENT_SETTING_EDIT,
  DEPOSIT_WITHDRAW_PAYMENT_SETTING_EDIT_FEE
} = PERMISSION;

const { SHOW_PARENT } = TreeSelect;

const DEVICE_OPTIONS = [
  {
    title: 'Pc',
    value: 1,
    key: 1
  },
  {
    title: 'App',
    value: 2,
    key: 2
  },
  {
    title: 'H5',
    value: 4,
    key: 4
  }
];

const ENABLE_STATUS = [
  {
    value: 0,
    text: 'Share.Status.Inactive'
  },
  {
    value: 1,
    text: 'Share.Status.Active'
  },
  {
    value: 2,
    text: 'Share.Status.Pause'
  }
];

const ALLOW_DEPOSITE_SITE = [
  {
    label: (
      <FormattedMessage id="PagePaymentSetting.DataTable.Cell.AllowDepositeSite.1" />
    ),
    value: 1
  },
  {
    label: (
      <FormattedMessage id="PagePaymentSetting.DataTable.Cell.AllowDepositeSite.2" />
    ),
    value: 2
  }
];
const { confirm } = Modal;
const { useForm } = Form;

export default function Detail() {
  const history = useHistory();
  const { getPaymentSetting, updatePaymentSetting } = paymentService();
  const { getMemberLevel } = optionsServices();
  const { paymentId } = useParams();
  const [form] = useForm();
  const [data, setData] = useState(null);
  const [memberLevel, setMemberLevel] = useState(null);
  const intl = useIntl();

  const LAYOUT = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 }
  };

  useEffect(() => {
    getPaymentSetting(paymentId).then(res => setData(res));
    getMemberLevel().then(res =>
      setMemberLevel(
        res.map(opt => ({
          title: opt.Name,
          value: opt.Id,
          key: opt.Id
        }))
      )
    );
  }, []);

  const saveSuccess = res => {
    history.push('/deposit-withdraw/payment-setting');
  };
  const saveFail = error => {
    console.log('Detail -> saveFail', error);
  };

  const onFinish = values => {
    confirm({
      title: intl.formatMessage({
        id: 'Share.ActionButton.Save'
      }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage({
        id: 'Share.ConfirmTips.WhetherToSaveExistingChanges'
      }),
      okText: intl.formatMessage({
        id: 'Share.ActionButton.Save'
      }),
      cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
      onOk() {
        const {
          MemberLevelId,
          AvailableDevice,
          AllowDepositeSite,
          ...rest
        } = values;
        const payload = {
          ...rest,
          MemberLevelId: MemberLevelId.map(value => Math.pow(2, value)).reduce(
            (a, b) => a + b,
            0
          ),
          AvailableDevice: AvailableDevice.reduce((a, b) => a + b, 0),
          AllowDepositeSite: AllowDepositeSite.reduce((a, b) => a + b, 0)
        };

        updatePaymentSetting(paymentId, payload)
          .then(saveSuccess)
          .catch(saveFail);
      },
      onCancel() {
        console.log('Cancel onFinish');
      }
    });
  };

  function showResetConfirm() {
    confirm({
      title: intl.formatMessage({
        id: 'Share.CommonKeys.FillInAgain'
      }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage({
        id: 'Share.ConfirmTips.DoYouWantToReeditTheContent'
      }),
      okText: intl.formatMessage({ id: 'Share.ActionButton.Restart' }),
      okType: 'danger',
      cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
      onOk() {
        form.resetFields();
      },
      onCancel() {
        console.log('Cancel showResetConfirm');
      }
    });
  }

  function showCancelConfirm() {
    confirm({
      title: intl.formatMessage({ id: 'Share.CommonKeys.GiveUpEditing' }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage(
        {
          id: 'Share.ConfirmTips.DoYouWantToGiveUpEditingAndReturn'
        },
        {
          targetname: intl.formatMessage({
            id: 'PagePaymentSetting.PageHeader',
            description: '充值優惠列表'
          })
        }
      ),
      okText: intl.formatMessage({ id: 'Share.ActionButton.Abandon' }),
      okType: 'danger',
      cancelText: intl.formatMessage({ id: 'Share.ActionButton.Cancel' }),
      onOk() {
        history.push('/deposit-withdraw/payment-setting');
      },
      onCancel() {
        console.log('Cancel showCancelConfirm');
      }
    });
  }

  return (
    <Permission isPage functionIds={[DEPOSIT_WITHDRAW_PAYMENT_SETTING_EDIT]}>
      <Wrapper title={<FormattedMessage id="PagePaymentSetting.EditPage" />}>
        {data ? (
          <Form form={form} {...LAYOUT} onFinish={onFinish}>
            <Form.Item
              // 类型
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.DepositTypeName" />
              }
            >
              {data.DepositTypeName}
            </Form.Item>
            <Form.Item
              // 银行
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.BankName" />
              }
            >
              {data.BankName}
            </Form.Item>
            <Form.Item
              // 帐户
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.BankAccount" />
              }
            >
              {data.BankAccount}
            </Form.Item>
            <Form.Item
              // 户名
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.AccountName" />
              }
            >
              {data.AccountName}
            </Form.Item>
            <Form.Item
              // 充值設定
              className="payment-amount"
              label={
                <FormattedMessage id="PagePaymentSetting.Form.Label.RechargeSetting" />
              }
            >
              <Descriptions layout="vertical" bordered>
                <Descriptions.Item
                  label={
                    // 最小存入金額
                    <FormattedMessage id="PagePaymentSetting.Detail.MinPaymentAmount" />
                  }
                >
                  {data.MinPaymentAmount ? (
                    <ReactIntlCurrencyWithFixedDecimal
                      value={data.MinPaymentAmount}
                    />
                  ) : (
                    NO_DATA
                  )}
                </Descriptions.Item>
                <Descriptions.Item
                  // 最大存入金額
                  label={
                    <FormattedMessage id="PagePaymentSetting.Detail.MaxPaymentAmount" />
                  }
                >
                  {data.MaxPaymentAmount ? (
                    <ReactIntlCurrencyWithFixedDecimal
                      value={data.MaxPaymentAmount}
                    />
                  ) : (
                    NO_DATA
                  )}
                </Descriptions.Item>
                <Descriptions.Item
                  label={
                    // 渠道手續費(%)
                    <FormattedMessage id="PagePaymentSetting.Detail.ThirdPartyCommissionRate" />
                  }
                >
                  {data.ThirdPartyCommissionRate ? (
                    <ReactIntlCurrencyWithFixedDecimal
                      value={data.ThirdPartyCommissionRate}
                    />
                  ) : (
                    NO_DATA
                  )}
                </Descriptions.Item>
              </Descriptions>
            </Form.Item>
            <Form.Item
              // 來源狀態
              label={
                <FormattedMessage id="PagePaymentSetting.Detail.SourceStatus" />
              }
            >
              <Tag color={data.SourceStatus ? 'processing' : 'warning'}>
                <FormattedMessage
                  id={`Share.Status.${
                    data.SourceStatus ? 'Enable' : 'Disable'
                  }`}
                />
              </Tag>
            </Form.Item>
            <Form.Item
              // 渠道手續費(%)
              className="commission-rate"
              label={
                <FormattedMessage id="PagePaymentSetting.Form.Label.ThirdPartyCommissionRate" />
              }
            >
              <Permission
                functionIds={[DEPOSIT_WITHDRAW_PAYMENT_SETTING_EDIT_FEE]}
                failedRender={
                  <Form.Item
                    // 站台手續費
                    label={
                      <FormattedMessage id="PagePaymentSetting.Form.Label.CompanyCommissionRate" />
                    }
                    className="third-party-commission-rate no-permission"
                  >
                    <ReactIntlCurrencyWithFixedDecimal
                      value={data.CompanyCommissionRate}
                      // eslint-disable-next-line react/style-prop-object
                      style="unit"
                      unit="percent"
                    />
                  </Form.Item>
                }
              >
                <Form.Item
                  // 站台手續費
                  label={
                    <FormattedMessage id="PagePaymentSetting.Form.Label.CompanyCommissionRate" />
                  }
                  className="third-party-commission-rate"
                  name="CompanyCommissionRate"
                  labelAlign="left"
                  initialValue={data.CompanyCommissionRate}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                      )
                    }
                  ]}
                >
                  <InputNumber
                    min={0}
                    max={data.ThirdPartyCommissionRate}
                    step={0.1}
                    parser={value => value.replace('%', '')}
                    formatter={value => (value ? `${value}%` : value)}
                  />
                </Form.Item>
              </Permission>
              <Permission
                functionIds={[DEPOSIT_WITHDRAW_PAYMENT_SETTING_EDIT_FEE]}
                failedRender={
                  <Form.Item
                    // 會員手續費
                    label={
                      <FormattedMessage id="PagePaymentSetting.Form.Label.MemberCommissionRate" />
                    }
                    className="third-party-commission-rate no-permission"
                  >
                    <ReactIntlCurrencyWithFixedDecimal
                      value={data.MemberCommissionRate}
                      // eslint-disable-next-line react/style-prop-object
                      style="unit"
                      unit="percent"
                    />
                  </Form.Item>
                }
              >
                <Form.Item
                  // 會員手續費
                  label={
                    <FormattedMessage id="PagePaymentSetting.Form.Label.MemberCommissionRate" />
                  }
                  className="third-party-commission-rate"
                  name="MemberCommissionRate"
                  labelAlign="left"
                  initialValue={data.MemberCommissionRate}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage id="Share.FormValidate.Required.Input" />
                      )
                    }
                  ]}
                >
                  <InputNumber
                    min={0}
                    max={100}
                    step={0.1}
                    parser={value => value.replace('%', '')}
                    formatter={value => (value ? `${value}%` : value)}
                  />
                </Form.Item>
              </Permission>
            </Form.Item>
            <Form.Item
              // 金額隨機
              name="IsRandomAmount"
              initialValue={data.IsRandomAmount}
              valuePropName="checked"
              label={
                <FormattedMessage id="PagePaymentSetting.Detail.RandomAmount" />
              }
            >
              <Switch
                checkedChildren={
                  <FormattedMessage id="Share.SwitchButton.IsEnable.Yes" />
                }
                unCheckedChildren={
                  <FormattedMessage id="Share.SwitchButton.IsEnable.No" />
                }
              />
            </Form.Item>
            <CurrencyExangeRateFormItem value={data.ExchangeRate} />
            <Form.Item
              className="display-name"
              label={
                <FormattedMessage
                  id="PagePaymentSetting.Form.Label.ChannelName"
                  description="通道名稱"
                />
              }
            >
              <Form.Item name="ChannelName" initialValue={data.ChannelName}>
                <Input />
              </Form.Item>
              <Form.Item
                name="IsChannelNameVisible"
                valuePropName="checked"
                initialValue={data.IsChannelNameVisible}
              >
                <Switch
                  checkedChildren={
                    <FormattedMessage id="PagePaymentSetting.Detail.Display.Show" />
                  }
                  unCheckedChildren={
                    <FormattedMessage id="PagePaymentSetting.Detail.Display.Hide" />
                  }
                />
              </Form.Item>
            </Form.Item>
            <Form.Item
              // 顯示名稱
              className="display-name"
              label={
                <FormattedMessage id="PagePaymentSetting.Detail.DisplayName" />
              }
            >
              <Form.Item name="DisplayName" initialValue={data.DisplayName}>
                <Input />
              </Form.Item>
              <Form.Item
                name="IsDisplayNameVisible"
                valuePropName="checked"
                initialValue={data.IsDisplayNameVisible}
              >
                <Switch
                  checkedChildren={
                    <FormattedMessage id="PagePaymentSetting.Detail.Display.Show" />
                  }
                  unCheckedChildren={
                    <FormattedMessage id="PagePaymentSetting.Detail.Display.Hide" />
                  }
                />
              </Form.Item>
            </Form.Item>
            <WalletProtocolFormItem value={data.WalletProtocol} />
            <Form.Item
              // 可用會員
              name="MemberLevelId"
              initialValue={data.MemberLevelId}
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.MemberLevelName" />
              }
            >
              <TreeSelect
                treeData={memberLevel}
                treeCheckable
                showCheckedStrategy={SHOW_PARENT}
                maxTagCount={5}
              />
            </Form.Item>
            <Form.Item
              // 可用裝置
              name="AvailableDevice"
              initialValue={data.AvailableDevice}
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.AvailableDevice" />
              }
            >
              <TreeSelect
                treeData={DEVICE_OPTIONS}
                treeCheckable
                showCheckedStrategy={SHOW_PARENT}
              />
            </Form.Item>
            <Form.Item
              // 可用站台
              name="AllowDepositeSite"
              initialValue={data.AllowDepositeSite}
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.AllowDepositeSite" />
              }
            >
              <Checkbox.Group options={ALLOW_DEPOSITE_SITE} />
            </Form.Item>
            <Form.Item
              // 啟用狀態
              name="EnableStatus"
              initialValue={data.EnableStatus}
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.EnableStatus" />
              }
            >
              <Radio.Group buttonStyle="solid">
                {ENABLE_STATUS.map(status => (
                  <Radio.Button key={status.value} value={status.value}>
                    <FormattedMessage id={status.text} />
                  </Radio.Button>
                ))}
              </Radio.Group>
            </Form.Item>
            <Form.Item
              // 備註
              name="Remarks"
              initialValue={data.Remarks}
              label={
                <FormattedMessage id="PagePaymentSetting.Form.Label.Remarks" />
              }
            >
              <Input.TextArea />
            </Form.Item>
            <Form.Item
              label={
                <FormattedMessage
                  id="Share.Fields.ModifiedTime"
                  description="異動時間"
                />
              }
            >
              <DateWithFormat time={data.ModifyTime} />
            </Form.Item>
            <Form.Item
              // 管理人员
              label={
                <FormattedMessage id="PagePaymentSetting.DataTable.Title.ModifyUserAccount" />
              }
            >
              {data.ModifyUserAccount ? data.ModifyUserAccount : NO_DATA}
            </Form.Item>
            <Form.Item wrapperCol={{ ...LAYOUT.wrapperCol, offset: 8 }}>
              <Space>
                <Button type="primary" htmlType="submit">
                  <FormattedMessage id="Share.ActionButton.Confirm" />
                </Button>
                <Button htmlType="button" onClick={showResetConfirm}>
                  <FormattedMessage id="Share.ActionButton.Restart" />
                </Button>
                <Button
                  type="link"
                  htmlType="button"
                  onClick={showCancelConfirm}
                >
                  <FormattedMessage id="Share.ActionButton.Cancel" />
                </Button>
              </Space>
            </Form.Item>
          </Form>
        ) : (
          <Skeleton active />
        )}
      </Wrapper>
    </Permission>
  );
}
