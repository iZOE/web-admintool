import styled from 'styled-components'
import LayoutPageBody from 'layouts/Page/Body'

export const Wrapper = styled(LayoutPageBody)`
    .ant-form {
        .payment-amount {
            .ant-descriptions-item-label,
            .ant-descriptions-item-content {
                font-size: 12px;
            }
        }
        .commission-rate,
        .display-name {
            .ant-form-item-control-input-content {
                display: flex;
                .ant-form-item {
                    flex: 1;
                    margin-bottom: 0;
                }
            }
        }
        .display-name {
            .ant-form-item {
                &:last-child {
                    width: 100px;
                    flex: unset;
                    .ant-form-item-control-input-content {
                        display: block;
                        text-align: right;
                    }
                }
            }
        }
        .commission-rate {
            .third-party-commission-rate {
                &:not(.no-permission) {
                    display: flex;
                    flex-direction: column;
                }
                &:last-child {
                    margin-left: 8px;
                }
                &.no-permission {
                    .ant-form-item-label {
                        text-align: left;
                    }
                }
                .ant-input-number {
                    width: 100%;
                }
            }
        }
    }
`
