import React, { useState, useMemo, useEffect } from "react";
import { Form, Row, Col, Select } from "antd";
import { useIntl, FormattedMessage } from "react-intl";

const { Option } = Select;

const renderLevel1Select = (SettingTypes, isUnShiftWithOptionsAll) => {
  if (!SettingTypes) {
    return [];
  }
  const Oprions = SettingTypes.Data.map(v => (
    <Option value={v.Id} key={v.Id}>
      {v.Name}
    </Option>
  ));
  if (isUnShiftWithOptionsAll) {
    Oprions.unshift(
      <Option value={null} key={0}>
        <FormattedMessage id="Share.Dropdown.All" />
      </Option>
    );
  }
  return Oprions;
};

const renderLevel2Select = (
  SettingTypes,
  level1SelectedId,
  isUnShiftWithOptionsAll
) => {
  if (!SettingTypes) {
    return [];
  }
  const selectedLevel1Item = SettingTypes.Data.filter(
    d => d.Id === level1SelectedId
  )[0];
  if (!selectedLevel1Item || !selectedLevel1Item.SubItems) {
    return (
      <Option value={null} key={0}>
        <FormattedMessage id="Share.Dropdown.All" />
      </Option>
    );
  }

  const Options =
    selectedLevel1Item.SubItems.map(v => (
      <Option value={v.Id} key={v.Id}>
        {v.Name}
      </Option>
    )) || [];
  if (isUnShiftWithOptionsAll) {
    Options.unshift(
      <Option value={null} key={0}>
        <FormattedMessage id="Share.Dropdown.All" />
      </Option>
    );
  }

  return Options;
};

const renderLevel3Select = (
  SettingTypes,
  level1SelectedId,
  level2SelectedId,
  isUnShiftWithOptionsAll,
  formInstance,
  isChangingLevel3
) => {
  if (!SettingTypes) {
    return [];
  }
  const selectedLevel2 = SettingTypes.Data.filter(
    d => d.Id === level1SelectedId
  )[0];

  if (
    !selectedLevel2 ||
    (!selectedLevel2.SubItems && isUnShiftWithOptionsAll)
  ) {
    return null;
  }

  if (!selectedLevel2 || !selectedLevel2.SubItems) {
    return (
      <Option value={null} key={0}>
        <FormattedMessage id="Share.Dropdown.All" />
      </Option>
    );
  }

  const selectedLevel2Item = selectedLevel2.SubItems.filter(
    v => v.Id === level2SelectedId
  )[0];

  if (!selectedLevel2Item || !selectedLevel2Item.SubItems) {
    return (
      <Option value={null} key={0}>
        <FormattedMessage id="Share.Dropdown.All" />
      </Option>
    );
  }
  const Options =
    selectedLevel2Item.SubItems.map(v => {
      return (
        <Option value={v.Id} key={v.Id}>
          {v.Name}
        </Option>
      );
    }) || [];
  if (isUnShiftWithOptionsAll) {
    Options.unshift(
      <Option value={null} key={0}>
        <FormattedMessage id="Share.Dropdown.All" />
      </Option>
    );

    formInstance.setFieldsValue({
      SettingId: null
    });
  } else {
    if (!isChangingLevel3) {
      formInstance.setFieldsValue({
        SettingId:
          (selectedLevel2Item.SubItems &&
            selectedLevel2Item.SubItems.length > 0 &&
            selectedLevel2Item.SubItems[0].Id) ||
          null
      });
    }
  }

  return Options;
};

export const getNowSettingIdConstrain = (formInstance, settingTypesMap) => {
  const SettingId = formInstance.getFieldValue("SettingId");
  const settingSubTypeId = formInstance.getFieldValue("SettingSubTypeId");
  // console.log("from <FORM>", settingSubTypeId, SettingId);
  const nowChoosedSettingIData =
    settingTypesMap.get(SettingId) || settingTypesMap.get(settingSubTypeId);
  let LimitAmount, IsAudit, Id, TurnoverMultiple;
  if (nowChoosedSettingIData) {
    LimitAmount = nowChoosedSettingIData.LimitAmount;
    IsAudit = nowChoosedSettingIData.IsAudit;
    Id = nowChoosedSettingIData.Id;
    TurnoverMultiple = nowChoosedSettingIData.TurnoverMultiple;
  }
  return { LimitAmount, IsAudit, Id, TurnoverMultiple };
};

// 進行充值時  作業類型  三層下拉選單  被 ANTD Form 使用的compount component
export const useFastDepositSettingTypersSelectors = (
  settingTypesData,
  isUnShiftWithOptionsAll = true,
  formInstance,
  settingTypesMap
) => {
  //when API `/api/Option/QuickDeposit/SettingTypes` resolve, count the init value of three Select for antd Form
  const [SettingTypeId, SettingSubTypeId, SettingId] = useMemo(() => {
    if (settingTypesData && settingTypesData.Data.length > 0) {
      const firstLevelData1 = settingTypesData.Data[0];
      const secondLevelData1 = firstLevelData1.SubItems[0] || null;
      const settingId =
        (secondLevelData1 &&
          secondLevelData1.SubItems &&
          secondLevelData1.SubItems.length > 0 &&
          secondLevelData1.SubItems[0].Id) ||
        null;

      return [firstLevelData1.Id, secondLevelData1.Id, settingId];
    } else {
      return [null, null, null];
    }
  }, [settingTypesData]);

  // set first default Option in the Selectors
  useEffect(() => {
    if (settingTypesData && settingTypesData.Data.length > 0) {
      formInstance.setFieldsValue({
        SettingTypeId: SettingTypeId,
        SettingSubTypeId: SettingSubTypeId,
        SettingId: SettingId
      });
    }
  }, [SettingTypeId, SettingSubTypeId, SettingId]);

  const intl = useIntl();
  const [level1SelectedId, setLevel1SelectedId] = useState(SettingTypeId || 0);
  const [level2SelectedId, setLevel2SelectedId] = useState(
    SettingSubTypeId || 0
  );
  const [isChangingLevel3, setIsChangingLevel3] = useState(false);

  const {
    LimitAmount,
    IsAudit,
    Id,
    TurnoverMultiple
  } = getNowSettingIdConstrain(formInstance, settingTypesMap);

  return {
    selectorsUI_JSXLiterial: (
      <Row gutter={24}>
        <Col sm={10} xs={24}>
          <Form.Item
            name="SettingTypeId"
            label={`${intl.formatMessage({
              id: "PageFastDeposit.Conditions.SettingTypeId"
            })}`}
          >
            <Select
              onChange={value => {
                setLevel1SelectedId(value);
                setIsChangingLevel3(false);
              }}
            >
              {renderLevel1Select(settingTypesData, isUnShiftWithOptionsAll)}
            </Select>
          </Form.Item>
        </Col>
        <Col sm={7} xs={24}>
          <Form.Item
            name="SettingSubTypeId"
            label={`${intl.formatMessage({
              id: "PageFastDeposit.Conditions.SettingSubTypeId"
            })}`}
          >
            <Select
              onChange={value => {
                setLevel2SelectedId(value);
                setIsChangingLevel3(false);
              }}
            >
              {renderLevel2Select(
                settingTypesData,
                level1SelectedId,
                isUnShiftWithOptionsAll,
                formInstance
              )}
            </Select>
          </Form.Item>
        </Col>
        <Col sm={7} xs={24}>
          <Form.Item
            name="SettingId"
            label={`${intl.formatMessage({
              id: "PageFastDeposit.Conditions.SettingId"
            })}`}
          >
            <Select onChange={value => setIsChangingLevel3({})}>
              {renderLevel3Select(
                settingTypesData,
                level1SelectedId,
                level2SelectedId,
                isUnShiftWithOptionsAll,
                formInstance,
                isChangingLevel3
              )}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    ),
    LimitAmount,
    IsAudit,
    Id,
    TurnoverMultiple
  };
};
