import React, { createContext } from 'react';
import { Space } from 'antd';
import { FormattedMessage } from 'react-intl';
import * as PERMISSION from 'constants/permissions';
import DataTable from 'components/DataTable';
import Permission from 'components/Permission';
import ExportReportButton from 'components/ExportReportButton';
import ReportScaffold from 'components/ReportScaffold';
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR';
import Condition from './Condition';
import SummaryView from './Summary';
import { COLUMNS_CONFIG } from './datatableConfig';
import FastDepositButton from './FastDepositButton';

const {
  DEPOSIT_WITHDRAW_FAST_DEPOSIT_VIEW,
  DEPOSIT_WITHDRAW_FAST_DEPOSIT_EXPORT
} = PERMISSION;

export const PageContext = createContext();

const QUERY_API_URL = '/api/QuickDeposit/Search';

const FastDepositPage = props => {
  // 查詢參數 有三種：form表單, 分頁區, table排序區
  const {
    fetching,
    dataSource,
    boundedMutate,
    onUpdateCondition,
    onReady,
    condition
  } = useGetDataSourceWithSWR({
    url: QUERY_API_URL,
    defaultSortKey: 'CreateTime',
    autoFetch: true
  });

  return (
    <PageContext.Provider value={{ dataSource, boundedMutate }}>
      <Permission isPage functionIds={[DEPOSIT_WITHDRAW_FAST_DEPOSIT_VIEW]}>
        <ReportScaffold
          displayResult={condition}
          conditionComponent={
            <Condition onReady={onReady} onUpdate={onUpdateCondition} />
          }
          summaryComponent={
            <SummaryView
              summary={dataSource && dataSource.Summary}
              totalCount={dataSource && dataSource.List.TotalCount}
            />
          }
          datatableComponent={
            <DataTable
              displayResult={condition}
              condition={condition}
              loading={fetching}
              title={<FormattedMessage id="Share.Table.SearchResult" />}
              config={COLUMNS_CONFIG}
              dataSource={dataSource && dataSource.List.Data}
              total={dataSource && dataSource.List.TotalCount}
              rowKey={record => record.DepositFastId}
              extendArea={
                <Permission
                  functionIds={[DEPOSIT_WITHDRAW_FAST_DEPOSIT_EXPORT]}
                >
                  <Space>
                    <FastDepositButton />
                    <ExportReportButton
                      condition={condition}
                      actionUrl="/api/QuickDeposit/Export"
                    />
                  </Space>
                </Permission>
              }
              onUpdate={onUpdateCondition}
            />
          }
        />
      </Permission>
    </PageContext.Provider>
  );
};

export default FastDepositPage;
