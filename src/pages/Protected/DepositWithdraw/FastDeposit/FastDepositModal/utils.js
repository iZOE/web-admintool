import React from 'react';
import { getNowSettingIdConstrain } from '../FastDepositSettingTypesSelectors';
import { FormattedMessage } from 'react-intl';

export const getHandleValueChange = (
  formInstance,
  settingTypesMap,
  intl,
  setMemberNameValidate,
  debouncedCheckIsMemberExist,
  setAmountValidate
) => updatingField => {
  const { LimitAmount } = getNowSettingIdConstrain(
    formInstance,
    settingTypesMap
  );
  const dAmount = Number(
    updatingField['Amount'] || formInstance.getFieldValue('Amount')
  );

  if (updatingField['MemberNames'] && debouncedCheckIsMemberExist) {
    const memberNameValue = updatingField['MemberNames'];
    debouncedCheckIsMemberExist(memberNameValue, intl, setMemberNameValidate);
  }
  if (!dAmount && LimitAmount) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'warning',
      help: (
        <FormattedMessage id="PageFastDeposit.Validation.DepositAmountIsEssential" />
      )
    }));
  }

  if (!dAmount && !LimitAmount) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'warning',
      help: (
        <FormattedMessage id="PageFastDeposit.Validation.NoDepositLimitAmount" />
      )
    }));
  }

  if (!LimitAmount && dAmount > 0) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'success',
      help: ''
    }));
  }
  if (LimitAmount && dAmount <= LimitAmount && dAmount > 0) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'success',
      help: ''
    }));
  }
  if (LimitAmount && dAmount > LimitAmount && dAmount > 0) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'error',
      help: (
        <FormattedMessage
          id="PageFastDeposit.Validation.DepositAmountExceed"
          values={{ limit: LimitAmount }}
        />
      )
    }));
  }
};

export const getHandleValueChangeTab2 = (
  formInstance,
  settingTypesMap,
  setMemberNameValidate,
  debouncedCheckEnterHowManyMemberName,
  setAmountValidate,
  setEnteringHowManyMemberName
) => updatingField => {
  const { LimitAmount } = getNowSettingIdConstrain(
    formInstance,
    settingTypesMap
  );

  const dAmount = Number(
    updatingField['Amount'] || formInstance.getFieldValue('Amount')
  );

  if (updatingField['MemberNames']) {
    const memberNameValue = updatingField['MemberNames'];
    debouncedCheckEnterHowManyMemberName(
      memberNameValue,
      setMemberNameValidate,
      setEnteringHowManyMemberName
    );
  }

  if (!dAmount) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'warning',
      help: (
        <FormattedMessage id="PageFastDeposit.Validation.NoDepositLimitAmount" />
      )
    }));
  }

  if (!LimitAmount && dAmount) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'success',
      help: ''
    }));
  }
  if (LimitAmount && dAmount <= LimitAmount && dAmount > 0) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'success',
      help: ''
    }));
  }
  if (LimitAmount && dAmount > LimitAmount) {
    setAmountValidate(prev => ({
      ...prev,
      status: 'error',
      help: (
        <FormattedMessage
          id="PageFastDeposit.Validation.DepositAmountExceed"
          values={{ limit: LimitAmount }}
        />
      )
    }));
  }
};
