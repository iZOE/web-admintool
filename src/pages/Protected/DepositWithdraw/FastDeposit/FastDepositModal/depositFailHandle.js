import { message } from 'antd';

function DepositFailHeadle({
  FailMemberAndReasons,
  SuccessCount,
  MembersLength,
  intl
}) {
  message.warning(
    intl.formatMessage(
      {
        id: 'PageFastDeposit.Message.Fail.OperationFailed'
      },
      {
        count: MembersLength - SuccessCount
      }
    )
  );
  const errorMessages = Object.keys(FailMemberAndReasons).reduce((obj, key) => {
    if (!obj[FailMemberAndReasons[key]]) {
      obj[FailMemberAndReasons[key]] = [];
    }
    obj[FailMemberAndReasons[key]].push(key);
    return obj;
  }, {});
  Object.keys(errorMessages).forEach(key => {
    message.error(
      intl.formatMessage(
        {
          id: 'PageFastDeposit.Message.Fail.FailedMemberAndReasons'
        },
        {
          reasons: key,
          members: errorMessages[key].join(', ')
        }
      )
    );
  });
}
export default DepositFailHeadle;
