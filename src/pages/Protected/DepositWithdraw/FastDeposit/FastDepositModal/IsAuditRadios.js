import React from "react";
import { Radio, Space } from "antd";
import { FormattedMessage, useIntl } from "react-intl";

const IsAuditRadios = ({ isAudit }) => {
  const intl = useIntl();
  return (
    <Space>
      <span>
        {intl.formatMessage({
          id: "PageFastDeposit.DepositModal.IsAudit"
        })}
        :
      </span>
      <Radio checked={Boolean(isAudit)} disabled={true}>
        <FormattedMessage id="Share.FormItem.Yes" />
      </Radio>
      <Radio checked={!Boolean(isAudit)} disabled={true}>
        <FormattedMessage id="Share.FormItem.No" />
      </Radio>
    </Space>
  );
};

export default IsAuditRadios;
