import React, { useMemo } from "react";
import { Modal, Tabs, Skeleton } from "antd";
import { FormattedMessage, useIntl } from "react-intl";
import Tab1OneRecordDeposit from "./Tab1OneRecordDeposit";
import Tab2MultiRecordDeposit from "./Tab2MultiRecordDeposit";
import Tab3ManualDeposit from "./Tab3ManualDeposit";
import useRequest from "hooks/useRequest";
import usePermission from "hooks/usePermission";
import {
  DEPOSIT_WITHDRAW_FAST_DEPOSIT_SINGLE_TASK,
  DEPOSIT_WITHDRAW_FAST_DEPOSIT_BATCH_TASK,
  DEPOSIT_WITHDRAW_FAST_DEPOSIT_QUICK_DEPOSIT,
} from "constants/permissions";

const { TabPane } = Tabs;

const SELECT_QUERY_API_URL = "/api/Option/QuickDeposit/SettingTypes";

export default function BackStageDepositModal({
  visible,
  onOk,
  manualSettingTypesData,
}) {
  // 取得 轉賬方 下拉選單裡的資料
  const { data: SettingTypes } = useRequest({
    url: SELECT_QUERY_API_URL,
  });

  const [
    depositWithdrawSingleTask,
    depositWithdrawBatchTask,
    depositWithdrawQuickDeposit,
  ] = usePermission(
    DEPOSIT_WITHDRAW_FAST_DEPOSIT_SINGLE_TASK,
    DEPOSIT_WITHDRAW_FAST_DEPOSIT_BATCH_TASK,
    DEPOSIT_WITHDRAW_FAST_DEPOSIT_QUICK_DEPOSIT
  );

  // 把第二層 第三層攤平成ES6 Map, key是 Id, 以方便用antd的Form裡的Id 找出現在選的項目的IsAudit, LimitAmount
  const settingTypesMap = useMemo(() => {
    const settingTypesMap = new Map();
    if (SettingTypes && SettingTypes.Data) {
      const _2DSettingSubType = SettingTypes.Data.map(d => d.SubItems); // 先在第二層攤平，這會變一個2D array
      _2DSettingSubType.forEach(d => {
        d.forEach(v => {
          settingTypesMap.set(v.Id, v);
          if (v.SubItems) {
            v.SubItems.forEach(s => {
              settingTypesMap.set(s.Id, s);
            });
          }
        });
      });
      return settingTypesMap;
    }
    return null;
  }, [SettingTypes]);
  const intl = useIntl();

  return (
    <Modal
      title={
        <FormattedMessage
          id="PageFastDeposit.DepositModal.Title"
          description="快速充值作業"
        />
      }
      visible={visible}
      width={1000}
      onOk={onOk}
      onCancel={onOk}
      destroyOnClose
      footer={null}
    >
      {" "}
      {settingTypesMap ? (
        <Tabs>
          {depositWithdrawSingleTask && (
            <TabPane
              tab={intl.formatMessage({
                id: "PageFastDeposit.DepositModal.Tab1Title",
              })}
              key="1"
            >
              <Tab1OneRecordDeposit
                closeModal={onOk}
                settingTypesData={SettingTypes && SettingTypes}
                settingTypesMap={settingTypesMap}
              />
            </TabPane>
          )}
          {depositWithdrawBatchTask && (
            <TabPane
              tab={intl.formatMessage({
                id: "PageFastDeposit.DepositModal.Tab2Title",
              })}
              key="2"
            >
              <Tab2MultiRecordDeposit
                closeModal={onOk}
                settingTypesData={SettingTypes && SettingTypes}
                settingTypesMap={settingTypesMap}
              />
            </TabPane>
          )}{" "}
          {depositWithdrawQuickDeposit && (
            <TabPane
              tab={intl.formatMessage({
                id: "PageFastDeposit.DepositModal.Tab3Title",
              })}
              key="3"
            >
              <Tab3ManualDeposit
                closeModal={onOk}
                settingTypesData={manualSettingTypesData}
                settingTypesMap={settingTypesMap}
              />
            </TabPane>
          )}
        </Tabs>
      ) : (
        <Skeleton />
      )}
    </Modal>
  );
}
