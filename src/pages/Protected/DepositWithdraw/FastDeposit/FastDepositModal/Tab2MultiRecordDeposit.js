import React, { useState, useCallback, useContext, useEffect } from 'react'
import { Button, Form, message, Row, Col, Input, Divider, Space } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import axios from 'axios'
import _debounce from 'lodash/debounce'
import { useFastDepositSettingTypersSelectors } from '../FastDepositSettingTypesSelectors'
import IsAuditRadios from './IsAuditRadios'
import { getHandleValueChangeTab2 } from './utils'
import { PageContext } from '../index'
import DepositFailHandle from './depositFailHandle'

const { TextArea } = Input

const initialConditions = {
    MemberNames: [],
    SettingTypeId: null,
    SettingSubTypeId: null,
    SettingId: null,
    Amount: null, // Number, if has data
    Remarks: '',
}

const checkEnterHowManyMemberName = enteringMemberName => {
    const members = enteringMemberName.split(',')
    if (members[members.length - 1] === '') {
        return members.length - 1
    }
    return members.length
}

const checkEnterHowManyMemberNameAndHandleValidation = (
    enteringMemberName,
    setMemberNameValidate,
    setEnteringHowManyMemberName,
) => {
    const enteringHowMany = checkEnterHowManyMemberName(enteringMemberName)
    if (enteringHowMany > 0) {
        setMemberNameValidate(prev => ({
            ...prev,
            status: 'success',
            help: '',
        }))
        setEnteringHowManyMemberName(enteringHowMany)
    } else {
        setMemberNameValidate(prev => ({
            ...prev,
            help: (
                <FormattedMessage
                    id="Share.FormValidate.Required.InputWithValue"
                    values={{
                        name: (
                            <FormattedMessage id="PageFastDeposit.DepositModal.MemberName" />
                        ),
                    }}
                />
            ),
            status: 'error',
        }))
        setEnteringHowManyMemberName(0)
    }
}

const debouncedCheckEnterHowManyMemberName = _debounce(
    checkEnterHowManyMemberNameAndHandleValidation,
    800,
)

export default function Tab2MultiRecordDeposit({
    closeModal,
    settingTypesData,
    settingTypesMap,
}) {
    const { boundedMutate } = useContext(PageContext)
    const intl = useIntl()
    const [formInstance] = Form.useForm()
    const {
        selectorsUI_JSXLiterial,
        LimitAmount,
        IsAudit,
        TurnoverMultiple,
    } = useFastDepositSettingTypersSelectors(
        settingTypesData,
        false,
        formInstance,
        settingTypesMap,
    )
    const [isSubmitAble, setIsSubmitAble] = useState(false)
    const [memberNameValidate, setMemberNameValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'PageFastDeposit.PlaceHolder.MemberNames',
        }),
        help: (
            <FormattedMessage
                id="Share.FormValidate.Required.InputWithValue"
                values={{
                    name: (
                        <FormattedMessage id="PageFastDeposit.DepositModal.MemberName" />
                    ),
                }}
            />
        ),
        status: 'error',
    })
    const [amountValidate, setAmountValidate] = useState({
        placeholder: intl.formatMessage({
            id: 'PageFastDeposit.PlaceHolder.Amount',
        }),
        help: (
            <FormattedMessage
                id="PageFastDeposit.Validation.DepositAmount"
                values={{ limit: LimitAmount }}
            />
        ),
        status: 'warning',
    })
    const [enteringHowManyMemberName, setEnteringHowManyMemberName] = useState(
        0,
    )

    useEffect(() => {
        setIsSubmitAble(!memberNameValidate.help && !amountValidate.help)
    }, [memberNameValidate.help, amountValidate.help])

    const submitBackStageDepositRequest = useCallback(
        parameter => {
            const url = '/api/QuickDeposit/'
            axios.put(url, parameter).then(res => {
                const { FailMemberAndReasons, SuccessCount } = res.data.Data
                if (parameter.MemberNames.length !== SuccessCount) {
                    DepositFailHandle({
                        FailMemberAndReasons,
                        SuccessCount,
                        MembersLength: parameter.MemberNames.length,
                        intl,
                    })
                } else {
                    message.success(
                        intl.formatMessage({
                            id: 'PageFastDeposit.Message.Success.FastDeposit',
                        }),
                    )
                    closeModal()
                    boundedMutate()
                }
            })
        },
        [boundedMutate],
    )

    // 按下發起 後台充值 時
    const onSubmitClick = values => {
        const { MemberNames, SettingId, Amount, Remarks } = values
        console.log('values', values)
        const members = MemberNames.split(',')
        const filteredMemberNames = members.filter(Boolean) //去除空字串
        submitBackStageDepositRequest({
            MemberNames: filteredMemberNames,
            SettingId,
            Amount: Number(Amount),
            Remarks,
        })
        setIsSubmitAble(false)
    }

    const handleValusChanges = getHandleValueChangeTab2(
        formInstance,
        settingTypesMap,
        setMemberNameValidate,
        debouncedCheckEnterHowManyMemberName,
        setAmountValidate,
        setEnteringHowManyMemberName,
    )

    return (
        <Form
            labelAlign="left"
            form={formInstance}
            labelCol={{
                xs: { span: 24 },
                sm: { span: 4 },
            }}
            wrapperCol={{ xs: { span: 24 }, sm: { span: 20 } }}
            onFinish={onSubmitClick}
            initialValues={initialConditions}
            onValuesChange={handleValusChanges}
        >
            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.MemberNames',
                        })}：`}
                        name="MemberNames"
                        hasFeedback
                        help={memberNameValidate.help}
                        validateStatus={memberNameValidate.status}
                    >
                        <TextArea
                            placeholder={memberNameValidate.placeholder}
                        />
                    </Form.Item>
                </Col>
            </Row>
            <Row justify="end">
                <FormattedMessage
                    id="PageFastDeposit.DepositModal.EnteringHowManyMemberName"
                    values={{ count: enteringHowManyMemberName }}
                />
            </Row>

            {selectorsUI_JSXLiterial}

            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.Amount',
                        })}：`}
                        name="Amount"
                        hasFeedback
                        help={amountValidate.help}
                        validateStatus={amountValidate.status}
                    >
                        <Input type="number" />
                    </Form.Item>
                </Col>
            </Row>

            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <IsAuditRadios isAudit={IsAudit} />
                </Col>
            </Row>
            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.AuditTurnover',
                        })}：`}
                        shouldUpdate
                    >
                        {() => {
                            const turnOver =
                                TurnoverMultiple &&
                                formInstance.getFieldValue('Amount') *
                                    TurnoverMultiple
                            return (
                                <span>
                                    {(TurnoverMultiple && (
                                        <ReactIntlCurrencyWithFixedDecimal
                                            value={turnOver}
                                        />
                                    )) ||
                                        '-'}
                                </span>
                            )
                        }}
                    </Form.Item>
                </Col>
            </Row>

            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.Remarks',
                        })}：`}
                        name="Remarks"
                    >
                        <TextArea />
                    </Form.Item>
                </Col>
            </Row>
            <Divider />
            <Space align="start">
                <Button
                    type="primary"
                    htmlType="submit"
                    disabled={!isSubmitAble}
                >
                    <FormattedMessage id="Share.ActionButton.Submit2" />
                </Button>
                <Button onClick={closeModal}>
                    <FormattedMessage id="Share.ActionButton.Cancel" />
                </Button>
            </Space>
        </Form>
    )
}
