import React, { useState } from "react";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";
import useSWR from "swr";
import { MoneyCollectOutlined } from "@ant-design/icons";
import axios from "axios";
import FastDepositModal from "../FastDepositModal";

function FastDepositButton() {
  const { data: manualSettingTypesData } = useSWR(
    "/api/Option/QuickDeposit/SettingTypes?showAll=true&for=manualSettingTypesData",
    url =>
      axios(url).then(res => {
        return {
          Data: res.data.Data.filter(d => d.Id === 4)
        };
      })
  );

  const [isFastDepositModalOpen, setIsFastDepositModalOpen] = useState(false);

  return (
    <>
      <FastDepositModal
        manualSettingTypesData={manualSettingTypesData}
        visible={isFastDepositModalOpen}
        onOk={() => setIsFastDepositModalOpen(false)}
      />
      <Button
        type="primary"
        onClick={() => setIsFastDepositModalOpen(true)}
        icon={<MoneyCollectOutlined />}
      >
        <FormattedMessage
          id="PageFastDeposit.PageHeader"
          description="快速充值"
        />
      </Button>
    </>
  );
}

FastDepositButton.propTypes = {};

export default FastDepositButton;
