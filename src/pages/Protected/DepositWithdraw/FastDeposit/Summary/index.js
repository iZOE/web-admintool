import React from "react";
import { Statistic, Row, Col, Skeleton } from "antd";
import { FormattedMessage } from "react-intl";

const SummaryView = ({ summary, totalCount }) => {
  if (!summary) {
    return <Skeleton active />;
  }

  const {
    NumberOfMembers,
    /**存款金額 */
    Amount,
    /**扣款金額 */
    GiveawayAmount,
    /**優惠金額 */
    PreferentialAmount
  } = summary;
  return (
    <Row gutter={24}>
      <Col span={4}>
        <Statistic
          title={
            <FormattedMessage
              id="PageFastDeposit.Summary.TotalCount"
              description="总作业笔数"
            />
          }
          value={totalCount || 0}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={
            <FormattedMessage
              id="PageFastDeposit.Summary.NumberOfMembers"
              description="充值人数"
            />
          }
          value={NumberOfMembers || 0}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={
            <FormattedMessage
              id="PageFastDeposit.Summary.Amount"
              description="存款金额"
            />
          }
          value={Amount || 0}
          precision={2}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={
            <FormattedMessage
              id="PageFastDeposit.Summary.GiveawayAmount"
              description="扣款金额"
            />
          }
          value={GiveawayAmount || 0}
          precision={2}
        />
      </Col>
      <Col span={4}>
        <Statistic
          title={
            <FormattedMessage
              id="PageFastDeposit.Summary.PreferentialAmount"
              description="优惠金额"
            />
          }
          value={PreferentialAmount || 0}
          precision={2}
        />
      </Col>
    </Row>
  );
};

export default SummaryView;
