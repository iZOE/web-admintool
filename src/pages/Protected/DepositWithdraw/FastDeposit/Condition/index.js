import React, { useContext, useEffect } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Form, Row, Col, Select, DatePicker } from 'antd'
import useSWR from 'swr'
import axios from 'axios'
import { ConditionContext } from 'contexts/ConditionContext'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import AdvancedSearchItem from 'components/FormItems/AdvancedSearchItem'
import IncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import MemberLevel from 'components/FormItems/MemberLevel'
import QueryButton from 'components/FormActionButtons/Query'
import { getISODateTimeString } from 'mixins/dateTime'
import { normalizedData } from './settingSchema'

const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const CONDITION_INITIAL_VALUE = {
    UpdateDate: RANGE.FROM_TODAY_TO_TODAY,
    SettingTypeId: null,
    SettingSubTypeId: null,
    SettingId: null,
    AdvancedSearchConditionItem: 1,
}
const ADVANCE_SEARCH_CONDITION_ITEMS = [1, 2, 3, 4, 5]
const ADVANCE_ITEM_WITH_AMOUNT = new Set([4])

const Condition = ({ onReady, onUpdate }) => {
    const { isReady } = useContext(ConditionContext)
    const intl = useIntl()
    const [form] = Form.useForm()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ UpdateDate, SettingSubTypeId, ...restValues }) {
        const [StartDate, EndDate] = UpdateDate || [null, null]
        if (SettingSubTypeId) {
            // 因為SettingSubTypeId在DB回傳的不是唯一值
            // 所以在資料讀進來時，SettingSubTypeId會變成 1.1 (ParentId.ThisId)
            // 最後再回api搜尋時，必須做split，只取dot後的數字，再回傳給後端
            SettingSubTypeId = Number(SettingSubTypeId.split('.')[0])
        }
        return {
            ...restValues,
            SettingSubTypeId,
            StartDate: getISODateTimeString(StartDate),
            EndDate: getISODateTimeString(EndDate),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    // 取得 轉賬方 下拉選單裡的資料
    const { data: settingTypeData } = useSWR(
        '/api/Option/QuickDeposit/SettingTypes?showAll=true',
        url =>
            axios(url).then(res => {
                const data = res.data.Data.map(({ Id, SubItems, ...rest }) => ({
                    ...rest,
                    Id,
                    SubItems: SubItems.map(subitem => ({
                        ...subitem,
                        CId: `${Id}.${subitem.Id}`,
                    })),
                }))
                return normalizedData(data)
            })
    )

    return (
        <Form
            onFinish={onFinish}
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.Conditions.CreateTime',
                        })}：`}
                        name="UpdateDate"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    {settingTypeData && (
                        <Form.Item
                            name="SettingTypeId"
                            label={`${intl.formatMessage({
                                id: 'PageFastDeposit.Conditions.SettingTypeId',
                            })}`}
                        >
                            <Select
                                onChange={() => {
                                    form.setFieldsValue({
                                        SettingSubTypeId: null,
                                        SettingId: null,
                                    })
                                }}
                            >
                                <Option value={null}>
                                    <FormattedMessage
                                        id="Share.Dropdown.All"
                                        description="全部"
                                    />
                                </Option>
                                {settingTypeData.result.map(typeId => (
                                    <Option value={typeId} key={typeId}>
                                        {
                                            settingTypeData.entities
                                                .settingTypes[typeId].Name
                                        }
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                    )}
                </Col>
                <Col sm={6}>
                    {settingTypeData && (
                        <Form.Item shouldUpdate noStyle>
                            {({ getFieldValue }) => (
                                <Form.Item
                                    name="SettingSubTypeId"
                                    label={`${intl.formatMessage({
                                        id:
                                            'PageFastDeposit.Conditions.SettingSubTypeId',
                                    })}`}
                                >
                                    <Select
                                        onChange={() => {
                                            form.setFieldsValue({
                                                SettingId: null,
                                            })
                                        }}
                                    >
                                        <Option value={null}>
                                            <FormattedMessage
                                                id="Share.Dropdown.All"
                                                description="全部"
                                            />
                                        </Option>
                                        {getFieldValue('SettingTypeId') &&
                                            settingTypeData.entities.settingTypes[
                                                getFieldValue('SettingTypeId')
                                            ].SubItems.map(id => (
                                                <Option value={id} key={id}>
                                                    {
                                                        settingTypeData.entities
                                                            .settingSubTypes[id]
                                                            .Name
                                                    }
                                                </Option>
                                            ))}
                                    </Select>
                                </Form.Item>
                            )}
                        </Form.Item>
                    )}
                </Col>
                <Col sm={6}>
                    {settingTypeData && (
                        <Form.Item shouldUpdate noStyle>
                            {({ getFieldValue }) => (
                                <Form.Item
                                    name="SettingId"
                                    label={`${intl.formatMessage({
                                        id:
                                            'PageFastDeposit.Conditions.SettingId',
                                    })}`}
                                >
                                    <Select>
                                        <Option value={null}>
                                            <FormattedMessage
                                                id="Share.Dropdown.All"
                                                description="全部"
                                            />
                                        </Option>
                                        {getFieldValue('SettingSubTypeId') &&
                                            settingTypeData.entities.settingSubTypes[
                                                getFieldValue(
                                                    'SettingSubTypeId'
                                                )
                                            ].SubItems.map(id => (
                                                <Option value={id} key={id}>
                                                    {
                                                        settingTypeData.entities
                                                            .settings[id].Name
                                                    }
                                                </Option>
                                            ))}
                                    </Select>
                                </Form.Item>
                            )}
                        </Form.Item>
                    )}
                </Col>
                <Col sm={8}>
                    <MemberLevel checkAll />
                </Col>
                <Col sm={10}>
                    <AdvancedSearchItem
                        OptionItems={ADVANCE_SEARCH_CONDITION_ITEMS}
                        MessageId="PageFastDeposit.Conditions.SelectItem.AdvancedSearchConditionItemOptions"
                        AmountSet={ADVANCE_ITEM_WITH_AMOUNT}
                    />
                </Col>
                <Col sm={4}>
                    <IncludeInnerMember />
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
