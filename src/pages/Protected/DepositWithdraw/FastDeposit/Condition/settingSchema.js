import { normalize, schema } from "normalizr";

// Define a users schema
const setting = new schema.Entity("settings", {}, { idAttribute: "Id" });

// Define your comments schema
const settingSubType = new schema.Entity(
  "settingSubTypes",
  {
    SubItems: [setting],
  },
  { idAttribute: "CId" }
);

// Define your article
const settingType = new schema.Entity(
  "settingTypes",
  {
    SubItems: [settingSubType],
  },
  { idAttribute: "Id" }
);

export const normalizedData = (originalData) =>
  normalize(originalData, [settingType]);
