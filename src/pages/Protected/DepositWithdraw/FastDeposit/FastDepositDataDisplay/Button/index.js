import React, { useState } from "react";
import { Button } from "antd";
import FastDepositDisplayModal from "../Modal";

function FastDepositDataDisplayButton({ fastDepositData, text }) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <>
      <FastDepositDisplayModal
        fastDepositId={text}
        fastDepositData={fastDepositData}
        visible={isModalOpen}
        onOk={() => setIsModalOpen(false)}
      />
      <Button type="link" size="small" onClick={() => setIsModalOpen(true)}>
        {text}
      </Button>
    </>
  );
}

export default FastDepositDataDisplayButton;
