import React from 'react'
import { Button, Form, Row, Col, Divider, Space, Skeleton } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import DateWithFormat from 'components/DateWithFormat'
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal'
import IsAuditRadios from '../../FastDepositModal/IsAuditRadios'

export default function FastDepositDataContent({
    visible,
    fastDepositId,
    fastDepositData,
    closeModal,
}) {
    const intl = useIntl()

    const { data: detailData } = useSWR(
        visible ? `/api/QuickDeposit/${fastDepositId}` : null,
        url => axios(url).then(res => res.data.Data)
    )

    if (!detailData) {
        return <Skeleton />
    }

    const { TypeName, SubTypeName, ItemName } = fastDepositData

    const {
        DepositFastId,
        MemberName,
        Amount,
        Audit: IsAudit,
        TurnoverVerificationAmount: TurnOver,
        Remarks,
        ModifyTime,
        ModifyUserAccount,
    } = detailData

    return (
        <Form
            labelAlign="left"
            labelCol={{
                xs: { span: 24 },
                sm: { span: 4 },
            }}
            wrapperCol={{ xs: { span: 24 }, sm: { span: 20 } }}
        >
            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DataTable.DepositFastId',
                        })}：`}
                    >
                        {DepositFastId}
                    </Form.Item>
                </Col>
            </Row>

            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.MemberNames',
                        })}：`}
                    >
                        {MemberName}
                    </Form.Item>
                </Col>
            </Row>

            <Row gutter={24}>
                <Col sm={10} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.Conditions.SettingTypeId',
                        })}`}
                    >
                        {TypeName}
                    </Form.Item>
                </Col>
                <Col sm={7} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.Conditions.SettingSubTypeId',
                        })}`}
                    >
                        {' '}
                        :{SubTypeName}
                    </Form.Item>
                </Col>
                <Col sm={7} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.Conditions.SettingId',
                        })}`}
                    >
                        {ItemName}
                    </Form.Item>
                </Col>
            </Row>

            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.Amount',
                        })}：`}
                    >
                        <ReactIntlCurrencyWithFixedDecimal value={Amount} />
                    </Form.Item>
                </Col>
            </Row>

            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <IsAuditRadios isAudit={IsAudit} />
                </Col>
            </Row>
            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.AuditTurnover',
                        })}：`}
                    >
                        {TurnOver ? (
                            <ReactIntlCurrencyWithFixedDecimal
                                value={TurnOver}
                            />
                        ) : (
                            '-'
                        )}
                    </Form.Item>
                </Col>
            </Row>

            <Row gutter={24}>
                <Col sm={24} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DepositModal.Remarks',
                        })}：`}
                    >
                        {Remarks}
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={24}>
                <Col sm={12} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'Share.Fields.ModifiedTime',
                        })}：`}
                    >
                        <DateWithFormat time={ModifyTime} />
                    </Form.Item>
                </Col>
                <Col sm={12} xs={24}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageFastDeposit.DataTable.ModifyUserAccount',
                        })}：`}
                    >
                        {ModifyUserAccount}
                    </Form.Item>
                </Col>
            </Row>
            <Divider />
            <Space align="start">
                <Button onClick={closeModal}>
                    <FormattedMessage id="Share.ActionButton.Close" />
                </Button>
            </Space>
        </Form>
    )
}
