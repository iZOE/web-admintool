import React from "react";
import { Modal } from "antd";
import { FormattedMessage } from "react-intl";
import DetailContent from "./content";

export default function FastDepositDataDisplayModal({
  visible,
  onOk,
  fastDepositId,
  fastDepositData
}) {
  return (
    <Modal
      title={
        <FormattedMessage
          id="PageFastDeposit.DepositModal.DetailPageTitle"
          description="快速充值明細"
        />
      }
      visible={visible}
      width={1000}
      onOk={onOk}
      onCancel={onOk}
      footer={null}
    >
      <DetailContent
        visible={visible}
        closeModal={onOk}
        fastDepositId={fastDepositId}
        fastDepositData={fastDepositData}
      />
    </Modal>
  );
}
