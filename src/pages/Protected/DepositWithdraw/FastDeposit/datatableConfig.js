import React from 'react';
import { FormattedMessage } from 'react-intl';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import FastDepositDataDisplayButton from './FastDepositDataDisplay/Button';
import { NO_DATA } from 'constants/noData';

const onFalsyValueRenderDash = value => value || NO_DATA;

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.DepositFastId" description="作業序號" />,
    dataIndex: 'DepositFastId',
    key: 'DepositFastId',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => <FastDepositDataDisplayButton fastDepositData={record} text={text} />,
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.CreateTime" description="作業時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, _1, _2) => {
      return <OpenMemberDetailButton memberId={text} memberName={text} />;
    },
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.NickName" description="名稱" />,
    dataIndex: 'NickName',
    key: 'NickName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.ParentMemberName" description="所屬上級" />,
    dataIndex: 'ParentMemberName',
    key: 'ParentMemberName',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: text => {
      return text ? <OpenMemberDetailButton memberId={text} memberName={text} /> : NO_DATA;
    },
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.TypeName" description="類型" />,
    dataIndex: 'TypeName',
    key: 'TypeName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => text,
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.SubTypeName" description="子類型名稱" />,
    dataIndex: 'SubTypeName',
    key: 'SubTypeName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.ItemName" description="項目名稱" />,
    dataIndex: 'ItemName',
    key: 'ItemName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: onFalsyValueRenderDash,
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.Amount" description="異動金額" />,
    dataIndex: 'Amount',
    key: 'Amount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.Audit" description="流水審核" />,
    dataIndex: 'Audit',
    key: 'Audit',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => {
      return <FormattedMessage id={`PageFastDeposit.Conditions.SelectItem.Audit.${Number(text)}`} />;
    },
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemModifyTime',
    key: 'SystemModifyTime',
    render: (_1, { SystemModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageFastDeposit.DataTable.ModifyUserAccount" description="管理者" />,
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
    isShow: true,
  },
];
