import React, { useContext, useEffect } from 'react';
import { Form, Row, Col, Input, Select, DatePicker } from 'antd';
import { FormattedMessage } from 'react-intl';
import { ConditionContext } from 'contexts/ConditionContext';
import { FORMAT, DEFAULT } from 'constants/dateConfig';
import { RULES } from 'constants/activity/validators';
import FormItemsMemberLevel from 'components/FormItems/MemberLevel';
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect';
import FormItemsIncludeInnerMember from 'components/FormItems/IncludeInnerMember';
import QueryButton from 'components/FormActionButtons/Query';
import { getISODateTimeString } from 'mixins/dateTime';

const { useForm } = Form;
const { Option } = Select;
const { RANGE } = DEFAULT;

const ADVANCED_SEARCH_CONDITION_ITEM_OPTIONS = [1, 2, 3, 4];

const CONDITION_INITIAL_VALUE = {
  AdvancedSearchConditionItem: 1,
  IncludeInnerMember: false,
  Date: RANGE.FROM_TODAY_TO_TODAY,
  Type: null,
};

function Condition({ onReady, onUpdate }) {
  const { isReady } = useContext(ConditionContext);
  const [form] = useForm();
  const { RangePicker } = DatePicker;

  useEffect(() => {
    onUpdate(resultCondition(form.getFieldsValue()));
    onReady(isReady);
  }, [isReady]);

  function resultCondition({ Date, ...restValues }) {
    const [StartTime, EndTime] = Date || [null, null];

    return {
      ...restValues,
      StartTime: getISODateTimeString(StartTime),
      EndTime: getISODateTimeString(EndTime),
    };
  }

  function onFinish(values) {
    onUpdate.bySearch(resultCondition(values));
  }

  return (
    <Form form={form} initialValues={CONDITION_INITIAL_VALUE} onFinish={onFinish}>
      <Row gutter={[16, 8]}>
        <Col sm={8}>
          <Form.Item
            label={<FormattedMessage id="PageLogTransaction.QueryCondition.TransactionDate" description="交易時間" />}
            name="Date"
            rules={[...RULES.REQUIRED]}
          >
            <RangePicker style={{ width: '100%' }} showTime format={FORMAT.DISPLAY.DEFAULT} />
          </Form.Item>
        </Col>
        <Col sm={8}>
          <Form.Item
            label={<FormattedMessage id="PageLogTransaction.QueryCondition.Type" description="類型" />}
            name="Type"
          >
            <Select style={{ width: '100%' }}>
              <Option value={null}>
                <FormattedMessage id="PageLogTransaction.QueryCondition.SelectItem.Type.All" description="全部" />
              </Option>
              <Option value="0">
                <FormattedMessage id="PageLogTransaction.QueryCondition.SelectItem.Type.0" description="出款" />
              </Option>
              <Option value="1">
                <FormattedMessage id="PageLogTransaction.QueryCondition.SelectItem.Type.1" description="入款" />
              </Option>
            </Select>
          </Form.Item>
        </Col>
        <Col sm={8}>
          <FormItemsMemberLevel checkAll />
        </Col>

        <Col sm={8}>
          <FormItemMultipleSelect
            checkAll
            name="Category"
            label={<FormattedMessage id="PageLogTransaction.QueryCondition.Category" description="項目" />}
            url="/api/Option/TrasactionLogCategories"
          />
        </Col>
        <Col sm={8}>
          <Form.Item
            label={<FormattedMessage id="Share.QueryCondition.AdvanceQuery" />}
            name="AdvancedSearchConditionValue"
          >
            <Input
              addonBefore={
                <Form.Item name="AdvancedSearchConditionItem" noStyle>
                  <Select>
                    {ADVANCED_SEARCH_CONDITION_ITEM_OPTIONS.map(id => (
                      <Option value={id} key={id}>
                        <FormattedMessage
                          id={`PageLogTransaction.QueryCondition.SelectItem.AdvancedSearchConditionItemOptions.${id -
                            1}`}
                          description="會員帳號"
                        />
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              }
              style={{ width: '100%' }}
            />
          </Form.Item>
        </Col>
        <Col sm={4}>
          <FormItemsIncludeInnerMember />
        </Col>
        <Col sm={4} align="right">
          <QueryButton />
        </Col>
      </Row>
    </Form>
  );
}

export default Condition;
