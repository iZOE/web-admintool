import React, { createContext } from 'react'
import { Button, Result } from 'antd'
import { ExportOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import uuidv1 from 'uuid/v1'
import * as PERMISSION from 'constants/permissions'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ExportReportButton from 'components/ExportReportButton'
import ReportScaffold from 'components/ReportScaffold'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import SummaryView from './Summary'

const {
    DEPOSIT_WITHDRAW_TRANSACTION_VIEW,
    DEPOSIT_WITHDRAW_TRANSACTION_EXPORT,
} = PERMISSION

export const PageContext = createContext()

const PageView = () => {
    const {
        fetching,
        dataSource,
        condition,
        onReady,
        onUpdateCondition,
    } = useGetDataSourceWithSWR({
        url: '/api/TrasactionLog/Search',
        defaultSortKey: 'Time',
        autoFetch: true,
        dataRefactor: data => {
            data.Data.List.Data = data.Data.List.Data.map(d => {
                d.UniqId = uuidv1()
                return d
            })
            return data.Data
        },
    })

    return (
        <Permission
            functionIds={[DEPOSIT_WITHDRAW_TRANSACTION_VIEW]}
            failedRender={
                <Result
                    status="warning"
                    title={
                        <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                    }
                    subTitle={
                        <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                    }
                />
            }
        >
            <ReportScaffold
                displayResult={condition}
                conditionComponent={
                    <Condition onReady={onReady} onUpdate={onUpdateCondition} />
                }
                summaryComponent={
                    <SummaryView
                        summary={dataSource && dataSource.Summary}
                        totalCount={dataSource && dataSource.List.TotalCount}
                    />
                }
                datatableComponent={
                    <DataTable
                        displayResult={condition}
                        condition={condition}
                        title={
                            <FormattedMessage
                                id="Share.Table.SearchResult"
                                description="查詢結果"
                            />
                        }
                        config={COLUMNS_CONFIG}
                        loading={fetching}
                        dataSource={dataSource && dataSource.List.Data}
                        total={dataSource && dataSource.List.TotalCount}
                        rowKey={record => record.UniqId}
                        extendArea={
                            <Permission
                                functionIds={[
                                    DEPOSIT_WITHDRAW_TRANSACTION_EXPORT,
                                ]}
                                failedRender={
                                    <Button
                                        type="primary"
                                        icon={<ExportOutlined />}
                                        disabled
                                    >
                                        <FormattedMessage id="Share.ActionButton.Export" />
                                    </Button>
                                }
                            >
                                <ExportReportButton
                                    condition={condition}
                                    actionUrl="/api/TrasactionLog/Export"
                                />
                            </Permission>
                        }
                        onUpdate={onUpdateCondition}
                    />
                }
            />
        </Permission>
    )
}

export default PageView
