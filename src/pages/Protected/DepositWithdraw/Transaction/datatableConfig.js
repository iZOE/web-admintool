import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.TransactionNo" description="交易序號" />,
    dataIndex: 'TransactionNo',
    key: 'TransactionNo',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.Type" description="類型" />,
    dataIndex: 'Type',
    key: 'Type',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
    render: (_1, record, _2) => record.TypeText,
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.Category" description="項目" />,
    dataIndex: 'CategoryName',
    key: 'Category',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.OrderNumber" description="來源單號" />,
    dataIndex: 'OrderNumber',
    key: 'OrderNumber',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
    render: text => <OpenMemberDetailButton memberName={text} />,
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.Amount" description="交易金額" />,
    dataIndex: 'Amount',
    key: 'Amount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.BeforeBalance" description="交易前" />,
    dataIndex: 'BeforeBalance',
    key: 'BeforeBalance',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.AfterBalance" description="交易後" />,
    dataIndex: 'AfterBalance',
    key: 'AfterBalance',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    }, // do nothing
    render: text => <ReactIntlCurrencyWithFixedDecimal value={text} />,
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.Time" description="交易時間" />,
    dataIndex: 'Time',
    key: 'Time',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { Time: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageLogTransaction.DataTable.Title.Time" description="交易時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemTime',
    key: 'SystemTime',
    render: (_1, { SystemTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageLogTransaction.DataTable.Title.Remarks" description="備註" />,
    dataIndex: 'Remarks',
    key: 'Remarks',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    render: (_1, { Remarks: text }, _2) => text || NO_DATA,
  },
];
