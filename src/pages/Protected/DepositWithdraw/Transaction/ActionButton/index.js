import React, { useContext } from 'react'
import { MemberDetailContext } from 'contexts/MemberDetailContext'

export default function ActionButton({ memberName }) {
    const { onDisplayMemberDetail } = useContext(MemberDetailContext)

    return <a onClick={() => onDisplayMemberDetail(memberName)}>{memberName}</a>
}
