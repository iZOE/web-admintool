import React from 'react'
import { Statistic, Row, Col, Skeleton } from 'antd'
import { FormattedMessage } from 'react-intl'

const SummaryView = ({ summary, totalCount }) => {
    return summary ? (
        <Row justify="space-between">
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogTransaction.Summary.TotalCount"
                            description="總交易筆數"
                        />
                    }
                    value={totalCount}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogTransaction.Summary.RequestAmount"
                            description="總入款額度"
                        />
                    }
                    value={summary.IncomingAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogTransaction.Summary.OutgoingAmount"
                            description="總出款額度"
                        />
                    }
                    value={summary.OutgoingAmount}
                    precision={2}
                />
            </Col>
            <Col>
                <Statistic
                    title={
                        <FormattedMessage
                            id="PageLogTransaction.Summary.BalanceAmount"
                            description="平衡額度"
                        />
                    }
                    value={summary.BalanceAmount}
                    precision={2}
                />
            </Col>
        </Row>
    ) : (
        <Skeleton active />
    )
}

export default SummaryView
