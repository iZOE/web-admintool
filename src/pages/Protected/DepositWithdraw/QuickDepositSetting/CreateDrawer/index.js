import React, { useState } from 'react'
import {
    message,
    Drawer,
    Form,
    Button,
    Input,
    Select,
    Switch,
    InputNumber,
} from 'antd'
import { useIntl, FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import caller from 'utils/fetcher'
import FormItemsLotteries from 'components/FormItems/Lotteries'
import SwitchWithDefaultName from 'components/SwitchWithDefaultName'

const { useForm } = Form
const { Option } = Select

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 8,
    },
}

export default function EditDrawer({ visible, onClose, boundedMutate }) {
    const intl = useIntl()
    const [form] = useForm()
    const [selectTypeId, SetSelectTypeId] = useState() // 充值 = 1 / 扣款 = 2 / 優惠 = 3 / 人工快充 = 4

    const { data } = useSWR('/api/Option/QuickDepositSetting/Types', url =>
        axios(url).then(res => {
            const types = []
            const subTypes = {}

            res.data.Data.forEach(d => {
                types.push({
                    Id: d.Id,
                    Name: d.Name,
                })
                subTypes[d.Id] = d.SubItems
            })

            return {
                types,
                subTypes,
            }
        })
    )

    async function onFinish() {
        let validateRes
        try {
            validateRes = await form.validateFields()
        } catch (error) {}

        if (validateRes) {
            try {
                onClose()
                form.resetFields()
                const body = {
                    IsAudit: validateRes.IsAudit,
                    IsEnable: validateRes.Status,
                    LimitAmount: validateRes.LimitAmount,
                    TurnoverMultiple: validateRes.TurnoverMultiple,
                    Name: validateRes.ItemName,
                    TypeId: validateRes.TypeId,
                    SubTypeId: validateRes.SubTypeId,
                    CountPerDay: validateRes.CountPerDay,
                    CalculatedPercent: validateRes.CalculatedPercent,
                    LotteryCodes: validateRes.LotteryCodes,
                }
                await caller({
                    method: 'post',
                    endpoint: '/api/QuickDepositSetting',
                    body,
                })

                boundedMutate()
                form.resetFields()
                message.success(
                    intl.formatMessage({
                        id: 'Share.SuccessMessage.UpdateSuccess',
                    })
                )
            } catch (error) {
                message.warning(
                    intl.formatMessage({
                        id: 'Share.ErrorMessage.UnknownError',
                        description: '不明错误',
                    }),
                    10
                )
            }
        }
    }

    if (!data) {
        return null
    }

    return (
        <Drawer
            title={
                <FormattedMessage
                    id="Share.ActionButton.Create"
                    description="新增"
                />
            }
            placement="right"
            closable={false}
            onClose={onClose}
            visible={visible}
            width={500}
            footer={
                <div
                    style={{
                        textAlign: 'right',
                    }}
                >
                    <Button onClick={onClose} style={{ marginRight: 8 }}>
                        <FormattedMessage
                            id="Share.ActionButton.Cancel"
                            description="取消"
                        />
                    </Button>
                    <Button onClick={onFinish} type="primary">
                        <FormattedMessage
                            id="Share.ActionButton.Submit"
                            description="提交"
                        />
                    </Button>
                </div>
            }
        >
            <Form {...layout} name="control-hooks" form={form}>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.QueryCondition.TypeName"
                            description="类型"
                        />
                    }
                    name="TypeId"
                    initialValue={1}
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Select
                        style={{ width: 120 }}
                        onChange={id => {
                            SetSelectTypeId(id)
                            form.setFieldsValue({
                                SubTypeId: data.subTypes[String(id)][0].Id,
                                IsAuditAudit: true,
                                CalculatedPercent: 100,
                                TurnoverMultiple: 1,
                            })
                        }}
                    >
                        {data.types.map(d => (
                            <Option key={d.Id} value={d.Id}>
                                {d.Name}
                            </Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    noStyle
                    shouldUpdate={(prevValues, curValues) =>
                        prevValues.TypeId !== curValues.TypeId ||
                        prevValues.SubTypeId !== curValues.SubTypeId
                    }
                >
                    {({ getFieldValue }) => {
                        return (
                            <Form.Item
                                label={
                                    <FormattedMessage
                                        id="PagePaymentQuickDepositSetting.QueryCondition.SubTypeName"
                                        description="子类型"
                                    />
                                }
                                initialValue={1}
                                name="SubTypeId"
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Select style={{ width: 120 }}>
                                    {data.subTypes[
                                        getFieldValue('TypeId') || 1
                                    ].map(d => (
                                        <Option key={d.Id} value={d.Id}>
                                            {d.Name}
                                        </Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        )
                    }}
                </Form.Item>
                <Form.Item
                    name="ItemName"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.ItemName"
                            description="项目名称"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                {selectTypeId === 3 && (
                    <FormItemsLotteries
                        checkAll
                        name="LotteryCodes"
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.LotteryCodes"
                                description="适用游戏"
                            />
                        }
                        rules={[{ required: true }]}
                    />
                )}
                {selectTypeId !== 2 && (
                    <Form.Item
                        name="IsAudit"
                        valuePropName="checked"
                        initialValue={true}
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.IsAudit"
                                description="流水审核"
                            />
                        }
                    >
                        <Switch
                            checkedChildren={
                                <FormattedMessage id="Share.SwitchButton.IsTrue.Yes" />
                            }
                            unCheckedChildren={
                                <FormattedMessage id="Share.SwitchButton.IsTrue.No" />
                            }
                        />
                    </Form.Item>
                )}
                {selectTypeId !== 2 && (
                    <Form.Item
                        name="TurnoverMultiple"
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.TurnoverMultiple"
                                description="流水倍数"
                            />
                        }
                        required
                        rules={[{ required: true }]}
                    >
                        <InputNumber
                            style={{ width: '100%' }}
                            min={1}
                            precision={2}
                            formatter={value => `${value}倍`}
                        />
                    </Form.Item>
                )}
                {selectTypeId !== 2 && (
                    <Form.Item
                        name="CalculatedPercent"
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.CalculatedPercent"
                                description="计算比例"
                            />
                        }
                    >
                        <InputNumber
                            style={{ width: '100%' }}
                            disabled
                            formatter={value => `${value}%`}
                        />
                    </Form.Item>
                )}
                <Form.Item
                    name="LimitAmount"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.LimitAmount"
                            description="单笔限额"
                        />
                    }
                >
                    <InputNumber style={{ width: '100%' }} />
                </Form.Item>
                <Form.Item
                    name="CountPerDay"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.Count"
                            description="每日次数"
                        />
                    }
                >
                    <InputNumber
                        style={{ width: '100%' }}
                        formatter={value => `${value}次`}
                    />
                </Form.Item>
                <Form.Item
                    name="Status"
                    valuePropName="checked"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.Status"
                            description="状态"
                        />
                    }
                >
                    <SwitchWithDefaultName />
                </Form.Item>
            </Form>
        </Drawer>
    )
}
