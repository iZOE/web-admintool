import React, { useEffect } from 'react'
import { Form, Row, Col, Select } from 'antd'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import axios from 'axios'
import FormItemCheckboxWithLabel from 'components/FormItems/CheckboxWithLabel'
import QueryButton from 'components/FormActionButtons/Query'

const { Option } = Select
const { useForm } = Form
const CONDITION_INITIAL_VALUE = {
    TypeId: null,
    SubTypeId: null,
}

function Condition({ onReady, onUpdate }) {
    const [form] = useForm()

    useEffect(() => {
        onUpdate(form.getFieldsValue())
        onReady(true)
    }, [])

    const { data } = useSWR('/api/Option/QuickDepositSetting/Types', url =>
        axios(url).then(res => {
            const types = []
            const subTypes = {}

            res.data.Data.forEach(d => {
                types.push({
                    Id: d.Id,
                    Name: d.Name,
                })
                subTypes[d.Id] = d.SubItems
            })

            return {
                types,
                subTypes,
            }
        })
    )

    return (
        <Form
            form={form}
            initialValues={CONDITION_INITIAL_VALUE}
            onFinish={onUpdate.bySearch}
        >
            <Row gutter={[16, 8]}>
                <Col sm={6}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.QueryCondition.TypeName"
                                description="类型"
                            />
                        }
                        name="TypeId"
                    >
                        <Select
                            onChange={() => {
                                form.setFieldsValue({ SubTypeId: null })
                            }}
                        >
                            <Option value={null}>
                                <FormattedMessage
                                    id="Share.Dropdown.All"
                                    description="全部"
                                />
                            </Option>
                            {data?.types.map(d => (
                                <Option key={d.Id} value={d.Id}>
                                    {d.Name}
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, curValues) =>
                            prevValues.TypeId !== curValues.TypeId ||
                            prevValues.SubTypeId !== curValues.SubTypeId
                        }
                    >
                        {({ getFieldValue }) => {
                            const typeId = getFieldValue('TypeId')
                            return (
                                <Form.Item
                                    label={
                                        <FormattedMessage
                                            id="PagePaymentQuickDepositSetting.QueryCondition.SubTypeName"
                                            description="子类型"
                                        />
                                    }
                                    name="SubTypeId"
                                >
                                    <Select>
                                        <Option value={null}>
                                            <FormattedMessage
                                                id="Share.Dropdown.All"
                                                description="全部"
                                            />
                                        </Option>
                                        {data?.subTypes[typeId] &&
                                            data?.subTypes[typeId].map(d => (
                                                <Option value={d.Id} key={d.Id}>
                                                    {d.Name}
                                                </Option>
                                            ))}
                                    </Select>
                                </Form.Item>
                            )
                        }}
                    </Form.Item>
                </Col>
                <Col sm={6}>
                    <FormItemCheckboxWithLabel
                        name="Status"
                        formattedMessageId="Share.QueryCondition.IsOnlyEnabled"
                        initialValue={null}
                        onChange={() => {
                            const IsEnabled = form.getFieldsValue().Status
                            if (IsEnabled === true) {
                                form.setFieldsValue({ Status: true })
                            } else {
                                form.setFieldsValue({ Status: null })
                            }
                        }}
                    />
                </Col>
                <Col sm={6} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
