import React, { useEffect, useState } from 'react'
import { message, Drawer, Form, Button, Input, Switch, InputNumber } from 'antd'
import { useIntl, FormattedMessage } from 'react-intl'
import caller from 'utils/fetcher'
import FormItemsLotteries from 'components/FormItems/Lotteries'
import SwitchWithDefaultName from 'components/SwitchWithDefaultName'

const { useForm } = Form

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 8,
    },
}

export default function EditDrawer({
    record,
    visible,
    onClose,
    boundedMutate,
}) {
    const intl = useIntl()
    const [form] = useForm()
    const [selectTypeId, SetSelectTypeId] = useState(
        // 充值 = 1 / 扣款 = 2 / 優惠 = 3 / 人工快充 = 4
        record?.DepositFastSettingTypeId
    )

    useEffect(() => {
        console.log('record', record)
        const { LotteryCodes, ...restEecord } = record || {}
        if (visible) {
            SetSelectTypeId(restEecord?.DepositFastSettingTypeId)
            form.setFieldsValue({
                ...restEecord,
                LotteryCodes: LotteryCodes || [],
            })
        }
    }, [visible])

    async function onFinish() {
        let validateRes
        try {
            validateRes = await form.validateFields()
        } catch (error) {}

        if (validateRes) {
            try {
                onClose()
                const body = {
                    IsAudit: validateRes.IsAudit,
                    Status: validateRes.Status,
                    LimitAmount: validateRes.LimitAmount,
                    CountPerDay: validateRes.Count,
                    CalculatedPercent: record.CalculatedPercent,
                    TurnoverMultiple: validateRes.TurnoverMultiple,
                    Name: validateRes.ItemName,
                }

                await caller({
                    method: 'put',
                    endpoint: `/api/QuickDepositSetting/${record.DepositFastSettingId}`,
                    body,
                })

                boundedMutate()

                message.success(
                    intl.formatMessage({
                        id: 'Share.SuccessMessage.UpdateSuccess',
                    })
                )
            } catch (error) {
                message.warning(
                    intl.formatMessage({
                        id: 'Share.ErrorMessage.UnknownError',
                        description: '不明错误',
                    }),
                    10
                )
            }
        }
    }

    return (
        <Drawer
            title={
                <FormattedMessage
                    id="Share.CommonKeys.Edit"
                    description="编辑"
                />
            }
            placement="right"
            closable={false}
            onClose={onClose}
            visible={visible}
            width={500}
            footer={
                <div
                    style={{
                        textAlign: 'right',
                    }}
                >
                    <Button onClick={onClose} style={{ marginRight: 8 }}>
                        <FormattedMessage
                            id="Share.ActionButton.Cancel"
                            description="取消"
                        />
                    </Button>
                    <Button onClick={onFinish} type="primary">
                        <FormattedMessage
                            id="Share.ActionButton.Submit"
                            description="提交"
                        />
                    </Button>
                </div>
            }
        >
            <Form
                {...layout}
                name="control-hooks"
                form={form}
                // initialValues={record}
            >
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.QueryCondition.TypeName"
                            description="类型"
                        />
                    }
                >
                    {record && record.TypeName}
                </Form.Item>
                <Form.Item
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.QueryCondition.SubTypeName"
                            description="子类型"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    {record && record.SubTypeName}
                </Form.Item>
                <Form.Item
                    name="ItemName"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.ItemName"
                            description="项目名称"
                        />
                    }
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                {selectTypeId === 3 && (
                    <FormItemsLotteries
                        disabled={true}
                        name="LotteryCodes"
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.LotteryCodes"
                                description="适用游戏"
                            />
                        }
                    />
                )}
                {selectTypeId !== 2 && (
                    <Form.Item
                        name="IsAudit"
                        valuePropName="checked"
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.IsAudit"
                                description="流水审核"
                            />
                        }
                    >
                        <Switch
                            checkedChildren={
                                <FormattedMessage id="Share.SwitchButton.IsTrue.Yes" />
                            }
                            unCheckedChildren={
                                <FormattedMessage id="Share.SwitchButton.IsTrue.No" />
                            }
                        />
                    </Form.Item>
                )}
                {selectTypeId !== 2 && (
                    <Form.Item
                        name="TurnoverMultiple"
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.TurnoverMultiple"
                                description="流水倍数"
                            />
                        }
                    >
                        <InputNumber
                            style={{ width: '100%' }}
                            min={1}
                            precision={2}
                            formatter={value => `${value}倍`}
                        />
                    </Form.Item>
                )}
                {selectTypeId !== 2 && (
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="PagePaymentQuickDepositSetting.DataTable.Title.CalculatedPercent"
                                description="计算比例"
                            />
                        }
                    >
                        {(record && record.CalculatedPercent) === null
                            ? `--`
                            : `${record.CalculatedPercent}%`}
                    </Form.Item>
                )}
                <Form.Item
                    name="LimitAmount"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.LimitAmount"
                            description="单笔限额"
                        />
                    }
                >
                    <InputNumber style={{ width: '100%' }} />
                </Form.Item>
                <Form.Item
                    name="Count"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.Count"
                            description="每日次数"
                        />
                    }
                >
                    <InputNumber
                        style={{ width: '100%' }}
                        formatter={value => `${value}次`}
                    />
                </Form.Item>
                <Form.Item
                    name="Status"
                    valuePropName="checked"
                    label={
                        <FormattedMessage
                            id="PagePaymentQuickDepositSetting.DataTable.Title.Status"
                            description="状态"
                        />
                    }
                >
                    <SwitchWithDefaultName />
                </Form.Item>
            </Form>
        </Drawer>
    )
}
