import React from 'react';
import { FormattedMessage } from 'react-intl';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import BreakSpaces from 'components/BreakSpaces';
import { NO_DATA } from 'constants/noData';

export const COLUMNS_CONFIG = [
  {
    title: (
      <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.DepositFastSettingId" description="序号" />
    ),
    dataIndex: 'DepositFastSettingId',
    key: 'DepositFastSettingId',
    isShow: true,
    fixed: 'left',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.TypeName" description="类型" />,
    dataIndex: 'DepositFastSettingTypeId',
    key: 'DepositFastSettingTypeId',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => record.TypeName,
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.SubTypeName" description="子类型" />,
    dataIndex: 'DepositFastSettingSubTypeId',
    key: 'DepositFastSettingSubTypeId',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => record.SubTypeName,
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.ItemName" description="项目名称" />,
    dataIndex: 'ItemName',
    key: 'ItemName',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.IsAudit" description="审核" />,
    dataIndex: 'IsAudit',
    key: 'IsAudit',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { IsAudit }, _2) => {
      return IsAudit === null ? (
        NO_DATA
      ) : (
        <FormattedMessage id={`Share.SwitchButton.IsTrue.${!!IsAudit ? 'Yes' : 'No'}`} />
      );
    },
  },
  {
    title: (
      <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.TurnoverMultiple" description="流水倍数" />
    ),
    dataIndex: 'TurnoverMultiple',
    key: 'TurnoverMultiple',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { TurnoverMultiple }, _2) => {
      return TurnoverMultiple ? (
        <FormattedMessage id="Share.Unit.Multiple" values={{ value: TurnoverMultiple }} />
      ) : (
        NO_DATA
      );
    },
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.GameSettings" description="参加游戏" />,
    dataIndex: 'GameSettings',
    key: 'GameSettings',
    isShow: true,
    render: text => <BreakSpaces>{text}</BreakSpaces>,
  },
  {
    title: (
      <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.CalculatedPercent" description="计算比例" />
    ),
    dataIndex: 'CalculatedPercent',
    key: 'CalculatedPercent',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (text ? `${text}%` : '--'),
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.LimitAmount" description="单笔限额" />,
    dataIndex: 'LimitAmount',
    key: 'LimitAmount',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.Count" description="每日次数" />,
    dataIndex: 'Count',
    key: 'Count',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: text => (text ? text : '--'),
  },
  {
    title: <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.Status" description="状态" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, record, _2) => (
      <FormattedMessage id={`PagePaymentQuickDepositSetting.DataTable.Items.Status.${record.Status}`} />
    ),
  },
  {
    title: <FormattedMessage id="Share.Fields.ModifiedTime" description="異動時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => {
      return;
    },
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <FormattedMessage id="PagePaymentQuickDepositSetting.DataTable.Title.ModifyUserAccount" description="异动人员" />
    ),
    dataIndex: 'ModifyUserAccount',
    key: 'ModifyUserAccount',
    isShow: true,
  },
];
