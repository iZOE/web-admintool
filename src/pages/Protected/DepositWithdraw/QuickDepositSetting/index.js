import React, { createContext, useState, useMemo } from 'react'
import { Button, Result } from 'antd'
import { FormattedMessage } from 'react-intl'
import * as PERMISSION from 'constants/permissions'
import ReportScaffold from 'components/ReportScaffold'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import usePermission from 'hooks/usePermission'
import Condition from './Condition'
import { COLUMNS_CONFIG } from './datatableConfig'
import ActionButton from './ActionButton'
import EditDrawer from './EditDrawer'
import CreateDrawer from './CreateDrawer'

const {
    DEPOSIT_WITHDRAW_QUICK_DEPOSIT_SETTING_VIEW,
    DEPOSIT_WITHDRAW_QUICK_DEPOSIT_SETTING_EDIT,
} = PERMISSION

export const PageContext = createContext()

// condition initial value

const PageView = () => {
    const [hasEditPermission] = usePermission(
        DEPOSIT_WITHDRAW_QUICK_DEPOSIT_SETTING_EDIT
    )

    const [drawerData, setDrawerData] = useState({
        visible: false,
        record: null,
    })
    const [createDrawerData, setCreateDrawerData] = useState({
        visible: false,
    })

    const {
        fetching,
        dataSource,
        onUpdateCondition,
        condition,
        onReady,
        boundedMutate,
    } = useGetDataSourceWithSWR({
        url: '/api/QuickDepositSetting/Search',
        defaultSortKey: 'DepositFastSettingId',
        autoFetch: true,
    })

    const columnsConfig = useMemo(() => {
        if (hasEditPermission) {
            return COLUMNS_CONFIG.concat([
                {
                    title: (
                        <FormattedMessage
                            id="Share.ActionButton.Manage"
                            description="管理"
                        />
                    ),
                    dataIndex: 'action',
                    key: 'action',
                    isShow: true,
                    width: 160,
                    fixed: 'right',
                    render: (_1, record, _2) => (
                        <ActionButton record={record} />
                    ),
                },
            ])
        }
        return COLUMNS_CONFIG
    }, [hasEditPermission])

    function onOpenEditDrawer({ record }) {
        setDrawerData({ record, visible: true })
    }

    function onOpenCreateDrawer() {
        setCreateDrawerData({ visible: true })
    }

    function onCloseDrawer() {
        setDrawerData({ record: null, visible: false })
    }

    function onCloseCreateDrawer() {
        setCreateDrawerData({ visible: false })
    }

    return (
        // 為了配合啟動全螢幕時 後續的流程 可以抓到domNode
        <PageContext.Provider value={{ onOpenEditDrawer, boundedMutate }}>
            <Permission
                functionIds={[DEPOSIT_WITHDRAW_QUICK_DEPOSIT_SETTING_VIEW]}
                failedRender={
                    <Result
                        status="warning"
                        title={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Title" />
                        }
                        subTitle={
                            <FormattedMessage id="Share.ErrorMessage.Forbidden.Message" />
                        }
                    />
                }
            >
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            title={
                                <FormattedMessage
                                    id="Share.Table.SearchResult"
                                    description="查詢結果"
                                />
                            }
                            config={columnsConfig}
                            loading={fetching}
                            dataSource={dataSource && dataSource.Data}
                            total={dataSource && dataSource.TotalCount}
                            onUpdate={onUpdateCondition}
                            rowKey={record => record.DepositFastSettingId}
                            extendArea={
                                <Permission
                                    functionIds={[
                                        DEPOSIT_WITHDRAW_QUICK_DEPOSIT_SETTING_EDIT,
                                    ]}
                                >
                                    <Button
                                        type="primary"
                                        onClick={() => onOpenCreateDrawer()}
                                    >
                                        <FormattedMessage id="Share.ActionButton.Create" />
                                    </Button>
                                </Permission>
                            }
                        />
                    }
                />
                {hasEditPermission && (
                    <>
                        <EditDrawer
                            record={drawerData.record}
                            visible={drawerData.visible}
                            onClose={onCloseDrawer}
                            boundedMutate={boundedMutate}
                        />
                        <CreateDrawer
                            visible={createDrawerData.visible}
                            onClose={onCloseCreateDrawer}
                            boundedMutate={boundedMutate}
                        />
                    </>
                )}
            </Permission>
        </PageContext.Provider>
    )
}

export default PageView
