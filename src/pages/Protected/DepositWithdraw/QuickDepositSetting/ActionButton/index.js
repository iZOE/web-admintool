import React, { useContext } from 'react'
import { Button } from 'antd'
import { FormattedMessage } from 'react-intl'
import { PageContext } from '../index'

export default function ActionButton({ record }) {
    const { onOpenEditDrawer } = useContext(PageContext)

    return (
        <Button
            type="link"
            onClick={() => {
                onOpenEditDrawer({ record })
            }}
        >
            <FormattedMessage id="Share.ActionButton.Edit" description="編輯" />
        </Button>
    )
}
