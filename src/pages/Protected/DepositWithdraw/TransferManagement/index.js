import React, { createContext } from 'react'
import useGetDataSourceWithSWR from 'hooks/useGetDataSourceWithSWR'
import { FormattedMessage } from 'react-intl'
import ReportScaffold from 'components/ReportScaffold'
import DataTable from 'components/DataTable'
import Permission from 'components/Permission'
import ExportReportButton from 'components/ExportReportButton'
import * as PERMISSION from 'constants/permissions'
import Condition from './Condition'
import SummaryView from './Summary'
import { COLUMNS_CONFIG } from './datatableConfig'

const {
    DEPOSIT_WITHDRAW_MEMBER_TRANSFER_VIEW,
    DEPOSIT_WITHDRAW_MEMBER_TRANSFER_EXPORT,
} = PERMISSION

export const PageContext = createContext()

const TransferManagementPage = () => {
    const {
        fetching,
        dataSource,
        boundedMutate,
        onUpdateCondition,
        onReady,
        condition,
    } = useGetDataSourceWithSWR({
        url: '/api/FundTransferLog/Manage',
        defaultSortKey: 'CreateTime',
        autoFetch: true,
    })

    return (
        <PageContext.Provider value={{ boundedMutate, dataSource }}>
            <Permission functionIds={[DEPOSIT_WITHDRAW_MEMBER_TRANSFER_VIEW]}>
                <ReportScaffold
                    displayResult={condition}
                    conditionComponent={
                        <Condition
                            onReady={onReady}
                            onUpdate={onUpdateCondition}
                        />
                    }
                    summaryComponent={
                        <SummaryView
                            summary={dataSource && dataSource.Summary}
                        />
                    }
                    datatableComponent={
                        <DataTable
                            displayResult={condition}
                            condition={condition}
                            loading={fetching}
                            title={
                                <FormattedMessage id="Share.Table.SearchResult" />
                            }
                            config={COLUMNS_CONFIG}
                            dataSource={dataSource && dataSource.List.Data}
                            total={dataSource && dataSource.List.TotalCount}
                            rowKey={record => record.FundTransferNo}
                            extendArea={
                                <>
                                    <Permission
                                        functionIds={[
                                            DEPOSIT_WITHDRAW_MEMBER_TRANSFER_EXPORT,
                                        ]}
                                    >
                                        <ExportReportButton
                                            condition={condition}
                                            actionUrl="/api/FundTransferLog/Manage/Export"
                                        />
                                    </Permission>
                                </>
                            }
                            onUpdate={onUpdateCondition}
                        />
                    }
                />
            </Permission>
        </PageContext.Provider>
    )
}

export default TransferManagementPage
