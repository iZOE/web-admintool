import React from 'react';
import { FormattedMessage } from 'react-intl';
import OpenMemberDetailButton from 'components/OpenMemberDetailButton';
import DateWithFormat from 'components/DateWithFormat';
import ReactIntlCurrencyWithFixedDecimal from 'components/ReactIntlCurrencyWithFixedDecimal';
import CloseTicketButton from './CloseTicketButton';
import { NO_DATA } from 'constants/noData';

const TRANSFER_STATUS = {
  PROCESSING: 1,
  DONE: 2,
  FAIL: 3,
  PENDING: 4,
  CLOSED: 5,
};

export const COLUMNS_CONFIG = [
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.FundTransferNo" description="訂單ID" />,
    dataIndex: 'FundTransferNo',
    key: 'FundTransferNo',
    isShow: true,
    fixed: 'left',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => text,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.CreateTime" description="轉賬時間" />,
    dataIndex: 'CreateTime',
    key: 'CreateTime',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { CreateTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.MemberName" description="會員帳號" />,
    dataIndex: 'MemberName',
    key: 'MemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, _1, _2) => {
      return <OpenMemberDetailButton memberId={text} memberName={text} />;
    },
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.CustomMemberName" description="第三方帐号" />,
    dataIndex: 'CustomMemberName',
    key: 'CustomMemberName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, _1, _2) => {
      return text ? <OpenMemberDetailButton memberId={text} memberName={text} /> : NO_DATA;
    },
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.Description" description="轉賬方" />,
    dataIndex: 'Description',
    key: 'Description',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, _1, _2) => {
      return text;
    },
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.TransferModes" description="轉賬類型" />,
    dataIndex: 'TransferMode',
    key: 'TransferMode',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => {
      return <FormattedMessage id={`PageTransferManagement.Conditions.SelectItem.TransferModes.${text}`} />;
    },
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.DepositMemberAccount" description="入款會員名稱" />,
    dataIndex: 'DepositMemberAccount',
    key: 'DepositMemberAccount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => (value ? value : NO_DATA),
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.RequestAmount" description="訂單金額" />,
    dataIndex: 'RequestAmount',
    key: 'RequestAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.AdministrationFee" description="行政費" />,
    dataIndex: 'AdministrationFee',
    key: 'AdministrationFee',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.Amount" description="已領金額" />,
    dataIndex: 'Amount',
    key: 'Amount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.RefundAmount" description="退還金額" />,
    dataIndex: 'RefundAmount',
    key: 'RefundAmount',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: value => <ReactIntlCurrencyWithFixedDecimal value={value} />,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.Status" description="狀態" />,
    dataIndex: 'Status',
    key: 'Status',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (text, record, index) => {
      return <FormattedMessage id={`PageTransferManagement.Conditions.SelectItem.StatusOptions.${text}`} />;
    },
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.Account" description="建立人员" />,
    dataIndex: 'Account',
    key: 'Account',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { Account: text }, _2) => text || NO_DATA,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.ModifyTime" description="更新時間" />,
    dataIndex: 'ModifyTime',
    key: 'ModifyTime',
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: (
      <>
        <FormattedMessage id="PageTransferManagement.DataTable.ModifyTime" description="更新時間" />
        <FormattedMessage id="Share.Fields.SystemTime" description="(系統時間)" />
      </>
    ),
    dataIndex: 'SystemModifyTime',
    key: 'SystemModifyTime',
    render: (_1, { SystemModifyTime: time }, _2) => <DateWithFormat time={time} />,
  },
  {
    title: <FormattedMessage id="PageTransferManagement.DataTable.ModifyUserName" description="管理者" />,
    dataIndex: 'ModifyUserName',
    key: 'ModifyUserName',
    isShow: true,
    sorter: (a, b) => {
      return;
    },
    sortDirections: ['descend', 'ascend'],
    render: (_1, { ModifyUserName: text }, _2) => text || NO_DATA,
  },
  {
    title: <FormattedMessage id="Share.ActionButton.Manage" description="管理" />,
    dataIndex: 'action',
    key: 'action',
    isShow: true,
    fixed: 'right',
    render: (text, record, index) =>
      record.Status === TRANSFER_STATUS.PENDING ? (
        <CloseTicketButton FundTransferNo={record.FundTransferNo} />
      ) : (
        NO_DATA
      ),
  },
];
