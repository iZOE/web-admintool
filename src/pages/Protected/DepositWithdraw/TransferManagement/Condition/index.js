import React, { useContext, useEffect } from 'react'
import { Form, Row, Col, Input, Select, DatePicker } from 'antd'
import { FORMAT, DEFAULT } from 'constants/dateConfig'
import { FormattedMessage, useIntl } from 'react-intl'
import { ConditionContext } from 'contexts/ConditionContext'
import QueryButton from 'components/FormActionButtons/Query'
import FormItemIncludeInnerMember from 'components/FormItems/IncludeInnerMember'
import WithSelectAllCheckBoxs from 'components/WithSelectAllCheckboxs'
import FormItemMultipleSelect from 'components/FormItems/MultipleSelect'
import { getISODateTimeString } from 'mixins/dateTime'

const { Option } = Select
const { RangePicker } = DatePicker
const { RANGE } = DEFAULT

const WHEN_STATUS_SELECT_ALL = [1, 2, 3, 4, 5]
const WHEN_TRANSFER_MODE_SELECT_ALL = [0, 1, 2, 3, 4, 5, 6]

const CONDITION_INITIAL_VALUE = {
    UpdateDate: RANGE.FROM_TODAY_TO_TODAY,
    Status: WHEN_STATUS_SELECT_ALL,
    TransferModes: WHEN_TRANSFER_MODE_SELECT_ALL,
    AdvancedSearchConditionItem: 1,
}

const StatusData = WHEN_STATUS_SELECT_ALL.map(v => ({
    Id: v,
    Name: (
        <FormattedMessage
            id={`PageTransferManagement.Conditions.SelectItem.StatusOptions.${v}`}
        />
    ),
}))

const TRANSFER_MODE = WHEN_TRANSFER_MODE_SELECT_ALL.map(v => ({
    Id: v,
    Name: (
        <FormattedMessage
            id={`PageTransferManagement.Conditions.SelectItem.TransferModes.${v}`}
        />
    ),
}))
const ADVANCE_SEARCH_CONDITION_ITEMS = [1, 2, 3, 4]

const Condition = ({ onReady, onUpdate }) => {
    const { isReady } = useContext(ConditionContext)
    const [form] = Form.useForm()
    const intl = useIntl()

    useEffect(() => {
        onUpdate(resultCondition(form.getFieldsValue()))
        onReady(isReady)
    }, [isReady])

    function resultCondition({ UpdateDate, ...restValues }) {
        const [StartDate, EndDate] = UpdateDate || [null, null]

        return {
            ...restValues,
            StartDate: getISODateTimeString(StartDate),
            EndDate: getISODateTimeString(EndDate),
        }
    }

    function onFinish(values) {
        onUpdate.bySearch(resultCondition(values))
    }

    return (
        <Form
            form={form}
            onFinish={onFinish}
            initialValues={CONDITION_INITIAL_VALUE}
        >
            <Row gutter={[16, 8]}>
                <Col sm={8}>
                    <Form.Item
                        label={`${intl.formatMessage({
                            id: 'PageTransferManagement.Conditions.UpdateDate',
                        })}：`}
                        name="UpdateDate"
                    >
                        <RangePicker
                            style={{ width: '100%' }}
                            showTime
                            format={FORMAT.DISPLAY.DEFAULT}
                        />
                    </Form.Item>
                </Col>
                <Col sm={8}>
                    <WithSelectAllCheckBoxs
                        label={intl.formatMessage({
                            id: 'PageTransferManagement.Conditions.Status',
                        })}
                        name="Status"
                        data={StatusData}
                        whenSelectEmptyPlaceHolder={''}
                    />
                </Col>
                <Col sm={8}>
                    <WithSelectAllCheckBoxs
                        label={intl.formatMessage({
                            id:
                                'PageTransferManagement.Conditions.TransferModes',
                        })}
                        name="TransferModes"
                        data={TRANSFER_MODE}
                        whenSelectEmptyPlaceHolder={''}
                    />
                </Col>
                <Col sm={8}>
                    <FormItemMultipleSelect
                        checkAll
                        name="TransferType"
                        label={
                            <FormattedMessage id="PageTransferManagement.Conditions.TransferType" />
                        }
                        url="/api/Option/TransferTypes"
                    />
                </Col>
                <Col sm={10}>
                    <Form.Item
                        label={
                            <FormattedMessage
                                id="Share.QueryCondition.AdvanceQuery"
                                description="進階查詢"
                            />
                        }
                        name="AdvancedSearchConditionValue"
                    >
                        <Input
                            addonBefore={
                                <Form.Item
                                    name="AdvancedSearchConditionItem"
                                    noStyle
                                >
                                    <Select>
                                        {ADVANCE_SEARCH_CONDITION_ITEMS.map(
                                            v => (
                                                <Option value={v} key={v}>
                                                    <FormattedMessage
                                                        id={`PageTransferManagement.Conditions.SelectItem.AdvancedSearchConditionItemOptions.${v}`}
                                                    />
                                                </Option>
                                            )
                                        )}
                                    </Select>
                                </Form.Item>
                            }
                            style={{ width: '100%' }}
                        />
                    </Form.Item>
                </Col>
                <Col sm={4}>
                    <FormItemIncludeInnerMember />
                </Col>
                <Col sm={2} align="right">
                    <QueryButton />
                </Col>
            </Row>
        </Form>
    )
}

export default Condition
