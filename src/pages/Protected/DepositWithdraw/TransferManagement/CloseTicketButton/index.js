import React, { useContext } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import { message, Modal } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { useIntl, FormattedMessage } from "react-intl";
import { PageContext } from "../index";
import _cloneDeep from "lodash.clonedeep";

const mutateTheDataRowByFundTransferNo = (dataSource, fundTransferNo) => {
  const clonedDataSource = _cloneDeep(dataSource);
  clonedDataSource.List.Data.forEach(d => {
    if (d.FundTransferNo === fundTransferNo) {
      // 改成5 => 關閉
      d.Status = 5;
    }
  });
  return clonedDataSource;
};

function CloseTicketButton({ FundTransferNo }) {
  const CloseTicketAPI = `/api/FundTransferLog/${FundTransferNo}/Close`; // get
  const intl = useIntl();

  const { dataSource, boundedMutate } = useContext(PageContext);

  const issueCloseTicket = () => {
    axios
      .get(CloseTicketAPI)
      .then(res => {
        if (!res.data.Status) {
          message.error(
            `${intl.formatMessage({
              id: "Share.ErrorMessage.ErrorKey.JohnsonApiException"
            })}`
          );
        } else {
          boundedMutate(
            mutateTheDataRowByFundTransferNo(dataSource, FundTransferNo),
            false
          );
          message.success(
            `${intl.formatMessage({
              id: "Share.SuccessMessage.UpdateSuccess"
            })}`
          );
        }
      })
      .catch(err => {
        message.error(
          `${intl.formatMessage({
            id: "Share.ErrorMessage.ErrorKey.JohnsonApiException"
          })}: ${err.message.status}, ${err.message.statusText}`
        );
      });
  };

  const isIssueCloseTicket = () => {
    Modal.confirm({
      title: intl.formatMessage({
        id: "PageTransferManagement.CloseTicketPromptTitle"
      }),
      icon: <ExclamationCircleOutlined />,
      content: intl.formatMessage({
        id: "PageTransferManagement.CloseTicketPromptContent"
      }),
      okText: intl.formatMessage({ id: "Share.ActionButton.Confirm" }),
      cancelText: intl.formatMessage({ id: "Share.ActionButton.Cancel" }),
      onOk: issueCloseTicket
    });
  };

  return (
    <Button type="primary" onClick={isIssueCloseTicket}>
      <FormattedMessage
        id="Share.ActionButton.CloseTicket"
        description="關閉單據"
      />
    </Button>
  );
}

CloseTicketButton.propTypes = {};

export default CloseTicketButton;
