import React from 'react';
import { Statistic, Row, Col, Skeleton } from 'antd';
import { FormattedMessage } from 'react-intl';

const SummaryView = ({ summary }) => {
  if (!summary) {
    return <Skeleton active />;
  }

  const {
    AdministrationFeeTotalAmount,
    FundTransferCount,
    FundTransferTotalAmount,
    RefundAmountTotalAmount,
    TotalAmount,
    TotalCount
  } = summary;
  return (
    <>
      <Row gutter={24}>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageTransferManagement.Summary.TotalCount"
                description="總訂單數"
              />
            }
            value={TotalCount || 0}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageTransferManagement.Summary.FundTransferCount"
                description="轉賬人数"
              />
            }
            value={FundTransferCount || 0}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageTransferManagement.Summary.FundTransferTaotalAmount"
                description="轉賬總額"
              />
            }
            value={FundTransferTotalAmount || 0}
            precision={2}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageTransferManagement.Summary.AdministrationFeeTaotalAmount"
                description="行政費用"
              />
            }
            value={AdministrationFeeTotalAmount || 0}
            precision={2}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageTransferManagement.Summary.TaotalAmount"
                description="已領總額"
              />
            }
            value={TotalAmount || 0}
            precision={2}
          />
        </Col>
        <Col span={4}>
          <Statistic
            title={
              <FormattedMessage
                id="PageTransferManagement.Summary.RefundAmountTaotalAmount"
                description="退還總額"
              />
            }
            value={RefundAmountTotalAmount || 0}
            precision={2}
          />
        </Col>
      </Row>
    </>
  );
};

export default SummaryView;
