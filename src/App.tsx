import React, { useContext } from 'react';
import useAxiosMiddleware from 'hooks/useAxiosMiddleware';
import { UserContext, API_PROCESS } from 'contexts/UserContext';
import Login from './pages/Login';
import Protected from './pages/Protected';
import SignalR from './SignalR';

import './App.less';

const App = () => {
  useAxiosMiddleware();
  const { userFetchStatus, data: userData } = useContext(UserContext);

  if (userFetchStatus === API_PROCESS.LOADING) {
    return <div>Loading...</div>;
  } else if (
    userFetchStatus === API_PROCESS.ERROR ||
    (userFetchStatus === API_PROCESS.SUCCESS && !userData)
  ) {
    return <Login />;
  } else if (userFetchStatus === API_PROCESS.SUCCESS && userData) {
    return (
      <>
        {/*<SignalR />*/}
        <Protected />
      </>
    );
  }
  return <div>ERROR</div>;
};

export default App;
