const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(
    "/api",
    proxy({
      target: process.env.REACT_APP_PROXY,
      changeOrigin: true,
      secure: false,
    })
  );
  app.use(
    "/signalr",
    proxy({
      target: process.env.REACT_APP_PROXY,
      changeOrigin: true,
      secure: false,
    })
  );
};
