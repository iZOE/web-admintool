import React, { useEffect } from "react";
import $ from "jquery";

export default function SignalR() {
  useEffect(() => {
    $.getScript("/signalr/hubs", () => {
      let connection = $.hubConnection();
      let userHubProxy = connection.createHubProxy("user");
      // $.connection.hub.url = "/signalr";
      // 不加這段會無法順利連到signalR
      /*
      $.connection.mainHub.client.getMessage = (res) => {
        getMessage(res);
      };
      */
      connection
        .start({
          jsonp: false,
          transport: "auto",
          withCredentials: false,
        })
        .done(() => {
          // resolve($.connection.mainHub);
          console.log("Connection established, ID: " + connection.id);
          console.log(
            "Connection established, Transport: " + connection.transport.name
          );
        })
        .fail((err) => {
          console.log("err", err);
          // reject();
        });
    });
    /*
    let connection = $.hubConnection();
    connection.hub.url = "/signalr";
    let contosoChatHubProxy = connection.createHubProxy("user");
    contosoChatHubProxy.on("noOp", function(userName, message) {
      console.log(userName + " " + message);
    });
    connection
      .start()
      .done(function() {
        console.log("Now connected, connection ID=" + connection.id);
      })
      .fail(function() {
        console.log("Could not connect");
      });
      */
  }, []);
  return <div />;
}
