export default function ClassNamer(obj) {
  return Object.keys(obj)
    .reduce((prev, key) => (obj[key] ? [...prev, key] : prev), [])
    .join(' ');
}
