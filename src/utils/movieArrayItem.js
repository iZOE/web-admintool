/**
 * 移動 Array 裡某個 Item 到另一個 Item 後面
 *
 * @author Ray Chang <ray.chang@johnsontechinc.com.tw>
 * @param {(string|number)[]} array 需要移動項目位置的Array
 * @param {string|number} item 被移動的項目
 * @param {string|number} target 要移動到哪個項目之後
 * @returns {(string|number)[]} 移動項目位置後的Array
 */
const movieArrayItem = (array, item, target) => {
    let temp = 0
    for (let i = 0; i < array.length; i++) {
        if (array[i] === item) {
            temp = 1
        }
        if (array[i] === target) {
            temp = 0
            array[i] = item
            continue
        }
        array[i] = array[i + temp]
    }
    return array
}

export default movieArrayItem
