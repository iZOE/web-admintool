import axios from "axios";

export default ({ url, method = "get" }) => {
  return payload =>
    axios({
      method,
      url,
      [method === "get" ? "params" : "data"]: payload
    })
      .then(res => {
        console.log("res", res.data);
        if (res && res.data) {
          if (res.data.Status) {
            return res.data.Data;
          } else {
            return Promise.reject(res.data);
          }
        }
      })
      .catch(error => Promise.reject(error));
};
