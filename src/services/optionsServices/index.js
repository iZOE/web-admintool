import makeService from "utils/makeService";

const optionsService = () => {
  const getAgentOptions = makeService({
    method: "get",
    url: "/api/Option/AgentIdName"
  });

  const getGroupOptions = makeService({
    method: "get",
    url: "/api/Option/Group"
  });

  const getTopGroupOptions = makeService({
    method: "get",
    url: "/api/Option/TopGroup"
  });

  const getMemberLevel = makeService({
    method: "get",
    url: "/api/Option/MemberLevel"
  });

  const getBetTypes = id =>
    makeService({
      method: "get",
      url: `/api/Option/BetTypes//${id}`
    })();

  const getBetSubTypes = id =>
    makeService({
      method: "get",
      url: `/api/Option/BetSubTypes/${id}`
    })();

  const getApproveState = () =>
    makeService({
      method: "get",
      url: `/api/Option/Group/ApproveState`
    })();

  return {
    getAgentOptions,
    getGroupOptions,
    getTopGroupOptions,
    getMemberLevel,
    getBetTypes,
    getBetSubTypes,
    getApproveState
  };
};

export default optionsService;
