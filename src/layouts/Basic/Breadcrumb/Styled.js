import styled from 'styled-components'
import { PageHeader as PageHeaderComponent } from 'antd'

export const PageHeader = styled(PageHeaderComponent)`
    .ant-tabs-top > .ant-tabs-nav:before {
        border-bottom: unset;
    }
`
