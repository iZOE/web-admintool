import React, { useContext } from 'react'
import { Breadcrumb } from 'antd'
import { Link } from 'react-router-dom'
import ICON_MAPPER from 'constants/iconMapper'
import { BreadCrumbsContext } from 'contexts/BreadcrumbsContext'
import { PageHeader } from './Styled'

export default function AppBreadcrumb() {
    const { routes, suffixBreadcrumbs, title, subTitle } = useContext(
        BreadCrumbsContext
    )

    const breadcrumbItem = routes.map((item, index) =>
        item.path ? (
            <Breadcrumb.Item key={`breadcrumb_${index}`}>
                {item.routerKey && ICON_MAPPER[item.routerKey]}
                <Link to={item.path}>{item.breadcrumbName}</Link>
            </Breadcrumb.Item>
        ) : (
            <Breadcrumb.Item key={`breadcrumb_${index}`}>
                {item.routerKey && ICON_MAPPER[item.routerKey]}
                <span>{item.breadcrumbName}</span>
            </Breadcrumb.Item>
        )
    )

    return (
        <PageHeader
            ghost={false}
            subTitle={subTitle}
            title={
                <>
                    <Breadcrumb>
                        {breadcrumbItem}
                        {suffixBreadcrumbs}
                    </Breadcrumb>
                    {title && <div>{title}</div>}
                </>
            }
            style={{ padding: '4px 12px' }}
        ></PageHeader>
    )
}
