import React, { useState, useEffect, useContext, useMemo } from 'react';
import { Space, Spin } from 'antd';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';
import { FORMAT } from 'constants/dateConfig';
import { UserContext } from 'contexts/UserContext';

const SYSTEM_TIMEZONE = 'Asia/Taipei';

const getDisplayTime = (time, timezone = SYSTEM_TIMEZONE) => {
  return moment(time)
    .tz(timezone)
    .format(FORMAT.DISPLAY.DEFAULT);
};

export default function Time() {
  const { data: userCtxData } = useContext(UserContext);
  const [currentTime, setCurrentTime] = useState(
    moment().format(FORMAT.DEFAULT.FULL)
  );

  const siteTimeZone = useMemo(() => {
    if (userCtxData) {
      return userCtxData?.Locale?.TimeZone;
    }
  }, [userCtxData]);

  useEffect(() => {
    function timer() {
      setTimeout(() => {
        setCurrentTime(moment().format(FORMAT.DEFAULT.FULL));
        timer();
      }, 1000);
    }

    timer();
    return () => {
      clearTimeout(timer);
    };
  }, []);

  if (!currentTime) {
    return null;
  }

  return currentTime && siteTimeZone ? (
    <Space>
      <span>
        <FormattedMessage id="Layout.Header.Time.System" />
        {getDisplayTime(currentTime)}
      </span>
      <span>
        <FormattedMessage id="Layout.Header.Time.Site" />
        {getDisplayTime(currentTime, siteTimeZone)}
      </span>
    </Space>
  ) : (
    <Spin />
  );
}
