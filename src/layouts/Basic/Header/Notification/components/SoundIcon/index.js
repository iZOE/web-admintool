import React, { useState, useContext } from 'react'
import { PageContext } from '../../index'
import { SoundOutlinedIcon } from './Styled'

export default function SoundIcon({ type, className }) {
    const { remindStatus, sendNotificationSetting } = useContext(PageContext)
    const currentStatus = remindStatus ? !remindStatus.includes(type) : false
    const [close, setClose] = useState(currentStatus)

    const handleMute = () => {
        sendNotificationSetting(type, !close)
        setClose(!close)
    }

    return (
        <a onClick={() => handleMute()}>
            <SoundOutlinedIcon className={`${className} ${close && 'mute'}`} />
        </a>
    )
}
