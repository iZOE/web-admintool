import React from 'react'
import styled from 'styled-components'
import { SoundOutlined } from '@ant-design/icons'

export const SoundOutlinedIcon = styled(SoundOutlined)`
    margin-right: 0.5rem;
    &.mute {
        position: relative;
        &::before {
            content: '';
            display: block;
            width: 18px;
            height: 1px;
            transform: rotate(135deg);
            position: absolute;
            background: #1890ff;
            top: 6px;
            left: -2px;
        }
    }
    &.disabled {
        color: #ccc;
        &::before {
            background: none;
        }
    }
`
