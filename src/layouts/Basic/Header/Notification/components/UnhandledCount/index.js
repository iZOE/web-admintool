import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Link } from 'react-router-dom'

export default function UnhandledCount({ link, type, count }) {
    return (
        <>
            <FormattedMessage
                id={`Layout.Header.Notification.${type}.Unhandled`}
                defaultMessage="Unhandled"
            />
            {': '}
            <Link to={link} style={{ display: 'inline-block' }}>
                {count || 0}
            </Link>
        </>
    )
}
