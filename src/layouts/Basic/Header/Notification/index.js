import React, { useEffect, useState, createContext } from 'react'
import useSWR, { mutate } from 'swr'
import axios from 'axios'
import caller from 'utils/fetcher'
import { useHistory } from 'react-router-dom'
import { notification, Dropdown, Badge } from 'antd'
import { DownOutlined } from '@ant-design/icons'
import { FormattedMessage } from 'react-intl'
import { useIntl } from 'react-intl'
import usePermission from 'hooks/usePermission'
import * as PERMISSION from 'constants/permissions'
import mp3file from './assets/dindong.mp3'
import UnhandledCount from './components/UnhandledCount'
import SoundIcon from './components/SoundIcon'
import { Menu } from './Styled'

const NOTIFICATION = {
    APPROVAL: 1,
    WITHDRAWAL: 2,
    DEPOSIT: 4,
}

const refreshSetting = {
    refreshInterval: 40000,
    refreshWhenHidden: true,
}

const {
    DEPOSIT_WITHDRAW_WITHDRAW_REMIND,
    DEPOSIT_WITHDRAW_DEPOSIT_REMIND,
} = PERMISSION

let audio = new Audio(mp3file)

export const PageContext = createContext()

export default function Notification() {
    const intl = useIntl()
    const [unhandleCount, setUnhandleCount] = useState(0)
    const history = useHistory()

    const [
        depositWithdrawWithdrawRemind,
        depositWithdrawDepositRemind,
    ] = usePermission(
        DEPOSIT_WITHDRAW_WITHDRAW_REMIND,
        DEPOSIT_WITHDRAW_DEPOSIT_REMIND
    )

    // 提醒狀態
    const { data: remindStatus } = useSWR(
        depositWithdrawWithdrawRemind || depositWithdrawDepositRemind
            ? '/api/User/remind-status'
            : null,
        url =>
            axios({ url, closeMessage: true }).then(
                res => res.data.Data.RemindStatus
            ),
        {
            shouldRetryOnError: false,
        }
    )

    // 審核
    const { data: approveData } = useSWR('/api/Approve/WaitApproveCount', url =>
        axios(url).then(res => res.data.Data)
    )

    // 提現
    const { data: withdrawalData } = useSWR(
        depositWithdrawWithdrawRemind
            ? ['/api/Withdrawal/Unhandled', remindStatus]
            : null,
        (url, rs) =>
            axios(url).then(res => {
                if (
                    rs.includes(NOTIFICATION.WITHDRAWAL) &&
                    res.data.Data.Unhandled -
                        res.data.Data.UnhandledAndNotRemind >
                        0
                ) {
                    notificationWarning('Withdraw')
                }
                return {
                    unhandleData: res.data.Data,
                    updateTime: res.headers.date,
                }
            }),
        refreshSetting
    )

    // 充值
    const { data: depositData } = useSWR(
        depositWithdrawDepositRemind
            ? ['/api/Deposit/Unhandled', remindStatus]
            : null,
        (url, rs) =>
            axios(url).then(res => {
                if (rs.includes(NOTIFICATION.DEPOSIT) && res.data.Data) {
                    notificationWarning('Deposit')
                }
                return { unhandleData: res.data.Data }
            }),
        refreshSetting
    )

    useEffect(() => {
        const WaitApproveCount =
            (approveData && approveData.WaitApproveCount) || 0
        const withdrawalCount =
            (withdrawalData &&
                withdrawalData.unhandleData &&
                withdrawalData.unhandleData.Unhandled) ||
            0
        const depositCount = (depositData && depositData.unhandleData) || 0

        setUnhandleCount(WaitApproveCount + withdrawalCount + depositCount)
    }, [approveData, withdrawalData, depositData])

    function notificationWarning(type) {
        audio.play()
        notification.warning({
            message: intl.formatMessage({
                id: `Layout.Header.Notification.${type}.Message`,
                defaultMessage: 'Message',
            }),
            description: intl.formatMessage({
                id: `Layout.Header.Notification.${type}.Description`,
                defaultMessage: 'Description',
            }),
            onClick: () =>
                history.push(
                    `/deposit-withdraw/${type.toLowerCase()}?Status=1`
                ),
        })
    }

    function sendNotificationSetting(type, close) {
        caller({
            method: 'put',
            endpoint: `/api/user/${type}/${close}`,
        }).then(() => {
            mutate('/api/User/remind-status')
        })
    }

    const menu = (
        <Menu>
            <Menu.Item
                key="0"
                icon={
                    <SoundIcon
                        type={NOTIFICATION.APPROVAL}
                        className="disabled"
                    />
                }
            >
                <UnhandledCount
                    link="/?State=0"
                    type="Approve"
                    count={approveData && approveData.WaitApproveCount}
                />
            </Menu.Item>
            {depositWithdrawWithdrawRemind && (
                <Menu.Item key="1">
                    <SoundIcon type={NOTIFICATION.WITHDRAWAL} />
                    <UnhandledCount
                        link="/deposit-withdraw/withdraw?Status=1"
                        type="Withdraw"
                        count={
                            withdrawalData &&
                            withdrawalData.unhandleData &&
                            withdrawalData.unhandleData.Unhandled
                        }
                    />
                </Menu.Item>
            )}
            {depositWithdrawDepositRemind && (
                <Menu.Item key="2">
                    <SoundIcon type={NOTIFICATION.DEPOSIT} />
                    <UnhandledCount
                        link="/deposit-withdraw/deposit?Status=1"
                        type="Deposit"
                        count={depositData && depositData.unhandleData}
                    />
                </Menu.Item>
            )}
        </Menu>
    )

    return (
        <PageContext.Provider
            value={{
                remindStatus,
                sendNotificationSetting,
            }}
        >
            <Dropdown overlay={menu}>
                <a
                    className="ant-dropdown-link"
                    onClick={e => e.preventDefault()}
                >
                    <Badge status={unhandleCount > 0 ? 'error' : 'success'} />
                    <FormattedMessage
                        id="Layout.Header.Notification.AllWaitItems"
                        defaultMessage="未处理项目"
                    />
                    {': '}
                    {unhandleCount} <DownOutlined />
                </a>
            </Dropdown>
        </PageContext.Provider>
    )
}
