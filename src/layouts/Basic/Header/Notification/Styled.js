import { Menu as MenuComponent } from 'antd'
import styled from 'styled-components'

export const Menu = styled(MenuComponent)`
    .ant-dropdown-menu-item > a {
        display: inline-block;
        color: #1890ff;
    }
`
