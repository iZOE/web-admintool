import React, { useContext } from 'react'
import { Dropdown, Menu, Avatar } from 'antd'
import { mutate } from 'swr'
import { UserOutlined, PoweroffOutlined } from '@ant-design/icons'
import { UserContext } from 'contexts/UserContext'
import cookies from 'js-cookie'
import { FormattedMessage } from 'react-intl'
export default function UserMenu() {
    const { data: userData } = useContext(UserContext)

    const handleOnClick = () => {
        cookies.remove('gameadmin_auth')
        mutate('local/user')
        window.location.reload()
    }

    const menu = (
        <Menu>
            <Menu.Item disabled>
                <UserOutlined />
                {userData && userData.UserName + ' - ' + userData.GroupName}
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item onClick={handleOnClick}>
                <PoweroffOutlined />
                {
                    <FormattedMessage
                        id="Share.ActionButton.Logout"
                        defaultMessage="Logout"
                    />
                }
            </Menu.Item>
        </Menu>
    )
    return (
        <Dropdown overlay={menu} placement="bottomRight">
            <Avatar icon={<UserOutlined />} size="small" />
        </Dropdown>
    )
}
