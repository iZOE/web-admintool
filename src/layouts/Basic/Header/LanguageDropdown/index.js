import React, { useContext } from 'react';
import { Dropdown, Menu, Button } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { LANG_OPTIONS, LanguageContext } from 'i18n/LanguageProvider';

export default function LanguageDropdown() {
  const { locale, onUpdateLocale } = useContext(LanguageContext);
  const menu = (
    <Menu>
      {Object.keys(LANG_OPTIONS).map(key => (
        <Menu.Item key={key}>
          <Button type="link" onClick={() => handleUpdateLocal(key)}>
            {LANG_OPTIONS[key].displayName}
          </Button>
        </Menu.Item>
      ))}
    </Menu>
  );

  function handleUpdateLocal(newLocale) {
    onUpdateLocale(newLocale);
  }

  return (
    <Dropdown overlay={menu} placement="bottomRight">
      <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
        {LANG_OPTIONS[locale].displayName} <DownOutlined />
      </a>
    </Dropdown>
  );
}
