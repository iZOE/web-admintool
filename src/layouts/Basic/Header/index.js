import React from 'react'
import { Layout, Divider, Space, Menu } from 'antd'
import UserMenu from './UserMenu'
import LanguageDropdown from './LanguageDropdown'
import Time from './Time'
import Notification from './Notification'

const { Header } = Layout

export default function AppHeader() {
    return (
        <Header id="app-header" style={{ overflow: 'hidden' }}>
            <Space size="large">
                <Time />
                <Divider type="vertical" />
                <Notification />
                <Divider type="vertical" />
                <LanguageDropdown />
                <Divider type="vertical" />
                <UserMenu />
            </Space>
        </Header>
    )
}
