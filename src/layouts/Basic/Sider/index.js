import React, { memo, useContext, useMemo, useState, useEffect } from 'react';
import { Layout, Menu, Button } from 'antd';
import { useHistory, useLocation } from 'react-router-dom';
import useSWR from 'swr';
import axios from 'axios';
import ICON_MAPPER from 'constants/iconMapper';
import { UserContext } from 'contexts/UserContext';
import { BreadCrumbsContext } from 'contexts/BreadcrumbsContext';
import snakeToCamel from 'utils/snakeToCamel';

const { Sider } = Layout;
const { SubMenu } = Menu;
const DASHOARD_ROUTE = {
  path: '/',
  routerKey: 'dashboard',
  breadcrumbName: 'Dashboard',
};

function AppSider({ displayDrawer, handleDrawer }) {
  const location = useLocation();
  const history = useHistory();
  const [keys, setKeys] = useState();
  const [collapsed, setCollapsed] = useState(false);
  const { data: userData } = useContext(UserContext);
  const { onUpdateBreadCrumb } = useContext(BreadCrumbsContext);

  const { data: allowMenuData } = useSWR('/api/Menu/AllowMenu', url => axios(url).then(res => res.data.Data));

  const allowMenu = useMemo(
    () =>
      allowMenuData
        ? allowMenuData.map(firstMenu => ({
            ...firstMenu,
            routerKey:
              Array.isArray(firstMenu.SubMenus) && firstMenu.SubMenus.length
                ? snakeToCamel(firstMenu.SubMenus[0].Link.split('/')[1])
                : null,
          }))
        : [],
    [allowMenuData]
  );

  // console.log('location', location)

  // 從pathname判斷哪個menu應該active
  useEffect(() => {
    // console.log('location.pathname', location.pathname)
    if (allowMenu) {
      if (location.pathname !== '/') {
        allowMenu.forEach(firstMenu => {
          firstMenu.SubMenus.forEach(subMenu => {
            const regLink = new RegExp(subMenu.Link);
            if (regLink.test(location.pathname)) {
              onUpdateBreadCrumb({
                routes: [
                  DASHOARD_ROUTE,
                  {
                    path: null,
                    routerKey: firstMenu.routerKey,
                    breadcrumbName: firstMenu.Name,
                  },
                  {
                    path: null,
                    routerKey: null,
                    breadcrumbName: subMenu.Name,
                  },
                ],
              });
              setKeys({
                selectedKeys: [subMenu.Name],
                openKeys: [firstMenu.Name],
              });
            }
          });
        });
      } else {
        onUpdateBreadCrumb({
          routes: [DASHOARD_ROUTE],
          title: null,
        });
        setKeys({
          selectedKeys: [],
          openKeys: [],
        });
      }
    }
  }, [allowMenu, location.pathname]);

  const handleRedirect = ({ Link }) => {
    history.push(Link);
  };

  const handleOpenKeys = key => {
    const result = keys.openKeys.indexOf(key);
    const newKeys = keys.openKeys.slice();
    if (result === -1) {
      setKeys({
        ...keys,
        openKeys: [...newKeys, key],
      });
    } else {
      newKeys.splice(result, 1);
      setKeys({
        ...keys,
        openKeys: newKeys,
      });
    }
  };

  const onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(key => keys.openKeys.indexOf(key) === -1);
    setKeys(prev => ({
      selectedKeys: prev?.selectedKeys,
      openKeys: [latestOpenKey],
    }));
  };

  if (!keys) {
    return null;
  }

  return (
    <Sider
      collapsible
      id="app-sidebar"
      width="240px"
      breakpoint="lg"
      collapsed={collapsed}
      onBreakpoint={() => {}}
      onCollapse={() => {
        setCollapsed(prev => !prev);
      }}
    >
      {!collapsed && (
        <Button
          type="link"
          id="app-logo"
          onClick={() => {
            // handleDrawer(!displayDrawer)
            history.push('/');
          }}
        >
          {userData && userData.AgentName}
        </Button>
      )}
      {allowMenu && (
        <Menu
          id="app-menu"
          theme="dark"
          mode="inline"
          onOpenChange={onOpenChange}
          selectedKeys={keys && keys.selectedKeys}
          openKeys={keys && keys.openKeys}
        >
          {allowMenu.map(firstMenu => (
            <SubMenu
              key={firstMenu.Name}
              icon={firstMenu.routerKey && ICON_MAPPER[firstMenu.routerKey]}
              title={firstMenu.Name}
              onTitleClick={dom => handleOpenKeys(dom.key)}
            >
              {firstMenu.SubMenus.map(subMenu => (
                <Menu.Item key={subMenu.Name} onClick={() => handleRedirect(subMenu)}>
                  {subMenu.Name}
                </Menu.Item>
              ))}
            </SubMenu>
          ))}
        </Menu>
      )}
    </Sider>
  );
}

export default AppSider;
