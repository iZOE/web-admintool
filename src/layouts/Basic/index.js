import React, { useState } from 'react';
import { Layout } from 'antd';
import { Wrapper } from './Styled';
import AppHeader from './Header';
import AppBreadcrumb from './Breadcrumb';
import AppSider from './Sider';
import AgentSwitch from './Drawer';

const { Content, Footer } = Layout;

export default function BasicLayout({ children, ...rest }) {
  const [showDrawer, setShowDrawer] = useState(false);
  const { noWrapWithContent } = rest;
  return (
    <Wrapper>
      <Layout>
        <AppSider displayDrawer={showDrawer} handleDrawer={status => setShowDrawer(status)} />
        {/*<AgentSwitch
          displayDrawer={showDrawer}
          onClose={() => setShowDrawer(false)}
        />*/}
        <Layout>
          <AppHeader />
          <AppBreadcrumb />
          {noWrapWithContent ? children : <Content style={{ padding: '0.5rem' }}>{children}</Content>}
          <Footer id="app-footer">{process.env.REACT_APP_BUILD_SOURCEBRANCHNAME || 'ADMIN TOOL'}</Footer>
        </Layout>
      </Layout>
    </Wrapper>
  );
}
