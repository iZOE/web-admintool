import styled from 'styled-components';

export const Wrapper = styled.div`
  #app-sidebar {
    min-height: 100vh;
  }
  #app-menu {
    border-right: 0;
  }
  #app-logo {
    height: 64px;
    line-height: 64px;
    margin-left: 24px;
    padding-left: 0;
    color: #fff;
    font-weight: bold;
    font-size: 18px;
  }
  #app-header {
    z-index: 1;
    background: #fff;
    padding: 0 16px;
    text-align: right;
    box-shadow: 0 1px 4px rgba(0, 21, 41, 0.08);
  }
  #app-breadcrumb {
    margin: 16px;
  }
  #app-footer {
    text-align: center;
    padding: 16px;
  }
  .ant-layout-sider-zero-width-trigger {
    top: 12px;
  }
`;

export const SwitchButton = styled.button`
  display: flex;
`;
