import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  margin: 1.5rem 0;
  .ant-card {
    margin-bottom: 1.5rem;
  }
`;
