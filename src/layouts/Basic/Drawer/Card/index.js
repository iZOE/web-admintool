import React from "react";
import { Card } from "antd";
import { Wrapper } from "./Styled";

export default function AgentCard({ name }) {
  return (
    <Wrapper>
      <Card title={name.title}>
        <p>{name.content}</p>
      </Card>
    </Wrapper>
  );
}
