import React, { useState } from "react";
import AgentCard from "./Card";
import { Drawer, Select } from "antd";
import { AGENT_NAME as agent } from "./constants";

const { Option } = Select;

export default function AgentSwitch({ displayDrawer, onClose }) {
  const [filterAgent, setFilterAgent] = useState(agent);
  function onChange(value) {
    setFilterAgent(
      agent.filter(
        a => a.agentCode.toLowerCase().indexOf(value.toLowerCase()) >= 0
      )
    );
  }

  return (
    <Drawer
      title="Switch Agent"
      closable={false}
      maskClosable={true}
      onClose={onClose}
      visible={displayDrawer}
    >
      <Select
        showSearch
        style={{ width: "100%" }}
        placeholder="type an Agent Name"
        optionFilterProp="children"
        onChange={value => onChange(value)}
      >
        {agent &&
          agent.map((agent, index) => (
            <Option value={agent.agentCode} key={index.toString()}>
              {agent.title}
            </Option>
          ))}
      </Select>
      {filterAgent.map((name, index) => (
        <AgentCard name={name} key={index.toString()} />
      ))}
    </Drawer>
  );
}
