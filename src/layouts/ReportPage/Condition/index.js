import React from 'react';
import { Collapse } from 'antd';
import { FormattedMessage } from 'react-intl';
import { ConditionContextProvider } from 'contexts/ConditionContext';
import { Condition } from './Styled';

const CONDITION_PANEL_KEY = 1;

export default function ConditionWrapper({ children }) {
  return (
    <ConditionContextProvider>
      <Condition>
        <Collapse defaultActiveKey={[CONDITION_PANEL_KEY]}>
          <Collapse.Panel
            key={CONDITION_PANEL_KEY}
            header={
              <FormattedMessage
                id="Share.ReportScaffold.Condition"
                defaultMessage="Condition"
              />
            }
          >
            {children}
          </Collapse.Panel>
        </Collapse>
      </Condition>
    </ConditionContextProvider>
  );
}
