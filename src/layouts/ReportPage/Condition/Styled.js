import styled from "styled-components";

export const Condition = styled.div`
  .ant-collapse-content-active {
    & > .ant-collapse-content-box {
      padding-bottom: 8px;
      .ant-form {
        & > .ant-row {
          .ant-form-item {
            margin-bottom: 0;
          }
        }
      }
    }
  }
`;
