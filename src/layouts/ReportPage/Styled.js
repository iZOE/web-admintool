import styled from "styled-components";

export const PageWrapper = styled.div`
  overflow: auto;
  .card {
    background-color: #eff2f5;
  }
`;
