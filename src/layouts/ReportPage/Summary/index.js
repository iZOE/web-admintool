import React from "react";
import { Collapse } from "antd";
import { FormattedMessage } from "react-intl";

export default function SummaryWrapper({ summaryResult, children }) {
  return (
    <Collapse>
      <Collapse.Panel
        header={
          <FormattedMessage
            id="Share.ReportScaffold.Summary"
            defaultMessage="Summary"
          />
        }
      >
        {summaryResult ? (
          children
        ) : (
          <FormattedMessage
            id="Share.ReportScaffold.PleaseSearch"
            defaultMessage="Please enter search button"
          />
        )}
      </Collapse.Panel>
    </Collapse>
  );
}
