import React from "react";
import { Space } from "antd";

export default function ReportPageWrapper({ children }) {
  return (
    <Space direction="vertical" style={{ width: "100%" }}>
      {children}
    </Space>
  );
}
