import React from 'react'
import { Drawer } from 'antd'

export default function BasicDrawerWrapper({ children, ...restProps }) {
    const { title } = { ...restProps }

    return (
        <Drawer
            {...restProps}
            closable={false}
            headerStyle={{ paddingBottom: 0, border: 'none' }}
            placement="right"
            title={title}
            width={720}
        >
            {children}
        </Drawer>
    )
}
