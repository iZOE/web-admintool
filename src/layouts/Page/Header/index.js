import React from "react";
import { PageHeader } from "antd";

export default function LayoutPageHeader({ title, subTitle }) {
  return (
    <PageHeader
      ghost={false}
      title={title}
      subTitle={subTitle}
      onBack={() => window.history.back()}
    />
  );
}
