import React from "react";
import { Card, Space, Typography } from "antd";
import { ArrowLeftOutlined } from "@ant-design/icons";

const { Title, Text } = Typography;

export default function LayoutPageBody({
  children,
  title,
  subTitle,
  goBack = true,
  ...restProps
}) {
  return (
    <Card
      bordered={false}
      size="small"
      title={
        <Space size="small">
          {goBack && (
            <ArrowLeftOutlined
              onClick={() => window.history.back()}
              style={{ cursor: "pointer" }}
            />
          )}
          <Title level={4} style={{ margin: 0 }}>
            {title}
          </Title>
          <Text>{subTitle}</Text>
        </Space>
      }
      {...restProps}
    >
      {children}
    </Card>
  );
}
