import React from "react";
import { FormattedMessage } from "react-intl";
import { Descriptions } from "antd";

export default function DrawerConditionWrapper({ children }) {
  return (
    <Descriptions
      title={<FormattedMessage id="Share.QueryCondition.Title" />}
      column={1}
      size="small"
      style={{ paddingLeft: 16 }}
    >
      <Descriptions.Item>{children}</Descriptions.Item>
    </Descriptions>
  );
}
