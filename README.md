This project is for Web Admin Tool.

## Available Scripts

In the project directory, you can run:

### `yarn start:${env}`

Runs the app in the development mode.<br>
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

Note: Available environments: `dev`, `qat` and `stg`.

## Environments

- Node JS v.12.4.1
- React v.16.12.0
- Antd v.4.0.0
